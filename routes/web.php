<?php

use Illuminate\Support\Facades\Route;
// Admin
use App\Http\Controllers\admin\dashboard\DashboardController;
use App\Http\Controllers\admin\login\LoginController;
use App\Http\Controllers\admin\admin\AdminController;
use App\Http\Controllers\admin\user\UserController;
use App\Http\Controllers\admin\user_lab\UserLabController;
use App\Http\Controllers\admin\customer\CustomerController;
use App\Http\Controllers\admin\lab_admin\LabAdminController;
use App\Http\Controllers\admin\office_admin\OfficeAdminController;
use App\Http\Controllers\admin\office\OfficeController;
use App\Http\Controllers\admin\customertype\CustomerTypeController;
use App\Http\Controllers\admin\casepan\CasepanController;
use App\Http\Controllers\admin\call\CallController;
use App\Http\Controllers\admin\casepannumber\CasepanNumberController;
use App\Http\Controllers\admin\grade\GradeController;
use App\Http\Controllers\admin\schedule\ScheduleController;
use App\Http\Controllers\admin\calendar\CalendarController;
use App\Http\Controllers\admin\department\DepartmentController;
use App\Http\Controllers\admin\staff\StaffController;
use App\Http\Controllers\admin\doctor\DoctorController;
use App\Http\Controllers\admin\category\CategoryController;
use App\Http\Controllers\admin\categoryproduct\CategoryProductController;
use App\Http\Controllers\admin\categoryproductoffice\CategoryProductOfficeController;
use App\Http\Controllers\admin\banner\BannerController;
use App\Http\Controllers\admin\stage\StageController;
use App\Http\Controllers\admin\productstage\ProductStageController;
use App\Http\Controllers\admin\email\EmailController;
use App\Http\Controllers\admin\notification\NotificationController;
use App\Http\Controllers\admin\setting\SettingController;
use App\Http\Controllers\admin\addoncategory\AddoncategoryController;
use App\Http\Controllers\admin\subaddoncategory\SubaddoncategoryController;
use App\Http\Controllers\admin\role\RoleController;
use App\Http\Controllers\admin\moduleMaster\ModuleMasterController;
use App\Http\Controllers\admin\modulePermission\ModulePermissionController;
use App\Http\Controllers\admin\labcase\LabcaseController;
use App\Http\Controllers\admin\billing\BillingController;
use App\Http\Controllers\admin\system_email\SystemEmailController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
  Route::get('/admin/login',[LoginController::class, 'index'])->name('admin.login');
  Route::get('/',[LoginController::class, 'index'])->name('admin.login');
  Route::post('admin/admin/CheckExistEmail', [LoginController::class, 'CheckExistEmail'])->name('admin.admin.CheckExistEmail');
  Route::get('/reset-password/{code}', [LoginController::class, 'reset_password'])->name('admin.reset_password');
  Route::post('/reset-password-action',[LoginController::class, 'reset_password_action'])->name('admin.reset_password_action');
  Route::get('admin/login/same_day_delivery_crone_10_30', [LoginController::class, 'same_day_delivery_crone_10_30'])->name('admin.login.Same_day_delivery_crone');
  Route::get('admin/login/same_day_delivery_crone_10_00', [LoginController::class, 'same_day_delivery_crone_10_00'])->name('admin.login.Same_day_delivery_crone');

  // forgot password start
  Route::get('admin/admin/forgot-password', [LoginController::class, 'forgotPassword'])->name('admin.admin.forgotPassword');
  Route::post('admin/admin/forgot-password-email', [LoginController::class, 'forgotPasswordEmailCheck'])->name('admin.forgot_password_email_check');
  Route::get('admin/forgot-password-change/{code}', [LoginController::class, 'forgotPasswordChange'])->name('admin.admin.forgotPasswordChange');
  Route::post('admin/forgot-password-action', [LoginController::class, 'forgotPasswordAction'])->name('admin.admin.forgotPasswordAction');
  // forgot password end

  Route::get('/admin/register/customer',[LoginController::class, 'registration'])->name('admin.register.customer');
  Route::post('/admin/login/registration_action',[LoginController::class, 'registration_action'])->name('registration_action');

  Route::post('/admin/login/login_action',[LoginController::class, 'login_action'])->name('login_action');

  Route::group(['middleware' => ['loginAuth']], function() {
  Route::get('/admin/dashboard',[DashboardController::class, 'index'])->name('admin.dashboard')->middleware("restricted_route");
  
  Route::get('/admin/viewCustomerRequest/{id}',[DashboardController::class, 'viewCustomerRequest'])->name('admin.viewCustomerRequest');
  Route::get('/admin/approveCustomer/{id}',[DashboardController::class, 'approveCustomer'])->name('admin.approveCustomer');
  Route::get('/admin/rejectCustomer/{id}',[DashboardController::class, 'rejectCustomer'])->name('admin.rejectCustomer');
  
  Route::get('admin/dashboard/ajax_pending_listing',[DashboardController::class, 'ajax_listing'])->name('admin.dashboard.ajaxPendingListing');

  Route::post('admin/dashboard/show_lab_office_details',[DashboardController::class, 'show_lab_office_details'])->name('admin.dashboard.show_lab_office_details');
  
  
  // Office Profile
  Route::get('/admin/OfficeProfile',[CustomerController::class, 'OfficeProfile'])->name('admin.OfficeProfile');
  Route::post('/admin/OfficeProfileAction',[CustomerController::class, 'OfficeProfileAction'])->name('admin.OfficeProfileAction');

  // Lab Profile
  Route::get('/admin/LabProfile',[CustomerController::class, 'LabProfile'])->name('admin.LabProfile');
  Route::post('/admin/LabProfileAction',[CustomerController::class, 'LabProfileAction'])->name('admin.LabProfileAction');


  //Admin Module
  Route::get('admin/admin',[AdminController::class, 'index'])->name('admin.admin')->middleware("restricted_route")->middleware("restricted_route");
  Route::get('admin/admin/create',[AdminController::class, 'create'])->name('admin.admin.create')->middleware("restricted_route");
  Route::post('admin/admin/store',[AdminController::class, 'store'])->name('admin.admin.store');
  Route::get('admin/admin/edit/{iAdminId}',[AdminController::class, 'edit'])->name('admin.admin.edit')->middleware("restricted_route");
  Route::get('admin/admin/ajax_listing',[AdminController::class, 'ajax_listing'])->name('admin.admin.ajaxListing')->middleware("restricted_route");
  Route::get('admin/admin/ChangePassword/{id}', [AdminController::class, 'showChangePasswordForm'])->name('admin.admin.ChangePassword');
  Route::post('admin/admin/changePassword', [AdminController::class, 'changePassword'])->name('admin.admin.changePassword');
  Route::get('/admin/logout', [LoginController::class, 'logout'])->name('admin.logout');
  
  Route::get('admin/dashboard/index_data',[DashboardController::class, 'index_data'])->name('admin.dashboard.index_data');
  Route::post('admin/dashboard/lab_action',[DashboardController::class, 'LabProfileAction'])->name('admin.dashboard.lab_action');
  Route::post('admin/dashboard/office_action',[DashboardController::class, 'OfficeProfileAction'])->name('admin.dashboard.office_action');

  //Customer Module 
  Route::get('admin/customer',[CustomerController::class, 'index'])->name('admin.customer')->middleware("restricted_route");
  Route::get('admin/customer/create',[CustomerController::class, 'create'])->name('admin.customer.create')->middleware("restricted_route");
  Route::post('admin/customer/store',[CustomerController::class, 'store'])->name('admin.customer.store');
  Route::get('admin/customer/edit/{id}',[CustomerController::class, 'edit'])->name('admin.customer.edit')->middleware("restricted_route");
  Route::get('admin/customer/ajax_listing',[CustomerController::class, 'ajax_listing'])->name('admin.customer.ajaxListing')->middleware("restricted_route");
  Route::get('admin/customer/ChangePassword/{id}', [CustomerController::class, 'showChangePasswordForm'])->name('admin.customer.ChangePassword');
  Route::post('admin/customer/changePassword', [CustomerController::class, 'changePassword'])->name('admin.customer.changePassword');

   //Doctor Module

  //Lab Admin Module Start july 21
  Route::get('admin/lab_admin',[LabAdminController::class, 'index'])->name('admin.lab_admin')->middleware("restricted_route");
  Route::get('admin/lab_admin/create',[LabAdminController::class, 'create'])->name('admin.lab_admin.create')->middleware("restricted_route");
  Route::get('admin/lab_admin/create-staff/{iCustomerId}',[LabAdminController::class, 'create_staff'])->name('admin.lab_admin.create_staff');
  Route::get('admin/lab_admin/listing/{iCustomerId}',[LabAdminController::class, 'own_listing'])->name('admin.lab_admin.listing')->middleware("restricted_route");
  Route::post('admin/lab_admin/store',[LabAdminController::class, 'store'])->name('admin.lab_admin.store');
  Route::get('admin/lab_admin/generatepassword',[LabAdminController::class, 'generatepassword'])->name('admin.lab_admin.generatepassword');
  Route::get('admin/lab_admin/edit/{id}',[LabAdminController::class, 'edit'])->name('admin.lab_admin.edit')->middleware("restricted_route");
  Route::get('admin/lab_admin/ajax_listing',[LabAdminController::class, 'ajax_listing'])->name('admin.lab_admin.ajaxListing')->middleware("restricted_route");
  
  //Lab Admin Module End july 21

  //Office Admin Module Start july 21
  Route::get('admin/office_admin',[OfficeAdminController::class, 'index'])->name('admin.office_admin')->middleware("restricted_route");
  Route::get('admin/office_admin/create',[OfficeAdminController::class, 'create'])->name('admin.office_admin.create')->middleware("restricted_route");
  Route::get('admin/office_admin/create-staff/{iCustomerId}',[OfficeAdminController::class, 'create_staff'])->name('admin.office_admin.create_staff');
  Route::get('admin/office_admin/listing/{iCustomerId}',[OfficeAdminController::class, 'own_listing'])->name('admin.office_admin.listing')->middleware("restricted_route");
  Route::post('admin/office_admin/store',[OfficeAdminController::class, 'store'])->name('admin.office_admin.store');
  Route::get('admin/office_admin/generatepassword',[OfficeAdminController::class, 'generatepassword'])->name('admin.office_admin.generatepassword');
  Route::get('admin/office_admin/edit/{id}',[OfficeAdminController::class, 'edit'])->name('admin.office_admin.edit')->middleware("restricted_route");
  Route::get('admin/office_admin/ajax_listing',[OfficeAdminController::class, 'ajax_listing'])->name('admin.office_admin.ajaxListing')->middleware("restricted_route");
  //Office Admin Module End july 21

  //  Route::get('admin/doctor',[DoctorController::class, 'index'])->name('admin.doctor');
  Route::get('admin/doctor/create',[DoctorController::class, 'create'])->name('admin.doctor.create')->middleware("restricted_route");
  Route::post('admin/doctor/store',[DoctorController::class, 'store'])->name('admin.doctor.store');
  Route::get('admin/doctor/edit/{id}',[DoctorController::class, 'edit'])->name('admin.doctor.edit')->middleware("restricted_route");
  Route::get('admin/doctor/ajax_listing',[DoctorController::class, 'ajax_listing'])->name('admin.doctor.ajaxListing')->middleware("restricted_route");
  Route::post('admin/doctor/fetch_office',[DoctorController::class, 'fetch_office'])->name('admin.doctor.fetch_office');
  
  Route::get('admin/doctorlist/{iCustomerId}/{iOfficeId}',[DoctorController::class, 'doctorlist'])->name('admin.doctorlist')->middleware("restricted_route");
  Route::get('admin/doctor/doctorcreate/{iCustomerId}/{iOfficeId}',[DoctorController::class, 'doctorcreate'])->name('admin.doctor.doctor-create');
  Route::post('admin/doctor/doctorstore',[DoctorController::class, 'doctorstore'])->name('admin.doctor.doctorstore');
  Route::get('admin/doctor/doctoredit/{iCustomerId}/{iOfficeId}/{id}',[DoctorController::class, 'doctoredit'])->name('admin.doctor.doctoredit');
  Route::get('admin/doctor/doctor_ajax_listing',[DoctorController::class, 'doctor_ajax_listing'])->name('admin.doctor.doctorajaxListing');
  Route::get('admin/doctor/{iCustomerId?}',[DoctorController::class, 'index'])->name('admin.doctor')->middleware("restricted_route");
  Route::get('admin/doctor/ChangePassword/{id}', [DoctorController::class, 'showChangePasswordForm'])->name('admin.doctor.ChangePassword')->middleware("restricted_route");
  Route::post('admin/doctor/changePassword', [DoctorController::class, 'changePassword'])->name('admin.doctor.changePassword');
  //Customer Type Module
  Route::get('admin/customertype',[CustomerTypeController::class, 'index'])->name('admin.customertype')->middleware("restricted_route");
  Route::get('admin/customertype/create',[CustomerTypeController::class, 'create'])->name('admin.customertype.create')->middleware("restricted_route");
  Route::post('admin/customertype/store',[CustomerTypeController::class, 'store'])->name('admin.customertype.store');
  Route::get('admin/customertype/edit/{id}',[CustomerTypeController::class, 'edit'])->name('admin.customertype.edit')->middleware("restricted_route");
  Route::get('admin/customertype/ajax_listing',[CustomerTypeController::class, 'ajax_listing'])->name('admin.customertype.ajaxListing')->middleware("restricted_route");

  //Casepan Module

  Route::get('admin/casepan',[CasepanController::class, 'index'])->name('admin.casepan')->middleware("restricted_route");
  Route::post('admin/casepan/store_model_casepan',[CasepanController::class, 'store_model_casepan'])->name('admin.casepan.store_model_casepan');
  Route::get('admin/casepan/create',[CasepanController::class, 'create'])->name('admin.casepan.create')->middleware("restricted_route");
  Route::post('admin/casepan/store',[CasepanController::class, 'store'])->name('admin.casepan.store');
  Route::get('admin/casepan/edit/{id}',[CasepanController::class, 'edit'])->name('admin.casepan.edit')->middleware("restricted_route");
  Route::get('admin/casepan/ajax_listing',[CasepanController::class, 'ajax_listing'])->name('admin.casepan.ajaxListing')->middleware("restricted_route");
  
  //Casepan Number Module

  Route::post('admin/casepannumber/store',[CasepanNumberController::class, 'store'])->name('admin.casepannumber.store');

  Route::get('admin/casepannumber/edit/{iCaspanId}/{id}',[CasepanNumberController::class, 'edit'])->name('admin.casepannumber.edit')->middleware("restricted_route");
  Route::get('admin/casepannumber/ajax_listing',[CasepanNumberController::class, 'ajax_listing'])->name('admin.casepannumber.ajaxListing')->middleware("restricted_route");
  Route::get('admin/casepannumber/create/{iCasepanId?}',[CasepanNumberController::class, 'create'])->name('admin.casepannumber.create')->middleware("restricted_route");
  Route::get('admin/casepannumber/{iCasepanId?}',[CasepanNumberController::class, 'index'])->name('admin.casepannumber')->middleware("restricted_route");

  //Call Module

  Route::get('admin/call',[CallController::class, 'index'])->name('admin.call')->middleware("restricted_route");
  Route::get('admin/call/create',[CallController::class, 'create'])->name('admin.call.create')->middleware("restricted_route");
  Route::post('admin/call/store',[CallController::class, 'store'])->name('admin.call.store');
  Route::get('admin/call/edit/{id}',[CallController::class, 'edit'])->name('admin.call.edit')->middleware("restricted_route");
  Route::get('admin/call/ajax_listing',[CallController::class, 'ajax_listing'])->name('admin.call.ajaxListing')->middleware("restricted_route");

  //Grade Module

  Route::get('admin/grade/create',[GradeController::class, 'create'])->name('admin.grade.create')->middleware("restricted_route");
  Route::post('admin/grade/store',[GradeController::class, 'store'])->name('admin.grade.store');
  Route::get('admin/grade/edit/{id}',[GradeController::class, 'edit'])->name('admin.grade.edit')->middleware("restricted_route");
  Route::get('admin/grade/ajax_listing',[GradeController::class, 'ajax_listing'])->name('admin.grade.ajaxListing')->middleware("restricted_route");
  Route::get('admin/grade/{iCasepanId?}',[GradeController::class, 'index'])->name('admin.grade')->middleware("restricted_route");

  //Schedule Module
  Route::get('admin/schedule',[ScheduleController::class, 'index'])->name('admin.schedule')->middleware("restricted_route");
  Route::get('admin/schedule/create',[ScheduleController::class, 'create'])->name('admin.schedule.create')->middleware("restricted_route");
  Route::post('admin/schedule/store',[ScheduleController::class, 'store'])->name('admin.schedule.store');
  Route::post('admin/schedule/store_new',[ScheduleController::class, 'store_new'])->name('admin.schedule.store_new');
  Route::get('admin/schedule/edit/{iScheduleId}',[ScheduleController::class, 'edit'])->name('admin.schedule.edit')->middleware("restricted_route");
  Route::get('admin/schedule/ajax_listing',[ScheduleController::class, 'ajax_listing'])->name('admin.schedule.ajaxListing')->middleware("restricted_route");

  // category Addon
  Route::get('admin/addoncategory',[AddoncategoryController::class, 'index'])->name('admin.addoncategory')->middleware("restricted_route");
  Route::get('admin/addoncategory/create',[AddoncategoryController::class, 'create'])->name('admin.addoncategory.create')->middleware("restricted_route");
  Route::post('admin/addoncategory/store',[AddoncategoryController::class, 'store'])->name('admin.addoncategory.store');
  Route::get('admin/addoncategory/edit/{iaddoncategoryId}',[AddoncategoryController::class, 'edit'])->name('admin.addoncategory.edit')->middleware("restricted_route");
  Route::get('admin/addoncategory/ajax_listing',[AddoncategoryController::class, 'ajax_listing'])->name('admin.addoncategory.ajaxListing')->middleware("restricted_route");
  Route::get('admin/addoncategory/lower_ajax_listing',[AddoncategoryController::class, 'lower_ajax_listing'])->name('admin.addoncategory.LowerajaxListing')->middleware("restricted_route");
  
  // Sub Category Addon
  Route::get('admin/subaddoncategory',[SubaddoncategoryController::class, 'index'])->name('admin.subaddoncategory')->middleware("restricted_route");
  Route::get('admin/subaddoncategory/create',[SubaddoncategoryController::class, 'create'])->name('admin.subaddoncategory.create')->middleware("restricted_route");
  Route::post('admin/subaddoncategory/store',[SubaddoncategoryController::class, 'store'])->name('admin.subaddoncategory.store');
  Route::get('admin/subaddoncategory/edit/{iSubAddCategoryId}',[SubaddoncategoryController::class, 'edit'])->name('admin.subaddoncategory.edit')->middleware("restricted_route");
  Route::get('admin/subaddoncategory/ajax_listing',[SubaddoncategoryController::class, 'ajax_listing'])->name('admin.subaddoncategory.ajaxListing')->middleware("restricted_route");
  Route::get('admin/subaddoncategory/lower_ajax_listing',[SubaddoncategoryController::class, 'lower_ajax_listing'])->name('admin.subaddoncategory.LowerajaxListing')->middleware("restricted_route");
  Route::post('admin/subaddoncategory/fetch_category',[SubaddoncategoryController::class, 'fetch_category'])->name('admin.subaddoncategory.fetch_category');

  //Calendar Module
  Route::get('admin/calendar',[CalendarController::class, 'index'])->name('admin.calendar')->middleware("restricted_route");
  Route::get('admin/calendar/create',[CalendarController::class, 'create'])->name('admin.calendar.create')->middleware("restricted_route");
  Route::post('admin/calendar/store',[CalendarController::class, 'store'])->name('admin.calendar.store')->middleware("restricted_route");
  Route::get('admin/calendar/getofficedata',[CalendarController::class, 'getofficedata'])->name('admin.calendar.getofficedata');
  Route::get('admin/calendar/edit/{id}',[CalendarController::class, 'edit'])->name('admin.calendar.edit')->middleware("restricted_route");
  Route::get('admin/calendar/ajax_listing',[CalendarController::class, 'ajax_listing'])->name('admin.calendar.ajaxListing')->middleware("restricted_route");
  

  //Department Module

  Route::get('admin/department',[DepartmentController::class, 'index'])->name('admin.department');
  Route::get('admin/department/create',[DepartmentController::class, 'create'])->name('admin.department.create');
  Route::post('admin/department/store',[DepartmentController::class, 'store'])->name('admin.department.store');
  Route::get('admin/department/edit/{id}',[DepartmentController::class, 'edit'])->name('admin.department.edit');
  Route::get('admin/department/ajax_listing',[DepartmentController::class, 'ajax_listing'])->name('admin.department.ajaxListing');

  //Staff Module

  Route::get('admin/staff',[StaffController::class, 'index'])->name('admin.staff')->middleware("restricted_route");
  Route::get('admin/staff/create',[StaffController::class, 'create'])->name('admin.staff.create')->middleware("restricted_route");
  Route::post('admin/staff/store',[StaffController::class, 'store'])->name('admin.staff.store');
  Route::get('admin/staff/edit/{id}',[StaffController::class, 'edit'])->name('admin.staff.edit')->middleware("restricted_route");
  Route::get('admin/staff/ajax_listing',[StaffController::class, 'ajax_listing'])->name('admin.staff.ajaxListing')->middleware("restricted_route");
  Route::get('admin/staff/generatepassword',[StaffController::class, 'generatepassword'])->name('admin.staff.generatepassword');
  Route::get('admin/staff/ChangePassword/{id}', [StaffController::class, 'showChangePasswordForm'])->name('admin.staff.ChangePassword')->middleware("restricted_route");
  Route::post('admin/staff/changePassword', [StaffController::class, 'changePassword'])->name('admin.staff.changePassword');
  Route::post('admin/staff/CheckExistEmail', [StaffController::class, 'CheckExistEmail'])->name('admin.staff.CheckExistEmail');
  Route::post('admin/lab_admin/user-data',[StaffController::class, 'ShowUSerDetail'])->name('admin.staff.ShowUSerDetail');
  //User Module Lab
  Route::get('admin/user/lab/create',[UserLabController::class, 'create'])->name('admin.user_lab.create')->middleware("restricted_route");
  Route::post('admin/user/lab/store',[UserLabController::class, 'store'])->name('admin.user_lab.store');
  Route::get('admin/user/lab/generatepassword',[UserLabController::class, 'generatepassword'])->name('admin.user_lab.generatepassword');
  Route::get('admin/user/lab/edit/{id}',[UserLabController::class, 'edit'])->name('admin.user_lab.edit')->middleware("restricted_route");
  Route::get('admin/user/lab/ajax_listing',[UserLabController::class, 'ajax_listing'])->name('admin.user_lab.ajaxListing')->middleware("restricted_route");
  Route::post('admin/user/lab/fetch_office',[UserLabController::class, 'fetch_office'])->name('admin.user_lab.fetch_office');
  Route::post('admin/user/lab/fetch_doctor',[UserLabController::class, 'fetch_doctor'])->name('admin.user_lab.fetch_doctor');

  Route::get('admin/lab/userlist/{iCustomerId}/{iOfficeId}/{iDoctorId}',[UserLabController::class, 'userlist'])->name('admin.userlist')->middleware("restricted_route");
  Route::get('admin/lab/user/usercreate/{iCustomerId}/{iOfficeId}/{iDoctorId}',[UserLabController::class, 'usercreate'])->name('admin.user_lab.user-create');
  Route::post('admin/lab/user/userstore',[UserLabController::class, 'userstore'])->name('admin.user_lab.userstore');
  Route::get('admin/lab/user/useredit/{iCustomerId}/{iOfficeId}/{iDoctorId}/{id}',[UserLabController::class, 'useredit'])->name('admin.user_lab.useredit');
  Route::get('admin/lab/user/user_ajax_listing',[UserLabController::class, 'user_ajax_listing'])->name('admin.user_lab.userajaxListing');
  Route::get('admin/lab/user/{iCustomerId?}',[UserLabController::class, 'index'])->name('admin.user_lab')->middleware("restricted_route");
  Route::get('admin/lab/user/officeuser/{iOfficeId?}',[UserLabController::class, 'officeuser'])->name('admin.officeuser')->middleware("restricted_route");
  Route::get('admin/lab/user/ChangePassword/{id}', [UserLabController::class, 'showChangePasswordForm'])->name('admin.user_lab.ChangePassword')->middleware("restricted_route");
  Route::post('admin/lab/user/changePassword', [UserLabController::class, 'changePassword'])->name('admin.user_lab.changePassword');

  //User Module Office
  Route::get('admin/user/office/create',[UserController::class, 'create'])->name('admin.user.create')->middleware("restricted_route");
  Route::post('admin/user/office/store',[UserController::class, 'store'])->name('admin.user.store');
  Route::get('admin/user/office/generatepassword',[UserController::class, 'generatepassword'])->name('admin.user.generatepassword');
  Route::get('admin/user/office/edit/{id}',[UserController::class, 'edit'])->name('admin.user.edit')->middleware("restricted_route");
  Route::get('admin/user/office/ajax_listing',[UserController::class, 'ajax_listing'])->name('admin.user.ajaxListing')->middleware("restricted_route");
  Route::post('admin/user/office/fetch_office',[UserController::class, 'fetch_office'])->name('admin.user.fetch_office');
  Route::post('admin/user/office/fetch_doctor',[UserController::class, 'fetch_doctor'])->name('admin.user.fetch_doctor');
  
  Route::get('admin/office/userlist/{iCustomerId}/{iOfficeId}/{iDoctorId}',[UserController::class, 'userlist'])->name('admin.userlist')->middleware("restricted_route");
  Route::get('admin/office/user/usercreate/{iCustomerId}/{iOfficeId}/{iDoctorId}',[UserController::class, 'usercreate'])->name('admin.user.user-create');
  Route::post('admin/office/user/userstore',[UserController::class, 'userstore'])->name('admin.user.userstore');
  Route::get('admin/office/user/useredit/{iCustomerId}/{iOfficeId}/{iDoctorId}/{id}',[UserController::class, 'useredit'])->name('admin.user.useredit');
  Route::get('admin/office/user/user_ajax_listing',[UserController::class, 'user_ajax_listing'])->name('admin.user.userajaxListing');
  Route::get('admin/office/user/{iCustomerId?}',[UserController::class, 'index'])->name('admin.user')->middleware("restricted_route");
  Route::get('admin/office/user/officeuser/{iOfficeId?}',[UserController::class, 'officeuser'])->name('admin.officeuser')->middleware("restricted_route");
  Route::get('admin/office/user/ChangePassword/{id}', [UserController::class, 'showChangePasswordForm'])->name('admin.user.ChangePassword')->middleware("restricted_route");
  Route::post('admin/office/user/changePassword', [UserController::class, 'changePassword'])->name('admin.user.changePassword');
  //Office Module
  Route::get('admin/office/create',[OfficeController::class, 'create'])->name('admin.office.create')->middleware("restricted_route");
  Route::get('admin/office/create-staff/{iCustomerId}',[OfficeController::class, 'create_staff'])->name('admin.office.create_staff');
  Route::post('admin/office/store',[OfficeController::class, 'store'])->name('admin.office.store');
  Route::get('admin/office/generatepassword',[OfficeController::class, 'generatepassword'])->name('admin.office.generatepassword');
  Route::get('admin/office/edit/{id}',[OfficeController::class, 'edit'])->name('admin.office.edit')->middleware("restricted_route");
  Route::get('admin/office/ajax_listing',[OfficeController::class, 'ajax_listing'])->name('admin.office.ajaxListing')->middleware("restricted_route");
  
  Route::get('admin/officelist/{iCustomerId}',[OfficeController::class, 'officelist'])->name('admin.officelist');
  Route::get('admin/office/officecreate/{iCustomerId}',[OfficeController::class, 'officecreate'])->name('admin.office.office-create');
  Route::post('admin/office/officestore',[OfficeController::class, 'officestore'])->name('admin.office.officestore');
  Route::get('admin/office/officeedit/{iCustomerId}/{id}',[OfficeController::class, 'officeedit'])->name('admin.office.officeedit');
  Route::get('admin/office/office_ajax_listing',[OfficeController::class, 'office_ajax_listing'])->name('admin.office.OfficeajaxListing');
  Route::get('admin/office/{iCustomerId?}',[OfficeController::class, 'index'])->name('admin.office')->middleware("restricted_route");
  Route::get('admin/office/ChangePassword/{id}', [OfficeController::class, 'showChangePasswordForm'])->name('admin.office.ChangePassword')->middleware("restricted_route");
  Route::post('admin/office/changePassword', [OfficeController::class, 'changePassword'])->name('admin.office.changePassword');
  //Stage Module
  Route::get('admin/stage',[StageController::class, 'index'])->name('admin.stage')->middleware("restricted_route");
  Route::get('admin/stage/create',[StageController::class, 'create'])->name('admin.stage.create')->middleware("restricted_route");
  Route::post('admin/stage/store',[StageController::class, 'store'])->name('admin.stage.store');
  Route::get('admin/stage/edit/{istageId}',[StageController::class, 'edit'])->name('admin.stage.edit')->middleware("restricted_route");
  Route::get('admin/stage/ajax_listing',[StageController::class, 'ajax_listing'])->name('admin.stage.ajaxListing')->middleware("restricted_route");

  //Stage Impression
  Route::get('admin/stage/impression',[StageController::class, 'impression'])->name('admin.impression')->middleware("restricted_route");
  Route::get('admin/stage/impression/impression_create',[StageController::class, 'impression_create'])->name('admin.stage.impression_create')->middleware("restricted_route");
  Route::post('admin/stage/impression/impression_store',[StageController::class, 'impression_store'])->name('admin.stage.impression_store');
  Route::get('admin/stage/impression/edit/{istageId}',[StageController::class, 'impression_edit'])->name('admin.stage.impression_edit')->middleware("restricted_route");
  Route::get('admin/stage/impression/ajax_listing',[StageController::class, 'impression_ajax_listing'])->name('admin.stage.ImpressionajaxListing')->middleware("restricted_route");

  //Stage Toothshade
  Route::get('admin/stage/toothshades',[StageController::class, 'toothshades'])->name('admin.toothshades')->middleware("restricted_route");
  Route::get('admin/stage/toothshades/toothshades_create',[StageController::class, 'toothshades_create'])->name('admin.stage.toothshades_create')->middleware("restricted_route");
  Route::post('admin/stage/toothshades/toothshades_store',[StageController::class, 'toothshades_store'])->name('admin.stage.toothshades_store');
  Route::post('admin/stage/toothshades/toothshades_import',[StageController::class, 'toothshades_import'])->name('admin.stage.toothshades_import');
  Route::get('admin/stage/toothshades/edit/{istageId}',[StageController::class, 'toothshades_edit'])->name('admin.stage.toothshades_edit')->middleware("restricted_route");
  Route::get('admin/stage/toothshades/ajax_listing',[StageController::class, 'toothshades_ajax_listing'])->name('admin.stage.ToothshadesajaxListing')->middleware("restricted_route");

  //Stage Toothshade Brand
  Route::get('admin/stage/toothshadesbrand',[StageController::class, 'toothshadesbrand'])->name('admin.toothshadesbrand')->middleware("restricted_route");
  Route::get('admin/stage/toothshadesbrand/toothshadesbrand_create',[StageController::class, 'toothshadesbrand_create'])->name('admin.stage.toothshadesbrand_create')->middleware("restricted_route");
  Route::post('admin/stage/toothshadesbrand/toothshadesbrand_store',[StageController::class, 'toothshadesbrand_store'])->name('admin.stage.toothshadesbrand_store');
  Route::get('admin/stage/toothshadesbrand/edit/{istageId}',[StageController::class, 'toothshadesbrand_edit'])->name('admin.stage.toothshadesbrand_edit')->middleware("restricted_route");
  Route::get('admin/stage/toothshadesbrand/ajax_listing',[StageController::class, 'toothshadesbrand_ajax_listing'])->name('admin.stage.toothshadesbrandajaxListing')->middleware("restricted_route");
  Route::post('admin/stage/toothshadesbrand/toothbrand_store',[StageController::class, 'toothbrand_store'])->name('admin.stage.toothbrand_store');
  Route::get('admin/stage/toothshades/delete', [StageController::class, 'delete'])->name('admin.stage.ToothshadesDelete')->middleware("restricted_route");

  //Stage GumShade
  Route::get('admin/stage/gumshades',[StageController::class, 'gumshades'])->name('admin.gumshades')->middleware("restricted_route");
  Route::get('admin/stage/gumshades/gumshades_create',[StageController::class, 'gumshades_create'])->name('admin.stage.gumshades_create')->middleware("restricted_route");
  Route::post('admin/stage/gumshades/gumshades_store',[StageController::class, 'gumshades_store'])->name('admin.stage.gumshades_store');
  Route::get('admin/stage/gumshades/edit/{istageId}',[StageController::class, 'gumshades_edit'])->name('admin.stage.gumshades_edit')->middleware("restricted_route");
  Route::get('admin/stage/gumshades/ajax_listing',[StageController::class, 'gumshades_ajax_listing'])->name('admin.stage.GumshadesajaxListing')->middleware("restricted_route");
  Route::post('admin/stage/gumshades/gumbrand_store',[StageController::class, 'gumbrand_store'])->name('admin.stage.gumbrand_store');
  Route::get('admin/stage/gumshades/delete', [StageController::class, 'gumshades_delete'])->name('admin.stage.GumshadesDelete')->middleware("restricted_route");

  //Stage GumShade Brand  
  Route::get('admin/stage/gumbrand',[StageController::class, 'gumbrand'])->name('admin.gumbrand')->middleware("restricted_route");
  Route::get('admin/stage/gumbrand/gumbrand_create',[StageController::class, 'gumbrand_create'])->name('admin.stage.gumbrand_create');
  Route::post('admin/stage/gumbrand/gumbrand_store',[StageController::class, 'gumbrand_store'])->name('admin.stage.gumbrand_store');
  Route::get('admin/stage/gumbrand/edit/{istageId}',[StageController::class, 'gumbrand_edit'])->name('admin.stage.gumbrand_edit');
  Route::get('admin/stage/gumbrand/ajax_listing',[StageController::class, 'gumbrand_ajax_listing'])->name('admin.stage.gumbrandajaxListing');
  Route::post('admin/stage/gumbrand/toothbrand_store',[StageController::class, 'toothbrand_store'])->name('admin.stage.toothbrand_store');
  Route::get('admin/stage/gum/delete', [StageController::class, 'delete'])->name('admin.stage.GumDelete');

  //Stage Notes  
  Route::get('admin/stage/notes',[StageController::class, 'notes'])->name('admin.notes')->middleware("restricted_route");
  Route::get('admin/stage/notes/notes_create',[StageController::class, 'notes_create'])->name('admin.stage.notes_create');
  Route::post('admin/stage/notes/notes_store',[StageController::class, 'notes_store'])->name('admin.stage.notes_store');
  Route::get('admin/stage/notes/edit/{istageId}',[StageController::class, 'notes_edit'])->name('admin.stage.notes_edit');
  Route::get('admin/stage/notes/ajax_listing',[StageController::class, 'notes_ajax_listing'])->name('admin.stage.notesajaxListing');

  //Stage Rush Dates  
  Route::get('admin/stage/rushdate',[StageController::class, 'rushdate'])->name('admin.rushdate')->middleware("restricted_route");
  Route::get('admin/stage/rushdate/rushdate_create',[StageController::class, 'rushdate_create'])->name('admin.stage.rushdate_create');
  Route::post('admin/stage/rushdate/rushdate_store',[StageController::class, 'rushdate_store'])->name('admin.stage.rushdate_store');
  Route::get('admin/stage/rushdate/edit/{istageId}',[StageController::class, 'rushdate_edit'])->name('admin.stage.rushdate_edit');
  Route::get('admin/stage/rushdate/rushdate_ajax_listing',[StageController::class, 'rushdate_ajax_listing'])->name('admin.stage.rushdateajaxListing');
  Route::post('admin/stage/fetch_categoryproduct',[StageController::class, 'fetch_categoryproduct'])->name('admin.stage.fetch_categoryproduct');
  Route::post('admin/stage/fetch_productstage',[StageController::class, 'fetch_productstage'])->name('admin.stage.fetch_productstage');


//Stage Rush Dates Count  
  Route::get('admin/stage/rushdate/rushdate_count_ajax_listing',[StageController::class, 'rushdate_count_ajax_listing'])->name('admin.stage.rushdatecountajaxListing');
  Route::post('admin/stage/rushdate/uppercount_create/',[StageController::class, 'uppercount_create'])->name('admin.stage.uppercountcreate');
  Route::post('admin/stage/rushdate/uppercount_store',[StageController::class, 'uppercount_store'])->name('admin.stage.uppercount_store');
  Route::get('admin/stage/rushdate/uppercount_edit/{id}',[StageController::class, 'uppercount_edit'])->name('admin.stage.uppercount_edit');
  
   
  //category Module
  Route::get('admin/category',[CategoryController::class, 'index'])->name('admin.category')->middleware("restricted_route");
  Route::get('admin/category/create',[CategoryController::class, 'create'])->name('admin.category.create')->middleware("restricted_route");
  Route::post('admin/category/store',[CategoryController::class, 'store'])->name('admin.category.store');
  Route::get('admin/category/edit/{icategoryId}',[CategoryController::class, 'edit'])->name('admin.category.edit')->middleware("restricted_route");
  Route::get('admin/category/ajax_listing',[CategoryController::class, 'ajax_listing'])->name('admin.category.ajaxListing');
  Route::get('admin/category/lower_ajax_listing',[CategoryController::class, 'lower_ajax_listing'])->name('admin.category.LowerajaxListing');

   //Category Product Module
   
  Route::get('admin/categoryproduct',[CategoryProductController::class, 'index'])->name('admin.categoryproduct')->middleware("restricted_route");
  Route::get('admin/categoryproduct/create',[CategoryProductController::class, 'create'])->name('admin.categoryproduct.create')->middleware("restricted_route");
  Route::get('admin/categoryproduct/lower_create',[CategoryProductController::class, 'lower_create'])->name('admin.categoryproduct.lower_create');
  Route::post('admin/categoryproduct/store',[CategoryProductController::class, 'store'])->name('admin.categoryproduct.store');
  Route::get('admin/categoryproduct/edit/{iCategoryProductId}',[CategoryProductController::class, 'edit'])->name('admin.categoryproduct.edit');
  Route::get('admin/categoryproduct/lower_edit/{iCategoryProductId}',[CategoryProductController::class, 'lower_edit'])->name('admin.categoryproduct.lower_edit');
  Route::get('admin/categoryproduct/ajax_listing',[CategoryProductController::class, 'ajax_listing'])->name('admin.categoryproduct.ajaxListing');
  Route::get('admin/categoryproduct/lower_ajax_listing',[CategoryProductController::class, 'lower_ajax_listing'])->name('admin.categoryproduct.LowerajaxListing');
  Route::post('admin/categoryproduct/fetch_category',[CategoryProductController::class, 'fetch_category'])->name('admin.categoryproduct.fetch_category');
  Route::get('admin/categoryproductselect/{iCategoryProductId}',[CategoryProductController::class, 'categoryproductselect'])->name('admin.categoryproductselect');
  

  // Office User lab start
  Route::get('admin/categoryproductoffice',[CategoryProductOfficeController::class, 'index'])->name('admin.officelab');
  Route::get('admin/categoryproductoffice/ajax_listing',[CategoryProductOfficeController::class, 'ajax_listing'])->name('admin.categoryproductoffice.ajaxListing');
  Route::get('admin/categoryproductoffice_list/{iCategoryProductId}',[CategoryProductOfficeController::class, 'categoryproductoffice_list'])->name('admin.categoryproductoffice_list');
  Route::get('admin/categoryproductoffice/ajax_listing_upper',[CategoryProductOfficeController::class, 'ajax_listing_upper'])->name('admin.categoryproductoffice.ajaxListingUpper');
  Route::get('admin/categoryproductoffice/lower_ajax_listing',[CategoryProductOfficeController::class, 'lower_ajax_listing'])->name('admin.categoryproductoffice.LowerajaxListing');
  Route::get('admin/categoryproductoffice/upper_ajax_listing',[CategoryProductOfficeController::class, 'upper_ajax_listing'])->name('admin.categoryproductoffice.UpperajaxListing');

  Route::get('admin/productstageoffice_list/{iCategoryProductId}',[ProductStageOfficeController::class, 'productstageoffice_list'])->name('admin.productstageoffice_list');
  Route::get('admin/productstageoffice/ajax_listing_upper',[ProductStageOfficeController::class, 'ajax_listing_upper'])->name('admin.productstageoffice.ajaxListingUpper');
  Route::get('admin/productstageoffice/lower_ajax_listing',[ProductStageOfficeController::class, 'lower_ajax_listing'])->name('admin.productstageoffice.LowerajaxListing');
  Route::get('admin/productstageoffice/upper_ajax_listing',[ProductStageOfficeController::class, 'upper_ajax_listing'])->name('admin.productstageoffice.UpperajaxListing');

  
  
  
  // Office User lab End

  //Category Product Module
  Route::get('admin/productstage',[ProductStageController::class, 'index'])->name('admin.productstage')->middleware("restricted_route");
  Route::get('admin/productstage/create',[ProductStageController::class, 'create'])->name('admin.productstage.create')->middleware("restricted_route");
  Route::get('admin/productstage/lower_create',[ProductStageController::class, 'lower_create'])->name('admin.productstage.lower_create');
  Route::post('admin/productstage/store',[ProductStageController::class, 'store'])->name('admin.productstage.store');
  Route::get('admin/productstage/edit/{iProductstageId}',[ProductStageController::class, 'edit'])->name('admin.productstage.edit')->middleware("restricted_route");
  Route::get('admin/productstage/lower_edit/{iProductstageId}',[ProductStageController::class, 'lower_edit'])->name('admin.productstage.lower_edit');
  Route::get('admin/productstage/ajax_listing',[ProductStageController::class, 'ajax_listing'])->name('admin.productstage.ajaxListing')->middleware("restricted_route");
  Route::get('admin/productstage/lower_ajax_listing',[ProductStageController::class, 'lower_ajax_listing'])->name('admin.productstage.LowerajaxListing');
  Route::post('admin/productstage/fetch_category',[ProductStageController::class, 'fetch_category'])->name('admin.productstage.fetch_category');
  Route::post('admin/productstage/fetch_product',[ProductStageController::class, 'fetch_product'])->name('admin.productstage.fetch_product');
  Route::post('admin/productstage/getproduct',[ProductStageController::class, 'getproduct'])->name('admin.productstage.getproduct');
  Route::post('admin/productstage/getproduct_edit',[ProductStageController::class, 'getproduct_edit'])->name('admin.productstage.getproduct_edit');
  Route::get('admin/productstageselect/{iCategoryProductId}',[ProductStageController::class, 'productstageselect'])->name('admin.productstageselect');
  Route::post('admin/productstage/fetch_grade_price',[ProductStageController::class, 'fetch_grade_price'])->name('admin.productstage.fetch_grade_price');
  Route::post('admin/productstage/fetch_office_grade_price',[ProductStageController::class, 'fetch_office_grade_price'])->name('admin.productstage.fetch_office_grade_price');

  //Banner Module
  Route::get('admin/banner',[BannerController::class, 'index'])->name('admin.banner');
  Route::get('admin/banner/create',[BannerController::class, 'create'])->name('admin.banner.create');
  Route::post('admin/banner/store',[BannerController::class, 'store'])->name('admin.banner.store');
  Route::get('admin/banner/edit/{iBannerId}',[BannerController::class, 'edit'])->name('admin.banner.edit');
  Route::get('admin/banner/ajax_listing',[BannerController::class, 'ajax_listing'])->name('admin.banner.ajaxListing');
    
  //Email Module
  Route::get('admin/email', [EmailController::class, 'index'])->name('admin.email');
  // Route::match(['get', 'post'],'admin/email/ajax',[EmailController::class, 'ajax_listing'])->name('ajax');
  Route::get('admin/email/create', [EmailController::class, 'create'])->name('admin.email.create');
  Route::post('admin/email/store', [EmailController::class, 'store'])->name('admin.email.store');
  Route::get('admin/email/edit/{id}', [EmailController::class, 'edit'])->name('admin.email.edit');
  Route::get('admin/delete-email/{id}', [EmailController::class, 'destroy'])->name('admin.email.delete');
  Route::post('admin/change-status-emails', [EmailController::class, 'changeStatus'])->name('admin.email.changeStatus');
  Route::delete('admin/emails-multi-delete', [EmailController::class, 'multiDelete'])->name('admin.email.multiDelete');
  Route::get('admin/email/ajax_listing',[EmailController::class, 'ajax_listing'])->name('admin.email.ajaxListing');

  //Notification Module
  Route::get('admin/notification',[NotificationController::class, 'index'])->name('admin.notification');
  Route::get('admin/notification/create',[NotificationController::class, 'create'])->name('admin.notification.create');
  Route::post('admin/notification/store',[NotificationController::class, 'store'])->name('admin.notification.store');
  Route::get('admin/notification/edit/{id}',[NotificationController::class, 'edit'])->name('admin.notification.edit');
  Route::get('admin/notification/ajax_listing',[NotificationController::class, 'ajax_listing'])->name('admin.notification.ajaxListing');

  //Setting Module
  Route::get('admin/setting',[SettingController::class, 'index'])->name('admin.setting');
  Route::post('admin/setting/store',[SettingController::class, 'store'])->name('admin.setting.store');
  Route::get('admin/setting/{eConfigType}',[SettingController::class, 'edit'])->name('admin.setting.edit');


  //Role Module
  Route::get('admin/role',[RoleController::class, 'index'])->name('admin.role');
  Route::get('admin/role/create', [RoleController::class, 'create'])->name('admin.role.create');
  Route::post('admin/role/store', [RoleController::class, 'store'])->name('admin.role.store');
  Route::get('admin/role/edit/{id}', [RoleController::class, 'edit'])->name('admin.role.edit');
  Route::get('admin/role/ajax_listing', [RoleController::class, 'ajax_listing'])->name('admin.role.ajax_listing');
  
  //Module Master 
  Route::get('admin/module-master',[ModuleMasterController::class, 'index'])->name('admin.moduleMaster')->middleware("restricted_route");
  Route::get('admin/module-master/create', [ModuleMasterController::class, 'create'])->name('admin.moduleMaster.create')->middleware("restricted_route");
  Route::post('admin/module-master/store', [ModuleMasterController::class, 'store'])->name('admin.moduleMaster.store');
  Route::get('admin/module-master/edit/{id}', [ModuleMasterController::class, 'edit'])->name('admin.moduleMaster.edit')->middleware("restricted_route");
  Route::get('admin/module-master/ajax_listing', [ModuleMasterController::class, 'ajax_listing'])->name('admin.moduleMaster.ajax_listing')->middleware("restricted_route");
  
  //Module Permission
  Route::get('admin/module-permission/create/', [ModulePermissionController::class, 'create'])->name('admin.modulePermission.create')->middleware("restricted_route");
  Route::post('admin/module-permission/store', [ModulePermissionController::class, 'store'])->name('admin.modulePermission.store');
  Route::get('admin/module-permission/edit/{id}', [ModulePermissionController::class, 'edit'])->name('admin.modulePermission.edit')->middleware("restricted_route");
  
  //Module Permission
  Route::get('admin/labcase/', [LabcaseController::class, 'index'])->name('admin.labcase')->middleware("restricted_route");
  Route::get('admin/labcase/office_admin_listing', [LabcaseController::class, 'office_admin_listing'])->name('admin.labcase.office_admin_listing')->middleware("restricted_route");
  Route::get('admin/labcase/add', [LabcaseController::class, 'create'])->name('admin.labcase.add');
  Route::post('admin/labcase/store', [LabcaseController::class, 'store'])->name('admin.labcase.store');
  Route::post('admin/labcase/UpdateSlipData', [LabcaseController::class, 'UpdateSlipData'])->name('admin.labcase.UpdateSlipData');
  Route::get('admin/labcase/edit/{id}', [LabcaseController::class, 'edit'])->name('admin.labcase.edit');
  Route::get('admin/labcase/getcategory', [LabcaseController::class, 'getcategory'])->name('admin.labcase.getcategory');
  Route::get('admin/labcase/gettoothbrand', [LabcaseController::class, 'gettoothbrand'])->name('admin.labcase.gettoothbrand');
  Route::get('admin/labcase/getgumbrand', [LabcaseController::class, 'getgumbrand'])->name('admin.labcase.getgumbrand');
  Route::post('admin/labcase/get_impressions', [LabcaseController::class, 'get_impressions'])->name('admin.labcase.get_impressions');
  Route::post('admin/labcase/getdoc', [LabcaseController::class, 'getdoc'])->name('admin.labcase.getdoc');
  Route::post('admin/labcase/getdoc_2', [LabcaseController::class, 'getdoc_2'])->name('admin.labcase.getdoc_2');
  Route::post('admin/labcase/getofficedoc', [LabcaseController::class, 'getofficedoc'])->name('admin.labcase.getofficedoc');
  Route::get('admin/labcase/ajax_listing', [LabcaseController::class, 'ajax_listing'])->name('admin.labcase.ajax_listing');
  Route::get('admin/labcase/ajax_listing_office', [LabcaseController::class, 'ajax_listing_office'])->name('admin.labcase.ajax_listing_office');
  Route::post('admin/labcase/getproduct', [LabcaseController::class, 'getproduct'])->name('admin.labcase.getproduct');
  Route::post('admin/labcase/getgrade', [LabcaseController::class, 'getgrade'])->name('admin.labcase.getgrade');
  Route::post('admin/labcase/getstage', [LabcaseController::class, 'getstage'])->name('admin.labcase.getstage');
  Route::post('admin/labcase/getstage_2', [LabcaseController::class, 'getstage_2'])->name('admin.labcase.getstage_2');
  Route::post('admin/labcase/getOldStage', [LabcaseController::class, 'getOldStage'])->name('admin.labcase.getOldStage');
  Route::post('admin/labcase/getshadetype', [LabcaseController::class, 'getshadetype'])->name('admin.labcase.getshadetype');
  Route::post('admin/labcase/gettoothshades', [LabcaseController::class, 'gettoothshades'])->name('admin.labcase.gettoothshades');
  Route::post('admin/labcase/getgumshades', [LabcaseController::class, 'getgumshades'])->name('admin.labcase.getgumshades');
  Route::post('admin/labcase/getcategoryaddons', [LabcaseController::class, 'getcategoryaddons'])->name('admin.labcase.getcategoryaddons');
  Route::get('admin/labcase/pickupdays', [LabcaseController::class, 'pickupdays'])->name('admin.labcase.pickupdays');
  Route::get('admin/labcase/old_pickupdays', [LabcaseController::class, 'old_pickupdays'])->name('admin.labcase.old_pickupdays');
  Route::get('admin/labcase/rushdate', [LabcaseController::class, 'rushdate'])->name('admin.labcase.rushdate');
  Route::post('admin/labcase/old_rushdate', [LabcaseController::class, 'old_rushdate'])->name('admin.labcase.old_rushdate');
  Route::get('admin/labcase/getcasepan', [LabcaseController::class, 'getcasepan'])->name('admin.labcase.getcasepan');
  Route::get('admin/labcase/slip_view/{id}', [LabcaseController::class, 'slip_view'])->name('admin.labcase.slip_view');
  Route::post('admin/labcase/get_addons_category/', [LabcaseController::class, 'get_addons_category'])->name('admin.labcase.get_addons_category');
  Route::post('admin/labcase/dropzoneStore/', [LabcaseController::class, 'gallery_action'])->name('admin.labcase.dropzoneStore');
  Route::get('admin/labcase/dropzone/{id}', [ProductController::class, 'dropzoneStore'])->name('admin.labcase.dropzone');

  Route::post('admin/labcase/attacement', [LabcaseController::class, 'attacement'])->name('admin.labcase.attacement');
  Route::post('admin/labcase/attacement_newstage', [LabcaseController::class, 'attacement_newstage'])->name('admin.labcase.attacement_newstage');
  Route::post('admin/labcase/view_attacement', [LabcaseController::class, 'view_attacement'])->name('admin.labcase.view_attacement');
  Route::get('admin/labcase/delete_gallery', [LabcaseController::class, 'delete_gallery'])->name('admin.labcase.delete_gallery');
  Route::get('admin/labcase/delete_gallery_slip', [LabcaseController::class, 'delete_gallery_slip'])->name('admin.labcase.delete_gallery_slip');

  Route::get('admin/labcase/virtual_slip/{iCaseId}/{iSlipId}', [LabcaseController::class, 'virtual_slip'])->name('admin.labcase.virtual_slip');
  Route::get('admin/labcase/edit_slip/{iCaseId}/{iSlipId}', [LabcaseController::class, 'edit_slip'])->name('admin.labcase.edit_slip');
  Route::get('admin/labcase/edit_slip_lab/{iCaseId}/{iSlipId}', [LabcaseController::class, 'edit_slip_lab'])->name('admin.labcase.edit_slip_lab');
  // Route::get('admin/labcase/paper_slip_pdf/{iCaseId}/{iSlipId}', [LabcaseController::class, 'paper_slip'])->name('admin.labcase.paper_slip_pdf');
  Route::post('admin/labcase/paper_slip', [LabcaseController::class, 'paper_slip'])->name('admin.labcase.paper_slip');
  Route::post('admin/labcase/view_driverhistory', [LabcaseController::class, 'view_driverhistory'])->name('admin.labcase.view_driverhistory');
  Route::post('admin/labcase/view_notes', [LabcaseController::class, 'view_notes'])->name('admin.labcase.view_notes');
  Route::post('admin/labcase/store_driverHistory', [LabcaseController::class, 'store_driverHistory'])->name('admin.labcase.store_driverHistory');

  Route::post('admin/labcase/ready_to_send', [LabcaseController::class, 'ready_to_send'])->name('admin.labcase.ready_to_send');
  Route::post('admin/labcase/undo_status', [LabcaseController::class, 'undo_status'])->name('admin.labcase.undo_status');
  // Route::post('admin/labcase/undo_status_billing/{iCaseId}/{iSlipId}', [LabcaseController::class, 'undo_status_billing'])->name('admin.labcase.undo_status_billing');
  Route::get('admin/labcase/merge_name', [LabcaseController::class, 'merge_name'])->name('admin.labcase.merge_name');
  Route::get('admin/labcase/ready_to_send_qr/{iSlipId}', [LabcaseController::class, 'ready_to_send_qr'])->name('admin.labcase.ready_to_send_qr');
  Route::get('admin/labcase/new_stage/{iCaseId}/{iSlipId}', [LabcaseController::class, 'new_stage'])->name('admin.labcase.new_stage');
  Route::get('admin/labcase/new_stage_lab/{iCaseId}/{iSlipId}', [LabcaseController::class, 'new_stage_lab'])->name('admin.labcase.new_stage_lab');
  Route::post('admin/labcase/get_new_stage_details/', [LabcaseController::class, 'get_new_stage_details'])->name('admin.labcase.get_new_stage_details');
  Route::post('admin/labcase/edit_stage_notes', [LabcaseController::class, 'edit_stage_notes'])->name('admin.labcase.edit_stage_notes');
  Route::get('admin/labcase/print_slip_pdf', [LabcaseController::class, 'print_slip_pdf'])->name('admin.labcase.print_slip_pdf');
  Route::post('admin/labcase/store_call_log', [LabcaseController::class, 'store_call_log'])->name('admin.labcase.store_call_log');
  Route::post('admin/labcase/view_call_log', [LabcaseController::class, 'view_call_log'])->name('admin.labcase.view_call_log');
  Route::post('admin/labcase/insert_call_log', [LabcaseController::class, 'insert_call_log'])->name('admin.labcase.insert_call_log');
  Route::post('admin/labcase/new_stage_add', [LabcaseController::class, 'new_stage_add'])->name('admin.labcase.new_stage_add');
  Route::post('admin/labcase/next_previous_exist', [LabcaseController::class, 'next_previous_exist'])->name('admin.labcase.next_previous_exist');
  Route::get('admin/labcase/edit_stage/{iCaseId}/{iSlipId}', [LabcaseController::class, 'edit_stage'])->name('admin.labcase.edit_stage');
  Route::get('admin/labcase/edit_stage_lab/{iCaseId}/{iSlipId}', [LabcaseController::class, 'edit_stage_lab'])->name('admin.labcase.edit_stage_lab');
  Route::post('admin/labcase/UpdateStageData', [LabcaseController::class, 'UpdateStageData'])->name('admin.labcase.UpdateStageData');

  Route::get('admin/labcase/testheic', [LabcaseController::class, 'testheic'])->name('admin.labcase.testheic');   
  Route::post('admin/labcase/update_billing', [LabcaseController::class, 'update_billing'])->name('admin.labcase.update_billing');   
  Route::post('admin/labcase/update_billing_ref', [LabcaseController::class, 'update_billing_ref'])->name('admin.labcase.update_billing_ref');   
  
  Route::get('admin/labcase/soft_delete_slip', [LabcaseController::class, 'soft_delete_slip'])->name('admin.labcase.soft_delete_slip');
  Route::get('admin/labcase/undo_delete_slip/{iCaseId}/{iSlipId}', [LabcaseController::class, 'undo_delete_slip'])->name('admin.labcase.undo_delete_slip');
  
  // Qr scan driver history start
  Route::get('admin/labcase/view-driverhistory-by-qr/{iCaseId}/{iSlipId}', [LabcaseController::class, 'view_driverhistory_by_qr'])->name('admin.labcase.view_driverhistory_by_qr');
  Route::get('admin/labcase/view-driverhistory-by-listing/{iCaseId}/{iSlipId}/{Type}', [LabcaseController::class, 'view_driverhistory_by_listing'])->name('admin.labcase.view_driverhistory_by_listing');
  Route::get('admin/labcase/view-driverhistory-all-lab-listing/{Location}', [LabcaseController::class, 'view_driverhistory_all_lab_listing'])->name('admin.labcase.view_driverhistory_all_lab_listing');
  
  Route::post('admin/labcase/store_multiple_driverHistory', [LabcaseController::class, 'store_multiple_driverHistory'])->name('admin.labcase.store_multiple_driverHistory');
  Route::post('admin/labcase/fetch_addons', [LabcaseController::class, 'fetch_addons'])->name('admin.labcase.fetch_addons');
  Route::post('admin/labcase/upload_user_image', [LabcaseController::class, 'UploadUserImage'])->name('admin.labcase.UploadUserImage');
 
  Route::post('admin/labcase/add_layouts', [LabcaseController::class, 'add_layouts'])->name('admin.labcase.add_layouts');
  // Qr scan driver history end
  Route::get('admin/labcase/addon-listing-modal/{iCaseId}/{iSlipId}/{iLabId}', [LabcaseController::class, 'addon_listing_modal'])->name('admin.labcase.addon_listing_modal');
  Route::post('admin/labcase/store-addons', [LabcaseController::class, 'store_addons'])->name('admin.labcase.store_addons');
  // Route::get('admin/labcase/check_permission', [AdminController::class, 'check_permission'])->name('admin.admin.check_permission');   

  // Billing Start
  Route::get('admin/billing',[BillingController::class, 'index'])->name('admin.billing')->middleware("restricted_route");
  Route::get('admin/billing/create',[BillingController::class, 'create'])->name('admin.billing.create')->middleware("restricted_route");
  Route::post('admin/billing/PrintBilling',[BillingController::class, 'PrintBilling'])->name('admin.billing.PrintBilling');
  Route::post('admin/billing/SentMailBillingInvoice',[BillingController::class, 'SentMailBillingInvoice'])->name('admin.billing.SentMailBillingInvoice');
  Route::post('admin/billing/PrintBillingInvoice',[BillingController::class, 'PrintBillingInvoice'])->name('admin.billing.PrintBillingInvoice');
  Route::get('admin/billing/PrintBillingSingle/{iBillingId}',[BillingController::class, 'PrintBillingSingle'])->name('admin.billing.PrintBillingSingle');
  Route::get('admin/billing/PrintBillingSingleInvoice/{iBillingId}',[BillingController::class, 'PrintBillingSingleInvoice'])->name('admin.billing.PrintBillingSingleInvoice');
  Route::get('admin/billing/PrintBillingSingleSlipInvoice/{iSlipId}',[BillingController::class, 'PrintBillingSingleSlipInvoice'])->name('admin.billing.PrintBillingSingleSlipInvoice');
  Route::get('admin/billing/ajax_listing',[BillingController::class, 'ajax_listing'])->name('admin.billing.ajaxListing')->middleware("restricted_route");
  Route::get('admin/billing/{iBillingId}',[BillingController::class, 'edit'])->name('admin.billing.edit')->middleware("restricted_route");
  Route::post('admin/billing/store',[BillingController::class, 'store'])->name('admin.billing.store')->middleware("restricted_route");
  Route::post('admin/billing/driverSlipPrint',[BillingController::class, 'driverSlipPrint'])->name('admin.billing.driverSlipPrint');
  Route::post('admin/billing/fetchCustomerEmail',[BillingController::class, 'fetchCustomerEmail'])->name('admin.billing.fetchCustomerEmail');
  // Billing End
  
  //system_email
  Route::get('admin/system_email/listing',[SystemEmailController::class, 'index'])->name('admin.systemEmail.listing')->middleware("restricted_route");
  Route::get('admin/system_email/add',[SystemEmailController::class, 'add'])->name('admin.systemEmail.add')->middleware("restricted_route");
  Route::post('admin/system_email/store',[SystemEmailController::class, 'store'])->name('admin.systemEmail.store');
  Route::post('admin/system_email/ajax_listing',[SystemEmailController::class, 'ajax_listing'])->name('admin.systemEmail.ajaxListing')->middleware("restricted_route");
  Route::get('admin/system_email/edit/{iSystemEmailId}',[SystemEmailController::class, 'edit'])->name('admin.systemEmail.edit')->middleware("restricted_route");

  //setting
  Route::get('admin/setting/listing',[SettingController::class, 'index'])->name('admin.setting.listing')->middleware("restricted_route");
  Route::post('admin/setting/store',[SettingController::class, 'store'])->name('admin.setting.store');
  Route::get('admin/setting/{eConfigType}',[SettingController::class, 'edit'])->name('admin.setting.edit')->middleware("restricted_route");


  // page setting
});