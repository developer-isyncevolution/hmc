!function()
{
    "use strict";
    $.Toast=function(e,t,n,r)
    {
        function o()
        {
            $.Toast.remove(s)
        }
        var i=
        {
            appendTo:"body",
            stack:!1,
            position_class:"toast-bottom-right",
            fullscreen:!1,
            width:250,
            spacing:20,
            timeout:4e3,
            has_close_btn:!0,
            has_icon:!0,
            sticky:!1,
            border_radius:6,
            has_progress:!1,
            rtl:!1
        },
        s=null,
        a=$.extend(!0,{},i,r),
        l=a.spacing,
        u={position:"body"==a.appendTo?"fixed":"absolute","min-width":a.width,display:"none","border-radius":a.border_radius,"z-index":99999};if(s=$('<div class="toast-item-wrapper '+n+" "+a.position_class+'"></div>'),$('<p class="toast-title">'+e+"</p>").appendTo(s),$('<p class="toast-message">'+t+"</p>").appendTo(s),a.fullscreen&&s.addClass("fullscreen"),a.rtl&&s.addClass("rtl"),a.has_close_btn&&($('<span class="toast-close">&times;</span>').appendTo(s),a.rtl?u["padding-left"]=20:u["padding-right"]=20),a.has_icon&&($('<i class="toast-icon toast-icon-'+n+'"></i>').appendTo(s),a.rtl?u["padding-right"]=50:u["padding-left"]=50),a.has_progress&&a.timeout>0&&$('<div class="toast-progress"></div>').appendTo(s),a.sticky)switch(a.spacing=0,l=0,a.position_class){case"toast-top-left":u.top=0,u.left=0;break;case"toast-top-right":u.top=0,u.left=0;break;case"toast-top-center":u.top=0,u.left=u.right=0,u.width="100%";break;case"toast-bottom-left":u.bottom=0,u.left=0;break;case"toast-bottom-right":u.bottom=0,u.right=0;break;case"toast-bottom-center":u.bottom=0,u.left=u.right=0,u.width="100%"}return a.stack&&(-1!==a.position_class.indexOf("toast-top")?$(a.appendTo).find(".toast-item-wrapper").each(function(){u.top=parseInt($(this).css("top"))+this.offsetHeight+l}):-1!==a.position_class.indexOf("toast-bottom")&&$(a.appendTo).find(".toast-item-wrapper").each(function(){u.bottom=parseInt($(this).css("bottom"))+this.offsetHeight+l})),s.css(u),s.appendTo(a.appendTo),s.fadeIn?s.fadeIn():$alert.css({display:"block",opacity:1}),a.timeout>0&&(setTimeout(o,a.timeout),a.has_progress&&$(".toast-progress",s).animate({width:"100%"},a.timeout)),$(".toast-close",s).click(o),s},$.Toast.remove=function(e){e.fadeOut?e.fadeOut(function(){return e.remove()}):e.remove()}}();