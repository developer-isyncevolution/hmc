<!DOCTYPE html>

<html lang="en">

<head>
	<base href="">
	<title>Estraha</title>
	<meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
	<meta property="og:url" content="https://keenthemes.com/metronic" />
	<meta property="og:site_name" content="Keenthemes | Metronic" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.0/css/toastr.css" rel="stylesheet" />
	
	<!-- JS -->
	<!--end::Fonts-->
	@include('layouts.admin.css')
	@include('layouts.admin.js')
	<!-- inc css -->
	<script src="{{asset('admin/assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('admin/assets/js/adminlte.js')}}"></script>
</head>

<body id="kt_body" data-kt-aside-minimize="on" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed on_off_left" data-id="on" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
	
	<div class="d-flex flex-column flex-root">
		<div class="page flex-row flex-column-fluid">
			@include('layouts.admin.left')
			<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
				@include('layouts.admin.header')
				@yield('content')
				@include('layouts.admin.footer')
			</div>
		</div>
	</div>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.0/js/toastr.js"></script>
	<script src="{{asset('admin/assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('admin/assets/js/adminlte.js')}}"></script>
	@include('layouts.admin.js')
	@yield('custom-js')
	<script>
		$(function(){
        $.each(document.images, function(){
                var this_image = this;
                var src = $(this_image).attr('src') || '' ;
                if(!src.length > 0){
                    //this_image.src = options.loading; // show loading
                    var lsrc = $(this_image).attr('lsrc') || '' ;
                    if(lsrc.length > 0){
                        var img = new Image();
                        img.src = lsrc;
                        $(img).load(function() {
                            this_image.src = this.src;
                        });
                    }
                }
            });
    });
        $(document).ready(function() {
            toastr.options.timeOut = 4000;
            @if(Session::has('success'))
                toastr.success('{{ Session::get('success') }}');
            @endif
        });
    </script>
</body>
</html>