function notification_success(e) 
{ 
    $.Toast(e, "", "success", 
    { 
        has_icon: !0, 
        has_close_btn: !0, 
        stack: !1, 
        fullscreen: !0, 
        timeout: 4000, 
        sticky: !1, 
        has_progress: !0, 
        rtl: !1, 
        showHideTransition: "slide" 
    }) 
} 
function notification_error(e) 
{ 

    $.Toast(e, "", "error", 
    { 
        has_icon: !0, 
        has_close_btn: !0, 
        stack: !1, 
        fullscreen: !0, 
        timeout: 4000, 
        sticky: !1, 
        has_progress: !0, 
        rtl: !1, 
        showHideTransition: "slide"
     })
} 


 // Auto fill username and password blank
 $(document).ready(function() {
    if($('#vEmail').val() =='')
    {
        $('#vEmail').attr('readonly', true);
    }
    if($('#vPassword').val() =='')
    {
        $('#vPassword').attr('readonly', true);
    }
    setTimeout(function() {
        $('#vEmail').attr('readonly', false);
        $('#vPassword').attr('readonly', false);
    }, 1000);
});