<?php

namespace App\Libraries;
use App\Models\admin\category\Category;
use App\Models\admin\admin\Admin;
use App\Models\admin\modulePermission\ModulePermission;
use Session;
use Redirect;
use DB;
use Illuminate\Support\Facades\Route;
use Mail;
use App\Models\admin\setting\Setting;
use App\Models\admin\banner\Banner;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Log;
use Throwable;
use Symfony\Component\ErrorHandler\ErrorRenderer\HtmlErrorRenderer;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use App\Models\admin\labOffice\LabOffice;
use App\Models\admin\customer\Customer;

/**
 * 
 */
class General
{

	public function __construct()
    { 

    }

    public static function authentication($iAdminId="")
    {
        
        if(empty($iAdminId))
        {
            Redirect::to('/admin/login')->send();
        }
    }
 
    static function admin_info()
    {
        $admin = Session::get('data');
        if(!empty($admin))
        {
            return $admin;
        }
        else
        {
            return false;
        }
        
    }

    static function get_customer_logo(){
        $adminData = General::admin_info();

        if ($adminData['iDepartmentId'] != 1) {
            $SQL = DB::table("customer");
            $SQL->where("iCustomerId", $adminData['iCustomerId']);
            $result = $SQL->get()->first();

            if (isset($result->vImage) && !empty($result->vImage)) {
                return $result->vImage;
            }else{
                return "LabslipLogo";
            }
        }else{
            return "LabslipLogo";
        }


    }
    static function get_user_logo(){
        $adminData = General::admin_info();
        // dd($adminData);
        $iCustomerId = $adminData['iCustomerId'];
        if (!empty($iCustomerId)) 
        {
            // dd($adminData['customerType']);
                if($adminData['customerType'] =='Lab Admin')
                {
                    $SQL = DB::table("lab_admin");
                }
                else if($adminData['customerType'] =='Office Admin')
                {
                    $SQL = DB::table("office_admin");
                }
                else if($adminData['customerType'] =='Doctor')
                {
                    $SQL = DB::table("doctor");
                }
                elseif($adminData['customerType'] =='Office User' || $adminData['customerType'] =='Lab User')
                {
                    $SQL = DB::table("user");
                }
                else
                {
                    $SQL = DB::table("staff");
                }
              
                $SQL->where("vEmail", $adminData['vEmail']);
                $result = $SQL->get()->first();
                // dd($adminData['customerType']);
                if (isset($result->vImage) && !empty($result->vImage) && ($adminData['customerType'] =='Lab Admin' || $adminData['customerType'] =='Office Admin' || $adminData['customerType'] =='User')) 
                {
                    return asset('uploads/lab_office/'.$result->vImage);
                }
                elseif(isset($result->vImage) && !empty($result->vImage) && ($adminData['customerType'] =='Doctor'))
                {
                    return asset('uploads/doctor/'.$result->vImage);
                }
                elseif(isset($result->vImage) && !empty($result->vImage) && ($adminData['customerType'] =='Office User' || $adminData['customerType'] =='Lab User'))
                {
                    return asset('uploads/lab_office/'.$result->vImage);
                }
                elseif(isset($result->vImage) && !empty($result->vImage) )
                {
                    return asset('uploads/staff/'.$result->vImage);
                }
                else{
                    return "LabslipLogo";
                }
            
        }
        else if($adminData['customerType'] =='Super Admin')
        {
            $SQL = DB::table("admin");
            $SQL->where("email", $adminData['vEmail']);
            $result = $SQL->get()->first();
            if(isset($result->vImage) && !empty($result->vImage) && $adminData['customerType'] =='Super Admin') {
                return asset('uploads/admin/'.$result->vImage);
            }else{
                return "LabslipLogo";
            }
        }
        
        else{
            return "LabslipLogo";
        }


    }

    static function get_customer_info($iCustomerId){
        $SQL = DB::table("customer");
        $SQL->select("vHost","vEmailUserName","vEmailPassword","vEmailPort","vEmailEncryption","vFromEmailAddress");
        $SQL->where("iCustomerId", $iCustomerId);
        $result = $SQL->get()->first();

        $error = "false";

        foreach ($variable as $key => $value) {
            if (empty($value)) {
                $error = "true";
            }
        }

        if ($error == "false") {
            return $result;
        }else{
            return "false";
        }

    }

    static function get_module_permission($iDepartmentId, $vControllerName)
    {
        // dd($iDepartmentId);
        $SQL = DB::table("module_master");
        $SQL->join('module_permission','module_master.iModuleId','module_permission.iModuleId');
        $SQL->where("module_permission.iDepartmentId", $iDepartmentId);
        $SQL->where("module_master.vControllerName", $vControllerName);
        $result = $SQL->get()->first();
        return $result;
    }

    static function get_module_permission_by_name($iDepartmentId, $vModuleName)
    {
        // dd($iDepartmentId);
        $SQL = DB::table("module_master");
        $SQL->join('module_permission','module_master.iModuleId','module_permission.iModuleId');
        $SQL->where("module_permission.iDepartmentId", $iDepartmentId);
        $SQL->where("module_master.vModuleName", $vModuleName);
        $result = $SQL->get()->first();
        return $result;
    }
    static function get_slip_permission($iDepartmentId,$access)
    {
        $SQL = DB::table("module_master");
        $SQL->join('module_permission','module_master.iModuleId','module_permission.iModuleId');
        $SQL->where("module_permission.iDepartmentId", $iDepartmentId);
        $SQL->where("module_master.vModuleName", 'slip');
        $result = $SQL->get($access)->first();
        return $result;
    }

    static function check_permission($controllername='',$methodName='',$moduleName='')
    {
       
            if(empty($methodName))
            {
                $route = Route::currentRouteAction();
                $methodName = substr($route, strrpos($route, '@' )+1);
                if(isset($methodName) && ($methodName =='index' || $methodName =='toothshades' ||$methodName =='doctorlist' || $methodName =='userlist' || $methodName =='own_listing' || $methodName =='impression' || $methodName =='toothshadesbrand' || $methodName =='gumshades' || $methodName =='rushdate' || $methodName =='rushdate' || $methodName =='notes'))
                {
                    $methodName = 'eRead';
                }
                elseif(isset($methodName) && ($methodName =='create' || $methodName =='impression_create' || $methodName =='toothshades_create' || $methodName =='toothshadesbrand_create' || $methodName =='gumshades_create'))
                {
                    $methodName = 'eCreate';
                }
                elseif(isset($methodName) && ($methodName =='edit' || $methodName =='impression_edit' || $methodName =='toothshades_edit' || $methodName =='toothshadesbrand_edit' || $methodName =='gumshades_edit' || $methodName =='generatepassword'))
                {
                    $methodName = 'eEdit';
                }
                elseif(isset($methodName) && ($methodName =='delete' || $methodName =='gumshades_delete'))
                {
                    $methodName = 'eDelete';
                }
                elseif(isset($methodName) && $methodName =='store')
                {
                    $methodName = 'eCreate';
                }
            }
           
            $routeArray = app('request')->route()->getAction();
            $controllerAction = class_basename($routeArray['controller']);
            list($controller, $action) = explode('@', $controllerAction);
            if(empty($controllername))
            {
                $controllername =  $controller;
            }

            $adminData = General::admin_info();
          
            if(!empty($moduleName))
            {
                $getPermission = General::get_module_permission_by_name($adminData['iDepartmentId'], $moduleName);
            }
            else
            {
                $getPermission = General::get_module_permission($adminData['iDepartmentId'], $controllername);
            }
            // dd($getPermission);
            if(isset($getPermission) && !empty($getPermission))
            {
                if(($methodName == 'eRead') && $getPermission->eRead =='Yes')
                {
                    return true;
                }
                elseif($methodName == 'eEdit' && $getPermission->eEdit =='Yes')
                {
                    return true;
                }
                elseif($methodName == 'eCreate' && $getPermission->eCreate =='Yes')
                {
                    return true;
                }
                elseif($methodName == 'eDelete' && $getPermission->eDelete =='Yes')
                {
                    return true;
                }
                elseif($methodName == 'eReadSlip' && $getPermission->eReadSlip =='Yes')
                {
                    return true;
                }
                elseif($methodName =='ajax_listing' || $methodName =='lower_ajax_listing' || $methodName =='impression_ajax_listing' || $methodName =='toothshades_ajax_listing' || $methodName =='toothshadesbrand_ajax_listing' || $methodName =='gumshades_ajax_listing')
                {
                    $action = $request->query('action');
                    if(isset($action) && $action == 'delete' && $getPermission->eDelete =='No')
                    {
                        return redirect()->back()->withError('Access Denied');
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    // return redirect()->back()->withError('Access Denied');
                    return false;
                }
            }
            else
            {
                return false;
            }   

            if($controllername == 'LabcaseController')
            {
                return true;
            }
        
    }
    static function check_permission_slip($access='')
    {
        $adminData = General::admin_info();

        $getPermission = General::get_slip_permission($adminData['iDepartmentId'], $access);
        return $getPermission->$access;
        
    }

    static function send($criteria = array(), $data = array(),$files = array())
    {
        $email_info = Setting::get_setting('Email'); 
            if(isset($data['vHost']) && !empty($data['vHost']))
            {
                $mailConfig = [
                    'transport' => 'smtp',
                    'host' => $data['vHost'],
                    'port' => $data['vEmailPort'],
                    'encryption' => $data['vEmailEncryption'],
                    'username' => $data['vEmailUserName'],
                    'password' => $data['vEmailPassword'],
                    'timeout' => null ];
            }
            else
            {
               
                    $mailConfig = [
                    'transport' => 'smtp',
                    'host' => $email_info['SMTP_HOST']['vValue'],
                    'port' => $email_info['SMTP_PORT']['vValue'],
                    'encryption' => $email_info['EMAIL_PROTOCOL']['vValue'],
                    'username' => $email_info['SMTP_USERNAME']['vValue'],
                    'password' => $email_info['SMTP_PASS']['vValue'],
                    'timeout' => null ];
            }
      
        config(['mail.mailers.smtp' => $mailConfig]);
        try {
            // dd($data);
            if(isset($data['vHost']) && !empty($data['vHost']))
            {
                $url = 'admin.email.email_template_billing';
            }
            else
            {
                $url = 'admin.email.email_template'; 
            }
            Mail::send(['html' => $url], $data, function ($message) use ($data,$files) {
                if(isset($data['vHost']) && !empty($data['vHost']) && !empty($data['vOfficeName']))
                {
                    $from = $data['vOfficeName']; 
                }
                else
                {
                    $from = 'Labslips Online'; 
                }
                $message->to($data["to"], $from)
                ->subject($data["subject"]);
                $message->from($data["from"], $from);
                if(isset($data['ccEmails']) && !empty($data['ccEmails']))
                {
                    $message->cc($data['ccEmails']);
                }
                if(isset($data['bccEmails']) && !empty($data['bccEmails']))
                {
                    $message->bcc($data['bccEmails']);
                }
                if(isset($files) && !empty($files))
                {
                    foreach ($files as $file){
                        $message->attach($file);
                    }
                }
             
            });
        } catch (\Exception $e) {
            return true;
            // return redirect()->back()->withError($e->getMessage());
        }
           
    }

    static function setting_info($eConfigType)
    {
        $setting_info = Setting::get_setting($eConfigType);
        return $setting_info;
    }

    static function get_banner_data($iCustomerId)
    {
        $labOffice = LabOffice::get_active_lab_by_officeId($iCustomerId);
        $labs = array();
        foreach ($labOffice as $key => $value) {
            $labs[] = Customer::get_by_id($value->iLabId);
        }
        $data['labs'] = $labs;
        $customer_array = array();
        if(count($data['labs'])>0)
        {
            foreach ($data['labs'] as $key_lab => $value_lab) {
                $customer_array[]= $value_lab->iCustomerId;
            }
        }
        $banner_info = array();
        if(count($customer_array)>0)
        {
            $banner_info = Banner::get_banner_data($customer_array);
            return $banner_info;
        }
        else
        { 
             return $banner_info;
        }
    }
		
}

?>