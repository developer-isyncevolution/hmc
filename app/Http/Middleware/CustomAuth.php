<?php

namespace App\Http\Middleware;

use Session;

class CustomAuth extends Middleware
{
    public function Authentication(){
        $admin = Session::get('data');
        $iAdminId = $admin['user_id'];
        if(empty($iAdminId))
        {
            Redirect::to('/admin/login')->send();
        }
    }
}
