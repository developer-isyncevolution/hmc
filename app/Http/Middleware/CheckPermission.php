<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Libraries\General;
use Illuminate\Support\Facades\Route;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $route = Route::currentRouteAction();
        $methodName = substr($route, strrpos($route, '@' )+1);
      
        if(isset($methodName) && ($methodName =='index' || $methodName =='toothshades' ||$methodName =='doctorlist' || $methodName =='userlist' || $methodName =='own_listing' || $methodName =='impression' || $methodName =='toothshadesbrand' || $methodName =='gumshades' || $methodName =='gumbrand' || $methodName =='rushdate' || $methodName =='rushdate' || $methodName =='notes' || $methodName =='officeuser' || $methodName =='eReadSlip' || $methodName == 'office_admin_listing'))
        {
            $methodName = 'eRead';
        }
        elseif(isset($methodName) && ($methodName =='create' || $methodName =='impression_create' || $methodName =='toothshades_create' || $methodName =='toothshadesbrand_create' || $methodName =='gumshades_create' || $methodName =='add'))
        {
            $methodName = 'eCreate';
        }
        elseif(isset($methodName) && ($methodName =='edit' || $methodName =='impression_edit' || $methodName =='toothshades_edit' || $methodName =='toothshadesbrand_edit' || $methodName =='gumshades_edit' || $methodName =='generatepassword' || $methodName =='showChangePasswordForm'))
        {
            $methodName = 'eEdit';
        }
        elseif(isset($methodName) && ($methodName =='delete' || $methodName =='gumshades_delete'))
        {
            $methodName = 'eDelete';
        }
        elseif(isset($methodName) && $methodName =='store')
        {
            $methodName = 'eCreate';
        }
        
     
        
        $routeArray = app('request')->route()->getAction();
        $controllerAction = class_basename($routeArray['controller']);
        list($controller, $action) = explode('@', $controllerAction);
        $controllername =  $controller;

        if(isset($methodName) && $methodName =='eRead' && $controllername == 'LabcaseController')
        {
            $methodName = 'eReadSlip';
        }
        $adminData = General::admin_info();
        $getPermission = General::get_module_permission($adminData['iDepartmentId'], $controllername);
        if(isset($getPermission) && !empty($getPermission))
        {
            if($methodName == 'eRead' && $getPermission->eRead =='Yes')
            {
                return $next($request);
            }
            elseif($methodName == 'eEdit' && $getPermission->eEdit =='Yes')
            {
                return $next($request);
            }
            elseif($methodName == 'eCreate' && $getPermission->eCreate =='Yes')
            {
                return $next($request);
            }
            elseif($methodName == 'eDelete' && $getPermission->eDelete =='Yes')
            {
                return $next($request);
            }
            elseif($methodName == 'eReadSlip' && $getPermission->eReadSlip =='Yes')
            {
                return $next($request);
            }
            elseif($methodName =='ajax_listing' || $methodName =='lower_ajax_listing' || $methodName =='impression_ajax_listing' || $methodName =='toothshades_ajax_listing' || $methodName =='toothshadesbrand_ajax_listing' || $methodName =='gumshades_ajax_listing' || $methodName =='ajax_listing_office')
            {
                $action = $request->query('action');
                if(isset($action) && $action == 'delete' && $getPermission->eDelete =='No')
                {
                    return redirect()->back()->withError('Access Denied');
                    return false;
                }
                else
                {
                    return $next($request);
                }
            }
            else
            {
                return redirect()->back()->withError('Access Denied');
                return false;
            }
        }
        else
        {
            return $next($request);
        }   
    }
}