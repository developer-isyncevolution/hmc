<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

// namespace App\Http\Controllers\admin\local;
// use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class loginAuth
{
    public function handle(Request $request, Closure $next)
    {   
        $admin = Session::get('data');
        if(isset($admin) && !empty($admin))
        {
            return $next($request);
        }
        else
        {
            return redirect()->route('admin.login');
        }
        
    }
}
