<?php

namespace App\Http\Controllers\admin\banner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\banner\Banner;
use App\Libraries\Paginator;
use App\Libraries\General;


class BannerController extends Controller
{
    public function index()
    {    
        return view('admin.banner.listing');
    }
    
    public function ajax_listing(Request $request)
    {

        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iBannerId";
            $order = "DESC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iBannerId']    = $request->id;

            Banner::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Banner_ID = (explode(",",$request->id));

            foreach ($Banner_ID as $key => $value) {
                $where                 = array();
                $where['iBannerId']    = $value;

                Banner::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        $banner_data = Banner::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($banner_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = Banner::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        
        return view('admin.banner.ajax_listing')->with($data);   
    }

    public function create()
    {
        return view('admin.banner.create');
    }

    public function store(Request $request)
    {

        $iBannerId = $request->id;

        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/banner'); 
            $request->vImage->move($path, $imageName);
        }

        $data['vTitle']        = $request->vTitle;
        $data['tDescription']  = $request->tDescription;
        $data['vLocation']     = $request->vLocation;
        $data['eStatus']       = $request->eStatus;
        $data['iCustomerId']   = General::admin_info()['iCustomerId'];
        $data['dtStartDate']       = date('Y-m-d',strtotime($request->dtStartDate));
        $data['dtEndDate']       = date('Y-m-d',strtotime($request->dtEndDate));;

        if($iBannerId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if ($request->hasFile('vImage')) {   
            $data['vImage']    = $imageName;
        }

        if(!empty($iBannerId)){
            $where                      = array();
            $where['iBannerId']         = $iBannerId;

            $Banner_id = new Banner();
            $Banner_id->update($where, $data);

            return redirect()->route('admin.banner')->withSuccess('Banner updated successfully.');
        }
        else{
            $Banner_id = Banner::add($data);

            return redirect()->route('admin.banner')->withSuccess('Banner created successfully.');
        }
    }

    public function edit($iBannerId)
    {
        $data['banners'] = Banner::get_by_id($iBannerId);

        return view('admin.banner.create')->with($data);
    }

    public function destroy($id)
    {
        $banner = Banner::find($id);
        $banner->delete();

        return redirect()->route('admin.banner')->withSuccess('Banner deleted successfully.');
    }

    public function multiDelete(Request $request)
    {
        $id = $request->id;

        Banner::whereIn('id',explode(",",$id))->delete();
    }
}
