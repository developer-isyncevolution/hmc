<?php

namespace App\Http\Controllers\admin\customer;

use App\Helper\GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\admin\customer\Customer;
use App\Models\admin\customertype\CustomerType;
use App\Models\admin\schedule\Schedule;
use App\Models\admin\labOffice\LabOffice;
use App\Models\admin\state\State;
use Illuminate\Http\Request;
// use Intervention\Image\Facades\Image as Image;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use App\Libraries\Paginator;
use Validator;
use Hash;
use Auth;
use Session;
use Image;
use App\Libraries\General;
class CustomerController extends Controller
{
    public function index()
    {      
        // $data = General::get_customer_info(1); 

        $per_page = Session::forget('per_page_lan');
        $data['customerType'] = CustomerType::get_all_type();
        return view('admin.customer.listing',$data);
    }
    
    public function ajax_listing(Request $request)
    {
        $action = $request->action;
       
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iCustomerId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iCustomerId']    = $request->id;

            Customer::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Customer_ID = (explode(",",$request->id));

            foreach ($Customer_ID as $key => $value) {
                $where                 = array();
                $where['iCustomerId']    = $value;

                Customer::delete_by_id($where);
            }
        }
        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        if(isset($request->iCustomerTypeId) && !empty($request->iCustomerTypeId))
        {
            $criteria['iCustomerTypeId'] = $request->iCustomerTypeId;
        }
        else
        {
            $criteria['iCustomerTypeId'] = '';
        }
        // dd($criteria);
        
        $Admin_data = Customer::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

       
        
        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }
        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        // dd($paginator->itemsPerPage);
        if($request->limit_page !='')
        {
            $limit = $request->limit_page;
        }
        else
        {
            $limit = $paginator->itemsPerPage;
        }
        
        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $data['data'] = Customer::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        return view('admin.customer.ajax_listing')->with($data);   
    }

    public function create()
    {
        $criteria = array();
        $data['customertype'] = CustomerType::get_all_type($criteria);
        $iCountryId = "223";
        $data['states'] = State::get_by_iCountryId($iCountryId);
        $officeid = Customer::get_office_id();
        if(!empty($officeid->iCustomerId)) {
            $id = $officeid->iCustomerId+1;
        }else{
            
            $id = 1;
        }
        $data['id'] = "1".str_pad($id,3,"0",STR_PAD_LEFT);
        return view('admin.customer.create')->with($data);
    }

    public function store(Request $request)
    {
        $iCustomerId = $request->id;
        $image = $request->file('vImage');
        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/customer');
            $img            = Image::make($image->getRealPath()); 
            $img->resize(50, 50, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.'/'.$imageName);
            $request->vImage->move($path, $imageName);
            $data['vImage']    = $imageName;
        }
        
        // $data['vName']        = $request->vName;
        $data['iCustomerTypeId']= $request->iCustomerTypeId;
        $data['vEmail']       = $request->vEmail;
        $data['vNotificationEmail']       = $request->vNotificationEmail;
        $data['vMobile']      = $request->vMobile;
        $data['vCellulor']    = $request->vCellulor;
        $data['vTitle']       = $request->vTitle;
        $data['vWebsiteName'] = $request->vWebsiteName;
        $data['tDescription'] = $request->tDescription;
        $data['vOfficeName']  = $request->vOfficeName;
        $data['vAddress']     = $request->vAddress;
        $data['vCity']        = $request->vCity;
        $data['iStateId']     = $request->iStateId;
        $data['vZipCode']     = $request->vZipCode;
        $data['eStatus']      = $request->eStatus;
        $data['vOfficeCode']  = $request->vOfficeCode;
        $data['vFirstName']   = $request->vFirstName;
        $data['vLastName']    = $request->vLastName;


        $data['vHost']              = $request->vHost;
        $data['vEmailUserName']     = $request->vEmailUserName;
        $data['vEmailPassword']    = $request->vEmailPassword;
        $data['vEmailPort']         = $request->vEmailPort;
        $data['vEmailEncryption']   = $request->vEmailEncryption;
        $data['vFromEmailAddress']  = $request->vFromEmailAddress;


        if(empty($iCustomerId)){
            $data['vPassword']    = md5($request->vPassword);
          }
        if($iCustomerId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iCustomerId)){
            $where               = array();
            $where['iCustomerId']         = $iCustomerId;

            $Customer_ID = new Customer();
            $Customer_ID->update($where, $data);
            return redirect()->route('admin.customer')->withSuccess('Customer updated successfully.');
        }   
        else{
            $NewCustomerId = Customer::add($data);

            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));
            // Update schedule start
            $WeekData = array ('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
            $NotWeekData = array ('Sunday','Friday','Saturday');
            foreach($WeekData as $key=>$WeekDataVal)
            {
                $schedule_data['iCustomerId'] =  $NewCustomerId;
                $schedule_data['eMonth']         = 'All';
                $schedule_data['eWeek']          = $WeekDataVal;
                $schedule_data['eWeekMonth']     = 'All';
                if(in_array($WeekDataVal,$NotWeekData))
                {     
                    $schedule_data['eStatus']        = 'CLOSED';
                    $schedule_data['vTitle']         = 'Weekend';
                    $schedule_data['tiOpen']         = Null;
                    $schedule_data['tiClose']        = Null;
                }
                else
                {
                    $schedule_data['eStatus']        = 'OPEN';
                    $schedule_data['vTitle']         = 'Open';
                    $schedule_data['tiOpen']         = '10:00:00';
                    $schedule_data['tiClose']        = '16:00:00';
                }
                 Schedule::add($schedule_data);
            }
            // Update schedule end
            if($request->iCustomerTypeId == '1')
            {
                return redirect()->route('admin.lab_admin.create_staff', ['iCustomerId' => $NewCustomerId]);
            }
            elseif($request->iCustomerTypeId == '6')
            {
                return redirect()->route('admin.office_admin.create_staff', ['iCustomerId' => $NewCustomerId]);
            }
            // return redirect()->route('admin.customer')->withSuccess('Customer created successfully.');
        }
    }

    public function edit($iCustomerId)
    {
        $data['customers'] = Customer::get_by_id($iCustomerId);
        $criteria = array();
        $data['customertype'] = CustomerType::get_all_type($criteria);
        $iCountryId = "223";
        $data['states'] = State::get_by_iCountryId($iCountryId);
        $officeid = Customer::get_office_id();
        $id = $officeid->iCustomerId;
        $data['id'] = "1".str_pad($id,3,"0",STR_PAD_LEFT);
        return view('admin.customer.create')->with($data);
    }
    public function showChangePasswordForm($iCustomerId)
    {
        $data['customers'] = Customer::get_by_id($iCustomerId);
        return view('admin.customer.showChangePasswordForm')->with($data);
    }
    
    public function changePassword(Request $request)
    {
        $iCustomerId = $request->id;
        $where                     = array();
        $data['vPassword']        = md5($request->vPassword);
        $where['iCustomerId']     = $iCustomerId;

        $Customer_id = new Customer();
        $Customer_id->update($where, $data);
        
        return redirect()->back()->with("success","Password changed successfully !");

    }

    public function OfficeProfile()
    {
        $iDepartmentId = General::admin_info()['iDepartmentId'];
        $iCustomerId = General::admin_info()['iCustomerId'];


        if ($iDepartmentId == 3) {
            $iCountryId = "223";
            $data['states'] = State::get_by_iCountryId($iCountryId);
            $data['data'] = Customer::get_by_id($iCustomerId);
            $data['Labs'] = Customer::get_all_lab();
            $data['LabOffice'] = LabOffice::get_by_officeid($iCustomerId);
            
            $LabOffice_labids = array();
            foreach ($data['LabOffice'] as $key => $value) {
                $LabOffice_labids[] = $value->iLabId;
            }

            $data['LabOffice_labids'] = $LabOffice_labids;

            return view('admin.customer.officeProfile')->with($data);
        }else{
            return redirect()->route('admin.dashboard')->with('error','Page not found');
        }
    }

    public function LabProfile()
    {
        $iDepartmentId = General::admin_info()['iDepartmentId'];
        $iCustomerId = General::admin_info()['iCustomerId'];

        if ($iDepartmentId == 2) {
            $iCountryId = "223";
            $data['states'] = State::get_by_iCountryId($iCountryId);
            $data['data'] = Customer::get_by_id($iCustomerId);

            $data['Office_request'] = LabOffice::get_by_labid($iCustomerId);

            return view('admin.customer.labProfile')->with($data);
        }else{
            return redirect()->route('admin.dashboard')->with('error','Page not found');
        }
    }

    public function OfficeProfileAction(Request $request)
    {
        $iCustomerId = General::admin_info()['iCustomerId'];
        $image = $request->file('vImage');
        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/customer');
            $img            = Image::make($image->getRealPath()); 
            $img->resize(50, 50, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.'/'.$imageName);
            $request->vImage->move($path, $imageName);
            $data['vImage']    = $imageName;
        }
        
        $data['vName']        = $request->vName;
        $data['vEmail']       = $request->vEmail;
        $data['vNotificationEmail'] = $request->vNotificationEmail;
        $data['vMobile']      = $request->vMobile;
        $data['vCellulor']    = $request->vCellulor;
        $data['vTitle']       = $request->vTitle;
        $data['vWebsiteName'] = $request->vWebsiteName;
        $data['tDescription'] = $request->tDescription;
        $data['vOfficeName']  = $request->vOfficeName;
        $data['vAddress']     = $request->vAddress;
        $data['vCity']        = $request->vCity;
        $data['iStateId']     = $request->iStateId;
        $data['vZipCode']     = $request->vZipCode;
        $data['vOfficeCode']     = $request->vOfficeCode;
                
        if($iCustomerId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        $where               = array();
        $where['iCustomerId']         = $iCustomerId;
        $Customer_ID = new Customer();
        $Customer_ID->update($where, $data);

        if (isset($request->NewLab) && !empty($request->NewLab)) {

            $lab_request = $request->NewLab;

            foreach ($lab_request as $key => $value) {
                $lab_office_data = array();
                $lab_office_data['iOfficeId']   = $iCustomerId;
                $lab_office_data['iLabId']      = $value;
                labOffice::add($lab_office_data);
            }
        }

        return redirect()->route('admin.OfficeProfile')->withSuccess('Profile update successfully.');
        // dd($data);
    }

    public function LabProfileAction(Request $request)
    {
        $iCustomerId = General::admin_info()['iCustomerId'];
        $image = $request->file('vImage');
        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/customer');
            $img            = Image::make($image->getRealPath()); 
            $img->resize(50, 50, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.'/'.$imageName);
            $request->vImage->move($path, $imageName);
            $data['vImage']    = $imageName;
        }
        
        $data['vName']        = $request->vName;
        $data['vEmail']       = $request->vEmail;
        $data['vNotificationEmail'] = $request->vNotificationEmail;
        $data['vMobile']      = $request->vMobile;
        $data['vCellulor']    = $request->vCellulor;
        $data['vTitle']       = $request->vTitle;
        $data['vWebsiteName'] = $request->vWebsiteName;
        $data['tDescription'] = $request->tDescription;
        $data['vOfficeName']  = $request->vOfficeName;
        $data['vAddress']     = $request->vAddress;
        $data['vCity']        = $request->vCity;
        $data['iStateId']     = $request->iStateId;
        $data['vZipCode']     = $request->vZipCode;
        
        if($iCustomerId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if (isset($request->reject_list) && !empty($request->reject_list)) {
            foreach ($request->reject_list as $key => $value) {
                $LabOfficeData = array();
                $LabOfficeData['eStatus'] = "Inactive"; 

                $where = array();
                $where['iLabOfficeId']     = $value;

                LabOffice::update_data($where,$LabOfficeData);
            }
        }

        if (isset($request->approve_list) && !empty($request->approve_list)) {
            foreach ($request->approve_list as $key => $value) {
                $LabOfficeData = array();
                $LabOfficeData['eStatus'] = "Active"; 

                $where = array();
                $where['iLabOfficeId']     = $value;

                LabOffice::update_data($where,$LabOfficeData);
            }
        }

        $where               = array();
        $where['iCustomerId']         = $iCustomerId;
        $Customer_ID = new Customer();
        $Customer_ID->update($where, $data);
        return redirect()->route('admin.LabProfile')->withSuccess('Profile update successfully.');
        // dd($data);
    }
}
