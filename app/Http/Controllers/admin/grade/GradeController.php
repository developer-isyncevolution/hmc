<?php

namespace App\Http\Controllers\admin\grade;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\grade\Grade;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;

class GradeController extends Controller
{
    public function index()
    {
        return view('admin.grade.listing');
    }

    public function ajax_listing(Request $request)
    {

        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iGradeId']    = $request->id;

            Grade::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Grade_ID = (explode(",",$request->id));

            foreach ($Grade_ID as $key => $value) {
                $where                 = array();
                $where['iGradeId']    = $value;

                Grade::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $Grade_data = Grade::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Grade_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = Grade::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        
        return view('admin.grade.ajax_listing')->with($data);   
    }

    public function create()
    {
        return view('admin.grade.create');
    }

   public function store(Request $request)
    {
        $iGradeId = $request->id;

        $data['vQualityName'] = $request->vQualityName;
        $data['iSequence']    = $request->iSequence;
        $data['eStatus']      = $request->eStatus;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        if($iGradeId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iGradeId)){
            $where                      = array();
            $where['iGradeId']       = $iGradeId;

            $Grade_id = new Grade();
            $Grade_id->update($where, $data);

            return redirect()->route('admin.grade')->withSuccess('Grade updated successfully.');
        }
        else{
            $Grade_id = Grade::add($data);

            return redirect()->route('admin.grade')->withSuccess('Grade created successfully.');
        }
    }

    public function edit($iGradeId)
    {
        $data['grades'] = Grade::get_by_id($iGradeId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['grades']->iCustomerId) && $data['grades']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['grades']->iCustomerId)))
        {
            return redirect()->route('admin.grade')->withError('Invalid Input');
        }
        return view('admin.grade.create')->with($data);
    }

    public function destroy($id)
    {
        $Grade = Grade::find($id);
        $Grade->delete();

        return redirect()->route('admin.grade')->withSuccess('Grade deleted successfully.');
    }

    public function multiDelete(Request $request)
    {
        $id = $request->id;

        Grade::whereIn('id',explode(",",$id))->delete();
    }
}
