<?php

namespace App\Http\Controllers\admin\lab_admin;

use App\Http\Controllers\Controller;
use App\Models\admin\lab_admin\LabAdmin;
use App\Models\admin\customer\Customer;
use App\Models\admin\staff\Staff;
use Illuminate\Http\Request;
use App\Libraries\Paginator;
use Validator;
use Session;
use App\Libraries\General;
use Image;

class LabAdminController extends Controller
{
    public function index($iCustomerId = "")
    { 
        $per_page = Session::forget('per_page_lan');
        $iCustomerId = General::admin_info()['iCustomerId'];
        $data['lab_data'] = LabAdmin::get_all_data($iCustomerId); 
        $data['customer'] = Customer::get_all_customer(); 

        return view('admin.lab_admin.listing')->with($data);
    }
    
    public function ajax_listing(Request $request)
    {
        
        $action = $request->action;
       
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iLabAdminId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iLabAdminId']    = $request->id;

            LabAdmin::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Office_ID = (explode(",",$request->id));

            foreach ($Office_ID as $key => $value) {
                $where                 = array();
                $where['iLabAdminId']    = $value;

                LabAdmin::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        if(isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $criteria['iCustomerId'] = $request->iCustomerId;
        }
        else
        {
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        }
        
        $Admin_data = LabAdmin::get_all_data($criteria);
        

        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

      

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;
        
        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        // $criteria['iCustomerId'] = $iCustomerId;
   
        $data['data'] = LabAdmin::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        // dd($data);
        return view('admin.lab_admin.ajax_listing')->with($data);   
    }

    public function create()
    {
        $data['customer'] = Customer::get_all_customer();
        return view('admin.lab_admin.create',$data);
    }

    public function create_staff($iCustomerId='')
    {
        $data['customer'] = Customer::get_all_customer();
        $data['iCustomerId_New'] = $iCustomerId;
        return view('admin.lab_admin.create',$data);
    }
    
    public function store(Request $request){
        $iLabAdminId = $request->iLabAdminId;
        if(isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $iCustomerId = $request->iCustomerId;
        }
        else
        {
            $iCustomerId = General::admin_info()['iCustomerId'];;
        }
        $image = $request->file('vImage');
        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/lab_office');
            $img            = Image::make($image->getRealPath()); 
            $img->resize(50, 50, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.'/'.$imageName);
            $request->vImage->move($path, $imageName);
            $data['vImage']    = $imageName;
        }
        $data['iCustomerId']   = $iCustomerId;
        $data['vFirstName']      = $request->vFirstName;
        $data['vLastName']      = $request->vLastName;
        // $data['vName']      = $request->vFirstName.' '. $request->vLastName;
        $data['vEmail']    = $request->vEmail;
        $data['vMobile']    = $request->vMobile;
        $data['vCellulor']  = isset($request->vCellulor)?$request->vCellulor:'';
        $data['eStatus']       = $request->eStatus;
        $data['vPassword']    = md5($request->vPassword);

        if($iLabAdminId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        
        if(!empty($iLabAdminId)){
            $where               = array();
            $where['iLabAdminId']         = $iLabAdminId;

            $Doctor_ID = new LabAdmin();
            $Doctor_ID->update($where, $data);
            // Update in staff table
            if(isset($request->iLabAdminId) && !empty($request->iLabAdminId) )
            {
                $iLabAdminId = $request->iLabAdminId;
               
                // Check this user exist in lab admin table 
                $exist_labAdmin = LabAdmin::user_exit_in_staff($iLabAdminId,$iCustomerId);
                if($exist_labAdmin>0)
                {
                    $labData = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        // 'vUserName'=>$request->vFirstName,
                        'vEmail'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $whereLabData = array(
                        'iLoginAdminId'=>$iLabAdminId,
                        'iCustomerId'=>$iCustomerId
                    );
                    LabAdmin::update_staff_data($labData,$whereLabData);
                }
                
            }
            return redirect()->route('admin.lab_admin')->withSuccess('Lab Admin updated successfully.');
        }
        else{
            $iLabAdminId = LabAdmin::add($data);
            $data_staff['iDepartmentId']    = 2;
            // $data_staff['vMiddleInitial']   = $request->vName;
            $data_staff['vFirstName']       = $request->vFirstName;
            $data_staff['vLastName']        = $request->vLastName;
            // $data_staff['vUserName']        = $request->vFirstName;
            $data_staff['vEmail']           = $request->vEmail;
            $data_staff['vPassword']        = md5($request->vPassword);
            $data_staff['vMobile']          = $request->vMobile;
            $data_staff['eStatus']          = $request->eStatus;
            $data_staff['iLoginAdminId']    = $iLabAdminId;
            $data_staff['dtAddedDate']      = date("Y-m-d H:i:s");
            if(isset($request->iCustomerId) && !empty($request->iCustomerId))
            {
                $data_staff['iCustomerId'] = $request->iCustomerId;
            }
            else
            {
                $data_staff['iCustomerId'] = General::admin_info()['iCustomerId'];
            }
            Staff::add($data_staff);
            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            return redirect()->route('admin.lab_admin')->withSuccess('Lab Admin created successfully.');
        }
    }

    public function edit($iLabAdminId)
    {
        $data['lab_data'] = LabAdmin::get_by_id($iLabAdminId);
        $data['customer'] = Customer::get_all_customer();

        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['lab_data']->iCustomerId) && $data['lab_data']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['lab_data']->iCustomerId)))
        {
            return redirect()->route('admin.lab_admin')->withError('Invalid Input');
        }
        
        return view('admin.lab_admin.create')->with($data);
    }

    public function destroy($id)
    {
        $Grade = LabAdmin::find($id);
        $Grade->delete();

        return redirect()->route('admin.grade')->withSuccess('Lab Admin deleted successfully.');
    }

    public function own_listing($iCustomerId)
    {
        $data['lab_data'] = LabAdmin::get_all_data(); 
        $data['iCustomerId'] = $iCustomerId; 
        $data['customer'] = Customer::get_all_customer();
        return view('admin.lab_admin.listing')->with($data);
    }
}
