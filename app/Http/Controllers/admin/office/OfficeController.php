<?php

namespace App\Http\Controllers\admin\office;

use App\Http\Controllers\Controller;
use App\Models\admin\office\Office;
use App\Models\admin\customertype\CustomerType;
use App\Models\admin\state\State;
use App\Models\admin\staff\Staff;
use Illuminate\Http\Request;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use App\Libraries\Paginator;
use Validator;
use Hash;
use Auth;
use Session;
class OfficeController extends Controller
{
    public function index($iCustomerId = "")
    { 
        if (!empty($iCustomerId)) {
            $data['iCustomerId'] = $iCustomerId;
         }
        $data['customer'] = Office::get_all_customer(); 
        return view('admin.office.listing')->with($data);
    }
    
    public function ajax_listing(Request $request)
    {

        $action = $request->action;
        $iCustomerId = $request->iCustomerId;
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iOfficeId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iOfficeId']    = $request->id;

            Office::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Office_ID = (explode(",",$request->id));

            foreach ($Office_ID as $key => $value) {
                $where                 = array();
                $where['iOfficeId']    = $value;

                Office::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $Admin_data = Office::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iCustomerId'] = $iCustomerId;

        $data['data'] = Office::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        return view('admin.office.ajax_listing')->with($data);   
    }

    public function create()
    {
        $criteria = array();
        $data['customer'] = Office::get_all_customer();
        $iCountryId = "223";
        $data['states'] = State::get_by_iCountryId($iCountryId);
        $officeid = Office::get_office_id();
        if(!empty($officeid->iOfficeId)) {
            $id = $officeid->iOfficeId+1;
        }else{           
            $id = 1;
        }
        $data['id'] = "1".str_pad($id,3,"0",STR_PAD_LEFT);
        return view('admin.office.create')->with($data);
    }
    public function create_staff($iCustomerId='')
    {
        $criteria = array();
        $data['customer'] = Office::get_all_customer();
        $iCountryId = "223";
        $data['states'] = State::get_by_iCountryId($iCountryId);
        $officeid = Office::get_office_id();
        if(!empty($officeid->iOfficeId)) {
            $id = $officeid->iOfficeId+1;
        }else{           
            $id = 1;
        }
        $data['iCustomerId_New'] = $iCustomerId;
        $data['id'] = "1".str_pad($id,3,"0",STR_PAD_LEFT);
        return view('admin.office.create')->with($data);
    }

    public function store(Request $request){

        $iOfficeId = $request->id;

        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/customer'); 
            $request->vImage->move($path, $imageName);
        }

        $data['vName']        = $request->vName;
        $data['iCustomerId']  = $request->iCustomerId;
        $data['vEmail']       = $request->vEmail;
        $data['vMobile']      = $request->vMobile;
        $data['vOfficeCode']  = $request->vOfficeCode;
        $data['vCellulor']    = $request->vCellulor;
        $data['vTitle']       = $request->vTitle;
        $data['vWebsiteName'] = $request->vWebsiteName;
        $data['tDescription'] = $request->tDescription;
        $data['vOfficeName']  = $request->vOfficeName;
        $data['vAddress']     = $request->vAddress;
        $data['vCity']        = $request->vCity;
        $data['iStateId']     = $request->iStateId;
        $data['vZipCode']     = $request->vZipCode;
        $data['eStatus']      = $request->eStatus;
        
        if(empty($iOfficeId)){
            $data['vPassword']    = md5($request->vPassword);
          }
        if($iOfficeId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if ($request->hasFile('vImage')) {   
            $data['vImage']    = $imageName;
        }

        if(!empty($iOfficeId)){
            $where               = array();
            $where['iOfficeId']         = $iOfficeId;

            $Office_ID = new Office();
            $Office_ID->update($where, $data);
            $iCustomerId = $request->iCustomerId;
           
            return redirect()->route('admin.office')->withSuccess('Office updated successfully.');
        }
        else{
            $iLoginAdminId = Office::add($data);
           
            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            return redirect()->route('admin.office')->withSuccess('Office created successfully.');
        }
    }

    public function edit($iOfficeId)
    {
        $data['offices'] = Office::get_by_id($iOfficeId);
        $criteria = array();
        $data['customer'] = Office::get_all_customer();
        $iCountryId = "223";
        $data['states'] = State::get_by_iCountryId($iCountryId);
        $officeid = Office::get_office_id();
        $id = $officeid->iOfficeId;
        $data['id'] = "1".str_pad($id,3,"0",STR_PAD_LEFT);
        return view('admin.office.create')->with($data);
    }
    public function officelist($iCustomerId)
    {   
        $data['iCustomerId'] = $iCustomerId;
        $data['customer'] = Office::get_all_customer();
        return view('admin.office.office_listing')->with($data);
    }
    
    public function office_ajax_listing(Request $request)
    {

        $action = $request->action;
        $iCustomerId = $request->iCustomerId;
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iOfficeId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iOfficeId']    = $request->id;

            Office::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Office_ID = (explode(",",$request->id));

            foreach ($Office_ID as $key => $value) {
                $where                 = array();
                $where['iOfficeId']    = $value;

                Office::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $Admin_data = Office::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iCustomerId'] = $iCustomerId;

        $data['data'] = Office::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        return view('admin.office.office_ajax_listing')->with($data);   
    }

    public function officecreate($iCustomerId)
    {
        $criteria = array();
        $data['customer'] = Office::get_all_customer();
        $iCountryId = "223";
        $data['states'] = State::get_by_iCountryId($iCountryId);
        $officeid = Office::get_office_id();
        if(!empty($officeid->iOfficeId)) {
            $id = $officeid->iOfficeId+1;
        }else{           
            $id = 1;
        }
        $data['id'] = "1".str_pad($id,3,"0",STR_PAD_LEFT);
        $data['iCustomerId'] = $iCustomerId;
        return view('admin.office.office_create')->with($data);
    }

    public function officestore(Request $request)
    {
        $iOfficeId = $request->id;
        $iCustomerId = $request->iCustomerId;
        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/customer'); 
            $request->vImage->move($path, $imageName);
        }

        $data['vName']        = $request->vName;
        $data['iCustomerId']  = $request->iCustomerId;
        $data['vEmail']       = $request->vEmail;
        $data['vMobile']      = $request->vMobile;
        $data['vOfficeCode']  = $request->vOfficeCode;
        $data['vCellulor']    = $request->vCellulor;
        $data['vTitle']       = $request->vTitle;
        $data['vWebsiteName'] = $request->vWebsiteName;
        $data['tDescription'] = $request->tDescription;
        $data['vOfficeName']  = $request->vOfficeName;
        $data['vAddress']     = $request->vAddress;
        $data['vCity']        = $request->vCity;
        $data['iStateId']     = $request->iStateId;
        $data['vZipCode']     = $request->vZipCode;
        $data['eStatus']      = $request->eStatus;
        
        if(empty($iOfficeId)){
            $data['vPassword']    = md5($request->vPassword);
          }

        if($iOfficeId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if ($request->hasFile('vImage')) {   
            $data['vImage']    = $imageName;
        }

        if(!empty($iOfficeId)){
            $where               = array();
            $where['iOfficeId']         = $iOfficeId;

            $Office_ID = new Office();
            $Office_ID->update($where, $data);

            return redirect()->route('admin.officelist',$iCustomerId)->withSuccess('Office updated successfully.');
        }
        else{
            $data = Office::add($data);

            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            return redirect()->route('admin.officelist',$iCustomerId)->withSuccess('Office created successfully.');
        }
    }
    public function officeedit($iCustomerId,$iOfficeId)
    {
        $data['offices'] = Office::get_by_id($iOfficeId);
        $criteria = array();
        $data['customer'] = Office::get_all_customer();
        $iCountryId = "223";
        $data['states'] = State::get_by_iCountryId($iCountryId);
        $officeid = Office::get_office_id();
        $id = $officeid->iOfficeId;
        $data['id'] = "1".str_pad($id,3,"0",STR_PAD_LEFT);
        $data['iCustomerId'] = $iCustomerId;
        return view('admin.office.office_create')->with($data);
    }
    public function showChangePasswordForm($iOfficeId)
    {
        $data['offices'] = Office::get_by_id($iOfficeId);
        return view('admin.office.showChangePasswordForm')->with($data);
    }
    
    public function changePassword(Request $request)
    {
        $iOfficeId = $request->id;
        $iCustomerId = $request->iCustomerId;
        $where                  = array();
        $data['vPassword']      = md5($request->vPassword);
        $where['iOfficeId']     = $iOfficeId;

        $Office_id = new Office();
        $Office_id->update($where, $data);

        
        return redirect()->back()->with("success","Password changed successfully !");

    }
}
