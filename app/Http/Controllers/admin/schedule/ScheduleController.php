<?php

namespace App\Http\Controllers\admin\schedule;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\schedule\Schedule;
use App\Models\admin\office\Office;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;

class ScheduleController extends Controller
{
    
    public function index()
    {
        $data['data'] = Schedule::get_all_schedule();
        return view('admin.schedule.listing')->with($data);
    }
    public function ajax_listing(Request $request)
    {

        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iScheduleId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iScheduleId']    = $request->id;

            Schedule::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Grade_ID = (explode(",",$request->id));

            foreach ($Grade_ID as $key => $value) {
                $where                 = array();
                $where['iScheduleId']    = $value;

                Schedule::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $Grade_data = Schedule::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Grade_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
               $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $data['data'] = Schedule::get_all_data($criteria, $start, $limit, $paging);
        // dd($data['data']);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        
        return view('admin.schedule.ajax_listing')->with($data);   
    }
    public function create()
    {
        $data['customer'] = Office::get_all_customer();
        return view('admin.schedule.create')->with($data);
    }

   public function store(Request $request)
    {
        $iScheduleId = $request->id;
        if($request->iCustomerId){
            $data['iCustomerId'] = $request->iCustomerId ;
            
        }else{
            
            $data['iCustomerId'] = null;
        }
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $data['dtHolidays']      = date("Y-m-d ",strtotime($request->dtHolidays));
        $data['iYear']          = $request->iYear;
        $data['eMonth']         = $request->eMonth;
        $data['iDay']           = $request->iDay;
        $data['eWeek']          = $request->eWeek;
        $data['eWeekMonth']     = $request->eWeekMonth;
        $data['tiOpen']         = $request->tiOpen;
        $data['tiClose']        = $request->tiClose;
        $data['vTitle']         = $request->vTitle;
        $data['eStatus']        = $request->eStatus;
        if($iScheduleId){
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        if(!empty($iScheduleId)){
            $where                      = array();
            $where['iScheduleId']       = $iScheduleId;

            $Grade_id = new Schedule();
            $Grade_id->update($where, $data);

            return redirect()->route('admin.schedule')->withSuccess('Schedule updated successfully.');
        }
        else{
            $Grade_id = Schedule::add($data);
            return redirect()->route('admin.schedule')->withSuccess('Schedule created successfully.');
        }
    }
   public function store_new(Request $request)
    {
       
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        if($request->eSunStatus){
            $data['eStatus']        = $request->eSunStatus;
            $data['tiOpen']         = $request->tSuniOpen;
            $data['tiClose']        = $request->tSuniClose;
            $where                  = array();
            $iScheduleId   = "1";

            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            $where                      = array();
            $where['iScheduleId']       = $iScheduleId;
        
            $Grade_id = new Schedule();
            $Grade_id->update($where, $data);
        }
        if($request->eMonStatus){
            $data['eStatus']     = $request->eMonStatus;
            $data['tiOpen']         = $request->tMoniOpen;
            $data['tiClose']        = $request->tMoniClose;
            $where                  = array();
            $iScheduleId   = "2";

            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            $where                      = array();
            $where['iScheduleId']       = $iScheduleId;
        
            $Grade_id = new Schedule();
            $Grade_id->update($where, $data);
        }
        if($request->eTueStatus){
            $data['eStatus']     = $request->eTueStatus;
            $data['tiOpen']         = $request->tTueiOpen;
            $data['tiClose']        = $request->tTueiClose;
            $where                  = array();
            $iScheduleId   = "3";

            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            $where                      = array();
            $where['iScheduleId']       = $iScheduleId;
        
            $Grade_id = new Schedule();
            $Grade_id->update($where, $data);
        }
        if($request->eWedStatus){
            $data['eStatus']     = $request->eWedStatus;
            $data['tiOpen']         = $request->tWediOpen;
            $data['tiClose']        = $request->tWediClose;
            $where                  = array();
            $iScheduleId   = "4";

            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            $where                      = array();
            $where['iScheduleId']       = $iScheduleId;
        
            $Grade_id = new Schedule();
            $Grade_id->update($where, $data);
        }
        if($request->eThuStatus){
            $data['eStatus']     = $request->eThuStatus;
            $data['tiOpen']         = $request->tThuiOpen;
            $data['tiClose']        = $request->tThuiClose;
            $where                  = array();
            $iScheduleId   = "5";

            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            $where                      = array();
            $where['iScheduleId']       = $iScheduleId;
        
            $Grade_id = new Schedule();
            $Grade_id->update($where, $data);
        }
        if($request->eFriStatus){
            $data['eStatus']     = $request->eFriStatus;
            $data['tiOpen']         = $request->tFriiOpen;
            $data['tiClose']        = $request->tFriiClose;
            $where                  = array();
            $iScheduleId   = "6";

            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            $where                      = array();
            $where['iScheduleId']       = $iScheduleId;
        
            $Grade_id = new Schedule();
            $Grade_id->update($where, $data);
        }
        if($request->eSatStatus){
            $data['eStatus']        = $request->eSatStatus;
            $data['tiOpen']         = $request->tSatiOpen;
            $data['tiClose']        = $request->tSatiClose;
            $where                  = array();
            $iScheduleId   = "7";
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            $where                      = array();
            $where['iScheduleId']       = $iScheduleId;
        
            $Grade_id = new Schedule();
            $Grade_id->update($where, $data);
        }
            
            return redirect()->route('admin.schedule')->withSuccess('Schedule updated successfully.');
    }
    
    public function edit($iScheduleId)
    {
        $data['schedule'] = Schedule::get_by_id($iScheduleId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['schedule']->iCustomerId) && $data['schedule']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['schedule']->iCustomerId)))
        {
            return redirect()->route('admin.schedule')->withError('Invalid Input');
        }
        $data['customer'] = Office::get_all_customer();
        return view('admin.schedule.create')->with($data);
    }
}
