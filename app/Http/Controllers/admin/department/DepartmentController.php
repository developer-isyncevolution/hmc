<?php

namespace App\Http\Controllers\admin\department;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\department\Department;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;

class DepartmentController extends Controller
{
    public function index()
    {
        return view('admin.department.listing');
    }

    public function ajax_listing(Request $request)
    {

        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iDepartmentId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iDepartmentId']    = $request->id;

            Department::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Department_ID = (explode(",",$request->id));

            foreach ($Department_ID as $key => $value) {
                $where                 = array();
                $where['iDepartmentId']    = $value;

                Department::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $Department_data = Department::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Department_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = Department::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        
        return view('admin.department.ajax_listing')->with($data);   
    }

    public function create()
    {
        return view('admin.department.create');
    }

   public function store(Request $request)
    {
        $iDepartmentId = $request->id;

        $data['vName']         = $request->vName;
        $data['eStatus']       = $request->eStatus;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        if($iDepartmentId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iDepartmentId)){
            $where                      = array();
            $where['iDepartmentId']       = $iDepartmentId;

            $Department_id = new Department();
            $Department_id->update($where, $data);

            return redirect()->route('admin.department')->withSuccess('Department updated successfully.');
        }
        else{
            $Department_id = Department::add($data);

            return redirect()->route('admin.department')->withSuccess('Department created successfully.');
        }
    }

    public function edit($iDepartmentId)
    {
        $data['departments'] = Department::get_by_id($iDepartmentId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['departments']->iCustomerId) && $data['departments']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['departments']->iCustomerId)))
        {
            return redirect()->route('admin.department')->withError('Invalid Input');
        }
        return view('admin.department.create')->with($data);
    }

    public function destroy($id)
    {
        $Department = Department::find($id);
        $Department->delete();

        return redirect()->route('admin.department')->withSuccess('Department deleted successfully.');
    }

    public function multiDelete(Request $request)
    {
        $id = $request->id;

        Department::whereIn('id',explode(",",$id))->delete();
    }
}
