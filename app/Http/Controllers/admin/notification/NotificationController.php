<?php

namespace App\Http\Controllers\admin\notification;

use App\Http\Controllers\Controller;
use App\Models\admin\notification\Notification;
use Illuminate\Http\Request;
use Validator;
use App\Libraries\Paginator;

class NotificationController extends Controller
{
    public function index()
    {
        $data['notifications'] = notification::get();
        return view('admin.notification.listing')->with($data);
    }

    public function ajax_listing(Request $request)
    {

        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iNotificationId";
            $order = "DESC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iNotificationId']    = $request->id;

            Notification::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Notification_ID = (explode(",",$request->id));

            foreach ($Notification_ID as $key => $value) {
                $where                 = array();
                $where['iNotificationId']    = $value;

                Notification::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $notification_data = Notification::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($notification_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = Notification::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        
        return view('admin.notification.ajax_listing')->with($data);   
    }

    public function create()
    {
        return view('admin.notification.create');
    }

    public function store(Request $request)
    {
        $iNotificationId = $request->id;

        $data['vNotificationCode']= $request->vNotificationCode;
        $data['eEmail']           = $request->eEmail;
        $data['eSms']             = $request->eSms;

        if($iNotificationId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iNotificationId)){
            $where                      = array();
            $where['iNotificationId']         = $iNotificationId;

            $Notification_id = new Notification();
            $Notification_id->update($where, $data);

            return redirect()->route('admin.notification')->withSuccess('Notification updated successfully.');
        }
        else{
            $Notification_id = Notification::add($data);

            return redirect()->route('admin.notification')->withSuccess('Notification created successfully.');
        }
    }

    public function edit($iNotificationId)
    {
        $data['notifications'] = Notification::get_by_id($iNotificationId);

        return view('admin.notification.create')->with($data);
    }

    public function update(Request $request, notification $notification)
    {
        //
    }

    public function destroy($id)
    {
        $notification = Notification::find($id);
        $notification->delete();

        return redirect()->route('admin.notification')->withSuccess('Notification deleted successfully.');
    }

    public function changeStatus(Request $request)
    {  
        $id                     = $request->id;
        $notification           = Notification::find($id);
        $notification->status   = $request->status;
        $notification->save();

        return response()->json(['message'=>"Status changed successfully."]);
    }

    public function multiDelete(Request $request)
    {
        $id = $request->notificationID;
        Notification::whereIn('id',explode(",",$id))->delete();
    }
}
