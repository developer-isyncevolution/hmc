<?php

namespace App\Http\Controllers\admin\billing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\billing\Billing;
use App\Models\admin\labOffice\LabOffice;
use App\Models\admin\customer\Customer;
use App\Models\admin\lab_admin\LabAdmin;
use App\Models\admin\office_admin\OfficeAdmin;
use App\Models\admin\driver_history\DriverHistory;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;
use PDF;
use Illuminate\Support\Facades\File;
use App\Models\admin\system_email\SystemEmail;

class BillingController extends Controller
{
    public function index()
    {
        Session::forget('per_page_lan');

        $iCustomerId = General::admin_info()['iCustomerId'];
        $OfficeData = LabOffice::get_active_office_by_labId($iCustomerId);

        $labsData = LabOffice::get_active_lab_by_officeId($iCustomerId);
        $office = array();
        $labs = array();
        foreach ($OfficeData as $key => $value) {
            $office[] = Customer::get_by_id($value->iOfficeId);
        }
        $data['office'] = $office;
        foreach ($labsData as $key => $value) {
            $labs[] = Customer::get_by_id($value->iLabId);
        }
        // dd($data);
        $data['labs'] = $labs;
        $CustomerType = General::admin_info()['customerType'];
        $BillingExist = Billing::all()->count();
        if($BillingExist>0)
        {
            $LatestRecord  = Billing::get_last_record();
            if($CustomerType =='Lab Admin')
            {
                $iChangeId = $LatestRecord->iOfficeId;
            }
            else
            {
                $iChangeId = $LatestRecord->iLabId;
            }
            $data['iChangeId'] = $iChangeId;
        }
        return view('admin.billing.listing',$data);
    }

    public function ajax_listing(Request $request)
    {
       
        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iBillingId";
            $order = "ASC";
        }

        $iChangeId = "";
        if($action == "search"){
            $vKeyword = $request->keyword;
            $duration = $request->duration;
            $startdate = "";
            $enddate = "";
            if(isset($request->startdate) && !empty($request->startdate))
            {
                $startdate = date("Y-m-d", strtotime($request->startdate));
            }
            if(isset($request->enddate) && !empty($request->enddate))
            {
                $enddate = date("Y-m-d", strtotime($request->enddate));
            }
            if(isset($request->iChangeId) && !empty($request->iChangeId))
            {
                $iChangeId = $request->iChangeId;
            }
        } else {
            $vKeyword = "";
            $duration = '';
            if($request->pages != "")
            {
                $duration = '';
            }
            else
            {  
                $duration = 'Today';
            }
            $startdate = "";
            $enddate = "";
            $iCustomerId = "";
            $CustomerType = General::admin_info()['customerType'];
            $BillingExist = Billing::all()->count();

            if($BillingExist>0)
            {
                $LatestRecord  = Billing::get_last_record();
                if($CustomerType =='Lab Admin')
                {
                    $iChangeId = $LatestRecord->iOfficeId;
                }
                else
                {
                    $iChangeId = $LatestRecord->iLabId;
                }
                
            }
        }
        
        if($action == "delete"){
            $where                 = array();
            $where['iGradeId']    = $request->id;

            Billing::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Grade_ID = (explode(",",$request->id));

            foreach ($Grade_ID as $key => $value) {
                $where                 = array();
                $where['iGradeId']    = $value;

                Billing::delete_by_id($where);
            }
        }

        $criteria_sess = Session::get('criteria');
       
        if(!empty($action) && $action == "search")
        {
            $criteria = array();
            $criteria['vKeyword']   = $vKeyword;
            $criteria['duration']   = $duration;
            $criteria['startdate']   = $startdate;
            $criteria['enddate']   = $enddate;
            $criteria['column']     = $column;
            $criteria['order']      = $order;
            // For Perticular customer show start
            $criteria['iChangeId'] = $iChangeId;
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
            Session::put('criteria', $criteria); 
        // For Perticular customer show end
        }elseif((isset($criteria_sess) && !empty($criteria_sess)))
        {
            
            $criteria = array();
            $criteria['vKeyword']   = isset($criteria_sess['vKeyword'])?$criteria_sess['vKeyword']:'';
            $criteria['duration']   = isset($criteria_sess['duration'])?$criteria_sess['duration']:'';
            $criteria['startdate']   = isset($criteria_sess['startdate'])?$criteria_sess['startdate']:'';
            $criteria['enddate']   = isset($criteria_sess['enddate'])?$criteria_sess['enddate']:'';
            $criteria['column']     = isset($criteria_sess['column'])?$criteria_sess['column']:'';
            $criteria['order']      = isset($criteria_sess['order'])?$criteria_sess['order']:'';
            // For Perticular customer show start
            $criteria['iChangeId'] = isset($criteria_sess['iChangeId'])?$criteria_sess['iChangeId']:'';;
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
         
        }
        else
        {
            $criteria = array();
            $criteria['vKeyword']   = $vKeyword;
            $criteria['duration']   = $duration;
            $criteria['startdate']   = $startdate;
            $criteria['enddate']   = $enddate;
            $criteria['column']     = $column;
            $criteria['order']      = $order;
            // For Perticular customer show start
            $criteria['iChangeId'] = $iChangeId;
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
            Session::put('criteria', $criteria); 
        }
        $limit = 9;
        $criteria['paging']     = false;  
        $Billing_data = Billing::get_all_data($criteria);
        // $Billing_data = Billing::all()->count();
        $pages = 1;  
        
        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Billing_data);

      
        if($request->limit_page !='')
         {
             $per_page = $request->limit_page;
             Session::put('per_page_lan', $per_page);
             $paginator->itemsPerPage = $per_page;
             //$paginator->range = $per_page;
         }
         else
         {
             $per_page = Session::get('per_page_lan');
             if($per_page !='')
             {
                 $paginator->itemsPerPage = $per_page;
                 //$paginator->range = $per_page;
                 $limit = $per_page;
             }
             else
             {
                 $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
             }
             
         }
         $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
         if($request->limit_page !='')
         {
             $limit = $request->limit_page;
         }
         else
         {
             $limit = $paginator->itemsPerPage;
         }
        $paginator->is_ajax = true;
        $paging = true;
       
        $data['data'] = Billing::get_all_data($criteria, $start, $limit, $paging);
     
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        $data['iChangeId'] = $iChangeId;        
       
        return view('admin.billing.ajax_listing')->with($data);   
    }

    public function create()
    {
        return view('admin.billing.create');
    }

    public function store(Request $request)
    {
        $iBillingId = $request->id;
        $data['iMainTotal'] = $request->iMainTotal;
        if($iBillingId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }
        if(!empty($iBillingId)){
            $where                      = array();
            $where['iBillingId']       = $iBillingId;
          
            $Billing_id = new Billing();
            $Billing_id->update($where, $data);

            // Update stage data start
            if(isset($request->UpperStageTotal) && count($request->UpperStageTotal)>0)
            {
                $Billing_stage = new Billing();
                $where_stage['eType'] = 'Upper';
                $where_stage['iBillingId'] = $iBillingId;
                if(isset($request->UpperStageTotal['iStagePrice']) && !empty($request->UpperStageTotal['iStagePrice']))
                {
                    $data_satge['iStagePrice']=$request->UpperStageTotal['iStagePrice'];
                }
                elseif(isset($request->UpperStageTotal['iStageTotalWithRush']) && !empty($request->UpperStageTotal['iStageTotalWithRush']))
                {
                    $data_satge['iStageTotalWithRush']=$request->UpperStageTotal['iStageTotalWithRush'];
                }
                if(isset($request->UpperStageTotal['dDeliveryDate']) && !empty($request->UpperStageTotal['dDeliveryDate']))
                {
                    $data_satge['dDeliveryDate']=$request->UpperStageTotal['dDeliveryDate'];
                }
                $Billing_stage->update_stage($where_stage, $data_satge);
            }
            if(isset($request->LowerStageTotal) && count($request->LowerStageTotal)>0)
            {
                $Billing_stage = new Billing();
                $where_stage['eType'] = 'Lower';
                $where_stage['iBillingId'] = $iBillingId;
                if(isset($request->LowerStageTotal['iStagePrice']) && !empty($request->LowerStageTotal['iStagePrice']))
                {
                    $data_satge['iStagePrice']=$request->LowerStageTotal['iStagePrice'];
                }
                elseif(isset($request->LowerStageTotal['iStageTotalWithRush']) && !empty($request->LowerStageTotal['iStageTotalWithRush']))
                {
                    $data_satge['iStageTotalWithRush']=$request->LowerStageTotal['iStageTotalWithRush'];
                }
                if(isset($request->LowerStageTotal['dDeliveryDate']) && !empty($request->LowerStageTotal['dDeliveryDate']))
                {
                    $data_satge['dDeliveryDate']=$request->LowerStageTotal['dDeliveryDate'];
                }
                $Billing_stage->update_stage($where_stage, $data_satge);
            }
            // Update stage data end
            // Update addons price start

            if(isset($request->UpperAddonTotal) && count($request->UpperAddonTotal)>0)
            {
                foreach($request->UpperAddonTotal as $key=> $UpperAddonTotal_val)
                {
                    $billingProductWhere['iBillingId'] = $iBillingId;
                    $billingProductWhere['eType'] = 'Upper';
                    $billingProductData = Billing::get_billing_product($billingProductWhere);
                    $data_addons['iAddOnsTotal'] = $UpperAddonTotal_val['iAddOnsTotal'];
                    $where_addons['iAddCategoryId'] = $UpperAddonTotal_val['iAddCategoryId'];
                    $where_addons['iBillingProductId']= $billingProductData->iBillingProductId;
                    $where_addons['iBillingId']= $iBillingId;
                    $Billing_stage->update_billing($where_addons, $data_addons);
                }
            }
            if(isset($request->LowerAddonTotal) && count($request->LowerAddonTotal)>0)
            {
                foreach($request->LowerAddonTotal as $key=> $LowerAddonTotal_val)
                {
                    $billingProductWhere['iBillingId'] = $iBillingId;
                    $billingProductWhere['eType'] = 'Lower';
                    $billingProductData = Billing::get_billing_product($billingProductWhere);
                    $data_addons['iAddOnsTotal'] = $LowerAddonTotal_val['iAddOnsTotal'];
                    $where_addons['iAddCategoryId'] = $LowerAddonTotal_val['iAddCategoryId'];
                    $where_addons['iBillingProductId']= $billingProductData->iBillingProductId;
                    $where_addons['iBillingId']= $iBillingId;
                    $Billing_stage->update_billing($where_addons, $data_addons);
                }
            }
            // Update addons price end

            return redirect()->route('admin.billing')->withSuccess('Bill updated successfully.');
        }
        
    }

    public function edit($iBillingId)
    {
        $criteria = array();
        $criteria['iBillingId'] = $iBillingId;
        $data['billings'] = Billing::get_all_data_edit($criteria);
        return view('admin.billing.create')->with($data);
    }

    public function destroy($id)
    {
        $Grade = Billing::find($id);
        $Grade->delete();

        return redirect()->route('admin.billing')->withSuccess('Grade deleted successfully.');
    }

    public function multiDelete(Request $request)
    {
        $id = $request->id;

        Billing::whereIn('id',explode(",",$id))->delete();
    }

    public function PrintBilling(Request $request)
    {
        $criteria['iBillingIds'] = explode(',', $request->checked_id);
        $data['data'] = Billing::get_all_data($criteria);
        if(isset($data['data']) && !empty($data['data']))
        {
            $iOfficeId = $data['data'][0]->iOfficeId;
            $iLabId = $data['data'][0]->iLabId;
            $office = Customer::get_by_id($iOfficeId);
            $labs = Customer::get_by_id($iLabId);
            $data['lab_logo'] = LabAdmin::get_by_iCustomerId($iLabId);
            $data['office_logo'] = OfficeAdmin::get_by_iCustomerId($iOfficeId);
            $data['office'] = $office;  
            $data['labs'] = $labs;
            if(isset($request->duration_val) && !empty($request->duration_val))
            {
                if($request->duration_val == 'Today')
                {
                    $data['duration_month']= date('l - m/d/Y');
                }
                elseif($request->duration_val == 'Yesterday')
                {
                    $data['duration_month']= date('l - m/d/Y',time() - 60 * 60 * 24);
                }
                elseif($request->duration_val == 'ThisWeek')
                {
                    $data['duration_month']= date('m/d/Y', strtotime("this week")).' - '.date('m/d/Y');
                }
                elseif($request->duration_val == 'LastWeek')
                {
                    $data['duration_month']= date('m/d/Y', strtotime("sunday -2 week")).' - '.date('m/d/Y',strtotime("sunday -1 week"));
                }
                elseif($request->duration_val == 'ThisMonth')
                {
                    $data['duration_month']= date("F / Y");;
                }
                elseif($request->duration_val == 'LastMonth')
                {
                    $currentMonth = date('F');
                    $data['duration_month']= date('F / Y', strtotime($currentMonth . "last month"));
                }
                elseif($request->duration_val == 'ThisYear')
                {
                    $data['duration_month']= date('Y');
                }
                elseif($request->duration_val == 'LastYear')
                {
                    $data['duration_month']= date("Y",strtotime("-1 year"));
                }
                else
                {
                    $data['duration_month'] = $request->duration_val;
                }
            }
        }
        // return view('admin.billing.print_billing_statement',$data);
        $pdf = PDF::loadView('admin.billing.print_billing_statement', $data);
        return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
       
    }
    public function PrintBillingInvoice(Request $request)
    {
        $criteria['iBillingIds'] = explode(',', $request->checked_id);
        $data['data'] = Billing::get_all_data($criteria);

        if(isset($data['data']) && !empty($data['data']))
        {
            $iOfficeId = $data['data'][0]->iOfficeId;
            $iLabId = $data['data'][0]->iLabId;
            $office = Customer::get_by_id($iOfficeId);
            $labs = Customer::get_by_id($iLabId);
            // $data['lab_logo'] = LabAdmin::get_by_iCustomerId($iLabId);
            // $data['office_logo'] = OfficeAdmin::get_by_iCustomerId($iOfficeId);
            $data['office'] = $office;  
            $data['labs'] = $labs;
            if(isset($request->duration_val) && !empty($request->duration_val))
            {
                if($request->duration_val == 'Today')
                {
                    $data['duration_month']= date('l - m/d/Y');
                }
                elseif($request->duration_val == 'Yesterday')
                {
                    $data['duration_month']= date('l - m/d/Y',time() - 60 * 60 * 24);
                }
                elseif($request->duration_val == 'ThisWeek')
                {
                    $data['duration_month']= date('m/d/Y', strtotime("this week")).' - '.date('m/d/Y');
                }
                elseif($request->duration_val == 'LastWeek')
                {
                    $data['duration_month']= date('m/d/Y', strtotime("sunday -2 week")).' - '.date('m/d/Y',strtotime("sunday -1 week"));
                }
                elseif($request->duration_val == 'ThisMonth')
                {
                    $data['duration_month']= date("F / Y");;
                }
                elseif($request->duration_val == 'LastMonth')
                {
                    $currentMonth = date('F');
                    $data['duration_month']= date('F / Y', strtotime($currentMonth . "last month"));
                }
                elseif($request->duration_val == 'ThisYear')
                {
                    $data['duration_month']= date('Y');
                }
                elseif($request->duration_val == 'LastYear')
                {
                    $data['duration_month']= date("Y",strtotime("-1 year"));
                }
                else
                {
                    $data['duration_month'] = $request->duration_val;
                }
            }
        }
        $pdf = PDF::loadView('admin.billing.print_billing_invoice', $data);
        return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
       
    }
    public function SentMailBillingInvoice(Request $request)
    {
        $criteria['iBillingIds'] = explode(',', $request->checked_id);
        // dd($criteria['iBillingIds']);
        $data['data'] = Billing::get_all_data($criteria);
        // dd($data);
        if(isset($data['data']) && !empty($data['data']) && count($data['data'])>0)
        {
            $iOfficeId = $data['data'][0]->iOfficeId;
            $iLabId = $data['data'][0]->iLabId;
            $office = Customer::get_by_id($iOfficeId);
            $labs = Customer::get_by_id($iLabId);

            // $data['lab_logo'] = LabAdmin::get_by_iCustomerId($iLabId);
            // $data['office_logo'] = OfficeAdmin::get_by_iCustomerId($iOfficeId);
            $data['office'] = $office;  
            $data['labs'] = $labs;
            if(isset($request->duration_val) && !empty($request->duration_val))
            {
                if($request->duration_val == 'Today')
                {
                    $data['duration_month']= date('l - m/d/Y');
                }
                elseif($request->duration_val == 'Yesterday')
                {
                    $data['duration_month']= date('l - m/d/Y',time() - 60 * 60 * 24);
                }
                elseif($request->duration_val == 'ThisWeek')
                {
                    $data['duration_month']= date('m/d/Y', strtotime("this week")).' - '.date('m/d/Y');
                }
                elseif($request->duration_val == 'LastWeek')
                {
                    $data['duration_month']= date('m/d/Y', strtotime("sunday -2 week")).' - '.date('m/d/Y',strtotime("sunday -1 week"));
                }
                elseif($request->duration_val == 'ThisMonth')
                {
                    $data['duration_month']= date("F / Y");;
                }
                elseif($request->duration_val == 'LastMonth')
                {
                    $currentMonth = date('F');
                    $data['duration_month']= date('F / Y', strtotime($currentMonth . "last month"));
                }
                elseif($request->duration_val == 'ThisYear')
                {
                    $data['duration_month']= date('Y');
                }
                elseif($request->duration_val == 'LastYear')
                {
                    $data['duration_month']= date("Y",strtotime("-1 year"));
                }
                else
                {
                    $data['duration_month'] = $request->duration_val;
                }
            }
            $BillingIds = implode("_",$criteria['iBillingIds']);
            $pdf = PDF::loadView('admin.billing.print_billing_invoice', $data);
            $path = public_path('uploads/invoice_pdf');
            $fileName =  $iOfficeId.'-Office-'.$BillingIds. '.' . 'pdf' ;
            // Exist pdf delete start
            $isExists = File::exists($path.'/'.$fileName);
            if($isExists == false)
            {
                $pdf->save($path . '/' . $fileName);
            }
            // Exist pdf delete end
    
            // Email send start
            $criteria = array();
            $criteria['vEmailCode'] = 'USER_INVOICE';
            $email_system = SystemEmail::get_all_data($criteria);
            $iCustomerId = General::admin_info()['iCustomerId'];
            $email = Customer::get_by_id($iCustomerId);
            $subject = str_replace("#SYSTEM.COMPANY_NAME#", $email->vOfficeName, $email_system[0]->vEmailSubject);

            $constant   = array('#SITE_NAME#','#OfficeLogo#','#ContactEmail#');
            $path_img = asset('uploads/customer/'.$email->vImage);
            $value      = array($email->vOfficeName,'<img src="'.$path_img.'"
            style="text-align: center; display: block;margin: 0px auto 30px;"
            height="52" width="212" alt="">',$email->vEmail);
            $message = str_replace($constant, $value, $email_system[0]->tEmailMessage);
        
            // $email_data['to']       = $office->vEmail;
            $email_data['to']       = $request->vNotificationEmail;
            // $email_data['to']       = 'krupesh.thakkar@isyncevolution.com';
            $email_data['subject']  = $subject;
            $email_data['msg']      = $message;
            $email_data['from']     = $email->vFromEmailAddress;
            
            $files = [
                $path.'/'.$fileName
            ];
            if(isset($email->vHost) && !empty($email->vHost))
            {
                $email_data['vHost'] = $email->vHost;
                $email_data['vEmailUserName'] = $email->vEmailUserName;
                $email_data['vEmailPassword'] = $email->vEmailPassword;
                $email_data['vEmailPort'] = $email->vEmailPort;
                $email_data['vEmailEncryption'] = $email->vEmailEncryption;
                $email_data['vOfficeName'] = $email->vOfficeName;
                // $email_data['ccEmails'] = 'hon@labslips.online';
                // $email_data['bccEmails'] = 'krupesh.thakkar@isyncevolution.com';
                $email_data['bccEmails'] = 'kasim.abbas@isyncevolution.com';
            }
            
            General::send('USER_INVOICE', $email_data,$files);
            // Email send end
            return redirect()->back()->withSuccess('Invoce mail sent successfully.');
        }
       
    }

    public function PrintBillingSingle($iBillingId)
    {
        $criteria['iBillingId'] = $iBillingId;
        $data['data'] = Billing::get_all_data($criteria);

        if(isset($data['data']) && !empty($data['data']))
        {
            $iOfficeId = $data['data'][0]->iOfficeId;
            $iLabId = $data['data'][0]->iLabId;
            $office = Customer::get_by_id($iOfficeId);
            $labs = Customer::get_by_id($iLabId);
            $data['lab_logo'] = LabAdmin::get_by_iCustomerId($iLabId);
            $data['office_logo'] = OfficeAdmin::get_by_iCustomerId($iOfficeId);
            $data['office'] = $office;  
            $data['labs'] = $labs;
            if(isset($request->duration_val) && !empty($request->duration_val))
            {
                if($request->duration_val == 'Today')
                {
                    $data['duration_month']= date('l - m/d/Y');
                }
                elseif($request->duration_val == 'Yesterday')
                {
                    $data['duration_month']= date('l - m/d/Y',time() - 60 * 60 * 24);
                }
                elseif($request->duration_val == 'ThisWeek')
                {
                    $data['duration_month']= date('m/d/Y', strtotime("this week")).' - '.date('m/d/Y');
                }
                elseif($request->duration_val == 'LastWeek')
                {
                    $data['duration_month']= date('m/d/Y', strtotime("sunday -2 week")).' - '.date('m/d/Y',strtotime("sunday -1 week"));
                }
                elseif($request->duration_val == 'ThisMonth')
                {
                    $data['duration_month']= date("F / Y");;
                }
                elseif($request->duration_val == 'LastMonth')
                {
                    $currentMonth = date('F');
                    $data['duration_month']= date('F / Y', strtotime($currentMonth . "last month"));
                }
                elseif($request->duration_val == 'ThisYear')
                {
                    $data['duration_month']= date('Y');
                }
                elseif($request->duration_val == 'LastYear')
                {
                    $data['duration_month']= date("Y",strtotime("-1 year"));
                }
                else
                {
                    $data['duration_month'] = $request->duration_val;
                }
            }
        }
        $pdf = PDF::loadView('admin.billing.print_billing_statement', $data);
        return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
       
    }
    public function PrintBillingSingleInvoice($iBillingId)
    {
        $criteria['iBillingId'] = $iBillingId;
        $data['data'] = Billing::get_all_data($criteria);

        if(isset($data['data']) && !empty($data['data']))
        {
            $iOfficeId = $data['data'][0]->iOfficeId;
            $iLabId = $data['data'][0]->iLabId;
            $office = Customer::get_by_id($iOfficeId);
            $labs = Customer::get_by_id($iLabId);
            $data['lab_logo'] = LabAdmin::get_by_iCustomerId($iLabId);
            $data['office_logo'] = OfficeAdmin::get_by_iCustomerId($iOfficeId);
            $data['office'] = $office;  
            $data['labs'] = $labs;
            if(isset($request->duration_val) && !empty($request->duration_val))
            {
                if($request->duration_val == 'Today')
                {
                    $data['duration_month']= date('l - m/d/Y');
                }
                elseif($request->duration_val == 'Yesterday')
                {
                    $data['duration_month']= date('l - m/d/Y',time() - 60 * 60 * 24);
                }
                elseif($request->duration_val == 'ThisWeek')
                {
                    $data['duration_month']= date('m/d/Y', strtotime("this week")).' - '.date('m/d/Y');
                }
                elseif($request->duration_val == 'LastWeek')
                {
                    $data['duration_month']= date('m/d/Y', strtotime("sunday -2 week")).' - '.date('m/d/Y',strtotime("sunday -1 week"));
                }
                elseif($request->duration_val == 'ThisMonth')
                {
                    $data['duration_month']= date("F / Y");;
                }
                elseif($request->duration_val == 'LastMonth')
                {
                    $currentMonth = date('F');
                    $data['duration_month']= date('F / Y', strtotime($currentMonth . "last month"));
                }
                elseif($request->duration_val == 'ThisYear')
                {
                    $data['duration_month']= date('Y');
                }
                elseif($request->duration_val == 'LastYear')
                {
                    $data['duration_month']= date("Y",strtotime("-1 year"));
                }
                else
                {
                    $data['duration_month'] = $request->duration_val;
                }
            }
        }
        $pdf = PDF::loadView('admin.billing.print_billing_invoice', $data);
        return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
       
    }
    public function PrintBillingSingleSlipInvoice($iSlipId)
    {
        $criteria['iSlipId'] = $iSlipId;
        $data['data'] = Billing::get_all_data($criteria);
       
        if(isset($data['data']) && !empty($data['data']) && count($data['data'])>0)
        {
            $iOfficeId = $data['data'][0]->iOfficeId;
            $iLabId = $data['data'][0]->iLabId;
            $office = Customer::get_by_id($iOfficeId);
            $labs = Customer::get_by_id($iLabId);
            $data['lab_logo'] = LabAdmin::get_by_iCustomerId($iLabId);
            $data['office_logo'] = OfficeAdmin::get_by_iCustomerId($iOfficeId);
            $data['office'] = $office;  
            $data['labs'] = $labs;
            if(isset($request->duration_val) && !empty($request->duration_val))
            {
                if($request->duration_val == 'Today')
                {
                    $data['duration_month']= date('l - m/d/Y');
                }
                elseif($request->duration_val == 'Yesterday')
                {
                    $data['duration_month']= date('l - m/d/Y',time() - 60 * 60 * 24);
                }
                elseif($request->duration_val == 'ThisWeek')
                {
                    $data['duration_month']= date('m/d/Y', strtotime("this week")).' - '.date('m/d/Y');
                }
                elseif($request->duration_val == 'LastWeek')
                {
                    $data['duration_month']= date('m/d/Y', strtotime("sunday -2 week")).' - '.date('m/d/Y',strtotime("sunday -1 week"));
                }
                elseif($request->duration_val == 'ThisMonth')
                {
                    $data['duration_month']= date("F / Y");;
                }
                elseif($request->duration_val == 'LastMonth')
                {
                    $currentMonth = date('F');
                    $data['duration_month']= date('F / Y', strtotime($currentMonth . "last month"));
                }
                elseif($request->duration_val == 'ThisYear')
                {
                    $data['duration_month']= date('Y');
                }
                elseif($request->duration_val == 'LastYear')
                {
                    $data['duration_month']= date("Y",strtotime("-1 year"));
                }
                else
                {
                    $data['duration_month'] = $request->duration_val;
                }
            }
            $pdf = PDF::loadView('admin.billing.print_billing_invoice', $data);
            return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
        }
        else
        {
            return redirect()->back()->withError('No invoice found');
        }
       
    }
    public function fetchCustomerEmail(Request $request)
    {
        $CustomerData = Customer::get_by_id($request->iOfficeId);
        return isset($CustomerData->vNotificationEmail)?$CustomerData->vNotificationEmail:null;
    }
    public function driverSlipPrint(Request $request)
    {
        
        $iCustomerId = General::admin_info()['iCustomerId'];
        $criteria['iCustomerId'] = $iCustomerId;
        if(isset($request->start_date) && !empty($request->start_date))
        {
            $criteria['startdate'] = date('Y-m-d',strtotime($request->start_date));
        }
        if(isset($request->deliveryDateArray) && !empty($request->deliveryDateArray) && $request->deliveryDateArray !='CustomDate' && !empty($request->deliveryDateArray[0]))
        {
            // dd($request);
            if(isset($request->deliveryDate_array) && $request->deliveryDate_array == 'Yes')
            {
                $criteria['deliveryDateArray'] = $request->deliveryDateArray;
            }
            else
            {
                $deliveryDateArraySet = explode(',',$request->deliveryDateArray);
                $criteria['deliveryDateArray'] = $deliveryDateArraySet;
            }
        }
        if(isset($request->end_date) && !empty($request->end_date))
        {
            $criteria['enddate'] = date('Y-m-d',strtotime($request->end_date));
        }
        if(isset($request->islipIdPrint) && !empty($request->islipIdPrint))
        {
            $criteria['iSlipId'] = explode(',',$request->islipIdPrint);
        }
        // dd($criteria);
        $data['lab_name']=General::admin_info()['CustomerName'];
        $show_history =DriverHistory::show_paper_slip_history($criteria);
      
        foreach ($show_history as $key => $value) {
            $dPickUpDate = "";
            $slip_product_data = DriverHistory::get_slip_product_by_slipid($value->iSlipId);
            // dd($slip_product_data);
            $show_history[$key]->dPickUpDate = $slip_product_data->dPickUpDate;
        }
        if(count($show_history)>0)
        {
            if(isset($request->regualarSlip) && $request->regualarSlip =='Yes')
            {

                $data['show_history'] =array_chunk(json_decode(json_encode($show_history), true),2);
            }
            else
            {
                $show_history_final =json_decode(json_encode($show_history), true);
                // dd($show_history_final);
                $data['LayerArray'] = explode(',',$request->LayerArray);
                $data['StickerNumber'] = explode(',',$request->StickerNumber);
    
                $max_sticker_num = max($data['StickerNumber']);
    
                $new_show_history = array();
    
                $var_main_length = 0;
                
                for ($i=1; $i <= $max_sticker_num ; $i++) { 
                    // echo "<br>";
                    if(in_array($i,$data['StickerNumber'])){
    
                        $new_show_history[] = $show_history_final[$var_main_length];
                        $var_main_length++;
                    }else{
                        // echo(0);
                        $new_show_history[] = array();
                    }
                }
    
                $data['show_history'] =array_chunk($new_show_history,2);
            }
           
            if(isset($request->AvilableSlip) && $request->AvilableSlip=='Yes')
            {
                return count($show_history);
            }
            else
            {
                // return view('admin.billing.print_driver_slip',$data);
                $customPaper = array(0,0,800.929133858,612.464566929);
                $pdf = PDF::loadView('admin.billing.print_driver_slip', $data)->setPaper($customPaper, 'landscape');
                return $pdf->stream("driver_billing.pdf", array("Attachment" => false));
            }
        }
        else
        {
            if(isset($request->AvilableSlip) && $request->AvilableSlip=='Yes')
            {
                return 0;
            }
            else
            {
                return redirect()->back()->withError('No record found'); 
            }
        }
       
    }
}
