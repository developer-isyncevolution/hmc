<?php

namespace App\Http\Controllers\admin\user_lab;

use App\Http\Controllers\Controller;
use App\Models\admin\user_lab\UserLab;
use App\Models\admin\office\Office;
use App\Models\admin\doctor\Doctor;
use App\Models\admin\staff\Staff;
use Illuminate\Http\Request;
use App\Libraries\Paginator;
use Validator;
use Session;
use App\Libraries\General;
use Image;

class UserLabController extends Controller
{
    public function index($iCustomerId = "")
    {   
        $per_page = Session::forget('per_page_lan');
        if (!empty($iCustomerId)) {
            $data['iCustomerId'] = $iCustomerId;
         }
        $data['doctor'] = UserLab::get_all_doctor();
        $data['customer'] = UserLab::get_all_customer();
        $data['office'] = UserLab::get_all_office();
        return view('admin.user_lab.listing')->with($data);
    }   
    public function officeuser($iOfficeId = "")
    {   
        if (!empty($iOfficeId)) {
            $data['iOfficeId'] = $iOfficeId;
        }
        $data['doctor'] = UserLab::get_all_doctor();
        $data['customer'] = UserLab::get_all_customer();
        $data['office'] = UserLab::get_all_office();
        return view('admin.user_lab.listing')->with($data);
    }   
    public function ajax_listing(Request $request)
    {
        $action = $request->action;
        $iDoctorId = $request->iDoctorId;
        if(isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $iCustomerId = $request->iCustomerId;
        }
        else
        {
            $iCustomerId = General::admin_info()['iCustomerId'];;
        }
        $iOfficeId = $request->iOfficeId;
        
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iUserId";
            $order = "ASC";
        }
        
        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }
        
        if($action == "delete"){
            $where                 = array();
            $where['iUserId']    = $request->id;
            
            UserLab::delete_by_id($where);
        }
        
        if($action == "multiple_delete"){
            $User_ID = (explode(",",$request->id));
            
            foreach ($User_ID as $key => $value) {
                $where                 = array();
                $where['iUserId']    = $value;
                
                UserLab::delete_by_id($where);
            }
        }
        
        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['iDoctorId'] = $iDoctorId;
        $criteria['iCustomerId'] = $iCustomerId;
        $criteria['iOfficeId'] = $iOfficeId;
        $Admin_data = UserLab::get_all_data($criteria);
        $pages = 1;  
        $data['doctor'] = UserLab::get_all_doctor();
        $data['customer'] = UserLab::get_all_customer();
        $data['office'] = UserLab::get_all_office();
        
        if($request->pages != "")
        {
            $pages = $request->pages;
        }
        
        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);
        
      
        
        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }
        
        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;
        
        $paginator->is_ajax = true;
        $paging = true;
        
        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iDoctorId'] = $iDoctorId;
        // $criteria['iCustomerId'] = $iCustomerId;
        $criteria['iOfficeId'] = $iOfficeId;
                
        $data['data'] = UserLab::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        return view('admin.user_lab.ajax_listing')->with($data);   
    }
    
    public function create()
    {
        $data['doctor'] = UserLab::get_all_doctor();
        $data['customer'] = UserLab::get_all_customer();
        $data['office'] = UserLab::get_all_office();
        return view('admin.user_lab.create')->with($data);
    }

    public function store(Request $request){

        $iUserId = $request->id;
        $image = $request->file('vImage');
        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/lab_office');
            $img            = Image::make($image->getRealPath()); 
            $img->resize(50, 50, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.'/'.$imageName);
            $request->vImage->move($path, $imageName);
            $data['vImage']    = $imageName;
        }
        $data['vFirstName']   = $request->vFirstName;
        $data['vLastName']    = $request->vLastName;
        $data['vUserName']    = $request->vUserName;
        // $data['iDoctorId']    = null;
        $data['iCustomerId']  = $request->iCustomerId;
        $data['iOfficeId']    = $request->iOfficeId;
        $data['vEmail']       = $request->vEmail;
        $data['vMobile']      = $request->vMobile;
        $data['vTitle']       = $request->vTitle;
        $data['tDescription'] = $request->tDescription;
        $data['vPassword']    = md5($request->vPassword);   
        $data['eStatus']      = $request->eStatus;

        if($request->iCustomerId)
        {
            $data['iCustomerId']    = $request->iCustomerId;
        }else{
               // For Perticular customer show start
               $data['iCustomerId'] = General::admin_info()['iCustomerId'];
               // For Perticular customer show end
        }
        if($request->iOfficeId)
        {
            $data['iOfficeId']    = $request->iOfficeId;
        }else{
            
            $data['iOfficeId']    = NULL;
        }
        if($request->iDoctorId)
        {
            $data['iDoctorId']    = $request->iDoctorId;
        }else{
            
            $data['iDoctorId']    = NULL;
        }
        if($iUserId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iUserId)){
            $where               = array();
            $where['iUserId']         = $iUserId;

            $User_ID = new UserLab();
            $User_ID->update($where, $data);
            // Update in staff table
            if(isset($iUserId) && !empty($iUserId))
            {
                if(isset($request->iCustomerId) && !empty($request->iCustomerId))
                {
                    $iCustomerId = $request->iCustomerId;
                }
                else
                {
                    $iCustomerId = General::admin_info()['iCustomerId'];
                }
                // Check this user exist in lab admin table 
                $exist_labAdmin = UserLab::user_exit_in_staff($iUserId,$iCustomerId);
                if($exist_labAdmin>0)
                {
                    $labData = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        'vUserName'=>$request->vUserName,
                        'vEmail'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $whereLabData = array(
                        'iLoginAdminId'=>$iUserId,
                        'iCustomerId'=>$iCustomerId
                    );
                    UserLab::update_staff_data($labData,$whereLabData);
                }
            }
            return redirect()->route('admin.user_lab')->withSuccess('User updated successfully.');
        }
        else{
            if(isset($request->iCustomerId) && !empty($request->iCustomerId))
            {
                $iCustomerId = $request->iCustomerId;
            }
            else
            {
                $iCustomerId = General::admin_info()['iCustomerId'];
            }
            $iLoginAdminId = UserLab::add($data);
            // Add doctor into staff
            $data_staff['iDepartmentId']= 5;
            $data_staff['vFirstName']= $request->vFirstName;
            $data_staff['vLastName']= $request->vLastName;
            $data_staff['vUserName']= $request->vUserName;
            $data_staff['vEmail']= $request->vEmail;
            $data_staff['vPassword']= md5($request->vPassword);
            $data_staff['vMobile']= $request->vMobile;
            $data_staff['eStatus']= $request->eStatus;
            $data_staff['iLoginAdminId']= $iLoginAdminId;
            $data_staff['iCustomerId'] = $iCustomerId;
            $data_staff['dtAddedDate'] = date("Y-m-d H:i:s");
            Staff::add($data_staff);   
            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            return redirect()->route('admin.user_lab')->withSuccess('User created successfully.');
        }
    }

    public function edit($iUserId)
    {
        $data['doctor'] = UserLab::get_all_doctor();
        $data['customer'] = UserLab::get_all_customer();
        $data['office'] = UserLab::get_all_office();
        $data['users'] = UserLab::get_by_id($iUserId);

        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['users']->iCustomerId) && $data['users']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['users']->iCustomerId)))
        {
            return redirect()->route('admin.user_lab')->withError('Invalid Input');
        }

        return view('admin.user_lab.create')->with($data);
    }
    public function fetch_office(Request $request)
    {
        $iCustomerId = $request->iCustomerId;
         
        $data['data'] = Office::where("iCustomerId",$iCustomerId)
                                ->orderBy("iOfficeId", "desc")
                                ->get();
        return view('admin.user_lab.ajax_fetch_office_listing')->with($data);
    }
    public function fetch_doctor(Request $request)
    {
        $iOfficeId = $request->iOfficeId;
         
        $data['data'] = Doctor::where("iOfficeId",$iOfficeId)
                                ->orderBy("iOfficeId", "desc")
                                ->get();
        return view('admin.user_lab.ajax_fetch_doctor_listing')->with($data);
    }
    public function userlist($iCustomerId,$iOfficeId,$iDoctorId)
    {   
        $data['iCustomerId'] = $iCustomerId;
        $data['iOfficeId'] = $iOfficeId;
        $data['iDoctorId'] = $iDoctorId;
        $data['doctor'] = UserLab::get_all_doctor();
        return view('admin.user_lab.user_listing')->with($data);
    }
    
    public function user_ajax_listing(Request $request)
    {
        $action = $request->action;
        $iCustomerId = $request->iCustomerId;
        $iOfficeId = $request->iOfficeId;
        $iDoctorId = $request->iDoctorId;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iUserId";
            $order = "DESC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iUserId']    = $request->id;

            UserLab::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $User_ID = (explode(",",$request->id));

            foreach ($User_ID as $key => $value) {
                $where                 = array();
                $where['iUserId']    = $value;

                UserLab::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $Admin_data = UserLab::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limits_page !='')
         {
             $per_page = $request->limits_page;
             Session::put('per_page_lan', $per_page);
             $paginator->itemsPerPage = $per_page;
             //$paginator->range = $per_page;
         }
         else
         {
             $per_page = Session::get('per_page_lan');
             if($per_page !='')
             {
                 $paginator->itemsPerPage = $per_page;
                 //$paginator->range = $per_page;
             }
             else
             {
                 $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
             }             
         }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iDoctorId'] = $iDoctorId;
        $criteria['iCustomerId'] = $iCustomerId;
        $criteria['iOfficeId'] = $iOfficeId;


        $data['data'] = UserLab::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        
        return view('admin.user_lab.user_ajax_listing')->with($data);   
    }

    public function usercreate($iCustomerId,$iOfficeId,$iDoctorId)
    {
        $data['iCustomerId'] = $iCustomerId;
        $data['iOfficeId']   = $iOfficeId;
        $data['iDoctorId']   = $iDoctorId;
        $data['doctor']      = UserLab::get_all_doctor();
        
        return view('admin.user_lab.user_create')->with($data);
    }

    public function userstore(Request $request){

        $iUserId = $request->id;
        $iCustomerId = $request->iCustomerId;
        $iOfficeId =$request->iOfficeId;
        $iDoctorId = $request->iDoctorId;

        $data['vFirstName']   = $request->vFirstName;
        $data['vLastName']    = $request->vLastName;
        $data['vUserName']    = $request->vUserName;
        $data['iCustomerId']  = $request->iCustomerId;
        $data['iOfficeId']    = $request->iOfficeId;
        $data['iDoctorId']    = $request->iDoctorId;
        $data['vEmail']       = $request->vEmail;
        $data['vMobile']      = $request->vMobile;
        $data['vTitle']       = $request->vTitle;
        $data['tDescription'] = $request->tDescription;
        $data['vPassword']    = md5($request->vPassword);
        $data['eStatus']      = $request->eStatus;


        if($iUserId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iUserId)){
            $where               = array();
            $where['iUserId']         = $iUserId;

            $User_ID = new User();
            $User_ID->update($where, $data);

            return redirect()->route('admin.user_lablist',['iCustomerId'=>$iCustomerId,'iOfficeId'=>$iOfficeId,'iDoctorId'=>$iDoctorId])->withSuccess('User updated successfully.');
        }
        else{
            $data = UserLab::add($data);

            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            return redirect()->route('admin.user_lablist',[$iCustomerId,$iOfficeId,$iDoctorId])->withSuccess('User created successfully.');
        }
    }

    public function useredit($iCustomerId,$iOfficeId,$iDoctorId,$iUserId)
    {
        $data['iCustomerId'] = $iCustomerId;
        $data['iOfficeId']   = $iOfficeId;
        $data['iDoctorId']   = $iDoctorId;
        $data['doctor']      = UserLab::get_all_doctor();
        $data['users']       = UserLab::get_by_id($iUserId);
        return view('admin.user_lab.user_create')->with($data);
    }
    public function generatepassword()
    {
        $number = mt_rand(1000000000, 9999999999);    
        return $number;
        // $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        // // $number = substr(str_shuffle($permitted_chars), 0, 6);
        // return substr(str_shuffle($permitted_chars), 0, 6);
        // $str_result = 'mt_rand(0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz)';  
        
        // $number = substr(str_shuffle($str_result),0,6);
    }
    public function showChangePasswordForm($iUserId)
    {
        $data['users'] = UserLab::get_by_id($iUserId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['users']->iCustomerId) && $data['users']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['users']->iCustomerId)))
        {
            return redirect()->route('admin.user_lab')->withError('Invalid Input');
        }
        return view('admin.user_lab.showChangePasswordForm')->with($data);
    }
    
    public function changePassword(Request $request)
    {
        $iUserId = $request->id;
        $where                     = array();
        $data['vPassword']        = md5($request->vPassword);
        $where['iUserId']     = $iUserId;

        $User_id = new User();
        $User_id->update($where, $data);
        if(isset($iUserId) && !empty($iUserId) && isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $iLabAdminId = $iUserId;
            $iCustomerId = $request->iCustomerId;
            // Check this user exist in lab admin table 
            $exist_doctor = Doctor::user_exit_in_staff($iUserId,$iCustomerId);
            
            if($exist_doctor>0)
            {
                $labData = array(
                    'vPassword'=>md5($request->vPassword),
                    'dtUpdatedDate'=>date('Y-m-d H:i:s')
                );
                $whereLabData = array(
                    'iLoginAdminId'=>$iUserId,
                    'iCustomerId'=>$iCustomerId
                );
                Staff::update_staff_data($labData,$whereLabData);
            }
            
        }
        return redirect()->back()->with("success","Password changed successfully !");

    }
}
