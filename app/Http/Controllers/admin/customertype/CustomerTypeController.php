<?php

namespace App\Http\Controllers\admin\customertype;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\customertype\CustomerType;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Session;

class CustomerTypeController extends Controller
{
    public function index()
    {
        return view('admin.customertype.listing');
    }

    public function ajax_listing(Request $request)
    {

        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iCustomerTypeId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iCustomerTypeId']    = $request->id;

            CustomerType::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $CustomerType_ID = (explode(",",$request->id));

            foreach ($CustomerType_ID as $key => $value) {
                $where                 = array();
                $where['iCustomerTypeId']    = $value;

                CustomerType::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $CustomerType_data = CustomerType::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($CustomerType_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = CustomerType::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        
        return view('admin.customertype.ajax_listing')->with($data);   
    }

    public function create()
    {
        return view('admin.customertype.create');
    }

   public function store(Request $request)
    {
        $iCustomerTypeId = $request->id;

        $data['vTypeName']     = $request->vTypeName;
        $data['eStatus']       = $request->eStatus;

        if($iCustomerTypeId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iCustomerTypeId)){
            $where                      = array();
            $where['iCustomerTypeId']       = $iCustomerTypeId;

            $CustomerType_id = new CustomerType();
            $CustomerType_id->update($where, $data);

            return redirect()->route('admin.customertype')->withSuccess('CustomerType updated successfully.');
        }
        else{
            $CustomerType_id = CustomerType::add($data);

            return redirect()->route('admin.customertype')->withSuccess('CustomerType created successfully.');
        }
    }

    public function edit($iCustomerTypeId)
    {
        $data['customertypes'] = CustomerType::get_by_id($iCustomerTypeId);

        return view('admin.customertype.create')->with($data);
    }

    public function destroy($id)
    {
        $CustomerType = CustomerType::find($id);
        $CustomerType->delete();

        return redirect()->route('admin.customertype')->withSuccess('CustomerType deleted successfully.');
    }

    public function multiDelete(Request $request)
    {
        $id = $request->id;

        CustomerType::whereIn('id',explode(",",$id))->delete();
    }
}
