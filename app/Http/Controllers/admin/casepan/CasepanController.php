<?php

namespace App\Http\Controllers\admin\casepan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\casepan\Casepan;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use App\Libraries\General;
use Session;

class CasepanController extends Controller
{
    public function index()
    {
        // $sessionData                = session('data');
        // $iRoleId                    = $sessionData['iRoleId'];
        // $url                        = request()->segment(2);
        // $data['permission']         = General::get_module_permission($iRoleId, $url);
        return view('admin.casepan.listing');
    }

    public function ajax_listing(Request $request)
    {

        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iCasepanId']    = $request->id;

            Casepan::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Casepan_ID = (explode(",",$request->id));

            foreach ($Casepan_ID as $key => $value) {
                $where                 = array();
                $where['iCasepanId']    = $value;

                Casepan::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $Casepan_data = Casepan::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Casepan_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = Casepan::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        
        return view('admin.casepan.ajax_listing')->with($data);   
    }

    public function create()
    {
        return view('admin.casepan.create');
    }

    public function store(Request $request)
    {
        $iCasepanId = $request->id;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $data['vName']     = $request->vName;
        $data['iQuantity'] = $request->iQuantity;
        $data['iSequence'] = $request->iSequence;
        $data['eStatus']   = $request->eStatus;
        if(!empty($request->vColor)){
            $data['vColor']  = $request->vColor;
        }else{

            $data['vColor']  = Null;
        }
        
        if($iCasepanId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }   

        if(!empty($iCasepanId)){
            $where                      = array();
            $where['iCasepanId']       = $iCasepanId;

            $Casepan_id = new Casepan();
            $Casepan_id->update($where, $data);
           
            return redirect()->route('admin.casepan')->withSuccess('Casepan updated successfully.');
            
        }
        else{
            $Casepan_id = Casepan::add($data);
            return redirect()->route('admin.casepan')->withSuccess('Casepan created successfully.');
            
        }
    }

    public function store_model_casepan(Request $request)
    {
       
        $iCasepanId = $request->id;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $data['vName']     = $request->vName;
        $data['iQuantity'] = $request->iQuantity;
        $data['iSequence'] = $request->iSequence;
        $data['eStatus']   = $request->eStatus;
        if(!empty($request->vColor)){
            $data['vColor']  = $request->vColor;
        }else{

            $data['vColor']  = Null;
        }
        
        if($iCasepanId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }   

        if(!empty($iCasepanId)){
            $where                      = array();
            $where['iCasepanId']       = $iCasepanId;

            $Casepan_id = new Casepan();
            $Casepan_id->update($where, $data);

            return redirect()->back()->withSuccess('Casepan updated successfully.');
        }
        else{
            $Casepan_id = Casepan::add($data);

            return redirect()->back()->withSuccess('Casepan created successfully.');
        }
    }
    public function edit($iCasepanId)
    {
        $data['casepans'] = Casepan::get_by_id($iCasepanId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['casepans']->iCustomerId) && $data['casepans']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['casepans']->iCustomerId)))
        {
            return redirect()->route('admin.casepan')->withError('Invalid Input');
        }

        return view('admin.casepan.create')->with($data);
    }

    public function destroy($id)
    {
        $Casepan = Casepan::find($id);
        $Casepan->delete();

        return redirect()->route('admin.casepan')->withSuccess('Casepan deleted successfully.');
    }

    public function multiDelete(Request $request)
    {
        $id = $request->id;

        Casepan::whereIn('id',explode(",",$id))->delete();
    }
}
