<?php

namespace App\Http\Controllers\admin\casepannumber;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\casepannumber\CasepanNumber;
use App\Models\admin\casepan\Casepan;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;

class CasepanNumberController extends Controller
{
    public function index($iCasepanId = "")
    {
        Session::forget('per_page_lan');
        Session::forget('criteria');
        if (!empty($iCasepanId)) {
            $data['iCasepanId'] = $iCasepanId;
         }
        $data['casepan'] = Casepan::get_all_casepan();
        return view('admin.casepannumber.listing')->with($data);
    }
    
    public function ajax_listing(Request $request)
    {
        $action = $request->action;
        $iCasepanId = $request->iCasepanId;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iCasepanNumberId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iCasepanNumberId']    = $request->id;

            CasepanNumber::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $CasepanNumber_ID = (explode(",",$request->id));

            foreach ($CasepanNumber_ID as $key => $value) {
                $where                 = array();
                $where['iCasepanNumberId']    = $value;

                CasepanNumber::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['iCasepanId'] = $iCasepanId;

        $CasepanNumber_data = CasepanNumber::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($CasepanNumber_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        if($request->limit_page !='')
        {
            $limit = $request->limit_page;
        }
        else
        {
            $limit = $paginator->itemsPerPage;
        }

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = CasepanNumber::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        
        return view('admin.casepannumber.ajax_listing')->with($data);   
    }

    public function create($iCasepanId = "")
    {
        if (!empty($iCasepanId)) {
            $data['iCasepanId'] = $iCasepanId;
         }
        $data['casepan'] = Casepan::get_all_casepan();
        return view('admin.casepannumber.create')->with($data);
    }

   public function store(Request $request)
    {
        $iCasepanNumberId = $request->id;
        $iCasepanId = $request->iCasepanId;

        $data['iCasepanId'] = $iCasepanId;
        $data['vNumber']    = $request->vNumber;
        $data['eStatus']    = $request->eStatus;
        $data['eUse']    = $request->eUse;
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        if($iCasepanNumberId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iCasepanNumberId)){
            $where                      = array();
            $where['iCasepanNumberId']       = $iCasepanNumberId;

            $CasepanNumber_id = new CasepanNumber();
            $CasepanNumber_id->update($where, $data);

            if(isset($request->Action_back) && $request->Action_back == 'Yes')
            {
                return redirect()->back()->withSuccess('Casepan Number updated successfully.');
            }
            else
            {
                return redirect()->route('admin.casepannumber',$iCasepanId)->withSuccess('Casepan Number updated successfully.');
            }
        }
        else{
            $CasepanNumber_id = CasepanNumber::add($data);
            if(isset($request->Action_back) && $request->Action_back == 'Yes')
            {
                return redirect()->back()->withSuccess('Casepan Number created successfully.');
            }
            else
            {
                return redirect()->route('admin.casepannumber',$iCasepanId)->withSuccess('Casepan Number created successfully.');
            }    
        }
    }

    public function edit($iCasepanId,$iCasepanNumberId)
    {
        if (!empty($iCasepanId)) {
            $data['iCasepanId'] = $iCasepanId;
         }
        $data['casepannumbers'] = CasepanNumber::get_by_id($iCasepanNumberId);

        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['casepannumbers']->iCustomerId) && $data['casepannumbers']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['casepannumbers']->iCustomerId)))
        {
            return redirect()->route('admin.casepan')->withError('Invalid Input');
        }
        $data['casepan'] = Casepan::get_all_casepan();
        return view('admin.casepannumber.create')->with($data);
    }

    public function destroy($id)
    {
        $CasepanNumber = CasepanNumber::find($id);
        $CasepanNumber->delete();

        return redirect()->route('admin.casepannumber')->withSuccess('Casepan Number deleted successfully.');
    }

    public function multiDelete(Request $request)
    {
        $id = $request->id;

        CasepanNumber::whereIn('id',explode(",",$id))->delete();
    }
}
