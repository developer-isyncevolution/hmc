<?php

namespace App\Http\Controllers\admin\addoncategory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\Paginator;
use App\Models\admin\addoncategory\Addoncategory;
use Session;
use App\Libraries\General;

class AddoncategoryController extends Controller
{
    //
    public function index()
    {
        return view('admin.addoncategory.listing');
    }
    public function ajax_listing(Request $request)
    {

        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "vSequenc";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->upperkeyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iAddCategoryId']    = $request->id;

            Addoncategory::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Grade_ID = (explode(",",$request->id));

            foreach ($Grade_ID as $key => $value) {
                $where                 = array();
                $where['iAddCategoryId']    = $value;

                Addoncategory::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eType']      = "Upper";
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $Addoncategory_data = Addoncategory::get_all_data($criteria);

        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Addoncategory_data);
        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->upper_limit_page !='')
        {
            $per_page = $request->upper_limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['eType'] = 'Upper';

        $data['data'] = Addoncategory::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        // dd($start);
        $data['paging'] = str_replace('class="ajax_page','class="ajax_page_upper',$paginator->paginate());

        return view('admin.addoncategory.ajax_listing')->with($data);   
    }
    public function lower_ajax_listing(Request $request)
    {
        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "vSequenc";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->lower_keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iAddCategoryId']    = $request->id;

            Addoncategory::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Grade_ID = (explode(",",$request->id));

            foreach ($Grade_ID as $key => $value) {
                $where                 = array();
                $where['iAddCategoryId']    = $value;

                Addoncategory::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eType'] = 'Lower';
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $Addoncategory_data = Addoncategory::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Addoncategory_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->lower_limit_page !='')
        {
            $per_page = $request->lower_limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = Addoncategory::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = str_replace('class="ajax_page','class="ajax_page_lower',$paginator->paginate());
        
        return view('admin.addoncategory.lower_ajax_listing')->with($data);  
    }
    public function create()
    {
        return view('admin.addoncategory.create');
    }

   public function store(Request $request)
    {
        // dd($request);
        $iAddCategoryId             = $request->iAddCategoryId;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $data['vCategoryName']      = $request->vCategoryName;
        $data['vSequenc']           = $request->vSequenc;
        $data['eStatus']            = $request->eStatus;
        $data['eType']              = $request->eType;
        if($iAddCategoryId){
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        if(!empty($iAddCategoryId)){
            $where                      = array();
            $where['iAddCategoryId']       = $iAddCategoryId;

            $Grade_id = new Addoncategory();
            $Grade_id->update($where, $data);

            return redirect()->route('admin.addoncategory')->withSuccess('Addon Category updated successfully.');
        }
        else{
            $Grade_id = Addoncategory::add($data);
            return redirect()->route('admin.addoncategory')->withSuccess('Addon Category created successfully.');
        }
    }

    public function edit($iAddCategoryId)
    {
        $data['addon'] = Addoncategory::get_by_id($iAddCategoryId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['addon']->iCustomerId) && $data['addon']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['addon']->iCustomerId)))
        {
            return redirect()->route('admin.category')->withError('Invalid Input');
        }
        return view('admin.addoncategory.create')->with($data);
    }
}
