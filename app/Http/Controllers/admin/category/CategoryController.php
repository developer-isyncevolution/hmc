<?php

namespace App\Http\Controllers\admin\category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\category\Category;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;

class CategoryController extends Controller
{
    public function index()
    {
        return view('admin.category.listing');
    }
    

    public function ajax_listing(Request $request)
    {
        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->upperkeyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iCategoryId']    = $request->id;

            Category::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Category_ID = (explode(",",$request->id));

            foreach ($Category_ID as $key => $value) {
                $where                 = array();
                $where['iCategoryId']    = $value;

                Category::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eType'] = 'Upper';
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $Category_data = Category::get_all_data($criteria);
       
        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Category_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->upper_limit_page !='')
        {
            $per_page = $request->upper_limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['eType'] = 'Upper';

        $data['data'] = 'Upper';
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $data['data'] = Category::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = str_replace('class="ajax_page','class="ajax_page_upper',$paginator->paginate());
        
        return view('admin.category.ajax_listing')->with($data);   
    }
    public function lower_ajax_listing(Request $request)
    {
        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->lower_keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iCategoryId']    = $request->id;

            Category::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Category_ID = (explode(",",$request->id));

            foreach ($Category_ID as $key => $value) {
                $where                 = array();
                $where['iCategoryId']    = $value;

                Category::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eType'] = 'Lower';
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $Category_data = Category::get_all_data($criteria);

        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Category_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->lower_limit_page !='')
        {
            $per_page = $request->lower_limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;

            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['eType'] = 'Lower';
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $data['data'] = Category::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = str_replace('class="ajax_page','class="ajax_page_lower',$paginator->paginate());
        
        return view('admin.category.lower_ajax_listing')->with($data);   
    }
    
    public function create()
    {
        $data['casepan'] = Category::get_all_casepan();        
        return view('admin.category.create')->with($data);
    }

   public function store(Request $request)
    {
        $iCategoryId = $request->id;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $data['tPickTime'] 	= $request->tPickTime;
        $data['tDeliveryTime'] 	= $request->tDeliveryTime;
        $data['vName']      = $request->vName;
        $data['iCasepanId'] = $request->iCasepanId;
        $data['iSequence']  = $request->iSequence;
        $data['eType']      = $request->eType;
        $data['eStatus']    = $request->eStatus;
                
        if($iCategoryId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iCategoryId)){
            $where                      = array();
            $where['iCategoryId']       = $iCategoryId;

            $Category_id = new Category();
            $Category_id->update($where, $data);

            return redirect()->route('admin.category')->withSuccess('Category updated successfully.');
        }
        else{
            $Category_id = Category::add($data);

            return redirect()->route('admin.category')->withSuccess('Category created successfully.');
        }
    }

    public function edit($iCategoryId)
    {
        $data['casepan'] = Category::get_all_casepan();
        $data['categorys'] = Category::get_by_id($iCategoryId);

        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['categorys']->iCustomerId) && $data['categorys']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['categorys']->iCustomerId)))
        {
            return redirect()->route('admin.category')->withError('Invalid Input');
        }

        return view('admin.category.create')->with($data);
    }

    public function destroy($id)
    {
        $Category = Category::find($id);
        $Category->delete();

        return redirect()->route('admin.category')->withSuccess('Category deleted successfully.');
    }

    public function multiDelete(Request $request)
    {
        $id = $request->id;

        Category::whereIn('id',explode(",",$id))->delete();
    }

    public function notification(Request $request)
    {
        
    }
}
