<?php

namespace App\Http\Controllers\admin\moduleMaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\moduleMaster\ModuleMaster;
use App\Libraries\Paginator;
use Session;
class ModuleMasterController extends Controller
{
    public function index()
    {
        Session::forget('per_page_lan');
        return view('admin.module_master.listing');
    }

    public function ajax_listing(Request $request)
    {
        $action = $request->action;
        
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iModuleId";
            $order = "DESC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){

            $where                 = array();
            $where['iModuleId']    = $request->id;
            ModuleMaster::delete_by_id($where); 
        }

        if($action == "multiple_delete"){
            $iModuleId = (explode(",",$request->iModuleId));

            foreach ($iModuleId as $key => $value) {
                $where                 = array();
                $where['iModuleId']    = $value;

                ModuleMaster::delete_by_id($where);  
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $banner_data = ModuleMaster::all();

        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($banner_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        if($request->limit_page !='')
        {
            $limit = $request->limit_page;
        }
        else
        {
            $limit = $paginator->itemsPerPage;
        }
    
        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }
      
        
        $paginator->is_ajax = true;
        $paging = true;
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        // dd($limit);
        $data['data'] = ModuleMaster::get_all_data($criteria, $start, $limit, $paging);
        // dd($data);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();

        return view('admin.module_master.ajax_listing')->with($data);  
    }

    public function create()
    {  
        return view('admin.module_master.create');
    }

    public function store(Request $request)
    {
        $iModuleId             = $request->id;

        $data['vTitle']        = $request->vTitle;
        $data['vModuleName']   = $request->vModuleName;
        $data['eStatus']       = $request->eStatus;
       
        if(!empty($iModuleId)){
            $where                  = array();
            $where['iModuleId']     = $iModuleId;
           
            $data['dtUpdatedDate']  = date("Y-m-d H:i:s"); 
            ModuleMaster::update_data($where, $data);

            return redirect()->route('admin.moduleMaster')->withSuccess('Module Master updated successfully.');
        }
        else{
            $data['dtAddedDate']    = date("Y-m-d H:i:s");
            $insertedId             = ModuleMaster::add($data);      
            
            return redirect()->route('admin.moduleMaster')->withSuccess('Module Master created successfully.');
        }
    }

    public function edit($iModuleId)
    {
        $data['data'] = ModuleMaster::get_by_id($iModuleId);

        return view('admin.module_master.create')->with($data);
    }  
}
