<?php

namespace App\Http\Controllers\admin\office_admin;

use App\Http\Controllers\Controller;
use App\Models\admin\office_admin\OfficeAdmin;
use App\Models\admin\customer\Customer;
use App\Models\admin\staff\Staff;
use Illuminate\Http\Request;
use App\Libraries\Paginator;
use Validator;
use Session;
use App\Libraries\General;
use Image;

class OfficeAdminController extends Controller
{
    public function index($iCustomerId = "")
    { 
        $per_page = Session::forget('per_page_lan');
        $data['lab_data'] = OfficeAdmin::get_all_data($iCustomerId); 
        $data['customer'] = Customer::get_all_customer(); 
        return view('admin.office_admin.listing')->with($data);
    }
    
    public function ajax_listing(Request $request)
    {
        
        $action = $request->action;
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iOfficeAdminId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iOfficeAdminId']    = $request->id;
            OfficeAdmin::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Office_ID = (explode(",",$request->id));

            foreach ($Office_ID as $key => $value) {
                $where                 = array();
                $where['iOfficeAdminId']    = $value;

                OfficeAdmin::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        if(isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $criteria['iCustomerId'] = $request->iCustomerId;
        }
        else
        {
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        }
        // dd($criteria);

        $Admin_data = OfficeAdmin::get_all_data($criteria);
        

        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

     

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }
        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;
        
        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
   
        $data['data'] = OfficeAdmin::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        // dd($data);
        return view('admin.office_admin.ajax_listing')->with($data);   
    }

    public function create()
    {
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        $data['customer'] = Customer::get_all_customer($criteria);
        return view('admin.office_admin.create',$data);
    }

    public function create_staff($iCustomerId='')
    {
        $data['customer'] = Customer::get_all_customer();
        $data['iCustomerId_New'] = $iCustomerId;
        return view('admin.office_admin.create',$data);
    }
    
    public function store(Request $request){
        $iOfficeAdminId = $request->iOfficeAdminId;
        if($request->iCustomerId)
        {
            $iCustomerId    = $request->iCustomerId;
        }else{
               // For Perticular customer show start
            $iCustomerId = General::admin_info()['iCustomerId'];
               // For Perticular customer show end
        }
        $image = $request->file('vImage');
        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/lab_office');
            $img            = Image::make($image->getRealPath()); 
            $img->resize(50, 50, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.'/'.$imageName);
            $request->vImage->move($path, $imageName);
            $data['vImage']    = $imageName;
        }
        $data['vFirstName']      = $request->vFirstName;
        $data['vLastName']      = $request->vLastName;
        $data['vEmail']    = $request->vEmail;
        $data['vMobile']    = $request->vMobile;
        $data['vCellulor']  = isset($request->vCellulor)?$request->vCellulor:'';
        $data['eStatus']       = $request->eStatus;
        $data['iCustomerId']       = $iCustomerId;
        $data['vPassword']    = md5($request->vPassword);

        if($iOfficeAdminId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        
        if(!empty($iOfficeAdminId)){
            $where               = array();
            $where['iOfficeAdminId']         = $iOfficeAdminId;

            $Doctor_ID = new OfficeAdmin();
            $Doctor_ID->update($where, $data);
            // Update in staff table
            if(isset($request->iOfficeAdminId) && !empty($request->iOfficeAdminId) && isset($iCustomerId) && !empty($iCustomerId))
            {

                $iOfficeAdminId = $request->iOfficeAdminId;
                // Check this user exist in lab admin table 
                // Check this user exist in lab admin table 
                $where_staff = [
                    'iLoginAdminId'=>$iOfficeAdminId,
                    'iCustomerId'=>$iCustomerId
                ];
                $exist_staff = Staff::where($where_staff)->count();
                
                if($exist_staff>0)
                {
                    $labData = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        'vEmail'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $where_staff = [
                        'iLoginAdminId'=>$iOfficeAdminId,
                        'iCustomerId'=>$iCustomerId
                    ];
                    Staff::where($where_staff)->update($labData);
                }
                
            }
            return redirect()->route('admin.office_admin')->withSuccess('Lab Admin updated successfully.');
        }
        else{
            $iLoginAdminId = OfficeAdmin::add($data);
            $data_staff['iDepartmentId']    = 3;
            $data_staff['vLastName']        = $request->vFirstName;
            $data_staff['vFirstName']       = $request->vLastName;
            $data_staff['vEmail']           = $request->vEmail;
            $data_staff['vPassword']        = md5($request->vPassword);
            $data_staff['vMobile']          = $request->vMobile;
            $data_staff['eStatus']          = $request->eStatus;
            $data_staff['iLoginAdminId']    = $iLoginAdminId;
            $data_staff['dtAddedDate']      = date("Y-m-d H:i:s");
            $data_staff['iCustomerId']      = $iCustomerId;
            Staff::add($data_staff);
            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            return redirect()->route('admin.office_admin')->withSuccess('Lab Admin created successfully.');
        }
    }

    public function edit($iOfficeAdminId)
    {
        $data['lab_data'] = OfficeAdmin::get_by_id($iOfficeAdminId);
        $data['customer'] = Customer::get_all_customer();

        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['lab_data']->iCustomerId) && $data['lab_data']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['lab_data']->iCustomerId)))
        {
            return redirect()->route('admin.office_admin')->withError('Invalid Input');
        }

        return view('admin.office_admin.create')->with($data);
    }

    public function destroy($id)
    {
        $Grade = OfficeAdmin::find($id);
        $Grade->delete();

        return redirect()->route('admin.grade')->withSuccess('Lab Admin deleted successfully.');
    }

    public function own_listing($iCustomerId)
    {
        $data['lab_data'] = OfficeAdmin::get_all_data(); 
        $data['iCustomerId'] = $iCustomerId; 
        $data['customer'] = Customer::get_all_customer(); 
        return view('admin.office_admin.listing')->with($data);
    }
}
