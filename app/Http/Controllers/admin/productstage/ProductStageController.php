<?php

namespace App\Http\Controllers\admin\productstage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\productstage\ProductStage;
use App\Models\admin\categoryproduct\CategoryProduct;
use App\Models\admin\office_product_price\OfficeProductPrice;
use App\Models\admin\office_stage_price\OfficeStagePrice;
use App\Models\admin\stage_grade\StageGrade;
use App\Models\admin\category\Category;
use App\Models\admin\labOffice\LabOffice;
use App\Models\admin\customer\Customer;
use App\Models\admin\productgrade\ProductGrade;
use App\Models\admin\grade\Grade;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;

class ProductStageController extends Controller
{
    public function index()
    {
        $criteria['eType'] = 'Upper';
        $data['uppercategory'] = CategoryProduct::get_all_category($criteria);
        
        $criteria['eType'] = 'Lower';
        $data['lowercategory'] = CategoryProduct::get_all_category($criteria);

        $criteria['eType'] = 'Upper';
        $data['upperproduct'] = CategoryProduct::get_all_product($criteria);
        
        $criteria['eType'] = 'Lower';
        $data['lowerproduct'] = CategoryProduct::get_all_product($criteria);

        return view('admin.productstage.listing')->with($data);
    }
    public function productstageselect($iCategoryProductId)
    {
        $data['categoryproductselect'] = ProductStage::get_by_once_productcategoryid($iCategoryProductId);
        if(isset($data['categoryproductselect']))
        {
            $eType = $data['categoryproductselect']->eType;
            if($eType=='Upper')
            {
                $criteria['iCategoryId'] = $data['categoryproductselect']->iCategoryId;
                $criteria['iCategoryProductId'] = $data['categoryproductselect']->iCategoryProductId;
            }
            else if($eType=='Lower')
            {
            $criteria['iCategoryId'] = $data['categoryproductselect']->iCategoryId;
            $criteria['iCategoryProductId'] = $data['categoryproductselect']->iCategoryProductId;
            }
            else{
                $criteria['iCategoryId'] = $data['categoryproductselect']->iCategoryId;
                $criteria['iCategoryProductId'] = $data['categoryproductselect']->iCategoryProductId;
            }
        }
        $criteria['eType'] = 'Upper';
        $data['uppercategory'] = CategoryProduct::get_all_category($criteria);
        
        $criteria['eType'] = 'Lower';
        $data['lowercategory'] = CategoryProduct::get_all_category($criteria);
       
        $criteria['eType'] = 'Upper';
        $data['upperproduct'] = CategoryProduct::get_all_product($criteria);
        // dd($data['upperproduct']);
        $criteria['eType'] = 'Lower';
        $data['lowerproduct'] = CategoryProduct::get_all_product($criteria);

        return view('admin.productstage.listing')->with($data);
    }
    public function getproduct(Request $request)
    {
        $iCategoryId = $request->iCategoryId;
        $data['categoryproduct']   = CategoryProduct::get_by_categoryid($iCategoryId);  

        return view('admin.productstage.ajax_fetch_product_listing')->with($data);   
         
        // return response()->json($data);   
       
    }
    public function getproduct_edit(Request $request)
    {
        $iCategoryProductId = $request->iCategoryProductId;
        $data['categoryproduct']   = CategoryProduct::get_by_categoryid_edit($iCategoryProductId);   
        return response()->json($data);   
       
    }
    public function ajax_listing(Request $request)
    {
      
        $action = $request->action;
        $iCategoryId = $request->iCategoryId;
        $iCategoryProductId = $request->iCategoryProductId;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->upperkeyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iProductStageId']    = $request->id;

            ProductStage::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $ProductStage_ID = (explode(",",$request->id));

            foreach ($ProductStage_ID as $key => $value) {
                $where                 = array();
                $where['iProductStageId']    = $value;

                ProductStage::delete_by_id($where);
            }
        }

      
        // $criteria = array();
        // $criteria['vKeyword']   = $vKeyword;
        // $criteria['column']     = $column;
        // $criteria['order']      = $order;
        // $criteria['eType'] = 'Upper';
        // $criteria['iCategoryId'] = $iCategoryId;
        // $criteria['iCategoryProductId'] = $iCategoryProductId;

        $criteria_sess = Session::get('criteria_product_stage');
        // dd($criteria_sess);
        if(!empty($action) && $action == "search")
        {
            $criteria = array();
            $criteria['vKeyword']   = $vKeyword;
            $criteria['column']     = $column;
            $criteria['order']      = $order;
            $criteria['eType'] = 'Upper';
            $criteria['iCategoryId'] = $iCategoryId;
            $criteria['iCategoryProductId'] = $iCategoryProductId;
            
            // For Perticular customer show start
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
            // For Perticular customer show end
            Session::put('criteria_product_stage', $criteria); 
        }elseif((isset($criteria_sess) && !empty($criteria_sess)))
        {
            $criteria = array();
            $criteria['vKeyword']   = isset($criteria_sess['vKeyword'])?$criteria_sess['vKeyword']:'';
            $criteria['eType']   = isset($criteria_sess['eType'])?$criteria_sess['eType']:'';
            $criteria['iCategoryId']   = isset($criteria_sess['iCategoryId'])?$criteria_sess['iCategoryId']:'';
            $criteria['iCategoryProductId']   = isset($criteria_sess['iCategoryProductId'])?$criteria_sess['iCategoryProductId']:'';
            $criteria['column']     = isset($criteria_sess['column'])?$criteria_sess['column']:'';
            $criteria['order']      = isset($criteria_sess['order'])?$criteria_sess['order']:'';
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        }
        else
        {
            $criteria = array();
            $criteria['vKeyword']   = $vKeyword;
            $criteria['column']     = $column;
            $criteria['order']      = $order;
            $criteria['eType'] = 'Upper';
            $criteria['iCategoryId'] = $iCategoryId;
            $criteria['iCategoryProductId'] = $iCategoryProductId;
            // For Perticular customer show start
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
            // For Perticular customer show end
            Session::put('criteria_product_stage', $criteria); 
        }


        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        
        $ProductStage_data = ProductStage::get_all_data($criteria);
        // dd(count($ProductStage_data));

        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($ProductStage_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->upper_limit_page !='')
        {
            $per_page = $request->upper_limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        // $criteria['start']  = $start;
        // $criteria['limit']  = $limit;
        // $criteria['paging'] = $paging;
        // $criteria['eType'] = 'Upper';
        // $criteria['iCategoryId'] = $iCategoryId;
        // $criteria['iCategoryProductId'] = $iCategoryProductId;

       
      
        $data['data'] = 'Upper';
        $data['data'] = ProductStage::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        // $data['paging'] = $paginator->paginate();
        $data['paging'] = str_replace('class="ajax_page','class="ajax_page_upper',$paginator->paginate());
        
        return view('admin.productstage.ajax_listing')->with($data);   
    }
    public function lower_ajax_listing(Request $request)
    {
      
        $action = $request->action;
        $Category = $request->Category;
        $CategoryProduct = $request->CategoryProduct;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->lower_keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iProductStageId']    = $request->id;

            ProductStage::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $ProductStage_ID = (explode(",",$request->id));

            foreach ($ProductStage_ID as $key => $value) {
                $where                 = array();
                $where['iProductStageId']    = $value;

                ProductStage::delete_by_id($where);
            }
        }

        
        $criteria_sess = Session::get('criteria_product_lower_stage');
        if(!empty($action) && $action == "search")
        {
            $criteria = array();
            $criteria['vKeyword']   = $vKeyword;
            $criteria['column']     = $column;
            $criteria['order']      = $order;
            $criteria['eType'] = 'Lower';
            $criteria['iCategoryId'] = $Category;
            $criteria['iCategoryProductId'] = $CategoryProduct;
            
            // For Perticular customer show start
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
            // For Perticular customer show end
            Session::put('criteria_product_lower_stage', $criteria); 
        }elseif((isset($criteria_sess) && !empty($criteria_sess)))
        {
            $criteria = array();
            $criteria['vKeyword']   = isset($criteria_sess['vKeyword'])?$criteria_sess['vKeyword']:'';
            $criteria['eType']   = isset($criteria_sess['eType'])?$criteria_sess['eType']:'';
            $criteria['iCategoryId']   = isset($criteria_sess['iCategoryId'])?$criteria_sess['iCategoryId']:'';
            $criteria['iCategoryProductId']   = isset($criteria_sess['iCategoryProductId'])?$criteria_sess['iCategoryProductId']:'';
            $criteria['column']     = isset($criteria_sess['column'])?$criteria_sess['column']:'';
            $criteria['order']      = isset($criteria_sess['order'])?$criteria_sess['order']:'';
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        }
        else
        {
            $criteria = array();
            $criteria['vKeyword']   = $vKeyword;
            $criteria['column']     = $column;
            $criteria['order']      = $order;
            $criteria['eType'] = 'Lower';
            $criteria['iCategoryId'] = $Category;
            $criteria['iCategoryProductId'] = $CategoryProduct;

            // For Perticular customer show start
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
            // For Perticular customer show end
            Session::put('criteria_product_lower_stage', $criteria); 
        }
        $criteria['eType'] = 'Lower';
       
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $ProductStage_data = ProductStage::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($ProductStage_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->lower_limit_page !='')
        {
            $per_page = $request->lower_limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;    
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        // $criteria['start']  = $start;
        // $criteria['limit']  = $limit;
        // $criteria['paging'] = $paging;
        // $criteria['eType'] = 'Lower';
        // $criteria['iCategoryId'] = $Category;
        // $criteria['iCategoryProductId'] = $CategoryProduct;

        $criteria_sess = Session::get('criteria_product_lower_stage');
        
      
        $data['data'] = ProductStage::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        // $data['paging'] = $paginator->paginate();
        $data['paging'] = str_replace('class="ajax_page','class="ajax_page_lower',$paginator->paginate());
        return view('admin.productstage.lower_ajax_listing')->with($data);   
    }
    
    public function create()
    {
        // For Get customer Office satrt
        $iCustomerId = General::admin_info()['iCustomerId'];
        $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);
        $office = array();
        foreach ($labOffice as $key => $value) {
            $office[] = Customer::get_by_id($value->iOfficeId);
        }
        $data['office'] = $office;
        // For Get customer Office end
        $data['category'] = ProductStage::get_all_category();        
        $data['categoryproduct'] = ProductStage::get_all_categoryproduct();        
        
        return view('admin.productstage.create')->with($data);
    }

   public function store(Request $request)
    {
        $iProductStageId = $request->id;
        
        $data['vCode'] 	    = $request->vCode;
        $data['vName']      = $request->vName;
        $data['iCategoryId']= $request->iCategoryId;
        $data['iCategoryProductId']= $request->iCategoryProductId;
        $data['vPrice']     = $request->vPrice;
        $data['iSequence']  = $request->iSequence;
        $data['iPickUpDay'] = $request->iPickUpDay;
        $data['iProcessDay']= $request->iProcessDay;
        $data['iDeliverDay']= $request->iDeliverDay;
        $data['eType']      = $request->eType;
        $data['eStatus']    = $request->eStatus;
        $data['eExtractions']    = $request->eExtractions;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        
        $data['eReleasing']    = $request->eReleasing;
        if(empty($data['eReleasing']))
        {
            $data['eReleasing'] = 'No';
        }
        
        // Main Product
        if(empty($data['eExtractions']))
        {
            $data['eExtractions'] = 'No';
        }
        $data['eGumShades']    = $request->eGumShades;
        if(empty($data['eGumShades']))
        {
            $data['eGumShades'] = 'No';
        }
        $data['eTeethShades']    = $request->eTeethShades;
        if(empty($data['eTeethShades']))
        {
            $data['eTeethShades'] = 'No';
        }
        $data['eImpressions']    = $request->eImpressions;
        if(empty($data['eImpressions']))
        {
            $data['eImpressions'] = 'No';
        }
        $data['eTeethInMouth']    = $request->eTeethInMouth;
        if(empty($data['eTeethInMouth']))
        {
            $data['eTeethInMouth'] = 'No';
        }
        $data['eTeethInDefault']    = $request->eTeethInDefault;
        if(empty($data['eTeethInDefault']))
        {
            $data['eTeethInDefault'] = 'No';
        }
        $data['eTeethInRequird']    = $request->eTeethInRequird;
        if(empty($data['eTeethInRequird']))
        {
            $data['eTeethInRequird'] = 'No';
        }
        $data['eTeethInOptional']    = $request->eTeethInOptional;
        if(empty($data['eTeethInOptional']))
        {
            $data['eTeethInOptional'] = 'No';
        }
        $data['eMessingTeeth']    = $request->eMessingTeeth;
        if(empty($data['eMessingTeeth']))
        {
            $data['eMessingTeeth'] = 'No';
        }
        $data['eMessingDefault']    = $request->eMessingDefault;
        if(empty($data['eMessingDefault']))
        {
            $data['eMessingDefault'] = 'No';
        }
        $data['eMessingRequird']    = $request->eMessingRequird;
        if(empty($data['eMessingRequird']))
        {
            $data['eMessingRequird'] = 'No';
        }
        $data['eMessingOptional']    = $request->eMessingOptional;
        if(empty($data['eMessingOptional']))
        {
            $data['eMessingOptional'] = 'No';
        }
        $data['eExtractDelivery']    = $request->eExtractDelivery;
        if(empty($data['eExtractDelivery']))
        {
            $data['eExtractDelivery'] = 'No';
        }
        $data['eExtractDefault']    = $request->eExtractDefault;
        if(empty($data['eExtractDefault']))
        {
            $data['eExtractDefault'] = 'No';
        }
        $data['eExtractRequird']    = $request->eExtractRequird;
        if(empty($data['eExtractRequird']))
        {
            $data['eExtractRequird'] = 'No';
        }
        $data['eExtractOptional']    = $request->eExtractOptional;
        if(empty($data['eExtractOptional']))
        {
            $data['eExtractOptional'] = 'No';
        }
        $data['eExtracted']    = $request->eExtracted;
        if(empty($data['eExtracted']))
        {
            $data['eExtracted'] = 'No';
        }
        $data['eExtractedDefault']    = $request->eExtractedDefault;
        if(empty($data['eExtractedDefault']))
        {
            $data['eExtractedDefault'] = 'No';
        }
        $data['eExtractedRequird']    = $request->eExtractedRequird;
        if(empty($data['eExtractedRequird']))
        {
            $data['eExtractedRequird'] = 'No';
        }
        $data['eExtractedOptional']    = $request->eExtractedOptional;
        if(empty($data['eExtractedOptional']))
        {
            $data['eExtractedOptional'] = 'No';
        }
        $data['eFixorAdd']    = $request->eFixorAdd;
        if(empty($data['eFixorAdd']))
        {
            $data['eFixorAdd'] = 'No';
        }
        $data['eFixorAddDefault']    = $request->eFixorAddDefault;
        if(empty($data['eFixorAddDefault']))
        {
            $data['eFixorAddDefault'] = 'No';
        }
        $data['eFixorAddRequird']    = $request->eFixorAddRequird;
        if(empty($data['eFixorAddRequird']))
        {
            $data['eFixorAddRequird'] = 'No';
        }
        $data['eFixorAddOptional']    = $request->eFixorAddOptional;
        if(empty($data['eFixorAddOptional']))
        {
            $data['eFixorAddOptional'] = 'No';
        }
        $data['eClasps']    = $request->eClasps;
        if(empty($data['eClasps']))
        {
            $data['eClasps'] = 'No';
        }
        $data['eClaspsDefault']    = $request->eClaspsDefault;
        if(empty($data['eClaspsDefault']))
        {
            $data['eClaspsDefault'] = 'No';
        }
        $data['eClaspsRequird']    = $request->eClaspsRequird;
        if(empty($data['eClaspsRequird']))
        {
            $data['eClaspsRequird'] = 'No';
        }
        $data['eClaspsOptional']    = $request->eClaspsOptional;
        if(empty($data['eClaspsOptional']))
        {
            $data['eClaspsOptional'] = 'No';
        }
        
        // Opposing Product
        if(empty($data['eOpposingExtractions'] = $request->eOpposingExtractions))
        {
            $data['eOpposingExtractions'] = 'No';
        }
        $data['eOpposingGumShades']    = $request->eOpposingGumShades;
        if(empty($data['eOpposingGumShades']))
        {
            $data['eOpposingGumShades'] = 'No';
        }
        $data['eOpposingTeethShades']    = $request->eOpposingTeethShades;
        if(empty($data['eOpposingTeethShades']))
        {
            $data['eOpposingTeethShades'] = 'No';
        }
        $data['eOpposingImpressions']    = $request->eOpposingImpressions;
        if(empty($data['eOpposingImpressions']))
        {
            $data['eOpposingImpressions'] = 'No';
        }
        $data['eOpposingTeethInMouth']    = $request->eOpposingTeethInMouth;
        if(empty($data['eOpposingTeethInMouth']))
        {
            $data['eOpposingTeethInMouth'] = 'No';
        }
        $data['eOpposingTeethInDefault']    = $request->eOpposingTeethInDefault;
        if(empty($data['eOpposingTeethInDefault']))
        {
            $data['eOpposingTeethInDefault'] = 'No';
        }
        $data['eOpposingMessingTeeth']    = $request->eOpposingMessingTeeth;
        if(empty($data['eOpposingMessingTeeth']))
        {
            $data['eOpposingMessingTeeth'] = 'No';
        }
        $data['eOpposingMessingDefault']    = $request->eOpposingMessingDefault;
        if(empty($data['eOpposingMessingDefault']))
        {
            $data['eOpposingMessingDefault'] = 'No';
        }
        $data['eOpposingExtractDelivery']    = $request->eOpposingExtractDelivery;
        if(empty($data['eOpposingExtractDelivery']))
        {
            $data['eOpposingExtractDelivery'] = 'No';
        }
        $data['eOpposingExtractDefault']    = $request->eOpposingExtractDefault;
        if(empty($data['eOpposingExtractDefault']))
        {
            $data['eOpposingExtractDefault'] = 'No';
        }
        $data['eOpposingExtracted']    = $request->eOpposingExtracted;
        if(empty($data['eOpposingExtracted']))
        {
            $data['eOpposingExtracted'] = 'No';
        }
        $data['eOpposingExtractedDefault']    = $request->eOpposingExtractedDefault;
        if(empty($data['eOpposingExtractedDefault']))
        {
            $data['eOpposingExtractedDefault'] = 'No';
        }
        $data['eOpposingFixorAdd']    = $request->eOpposingFixorAdd;
        if(empty($data['eOpposingFixorAdd']))
        {
            $data['eOpposingFixorAdd'] = 'No';
        }
        $data['eOpposingFixorAddDefault']    = $request->eOpposingFixorAddDefault;
        if(empty($data['eOpposingFixorAddDefault']))
        {
            $data['eOpposingFixorAddDefault'] = 'No';
        }
      
        $data['eOpposingClasps']    = $request->eOpposingClasps;
        if(empty($data['eOpposingClasps']))
        {
            $data['eOpposingClasps'] = 'No';
        }
        $data['eOpposingClaspsDefault']    = $request->eOpposingClaspsDefault;
        if(empty($data['eOpposingClaspsDefault']))
        {
            $data['eOpposingClaspsDefault'] = 'No';
        }
         
        if($iProductStageId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        $iProductId = $request->iCategoryProductId;
        if(!empty($iProductStageId)){
            $where                      = array();
            $where['iProductStageId']       = $iProductStageId;

            $ProductStage_id = new ProductStage();
            $ProductStage_id->update($where, $data);

            $iGradeId    = $request->iGradeId;
            $vGradePrice    = $request->vStagePrice;
        
            if(!empty($iGradeId) ){

                $where['iCategoryStageId'] = $iProductStageId;
             
                StageGrade::delete_by_id($where);
                if(isset($request->is_grade) && $request->is_grade !='no')
                {
                    for ($i=0; $i < count($iGradeId) ; $i++) 
                    {   
                        $datas['iCategoryStageId'] = $iProductStageId;
                        $datas['iGradeId'] 	         = $iGradeId[$i];
                        if($vGradePrice[$i] ==null)
                        {
                            $datas['vPrice'] 	         = '';
                        }
                        else
                        {
                            $datas['vPrice'] = $vGradePrice[$i];
                        }
                        $ProductGrade_id = StageGrade::add($datas);
                    }
                }
            }
            
            //Store Office Prices start
            $iCustomerId = General::admin_info()['iCustomerId'];
            
            $OfficeExist = OfficeStagePrice::checkExistOffice($iProductStageId);
   
              if(isset($OfficeExist) && count($OfficeExist) >0)
              {
                  OfficeStagePrice::deleteExistOffice($iProductStageId);
              }
              //Store Office Prices start
             
              if(isset($request->Office) && !empty($request->Office))
              {
                  foreach($request->Office as $key=> $office_val)   
                  {
                        // For Grade price start
                        if(!isset($office_val['vOfficePrice']) )
                        {
                            if(isset($office_val['OfficeGrade']))
                            {
                                foreach ($office_val['OfficeGrade'] as $key_grade => $value_grade) 
                                {
                                    $office_data['vPrice'] = $value_grade['vOfficePrice'];
                                    $office_data['iGradeId'] = $value_grade['iGradeId'];
                                    if(empty($value_grade['vOfficePrice']) || $value_grade['vOfficePrice'] ==null)
                                    {
                                        if(isset($request['vPrice']) && !empty($request['vPrice']) && $request['vPrice'] != null)
                                        {
                                            $office_data['vPrice'] = $request['vPrice'];
                                        }
                                        else
                                        {
                                            $office_data['vPrice'] = '';
                                        }
                                        
                                    }
                                    $office_data['iOfficeId'] = $office_val['iOfficeId'];
                                    $office_data['iLabId'] = General::admin_info()['iCustomerId'];
                                    $office_data['vLabName'] = General::admin_info()['vName'];
                                    $office_data['vOfficeName'] = $office_val['vOfficeName'];
                                    $office_data['iStageId'] = $iProductStageId;
                                    $office_data['iProductId'] = $iProductId;
                                    $office_data['dtAddedDate'] = date("Y-m-d H:i:s");
                                    $office_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                                    
                                    $LastOfficeId = OfficeStagePrice::AddOfficePrice($office_data);
                                }
                            }
                        }
                        else
                        {
                            $office_data['vPrice'] = $office_val['vOfficePrice'];
                            if(empty($office_val['vOfficePrice']))
                            {
                                if(isset($request['vPrice']) && !empty($request['vPrice']) && $request['vPrice'] != null)
                                {
                                    $office_data['vPrice'] = $request['vPrice'];
                                }
                                else
                                {
                                    $office_data['vPrice'] = '';
                                }
                            }
                            
                            $office_data['iOfficeId'] = $office_val['iOfficeId'];
                            $office_data['iLabId'] = General::admin_info()['iCustomerId'];
                            $office_data['vLabName'] = General::admin_info()['vName'];
                            $office_data['vOfficeName'] = $office_val['vOfficeName'];
                            $office_data['iStageId'] = $iProductStageId;
                            $office_data['iProductId'] = $iProductId;
                            $office_data['dtAddedDate'] = date("Y-m-d H:i:s");
                            $office_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                            $LastOfficeId = OfficeStagePrice::AddOfficePrice($office_data);
                        }
                        // For Grade price end
                    }
                  
                
              }
            //Store Office Prices end

            return redirect()->route('admin.productstage')->withSuccess('Product Stage updated successfully.');
        }
        else{
            $ProductStage_id = ProductStage::add($data);
            $iGradeId    = $request->iGradeId;
            $vGradePrice    = $request->vStagePrice;
        
            if(!empty($iGradeId)){

                $where['iCategoryStageId'] = $ProductStage_id;
                StageGrade::delete_by_id($where);
                for ($i=0; $i < count($iGradeId) ; $i++) 
                {   
                    $datas['iCategoryStageId'] = $ProductStage_id;
                    $datas['iGradeId'] 	         = $iGradeId[$i];
                    if($vGradePrice[$i] ==null)
                    {
                        $datas['vPrice'] 	         = '';
                    }
                    else
                    {
                        $datas['vPrice'] = $vGradePrice[$i];
                    }
                    $ProductGrade_id = StageGrade::add($datas);
                }
            }
          //Store Office Prices start
          $iCustomerId = General::admin_info()['iCustomerId'];
            
          $OfficeExist = OfficeStagePrice::checkExistOffice($ProductStage_id);
         
            if(isset($OfficeExist) && count($OfficeExist) >0)
            {
                OfficeStagePrice::deleteExistOffice($ProductStage_id);
            }
            
            //Store Office Prices start
            // dd($request->Office);
            if(isset($request->Office) && !empty($request->Office))
            {
                foreach($request->Office as $key=> $office_val)   
                {
                    // For Grade price start
                
                    if(!isset($office_val['vOfficePrice']) && $office_val['vOfficePrice'] == null)
                    {
                        if(isset($office_val['OfficeGrade']))
                        {
                            foreach ($office_val['OfficeGrade'] as $key_grade => $value_grade) 
                            {
                                $office_data['vPrice'] = $value_grade['vOfficePrice'];
                                $office_data['iGradeId'] = $value_grade['iGradeId'];
                                if(empty($value_grade['vOfficePrice']))
                                {
                                    $office_data['vPrice'] = $request['vPrice'];
                                }
                                $office_data['iOfficeId'] = $office_val['iOfficeId'];
                                $office_data['iLabId'] = General::admin_info()['iCustomerId'];
                                $office_data['vLabName'] = General::admin_info()['vName'];
                                $office_data['vOfficeName'] = $office_val['vOfficeName'];
                                $office_data['iStageId'] = $ProductStage_id;
                                $office_data['iProductId'] = $iProductId;
                                $office_data['dtAddedDate'] = date("Y-m-d H:i:s");
                                $office_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                                $LastOfficeId = OfficeStagePrice::AddOfficePrice($office_data);
                            }
                        }
                    }
                    else
                    {
                        $office_data['vPrice'] = $office_val['vOfficePrice'];
                        if(empty($office_val['vOfficePrice']))
                        {
                            $office_data['vPrice'] = $request['vPrice'];
                        }
                        $office_data['iOfficeId'] = $office_val['iOfficeId'];
                        $office_data['iLabId'] = General::admin_info()['iCustomerId'];
                        $office_data['vLabName'] = General::admin_info()['vName'];
                        $office_data['vOfficeName'] = $office_val['vOfficeName'];
                        $office_data['iStageId'] = $ProductStage_id;
                        $office_data['iProductId'] = $iProductId;
                        $office_data['dtAddedDate'] = date("Y-m-d H:i:s");
                        $office_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                        $LastOfficeId = OfficeStagePrice::AddOfficePrice($office_data);
                    }
                    // For Grade price end
                
                }
            }
          //Store Office Prices end
            return redirect()->route('admin.productstage')->withSuccess('Product Stage created successfully.');
        }
    }

    public function edit($iProductStageId)
    {
        $data['categoryproduct'] = ProductStage::get_all_categoryproduct();
        $data['productstage'] = ProductStage::get_by_id($iProductStageId);

        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['productstage']->iCustomerId) && $data['productstage']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['productstage']->iCustomerId)))
        {
            return redirect()->route('admin.category')->withError('Invalid Input');
        }

        $criteria["eType"] = $data['productstage']->eType;
        $data['category'] = ProductStage::get_all_category($criteria);

        // For Get customer Office satrt
        $iCustomerId = General::admin_info()['iCustomerId'];
        $iTypeId = $iProductStageId;
        $OfficeExist = CategoryProduct::checkExistOffice($iCustomerId,$iTypeId);
        if(isset($OfficeExist) && count($OfficeExist) >0)
        {
           $data['office'] = $OfficeExist;
        }
        else
        {
            $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);
            $office = array();
            foreach ($labOffice as $key => $value) {
                $office[] = Customer::get_by_id($value->iOfficeId);
            }
            $data['office'] = $office;
        }
        // For Get customer Office end
        $data['grades']     = CategoryProduct::get_all_grade(); 
        $data['grade_data']     = StageGrade::get_by_product_id($iProductStageId);
        // dd($data['grade_data']);

        // office price update start
        $iCategoryProductId =$iProductStageId;
        // dd($iCategoryProductId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);

        $data['grades']     = CategoryProduct::get_all_grade(); 
        $product_grade     = StageGrade::get_by_product_id($iCategoryProductId);
     
        $data['product_grade'] = $product_grade;

        foreach ($product_grade as $key => $value) {
            $data['prograde'][] = $value->iGradeId;
            // $data['prices'][] = $value->vPrice;
        
        }

        $office = array();
        foreach ($labOffice as $key => $value) {
            $office[] = Customer::get_by_id($value->iOfficeId);
        }
        $new_array = array();
        foreach ($office as $key => $value_office) {
            $new_array[$key]['vOfficeName'] = $value_office->vOfficeName;
            $new_array[$key]['iOfficeId'] = $value_office->iCustomerId;
            $new_array[$key]['iCategoryProductId'] = $iCategoryProductId;
            if(isset($data['prograde']) && !empty($data['prograde']))
            {
                foreach ($data['prograde'] as $key_grade => $value_grade) {
                    $new_array[$key]['grades'][$key_grade]['iGradeid'] = $value_grade;
                    $new_array[$key]['grades'][$key_grade]['vQualityName'] = Grade::get_by_id($value_grade)->vQualityName;
                    $criteria_office = array();
                    $criteria_office['iOfficeId'] = $value_office->iCustomerId;
                    $criteria_office['iStageId'] = $iCategoryProductId;
                    $criteria_office['iGradeId'] = $value_grade;
                    $data_office_product_grade_price = OfficeStagePrice::get_by_product_grade_office($criteria_office);
                    // dd($data_office_product_grade_price);
                    foreach ($data_office_product_grade_price as $key_grade_price => $value_grade_price) {
                        $new_array[$key]['grades'][$key_grade]['vPrice']=$value_grade_price->vPrice;
                    }
                }
            }
            else
            {
                $criteria_office['iOfficeId'] = $value_office->iCustomerId;
                $criteria_office['iStageId'] = $iCategoryProductId;
                $data_office_product_grade_price = OfficeStagePrice::get_by_product_grade_office($criteria_office);
                if(isset($data_office_product_grade_price) && count($data_office_product_grade_price)>0)
                {
                    $new_array[$key]['vPrice'] = $data_office_product_grade_price[0]->vPrice;
                }
            }
            
        }
        $data['office']= $new_array;
        $data['edit']= 'Yes';
        // dd($data['office']);

        $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);
        $office = array();
        foreach ($labOffice as $key => $value) {
            $office[] = Customer::get_by_id($value->iOfficeId);
        }
        $data['office_old'] = $office;

        return view('admin.productstage.create')->with($data);
    }

    public function destroy($id)
    {
        $ProductStage = ProductStage::find($id);
        $ProductStage->delete();

        return redirect()->route('admin.productstage')->withSuccess('Product Stage deleted successfully.');
    }

    public function multiDelete(Request $request)
    {
        $id = $request->id;

        ProductStage::whereIn('id',explode(",",$id))->delete();
    }
    public function fetch_category(Request $request)
    {
        $eType = $request->eType;
        $data['data'] = Category::where("eType",$eType)
                                    ->orderBy("iCategoryId", "desc")
                                    ->get();
        return view('admin.productstage.ajax_fetch_category_listing')->with($data);
    }
    public function fetch_product(Request $request)
    {
       
        $eType = $request->eType;
        $data['data'] = CategoryProduct::where("eType",$eType)
                                    ->orderBy("iCategoryProductId", "desc")
                                    ->get();
        return view('admin.productstage.ajax_fetch_product_listing')->with($data);
    }
    public function fetch_grade_price(Request $request)
    {
        $iCategoryProductId = $request->iCategoryProductId;
        // Office data start
        $data['grades']     = CategoryProduct::get_all_grade(); 
        $product_grade     = ProductGrade::get_by_product_id($iCategoryProductId);
        $data['grade_data'] = $product_grade;
        if(count($product_grade)>0)
        {
            return view('admin.productstage.ajax_fetch_grade_listing')->with($data);
        }
        else
        {
            return 'no_grade';
        }
    }
    public function fetch_office_grade_price(Request $request)
    {
        // For Get customer Office satrt
        $iCategoryProductId =$request->iCategoryProductId;
        // dd($iCategoryProductId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);

        $data['grades']     = CategoryProduct::get_all_grade(); 
        $product_grade     = ProductGrade::get_by_product_id($iCategoryProductId);
       
        $data['product_grade'] = $product_grade;

        foreach ($product_grade as $key => $value) {
            $data['prograde'][] = $value->iGradeId;
            // $data['prices'][] = $value->vPrice;
        
        }

        $office = array();
        foreach ($labOffice as $key => $value) {
            $office[] = Customer::get_by_id($value->iOfficeId);
        }
        $new_array = array();
        foreach ($office as $key => $value_office) {
            $new_array[$key]['vOfficeName'] = $value_office->vOfficeName;
            $new_array[$key]['iOfficeId'] = $value_office->iCustomerId;
            $new_array[$key]['iCategoryProductId'] = $iCategoryProductId;
            if(isset($data['prograde']) && !empty($data['prograde']))
            {
                foreach ($data['prograde'] as $key_grade => $value_grade) {
                    $new_array[$key]['grades'][$key_grade]['iGradeid'] = $value_grade;
                    $new_array[$key]['grades'][$key_grade]['vQualityName'] = Grade::get_by_id($value_grade)->vQualityName;
                    $criteria_office = array();
                    $criteria_office['iOfficeId'] = $value_office->iCustomerId;
                    $criteria_office['iProductId'] = $iCategoryProductId;
                    $criteria_office['iGradeId'] = $value_grade;
                    $data_office_product_grade_price = OfficeStagePrice::get_by_product_grade_office($criteria_office);
                    foreach ($data_office_product_grade_price as $key_grade_price => $value_grade_price) {
                        $new_array[$key]['grades'][$key_grade]['vPrice']=$value_grade_price->vPrice;
                    }
                }
            }
            else
            {
                $criteria_office['iOfficeId'] = $value_office->iCustomerId;
                $criteria_office['iProductId'] = $iCategoryProductId;
                $data_office_product_grade_price = OfficeStagePrice::get_by_product_grade_office($criteria_office);
                if(isset($data_office_product_grade_price) && count($data_office_product_grade_price)>0)
                {
                    $new_array[$key]['vPrice'] = $data_office_product_grade_price[0]->vPrice;
                }
            }
            
        }
        $data['office']= $new_array;
        // dd($data['office']);
        if(count($data)>0)
        {
            return view('admin.productstage.ajax_fetch_grade_price_listing')->with($data);
        }
        else
        {
            return 0;
        }
    }
}
