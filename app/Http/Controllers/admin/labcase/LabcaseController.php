<?php

namespace App\Http\Controllers\admin\labcase;
use PDF;
use Session;
use Redirect;
use Validator;
use App\Libraries\General;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Illuminate\Http\Request;
use App\Models\admin\call\Call;
use App\Models\admin\lab_admin\LabAdmin;
use App\Models\admin\office_admin\OfficeAdmin;
use App\Models\admin\staff\Staff;
use App\Models\admin\fees\Fees;
use App\Models\admin\slip\Slip;
use App\Models\admin\admin\Admin;
use App\Models\admin\grade\Grade;
use App\Models\admin\doctor\Doctor;
use App\Models\admin\office\Office;
use App\Http\Controllers\Controller;
use App\Models\admin\casepan\Casepan;
use App\Models\admin\labcase\labcase;
use App\Models\admin\labOffice\LabOffice;
use App\Models\admin\customer\Customer;
use App\Models\admin\category\Category;
use App\Models\admin\gumbrand\GumBrand;
use App\Models\admin\schedule\Schedule;
use App\Models\admin\slipnote\SlipNote;
use App\Models\admin\gumshades\GumShades;
use App\Models\admin\impression\Impression;
use App\Models\admin\slipaddons\SlipAddons;
use App\Models\admin\toothbrand\ToothBrand;
use App\Models\admin\slipgallery\SlipGallery;
use App\Models\admin\slipproduct\SlipProduct;
use App\Models\admin\toothshades\ToothShades;
use App\Models\admin\productgrade\ProductGrade;
use App\Models\admin\productstage\ProductStage;
use App\Models\admin\addoncategory\Addoncategory;
use App\Models\admin\casepannumber\CasepanNumber;
use App\Models\admin\driver_history\DriverHistory;
use App\Models\admin\slipimpression\SlipImpression;
use App\Models\admin\categoryproduct\CategoryProduct;
use App\Models\admin\subaddoncategory\Subaddoncategory;
use App\Models\admin\office_prices\OfficePrices;
use App\Models\admin\billing_addons\BillingAddons;
use App\Models\admin\billing\Billing;
use App\Models\admin\billing_products\BillingProduct;
use App\Models\admin\office_product_price\OfficeProductPrice;
use App\Models\admin\office_stage_price\OfficeStagePrice;
use App\Models\admin\stage_grade\StageGrade;
use App\Models\admin\productgumshades\ProductGumShades;
use App\Models\admin\user\User;
use DB;
use App\Models\admin\system_email\SystemEmail;
use Image;


class LabcaseController extends Controller
{
    public function index()
    {
        Session::forget('limit_filter_labcase_lab');
        Session::forget('per_page_labcase');
        Session::forget('criteria');

        $iCustomerId = General::admin_info()['iCustomerId'];

        $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);
      

        $office = array();

        foreach ($labOffice as $key => $value) {
            $office[] = Customer::get_by_id($value->iOfficeId);
        }
        $office = collect($office);
        $office = $office->sortBy('vOfficeName');
        $data['office'] = $office;
        $data['doctor'] = Doctor::get_all_doctor();
        $data["Office"] = Office::get_office_by_customerid();
        $criteria_cust['eStatus'] = 'Active';
        $data["customer_data"] = Customer::get_all_data($criteria_cust);
        
        // $data['Upper_category'] = Category::get_category_by_type_customer("Upper");
        // $data['Lower_category'] = Category::get_category_by_type_customer("Lower");

        $data['toothshade'] = ToothShades::get_all_toothshades();
        $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        $data['toothbrand'] = ToothBrand::get_all_brand();

        // dd($data['impression']);

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

        $schedule = Schedule::get_all_schedule();
        
        $data['holidays'] = "";
        foreach($schedule as $value){
            if (!empty($value->dtHolidays) AND empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                $holiday_arr = explode("-",$value->dtHolidays);
                $holidays[] = $holiday_arr[1]."/".$holiday_arr[2]."/".$holiday_arr[0];
            }
        }
        if(!empty($holidays)){
            $data['holidays'] = implode(",",$holidays);
        }

        $admin = Session::get('data');
        if(empty($admin['user_id']))
        {
            Redirect::to('/admin/login')->send();
        }else{
            // $iAdminId = $admin['user_id'];
            // $admin    = Admin::get_by_id($iAdminId);
            if(isset($admin['vFirstName']) && !empty($admin['vFirstName']) && isset($admin['vLastName']) && !empty($admin['vLastName']))
            {
                $data['user_name'] = $admin['vFirstName'].' '.$admin['vLastName'];
            }
            else
            {
                $data['user_name'] = $admin['vName'];
            }
              
            // $data['AdminId'] = $iAdminId;
            $data['AdminId'] = $admin['user_id'];
        }

        $caseid = Labcase::get_office_id();
        if(!empty($caseid->iCaseId)) {
            $id = $caseid->iCaseId+1;
        }else{           
            $id = 1;
        }
        $data['case_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $slipid = Labcase::get_slip_id();
        if(!empty($slipid->iSlipId)) {
            $id = $slipid->iSlipId+1;
        }else{           
            $id = 1;
        }
        $data['slip_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $data['tampid'] =  str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
        $data['iLoggedinId']=$iCustomerId;
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        $slipData = Labcase::get_all_data_date($criteria);

       
        // $slipData = collect($slipData);
        // $slipData = $slipData->sortBy('current_date');
        $data['slip_data'] = $slipData;
        return view('admin.labcase.listing_lab')->with($data);

    }

    public function office_admin_listing()
    {
        Session::forget('limit_filter_labcase');
        Session::forget('per_page_labcase');
        Session::forget('criteria');

        $iCustomerId = General::admin_info()['iCustomerId'];
        $labOffice = LabOffice::get_active_lab_by_officeId($iCustomerId);
        
        $labs = array();
        
        foreach ($labOffice as $key => $value) {
            $labs[] = Customer::get_by_id($value->iLabId);
        }

        $labs = collect($labs);
        $labs = $labs->sortBy('vOfficeName');

        $data['labs'] = $labs;
        $data['office'] = Office::get_office_by_customerid();
        $data['doctor'] = Doctor::get_all_doctor();
        $data["Office"] = Office::get_office_by_customerid();
        
        // $data['Upper_category'] = Category::get_category_by_type_customer("Upper");
        // $data['Lower_category'] = Category::get_category_by_type_customer("Lower");

        $data['toothshade'] = ToothShades::get_all_toothshades();
        $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        $data['toothbrand'] = ToothBrand::get_all_brand();

        // dd($data['impression']);

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

        $schedule = Schedule::get_all_schedule();
        
        $data['holidays'] = "";
        foreach($schedule as $value){
            if (!empty($value->dtHolidays) AND empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                $holiday_arr = explode("-",$value->dtHolidays);
                $holidays[] = $holiday_arr[1]."/".$holiday_arr[2]."/".$holiday_arr[0];
            }
        }
        if(!empty($holidays)){
            $data['holidays'] = implode(",",$holidays);
        }

        $admin = Session::get('data');
        if(empty($admin['user_id']))
        {
            Redirect::to('/admin/login')->send();
        }else{
            // $iAdminId = $admin['user_id'];
            // $admin    = Admin::get_by_id($iAdminId);
           if(isset($admin['vFirstName']) && !empty($admin['vFirstName']) && isset($admin['vLastName']) && !empty($admin['vLastName']))
              {
                $data['user_name'] = $admin['vFirstName'].' '.$admin['vLastName'];
              }
              else
              {
                $data['user_name'] = $admin['vName'];
              }
                  
            // $data['AdminId'] = $iAdminId;
            $data['AdminId'] = $admin['user_id'];
        }

        $caseid = Labcase::get_office_id();
        if(!empty($caseid->iCaseId)) {
            $id = $caseid->iCaseId+1;
        }else{           
            $id = 1;
        }
        $data['case_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $slipid = Labcase::get_slip_id();
        if(!empty($slipid->iSlipId)) {
            $id = $slipid->iSlipId+1;
        }else{           
            $id = 1;
        }
        $data['slip_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $data['tampid'] =  str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
        
        return view('admin.labcase.listing')->with($data);

    }

    public function testheic(){
        $path = '/home/isync/Downloads/sample2.heic';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        echo($base64);
    }

    public function new_stage($iCaseId,$iSlipId)
    {
        $iCustomerId = General::admin_info()['iCustomerId'];

        $labOffice = LabOffice::get_active_lab_by_officeId($iCustomerId);

        $labs = array();

        foreach ($labOffice as $key => $value) {
            $labs[] = Customer::get_by_id($value->iLabId);
        }

        $data['labs'] = $labs;
        $data["doctor"] =   Doctor::get_doc_by_customerid($iCustomerId);

        $data['office'] = Office::get_office_by_customerid();
        
        // $data['Upper_category'] = Category::get_category_by_type("Upper");
        // $data['Lower_category'] = Category::get_category_by_type("Lower");
        $data['toothshade'] = ToothShades::get_all_toothshades();
        // $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        // $data['toothbrand'] = ToothBrand::get_all_brand();
        // $data['impression'] = Impression::get_all_impressions();
        // dd($data['impression']);

        // $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        // $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

        $schedule = Schedule::get_all_schedule();
        foreach($schedule as $value){
            if (!empty($value->dtHolidays) AND empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                $holiday_arr = explode("-",$value->dtHolidays);
                $holidays[] = $holiday_arr[1]."/".$holiday_arr[2]."/".$holiday_arr[0];
            }
        }
        if(!empty($holidays)){
            $data['holidays'] = implode(",",$holidays);
        }

        $admin = Session::get('data');
        if(empty($admin['user_id']))
        {
            Redirect::to('/admin/login')->send();
        }else{
            // $iAdminId = $admin['user_id'];
            // $admin    = Admin::get_by_id($iAdminId);
           if(isset($admin['vFirstName']) && !empty($admin['vFirstName']) && isset($admin['vLastName']) && !empty($admin['vLastName']))
              {
                $data['user_name'] = $admin['vFirstName'].' '.$admin['vLastName'];
              }
              else
              {
                $data['user_name'] = $admin['vName'];
              }
                          
            // $data['AdminId'] = $iAdminId;
            $data['AdminId'] = $admin['user_id'];
        }

        $caseid = Labcase::get_office_id();
        if(!empty($caseid->iCaseId)) {
            $id = $caseid->iCaseId+1;
        }else{           
            $id = 1;
        }
        $data['case_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $slipid = Labcase::get_slip_id();
        if(!empty($slipid->iSlipId)) {
            $id = $slipid->iSlipId+1;
        }else{           
            $id = 1;
        }
        $data['slip_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $data['tampid'] =  str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);

        $data['Case'] = Labcase::get_by_id($iCaseId);
        $data['Slip'] = Slip::get_by_id($iSlipId);

        // For Security purpose start
        $iCustomerId = General::admin_info()['iCustomerId'];
        $customerType =General::admin_info()['customerType'];
        if(isset($customerType) && $customerType =='Office Admin')
        {
            if(isset($data['Slip']->iOfficeId) && !empty($data['Slip']->iOfficeId))
            {
                $MatchId = $data['Slip']->iOfficeId;
            }
            else
            {
                $MatchId = '';
            }
        }
        if(isset($customerType) && $customerType =='Lab Admin')
        {
            if(isset($data['Slip']->iLabId) && !empty($data['Slip']->iLabId))
            {
                $MatchId = $data['Slip']->iLabId;
            }
            else
            {
                $MatchId = '';
            }
        }
        if((isset($MatchId) && $MatchId != $iCustomerId && !empty($iCustomerId)) || (empty($MatchId)) || (empty($data['Case'])))
        {
            if(isset($customerType) && $customerType =='Office Admin')
            {
                return redirect()->route('admin.labcase.office_admin_listing')->withError('Invalid Input');
            }
            elseif(isset($customerType) && $customerType =='Lab Admin')
            {
                return redirect()->route('admin.labcase')->withError('Invalid Input');
            }
        }
        // For Security purpose end
        
        $data['upper_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Upper");
        $data['lower_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Lower");

        if(isset($data['lower_product']) && $data['lower_product']->vStageName == "Finish" && $data['lower_product']->eStatus == "Finished"){
            unset($data['lower_product']);
        }

        if(isset($data['upper_product']) && $data['upper_product']->vStageName == "Finish" && $data['upper_product']->eStatus == "Finished"){
            unset($data['upper_product']);
        }
        

        // $data['doctor'] = Doctor::get_doc_by_office($data['Case']->iOfficeId);

        $data["rush"] = "No";

        $iLabId = $data['Slip']->iLabId;
        $data["Upper_category"] =   Category::get_category_by_type_customer('Upper',$iLabId);
        $data["Lower_category"] =   Category::get_category_by_type_customer('Lower',$iLabId);

        $data['toothbrand'] = ToothBrand::get_all_brand_by_customerid($iLabId);

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type('Upper',$iLabId);
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type('Lower',$iLabId);

        $criteria_new = array();
        $criteria_new['iLabId'] = $iLabId;
        $data["gumshade"] =   GumShades::get_product_by_gumbrand($criteria_new);

        $data["impression"] = Impression::get_all_impressions_by_customer($iLabId);

        if (!empty($data['upper_product'])) {



            $data['upper_vTeethInMouth'] = explode(",",$data['upper_product']->vTeethInMouth);
            $data['upper_vMissingTeeth'] = explode(",",$data['upper_product']->vMissingTeeth);
            $data['upper_vWillExtractOnDelivery'] = explode(",",$data['upper_product']->vWillExtractOnDelivery);
            $data['upper_vHasBeenExtracted'] = explode(",",$data['upper_product']->vHasBeenExtracted);
            $data['upper_vFixOrAdd'] = explode(",",$data['upper_product']->vFixOrAdd);
            $data['upper_vClasps'] = explode(",",$data['upper_product']->vClasps);

            $upper_teeth_in_mouth = array();
            $upper_extract= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

            $upper_merged_array = array_merge(
                $data['upper_vMissingTeeth'],
                $data['upper_vWillExtractOnDelivery'],
                $data['upper_vHasBeenExtracted'],
                $data['upper_vFixOrAdd'],
                $data['upper_vClasps']
            );

            foreach ($upper_extract as $value) {
                if (!in_array($value,$upper_merged_array)) {
                    $upper_teeth_in_mouth[] = $value;
                }
            }
            
            $data['upper_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['upper_vTeethInMouth'],
                    $upper_teeth_in_mouth
                )
            );

            $data['lower_vTeethInMouthOp'] = explode(",",$data['upper_product']->vTeethInMouthOp);
            $data['lower_vMissingTeethOp'] = explode(",",$data['upper_product']->vMissingTeethOp);
            $data['lower_vWillExtractOnDeliveryOp'] = explode(",",$data['upper_product']->vWillExtractOnDeliveryOp);
            $data['lower_vHasBeenExtractedOp'] = explode(",",$data['upper_product']->vHasBeenExtractedOp);
            $data['lower_vFixOrAddOp'] = explode(",",$data['upper_product']->vFixOrAddOp);
            $data['lower_vClaspsOp'] = explode(",",$data['upper_product']->vClaspsOp);

            if (!empty($data['upper_product']->lower_vTeethInMouthOp) AND !empty($data['upper_product']->lower_vMissingTeethOp) AND !empty($data['upper_product']->lower_vWillExtractOnDeliveryOp) AND !empty($data['upper_product']->lower_vHasBeenExtractedOp) AND !empty($data['upper_product']->lower_vFixOrAddOp) AND !empty($data['upper_product']->lower_vClaspsOp)) {


                $lower_teeth_in_mouthOp = array();
                $lower_extractOp= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");

                $lower_merged_arrayOp = array_merge(
                    $data['lower_vMissingTeethOp'],
                    $data['lower_vWillExtractOnDeliveryOp'],
                    $data['lower_vHasBeenExtractedOp'],
                    $data['lower_vFixOrAddOp'],
                    $data['lower_vClaspsOp']
                );

                foreach ($lower_extractOp as $value) {
                    if (!in_array($value,$lower_merged_arrayOp)) {
                        $lower_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['lower_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['lower_vTeethInMouthOp'],
                        $lower_teeth_in_mouthOp
                    )
                );
            }
            $data['lower_vTeethInMouthOp'] = array_filter($data['lower_vTeethInMouthOp']);
            $data['lower_vMissingTeethOp'] = array_filter($data['lower_vMissingTeethOp']);
            $data['lower_vWillExtractOnDeliveryOp'] = array_filter($data['lower_vWillExtractOnDeliveryOp']);
            $data['lower_vHasBeenExtractedOp'] = array_filter($data['lower_vHasBeenExtractedOp']);
            $data['lower_vFixOrAddOp'] = array_filter($data['lower_vFixOrAddOp']);
            $data['lower_vClaspsOp'] = array_filter($data['lower_vClaspsOp']);

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['lower_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['lower_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['lower_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $upper_delivery_date = $data['upper_product']->dDeliveryDate;
            if ($data['upper_product']->eRushDate == "Yes") {
                $data["rush"] = "Yes";
            }
        }

        if (!empty($data['lower_product'])) {

            $data['lower_vTeethInMouth'] = explode(",",$data['lower_product']->vTeethInMouth);
            $data['lower_vMissingTeeth'] = explode(",",$data['lower_product']->vMissingTeeth);
            $data['lower_vWillExtractOnDelivery'] = explode(",",$data['lower_product']->vWillExtractOnDelivery);
            $data['lower_vHasBeenExtracted'] = explode(",",$data['lower_product']->vHasBeenExtracted);
            $data['lower_vFixOrAdd'] = explode(",",$data['lower_product']->vFixOrAdd);
            $data['lower_vClasps'] = explode(",",$data['lower_product']->vClasps);

            
            $teeth_in_mouth = array();
            $lower_extract= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");
            
            $merged_array = array_merge(
                $data['lower_vMissingTeeth'],
                $data['lower_vWillExtractOnDelivery'],
                $data['lower_vHasBeenExtracted'],
                $data['lower_vFixOrAdd'],
                $data['lower_vClasps']
            );
            
            foreach ($lower_extract as $value) {
                if (!in_array($value,$merged_array)) {
                    $teeth_in_mouth[] = $value;
                }
            }

            $data['lower_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['lower_vTeethInMouth'],
                    $teeth_in_mouth
                )
            );


            $data['upper_vTeethInMouthOp'] = explode(",",$data['lower_product']->vTeethInMouthOp);
            $data['upper_vMissingTeethOp'] = explode(",",$data['lower_product']->vMissingTeethOp);
            $data['upper_vWillExtractOnDeliveryOp'] = explode(",",$data['lower_product']->vWillExtractOnDeliveryOp);
            $data['upper_vHasBeenExtractedOp'] = explode(",",$data['lower_product']->vHasBeenExtractedOp);
            $data['upper_vFixOrAddOp'] = explode(",",$data['lower_product']->vFixOrAddOp);
            $data['upper_vClaspsOp'] = explode(",",$data['lower_product']->vClaspsOp);

            if (!empty($data['lower_product']->upper_vTeethInMouthOp) AND !empty($data['lower_product']->upper_vMissingTeethOp) AND !empty($data['lower_product']->upper_vWillExtractOnDeliveryOp) AND !empty($data['lower_product']->upper_vHasBeenExtractedOp) AND !empty($data['lower_product']->upper_vFixOrAddOp) AND !empty($data['lower_product']->upper_vClaspsOp)) {

                $upper_teeth_in_mouthOp = array();
                $upper_extractOp= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

                $upper_merged_arrayOp = array_merge(
                    $data['upper_vMissingTeethOp'],
                    $data['upper_vWillExtractOnDeliveryOp'],
                    $data['upper_vHasBeenExtractedOp'],
                    $data['upper_vFixOrAddOp'],
                    $data['upper_vClaspsOp']
                );

                foreach ($upper_extractOp as $value) {
                    if (!in_array($value,$upper_merged_arrayOp)) {
                        $upper_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['upper_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['upper_vTeethInMouthOp'],
                        $upper_teeth_in_mouthOp
                    )
                );
            }
            $data['upper_vTeethInMouthOp'] = array_filter($data['upper_vTeethInMouthOp']);
            $data['upper_vMissingTeethOp'] = array_filter($data['upper_vMissingTeethOp']);
            $data['upper_vWillExtractOnDeliveryOp'] = array_filter($data['upper_vWillExtractOnDeliveryOp']);
            $data['upper_vHasBeenExtractedOp'] = array_filter($data['upper_vHasBeenExtractedOp']);
            $data['upper_vFixOrAddOp'] = array_filter($data['upper_vFixOrAddOp']);
            $data['upper_vClaspsOp'] = array_filter($data['upper_vClaspsOp']);

            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['upper_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['upper_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['upper_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $lower_delivery_date = $data['lower_product']->dDeliveryDate;
            if ($data['lower_product']->eRushDate == "Yes") {
                $data["rush"] = "Yes";
            }
        }
        
        if ($data["rush"] == "No") {
            if (!empty($upper_delivery_date) AND empty($lower_delivery_date)) {
                $data['final_delivery_date'] = $upper_delivery_date;
                $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
            }else if(!empty($lower_dilevery_date) AND empty($upper_delivery_date)){
                $data['final_delivery_date'] = $lower_delivery_date;
                $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
            }else if (!empty($upper_delivery_date) && !empty($lower_delivery_date)) {
                // dd($lower_delivery_date);
                if ($upper_delivery_date > $lower_delivery_date) {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                } else if ($upper_delivery_date < $lower_delivery_date) {
                    $data['final_delivery_date'] = $lower_delivery_date;
                    $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
                } else if ($upper_delivery_date == $lower_delivery_date) {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                }
            }
        }else{
            
        }
        $data["Office"] = Office::get_office_by_customerid();
        $data['Upper_category'] = Category::get_category_by_type("Upper");
        $data['Lower_category'] = Category::get_category_by_type("Lower");
        $data['toothshade'] = ToothShades::get_all_toothshades();
        $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        $data['toothbrand'] = ToothBrand::get_all_brand();

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

        // dd($data);
        

        return view('admin.labcase.new_stage')->with($data);

    }
    public function new_stage_lab($iCaseId,$iSlipId)
    {   
        $iCustomerId = General::admin_info()['iCustomerId'];
        
        $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);
        
        $office = array();
        
        foreach ($labOffice as $key => $value) {
            $office[] = Customer::get_by_id($value->iOfficeId);
        }
        $data['office'] = $office;
        $iOfficeId = $labOffice[0]->iOfficeId;
        $data["doctor"] =   Doctor::get_doc_by_office($iOfficeId);
        // $data['Upper_category'] = Category::get_category_by_type("Upper");
        // $data['Lower_category'] = Category::get_category_by_type("Lower");
        $data['toothshade'] = ToothShades::get_all_toothshades();
        // $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        // $data['toothbrand'] = ToothBrand::get_all_brand();
        // $data['impression'] = Impression::get_all_impressions();
        // dd($data['impression']);
        $data['iLoggedinId']=$iCustomerId;

        // $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        // $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

        $schedule = Schedule::get_all_schedule();
        foreach($schedule as $value){
            if (!empty($value->dtHolidays) AND empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                $holiday_arr = explode("-",$value->dtHolidays);
                $holidays[] = $holiday_arr[1]."/".$holiday_arr[2]."/".$holiday_arr[0];
            }
        }
        if(!empty($holidays)){
            $data['holidays'] = implode(",",$holidays);
        }

        $admin = Session::get('data');
        if(empty($admin['user_id']))
        {
            Redirect::to('/admin/login')->send();
        }else{
            // $iAdminId = $admin['user_id'];
            // $admin    = Admin::get_by_id($iAdminId);
           if(isset($admin['vFirstName']) && !empty($admin['vFirstName']) && isset($admin['vLastName']) && !empty($admin['vLastName']))
              {
                $data['user_name'] = $admin['vFirstName'].' '.$admin['vLastName'];
              }
              else
              {
                $data['user_name'] = $admin['vName'];
              }
                
            // $data['AdminId'] = $iAdminId;
            $data['AdminId'] = $admin['user_id'];
        }

        $caseid = Labcase::get_office_id();
        if(!empty($caseid->iCaseId)) {
            $id = $caseid->iCaseId+1;
        }else{           
            $id = 1;
        }
        $data['case_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $slipid = Labcase::get_slip_id();
        if(!empty($slipid->iSlipId)) {
            $id = $slipid->iSlipId+1;
        }else{           
            $id = 1;
        }
        $data['slip_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $data['tampid'] =  str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);

        $data['Case'] = Labcase::get_by_id($iCaseId);
        $data['Slip'] = Slip::get_by_id($iSlipId);

        // For Security purpose start
        $iCustomerId = General::admin_info()['iCustomerId'];
        $customerType =General::admin_info()['customerType'];
        if(isset($customerType) && $customerType =='Office Admin')
        {
            if(isset($data['Slip']->iOfficeId) && !empty($data['Slip']->iOfficeId))
            {
                $MatchId = $data['Slip']->iOfficeId;
            }
            else
            {
                $MatchId = '';
            }
        }
        if(isset($customerType) && $customerType =='Lab Admin')
        {
            if(isset($data['Slip']->iLabId) && !empty($data['Slip']->iLabId))
            {
                $MatchId = $data['Slip']->iLabId;
            }
            else
            {
                $MatchId = '';
            }
        }
        if((isset($MatchId) && $MatchId != $iCustomerId && !empty($iCustomerId)) || (empty($MatchId)) || (empty($data['Case'])))
        {
            if(isset($customerType) && $customerType =='Office Admin')
            {
                return redirect()->route('admin.labcase.office_admin_listing')->withError('Invalid Input');
            }
            elseif(isset($customerType) && $customerType =='Lab Admin')
            {
                return redirect()->route('admin.labcase')->withError('Invalid Input');
            }
        }
        // For Security purpose end
        
        $data['upper_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Upper");
        $data['lower_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Lower");

        if(isset($data['lower_product']) && $data['lower_product']->vStageName == "Finish" && $data['lower_product']->eStatus == "Finished"){
            unset($data['lower_product']);
        }

        if(isset($data['upper_product']) && $data['upper_product']->vStageName == "Finish" && $data['upper_product']->eStatus == "Finished"){
            unset($data['upper_product']);
        }
        

        // $data['doctor'] = Doctor::get_doc_by_office($data['Case']->iOfficeId);

        $data["rush"] = "No";

        $iLabId = $data['Slip']->iLabId;
        $data["Upper_category"] =   Category::get_category_by_type_customer('Upper',$iLabId);
        $data["Lower_category"] =   Category::get_category_by_type_customer('Lower',$iLabId);

        $data['toothbrand'] = ToothBrand::get_all_brand_by_customerid($iLabId);

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type('Upper',$iLabId);
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type('Lower',$iLabId);
     
        $criteria_new = array();
        $criteria_new['iLabId'] = $iLabId;
        $data["gumshade"] =   GumShades::get_product_by_gumbrand($criteria_new);

        $data["impression"] = Impression::get_all_impressions_by_customer($iLabId);

        if (!empty($data['upper_product'])) {



            $data['upper_vTeethInMouth'] = explode(",",$data['upper_product']->vTeethInMouth);
            $data['upper_vMissingTeeth'] = explode(",",$data['upper_product']->vMissingTeeth);
            $data['upper_vWillExtractOnDelivery'] = explode(",",$data['upper_product']->vWillExtractOnDelivery);
            $data['upper_vHasBeenExtracted'] = explode(",",$data['upper_product']->vHasBeenExtracted);
            $data['upper_vFixOrAdd'] = explode(",",$data['upper_product']->vFixOrAdd);
            $data['upper_vClasps'] = explode(",",$data['upper_product']->vClasps);

            $upper_teeth_in_mouth = array();
            $upper_extract= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

            $upper_merged_array = array_merge(
                $data['upper_vMissingTeeth'],
                $data['upper_vWillExtractOnDelivery'],
                $data['upper_vHasBeenExtracted'],
                $data['upper_vFixOrAdd'],
                $data['upper_vClasps']
            );

            foreach ($upper_extract as $value) {
                if (!in_array($value,$upper_merged_array)) {
                    $upper_teeth_in_mouth[] = $value;
                }
            }
            
            $data['upper_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['upper_vTeethInMouth'],
                    $upper_teeth_in_mouth
                )
            );

            $data['lower_vTeethInMouthOp'] = explode(",",$data['upper_product']->vTeethInMouthOp);
            $data['lower_vMissingTeethOp'] = explode(",",$data['upper_product']->vMissingTeethOp);
            $data['lower_vWillExtractOnDeliveryOp'] = explode(",",$data['upper_product']->vWillExtractOnDeliveryOp);
            $data['lower_vHasBeenExtractedOp'] = explode(",",$data['upper_product']->vHasBeenExtractedOp);
            $data['lower_vFixOrAddOp'] = explode(",",$data['upper_product']->vFixOrAddOp);
            $data['lower_vClaspsOp'] = explode(",",$data['upper_product']->vClaspsOp);

            if (!empty($data['upper_product']->lower_vTeethInMouthOp) AND !empty($data['upper_product']->lower_vMissingTeethOp) AND !empty($data['upper_product']->lower_vWillExtractOnDeliveryOp) AND !empty($data['upper_product']->lower_vHasBeenExtractedOp) AND !empty($data['upper_product']->lower_vFixOrAddOp) AND !empty($data['upper_product']->lower_vClaspsOp)) {

                


                $lower_teeth_in_mouthOp = array();
                $lower_extractOp= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");

                $lower_merged_arrayOp = array_merge(
                    $data['lower_vMissingTeethOp'],
                    $data['lower_vWillExtractOnDeliveryOp'],
                    $data['lower_vHasBeenExtractedOp'],
                    $data['lower_vFixOrAddOp'],
                    $data['lower_vClaspsOp']
                );

                foreach ($lower_extractOp as $value) {
                    if (!in_array($value,$lower_merged_arrayOp)) {
                        $lower_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['lower_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['lower_vTeethInMouthOp'],
                        $lower_teeth_in_mouthOp
                    )
                );
            }
            $data['lower_vTeethInMouthOp'] = array_filter($data['lower_vTeethInMouthOp']);
            $data['lower_vMissingTeethOp'] = array_filter($data['lower_vMissingTeethOp']);
            $data['lower_vWillExtractOnDeliveryOp'] = array_filter($data['lower_vWillExtractOnDeliveryOp']);
            $data['lower_vHasBeenExtractedOp'] = array_filter($data['lower_vHasBeenExtractedOp']);
            $data['lower_vFixOrAddOp'] = array_filter($data['lower_vFixOrAddOp']);
            $data['lower_vClaspsOp'] = array_filter($data['lower_vClaspsOp']);

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['lower_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['lower_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['lower_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $upper_delivery_date = $data['upper_product']->dDeliveryDate;
            if ($data['upper_product']->eRushDate == "Yes") {
                $data["rush"] = "Yes";
            }
        }

        if (!empty($data['lower_product'])) {

            $data['lower_vTeethInMouth'] = explode(",",$data['lower_product']->vTeethInMouth);
            $data['lower_vMissingTeeth'] = explode(",",$data['lower_product']->vMissingTeeth);
            $data['lower_vWillExtractOnDelivery'] = explode(",",$data['lower_product']->vWillExtractOnDelivery);
            $data['lower_vHasBeenExtracted'] = explode(",",$data['lower_product']->vHasBeenExtracted);
            $data['lower_vFixOrAdd'] = explode(",",$data['lower_product']->vFixOrAdd);
            $data['lower_vClasps'] = explode(",",$data['lower_product']->vClasps);

            
            $teeth_in_mouth = array();
            $lower_extract= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");
            
            $merged_array = array_merge(
                $data['lower_vMissingTeeth'],
                $data['lower_vWillExtractOnDelivery'],
                $data['lower_vHasBeenExtracted'],
                $data['lower_vFixOrAdd'],
                $data['lower_vClasps']
            );
            
            foreach ($lower_extract as $value) {
                if (!in_array($value,$merged_array)) {
                    $teeth_in_mouth[] = $value;
                }
            }

            $data['lower_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['lower_vTeethInMouth'],
                    $teeth_in_mouth
                )
            );


            $data['upper_vTeethInMouthOp'] = explode(",",$data['lower_product']->vTeethInMouthOp);
            $data['upper_vMissingTeethOp'] = explode(",",$data['lower_product']->vMissingTeethOp);
            $data['upper_vWillExtractOnDeliveryOp'] = explode(",",$data['lower_product']->vWillExtractOnDeliveryOp);
            $data['upper_vHasBeenExtractedOp'] = explode(",",$data['lower_product']->vHasBeenExtractedOp);
            $data['upper_vFixOrAddOp'] = explode(",",$data['lower_product']->vFixOrAddOp);
            $data['upper_vClaspsOp'] = explode(",",$data['lower_product']->vClaspsOp);

            if (!empty($data['lower_product']->upper_vTeethInMouthOp) AND !empty($data['lower_product']->upper_vMissingTeethOp) AND !empty($data['lower_product']->upper_vWillExtractOnDeliveryOp) AND !empty($data['lower_product']->upper_vHasBeenExtractedOp) AND !empty($data['lower_product']->upper_vFixOrAddOp) AND !empty($data['lower_product']->upper_vClaspsOp)) {

                $upper_teeth_in_mouthOp = array();
                $upper_extractOp= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

                $upper_merged_arrayOp = array_merge(
                    $data['upper_vMissingTeethOp'],
                    $data['upper_vWillExtractOnDeliveryOp'],
                    $data['upper_vHasBeenExtractedOp'],
                    $data['upper_vFixOrAddOp'],
                    $data['upper_vClaspsOp']
                );

                foreach ($upper_extractOp as $value) {
                    if (!in_array($value,$upper_merged_arrayOp)) {
                        $upper_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['upper_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['upper_vTeethInMouthOp'],
                        $upper_teeth_in_mouthOp
                    )
                );
            }
            $data['upper_vTeethInMouthOp'] = array_filter($data['upper_vTeethInMouthOp']);
            $data['upper_vMissingTeethOp'] = array_filter($data['upper_vMissingTeethOp']);
            $data['upper_vWillExtractOnDeliveryOp'] = array_filter($data['upper_vWillExtractOnDeliveryOp']);
            $data['upper_vHasBeenExtractedOp'] = array_filter($data['upper_vHasBeenExtractedOp']);
            $data['upper_vFixOrAddOp'] = array_filter($data['upper_vFixOrAddOp']);
            $data['upper_vClaspsOp'] = array_filter($data['upper_vClaspsOp']);

            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['upper_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['upper_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['upper_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $lower_delivery_date = $data['lower_product']->dDeliveryDate;
            if ($data['lower_product']->eRushDate == "Yes") {
                $data["rush"] = "Yes";
            }
        }
        
        if ($data["rush"] == "No") {
            if (!empty($upper_delivery_date) AND empty($lower_delivery_date)) {
                $data['final_delivery_date'] = $upper_delivery_date;
                $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
            }else if(!empty($lower_dilevery_date) AND empty($upper_delivery_date)){
                $data['final_delivery_date'] = $lower_delivery_date;
                $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
            }else if (!empty($upper_delivery_date) && !empty($lower_delivery_date)) {
                // dd($lower_delivery_date);
                if ($upper_delivery_date > $lower_delivery_date) {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                } else if ($upper_delivery_date < $lower_delivery_date) {
                    $data['final_delivery_date'] = $lower_delivery_date;
                    $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
                } else if ($upper_delivery_date == $lower_delivery_date) {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                }
            }
        }else{
            
        }
        $data["Office"] = Office::get_office_by_customerid();
        $data['Upper_category'] = Category::get_category_by_type("Upper");
        $data['Lower_category'] = Category::get_category_by_type("Lower");
        $data['toothshade'] = ToothShades::get_all_toothshades();
        $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        $data['toothbrand'] = ToothBrand::get_all_brand();

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

        // dd($data);
        

        return view('admin.labcase.new_stage_lab')->with($data);

    }
    public function getofficedoc(Request $request)
    {
        $iOfficeId = $request->iOfficeId;
        $data["data"] =   Doctor::get_doc_by_office($iOfficeId);
        
        return view('admin.labcase.ajax_office_doctor')->with($data);
    }
    public function ajax_listing(Request $request)
    {
        $action = $request->action;
        $iLabId = General::admin_info()['iCustomerId'];
        // dd($iLabId);
        $iDoctorId = $request->iDoctorId;
    
        $eStatus = $request->eStatus;
        
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "slip.dDeliveryDate";
            $order = "ASC";
        }
        
        if($action == "search"){
            $vKeyword = $request->keyword;
            $iOfficeId_list = $request->iOfficeId_list;
            $iDoctorId_list = $request->iDoctorId_list;
            $vLocation_list = $request->vLocation_list;
            $eStatus_list = $request->eStatus_list;
            
            $data_filter = $request->all(); 
            foreach($data_filter as $key => $value)
            {
                if(!empty($value) && $value !=null)
                {
                    $criteria[$key] = $value;
                }
            }
            Session::put('criteria', $criteria); 
        } 
        else 
        {
            $criteria_sess = Session::get('criteria');
            // dd($criteria_sess);
            if(isset($criteria_sess) && $criteria_sess !='')
            {
                if(isset($criteria_sess['keyword']) && $criteria_sess['keyword'] !='')
                {
                    $vKeyword = $criteria_sess['keyword'];
                }
                else
                {
                    $vKeyword = '';
                }   
                if(isset($criteria_sess['iOfficeId_list']) && $criteria_sess['iOfficeId_list'] !='')
                {
                    $iOfficeId_list = $criteria_sess['iOfficeId_list'];
                }   
                else
                {
                    $iOfficeId_list = '';
                }
                if(isset($criteria_sess['iDoctorId_list']) && $criteria_sess['iDoctorId_list'] !='')
                {
                    $iDoctorId_list = $criteria_sess['iDoctorId_list'];
                } 
                else
                {
                    $iDoctorId_list = '';
                }
                if(isset($criteria_sess['vLocation_list']) && $criteria_sess['vLocation_list'] !='')
                {
                    $vLocation_list = $criteria_sess['vLocation_list'];
                } 
                else
                {
                    $vLocation_list = '';
                }
                if(isset($criteria_sess['eStatus_list']) && $criteria_sess['eStatus_list'] !='')
                {
                    $eStatus_list = $criteria_sess['eStatus_list'];
                } 
                else
                {
                    $eStatus_list = '';
                }
            } 
            else
            {
                $vKeyword = $request->keyword;
                $iOfficeId_list = $request->iOfficeId_list;
                $iDoctorId_list = $request->iDoctorId_list;
                $vLocation_list = $request->vLocation_list;
                $eStatus_list = $request->eStatus_list;
            }
        }
         
        
        if($action == "delete"){
            $where                 = array();
            $where['iCaseId'] = $request->id;
            $data['eIsDeleted'] = 'Yes';
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            Labcase::update_data($where,$data);
        }
        
        if($action == "multiple_delete"){
            $Case_ID = (explode(",",$request->id));
            
            foreach ($Case_ID as $key => $value) {
                $where                 = array();
                $where['iCaseId']    = $value;
                
                Labcase::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['iOfficeId_list']   = $iOfficeId_list;
        $criteria['iDoctorId_list']   = $iDoctorId_list;
        $criteria['vLocation_list']   = $vLocation_list;
        if(isset($request->eStatus_list) && !empty($request->eStatus_list))
        {
            $criteria['eStatus_list']     = $eStatus_list;
        }
        else
        {
            $criteria['eStatus_list']     = 'On Process';
        }
        $criteria['column']     = $column;
        $criteria['order']      = $order;

      
        // dd($criteria);
        $limit = 9;
        $criteria['paging']     = false;  

        $criteria['iLabId'] = $iLabId;
        $criteria['iDoctorId'] = $iDoctorId;
        $criteria['eStatus'] = $eStatus;
        // dd($criteria);
        
        $Case_data = Labcase::get_all_data($criteria);
        if(!empty($Case_data))
        {
            $office_codes = array(); 
            foreach ($Case_data as $key => $value) {
                $office_codes[] = $value->vOffice__Code;
            }
        }
        $data['office_codes'] = implode(" ",array_unique($office_codes));
        // echo "<pre>"; print_r($Case_data); exit;
        // $Case_data = Labcase::all();
       
        $pages = 1;
        if($request->pages != "")
        {
            $pages = $request->pages;
        }
        $paginator = new Paginator($pages);
        $paginator->total = count($Case_data);
        
        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_labcase', $per_page);
            $paginator->itemsPerPage = $per_page;
            // //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_labcase');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                // //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                // //$paginator->range = 50;
            }
            
        }
        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $limit = $request->limit_page;
        }
        else
        {
            $limit = $paginator->itemsPerPage;
        }
        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iLabId'] = $iLabId;
        $criteria['iDoctorId'] = $iDoctorId;
        $criteria['eStatus'] = $eStatus;
        // dd($criteria);
        
        $slip_data = Labcase::get_all_data($criteria,$start,$limit,$paging);
       
        $total_rec = Session::get('limit_filter_labcase_lab');
        
        if($total_rec !='' && $total_rec != null )
        {
            $paginator->total = $total_rec;
        }
        else
        {
            $paginator->total = count($Case_data);
        }
        $data['paging'] = $paginator->paginate();
        
        // dd($data['paging']);
        // $new_data_delivered = array();

        // $count = count($slip_data);

        // // $count--;
        // foreach ($slip_data as $key => $value) {
        //     if  ($value->vLocation == "In office"){
        //         $new_data_delivered = $slip_data[$key];
        //         unset($slip_data[$key]);  
        //         $slip_data[$count] = $new_data_delivered;
        //         $count++;  
        //     }
        // }

        $data['data'] = $slip_data;
        $data['limit']  = $limit;

        return view('admin.labcase.ajax_listing_lab')->with($data);   
    }
    
    public function ajax_listing_office(Request $request)
    {
        $action = $request->action;
        $iOfficeId = General::admin_info()['iCustomerId'];
        $iDoctorId = $request->iDoctorId;
        
        $eStatus = $request->eStatus;
        
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "slip.dDeliveryDate";
            $order = "ASC";
        }
        
        if($action == "search"){
            $vKeyword = $request->keyword;
            $iLabId_list = $request->iLabId_list;
            $iDoctorId_list = $request->iDoctorId_list;
            $vLocation_list = $request->vLocation_list;
            $eStatus_list = $request->eStatus_list;
            
            $data_filter = $request->all(); 
            foreach($data_filter as $key => $value)
            {
                if(!empty($value) && $value !=null)
                {
                    $criteria[$key] = $value;
                }
            }
            Session::put('criteria', $criteria); 
        } 
        else 
        {
            $criteria_sess = Session::get('criteria');
            // dd($criteria_sess);
            if(isset($criteria_sess) && $criteria_sess !='')
            {
                if(isset($criteria_sess['keyword']) && $criteria_sess['keyword'] !='')
                {
                    $vKeyword = $criteria_sess['keyword'];
                }
                else
                {
                    $vKeyword = '';
                }   
                if(isset($criteria_sess['iLabId_list']) && $criteria_sess['iLabId_list'] !='')
                {
                    $iLabId_list = $criteria_sess['iLabId_list'];
                }   
                else
                {
                    $iLabId_list = '';
                }
                if(isset($criteria_sess['iDoctorId_list']) && $criteria_sess['iDoctorId_list'] !='')
                {
                    $iDoctorId_list = $criteria_sess['iDoctorId_list'];
                } 
                else
                {
                    $iDoctorId_list = '';
                }
                if(isset($criteria_sess['vLocation_list']) && $criteria_sess['vLocation_list'] !='')
                {
                    $vLocation_list = $criteria_sess['vLocation_list'];
                } 
                else
                {
                    $vLocation_list = '';
                }
                if(isset($criteria_sess['eStatus_list']) && $criteria_sess['eStatus_list'] !='')
                {
                    $eStatus_list = $criteria_sess['eStatus_list'];
                } 
                else
                {
                    $eStatus_list = '';
                }
            } 
            else
            {
                $vKeyword = $request->keyword;
                $iLabId_list = $request->iLabId_list;
                $iDoctorId_list = $request->iDoctorId_list;
                $vLocation_list = $request->vLocation_list;
                $eStatus_list = $request->eStatus_list;
            }
        }
        
        
        if($action == "delete"){
            $where                 = array();
            $where['iCaseId']    = $request->id;
            
            $data['eIsDeleted'] = 'Yes';
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            Labcase::update_data($where,$data);
        }
        
        if($action == "multiple_delete"){
            $Case_ID = (explode(",",$request->id));
            
            foreach ($Case_ID as $key => $value) {
                $where                 = array();
                $where['iCaseId']    = $value;
                
                Labcase::delete_by_id($where);
            }
        }
        
        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['iLabId_list']   = $iLabId_list;
        $criteria['iDoctorId_list']   = $iDoctorId_list;
        $criteria['vLocation_list']   = $vLocation_list;
        if(isset($request->eStatus_list) && !empty($request->eStatus_list))
        {
            $criteria['eStatus_list']     = $eStatus_list;
        }
        else
        {
            $criteria['eStatus_list']     = 'On Process';
        }
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['iOfficeId'] = $iOfficeId;
        
        // dd($criteria);
        $limit = 9;
        $criteria['paging']     = false;  
        $Case_data = Labcase::get_all_data_office($criteria);
  
        $pages = 1;
        if($request->pages != "")
        {
            $pages = $request->pages;
        }
        $paginator = new Paginator($pages);
        $paginator->total = count($Case_data);
        
        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_labcase', $per_page);
            $paginator->itemsPerPage = $per_page;
            // //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_labcase');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                // //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                // //$paginator->range = 50;
            }
            
        }
        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        
        if($request->limit_page !='')
        {
            $limit = $request->limit_page;
        }
        else
        {
            $limit = $paginator->itemsPerPage;
        }
        
        $paginator->is_ajax = true;
        $paging = true;
        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iOfficeId'] = $iOfficeId;
        $criteria['iDoctorId'] = $iDoctorId;
        $criteria['eStatus'] = $eStatus;
        $slip_data = Labcase::get_all_data_office($criteria,$start,$limit,$paging);
        
        
        $total_rec = Session::get('limit_filter_labcase');
        if($total_rec !='' && $total_rec != null )
        {
            $paginator->total = $total_rec;
        }
        else
        {
            $paginator->total = count($Case_data);
        }
        $data['paging'] = $paginator->paginate();

        $new_data_delivered = array();

        // $count = count($slip_data);

        // // $count--;
        // foreach ($slip_data as $key => $value) {
        //     if  ($value->vLocation == "In office"){
        //         $new_data_delivered = $slip_data[$key];
        //         unset($slip_data[$key]);  
        //         $slip_data[$count] = $new_data_delivered;
        //         $count++;  
        //     }
        // }
        $data['limit']  = $limit;
        $data['data'] = $slip_data;
        return view('admin.labcase.ajax_listing')->with($data);   
    }
    
    public function create()
    {
        $data["Office"] = Office::get_office_by_customerid();
        $data['Upper_category'] = Category::get_category_by_type("Upper");
        $data['Lower_category'] = Category::get_category_by_type("Lower");
        $data['toothshade'] = ToothShades::get_all_toothshades();
        $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        $data['toothbrand'] = ToothBrand::get_all_brand();

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

        return view('admin.labcase.create')->with($data);
    }
    
    
    public function get_impressions(Request $request)
    {
        
        $iLabId             = $request->iLabId;
        $data['eType']      = $request->eType;
        $data["impression"] = Impression::get_all_impressions_by_customer($iLabId);
        return view('admin.labcase.ajax_impression')->with($data);
    }


    public function getdoc(Request $request)
    {
        
        $iOfficeId = $request->iOfficeId;
        $data["Doctor"] =   Doctor::get_doc_by_office($iOfficeId);
        if(isset($request->isFilter) && $request->isFilter == 'Yes')
        {
            $data['is_filter']='Yes';
        }
        else
        {
            $data['is_filter']='No';
        }
        return view('admin.labcase.ajax_doctor',$data)->with($data);
    }


    public function getdoc_2(Request $request) //Krupesh Thakkar New Change
    {
        $iCustomerId = General::admin_info()['iCustomerId'];
        $data["Doctor"] =   Doctor::get_doc_by_customerid($iCustomerId);
        return view('admin.labcase.ajax_doctor',$data)->with($data);
    }

    public function get_addons_category(Request $request)
    {
        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type($request->eType,$request->iLabId);
        return view('admin.labcase.ajax_addons_category',$data)->with($data);
    }
    public function getcategory(Request $request)
    {
        $type = $request->type;
        $iLabId = $request->iLabId;
        $data["Category"] =   Category::get_category_by_type_customer($type,$iLabId);
        return view('admin.labcase.ajax_category',$data)->with($data);
    }
    
    public function getproduct(Request $request)
    {
        $iCategoryId = $request->iCategoryId;
        $eType = $request->eType;
        $data["Product"] =   CategoryProduct::get_product_by_category($iCategoryId,$eType);

        return view('admin.labcase.ajax_product',$data)->with($data);
    }
    
    public function getstage(Request $request)
    {
        $iCategoryProductId = $request->iCategoryProductId;
        $iCategoryId        = $request->iCategoryId;
        $eType              = $request->eType;
        
        $criteria                       = array();
        $criteria['iCategoryProductId'] = $iCategoryProductId;
        $criteria['iCategoryId']        = $iCategoryId;
        $criteria['eType']              = $eType;
        
        $data["Stage"] = ProductStage::get_all_for_slip($criteria);   
        
        return view('admin.labcase.ajax_stage',$data)->with($data);
    }
    public function getstage_2(Request $request)
    {
        $iCaseId            = $request->iCaseId;
        $iCategoryProductId = $request->iCategoryProductId;
        $iCategoryId        = $request->iCategoryId;
        $eType              = $request->eType;

        $upper_slips = Slip::get_all_by_case($iCaseId);

        $upper_older_slipids = array();

        foreach ($upper_slips as $key => $value) {
            $upper_older_slipids[] = $value->iSlipId;
        }

        $upper_slip_product = SlipProduct::get_for_all_slip($upper_older_slipids,$eType);
        
        foreach ($upper_slip_product as $key => $value) {
            $upper_older_stageids[] = $value->iStageId;
        }
        $upper_older_stageids = array_unique($upper_older_stageids);
        $sequest_max = ProductStage::get_all_by_stageIds($upper_older_stageids);

        $criteria                       = array();
        $criteria['iCategoryProductId'] = $iCategoryProductId;
        $criteria['iProductStageId']    = $upper_older_stageids;
        $criteria['iCategoryId']        = $iCategoryId;
        $criteria['eType']              = $eType;
        $criteria['iSequence']          = $sequest_max->iSequence;
    
        $FinishStages = ProductStage::get_data_for_finish_stage($criteria);
        if (count($FinishStages) > 0) {
            $data['Stage'] = $FinishStages;
        }else{
            array_unique($upper_older_stageids);
            unset($upper_older_stageids[0]);
            $criteria['iProductStageId'] = $upper_older_stageids;
            $data['Stage'] = ProductStage::get_all_for_new_stage($criteria);
             
        }
        return view('admin.labcase.ajax_stage',$data)->with($data);
        
    }

    public function getOldStage(Request $request)
    {
        $iCaseId            = $request->iCaseId;
        $iCategoryProductId = $request->iCategoryProductId;
        $iCategoryId        = $request->iCategoryId;
        $eType              = $request->eType;
      
        $upper_slips = Slip::get_all_by_case($iCaseId);
        $upper_older_slipids = array();
        
        foreach ($upper_slips as $key => $value) {
            $upper_older_slipids[] = $value->iSlipId;
        }
        
        $upper_slip_product = SlipProduct::get_for_all_for_old_slips($upper_older_slipids,$eType);
        
        // dd($upper_slip_product);
        
        foreach ($upper_slip_product as $key => $value) {
            $upper_older_stageids[] = $value->iStageId;
            $upper_older_slipids[] = $value->iSlipId;
        }
        
        $criteria                       = array();
        $criteria['iCategoryProductId'] = $iCategoryProductId;
        
        $criteria['iCategoryId']        = $iCategoryId;
        $criteria['eType']              = $eType;
        $Stage = array();
        
        foreach ($upper_older_stageids as $key => $value) {
            
            $criteria['iProductStageId']    = $value;
            $oneslip = ProductStage::get_all_for_old_stage($criteria);
            $driver_history = DriverHistory::show_history(array('iSlipId'=>$upper_older_slipids[$key],'vLocation'=>'On route to the office'));
            // dd($upper_older_stageids);
            // $oneslip->vName = $driver_history[0]->vStageName;
            $oneslip->dtAddedDate = $driver_history[0]->dtAddedDate;
            $Stage[] = $oneslip;
        }
        $data['Stage'] = $Stage;
        $data['eType'] = $eType;
        
        return view('admin.labcase.ajax_old_stage',$data)->with($data);
    }
    
    public function getgrade(Request $request)
    {
        $iCategoryProductId = $request->iCategoryProductId;
        $Grade =   ProductGrade::get_grade_by_product($iCategoryProductId);   
        $iGradeId = array();
        foreach ($Grade as $key => $value) {
            $iGradeId[] = $value->iGradeId;
        }

        if (!empty($iGradeId)) {
            $data["Grade"] =   Grade::get_grade_by_ids($iGradeId);
            return view('admin.labcase.ajax_grade',$data)->with($data);
        }else{
            echo "1";
        }

    }

    public function get_new_stage_details(Request $request)
    {
        $iProductStageId    = $request->iStageId;
        $iCaseId            = $request->iCaseId;
        $eType              = $request->eType;

        $upper_slips        = Slip::get_all_by_case($iCaseId);

        $upper_older_slipids = array();

        foreach ($upper_slips as $key => $value) {
            $upper_older_slipids[] = $value->iSlipId;
        }

        $upper_slip_product = SlipProduct::get_for_all_slip($upper_older_slipids,$eType);

        foreach ($upper_slip_product as $key => $value) {
            $upper_older_stageids[] = $value->iStageId;
        }
        // dd($upper_older_stageids);

        $old_attr = array();

        foreach ($upper_older_stageids as $key => $value) {
            $stage_data = ProductStage::get_by_id($value);

            if ($stage_data->eTeethShades == "Yes") {
                $old_attr[] = "eTeethShades";
            }
            if ($stage_data->eGumShades == "Yes") {
                $old_attr[] = "eGumShades";
            }
            if ($stage_data->eImpressions == "Yes") {
                $old_attr[] = "eImpressions";
            }
            if ($stage_data->eExtractions == "Yes") {
                $old_attr[] = "eExtractions";
            }
        }

        $old_attr = array_unique($old_attr);

        $new_attr = array();
        $new_stage = ProductStage::get_by_id($iProductStageId);

        if ($new_stage->eTeethShades == "Yes") {
            $new_attr[] = "eTeethShades";
        }
        if ($new_stage->eGumShades == "Yes") {
            $new_attr[] = "eGumShades";
        }
        if ($new_stage->eImpressions == "Yes") {
            $new_attr[] = "eImpressions";
        }
        if ($new_stage->eExtractions == "Yes") {
            $new_attr[] = "eExtractions";
        }

        $required_attr_for_newstage = array_diff($new_attr,$old_attr);

        if (!empty($required_attr_for_newstage)) {
            
            $data["data"] = $required_attr_for_newstage;
            $data["olds"] = $old_attr;
        }else{
            $data["olds"] = $old_attr;
            $data["data"] = "1";
        }


        echo json_encode($data);
    }
    public function getshadetype(Request $request)
    {
        $iProductStageId    = $request->iProductStageId;
        $eType              = $request->eType;
        
        $criteria                       = array();
        $criteria['iProductStageId']    = $iProductStageId;
        $criteria['eType']              = $eType;
        
        $data = ProductStage::get_all_for_slip($criteria);

        return json_encode($data,true) ;
    }
    public function gettoothbrand(Request $request)
    {
        $iLabId = $request->iLabId;
        $data['toothbrand'] = ToothBrand::get_all_brand_by_customerid($iLabId);
        return view('admin.labcase.ajax_toothbrand',$data)->with($data);
    }
    public function getgumbrand(Request $request)
    {
        $iLabId = $request->iLabId;
        $iProductId = $request->iProductId;

        $GumBrand = array();

        if (isset($iProductId) && !empty($iProductId) ) {
            $gshades = ProductGumShades::get_by_id($iProductId);
            $gumshadeids = array();
            foreach ($gshades as $key => $value) {
                $gumshadeids[] = $value->iGumShadesId;
            }


            $GumShades = GumShades::get_all_by_id($gumshadeids);
            
            $gumbrandid = array();
            foreach ($GumShades as $key => $value) {
                $gumbrandid[] = $value->iGumBrandId;
            }

            $GumBrand = GumBrand::get_all_by_id($gumbrandid);
        }



        // $data['gumbrand'] = GumBrand::get_all_brand_by_customerid($iLabId);
        $data['gumbrand'] = $GumBrand;
        return view('admin.labcase.ajax_gumbrand',$data)->with($data);
    }
    public function gettoothshades(Request $request)
    {
        $iToothBrandId = $request->iToothBrandId;
        $eType = $request->eType;
        $data["toothshade"] =   ToothShades::get_product_by_toothbrand($iToothBrandId);
        return view('admin.labcase.ajax_toothshade',$data)->with($data);
    }
    public function getgumshades(Request $request)
    {
        $iGumBrandId = $request->iGumBrandId;
        $iProductId = $request->iProductId;
        $iLabId = $request->iLabId;
        $eType = $request->eType;

        $criteria = array();
        $criteria['iProductId']     = $iProductId;
        $criteria['iGumBrandId']    = $iGumBrandId;
        $criteria['iLabId']         = $iLabId;

        $data["gumshade"] =   GumShades::get_product_by_gumbrand($criteria);
        return view('admin.labcase.ajax_gumshade',$data)->with($data);
    }
    public function getcategoryaddons(Request $request)
    {
        $iAddCategoryId = $request->iAddCategoryId;
        $eType = $request->eType;
        $data["addons"] =   Subaddoncategory::get_addons_by_category($iAddCategoryId,$eType);
        return view('admin.labcase.ajax_addons',$data)->with($data);
    }
    
    public function store(Request $request)
    {
        $iCaseId = $request->iCaseId;
        $iSlipId = $request->iSlipId;
        if(isset($request->iLabId) && !empty($request->iLabId))
        {
            $iLabId = $request->iLabId;
        }
        else
        {
            $iLabId = General::admin_info()['iCustomerId'];
        }
        if(isset($request->iOfficeId) && !empty($request->iOfficeId))
        {
            $iOfficeId = $request->iOfficeId;
        }
        else
        {
            $iOfficeId = General::admin_info()['iCustomerId'];
        }
        $data['iOfficeId'] = $iOfficeId;
        $data['iLabId']     = $iLabId;
        // $data['iOfficeId']     = $request->iOfficeId;
        $data['iDoctorId']     = $request->iDoctorId;
        $data['vPatientName']  = $request->vPatientName;
        $data['vCasePanNumber']= $request->vCasePanNumber;
        // $data['vCaseNumber']   = $request->vCaseNumber;
        $data['iCreatedById']  = $request->iCreatedById;
        $data['vCreatedByName']= $request->vCreatedByName;
        $data['eCreatedByType']= $request->eCreatedByType;
        
        if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
            
            $data['eStatus']       = "On Process";
        }else{
            
            $data['eStatus']       = $request->eStatus;
        }
        if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
            $data['vLocation']     = "In office ready to pickup";
        }else{
            $data['vLocation']     = $request->vLocation;
        }
        
        if($iCaseId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        
        if(!empty($iCaseId)){
            // $where                      = array();
            // $where['iCaseId']       = $iCaseId;
            
            // $Case_id = new Labcase();
            // $Case_id->update($where, $data);
            // $allData = $request;

            //Slip Update

            // $this->UpdateSlipData($allData);
            
            // return redirect()->route('admin.labcase')->withSuccess('Slip updated successfully.');
        }
        else{
            if (!empty($request->iUpperCategoryId) OR !empty($request->iLowerCategoryId)) {
                $Case_id = Labcase::add($data);
             
                //  Set Session For Print Slip Start
                if(!empty($request->information_checkbox) && $request->information_checkbox == "Yes" && isset($Case_id) && $Case_id !='')
                {
                    Session::put('Sess_iCaseId', $Case_id);
                }
                //  Set Session For Print Slip End
            }else{
                return redirect()->back()->withError('something went wrong');
                return false;
            }

            $iSlipId                    = $request->iSlipId;
            $slip_data['iCaseId']       = $Case_id;
            $slip_data['vSlipNumber']   = $request->vSlipNumber;
            $slip_data['iOfficeId']     = $iOfficeId;
            $slip_data['iLabId']        = $iLabId;
            $slip_data['iDoctorId']     = $request->iDoctorId;
            $slip_data['vPatientName']  = $request->vPatientName;
            $slip_data['vCasePanNumber']= $request->vCasePanNumber;
            $slip_data['vCaseNumber']   = $request->vCaseNumber;
            $slip_data['iCreatedById']  = $request->iCreatedById;
            $slip_data['vCreatedByName']= $request->vCreatedByName;
            $slip_data['eCreatedByType']= $request->eCreatedByType;

            $OfficeData    = Customer::get_by_id($iOfficeId);
            $LabData       = Customer::get_by_id($iLabId);
  
            if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {

                $slip_data['eStatus']       = "On Process";
            }else{
                
                $slip_data['eStatus']       = $request->eStatus;
            }
            if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
                $slip_data['vLocation']     = "In office ready to pickup";
            }else{
                $slip_data['vLocation']     = $request->vLocation;
            }
            
            $slip_data['tStagesNotes']  =  $request->tDescription;
            


            if (isset($request->iUpperCategoryId) && isset($request->iUpperStageId) && !empty($request->iUpperCategoryId) && !empty($request->iUpperStageId)) {

                $upper_without_rush_111 = $this->product_dates($request->iUpperCategoryId,$request->iUpperStageId);

                if(!empty($upper_without_rush_111))
                {
                    $request->upper_pickup_date = $upper_without_rush_111['pic_date'];
                    $request->upper_delivery_date = $upper_without_rush_111['del_date'];
                }

            }

            if (isset($request->iLowerCategoryId) && isset($request->iLowerStageId) && !empty($request->iLowerCategoryId) && !empty($request->iLowerStageId)) {

                $lower_without_rush_111 = $this->product_dates($request->iLowerCategoryId,$request->iLowerStageId);

                if(!empty($lower_without_rush_111))
                {
                    $request->lower_pickup_date = $lower_without_rush_111['pic_date'];
                    $request->lower_delivery_date = $lower_without_rush_111['del_date'];
                }

            }

            $upper_dilevery_date =  $request->upper_delivery_date;
            $lower_dilevery_date =  $request->lower_delivery_date;

            $email_array= array();
            $email_array['vPatientName']    = $request->vPatientName;
            $email_array['vSlipNumber']     = $request->vSlipNumber;
            $email_array['LabName']         = $LabData->vOfficeName;
            $email_array['OfficeName']      = $OfficeData->vOfficeName;
       
            // Send email for same day service start

            if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
                if(!empty($upper_dilevery_date) && !empty($request->upper_pickup_date) && $upper_dilevery_date == $request->upper_pickup_date && !empty($request->iUpperCategoryId)){
                    $email_array['dDeliveryDate'] = $upper_dilevery_date;
                    $this->send_email_to_lab_admin($email_array);
                }elseif(!empty($lower_dilevery_date) && !empty($request->lower_pickup_date) && $lower_dilevery_date == $request->lower_pickup_date && !empty($request->iLowerCategoryId)){
                    $email_array['dDeliveryDate'] = $lower_dilevery_date;
                    $this->send_email_to_lab_admin($email_array);
                }
            }
            // Send email for same day service end
             

                if( $upper_dilevery_date > $lower_dilevery_date){
                    if(isset($request->upper_pickup_date) && $request->upper_pickup_date != null)
                    {
                        $slip_data['dPickUpDate']   =  date("Y-m-d ",strtotime($request->upper_pickup_date));
                    }
                    if(isset($request->upper_delivery_date) && $request->upper_delivery_date != null)
                    {
                        $slip_data['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
                    }
                    if(isset($request->upper_delivery_time) && $request->upper_delivery_time != null)
                    {
                        $slip_data['tDeliveryTime'] =  date("H:i",strtotime($request->upper_delivery_time));
                    }

                    $iCategoryId = $request->iUpperCategoryId;
                    $category_info = Category::get_by_id($iCategoryId);

                    $slip_data['iCasepanId']    = $category_info->iCasepanId;

                        $data =  array();
                        $where = array();

                        $data['iCasepanId'] = $slip_data['iCasepanId'];
                        $where['iCaseId']   = $Case_id;
                        
                        $Case_id_new = new Labcase();
                        $Case_id_new->update($where, $data);

                            $Casepan = Casepan::get_by_id($data['iCasepanId']);
                            $Casepannumber = CasepanNumber::get_by_casepan_use($Casepan->iCasepanId);
                            
                            if (!empty($Casepannumber)) {
                                $data =  array();
                                $where = array();

                                $data['vCasePanNumber'] = $Casepannumber->vNumber;
                                $where['iCaseId']       = $Case_id;
                                
                                $Case_id_new = new Labcase();
                                $Case_id_new->update($where, $data);

                                $data =  array();
                                $where = array();

                                $data['eUse']                =  "Yes";
                                $where['iCasepanNumberId']   =  $Casepannumber->iCasepanNumberId;
                                
                                $iCasepanNumberId = new CasepanNumber();
                                $iCasepanNumberId->update($where, $data);
                            }
                            
                }
            
                if( $upper_dilevery_date == $lower_dilevery_date){
                  
                    if(isset($request->upper_pickup_date) && $request->upper_pickup_date != null)
                    {
                        $slip_data['dPickUpDate']   =  date("Y-m-d ",strtotime($request->upper_pickup_date));
                    }
                    if(isset($request->upper_delivery_date) && $request->upper_delivery_date != null)
                    {
                        $slip_data['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
                    }
                    if(isset($request->upper_delivery_time) && $request->upper_delivery_time != null)
                    {
                        $slip_data['tDeliveryTime'] =  date("H:i",strtotime($request->upper_delivery_time));
                    }

                    $iCategoryId = $request->iUpperCategoryId;
                    $category_info = Category::get_by_id($iCategoryId);

                    $slip_data['iCasepanId']    = $category_info->iCasepanId;

                        $data =  array();
                        $where = array();

                        $data['iCasepanId'] = $slip_data['iCasepanId'];
                        $where['iCaseId']   = $Case_id;
                        
                        $Case_id_new = new Labcase();
                        $Case_id_new->update($where, $data);

                            $Casepan = Casepan::get_by_id($data['iCasepanId']);
                            if(isset($Casepan) && !empty($Casepan))
                            {
                                $Casepannumber = CasepanNumber::get_by_casepan_use($Casepan->iCasepanId);
                                
                                if (!empty($Casepannumber)) {
                                    $data =  array();
                                    $where = array();
    
                                    $data['vCasePanNumber'] = $Casepannumber->vNumber;
                                    $where['iCaseId']       = $Case_id;
                                    
                                    $Case_id_new = new Labcase();
                                    $Case_id_new->update($where, $data);
    
                                    $data =  array();
                                    $where = array();
    
                                    $data['eUse']                =  "Yes";
                                    $where['iCasepanNumberId']   =  $Casepannumber->iCasepanNumberId;
                                    
                                    $iCasepanNumberId = new CasepanNumber();
                                    $iCasepanNumberId->update($where, $data);
                                }
                            }
                }
                if( $upper_dilevery_date < $lower_dilevery_date){
                    
                    if(isset($request->lower_pickup_date) && $request->lower_pickup_date != null)
                    {
                        $slip_data['dPickUpDate']   =  date("Y-m-d ",strtotime($request->lower_pickup_date));
                    }
                    if(isset($request->lower_delivery_date) && $request->lower_delivery_date != null)
                    {
                        $slip_data['dDeliveryDate'] =  date("Y-m-d",strtotime($request->lower_delivery_date));
                    }
                    if(isset($request->lower_delivery_time) && $request->lower_delivery_time != null)
                    {
                        $slip_data['tDeliveryTime'] =  date("H:i",strtotime($request->lower_delivery_time));
                    }
                    
                    $iCategoryId = $request->iLowerCategoryId;
                    $category_info = Category::get_by_id($iCategoryId);
                    
                    $slip_data['iCasepanId']    = $category_info->iCasepanId;

                        $data =  array();
                        $where = array();

                        $data['iCasepanId'] = $slip_data['iCasepanId'];
                        $where['iCaseId']   = $Case_id;
                        
                            $Case_id_new = new Labcase();
                            $Case_id_new->update($where, $data);

                            $Casepan = Casepan::get_by_id($data['iCasepanId']);
                            $Casepannumber = CasepanNumber::get_by_casepan_use($Casepan->iCasepanId);

                            if (!empty($Casepannumber)) {
                                $data =  array();
                                $where = array();

                                $data['vCasePanNumber'] = $Casepannumber->vNumber;
                                $where['iCaseId']       = $Case_id;
                                
                                $Case_id_new = new Labcase();
                                $Case_id_new->update($where, $data);

                                $data =  array();
                                $where = array();

                                $data['eUse']                =  "Yes";
                                $where['iCasepanNumberId']   =  $Casepannumber->iCasepanNumberId;
                                
                                $iCasepanNumberId = new CasepanNumber();
                                $iCasepanNumberId->update($where, $data);
                            }
                }
            
            if($iSlipId){
                $slip_data['dtAddedDate'] = date("Y-m-d H:i:s");
            }else{
                $slip_data['dtAddedDate'] = date("Y-m-d H:i:s");
                $slip_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            }
            
            $Slip_id = Slip::add($slip_data);
            //  Set Session For Print Slip Start
            
            if(!empty($request->information_checkbox) && $request->information_checkbox == "Yes" && isset($Slip_id) && $Slip_id !='')
            {
                Session::put('Sess_iSlipId', $Slip_id);
            }
            //  Set Session For Print Slip End

            /* Attachment Upload*/
            $iTampId = $request->iTampId;
            $criteria 				 = array();
            $criteria['iTampId'] 	 = $iTampId;
            
            $gallary = SlipGallery::get_all_data($criteria);
           
            if(!empty($gallary))
            {     
                $data = array();           
                foreach ($gallary as $key => $value) 
                {
                    if($value->iTampId == $iTampId)
                    {
                        $data['iSlipId']     = $Slip_id;
                        $data['iCaseId']     = $Case_id;
                        $data['iTampId'] 	 = null;
                        $where                      = array();
                        $where['iSlipAttachmentId']= $value->iSlipAttachmentId;
                        
                        $SlipGallery_id = new SlipGallery();
                        $SlipGallery_id->update($where, $data);
                    }
                }
            }


            /* Attachment Upload*/

            

            /*Upper Product Start*/
            if (!empty($request->iUpperCategoryId)) {

                $iSlipProductId                     = $request->id;
                $slip_product_data['iSlipId']       = $Slip_id;
                $slip_product_data['eType']         = "Upper";
                $slip_product_data['iCategoryId']   = $request->iUpperCategoryId;
                $slip_product_data['iProductId']    = $request->iUpperProductId;
                $slip_product_data['iGradeId']      = $request->iUpperGradeId;
                $slip_product_data['iStageId']      = $request->iUpperStageId;
                $slip_product_data['eExtraction']   = $request->upper_extraction_stage;
                $slip_product_data['eGumShade']     = $request->upper_gumshade_stage;
                $slip_product_data['eTeethShade']   = $request->upper_teethshade_stage;
                $slip_product_data['eImpression']   = $request->upper_Impression_stage;
                // $slip_product_data['eRushDate']     = $request->upper_rush_date_stage;  
                $slip_product_data['iToothBrandId']  = $request->upper_tsbrand;
                $slip_product_data['iToothShadesId'] = $request->upper_tshade;
                $slip_product_data['iGumBrandId']    = $request->upper_gsbrand;
                $slip_product_data['iGumShadesId']   = $request->upper_gshade;
                $slip_product_data['vTeethInMouth']  = $request->upper_in_mouth_value;
                $slip_product_data['vMissingTeeth']  = $request->upper_missing_teeth_value;
                $slip_product_data['vWillExtractOnDelivery']= $request->upper_ectract_delivery_value;
                $slip_product_data['vHasBeenExtracted']= $request->upper_been_extracted_value;
                $slip_product_data['vFixOrAdd']      = $request->upper_fix_value;
                $slip_product_data['vClasps']        = $request->upper_clasps_value;
                

                if($iSlipProductId){
                    $slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
                }else{
                    $slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
                    $slip_product_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                }
    
                    $SlipProduct_id = SlipProduct::add($slip_product_data);

                /*Upper Impression Start*/
                $impression_data_array = $request->upper_impression_checkbox;
                if(!empty($impression_data_array)){
                    foreach ($impression_data_array as $key => $value) 
                    {
                        $impression_data = array();
                        $impression_data['iSlipId']      = $Slip_id;
                        $impression_data['iImpressionId'] = $value;
                        
                        $impression_data['iQuantity'] = $request->input("upper_impression_qty_".$value);
                        $impression_data['eType'] = "Upper";        
                        
                       
                        $product_addons = SlipImpression::add($impression_data);
                    


                    }
                }
                /*Upper Impression End*/

                /*Upper Addons Start*/
                $upper_add_on_cat_id = $request->upper_add_on_cat_id;
                $upper_add_on_id = $request->upper_add_on_id;
                $upper_add_on_qty = $request->upper_add_on_qty;
                if(!empty($upper_add_on_cat_id)){
                    foreach ($upper_add_on_cat_id as $key => $value) 
                    {
                        $upper_slip_product_addons = array();
                        $upper_slip_product_addons['iSlipId']      = $Slip_id;
                        $upper_slip_product_addons['iAddCategoryId'] = $value;
                        $upper_slip_product_addons['iCaseId'] = $Case_id;
                        $upper_slip_product_addons['iSubAddCategoryId'] = $upper_add_on_id[$key];
                        $upper_slip_product_addons['iQuantity'] = $upper_add_on_qty[$key];
                        $upper_slip_product_addons['eType'] = "Upper";

                         $product_addons = SlipAddons::add($upper_slip_product_addons);
                        
                    }
                }

                /*Opposite Impressions Start*/
                if (empty($request->iLowerCategoryId) AND $request->lower_eOpposingImpressions == "Yes") {
                    $lowerimpression_data_array = $request->lower_impression_checkbox;

                    if(!empty($lowerimpression_data_array)){
                        foreach ($lowerimpression_data_array as $key => $value) 
                        {

                            $lower_impression_qty = $request->input("lower_impression_qty_".$value);
                            $lower_impression_data = array();
                            $lower_impression_data['iSlipId']      = $Slip_id;
                            $lower_impression_data['iImpressionId'] = $value;

                            $lower_impression_data['iQuantity'] = $lower_impression_qty;
                            $lower_impression_data['eType'] = "Opposite";
                            
                                $product_addons = SlipImpression::add($lower_impression_data);
                        
                        }
                    }
                }
                /*Opposite Impressions End*/
                /*Opposite Extractions Start*/
                if (empty($request->iLowerCategoryId) AND $request->lower_eOpposingExtractions == "Yes") {
                    $lower_opposite_extraction = array();
                    $lower_opposite_extraction['vTeethInMouthOp']  = $request->lower_in_mouth_value;
                    $lower_opposite_extraction['vMissingTeethOp']  = $request->lower_missing_teeth_value;
                    // dd($request);
                    $lower_opposite_extraction['vWillExtractOnDeliveryOp']= $request->lower_ectract_delivery_value;
                    $lower_opposite_extraction['vHasBeenExtractedOp']= $request->lower_been_extracted_value;
                    $lower_opposite_extraction['vFixOrAddOp']      = $request->lower_fix_value;
                    $lower_opposite_extraction['vClaspsOp']        = $request->lower_clasps_value;

                    $where                      = array();
                    $where['iSlipProductId']       = $SlipProduct_id;

                    $Slip_product_id = new SlipProduct();
                    $Slip_product_id->update($where, $lower_opposite_extraction);

                }
                /*Opposite Extractions End*/


                /* Upper Dates Settings */
                $upper_pikup_final_date = "";
                $upper_without_rush = $this->product_dates($request->iUpperCategoryId,$request->iUpperStageId);
                if(!empty($upper_without_rush))
                {
                    $upper_pikup_final_date = $upper_without_rush['pic_date'];
                    $upper_date_update = array();
                    $upper_date_update['dPickUpDate'] = $upper_without_rush['pic_date'];
                    $upper_date_update['dDeliveryDate'] = $upper_without_rush['del_date'];
                    $upper_date_update['tDeliveryTime'] = $upper_without_rush['delivery_time'];
                    
                    $upper_date_update['dDeliveryDateFinal'] = $upper_without_rush['del_date'];
    
    
                    $where                         = array();
                    $where['iSlipProductId']       = $SlipProduct_id;
                    
                    $Slip_product_id = new SlipProduct();
                    $Slip_product_id->update($where, $upper_date_update);
    
                    if(!empty($request->upper_rush_feesid))
                    {    
                        $fees_data = Fees::get_by_id($request->upper_rush_feesid);
                        $output = $this->product_dates($request->iUpperCategoryId,$request->iUpperStageId, $fees_data->iWorkingTime);
                        
                        $upper_rushdate_update = array();
                        $upper_rushdate_update['dDeliveryRushDate'] = $output['del_date'];
                        $upper_rushdate_update['dDeliveryDateFinal'] = $output['del_date'];
                        $upper_rushdate_update['eRushDate'] = "Yes";
                        $upper_rushdate_update['iFeesId'] = $request->upper_rush_feesid;
                        
                        $where                         = array();
                        $where['iSlipProductId']       = $SlipProduct_id;
    
                        $Slip_product_id = new SlipProduct();
                        $Slip_product_id->update($where, $upper_rushdate_update);
    
                        $upper_slip_rushdate_update['dUpperRushDate'] = $output['del_date'];
    
                        $where                  = array();
                        $where['iSlipId']       = $Slip_id;
                        $Slip_id_Update = new Slip();
                        $Slip_id_Update->update($where, $upper_slip_rushdate_update);
                    }
                }
                /* Upper Dates Settings */

            }
            /*Upper Product End*/

            /*Lower Product Start*/
            if (!empty($request->iLowerCategoryId)) {
                $iSlipProductId                           = $request->id;
                $lower_slip_product_data['iSlipId']       = $Slip_id;
                $lower_slip_product_data['eType']         = "Lower";
                $lower_slip_product_data['iCategoryId']   = $request->iLowerCategoryId;
                $lower_slip_product_data['iProductId']    = $request->iLowerProductId;
                $lower_slip_product_data['iGradeId']      = $request->iLowerGradeId;
                $lower_slip_product_data['iStageId']      = $request->iLowerStageId;
                $lower_slip_product_data['eExtraction']   = $request->lower_extraction_stage;
                $lower_slip_product_data['eGumShade']     = $request->lower_gumshade_stage;
                $lower_slip_product_data['eTeethShade']   = $request->lower_teethshade_stage;
                $lower_slip_product_data['eImpression']   = $request->lower_Impression_stage;
                // $lower_slip_product_data['eRushDate']     = $request->lower_rush_date_stage;
                $lower_slip_product_data['iToothBrandId']  = $request->lower_tsbrand;
                $lower_slip_product_data['iToothShadesId'] = $request->lower_tshade;
                $lower_slip_product_data['iGumBrandId']    = $request->lower_gsbrand;
                $lower_slip_product_data['iGumShadesId']   = $request->lower_gshade;
                $lower_slip_product_data['vTeethInMouth']  = $request->lower_in_mouth_value;
                $lower_slip_product_data['vMissingTeeth']  = $request->lower_missing_teeth_value;
                $lower_slip_product_data['vWillExtractOnDelivery']= $request->lower_ectract_delivery_value;
                $lower_slip_product_data['vHasBeenExtracted']= $request->lower_been_extracted_value;
                $lower_slip_product_data['vFixOrAdd']      = $request->lower_fix_value;
                $lower_slip_product_data['vClasps']        = $request->lower_clasps_value;
                $lower_slip_product_data['eStatus']       =  $request->lower_status;
                
                if($iSlipProductId){
                    $lower_slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
                }else{
                    $lower_slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
                    $lower_slip_product_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    
                }
                    $SlipProduct_id = SlipProduct::add($lower_slip_product_data);


                /*Lower Impression Start*/
                $lowerimpression_data_array = $request->lower_impression_checkbox;

                if(!empty($lowerimpression_data_array)){
                    foreach ($lowerimpression_data_array as $key => $value) 
                    {

                        $lower_impression_qty = $request->input("lower_impression_qty_".$value);
                        $lower_impression_data = array();
                        $lower_impression_data['iSlipId']      = $Slip_id;
                        $lower_impression_data['iImpressionId'] = $value;
                        $lower_impression_data['iQuantity'] = $lower_impression_qty;
                        $lower_impression_data['eType'] = "Lower";
                        
                        $product_addons = SlipImpression::add($lower_impression_data);
                    
                    }
                }
                /*Lower Impression End*/

                /*Lower Addons Start*/
                $lower_add_on_cat_id = $request->lower_add_on_cat_id;
                $lower_add_on_id = $request->lower_add_on_id;
                $lower_add_on_qty = $request->lower_add_on_qty;
                if(!empty($lower_add_on_cat_id)){
                    foreach ($lower_add_on_cat_id as $key => $value) 
                    {
                        $lower_slip_product_addons = array();
                        $lower_slip_product_addons['iSlipId']      = $Slip_id;
                        $lower_slip_product_addons['iAddCategoryId'] = $value;
                        $lower_slip_product_addons['iCaseId'] = $Case_id;
                        $lower_slip_product_addons['iSubAddCategoryId'] = $lower_add_on_id[$key];
                        $lower_slip_product_addons['iQuantity'] = $lower_add_on_qty[$key];
                        $lower_slip_product_addons['eType'] = "Lower";
                                    
                            $product_addons = SlipAddons::add($lower_slip_product_addons);
                        
                    }
                }
                /*Lower Addons End*/


                /*Opposite Impressions Start*/
                if (empty($request->iUpperCategoryId) AND $request->upper_eOpposingImpressions == "Yes") {
                    $impression_data_array = $request->upper_impression_checkbox;
                    if(!empty($impression_data_array)){
                        foreach ($impression_data_array as $key => $value) 
                        {   
                            $impression_data = array();
                            $impression_data['iSlipId']      = $Slip_id;
                            $impression_data['iImpressionId'] = $value;
                            
                            $impression_data['iQuantity'] = $request->input("upper_impression_qty_".$value);
                            $impression_data['eType'] = "Opposite";        
                           
                                $product_addons = SlipImpression::add($impression_data);
                        
                        }
                    }
                }
                /*Opposite Impressions End*/
                /*Opposite Extractions Start*/
                if (empty($request->iUpperCategoryId) AND $request->upper_eOpposingExtractions == "Yes") {
                    $upper_opposite_extraction = array();
                    $upper_opposite_extraction['vTeethInMouthOp']  = $request->upper_in_mouth_value;
                    $upper_opposite_extraction['vMissingTeethOp']  = $request->upper_missing_teeth_value;
                    $upper_opposite_extraction['vWillExtractOnDeliveryOp']= $request->upper_ectract_delivery_value;
                    $upper_opposite_extraction['vHasBeenExtractedOp']= $request->upper_been_extracted_value;
                    $upper_opposite_extraction['vFixOrAddOp']      = $request->upper_fix_value;
                    $upper_opposite_extraction['vClaspsOp']        = $request->upper_clasps_value;

                    $where                      = array();
                    $where['iSlipProductId']    = $SlipProduct_id;

                    $Slip_product_id = new SlipProduct();
                    $Slip_product_id->update($where, $upper_opposite_extraction);


                }
                /*Opposite Extractions End*/

                 /* Upper Dates Settings */
                    
                $lower_without_rush = $this->product_dates($request->iLowerCategoryId,$request->iLowerStageId);


                $lower_pikup_final_date = "";

                if(!empty($lower_without_rush))
                {

                    $lower_pikup_final_date = $lower_without_rush['pic_date'];
                    $lower_date_update = array();
                    $lower_date_update['dPickUpDate']   = $lower_without_rush['pic_date'];
                    $lower_date_update['dDeliveryDate'] = $lower_without_rush['del_date'];
                    $lower_date_update['tDeliveryTime'] = $lower_without_rush['delivery_time'];

                    $lower_date_update['dDeliveryDateFinal'] = $lower_without_rush['del_date'];
                    
                    $where                      = array();
                    $where['iSlipProductId']       = $SlipProduct_id;

                    $Slip_product_id = new SlipProduct();
                    $Slip_product_id->update($where, $lower_date_update);
                
                    if(!empty($request->lower_rush_feesid))
                    {    
                        $fees_data = Fees::get_by_id($request->lower_rush_feesid);
                        $output = $this->product_dates($request->iLowerCategoryId,$request->iLowerStageId, $fees_data->iWorkingTime);
                        
                        $lower_rushdate_update = array();
                        $lower_rushdate_update['dDeliveryRushDate'] = $output['del_date'];
                        $lower_rushdate_update['dDeliveryDateFinal'] = $output['del_date'];
                        $lower_rushdate_update['eRushDate'] = "Yes";
                        $lower_rushdate_update['iFeesId'] = $request->lower_rush_feesid;
                        $where                      = array();
                        $where['iSlipProductId']       = $SlipProduct_id;

                        $Slip_product_id = new SlipProduct();
                        $Slip_product_id->update($where, $lower_rushdate_update);

                        $lower_slip_rushdate_update['dLowerRushDate'] = $output['del_date'];

                        $where                  = array();
                        $where['iSlipId']       = $Slip_id;
                        $Slip_id_Update = new Slip();
                        $Slip_id_Update->update($where, $lower_slip_rushdate_update);
                    }

                }
                 /* Upper Dates Settings */
            }
            /*Lower Product End*/

            /*Upper Stage Notes End*/
            if(!empty($request->tDescription)){

                $slip_notes = array();
                $slip_notes['iSlipId']      = $Slip_id;
                $slip_notes['iCaseId']      = $slip_data['iCaseId'];
                if(!empty($request->iUpperCategoryId) AND !empty($request->iLowerCategoryId)){
                    $slip_notes['iStageId']     = $request->iUpperStageId."/".$request->iLowerStageId;

                    $upper_stage = ProductStage::get_by_id($request->iUpperStageId);
                    $upper_stage_name = $upper_stage->vName;

                    $lower_stage = ProductStage::get_by_id($request->iLowerStageId);
                    $lower_stage_name = $lower_stage->vName;

                    $slip_notes['vStageName']     =  $upper_stage_name."/". $lower_stage_name;

                }elseif(!empty($request->iUpperCategoryId) AND empty($request->iLowerCategoryId)){
                    
                    $slip_notes['iStageId']     = $request->iUpperStageId;

                    $upper_stage = ProductStage::get_by_id($request->iUpperStageId);
                    $upper_stage_name = $upper_stage->vName;
                    $slip_notes['vStageName']     =  $upper_stage_name;

                }elseif((empty($request->iUpperCategoryId) AND !empty($request->iLowerCategoryId))){
                    
                    $slip_notes['iStageId']     = $request->iLowerStageId;

                    $lower_stage = ProductStage::get_by_id($request->iLowerStageId);
                    $lower_stage_name = $lower_stage->vName;

                    $slip_notes['vStageName']     = $lower_stage_name;
                }
                $upper_dilevery_date =  $request->upper_delivery_date;
                $lower_dilevery_date =  $request->lower_delivery_date;

                if($upper_dilevery_date > $lower_dilevery_date){
                    
                    $slip_notes['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
                }

                if($upper_dilevery_date == $lower_dilevery_date){
                                    
                        $slip_notes['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
                }

                if($upper_dilevery_date < $lower_dilevery_date){
                                    
                    $slip_notes['dDeliveryDate'] =  date("Y-m-d",strtotime($request->lower_delivery_date));
                }
                $slip_notes['vCaseNumber']  = $request->vCaseNumber;
                $slip_notes['vCreatedByName'] = $request->vCreatedByName;
                $slip_notes['tStagesNotes']  =  $request->tDescription;
                $slip_notes['vSlipNumber']   = $request->vSlipNumber;
                $slip_notes['dtAddedDate'] = date("Y-m-d H:i:s");
                $slip_notes['tAddedTime'] = date("h:i");
                $slip_notes['eNewSlipAdded'] = 'Yes';
                
                $slip_note = SlipNote::add($slip_notes);
               
            }
            // Update CaseId & LabId start
            $vCaseNumber = 'C'."0".str_pad($Case_id,3,"0",STR_PAD_LEFT);
            $vSlipNumber = 'S'."0".str_pad($Slip_id,3,"0",STR_PAD_LEFT);
            $where_labcase['iCaseId']= $Case_id;
            $data_labcase['vCaseNumber']= $vCaseNumber;
            Labcase::update_data($where_labcase,$data_labcase);

            $where_Slip['iSlipId']= $Slip_id;
            $data_Slip['vCaseNumber']= $vCaseNumber;
            $data_Slip['vSlipNumber']= $vSlipNumber;
            Slip::update_new($where_Slip,$data_Slip);
            // Update CaseId & LabId start

            if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == '6')
            {
                return redirect()->route('admin.labcase.office_admin_listing')->withSuccess('labcase created successfully.');
            }
            else
            {
                return redirect()->route('admin.labcase')->withSuccess('labcase created successfully.');
            }
                                  
        }
    }
    
    public function UpdateSlipData(Request $request)
    {
        $Case_id = $request->iCaseId;

        if(isset($request->iLabId) && !empty($request->iLabId))
        {
            $iLabId = $request->iLabId;
        }
        else
        {
            $iLabId = General::admin_info()['iCustomerId'];
        }
        if(isset($request->iOfficeId) && !empty($request->iOfficeId))
        {
            $iOfficeId = $request->iOfficeId;
        }
        else
        {
            $iOfficeId = General::admin_info()['iCustomerId'];
        }

        $iSlipId                    = $request->iSlipId;
        $slip_data['iCaseId']       = $request->iCaseId;
        // $slip_data['vSlipNumber']   = $request->vSlipNumber;
        $slip_data['iOfficeId']     = $iOfficeId;
        $slip_data['iLabId']     = $iLabId;
        $slip_data['iDoctorId']     = $request->iDoctorId;
        $slip_data['vPatientName']  = $request->vPatientName;
        $slip_data['vCasePanNumber']= $request->vCasePanNumber;
        // $slip_data['vCaseNumber']   = $request->vCaseNumber;
        $slip_data['iCreatedById']  = $request->iCreatedById;
        $slip_data['vCreatedByName']= $request->vCreatedByName;
        $slip_data['eCreatedByType']= $request->eCreatedByType;

        $data['iOfficeId'] = $iOfficeId;
        $data['iLabId']     = $iLabId;
        $data['iDoctorId']     = $request->iDoctorId;
        $data['vPatientName']  = $request->vPatientName;
        // $data['vCaseNumber']   = $request->vCaseNumber;
        $data['iCreatedById']  = $request->iCreatedById;
        $data['vCreatedByName']= $request->vCreatedByName;
        $data['eCreatedByType']= $request->eCreatedByType;
        if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
            
            $data['eStatus']       = "On Process";
        }else{
            
            $data['eStatus']       = $request->eStatus;
        }
        if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
            $data['vLocation']     = "In office ready to pickup";
        }else{
            $data['vLocation']     = $request->vLocation;
        }
        
        if($Case_id){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        
        if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
            
            $slip_data['eStatus']       = "On Process";
        }else{
            
            $slip_data['eStatus']       = $request->eStatus;
        }
        if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
            $slip_data['vLocation']     = "In office ready to pickup";
        }else{
            $slip_data['vLocation']     = $request->vLocation;
        }
        
        $slip_data['tStagesNotes']  =  $request->tDescription;

        
        $upper_dilevery_date =  $request->upper_delivery_date;
        $lower_dilevery_date =  $request->lower_delivery_date;

        if($upper_dilevery_date > $lower_dilevery_date){
            
            $slip_data['dPickUpDate']   =  date("Y-m-d ",strtotime($request->upper_pickup_date));
            $slip_data['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
            $slip_data['tDeliveryTime'] =  date("h:i",strtotime($request->upper_delivery_time));

            $iCategoryId = $request->iUpperCategoryId;
            $category_info = Category::get_by_id($iCategoryId);

            $slip_data['iCasepanId']    = $category_info->iCasepanId;

                $data =  array();
                $where = array();

                $data['iCasepanId'] = $slip_data['iCasepanId'];
                $where['iCaseId']   = $Case_id;
                
                $Case_id_new = new Labcase();
                $Case_id_new->update($where, $data);

                    $Casepan = Casepan::get_by_id($data['iCasepanId']);
                    $Casepannumber = CasepanNumber::get_by_casepan_use($Casepan->iCasepanId);
                    
                    if (!empty($Casepannumber)) {
                        $data =  array();
                        $where = array();

                        $data['vCasePanNumber'] = $Casepannumber->vNumber;
                        $where['iCaseId']       = $Case_id;
                        
                        $Case_id = new Labcase();
                        $Case_id->update($where, $data);

                        $data =  array();
                        $where = array();

                        $data['eUse']                =  "Yes";
                        $where['iCasepanNumberId']   =  $Casepannumber->iCasepanNumberId;
                        
                        $iCasepanNumberId = new CasepanNumber();
                        $iCasepanNumberId->update($where, $data);
                    }
                    
        }
    
        if($upper_dilevery_date == $lower_dilevery_date){
            
            $slip_data['dPickUpDate']   =  date("Y-m-d ",strtotime($request->upper_pickup_date));
            $slip_data['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
            $slip_data['tDeliveryTime'] =  date("h:i",strtotime($request->upper_delivery_time));

            $iCategoryId = $request->iUpperCategoryId;
            $category_info = Category::get_by_id($iCategoryId);

            $slip_data['iCasepanId']    = $category_info->iCasepanId;

                $data =  array();
                $where = array();

                $data['iCasepanId'] = $slip_data['iCasepanId'];
                $where['iCaseId']   = $Case_id;
                
                $Case_id_new = new Labcase();
                $Case_id_new->update($where, $data);

                    $Casepan = Casepan::get_by_id($data['iCasepanId']);
                    $Casepannumber = CasepanNumber::get_by_casepan_use($Casepan->iCasepanId);
                    
                    if (!empty($Casepannumber)) {
                        $data =  array();
                        $where = array();

                        $data['vCasePanNumber'] = $Casepannumber->vNumber;
                        $where['iCaseId']       = $Case_id;
                        
                        $Case_id_new = new Labcase();
                        $Case_id_new->update($where, $data);

                        $data =  array();
                        $where = array();

                        $data['eUse']                =  "Yes";
                        $where['iCasepanNumberId']   =  $Casepannumber->iCasepanNumberId;
                        
                        $iCasepanNumberId = new CasepanNumber();
                        $iCasepanNumberId->update($where, $data);
                    }
        }
        if($upper_dilevery_date < $lower_dilevery_date){
            
            $slip_data['dPickUpDate']   =  date("Y-m-d ",strtotime($request->lower_pickup_date));
            $slip_data['dDeliveryDate'] =  date("Y-m-d",strtotime($request->lower_delivery_date));
            $slip_data['tDeliveryTime'] =  date("h:i",strtotime($request->lower_delivery_time));
            
            $iCategoryId = $request->iLowerCategoryId;
            $category_info = Category::get_by_id($iCategoryId);
            
            $slip_data['iCasepanId']    = $category_info->iCasepanId;

                $data =  array();
                $where = array();

                $data['iCasepanId'] = $slip_data['iCasepanId'];
                $where['iCaseId']   = $Case_id;
                
                    $Case_id_new = new Labcase();
                    $Case_id_new->update($where, $data);

                    $Casepan = Casepan::get_by_id($data['iCasepanId']);
                    $Casepannumber = CasepanNumber::get_by_casepan_use($Casepan->iCasepanId);

                    if (!empty($Casepannumber)) {
                        $data =  array();
                        $where = array();

                        $data['vCasePanNumber'] = $Casepannumber->vNumber;
                        $where['iCaseId']       = $Case_id;
                        
                        $Case_id_new = new Labcase();
                        $Case_id_new->update($where, $data);

                        $data =  array();
                        $where = array();

                        $data['eUse']                =  "Yes";
                        $where['iCasepanNumberId']   =  $Casepannumber->iCasepanNumberId;
                        
                        $iCasepanNumberId = new CasepanNumber();
                        $iCasepanNumberId->update($where, $data);
                    }
        }
        
        if($iSlipId){
            $slip_data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $slip_data['dtAddedDate'] = date("Y-m-d H:i:s");
            $slip_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        $where                      = array();
        $where['iSlipId']       = $iSlipId;
        
        $Slip_id11 = new Slip();
        $Slip_id11->update($where, $slip_data);

        $data =  array();
        $where = array();

        $data['vLocation']     = $slip_data['vLocation'];
        $data['iOfficeId'] = $iOfficeId;
        $data['iLabId']     = $iLabId;
        $data['iDoctorId']     = $request->iDoctorId;
        $data['vPatientName']  = $request->vPatientName;
        $data['vCaseNumber']   = $request->vCaseNumber;
        $data['iCreatedById']  = $request->iCreatedById;
        $data['vCreatedByName']= $request->vCreatedByName;
        $data['eCreatedByType']= $request->eCreatedByType;
        if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
            
            $data['eStatus']       = "On Process";
        }else{
            
            $data['eStatus']       = $request->eStatus;
        }
        if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
            $data['vLocation']     = "In office ready to pickup";
        }else{
            $data['vLocation']     = $request->vLocation;
        }
        
        if($Case_id){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        $where['iCaseId']      = $Case_id;
        $Case_id_data = new Labcase();
        $Case_id_data->update($where, $data);


        /* Attachment Upload*/
        $iTampId = $request->iTampId;
        $criteria 				 = array();
        $criteria['iTampId'] 	 = $iTampId;
        
        $gallary = SlipGallery::get_all_data($criteria);
        
        if(!empty($gallary))
        {     
            $data = array();           
            foreach ($gallary as $key => $value) 
            {
                if($value->iTampId == $iTampId)
                {
                    $data['iSlipId']     = $request->iSlipId;
                    $data['iTampId'] 	 = null;
                    $where                      = array();
                    $where['iSlipAttachmentId']= $value->iSlipAttachmentId;
                    
                    $SlipGallery_id = new SlipGallery();
                    $SlipGallery_id->update($where, $data);
                }
            }
        }


        /* Attachment Upload*/
        /* Slip Upper Product Update*/
        if (!empty($request->iUpperCategoryId)) {

            $iSlipProductId                     = $request->iUpperSlipProductId;
            $slip_product_data['iSlipId']       = $request->iSlipId;
            $slip_product_data['eType']         = "Upper";
            $slip_product_data['iCategoryId']   = $request->iUpperCategoryId;
            $slip_product_data['iProductId']    = $request->iUpperProductId;
            $slip_product_data['iGradeId']      = $request->iUpperGradeId;
            $slip_product_data['iStageId']      = $request->iUpperStageId;
            $slip_product_data['eExtraction']   = $request->upper_extraction_stage;
            $slip_product_data['eGumShade']     = $request->upper_gumshade_stage;
            $slip_product_data['eTeethShade']   = $request->upper_teethshade_stage;
            $slip_product_data['eImpression']   = $request->upper_Impression_stage;
            $slip_product_data['eRushDate']     = $request->upper_rush_date_stage;  
            $slip_product_data['iToothBrandId']  = $request->upper_tsbrand;
            $slip_product_data['iToothShadesId'] = $request->upper_tshade;
            $slip_product_data['iGumBrandId']    = $request->upper_gsbrand;
            $slip_product_data['iGumShadesId']   = $request->upper_gshade;
            $slip_product_data['vTeethInMouth']  = $request->upper_in_mouth_value;
            $slip_product_data['vMissingTeeth']  = $request->upper_missing_teeth_value;
            $slip_product_data['vWillExtractOnDelivery']= $request->upper_ectract_delivery_value;
            $slip_product_data['vHasBeenExtracted']= $request->upper_been_extracted_value;
            $slip_product_data['vFixOrAdd']      = $request->upper_fix_value;
            $slip_product_data['vClasps']        = $request->upper_clasps_value;
            
            if($iSlipProductId){
                $slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
            }else{
                $slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
                $slip_product_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            }
            if(!empty($iSlipProductId)){
                $where                      = array();
                $where['iSlipProductId']           = $iSlipProductId;
                $SlipProduct_obj = new SlipProduct();
                $SlipProduct_obj->update($where, $slip_product_data);
                $SlipProduct_id = $iSlipProductId;
            }
           
            
            /*Upper Impression Start*/
            $impression_data_array = $request->upper_impression_checkbox;
            if(!empty($impression_data_array)){
                $where                      = array();
                $where['iSlipId']           = $iSlipId;
                $where['eType']             = "Upper";
                SlipImpression::delete_by_id($where);
                foreach ($impression_data_array as $key => $value) 
                {
                    $impression_data = array();
                    $impression_data['iSlipId']      = $request->iSlipId;
                    $impression_data['iImpressionId'] = $value;
                    $impression_data['iQuantity'] = $request->input("upper_impression_qty_".$value);
                    $impression_data['eType'] = "Upper";    
                    SlipImpression::add($impression_data);
                }
            }
            /*Upper Impression End*/
            
            /*Upper Addons Start*/
            $upper_add_on_cat_id = $request->upper_add_on_cat_id;
            $upper_add_on_id = $request->upper_add_on_id;
            $upper_add_on_qty = $request->upper_add_on_qty;
            if(!empty($upper_add_on_cat_id)){
                    $where                      = array();
                    $where['iSlipId']           = $iSlipId;
                    $where['eType']             = "Upper";
                    SlipAddons::delete_by_id($where);
                    
                foreach ($upper_add_on_cat_id as $key => $value) 
                {
                    $upper_slip_product_addons = array();
                    $upper_slip_product_addons['iSlipId']      = $iSlipId;
                    $upper_slip_product_addons['iAddCategoryId'] = $value;
                    
                    $upper_slip_product_addons['iSubAddCategoryId'] = $upper_add_on_id[$key];
                    $upper_slip_product_addons['iQuantity'] = $upper_add_on_qty[$key];
                    $upper_slip_product_addons['eType'] = "Upper";
                     SlipAddons::add($upper_slip_product_addons);
                }
            }
            
            /*Upper Addons End*/
            

            /*Opposite Impressions Start*/
           
            if (empty($request->iLowerCategoryId) AND $request->lower_eOpposingImpressions == "Yes") {
                $lowerimpression_data_array = $request->lower_impression_checkbox;

                if(!empty($lowerimpression_data_array)){
                    foreach ($lowerimpression_data_array as $key => $value) 
                    {

                        $lower_impression_qty = $request->input("lower_impression_qty_".$value);
                        $lower_impression_data = array();
                        $lower_impression_data['iSlipId']      = $iSlipId;
                        $lower_impression_data['iImpressionId'] = $value;

                        $lower_impression_data['iQuantity'] = $lower_impression_qty;
                        $lower_impression_data['eType'] = "Opposite";
                        if(!empty($iSlipId)){
                            $where                      = array();
                            $where['iSlipId']           = $iSlipId;
                            
                            $Slip_product_obj = new SlipImpression();
                            $Slip_product_obj->update($where, $lower_impression_data);
                        }
                    }
                }
            }
            /*Opposite Impressions End*/
            /*Opposite Extractions Start*/
            if (empty($request->iLowerCategoryId) AND $request->lower_eOpposingExtractions == "Yes") {
                $lower_opposite_extraction = array();
                $lower_opposite_extraction['vTeethInMouthOp']  = $request->lower_in_mouth_value;
                $lower_opposite_extraction['vMissingTeethOp']  = $request->lower_missing_teeth_value;
                // dd($request);
                $lower_opposite_extraction['vWillExtractOnDeliveryOp']= $request->lower_ectract_delivery_value;
                $lower_opposite_extraction['vHasBeenExtractedOp']= $request->lower_been_extracted_value;
                $lower_opposite_extraction['vFixOrAddOp']      = $request->lower_fix_value;
                $lower_opposite_extraction['vClaspsOp']        = $request->lower_clasps_value;

                $where                      = array();
               
                $where['iSlipProductId']       = $iSlipProductId;

                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $lower_opposite_extraction);

            }
            /*Opposite Extractions End*/

            
            /* Upper Dates Settings */
            
            $upper_without_rush = $this->product_dates($request->iUpperCategoryId,$request->iUpperStageId);


            $upper_date_update = array();
            $upper_date_update['dPickUpDate'] = $upper_without_rush['pic_date'];
            $upper_date_update['dDeliveryDate'] = $upper_without_rush['del_date'];
            $upper_date_update['tDeliveryTime'] = $upper_without_rush['delivery_time'];

            $upper_date_update['dDeliveryDateFinal'] = $upper_without_rush['del_date'];

            $where                         = array();
            $where['iSlipProductId']       = $iSlipProductId;

            $Slip_product_obj = new SlipProduct();
            $Slip_product_obj->update($where, $upper_date_update);
            
            if(!empty($request->upper_rush_feesid))
            {    
                $fees_data = Fees::get_by_id($request->upper_rush_feesid);
               
                $output = $this->product_dates($request->iUpperCategoryId,$request->iUpperStageId, $fees_data->iWorkingTime);
                $upper_rushdate_update = array();
                $upper_rushdate_update['dDeliveryRushDate'] = $output['del_date'];
                $upper_rushdate_update['dDeliveryDateFinal'] = $output['del_date'];
                $upper_rushdate_update['eRushDate'] = "Yes";
                $upper_rushdate_update['iFeesId'] =$request->upper_rush_feesid;
                $where                         = array();
                $where['iSlipProductId']       = $SlipProduct_id;
                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $upper_rushdate_update);

                $upper_slip_rushdate_update['dUpperRushDate'] = $output['del_date'];

                $where                  = array();
                $where['iSlipId']       = $Slip_id;
                $Slip_id_Update = new Slip();
                $Slip_id_Update->update($where, $upper_slip_rushdate_update);
            }
            /* Upper Dates Settings */

            /*Lower Product Start*/
        }
       
        if (!empty($request->iLowerCategoryId)) {
            $iSlipProductId                           = $request->iLowerSlipProductId;
            $lower_slip_product_data['iSlipId']       = $iSlipId;
            $lower_slip_product_data['eType']         = "Lower";
            $lower_slip_product_data['iCategoryId']   = $request->iLowerCategoryId;
            $lower_slip_product_data['iProductId']    = $request->iLowerProductId;
            $lower_slip_product_data['iGradeId']      = $request->iLowerGradeId;
            $lower_slip_product_data['iStageId']      = $request->iLowerStageId;
            $lower_slip_product_data['eExtraction']   = $request->lower_extraction_stage;
            $lower_slip_product_data['eGumShade']     = $request->lower_gumshade_stage;
            $lower_slip_product_data['eTeethShade']   = $request->lower_teethshade_stage;
            $lower_slip_product_data['eImpression']   = $request->lower_Impression_stage;
            $lower_slip_product_data['eRushDate']     = $request->lower_rush_date_stage;
            $lower_slip_product_data['iToothBrandId']  = $request->lower_tsbrand;
            $lower_slip_product_data['iToothShadesId'] = $request->lower_tshade;
            $lower_slip_product_data['iGumBrandId']    = $request->lower_gsbrand;
            $lower_slip_product_data['iGumShadesId']   = $request->lower_gshade;
            $lower_slip_product_data['vTeethInMouth']  = $request->lower_in_mouth_value;
            $lower_slip_product_data['vMissingTeeth']  = $request->lower_missing_teeth_value;
            $lower_slip_product_data['vWillExtractOnDelivery']= $request->lower_ectract_delivery_value;
            $lower_slip_product_data['vHasBeenExtracted']= $request->lower_been_extracted_value;
            $lower_slip_product_data['vFixOrAdd']      = $request->lower_fix_value;
            $lower_slip_product_data['vClasps']        = $request->lower_clasps_value;
            $lower_slip_product_data['eStatus']       =  $request->lower_status;

            if($iSlipProductId){
                $lower_slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
            }else{
                $lower_slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
                $lower_slip_product_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            }
            if(!empty($iSlipProductId)){
                $where                      = array();
                $where['iSlipProductId']       = $iSlipProductId;
                $SlipProduct_obj = new SlipProduct();
                $SlipProduct_obj->update($where, $lower_slip_product_data);
            }else{
                $SlipProduct_id = SlipProduct::add($lower_slip_product_data);
            }
            /*Lower Impression Start*/
            $lowerimpression_data_array = $request->lower_impression_checkbox;
         
            
            if(!empty($lowerimpression_data_array)){
                $where                      = array();
                $where['iSlipId']           = $iSlipId;
                $where['eType']             = "Lower";
                
                SlipImpression::delete_by_id($where);
                foreach ($lowerimpression_data_array as $key => $value) 
                {
                    
                    $lower_impression_qty = $request->input("lower_impression_qty_".$value);
                    $lower_impression_data = array();
                    $lower_impression_data['iSlipId']      = $iSlipId;
                    $lower_impression_data['iImpressionId'] = $value;
                    
                    $lower_impression_data['iQuantity'] = $lower_impression_qty;
                    $lower_impression_data['eType'] = "Lower";

                     SlipImpression::add($lower_impression_data);
                    // if(!empty($iSlipId)){
                    //     $where                      = array();
                    //     $where['iSlipId']           = $iSlipId;
                        
                    //     $SlipProduct_obj = new SlipImpression();
                    //     $SlipProduct_obj->update($where, $lower_impression_data);
                    // }
                }
            }
            /*Lower Impression End*/

            /*Lower Addons Start*/
            $lower_add_on_cat_id = $request->lower_add_on_cat_id;
            $lower_add_on_id = $request->lower_add_on_id;
            $lower_add_on_qty = $request->lower_add_on_qty;
            if(!empty($lower_add_on_cat_id)){
                $where                      = array();
                $where['iSlipId']           = $iSlipId;
                $where['eType']             = "Lower";

                SlipAddons::delete_by_id($where);

                foreach ($lower_add_on_cat_id as $key => $value) 
                {
                    $lower_slip_product_addons = array();
                    $lower_slip_product_addons['iSlipId']      = $iSlipId;
                    $lower_slip_product_addons['iAddCategoryId'] = $value;
                    
                    $lower_slip_product_addons['iSubAddCategoryId'] = $lower_add_on_id[$key];
                    $lower_slip_product_addons['iQuantity'] = $lower_add_on_qty[$key];
                    $lower_slip_product_addons['eType'] = "Lower";


                     SlipAddons::add($lower_slip_product_addons);
                    // if(!empty($iSlipId)){
                    //     $where                      = array();
                    //     $where['iSlipId']           = $iSlipId;
                        
                    //     $SlipProduct_obj = new SlipAddons();
                    //     $SlipProduct_obj->update($where, $lower_slip_product_addons);

                    // }
                }
            }
            /*Lower Addons End*/


            /*Opposite Impressions Start*/
            if (empty($request->iUpperCategoryId) AND $request->upper_eOpposingImpressions == "Yes") {
                $impression_data_array = $request->upper_impression_checkbox;
                if(!empty($impression_data_array)){
                    foreach ($impression_data_array as $key => $value) 
                    {   
                        $impression_data = array();
                        $impression_data['iSlipId']      = $Slip_id;
                        $impression_data['iImpressionId'] = $value;
                        
                        $impression_data['iQuantity'] = $request->input("upper_impression_qty_".$value);
                        $impression_data['eType'] = "Opposite";        
                        
                        if(!empty($iSlipId)){
                            $where                      = array();
                            $where['iSlipId']           = $iSlipId;
                            
                            $SlipProduct_obj = new SlipImpression();
                            $SlipProduct_obj->update($where, $impression_data);
                        }
                    }
                }
            }
            /*Opposite Impressions End*/
            /*Opposite Extractions Start*/
            if (empty($request->iUpperCategoryId) AND $request->upper_eOpposingExtractions == "Yes") {
                $upper_opposite_extraction = array();
                $upper_opposite_extraction['vTeethInMouthOp']  = $request->upper_in_mouth_value;
                $upper_opposite_extraction['vMissingTeethOp']  = $request->upper_missing_teeth_value;
                $upper_opposite_extraction['vWillExtractOnDeliveryOp']= $request->upper_ectract_delivery_value;
                $upper_opposite_extraction['vHasBeenExtractedOp']= $request->upper_been_extracted_value;
                $upper_opposite_extraction['vFixOrAddOp']      = $request->upper_fix_value;
                $upper_opposite_extraction['vClaspsOp']        = $request->upper_clasps_value;

                $where                      = array();
                $where['iSlipProductId']    = $iSlipProductId;

                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $upper_opposite_extraction);


            }
            /*Opposite Extractions End*/

             /* Upper Dates Settings */

             $lower_without_rush = $this->product_dates($request->iLowerCategoryId,$request->iLowerStageId);

             $lower_date_update = array();
             $lower_date_update['dPickUpDate']   = $lower_without_rush['pic_date'];
             $lower_date_update['dDeliveryDate'] = $lower_without_rush['del_date'];
             $lower_date_update['tDeliveryTime'] = $lower_without_rush['delivery_time'];

             $lower_date_update['dDeliveryDateFinal'] = $lower_without_rush['del_date'];

             $where                      = array();
             $where['iSlipProductId']       = $iSlipProductId;

             $Slip_product_obj = new SlipProduct();
             $Slip_product_obj->update($where, $lower_date_update);

            
             if(!empty($request->lower_rush_feesid))
             {    
                 $fees_data = Fees::get_by_id($request->lower_rush_feesid);
                 $output = $this->product_dates($request->iLowerCategoryId,$request->iLowerStageId, $fees_data->iWorkingTime);
                 
                 $lower_rushdate_update = array();
                 $lower_rushdate_update['dDeliveryRushDate'] = $output['del_date'];
                 $lower_rushdate_update['dDeliveryDateFinal'] = $output['del_date'];
                 $lower_rushdate_update['eRushDate'] = "Yes";
                 $upper_rushdate_update['iFeesId'] =$request->lower_rush_feesid;
                
                    $where                      = array();
                    $where['iSlipProductId']       = $SlipProduct_id;

                    $Slip_product_obj = new SlipProduct();
                    $Slip_product_obj->update($where, $lower_rushdate_update);

                    $lower_slip_rushdate_update['dLowerRushDate'] = $output['del_date'];

                    $where                  = array();
                    $where['iSlipId']       = $Slip_id;
                    $Slip_id_Update = new Slip();
                    $Slip_id_Update->update($where, $lower_slip_rushdate_update);
             }
             /* Upper Dates Settings */

        }
        /*Lower Product End*/

        
        if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == '6')
        {
            return redirect()->route('admin.labcase.office_admin_listing')->withSuccess('Slip updated successfully.');
        }
        else
        {
            return redirect()->route('admin.labcase')->withSuccess('Slip updated successfully.');
        }




    }
    public function edit_slip($iCaseId,$iSlipId)
    {

        $data['office'] = Office::get_office_by_customerid();

        $iCustomerId = General::admin_info()['iCustomerId'];
        $data["doctor"] =   Doctor::get_doc_by_customerid($iCustomerId);

        // $data['doctor'] = Doctor::get_all_doctor();
        $data["Office"] = Office::get_office_by_customerid();
        // $data['Upper_category'] = Category::get_category_by_type("Upper");
        // $data['Lower_category'] = Category::get_category_by_type("Lower");

        


        $data['toothshade'] = ToothShades::get_all_toothshades();
        // $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        // $data['toothbrand'] = ToothBrand::get_all_brand();
        // $data['impression'] = Impression::get_all_impressions();
        // dd($data['impression']);

        $labOffice = LabOffice::get_active_lab_by_officeId($iCustomerId);

        $labs = array();

        foreach ($labOffice as $key => $value) {
            $labs[] = Customer::get_by_id($value->iLabId);
        }

        $data['labs'] = $labs;
        

        // $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        // $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

        $schedule = Schedule::get_all_schedule();
        foreach($schedule as $value){
            if (!empty($value->dtHolidays) AND empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                $holiday_arr = explode("-",$value->dtHolidays);
                $holidays[] = $holiday_arr[1]."/".$holiday_arr[2]."/".$holiday_arr[0];
            }
        }
        if(!empty($holidays)){
            $data['holidays'] = implode(",",$holidays);
        }

        $admin = Session::get('data');
        if(empty($admin['user_id']))
        {
            Redirect::to('/admin/login')->send();
        }else{
           // $iAdminId = $admin['user_id'];
            // $admin    = Admin::get_by_id($iAdminId);
           if(isset($admin['vFirstName']) && !empty($admin['vFirstName']) && isset($admin['vLastName']) && !empty($admin['vLastName']))
              {
                $data['user_name'] = $admin['vFirstName'].' '.$admin['vLastName'];
              }
              else
              {
                $data['user_name'] = $admin['vName'];
              }
                   
            // $data['AdminId'] = $iAdminId;
            $data['AdminId'] = $admin['user_id'];
        }

        $caseid = Labcase::get_office_id();
        if(!empty($caseid->iCaseId)) {
            $id = $caseid->iCaseId+1;
        }else{           
            $id = 1;
        }
        $data['case_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $slipid = Labcase::get_slip_id();
        if(!empty($slipid->iSlipId)) {
            $id = $slipid->iSlipId+1;
        }else{           
            $id = 1;
        }
        $data['slip_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $data['tampid'] =  str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);

        $data['Case'] = Labcase::get_by_id($iCaseId);
        $data['Slip'] = Slip::get_by_id($iSlipId);

         // For Security purpose start
         $iCustomerId = General::admin_info()['iCustomerId'];
         $customerType =General::admin_info()['customerType'];
         if(isset($customerType) && $customerType =='Office Admin')
         {
             if(isset($data['Slip']->iOfficeId) && !empty($data['Slip']->iOfficeId))
             {
                 $MatchId = $data['Slip']->iOfficeId;
             }
             else
             {
                 $MatchId = '';
             }
         }
         if(isset($customerType) && $customerType =='Lab Admin')
         {
             if(isset($data['Slip']->iLabId) && !empty($data['Slip']->iLabId))
             {
                 $MatchId = $data['Slip']->iLabId;
             }
             else
             {
                 $MatchId = '';
             }
         }
         if((isset($MatchId) && $MatchId != $iCustomerId && !empty($iCustomerId)) || (empty($MatchId)) || (empty($data['Case'])))
         {
            if(isset($customerType) && $customerType =='Office Admin')
            {
                return redirect()->route('admin.labcase.office_admin_listing')->withError('Invalid Input');
            }
            elseif(isset($customerType) && $customerType =='Lab Admin')
            {
                return redirect()->route('admin.labcase')->withError('Invalid Input');
            }
         }
         // For Security purpose end

        $data['upper_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Upper");
        $data['lower_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Lower");
        
        $data["rush"] = "No";


        $iLabId = $data['Slip']->iLabId;
        $data["Upper_category"] =   Category::get_category_by_type_customer('Upper',$iLabId);
        $data["Lower_category"] =   Category::get_category_by_type_customer('Lower',$iLabId);

        $data['toothbrand'] = ToothBrand::get_all_brand_by_customerid($iLabId);

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type('Upper',$iLabId);
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type('Lower',$iLabId);

        $criteria_new = array();
        $criteria_new['iLabId'] = $iLabId;

        $data["gumshade"] =   GumShades::get_product_by_gumbrand($criteria_new);

        $data["impression"] = Impression::get_all_impressions_by_customer($iLabId);
        
        if (!empty($data['upper_product'])) {

            $data['upper_vTeethInMouth'] = explode(",",$data['upper_product']->vTeethInMouth);
            $data['upper_vMissingTeeth'] = explode(",",$data['upper_product']->vMissingTeeth);
            $data['upper_vWillExtractOnDelivery'] = explode(",",$data['upper_product']->vWillExtractOnDelivery);
            $data['upper_vHasBeenExtracted'] = explode(",",$data['upper_product']->vHasBeenExtracted);
            $data['upper_vFixOrAdd'] = explode(",",$data['upper_product']->vFixOrAdd);
            $data['upper_vClasps'] = explode(",",$data['upper_product']->vClasps);

            $upper_teeth_in_mouth = array();
            $upper_extract= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

            $upper_merged_array = array_merge(
                $data['upper_vMissingTeeth'],
                $data['upper_vWillExtractOnDelivery'],
                $data['upper_vHasBeenExtracted'],
                $data['upper_vFixOrAdd'],
                $data['upper_vClasps']
            );

            foreach ($upper_extract as $value) {
                if (!in_array($value,$upper_merged_array)) {
                    $upper_teeth_in_mouth[] = $value;
                }
            }
            
            $data['upper_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['upper_vTeethInMouth'],
                    $upper_teeth_in_mouth
                )
            );

            $data['lower_vTeethInMouthOp'] = explode(",",$data['upper_product']->vTeethInMouthOp);
            $data['lower_vMissingTeethOp'] = explode(",",$data['upper_product']->vMissingTeethOp);
            $data['lower_vWillExtractOnDeliveryOp'] = explode(",",$data['upper_product']->vWillExtractOnDeliveryOp);
            $data['lower_vHasBeenExtractedOp'] = explode(",",$data['upper_product']->vHasBeenExtractedOp);
            $data['lower_vFixOrAddOp'] = explode(",",$data['upper_product']->vFixOrAddOp);
            $data['lower_vClaspsOp'] = explode(",",$data['upper_product']->vClaspsOp);

            if (!empty($data['upper_product']->lower_vTeethInMouthOp) AND !empty($data['upper_product']->lower_vMissingTeethOp) AND !empty($data['upper_product']->lower_vWillExtractOnDeliveryOp) AND !empty($data['upper_product']->lower_vHasBeenExtractedOp) AND !empty($data['upper_product']->lower_vFixOrAddOp) AND !empty($data['upper_product']->lower_vClaspsOp)) {


                $lower_teeth_in_mouthOp = array();
                $lower_extractOp= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");

                $lower_merged_arrayOp = array_merge(
                    $data['lower_vMissingTeethOp'],
                    $data['lower_vWillExtractOnDeliveryOp'],
                    $data['lower_vHasBeenExtractedOp'],
                    $data['lower_vFixOrAddOp'],
                    $data['lower_vClaspsOp']
                );

                foreach ($lower_extractOp as $value) {
                    if (!in_array($value,$lower_merged_arrayOp)) {
                        $lower_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['lower_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['lower_vTeethInMouthOp'],
                        $lower_teeth_in_mouthOp
                    )
                );
            }
            // dd($data['lower_vTeethInMouthOp']);

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['lower_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['lower_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['lower_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $upper_delivery_date = $data['upper_product']->dDeliveryDate;
            if ($data['upper_product']->eRushDate == "Yes") {
                $data["rush"] = "Yes";
            }
        }

        if (!empty($data['lower_product'])) {
            $data['lower_vTeethInMouth'] = explode(",",$data['lower_product']->vTeethInMouth);
            $data['lower_vMissingTeeth'] = explode(",",$data['lower_product']->vMissingTeeth);
            $data['lower_vWillExtractOnDelivery'] = explode(",",$data['lower_product']->vWillExtractOnDelivery);
            $data['lower_vHasBeenExtracted'] = explode(",",$data['lower_product']->vHasBeenExtracted);
            $data['lower_vFixOrAdd'] = explode(",",$data['lower_product']->vFixOrAdd);
            $data['lower_vClasps'] = explode(",",$data['lower_product']->vClasps);

            
            $teeth_in_mouth = array();
            $lower_extract= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");
            
            $merged_array = array_merge(
                $data['lower_vMissingTeeth'],
                $data['lower_vWillExtractOnDelivery'],
                $data['lower_vHasBeenExtracted'],
                $data['lower_vFixOrAdd'],
                $data['lower_vClasps']
            );
            
            foreach ($lower_extract as $value) {
                if (!in_array($value,$merged_array)) {
                    $teeth_in_mouth[] = $value;
                }
            }

            $data['lower_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['lower_vTeethInMouth'],
                    $teeth_in_mouth
                )
            );


            $data['upper_vTeethInMouthOp'] = explode(",",$data['lower_product']->vTeethInMouthOp);
            $data['upper_vMissingTeethOp'] = explode(",",$data['lower_product']->vMissingTeethOp);
            $data['upper_vWillExtractOnDeliveryOp'] = explode(",",$data['lower_product']->vWillExtractOnDeliveryOp);
            $data['upper_vHasBeenExtractedOp'] = explode(",",$data['lower_product']->vHasBeenExtractedOp);
            $data['upper_vFixOrAddOp'] = explode(",",$data['lower_product']->vFixOrAddOp);
            $data['upper_vClaspsOp'] = explode(",",$data['lower_product']->vClaspsOp);

            if (!empty($data['lower_product']->upper_vTeethInMouthOp) AND !empty($data['lower_product']->upper_vMissingTeethOp) AND !empty($data['lower_product']->upper_vWillExtractOnDeliveryOp) AND !empty($data['lower_product']->upper_vHasBeenExtractedOp) AND !empty($data['lower_product']->upper_vFixOrAddOp) AND !empty($data['lower_product']->upper_vClaspsOp)) {

                $upper_teeth_in_mouthOp = array();
                $upper_extractOp= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

                $upper_merged_arrayOp = array_merge(
                    $data['upper_vMissingTeethOp'],
                    $data['upper_vWillExtractOnDeliveryOp'],
                    $data['upper_vHasBeenExtractedOp'],
                    $data['upper_vFixOrAddOp'],
                    $data['upper_vClaspsOp']
                );

                foreach ($upper_extractOp as $value) {
                    if (!in_array($value,$upper_merged_arrayOp)) {
                        $upper_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['upper_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['upper_vTeethInMouthOp'],
                        $upper_teeth_in_mouthOp
                    )
                );
            }


            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['upper_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['upper_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['upper_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $lower_delivery_date = $data['lower_product']->dDeliveryDate;
            if ($data['lower_product']->eRushDate == "Yes") {
                $data["rush"] = "Yes";
            }
        }
        
        if ($data["rush"] == "No") {
            if (!empty($upper_delivery_date) AND empty($lower_delivery_date)) {
                $data['final_delivery_date'] = $upper_delivery_date;
                $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
            }else if(!empty($lower_dilevery_date) AND empty($upper_delivery_date)){
                $data['final_delivery_date'] = $lower_delivery_date;
                $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
            }else if (!empty($upper_delivery_date) && !empty($lower_delivery_date)) {
                // dd($lower_delivery_date);
                if ($upper_delivery_date > $lower_delivery_date) {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                } else if ($upper_delivery_date < $lower_delivery_date) {
                    $data['final_delivery_date'] = $lower_delivery_date;
                    $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
                } else if ($upper_delivery_date == $lower_delivery_date) {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                }
            }
        }else{
            
        }
        $data["Office"] = Office::get_office_by_customerid();
        $data['Upper_category'] = Category::get_category_by_type("Upper");
        $data['Lower_category'] = Category::get_category_by_type("Lower");
        $data['toothshade'] = ToothShades::get_all_toothshades();
        $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        $data['toothbrand'] = ToothBrand::get_all_brand();

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

            // dd($data);

        return view('admin.labcase.edit_slip')->with($data);
    }
    public function edit_slip_lab($iCaseId,$iSlipId)
    {

        $iCustomerId = General::admin_info()['iCustomerId'];
        $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);
        $office = array();
        foreach ($labOffice as $key => $value) {
            $office[] = Customer::get_by_id($value->iOfficeId);
        }
        $data['office'] = $office;
        $iOfficeId = $labOffice[0]->iOfficeId;
        $data["doctor"] =   Doctor::get_doc_by_office($iOfficeId);
        $data['iLoggedinId']=$iCustomerId;


        // $data['doctor'] = Doctor::get_all_doctor();
        // $data['Upper_category'] = Category::get_category_by_type("Upper");
        // $data['Lower_category'] = Category::get_category_by_type("Lower");

        


        $data['toothshade'] = ToothShades::get_all_toothshades();
        // $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        // $data['toothbrand'] = ToothBrand::get_all_brand();
        // $data['impression'] = Impression::get_all_impressions();
        // dd($data['impression']);

        $labOffice = LabOffice::get_active_lab_by_officeId($iCustomerId);

        $labs = array();

        foreach ($labOffice as $key => $value) {
            $labs[] = Customer::get_by_id($value->iLabId);
        }

        $data['labs'] = $labs;
        

        // $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        // $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

        $schedule = Schedule::get_all_schedule();
        foreach($schedule as $value){
            if (!empty($value->dtHolidays) AND empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                $holiday_arr = explode("-",$value->dtHolidays);
                $holidays[] = $holiday_arr[1]."/".$holiday_arr[2]."/".$holiday_arr[0];
            }
        }
        if(!empty($holidays)){
            $data['holidays'] = implode(",",$holidays);
        }

        $admin = Session::get('data');
        if(empty($admin['user_id']))
        {
            Redirect::to('/admin/login')->send();
        }else{
           // $iAdminId = $admin['user_id'];
            // $admin    = Admin::get_by_id($iAdminId);
           if(isset($admin['vFirstName']) && !empty($admin['vFirstName']) && isset($admin['vLastName']) && !empty($admin['vLastName']))
              {
                $data['user_name'] = $admin['vFirstName'].' '.$admin['vLastName'];
              }
              else
              {
                $data['user_name'] = $admin['vName'];
              }
                       
            // $data['AdminId'] = $iAdminId;
            $data['AdminId'] = $admin['user_id'];
        }

        $caseid = Labcase::get_office_id();
        if(!empty($caseid->iCaseId)) {
            $id = $caseid->iCaseId+1;
        }else{           
            $id = 1;
        }
        $data['case_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $slipid = Labcase::get_slip_id();
        if(!empty($slipid->iSlipId)) {
            $id = $slipid->iSlipId+1;
        }else{           
            $id = 1;
        }
        $data['slip_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $data['tampid'] =  str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);

        $data['Case'] = Labcase::get_by_id($iCaseId);
        $data['Slip'] = Slip::get_by_id($iSlipId);

         // For Security purpose start
         $iCustomerId = General::admin_info()['iCustomerId'];
         $customerType =General::admin_info()['customerType'];
         if(isset($customerType) && $customerType =='Office Admin')
         {
             if(isset($data['Slip']->iOfficeId) && !empty($data['Slip']->iOfficeId))
             {
                 $MatchId = $data['Slip']->iOfficeId;
             }
             else
             {
                 $MatchId = '';
             }
         }
         if(isset($customerType) && $customerType =='Lab Admin')
         {
             if(isset($data['Slip']->iLabId) && !empty($data['Slip']->iLabId))
             {
                 $MatchId = $data['Slip']->iLabId;
             }
             else
             {
                 $MatchId = '';
             }
         }
         if((isset($MatchId) && $MatchId != $iCustomerId && !empty($iCustomerId)) || (empty($MatchId)) || (empty($data['Case'])))
         {
            if(isset($customerType) && $customerType =='Office Admin')
            {
                return redirect()->route('admin.labcase.office_admin_listing')->withError('Invalid Input');
            }
            elseif(isset($customerType) && $customerType =='Lab Admin')
            {
                return redirect()->route('admin.labcase')->withError('Invalid Input');
            }
         }
         // For Security purpose end

        $data['upper_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Upper");
        $data['lower_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Lower");
        
        $data["rush"] = "No";


        $iLabId = $data['Slip']->iLabId;
        $data["Upper_category"] =   Category::get_category_by_type_customer('Upper',$iLabId);
        $data["Lower_category"] =   Category::get_category_by_type_customer('Lower',$iLabId);

        $data['toothbrand'] = ToothBrand::get_all_brand_by_customerid($iLabId);

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type('Upper',$iLabId);
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type('Lower',$iLabId);


        $criteria_new = array();
        $criteria_new['iLabId'] = $iLabId;
        $data["gumshade"] =   GumShades::get_product_by_gumbrand($criteria_new);

        $data["impression"] = Impression::get_all_impressions_by_customer($iLabId);
        
        if (!empty($data['upper_product'])) {

            $data['upper_vTeethInMouth'] = explode(",",$data['upper_product']->vTeethInMouth);
            $data['upper_vMissingTeeth'] = explode(",",$data['upper_product']->vMissingTeeth);
            $data['upper_vWillExtractOnDelivery'] = explode(",",$data['upper_product']->vWillExtractOnDelivery);
            $data['upper_vHasBeenExtracted'] = explode(",",$data['upper_product']->vHasBeenExtracted);
            $data['upper_vFixOrAdd'] = explode(",",$data['upper_product']->vFixOrAdd);
            $data['upper_vClasps'] = explode(",",$data['upper_product']->vClasps);

            $upper_teeth_in_mouth = array();
            $upper_extract= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

            $upper_merged_array = array_merge(
                $data['upper_vMissingTeeth'],
                $data['upper_vWillExtractOnDelivery'],
                $data['upper_vHasBeenExtracted'],
                $data['upper_vFixOrAdd'],
                $data['upper_vClasps']
            );

            foreach ($upper_extract as $value) {
                if (!in_array($value,$upper_merged_array)) {
                    $upper_teeth_in_mouth[] = $value;
                }
            }
            
            $data['upper_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['upper_vTeethInMouth'],
                    $upper_teeth_in_mouth
                )
            );

            $data['lower_vTeethInMouthOp'] = explode(",",$data['upper_product']->vTeethInMouthOp);
            $data['lower_vMissingTeethOp'] = explode(",",$data['upper_product']->vMissingTeethOp);
            $data['lower_vWillExtractOnDeliveryOp'] = explode(",",$data['upper_product']->vWillExtractOnDeliveryOp);
            $data['lower_vHasBeenExtractedOp'] = explode(",",$data['upper_product']->vHasBeenExtractedOp);
            $data['lower_vFixOrAddOp'] = explode(",",$data['upper_product']->vFixOrAddOp);
            $data['lower_vClaspsOp'] = explode(",",$data['upper_product']->vClaspsOp);

            if (!empty($data['upper_product']->lower_vTeethInMouthOp) AND !empty($data['upper_product']->lower_vMissingTeethOp) AND !empty($data['upper_product']->lower_vWillExtractOnDeliveryOp) AND !empty($data['upper_product']->lower_vHasBeenExtractedOp) AND !empty($data['upper_product']->lower_vFixOrAddOp) AND !empty($data['upper_product']->lower_vClaspsOp)) {

                $lower_teeth_in_mouthOp = array();
                $lower_extractOp= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");

                $lower_merged_arrayOp = array_merge(
                    $data['lower_vMissingTeethOp'],
                    $data['lower_vWillExtractOnDeliveryOp'],
                    $data['lower_vHasBeenExtractedOp'],
                    $data['lower_vFixOrAddOp'],
                    $data['lower_vClaspsOp']
                );

                foreach ($lower_extractOp as $value) {
                    if (!in_array($value,$lower_merged_arrayOp)) {
                        $lower_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['lower_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['lower_vTeethInMouthOp'],
                        $lower_teeth_in_mouthOp
                    )
                );
            }
            // dd($data['lower_vTeethInMouthOp']);

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['lower_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['lower_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['lower_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $upper_delivery_date = $data['upper_product']->dDeliveryDate;
            if ($data['upper_product']->eRushDate == "Yes") {
                $data["rush"] = "Yes";
            }
        }

        if (!empty($data['lower_product'])) {
            $data['lower_vTeethInMouth'] = explode(",",$data['lower_product']->vTeethInMouth);
            $data['lower_vMissingTeeth'] = explode(",",$data['lower_product']->vMissingTeeth);
            $data['lower_vWillExtractOnDelivery'] = explode(",",$data['lower_product']->vWillExtractOnDelivery);
            $data['lower_vHasBeenExtracted'] = explode(",",$data['lower_product']->vHasBeenExtracted);
            $data['lower_vFixOrAdd'] = explode(",",$data['lower_product']->vFixOrAdd);
            $data['lower_vClasps'] = explode(",",$data['lower_product']->vClasps);

            
            $teeth_in_mouth = array();
            $lower_extract= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");
            
            $merged_array = array_merge(
                $data['lower_vMissingTeeth'],
                $data['lower_vWillExtractOnDelivery'],
                $data['lower_vHasBeenExtracted'],
                $data['lower_vFixOrAdd'],
                $data['lower_vClasps']
            );
            
            foreach ($lower_extract as $value) {
                if (!in_array($value,$merged_array)) {
                    $teeth_in_mouth[] = $value;
                }
            }

            $data['lower_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['lower_vTeethInMouth'],
                    $teeth_in_mouth
                )
            );


            $data['upper_vTeethInMouthOp'] = explode(",",$data['lower_product']->vTeethInMouthOp);
            $data['upper_vMissingTeethOp'] = explode(",",$data['lower_product']->vMissingTeethOp);
            $data['upper_vWillExtractOnDeliveryOp'] = explode(",",$data['lower_product']->vWillExtractOnDeliveryOp);
            $data['upper_vHasBeenExtractedOp'] = explode(",",$data['lower_product']->vHasBeenExtractedOp);
            $data['upper_vFixOrAddOp'] = explode(",",$data['lower_product']->vFixOrAddOp);
            $data['upper_vClaspsOp'] = explode(",",$data['lower_product']->vClaspsOp);

            if (!empty($data['lower_product']->upper_vTeethInMouthOp) AND !empty($data['lower_product']->upper_vMissingTeethOp) AND !empty($data['lower_product']->upper_vWillExtractOnDeliveryOp) AND !empty($data['lower_product']->upper_vHasBeenExtractedOp) AND !empty($data['lower_product']->upper_vFixOrAddOp) AND !empty($data['lower_product']->upper_vClaspsOp)) {

                $upper_teeth_in_mouthOp = array();
                $upper_extractOp= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

                $upper_merged_arrayOp = array_merge(
                    $data['upper_vMissingTeethOp'],
                    $data['upper_vWillExtractOnDeliveryOp'],
                    $data['upper_vHasBeenExtractedOp'],
                    $data['upper_vFixOrAddOp'],
                    $data['upper_vClaspsOp']
                );

                foreach ($upper_extractOp as $value) {
                    if (!in_array($value,$upper_merged_arrayOp)) {
                        $upper_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['upper_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['upper_vTeethInMouthOp'],
                        $upper_teeth_in_mouthOp
                    )
                );
            }


            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['upper_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['upper_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['upper_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $lower_delivery_date = $data['lower_product']->dDeliveryDate;
            if ($data['lower_product']->eRushDate == "Yes") {
                $data["rush"] = "Yes";
            }
        }
        
        if ($data["rush"] == "No") {
            if (!empty($upper_delivery_date) AND empty($lower_delivery_date)) {
                $data['final_delivery_date'] = $upper_delivery_date;
                $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
            }else if(!empty($lower_dilevery_date) AND empty($upper_delivery_date)){
                $data['final_delivery_date'] = $lower_delivery_date;
                $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
            }else if (!empty($upper_delivery_date) && !empty($lower_delivery_date)) {
                // dd($lower_delivery_date);
                if ($upper_delivery_date > $lower_delivery_date) {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                } else if ($upper_delivery_date < $lower_delivery_date) {
                    $data['final_delivery_date'] = $lower_delivery_date;
                    $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
                } else if ($upper_delivery_date == $lower_delivery_date) {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                }
            }
        }else{
            
        }
        $data["Office"] = Office::get_office_by_customerid();
        $data['Upper_category'] = Category::get_category_by_type("Upper");
        $data['Lower_category'] = Category::get_category_by_type("Lower");
        $data['toothshade'] = ToothShades::get_all_toothshades();
        $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        $data['toothbrand'] = ToothBrand::get_all_brand();

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

            // dd($data);

        return view('admin.labcase.edit_slip_lab')->with($data);
    }

    public function pickupdays(Request $request){
        if(isset($request->iProductStageId) && !empty($request->iProductStageId) && isset($request->iCategoryId) && !empty($request->iCategoryId))
        {
            $iCategoryId        = $request->iCategoryId ;
            $iProductStageId    = $request->iProductStageId;
            $output = $this->product_dates($iCategoryId,$iProductStageId);
            echo json_encode($output);
        }
    }

    public function product_dates($iCategoryId,$iProductStageId,$iWorkingTime=0)
    {
        // $iCustomerId    = "11";
        $category_data = Category::get_by_id($iCategoryId);
        $iCustomerId = $category_data->iCustomerId;
        $schedule = Schedule::get_by_customerid($iCustomerId);
        $product_stage_data = ProductStage::get_by_id($iProductStageId);
        if(!empty($product_stage_data))
        {
            $Daye_To_Pick_Up = $product_stage_data->iPickUpDay;
            $Days_To_Process = $product_stage_data->iProcessDay;
            $Days_To_Deliver = $product_stage_data->iDeliverDay;
            if ($iWorkingTime > 0) {
                $Total_delivery_days = $iWorkingTime;
            }else{
                $Total_delivery_days = $Daye_To_Pick_Up+$Days_To_Process+$Days_To_Deliver;
            }
            $PickTime = $category_data->tPickTime;
            $DeliveryTime = $category_data->tDeliveryTime;
    
            $finel_Delivery_Time = date ('H:i',strtotime($DeliveryTime));
            
            $current_time = date('H:i');
            $current_date = date("Y-m-d");
            
            if($current_time >= $PickTime){
                $pick_up_date = date("Y-m-d",strtotime($current_date.'+1 day'));
            }else{
                $pick_up_date = $current_date;
            }
            
            // dd($pick_up_date);
            
            $delivery_days_for_calc = $Total_delivery_days-1;
            $estimated_delivery_date = date("Y-m-d",strtotime($pick_up_date . " +".$delivery_days_for_calc." days"));
            
            $all_dates = array();
            $all_dates[] = $pick_up_date;
            
            $temp_date = $pick_up_date;
            for ($i=1; $i <= $delivery_days_for_calc ; $i++) { 
                $temp_date = date("Y-m-d",strtotime($temp_date.'+1 day'));
                $all_dates[] = $temp_date;
            }
            
            foreach ($schedule as $key => $value) {
                if (empty($value->dtHolidays) AND !empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                    $schedule_closed_days[] = $value->eWeek;
                }
            }
            
            $schedule_closed_holidays = array();
            foreach ($schedule as $key => $value) {
                if (!empty($value->dtHolidays) AND empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                    $holiday_day = date('l', strtotime($value->dtHolidays));
                    if (!in_array($holiday_day, $schedule_closed_days)) {
                        $schedule_closed_holidays[] = $value->dtHolidays;
                    }
                }
            }
            
            $cur_dt = $pick_up_date;
            
            $add_dates = array();
            do{
                $day = date('l', strtotime($cur_dt));
                if (!in_array($day,$schedule_closed_days) AND !in_array($cur_dt,$schedule_closed_holidays)) {
                    $add_dates[] = $cur_dt;
                }
                
                $cur_dt = date("Y-m-d",strtotime($cur_dt.'+1 day'));
            }while(count($add_dates) < $Total_delivery_days);
            
            // dd($add_dates);
            $output = array();
            $output["del_date"] = end($add_dates);
            $output["pic_date"] = $add_dates[0];
            $output["delivery_time"] = $finel_Delivery_Time;
    
            return $output;
        }
        
        
        
        
        
    }

    public function old_pickupdays(Request $request){
        $iCategoryId        = $request->iCategoryId ;
        $iProductStageId    = $request->iProductStageId;
        $iOldPickupDate    = $request->iOldPickupDate;
        $vLocation    = $request->vLocation;
        // dd($iOldPickupDate);
        $output = $this->old_product_dates($iCategoryId,$iProductStageId,'',$iOldPickupDate,$vLocation);
        // dd($output);
        echo json_encode($output);
    }

    public function old_product_dates($iCategoryId,$iProductStageId,$iWorkingTime=0,$iOldPickupDate,$vLocation='')
    {
        
        // $iCustomerId    = "11";
        $category_data = Category::get_by_id($iCategoryId);
        $iCustomerId = $category_data->iCustomerId;
        $product_stage_data = ProductStage::get_by_id($iProductStageId);
        $schedule = Schedule::get_by_customerid($iCustomerId);
        $Daye_To_Pick_Up = $product_stage_data->iPickUpDay;

        $Days_To_Process = $product_stage_data->iProcessDay;
        $Days_To_Deliver = $product_stage_data->iDeliverDay;
        
        
        if ($iWorkingTime > 0) {
            $Total_delivery_days = $iWorkingTime;
        }else{
            if(isset($vLocation) && $vLocation =='In lab')
            {
                $Total_delivery_days = $Days_To_Process+$Days_To_Deliver;
            }
            else
            {
                $Total_delivery_days = $Daye_To_Pick_Up+$Days_To_Process+$Days_To_Deliver;
            }
        }
        
        
        $PickTime = $category_data->tPickTime;
        $DeliveryTime = $category_data->tDeliveryTime;

        $finel_Delivery_Time = date ('H:i',strtotime($DeliveryTime));
        
        $current_time = date('H:i');
        $current_date = date("Y-m-d");
        
        // if($current_time >= $PickTime){
        //     $pick_up_date = date("Y-m-d",strtotime($current_date.'+1 day'));
        // }else{
        //     $pick_up_date = $current_date;
        // }
        $pick_up_date = $iOldPickupDate;
        $delivery_days_for_calc = $Total_delivery_days-1;
        $estimated_delivery_date = date("Y-m-d",strtotime($pick_up_date . " +".$delivery_days_for_calc." days"));
        
        $all_dates = array();
        $all_dates[] = $pick_up_date;
        
        $temp_date = $pick_up_date;
        for ($i=1; $i <= $delivery_days_for_calc ; $i++) { 
            $temp_date = date("Y-m-d",strtotime($temp_date.'+1 day'));
            $all_dates[] = $temp_date;
        }
        
        foreach ($schedule as $key => $value) {
            if (empty($value->dtHolidays) AND !empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                $schedule_closed_days[] = $value->eWeek;
            }
        }
        
        $schedule_closed_holidays = array();
        foreach ($schedule as $key => $value) {
            if (!empty($value->dtHolidays) AND empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                $holiday_day = date('l', strtotime($value->dtHolidays));
                if (!in_array($holiday_day, $schedule_closed_days)) {
                    $schedule_closed_holidays[] = $value->dtHolidays;
                }
            }
        }
        
        $cur_dt = $pick_up_date;
        
        $add_dates = array();
        do{
            $day = date('l', strtotime($cur_dt));
            if (!in_array($day,$schedule_closed_days) AND !in_array($cur_dt,$schedule_closed_holidays)) {
                $add_dates[] = $cur_dt;
            }
            
            $cur_dt = date("Y-m-d",strtotime($cur_dt.'+1 day'));
        }while(count($add_dates) < $Total_delivery_days);
        
        // dd($add_dates);
        $output = array();
        $output["del_date"] = end($add_dates);
        $output["pic_date"] = $add_dates[0];
        $output["delivery_time"] = $finel_Delivery_Time;

        return $output;
        
    }
    
    public function rushdate(Request $request){

        $iProductStageId    = $request->iProductStageId;
        $data = Fees::get_by_iProductStageId($iProductStageId);

        $iCategoryId        = $request->iCategoryId ;//
        $iProductStageId    = $request->iProductStageId;//

        $rush_dates_array = array();
        // dd($data);
        foreach ($data as $key_main => $value_main) {
            // dd($value);
            // $iCustomerId    = "11";
            
            $category_data = Category::get_by_id($iCategoryId);
            $iCustomerId = $category_data->iCustomerId;
            $product_stage_data = ProductStage::get_by_id($iProductStageId);
            $schedule = Schedule::get_by_customerid($iCustomerId);
            $Daye_To_Pick_Up = $product_stage_data->iPickUpDay;
            $Days_To_Process = $product_stage_data->iProcessDay;
            $Days_To_Deliver = $product_stage_data->iDeliverDay;
                
            if($value_main->iWorkingTime){
                $Total_delivery_days = $value_main->iWorkingTime;
            }else{
                $Total_delivery_days = $Daye_To_Pick_Up+$Days_To_Process+$Days_To_Deliver;
            }
            
            $PickTime = $category_data->tPickTime;
            $DeliveryTime = $category_data->tDeliveryTime;

            $finel_Delivery_Time = date ('H:i',strtotime($DeliveryTime));
            
            $current_time = date('H:i');
            $current_date = date("Y-m-d");
            
            if($current_time >= $PickTime){
                $pick_up_date = date("Y-m-d",strtotime($current_date.'+1 day'));
            }else{
                $pick_up_date = $current_date;
            }

            
            
            $delivery_days_for_calc = $Total_delivery_days-1;
            $estimated_delivery_date = date("Y-m-d",strtotime($pick_up_date . " +".$delivery_days_for_calc." days"));
            
            $all_dates = array();
            $all_dates[] = $pick_up_date;
            
            $temp_date = $pick_up_date;
            for ($i=1; $i <= $delivery_days_for_calc ; $i++) { 
                $temp_date = date("Y-m-d",strtotime($temp_date.'+1 day'));
                $all_dates[] = $temp_date;
            }
            
            foreach ($schedule as $key => $value) {
                if (empty($value->dtHolidays) AND !empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                    $schedule_closed_days[] = $value->eWeek;
                }
            }
            
            $schedule_closed_holidays = array();
            foreach ($schedule as $key => $value) {
                if (!empty($value->dtHolidays) AND empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                    $holiday_day = date('l', strtotime($value->dtHolidays));
                    if (!in_array($holiday_day, $schedule_closed_days)) {
                        $schedule_closed_holidays[] = $value->dtHolidays;
                    }
                }
            }
            
            $cur_dt = $pick_up_date;
            
            $add_dates = array();
            do{
                $day = date('l', strtotime($cur_dt));
                if (!in_array($day,$schedule_closed_days) AND !in_array($cur_dt,$schedule_closed_holidays)) {
                    $add_dates[] = $cur_dt;
                }
                
                $cur_dt = date("Y-m-d",strtotime($cur_dt.'+1 day'));
            }while(count($add_dates) < $Total_delivery_days);
            
            $del_date = end($add_dates);
            // dd($add_dates);
            $output = array();
            $output["dDate"] = date("m-d-Y",strtotime($del_date));
            $output["iWorkingTime"] = $value_main->iWorkingTime;
            $output["vFee"] = $value_main->vFee;
            $output["iFeesId"] = $value_main->iFeesId;
            $output["etype"] = $request->etype;
            $rush_dates_array[$key_main] = $output; 
        }
        // dd($rush_dates_array);
        $send_data['data'] = $rush_dates_array;
        
        return view('admin.labcase.ajax_rushdate')->with($send_data);
    }

    public function old_rushdate(Request $request){
     
        if(isset($request->UpperdDeliveryRushDate) && $request->UpperdDeliveryRushDate !='')
        {
            $send_data['UpperdDeliveryRushDate'] = $request->UpperdDeliveryRushDate;
        }

        if(isset($request->LowerdDeliveryRushDate) && $request->LowerdDeliveryRushDate !='')
        {
            $send_data['LowerdDeliveryRushDate'] = $request->LowerdDeliveryRushDate;
        }
        $iOldPickupDate = $request->iOldPickupDate;

        $iProductStageId    = $request->iProductStageId;
        $data = Fees::get_by_iProductStageId($iProductStageId);

        $iCategoryId        = $request->iCategoryId ;//
        $iProductStageId    = $request->iProductStageId;//

        $rush_dates_array = array();
        // dd($data);
        foreach ($data as $key_main => $value_main) {
            // dd($value);
            // $iCustomerId    = "11";
            
            $category_data = Category::get_by_id($iCategoryId);
            $iCustomerId = $category_data->iCustomerId;
            $product_stage_data = ProductStage::get_by_id($iProductStageId);
            $schedule = Schedule::get_by_customerid($iCustomerId);
            $Daye_To_Pick_Up = $product_stage_data->iPickUpDay;
            $Days_To_Process = $product_stage_data->iProcessDay;
            $Days_To_Deliver = $product_stage_data->iDeliverDay;
                
            if($value_main->iWorkingTime){
                $Total_delivery_days = $value_main->iWorkingTime;
            }else{
                $Total_delivery_days = $Daye_To_Pick_Up+$Days_To_Process+$Days_To_Deliver;
            }
            
            $PickTime = $category_data->tPickTime;
            $DeliveryTime = $category_data->tDeliveryTime;

            $finel_Delivery_Time = date ('H:i',strtotime($DeliveryTime));
            
            $current_time = date('H:i');
            $current_date = date("Y-m-d");
            
            // if($current_time >= $PickTime){
            //     $pick_up_date = date("Y-m-d",strtotime($current_date.'+1 day'));
            // }else{
            //     $pick_up_date = $current_date;
            // }
            $pick_up_date = $iOldPickupDate;
            
            
            $delivery_days_for_calc = $Total_delivery_days-1;
            $estimated_delivery_date = date("Y-m-d",strtotime($pick_up_date . " +".$delivery_days_for_calc." days"));
            
            $all_dates = array();
            $all_dates[] = $pick_up_date;
            
            $temp_date = $pick_up_date;
            for ($i=1; $i <= $delivery_days_for_calc ; $i++) { 
                $temp_date = date("Y-m-d",strtotime($temp_date.'+1 day'));
                $all_dates[] = $temp_date;
            }
            
            foreach ($schedule as $key => $value) {
                if (empty($value->dtHolidays) AND !empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                    $schedule_closed_days[] = $value->eWeek;
                }
            }
            
            $schedule_closed_holidays = array();
            foreach ($schedule as $key => $value) {
                if (!empty($value->dtHolidays) AND empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                    $holiday_day = date('l', strtotime($value->dtHolidays));
                    if (!in_array($holiday_day, $schedule_closed_days)) {
                        $schedule_closed_holidays[] = $value->dtHolidays;
                    }
                }
            }
            
            $cur_dt = $pick_up_date;
            
            $add_dates = array();
            do{
                $day = date('l', strtotime($cur_dt));
                if (!in_array($day,$schedule_closed_days) AND !in_array($cur_dt,$schedule_closed_holidays)) {
                    $add_dates[] = $cur_dt;
                }
                
                $cur_dt = date("Y-m-d",strtotime($cur_dt.'+1 day'));
            }while(count($add_dates) < $Total_delivery_days);
            
            $del_date = end($add_dates);
            // dd($add_dates);
            $output = array();
            $output["dDate"] = date("m-d-Y",strtotime($del_date));
            $output["iWorkingTime"] = $value_main->iWorkingTime;
            $output["vFee"] = $value_main->vFee;
            $output["iFeesId"] = $value_main->iFeesId;
            $output["etype"] = $request->etype;
            $rush_dates_array[$key_main] = $output; 
        }

        $send_data['data'] = $rush_dates_array;
        // dd($send_data);
        return view('admin.labcase.ajax_rushdate')->with($send_data);
    }
    public function getcasepan(Request $request)
    {
        
        $iCategoryId = $request->iCategoryId;
        $Category    = Category::get_by_id($iCategoryId);
        $casepan     = Casepan::get_by_id($Category->iCasepanId);
        $iCasepanId  = $casepan->iCasepanId;
        $data['color']       =  $casepan->vColor;
        $data['data']        = CasepanNumber::get_by_casepan_id($iCasepanId);
       
        echo json_encode($data);
    }
    
    public function slip_view($iCaseId)
    {
        $data['labcases'] = Labcase::get_by_id($iCaseId);
        $data['slip'] = Slip::get_by_iCaseId($iCaseId);
        $iSlipId = $data['slip']->iSlipId;
        $slip_product = SlipProduct::get_by_iSlipId($iSlipId);
        
        if(isset($slip_product) && $slip_product !='')
        {
            foreach($slip_product as $key => $slip_product_val)
            {
                foreach($slip_product_val as $key_s => $slip_product_val_s)
                {
                    if($key_s == 'eType' && $slip_product_val_s == 'Upper')
                    {
                        $data['upper_product'] = $slip_product_val;
                    }
                    elseif($key_s == 'eType' && $slip_product_val_s == 'Lower')
                    {
                        $data['lower_product'] = $slip_product_val;
                    }
                    elseif($key_s == 'eType' && $slip_product_val_s == 'Oppising')
                    {
                        $data['opposing_product'] = $slip_product_val;
                    }
                }
            }
        }
        if(isset($data['lower_product'])){
            if ($data['lower_product']->iStageId != "") {
                
                $criteria = array();
                $criteria['eType'] = "Lower";
                $criteria['iProductStageId'] = $data['lower_product']->iStageId;
                $data['lowerstage'] = ProductStage::get_by_type($criteria);
            }
            $lower_dilevery_date = $data['lower_product']->dDeliveryDate;
        }
        if(isset($data['upper_product'])){
            if ($data['upper_product']->iStageId != "") {
                
                $criteria = array();
                $criteria['eType'] = "Upper";
                $criteria['iProductStageId'] = $data['upper_product']->iStageId;
                $data['upperstage'] = ProductStage::get_by_type($criteria);
            }
            $upper_dilevery_date = $data['upper_product']->dDeliveryDate;
        }
       
        
        if(isset($lower_dilevery_date) || isset($lower_dilevery_date)){
            if($upper_dilevery_date > $lower_dilevery_date){

                $data['final_dilevery_date'] = $data['upper_product']->dDeliveryDate;
                $data['final_pickup_date'] = $data['upper_product']->dPickUpDate;
                $data['final_dilevery_time'] = $data['upper_product']->tDeliveryTime;
            }
            
            if($upper_dilevery_date == $lower_dilevery_date){
                
                $data['final_dilevery_date'] = $data['upper_product']->dDeliveryDate;
                $data['final_pickup_date'] = $data['upper_product']->dPickUpDate;
                $data['final_dilevery_time'] = $data['upper_product']->tDeliveryTime;
            }
            if($upper_dilevery_date < $lower_dilevery_date){
                
                $data['final_dilevery_date'] = $data['lower_product']->dDeliveryDate;
                $data['final_pickup_date'] = $data['lower_product']->dPickUpDate;
                $data['final_dilevery_time'] = $data['lower_product']->tDeliveryTime;
            }
            
        }
        if(!empty($lower_dilevery_date) || empty($upper_dilevery_date)){
            $data['final_dilevery_date'] = $data['lower_product']->dDeliveryDate;
            $data['final_pickup_date'] = $data['lower_product']->dPickUpDate;
            $data['final_dilevery_time'] = $data['lower_product']->tDeliveryTime;
        }
        if(empty($lower_dilevery_date) || !empty($lower_dilevery_date)){
            $data['final_dilevery_date'] = $data['upper_product']->dDeliveryDate;
                $data['final_pickup_date'] = $data['upper_product']->dPickUpDate;
                $data['final_dilevery_time'] = $data['upper_product']->tDeliveryTime;
        }
        if(isset($data['upper_product'])){
            $data['upper_vTeethInMouth'] = explode(",",$data['upper_product']->vTeethInMouth);
            $data['upper_vMissingTeeth'] = explode(",",$data['upper_product']->vMissingTeeth);
            $data['upper_vWillExtractOnDelivery'] = explode(",",$data['upper_product']->vWillExtractOnDelivery);
            $data['upper_vHasBeenExtracted'] = explode(",",$data['upper_product']->vHasBeenExtracted);
            $data['upper_vFixOrAdd'] = explode(",",$data['upper_product']->vFixOrAdd);
            $data['upper_vClasps'] = explode(",",$data['upper_product']->vClasps);


            $upper_teeth_in_mouth = array();
            $upper_extract= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

            $upper_merged_array = array_merge($data['upper_vMissingTeeth'],$data['upper_vWillExtractOnDelivery'],$data['upper_vHasBeenExtracted'],$data['upper_vFixOrAdd'],$data['upper_vClasps']);

            foreach ($upper_extract as $value) {
                if (!in_array($value,$upper_merged_array)) {
                    $upper_teeth_in_mouth[] = $value;
                }
            }
            
            $data['upper_vTeethInMouth'] = array_unique(array_merge($data['upper_vTeethInMouth'],$upper_teeth_in_mouth));
        }
        if(isset($data['lower_product'])){

            $data['lower_vTeethInMouth'] = explode(",",$data['lower_product']->vTeethInMouth);
            $data['lower_vMissingTeeth'] = explode(",",$data['lower_product']->vMissingTeeth);
            $data['lower_vWillExtractOnDelivery'] = explode(",",$data['lower_product']->vWillExtractOnDelivery);
            $data['lower_vHasBeenExtracted'] = explode(",",$data['lower_product']->vHasBeenExtracted);
            $data['lower_vFixOrAdd'] = explode(",",$data['lower_product']->vFixOrAdd);
            $data['lower_vClasps'] = explode(",",$data['lower_product']->vClasps);

            
            $teeth_in_mouth = array();
            $lower_extract= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");
            
            $merged_array = array_merge($data['lower_vMissingTeeth'],$data['lower_vWillExtractOnDelivery'],$data['lower_vHasBeenExtracted'],$data['lower_vFixOrAdd'],$data['lower_vClasps']);
            
            foreach ($lower_extract as $value) {
                if (!in_array($value,$merged_array)) {
                    $teeth_in_mouth[] = $value;
                }
            }

            $data['lower_vTeethInMouth'] = array_unique(array_merge($data['lower_vTeethInMouth'],$teeth_in_mouth));
        }
        $criteria[]  = "";
        $criteria['eType'] = "Upper";
        $criteria['iSlipId'] = $iSlipId;
        $data['upper_addons'] = SlipAddons::get_by_iSlipId($criteria);

        $criteria[]  = "";
        $criteria['eType'] = "Lower";
        $criteria['iSlipId'] = $iSlipId;
        $data['lower_addons'] = SlipAddons::get_by_iSlipId($criteria);

        // dd($data);


        $criteria[]  = "";
        $criteria['eType'] = "Upper";
        $criteria['iSlipId'] = $iSlipId;
        $data['upper_impression'] = SlipImpression::get_by_iSlipId($criteria);
        


        $criteria[]  = "";
        $criteria['eType'] = "Lower";
        $criteria['iSlipId'] = $iSlipId;
        $data['lower_impression'] = SlipImpression::get_by_iSlipId($criteria);
        
        
        // Lower Impression End
        
        return view('admin.labcase.slip_view')->with($data);
    }
    
    public function gallery_action(Request $request)
	{
        // dd($request);
        $postPath = public_path('uploads/slip_gallery/');
        $iTampId = $request->iTampId;
        $iCaseId = $request->iCaseId;
        $iSlipId = $request->iSlipId;
        // dd($iSlipId);
        // dd($request->hasFile('vImage'));
        if (!empty($request->hasFile('vImage'))) 
        {
            $fileType = $_FILES['vImage']['type']; 
                if($fileType == "video/mp4" || $fileType == "video/mkv")
                {
                    $video = $_FILES['vImage']['name'];
                    if (!file_exists($postPath)) {
                        mkdir($postPath, 0777, TRUE);
                    }
                
                    $source_file = $postPath .'/'. $video;
                    $source_file_new = str_replace('//', '/', $source_file);
                    move_uploaded_file($_FILES['vImage']['tmp_name'], $source_file_new);
                    $data['vName']         = $video;
                    $data['iTampId']       = $iTampId;
                    $data['iCaseId']       = $iCaseId;
                    $data['iSlipId']       = $iSlipId;
                    $data['eType']         = "Video";
                    SlipGallery::add($data);
                    sleep(1);
				}
				if($fileType == "image/jpg" || $fileType == "image/png" || $fileType == "image/jpeg" )
				{
					$image = $_FILES['vImage']['name'];
                    if (!file_exists($postPath)) {
                        mkdir($postPath, 0777, TRUE);
                    }

                    $source_file = $postPath .'/'. $image;

                    $source_file_new = str_replace('//', '/', $source_file);

                    move_uploaded_file($_FILES['vImage']['tmp_name'], $source_file_new);
                    $data['vName']         = $image;
                    $data['iTampId']       = $iTampId;
                    $data['iCaseId']       = $iCaseId;
                    $data['iSlipId']       = $iSlipId;
                    $data['eType']         = "Image";
                    SlipGallery::add($data);
                    sleep(1);

				}
				if($fileType == "audio/mpeg" || $fileType == "audio/mp3" || $fileType == "audio/ogg")
				{
                    dd(3);
					$audio = $_FILES['vImage']['name'];

					
                    if (!file_exists($postPath)) {
                        mkdir($postPath, 0777, TRUE);
                    }

                    $source_file = $postPath .'/'. $audio;

                    $source_file_new = str_replace('//', '/', $source_file);

                    move_uploaded_file($_FILES['vImage']['tmp_name'], $source_file_new);

                    $data['vName']         = $audio;
                    $data['iTampId']       = $iTampId;
                    $data['iCaseId']       = $iCaseId;
                    $data['iSlipId']       = $iSlipId;
                    $data['eType']         = "Audio";
                    SlipGallery::add($data);
                    sleep(1);
				}
				if($fileType == 'application/zip' || $fileType == "model/stl" )
				{
					$audio = $_FILES['vImage']['name'];
                    if (!file_exists($postPath)) {
                        mkdir($postPath, 0777, TRUE);
                    }

                    $source_file = $postPath .'/'. $audio;

                    $source_file_new = str_replace('//', '/', $source_file);

                    move_uploaded_file($_FILES['vImage']['tmp_name'], $source_file_new);

                    $data['vName']         = $audio;
                    $data['iTampId']       = $iTampId;
                    $data['iCaseId']       = $iCaseId;
                    $data['iSlipId']       = $iSlipId;
                    $data['eType']         = "File";
                    SlipGallery::add($data);
                    sleep(1);
				}
			}
	}
   
    public function attacement(Request $request)
    {      
        $iTampId = $request->iTampId;
        
        $data['data'] = SlipGallery::get_by_iTampId($iTampId);
        return view('admin.labcase.ajax_attachment_list_add')->with($data);  
        
    }

    public function attacement_newstage(Request $request)
    {      
        $iCaseId = $request->iCaseId;
        $data['data'] = SlipGallery::get_by_iCaseId($iCaseId);
        // dd($data);
        $data['data_new'] = Slip::get_attatchment_data($iCaseId);
        return view('admin.labcase.ajax_attachment_list')->with($data);  
        
    }
    public function view_attacement(Request $request)
    {      
        $iSlipId = $request->iSlipId;
        $iCaseId = $request->iCaseId;
        $data['data'] = SlipGallery::get_by_iCaseId($iCaseId);
        $data['data_new'] = Slip::get_attatchment_data($iCaseId);
        // dd($data);
        return view('admin.labcase.ajax_attachment_list')->with($data);  
        
    }
    public function delete_gallery(Request $request)
    {
        $iSlipAttachmentId = $request->id;
        $iTampId = $request->iTampId;

        $product_info = SlipGallery::get_by_id($iSlipAttachmentId);
        
        
        $where                         = array();
        $where['iSlipAttachmentId']    = $request->id;
        
        SlipGallery::delete_by_id($where);
        
        $data['data'] = SlipGallery::get_by_iTampId($iTampId);

        return view('admin.labcase.ajax_attachment_list')->with($data);  
        

    }
    public function delete_gallery_slip(Request $request)
    {
        $iSlipAttachmentId = $request->id;
        $product_info = SlipGallery::get_by_id($iSlipAttachmentId);
        $where                         = array();
        $where['iSlipAttachmentId']    = $request->id;
        SlipGallery::delete_by_id($where);
        return 'success';
    }
    
    public function virtual_slip($iCaseId,$iSlipId)
    {
        $data['Case'] = Labcase::get_by_id($iCaseId);
        $data['Slip'] = Slip::get_by_id($iSlipId);
        
        // For Security purpose start
        $iCustomerId = General::admin_info()['iCustomerId'];
        $customerType =General::admin_info()['customerType'];
        if(isset($customerType) && $customerType =='Office Admin')
        {
            if(isset($data['Slip']->iOfficeId) && !empty($data['Slip']->iOfficeId))
            {
                $MatchId = $data['Slip']->iOfficeId;
            }
            else
            {
                $MatchId = '';
            }
        }
        if(isset($customerType) && $customerType =='Lab Admin')
        {
            if(isset($data['Slip']->iLabId) && !empty($data['Slip']->iLabId))
            {
                $MatchId = $data['Slip']->iLabId;
            }
            else
            {
                $MatchId = '';
            }
        }
      
        if((isset($MatchId) && $MatchId != $iCustomerId && !empty($iCustomerId)) || (empty($MatchId)) || (empty($data['Case'])))
        {
            if(isset($customerType) && $customerType =='Office Admin')
            {
                return redirect()->route('admin.labcase.office_admin_listing')->withError('Invalid Input');
            }
            elseif(isset($customerType) && $customerType =='Lab Admin')
            {
                return redirect()->route('admin.labcase')->withError('Invalid Input');
            }
        }
        // For Security purpose end

        $iOfficeId = $data['Slip']->iOfficeId;
        $iLabId = $data['Slip']->iLabId;
        
      
        $data['CustomerOfficeData'] = Customer::get_by_office_id($iOfficeId);
        $data['CustomerLabData'] = Customer::get_by_office_id($iLabId);
        $data['upper_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Upper");
        $data['lower_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Lower");

        $data["rush"] = "No";

        if (!empty($data['upper_product'])) {

            $data['upper_vTeethInMouth'] = explode(",",$data['upper_product']->vTeethInMouth);
            $data['upper_vMissingTeeth'] = explode(",",$data['upper_product']->vMissingTeeth);
            $data['upper_vWillExtractOnDelivery'] = explode(",",$data['upper_product']->vWillExtractOnDelivery);
            $data['upper_vHasBeenExtracted'] = explode(",",$data['upper_product']->vHasBeenExtracted);
            $data['upper_vFixOrAdd'] = explode(",",$data['upper_product']->vFixOrAdd);
            $data['upper_vClasps'] = explode(",",$data['upper_product']->vClasps);

            $upper_teeth_in_mouth = array();
            $upper_extract= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

            $upper_merged_array = array_merge(
                $data['upper_vMissingTeeth'],
                $data['upper_vWillExtractOnDelivery'],
                $data['upper_vHasBeenExtracted'],
                $data['upper_vFixOrAdd'],
                $data['upper_vClasps']
            );

            foreach ($upper_extract as $value) {
                if (!in_array($value,$upper_merged_array)) {
                    $upper_teeth_in_mouth[] = $value;
                }
            }
            
            $data['upper_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['upper_vTeethInMouth'],
                    $upper_teeth_in_mouth
                )
            );

            $data['lower_vTeethInMouthOp'] = explode(",",$data['upper_product']->vTeethInMouthOp);
            $data['lower_vMissingTeethOp'] = explode(",",$data['upper_product']->vMissingTeethOp);
            $data['lower_vWillExtractOnDeliveryOp'] = explode(",",$data['upper_product']->vWillExtractOnDeliveryOp);
            $data['lower_vHasBeenExtractedOp'] = explode(",",$data['upper_product']->vHasBeenExtractedOp);
            $data['lower_vFixOrAddOp'] = explode(",",$data['upper_product']->vFixOrAddOp);
            $data['lower_vClaspsOp'] = explode(",",$data['upper_product']->vClaspsOp);

            if (!empty($data['lower_vTeethInMouthOp']) AND !empty($data['lower_vMissingTeethOp']) AND !empty($data['lower_vWillExtractOnDeliveryOp']) AND !empty($data['lower_vHasBeenExtractedOp']) AND !empty($data['lower_vFixOrAddOp']) AND !empty($data['lower_vClaspsOp'])) {

                $lower_teeth_in_mouthOp = array();
                $lower_extractOp= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");

                $lower_merged_arrayOp = array_merge(
                    $data['lower_vMissingTeethOp'],
                    $data['lower_vWillExtractOnDeliveryOp'],
                    $data['lower_vHasBeenExtractedOp'],
                    $data['lower_vFixOrAddOp'],
                    $data['lower_vClaspsOp']
                );

                foreach ($lower_extractOp as $value) {
                    if (!in_array($value,$lower_merged_arrayOp)) {
                        $lower_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['lower_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['lower_vTeethInMouthOp'],
                        $lower_teeth_in_mouthOp
                    )
                );
            }
            // dd($data['lower_vTeethInMouthOp']);

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['lower_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['lower_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['lower_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $upper_delivery_date = $data['upper_product']->dDeliveryDate;

            
            if ($data['upper_product']->eRushDate == "Yes" AND !empty($data['upper_product']->eRushDate == "Yes")) {
                // if(!empty($data['lower_product'])){

                    $data["rush"] = "Yes";
                // }
            }
        }

        if (!empty($data['lower_product'])) {
            $data['lower_vTeethInMouth'] = explode(",",$data['lower_product']->vTeethInMouth);
            $data['lower_vMissingTeeth'] = explode(",",$data['lower_product']->vMissingTeeth);
            $data['lower_vWillExtractOnDelivery'] = explode(",",$data['lower_product']->vWillExtractOnDelivery);
            $data['lower_vHasBeenExtracted'] = explode(",",$data['lower_product']->vHasBeenExtracted);
            $data['lower_vFixOrAdd'] = explode(",",$data['lower_product']->vFixOrAdd);
            $data['lower_vClasps'] = explode(",",$data['lower_product']->vClasps);

            
            $teeth_in_mouth = array();
            $lower_extract= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");
            
            $merged_array = array_merge(
                $data['lower_vMissingTeeth'],
                $data['lower_vWillExtractOnDelivery'],
                $data['lower_vHasBeenExtracted'],
                $data['lower_vFixOrAdd'],
                $data['lower_vClasps']
            );
            
            foreach ($lower_extract as $value) {
                if (!in_array($value,$merged_array)) {
                    $teeth_in_mouth[] = $value;
                }
            }

            $data['lower_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['lower_vTeethInMouth'],
                    $teeth_in_mouth
                )
            );


            $data['upper_vTeethInMouthOp'] = explode(",",$data['lower_product']->vTeethInMouthOp);
            $data['upper_vMissingTeethOp'] = explode(",",$data['lower_product']->vMissingTeethOp);
            $data['upper_vWillExtractOnDeliveryOp'] = explode(",",$data['lower_product']->vWillExtractOnDeliveryOp);
            $data['upper_vHasBeenExtractedOp'] = explode(",",$data['lower_product']->vHasBeenExtractedOp);
            $data['upper_vFixOrAddOp'] = explode(",",$data['lower_product']->vFixOrAddOp);
            $data['upper_vClaspsOp'] = explode(",",$data['lower_product']->vClaspsOp);

            if (!empty($data['upper_vTeethInMouthOp']) AND !empty($data['upper_vMissingTeethOp']) AND !empty($data['upper_vWillExtractOnDeliveryOp']) AND !empty($data['upper_vHasBeenExtractedOp']) AND !empty($data['upper_vFixOrAddOp']) AND !empty($data['upper_vClaspsOp'])) {

                $upper_teeth_in_mouthOp = array();
                $upper_extractOp= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

                $upper_merged_arrayOp = array_merge(
                    $data['upper_vMissingTeethOp'],
                    $data['upper_vWillExtractOnDeliveryOp'],
                    $data['upper_vHasBeenExtractedOp'],
                    $data['upper_vFixOrAddOp'],
                    $data['upper_vClaspsOp']
                );

                foreach ($upper_extractOp as $value) {
                    if (!in_array($value,$upper_merged_arrayOp)) {
                        $upper_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['upper_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['upper_vTeethInMouthOp'],
                        $upper_teeth_in_mouthOp
                    )
                );
            }


            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['upper_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['upper_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['upper_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_addons'] = SlipAddons::get_by_iSlipId($criteria);



            $lower_delivery_date = $data['lower_product']->dDeliveryDate;
            if ($data['lower_product']->eRushDate == "Yes" AND !empty($data['lower_product']->eRushDate == "Yes")) {
                // if(!empty($data['upper_product'])){
                    $data["rush"] = "Yes";
                // }
            }
        }

        $criteria[]  = "";
        $criteria['iSlipId'] = $iSlipId;
        $data['stage_note'] = SlipNote::get_by_iSlipId($criteria);
        $data['rush_product_both'] = 'no';
        $data['rush_product'] = 'no';
        
        if((isset($data['upper_product']) && $data['upper_product'] !='') && (isset($data['lower_product']) && $data['lower_product']!=''))
        {
            $data['is_single_product'] ='no';
        }
        else
        {
            $data['is_single_product'] ='yes';
        }
        if ($data["rush"] == "No") {  
            
            if ((isset($upper_delivery_date) && !empty($upper_delivery_date)) && (isset($lower_delivery_date) && !empty($lower_delivery_date))) 
            {
                if($upper_delivery_date >= $lower_delivery_date)
                {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                    $data['is_single_product'] ='yes';
                }
                else
                {
                    $data['final_delivery_date'] = $lower_delivery_date;
                    $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
                    $data['is_single_product'] ='yes';
                }
            }
            else if ((isset($upper_delivery_date) && !empty($upper_delivery_date))) 
            {
                $data['final_delivery_date'] = $upper_delivery_date;
                $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
            }
            else if(isset($lower_delivery_date) && !empty($lower_delivery_date))
            {
                $data['final_delivery_date'] = $lower_delivery_date;
                $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
            }
            // dd($data);
            
        }else {
            
            // For Show rush date && delivery date start
            if((isset($data['upper_product']) && $data['upper_product'] !='') && (isset($data['lower_product']) && $data['lower_product']!=''))
            {
               
                $UpperDeliveryRushDate = $data['upper_product']->dDeliveryRushDate;
                $LowerDeliveryRushDate = $data['lower_product']->dDeliveryRushDate;
             
                if ((isset($UpperDeliveryRushDate) && !empty($UpperDeliveryRushDate)) && (isset($LowerDeliveryRushDate) && !empty($LowerDeliveryRushDate)))
                {
                    if(isset($UpperDeliveryRushDate) && !empty($UpperDeliveryRushDate))
                    {
                        $data['final_delivery_date_upper'] = $UpperDeliveryRushDate;
                        $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                        $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                        $data['rush_product'] = 'yes';
                        $data['rush_product_both'] = 'yes';
                    }
                    if(isset($LowerDeliveryRushDate) && !empty($LowerDeliveryRushDate))
                    {
                        $data['final_delivery_date_lower'] = $LowerDeliveryRushDate;
                        $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                        $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                        $data['rush_product'] = 'yes';
                        $data['rush_product_both'] = 'yes';
                    }
                }
                 if(isset($data['upper_product']) && $data['upper_product'] !='')
                {        
                           
                    $UpperDeliveryRushDate = $data['upper_product']->dDeliveryRushDate;
                    if (isset($UpperDeliveryRushDate) && !empty($UpperDeliveryRushDate)) {
                        $data['final_delivery_date'] = $UpperDeliveryRushDate;
                        $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                        $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                        $data['rush_product'] = 'yes';
                        $data['upper_rush_product'] = 'yes';
                    }
                }
                 if(isset($data['lower_product']) && $data['lower_product']!='')
                {
                    
                    $LowerDeliveryRushDate = $data['lower_product']->dDeliveryRushDate;
                    if (isset($LowerDeliveryRushDate) && !empty($LowerDeliveryRushDate)) {
                        $data['final_delivery_date'] = $LowerDeliveryRushDate;
                        $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                        $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                        $data['rush_product'] = 'yes';
                        $data['lower_rush_product'] = 'yes';
                    }
                }
                // dd($data['final_delivery_date_lower']);
            }
            else if(isset($data['upper_product']) && $data['upper_product'] !='')
            {         
                       
                $UpperDeliveryRushDate = $data['upper_product']->dDeliveryRushDate;
                if (isset($UpperDeliveryRushDate) && !empty($UpperDeliveryRushDate)) {
                    $data['final_delivery_date'] = $UpperDeliveryRushDate;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                    $data['rush_product'] = 'yes';
                    $data['upper_rush_product'] = 'yes';
                }
            }
            else if(isset($data['lower_product']) && $data['lower_product']!='')
            {
              
                $LowerDeliveryRushDate = $data['lower_product']->dDeliveryRushDate;
                if (isset($LowerDeliveryRushDate) && !empty($LowerDeliveryRushDate)) {
                    $data['final_delivery_date'] = $LowerDeliveryRushDate;
                    $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
                    $data['rush_product'] = 'yes';
                    $data['lower_rush_product'] = 'yes';
                }
            }
            // dd($data);
            // For Show rush date && delivery date end
        } 
        $data['tampid'] =  str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
        
        // Slip gallary data
        $data['gallary_image_exist'] = SlipGallery::get_by_iCaseId($iCaseId);

        return view('admin.labcase.virtual_slip')->with($data);
    }

    public function paper_slip(Request $request)
    {
        $iCaseId = $request->iCaseId;
        $iSlipId = $request->iSlipId;
        
        $data['Case'] = Labcase::get_by_id($iCaseId);
        $data['Slip'] = Slip::get_by_id($iSlipId);
        
         
        $iOfficeId = $data['Slip']->iOfficeId;
        $iLabId = $data['Slip']->iLabId;
      
        $data['CustomerOfficeData'] = Customer::get_by_office_id($iOfficeId);
        $data['CustomerLabData'] = Customer::get_by_office_id($iLabId);
        $data['Doctor'] = Doctor::get_by_id($data['Slip']->iDoctorId);
        $data['upper_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Upper");
        $data['lower_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Lower");

        $data["rush"] = "No";

        if (!empty($data['upper_product'])) {

            $data['upper_vTeethInMouth'] = explode(",",$data['upper_product']->vTeethInMouth);
            $data['upper_vMissingTeeth'] = explode(",",$data['upper_product']->vMissingTeeth);
            $data['upper_vWillExtractOnDelivery'] = explode(",",$data['upper_product']->vWillExtractOnDelivery);
            $data['upper_vHasBeenExtracted'] = explode(",",$data['upper_product']->vHasBeenExtracted);
            $data['upper_vFixOrAdd'] = explode(",",$data['upper_product']->vFixOrAdd);
            $data['upper_vClasps'] = explode(",",$data['upper_product']->vClasps);

            $upper_teeth_in_mouth = array();
            $upper_extract= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

            $upper_merged_array = array_merge(
                $data['upper_vMissingTeeth'],
                $data['upper_vWillExtractOnDelivery'],
                $data['upper_vHasBeenExtracted'],
                $data['upper_vFixOrAdd'],
                $data['upper_vClasps']
            );

            foreach ($upper_extract as $value) {
                if (!in_array($value,$upper_merged_array)) {
                    $upper_teeth_in_mouth[] = $value;
                }
            }
            
            $data['upper_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['upper_vTeethInMouth'],
                    $upper_teeth_in_mouth
                )
            );

            $data['lower_vTeethInMouthOp'] = explode(",",$data['upper_product']->vTeethInMouthOp);
            $data['lower_vMissingTeethOp'] = explode(",",$data['upper_product']->vMissingTeethOp);
            $data['lower_vWillExtractOnDeliveryOp'] = explode(",",$data['upper_product']->vWillExtractOnDeliveryOp);
            $data['lower_vHasBeenExtractedOp'] = explode(",",$data['upper_product']->vHasBeenExtractedOp);
            $data['lower_vFixOrAddOp'] = explode(",",$data['upper_product']->vFixOrAddOp);
            $data['lower_vClaspsOp'] = explode(",",$data['upper_product']->vClaspsOp);

            if (!empty($data['lower_vTeethInMouthOp']) AND !empty($data['lower_vMissingTeethOp']) AND !empty($data['lower_vWillExtractOnDeliveryOp']) AND !empty($data['lower_vHasBeenExtractedOp']) AND !empty($data['lower_vFixOrAddOp']) AND !empty($data['lower_vClaspsOp'])) {

                $lower_teeth_in_mouthOp = array();
                $lower_extractOp= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");

                $lower_merged_arrayOp = array_merge(
                    $data['lower_vMissingTeethOp'],
                    $data['lower_vWillExtractOnDeliveryOp'],
                    $data['lower_vHasBeenExtractedOp'],
                    $data['lower_vFixOrAddOp'],
                    $data['lower_vClaspsOp']
                );

                foreach ($lower_extractOp as $value) {
                    if (!in_array($value,$lower_merged_arrayOp)) {
                        $lower_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['lower_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['lower_vTeethInMouthOp'],
                        $lower_teeth_in_mouthOp
                    )
                );
            }
            // dd($data['lower_vTeethInMouthOp']);

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['lower_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['lower_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['lower_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $upper_delivery_date = $data['upper_product']->dDeliveryDate;
            if ($data['upper_product']->eRushDate == "Yes" AND !empty($data['upper_product']->eRushDate == "Yes")) {
                // if(!empty($data['lower_product'])){
            
                    $data["rush"] = "Yes";
                // }
            }
        }

        if (!empty($data['lower_product'])) {
            $data['lower_vTeethInMouth'] = explode(",",$data['lower_product']->vTeethInMouth);
            $data['lower_vMissingTeeth'] = explode(",",$data['lower_product']->vMissingTeeth);
            $data['lower_vWillExtractOnDelivery'] = explode(",",$data['lower_product']->vWillExtractOnDelivery);
            $data['lower_vHasBeenExtracted'] = explode(",",$data['lower_product']->vHasBeenExtracted);
            $data['lower_vFixOrAdd'] = explode(",",$data['lower_product']->vFixOrAdd);
            $data['lower_vClasps'] = explode(",",$data['lower_product']->vClasps);

            
            $teeth_in_mouth = array();
            $lower_extract= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");
            
            $merged_array = array_merge(
                $data['lower_vMissingTeeth'],
                $data['lower_vWillExtractOnDelivery'],
                $data['lower_vHasBeenExtracted'],
                $data['lower_vFixOrAdd'],
                $data['lower_vClasps']
            );
            
            foreach ($lower_extract as $value) {
                if (!in_array($value,$merged_array)) {
                    $teeth_in_mouth[] = $value;
                }
            }

            $data['lower_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['lower_vTeethInMouth'],
                    $teeth_in_mouth
                )
            );


            $data['upper_vTeethInMouthOp'] = explode(",",$data['lower_product']->vTeethInMouthOp);
            $data['upper_vMissingTeethOp'] = explode(",",$data['lower_product']->vMissingTeethOp);
            $data['upper_vWillExtractOnDeliveryOp'] = explode(",",$data['lower_product']->vWillExtractOnDeliveryOp);
            $data['upper_vHasBeenExtractedOp'] = explode(",",$data['lower_product']->vHasBeenExtractedOp);
            $data['upper_vFixOrAddOp'] = explode(",",$data['lower_product']->vFixOrAddOp);
            $data['upper_vClaspsOp'] = explode(",",$data['lower_product']->vClaspsOp);

            if (!empty($data['upper_vTeethInMouthOp']) AND !empty($data['upper_vMissingTeethOp']) AND !empty($data['upper_vWillExtractOnDeliveryOp']) AND !empty($data['upper_vHasBeenExtractedOp']) AND !empty($data['upper_vFixOrAddOp']) AND !empty($data['upper_vClaspsOp'])) {

                $upper_teeth_in_mouthOp = array();
                $upper_extractOp= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

                $upper_merged_arrayOp = array_merge(
                    $data['upper_vMissingTeethOp'],
                    $data['upper_vWillExtractOnDeliveryOp'],
                    $data['upper_vHasBeenExtractedOp'],
                    $data['upper_vFixOrAddOp'],
                    $data['upper_vClaspsOp']
                );

                foreach ($upper_extractOp as $value) {
                    if (!in_array($value,$upper_merged_arrayOp)) {
                        $upper_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['upper_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['upper_vTeethInMouthOp'],
                        $upper_teeth_in_mouthOp
                    )
                );
            }


            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['upper_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['upper_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['upper_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $lower_delivery_date = $data['lower_product']->dDeliveryDate;
            if ($data['lower_product']->eRushDate == "Yes" AND !empty($data['lower_product']->eRushDate == "Yes")) {
                // if(!empty($data['upper_product'])){
                    $data["rush"] = "Yes";
                // }
            }
        }

         $data['rush_product_both'] = 'no';
         $data['rush_product'] = 'no';
        
        if((isset($data['upper_product']) && $data['upper_product'] !='') && (isset($data['lower_product']) && $data['lower_product']!=''))
        {
            $data['is_single_product'] ='no';
        }
        else
        {
            $data['is_single_product'] ='yes';
        }
        // dd($data['upper_product']->tDeliveryTime);
        if ($data["rush"] == "No") {  
       
            if ((isset($upper_delivery_date) && !empty($upper_delivery_date)) && (isset($lower_delivery_date) && !empty($lower_delivery_date))) 
            {
                if($upper_delivery_date >= $lower_delivery_date)
                {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                    $data['is_single_product'] ='yes';
                }
                else
                {
                    $data['final_delivery_date'] = $lower_delivery_date;
                    $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
                    $data['is_single_product'] ='yes';
                }
            }
            else if ((isset($upper_delivery_date) && !empty($upper_delivery_date))) 
            {
                $data['final_delivery_date'] = $upper_delivery_date;
                $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
            }
            else if(isset($lower_delivery_date) && !empty($lower_delivery_date))
            {
                $data['final_delivery_date'] = $lower_delivery_date;
                    $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
            }
         
        }else {
            // For Show rush date && delivery date start
            if((isset($data['upper_product']) && $data['upper_product'] !='') && (isset($data['lower_product']) && $data['lower_product']!=''))
            {
                $UpperDeliveryRushDate = $data['upper_product']->dDeliveryRushDate;
                $LowerDeliveryRushDate = $data['lower_product']->dDeliveryRushDate;
             
                if ((isset($UpperDeliveryRushDate) && !empty($UpperDeliveryRushDate)) && (isset($LowerDeliveryRushDate) && !empty($LowerDeliveryRushDate)))
                {
                    if(isset($UpperDeliveryRushDate) && !empty($UpperDeliveryRushDate))
                    {
                        $data['final_delivery_date_upper'] = $UpperDeliveryRushDate;
                        $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                        $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                        $data['rush_product'] = 'yes';
                        $data['rush_product_both'] = 'yes';
                    }
                    if(isset($LowerDeliveryRushDate) && !empty($LowerDeliveryRushDate))
                    {
                        $data['final_delivery_date_lower'] = $LowerDeliveryRushDate;
                        $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                        $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                        $data['rush_product'] = 'yes';
                        $data['rush_product_both'] = 'yes';
                    }
                }
                else if(isset($data['upper_product']) && $data['upper_product'] !='')
                {                
                    $UpperDeliveryRushDate = $data['upper_product']->dDeliveryRushDate;
                    if (isset($UpperDeliveryRushDate) && !empty($UpperDeliveryRushDate)) {
                        $data['final_delivery_date'] = $UpperDeliveryRushDate;
                        $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                        $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                        $data['rush_product'] = 'yes';
                        $data['upper_rush_product'] = 'yes';
                    }
                }
                else if(isset($data['lower_product']) && $data['lower_product']!='')
                {
                    $LowerDeliveryRushDate = $data['lower_product']->dDeliveryRushDate;
                    if (isset($LowerDeliveryRushDate) && !empty($LowerDeliveryRushDate)) {
                        $data['final_delivery_date'] = $LowerDeliveryRushDate;
                        $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                        $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                        $data['rush_product'] = 'yes';
                        $data['lower_rush_product'] = 'yes';
                    }
                }
                // dd($data['final_delivery_date_lower']);
            }
            else if(isset($data['upper_product']) && $data['upper_product'] !='')
            {                
                $UpperDeliveryRushDate = $data['upper_product']->dDeliveryRushDate;
                if (isset($UpperDeliveryRushDate) && !empty($UpperDeliveryRushDate)) {
                    $data['final_delivery_date'] = $UpperDeliveryRushDate;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                    $data['rush_product'] = 'yes';
                    $data['upper_rush_product'] = 'yes';
                }
            }
            else if(isset($data['lower_product']) && $data['lower_product']!='')
            {
                $LowerDeliveryRushDate = $data['lower_product']->dDeliveryRushDate;
                if (isset($LowerDeliveryRushDate) && !empty($LowerDeliveryRushDate)) {
                    $data['final_delivery_date'] = $LowerDeliveryRushDate;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                    $data['rush_product'] = 'yes';
                    $data['lower_rush_product'] = 'yes';
                }
            }
            // For Show rush date && delivery date end
        }
        

        return view('admin.labcase.paper_slip')->with($data);
        // $pdf = PDF::loadView('admin.labcase.paper_slip',$data);
        // return $pdf->stream("print_slip.pdf", array("Attachment" => false));
    }    
    //For Open Driver Model history Start Jun 1 2022
    function view_driverhistory(Request $request)
    {
        // dd($request->CurrentStatus);
        $iSlipId = $request->iSlipId;
        $iCaseId = $request->iCaseId;
        // $iLoginId = General::admin_info()['user_id'];
        $vPatientName = $request->vPatientName;
        $vOfficeName = $request->vOfficeName;
        $vCaseNumber = $request->vCaseNumber;
        
        $vCasePanNumber = $request->vCasePanNumber;
        if(isset($iSlipId) && !empty($iSlipId))
        {
            $data['data'] = array(
                'iSlipId'=>$iSlipId,
                'vPatientName'=>$vPatientName,
                'vOfficeName'=>$vOfficeName,
                'vCaseNumber'=>$vCaseNumber,
                'vCasePanNumber'=>$vCasePanNumber,
            );
            $where_array_slip = array(
                'iSlipId'=>$iSlipId
            );
            $slipData = DriverHistory::get_slip_data($where_array_slip);
            $data['slip_number'] = $slipData->vSlipNumber;
            
            if(isset($request->CurrentStatus) && $request->CurrentStatus !=null && $request->CurrentStatus !='')
            {
                $data['CurrentStatus'] = $request->CurrentStatus;
            }
            if(isset($request->HistoryType) && $request->HistoryType =='DriverHistory')
            {
                $data['HistoryType'] = $request->HistoryType;
            }
            else
            {
                $data['HistoryType'] = 'ShowAll';
            }
            $where_array = array(
                'iCaseId'=>$iCaseId,
                // 'iLoginId'=>$iLoginId
            );
            $data['show_history']=DriverHistory::show_history($where_array);
            return view('admin.labcase.driver_model_popup')->with($data);
        }
    }
    function store_driverHistory(Request $request)
    {
        
        $data =  array();
        $iLoginId = General::admin_info()['user_id'];;
        $vLoginUserName = General::admin_info()['vName'];
        // dd($iLoginId);
        $iSlipId = $request->iSlipId;
        if($iSlipId !='' && isset($iSlipId))
        {
            $data_new = DriverHistory::get_history_data($iSlipId);
            $case_data = Labcase::get_by_id($data_new->iCaseId);
        }
        $SlipData = SlipProduct::get_by_iSlipId($iSlipId);
        $SlipSData = Slip::get_by_id($iSlipId);
        $SlipProductData = SlipProduct::get_by_slip_id($iSlipId);
       
        if(count($SlipProductData) == 1) 
        {
            $data['iStageId']     = $SlipProductData[0]->iProductStageId;
            $data['vStageName']     =  $SlipProductData[0]->vName;
        }
        elseif(count($SlipProductData)>1)
        {
            $data['iStageId']     = $SlipProductData[0]->iProductStageId."/".$SlipProductData[1]->iProductStageId;
            $data['vStageName']     =  $SlipProductData[0]->vName."/". $SlipProductData[1]->vName;
        }
        $data['iCaseId']=$data_new->iCaseId;
        $data['vPatientName']=$data_new->vPatientName;
        $data['iOfficeId']=$data_new->iOfficeId;
        $data['iCasepanId']=$data_new->iCasepanId;
        $data['vCasePanNumber']=$case_data->vCasePanNumber;
        $data['vOfficeName']=$data_new->vOfficeName;
        $data['iSlipId'] = $request->iSlipId;
        $data['vSignature'] = $request->vSignature;
        $data['iLoginId'] = $iLoginId;
        $data['vLoginUserName'] = $vLoginUserName;
        if(isset($request->CurrentStatus) && $request->CurrentStatus =='DropOff')
        {
            $data['eUserType'] = 'Receiver';
            $data['vLocation']='On route to the lab';
        }
        else if(isset($request->CurrentStatus) && $request->CurrentStatus =='LabPickUp')
        {
            $data['eUserType'] = 'Sender';
            $data['vLocation']='In lab ready to pickup';
        }
        else if(isset($request->CurrentStatus) && $request->CurrentStatus =='LabDropUp')
        {
            $data['eUserType'] = 'Driver';
            $data['vLocation']='On route to the office';
        }
        else
        {
            $data['eUserType'] = 'Driver';
            $data['vLocation']='In office ready to pickup';
        }    
        if(isset($request->vOfficeCode) && !empty($request->vOfficeCode))
        {
            $data['vOfficeCode']=$request->vOfficeCode;
        }
        $data['vSlipNumber'] = $SlipSData->vSlipNumber;
        $data['dtAddedDate'] = date("Y-m-d H:i:s");
        if(isset($data) && !empty($data))
        {
            $return = DriverHistory::insert($data);
        }
        try
        {
          
            $where_array =array(
                'iCaseId'=>$data_new->iCaseId,
                'iSlipId'=>$iSlipId,
            );
            
            if(isset($request->CurrentStatus) && $request->CurrentStatus =='DropOff')
            {
                $data_updated = array(
                    'vLocation'=>'In lab',
                    'dtUpdatedDate'=>date("Y-m-d H:i:s"),
                );
            }
            else if(isset($request->CurrentStatus) && $request->CurrentStatus =='LabPickUp')
            {
                $data_updated = array(
                    'vLocation'=>'On route to the office',
                    'dtUpdatedDate'=>date("Y-m-d H:i:s"),
                );
            }
            else if(isset($request->CurrentStatus) && $request->CurrentStatus =='LabDropUp')
            {
                $data_updated = array(
                    'vLocation'=>'In office',
                    'dtUpdatedDate'=>date("Y-m-d H:i:s"),
                );
                
                if(((isset($SlipData[0]->vStageName) && $SlipData[0]->vStageName =='Finish') && (isset($SlipData[1]->vStageName) && $SlipData[1]->vStageName =='Finish')) || ((isset($SlipData[0]->vStageName) && $SlipData[0]->vStageName =='Finish') && (!isset($SlipData[1]->vStageName))) 
                || ((!isset($SlipData[0]->vStageName)) && (isset($SlipData[1]->vStageName) && $SlipData[1]->vStageName =='Finish')))
                {
                    $where_casepannumber_release = array(
                        'iCasepanId'=>$data_new->iCasepanId,
                        'vNumber'=>$case_data->vCasePanNumber,
                    );
                    $data_relsecasepan = array(
                        'eUse' => 'No',
                        'dtUpdatedDate'=>date("Y-m-d H:i:s")
                    );
                    CasepanNumber::update_data($where_casepannumber_release,$data_relsecasepan);

                    // Update case status finish start
                    $iCaseId = $data_new->iCaseId;
                    $where_labcase = array(
                        'iCaseId'=>$iCaseId
                    );
                    
                    $data_labcase = array(
                        'eStatus'=>'Finished',
                        'dtUpdatedDate'=>date("Y-m-d H:i:s")
                    );
                    Labcase::update_data($where_labcase,$data_labcase);
                    // Update case status finish end
                    
                }
            }
            else
            {
                $data_updated = array(
                    'vLocation'=>'On route to the lab',
                    'dtUpdatedDate'=>date("Y-m-d H:i:s"),
                );
            } 
            if(isset($request->CurrentStatus) && ($request->CurrentStatus =='LabPickUp' || $request->CurrentStatus =='LabDropUp'))
            {
                $data_product =  array(
                    'eStatus'=>'Finished',
                    'dtUpdatedDate'=>date("Y-m-d H:i:s"),
                );
            }
            else
            {
                $data_product =  array(
                    'eStatus'=>'On Process',
                    'dtUpdatedDate'=>date("Y-m-d H:i:s"),
                );
            }   
           
            try
            {
                if(isset($request->is_qr_record) && $request->is_qr_record =='Yes')
                {
                    $where_array =array(
                        'iCaseId'=>$request->iCaseId,
                        'iSlipId'=>$request->iSlipId,
                    );
                }
                DriverHistory::update_slip_location($where_array,$data_updated);
                $result = DriverHistory::update_product_status($where_array,$data_product);
                if(isset($request->is_qr_record) && $request->is_qr_record =='Yes')
                {
                    return redirect()->route('admin.labcase')->withSuccess('Driver history updated successfully.');
                }
                else
                {
                    return redirect()->back()->withSuccess('Driver history updated successfully.');
                }
            }
            catch(Exeption $e)
            {
                return redirect()->back()->withError('Something went wrong');
            }
                
        }   
        catch(Exeption $e)
        {
            return redirect()->back()->withError('Something went wrong');
        }
        
    }
    function store_multiple_driverHistory(Request $request)
    {
        if(isset($request->slipIds) && !empty($request->slipIds))
        {
            $SlipIds= explode(',',$request->slipIds);
            $locationArray = Labcase::find_location_by_slip_id($SlipIds);
          
            if(isset($locationArray) && !empty($locationArray) && count($locationArray)>0)
            {
                foreach($locationArray as $key=> $locationArrayVal)
                {
                    $CurrentStatus = '';
                    if($locationArrayVal->vLocation=='In lab ready to pickup')
                    {
                        $CurrentStatus ='LabPickUp';
                    }
                    elseif($locationArrayVal->vLocation=='In office ready to pickup')
                    {
                        $CurrentStatus ='OfficePickUp';
                    }
                    elseif($locationArrayVal->vLocation=='On route to the lab')
                    {
                        $CurrentStatus ='DropOff';
                    }
                    elseif($locationArrayVal->vLocation=='On route to the office')
                    {
                        $CurrentStatus ='LabDropUp';
                    }
                    $data =  array();
                    $iLoginId = General::admin_info()['user_id'];;
                    $vLoginUserName = General::admin_info()['vName'];
                    // dd($iLoginId);
                    $iSlipId = $locationArrayVal->iSlipId;
                    if($iSlipId !='' && isset($iSlipId))
                    {
                        $data_new = DriverHistory::get_history_data($iSlipId);
                        $case_data = Labcase::get_by_id($data_new->iCaseId);
                    }
                    $SlipData = SlipProduct::get_by_iSlipId($iSlipId);
                    $SlipSData = Slip::get_by_id($iSlipId);
                    $SlipProductData = SlipProduct::get_by_slip_id($iSlipId);
                
                    if(count($SlipProductData) == 1) 
                    {
                        $data['iStageId']     = $SlipProductData[0]->iProductStageId;
                        $data['vStageName']     =  $SlipProductData[0]->vName;
                    }
                    elseif(count($SlipProductData)>1)
                    {
                        $data['iStageId']     = $SlipProductData[0]->iProductStageId."/".$SlipProductData[1]->iProductStageId;
                        $data['vStageName']     =  $SlipProductData[0]->vName."/". $SlipProductData[1]->vName;
                    }
                    $data['iCaseId']=$data_new->iCaseId;
                    $data['vPatientName']=$data_new->vPatientName;
                    $data['iOfficeId']=$data_new->iOfficeId;
                    $data['iCasepanId']=$data_new->iCasepanId;
                    $data['vCasePanNumber']=$case_data->vCasePanNumber;
                    $data['vOfficeName']=$data_new->vOfficeName;
                    $data['iSlipId'] = $locationArrayVal->iSlipId;
                    $data['vSignature'] = $request->vSignature;
                    $data['iLoginId'] = $iLoginId;
                    $data['vLoginUserName'] = $vLoginUserName;
                    if(isset($CurrentStatus) && $CurrentStatus =='DropOff')
                    {
                        $data['eUserType'] = 'Receiver';
                        $data['vLocation']='On route to the lab';
                    }
                    else if(isset($CurrentStatus) && $CurrentStatus =='LabPickUp')
                    {
                        $data['eUserType'] = 'Sender';
                        $data['vLocation']='In lab ready to pickup';
                    }
                    else if(isset($CurrentStatus) && $CurrentStatus =='LabDropUp')
                    {
                        $data['eUserType'] = 'Driver';
                        $data['vLocation']='On route to the office';
                    }
                    else
                    {
                        $data['eUserType'] = 'Driver';
                        $data['vLocation']='In office ready to pickup';
                    }    
                   
                    if(isset($request->vOfficeCode) && !empty($request->vOfficeCode))
                    {
                        $data['vOfficeCode']=$request->vOfficeCode;
                    }
                    $data['vSlipNumber'] = $SlipSData->vSlipNumber;
                    $data['dtAddedDate'] = date("Y-m-d H:i:s");
                    if(isset($data) && !empty($data))
                    {
                        $return = DriverHistory::insert($data);
                    }
                    try
                    {
                    
                        $where_array =array(
                            'iCaseId'=>$data_new->iCaseId,
                            'iSlipId'=>$iSlipId,
                        );
                      
                        if(isset($CurrentStatus) && $CurrentStatus =='DropOff')
                        {
                            $data_updated = array(
                                'vLocation'=>'In lab',
                                'dtUpdatedDate'=>date("Y-m-d H:i:s"),
                            );
                        }
                        else if(isset($CurrentStatus) && $CurrentStatus =='LabPickUp')
                        {
                            $data_updated = array(
                                'vLocation'=>'On route to the office',
                                'dtUpdatedDate'=>date("Y-m-d H:i:s"),
                            );
                        }
                        else if(isset($CurrentStatus) && $CurrentStatus =='LabDropUp')
                        {
                            $data_updated = array(
                                'vLocation'=>'In office',
                                'dtUpdatedDate'=>date("Y-m-d H:i:s"),
                            );
                            
                            if(((isset($SlipData[0]->vStageName) && $SlipData[0]->vStageName =='Finish') && (isset($SlipData[1]->vStageName) && $SlipData[1]->vStageName =='Finish')) || ((isset($SlipData[0]->vStageName) && $SlipData[0]->vStageName =='Finish') && (!isset($SlipData[1]->vStageName))) 
                            || ((!isset($SlipData[0]->vStageName)) && (isset($SlipData[1]->vStageName) && $SlipData[1]->vStageName =='Finish')))
                            {
                                $where_casepannumber_release = array(
                                    'iCasepanId'=>$data_new->iCasepanId,
                                    'vNumber'=>$case_data->vCasePanNumber,
                                );
                                $data_relsecasepan = array(
                                    'eUse' => 'No',
                                    'dtUpdatedDate'=>date("Y-m-d H:i:s")
                                );
                                CasepanNumber::update_data($where_casepannumber_release,$data_relsecasepan);
        
                                // Update case status finish start
                                $iCaseId = $data_new->iCaseId;
                                $where_labcase = array(
                                    'iCaseId'=>$iCaseId
                                );
                                
                                $data_labcase = array(
                                    'eStatus'=>'Finished',
                                    'dtUpdatedDate'=>date("Y-m-d H:i:s")
                                );
                                Labcase::update_data($where_labcase,$data_labcase);
                                // Update case status finish end
                                
                            }
                        }
                        else
                        {
                            $data_updated = array(
                                'vLocation'=>'On route to the lab',
                                'dtUpdatedDate'=>date("Y-m-d H:i:s"),
                            );
                        } 
                        if(isset($CurrentStatus) && ($CurrentStatus =='LabPickUp' || $CurrentStatus =='LabDropUp'))
                        {
                            $data_product =  array(
                                'eStatus'=>'Finished',
                                'dtUpdatedDate'=>date("Y-m-d H:i:s"),
                            );
                        }
                        else
                        {
                            $data_product =  array(
                                'eStatus'=>'On Process',
                                'dtUpdatedDate'=>date("Y-m-d H:i:s"),
                            );
                        }   
                    
                        try
                        {
                            if(isset($request->is_qr_record) && $request->is_qr_record =='Yes')
                            {
                                $where_array =array(
                                    'iCaseId'=>$data_new->iCaseId,
                                    'iSlipId'=>$locationArrayVal->iSlipId,
                                );
                            }
                            DriverHistory::update_slip_location($where_array,$data_updated);
                            $result = DriverHistory::update_product_status($where_array,$data_product);
                            
                        }
                        catch(Exeption $e)
                        {
                            return redirect()->back()->withError('Something went wrong');
                        }
                            
                    }   
                    catch(Exeption $e)
                    {
                        return redirect()->back()->withError('Something went wrong');
                    }   
                }
                return redirect()->route('admin.labcase')->withSuccess('Driver history updated successfully.');
            }
            else
            {
                return redirect()->route('admin.labcase')->withError('Location not found');
            }
        }
        else
        {
            return redirect()->route('admin.labcase')->withError("Slip id's not found");
        }
        
    }
    function view_notes(Request $request)
    {
        // dd($request->CurrentStatus);
        $iSlipId = $request->iSlipId;
        $iCaseId = $request->iCaseId;
        $iLoginId = General::admin_info()['user_id'];
        $vPatientName = $request->vPatientName;
        $vOfficeName = $request->vOfficeName;
        $vCaseNumber = $request->vCaseNumber;
        $vCasePanNumber = $request->vCasePanNumber;
          
        $data['data'] = array(
            'iSlipId'=>$iSlipId,
            'vPatientName'=>$vPatientName,
            'vOfficeName'=>$vOfficeName,
            'vCaseNumber'=>$vCaseNumber,
            'vCasePanNumber'=>$vCasePanNumber,
        );

        // $criteria['iSlipId'] = $iSlipId;
        $criteria['iCaseId'] = $iCaseId;
      
        // $data['slip_notes']= SlipNote::get_by_iSlipId($criteria);
        $data['slip_notes']= SlipNote::get_by_iCaseId($criteria);
        // dd($data);      
        return view('admin.labcase.notes_popup')->with($data);
        
    }

    public function ready_to_send(Request $request)
    {
        $status =  'failed';
        $data =  array();
        $iLoginId = General::admin_info()['user_id'];;
        $vLoginUserName = General::admin_info()['vName'];
        $iSlipId = $request->iSlipId;
        if($iSlipId !='' && isset($iSlipId))
        {
            $data_new = DriverHistory::get_history_data($iSlipId);
        }
        $SlipSData = Slip::get_by_id($iSlipId);
        $data['iCaseId']=$data_new->iCaseId;
        $data['vPatientName']=$data_new->vPatientName;
        $data['iOfficeId']=$data_new->iOfficeId;
        $data['iCasepanId']=$data_new->iCasepanId;
        $data['vOfficeCode']=$data_new->vOfficeCode;
        
        $data['vCasePanNumber']=$data_new->vCasePanNumber;
        $data['vLocation']='In lab';
        $data['vOfficeName']=$data_new->vOfficeName;
        $data['iSlipId'] = $request->iSlipId;
        // $data['vSignature'] = $request->vSignature;
        $data['iLoginId'] = $iLoginId;
        $data['vLoginUserName'] = $vLoginUserName;
        $data['eUserType'] = 'Sender';
        $data['dtAddedDate'] = date("Y-m-d H:i:s");
        $data['vSlipNumber'] = $SlipSData->vSlipNumber;
        if(isset($data) && !empty($data))
        {
            $return = DriverHistory::insert($data);

            $where_array =array(
                'iCaseId'=>$data_new->iCaseId,
                'iSlipId'=>$iSlipId,
            );
            $data_updated = array(
                'vLocation'=>'In lab ready to pickup',
                'dtUpdatedDate'=>date("Y-m-d H:i:s"),
            );
            $data_product =  array(
                'eStatus'=>'Finished',
                'dtUpdatedDate'=>date("Y-m-d H:i:s"),
            );
            DriverHistory::update_slip_location($where_array,$data_updated);
            $result = DriverHistory::update_product_status($where_array,$data_product);
            $status = 'success';
            $this->update_billing($data_new->iCaseId,$iSlipId);
        }
        return $status;
    }
    public function ready_to_send_qr($iSlipId)
    {
        $status =  'failed';
        $data =  array();
        $iLoginId = General::admin_info()['user_id'];;
        $vLoginUserName = General::admin_info()['vName'];
        if($iSlipId !='' && isset($iSlipId))
        {
            $data_new = DriverHistory::get_history_data($iSlipId);
        }
        $SlipSData = Slip::get_by_id($iSlipId);
        $data['iCaseId']=$data_new->iCaseId;
        $data['vPatientName']=$data_new->vPatientName;
        $data['iOfficeId']=$data_new->iOfficeId;
        $data['iCasepanId']=$data_new->iCasepanId;
        $data['vOfficeCode']=$data_new->vOfficeCode;
        
        $data['vCasePanNumber']=$data_new->vCasePanNumber;
        $data['vLocation']='In lab';
        $data['vOfficeName']=$data_new->vOfficeName;
        $data['iSlipId'] = $iSlipId;
        // $data['vSignature'] = $request->vSignature;
        $data['iLoginId'] = $iLoginId;
        $data['vLoginUserName'] = $vLoginUserName;
        $data['eUserType'] = 'Sender';
        $data['dtAddedDate'] = date("Y-m-d H:i:s");
        $data['vSlipNumber'] = $SlipSData->vSlipNumber;
        if(isset($data) && !empty($data))
        {
            $return = DriverHistory::insert($data);

            $where_array =array(
                'iCaseId'=>$data_new->iCaseId,
                'iSlipId'=>$iSlipId,
            );
            $data_updated = array(
                'vLocation'=>'In lab ready to pickup',
                'dtUpdatedDate'=>date("Y-m-d H:i:s"),
            );
            $data_product =  array(
                'eStatus'=>'Finished',
                'dtUpdatedDate'=>date("Y-m-d H:i:s"),
            );
            DriverHistory::update_slip_location($where_array,$data_updated);
            $result = DriverHistory::update_product_status($where_array,$data_product);
            $status = 'success';
            $this->update_billing($data_new->iCaseId,$iSlipId);
        }
        return redirect()->route('admin.labcase')->withSuccess('Driver history updated successfully.');
    }
    //For Open Driver Model history End Jun 1 2022

    // For Edit Satge Start
    public function edit_stage_notes(Request $request)
    {
        $tDescription = $request->tDescription_Ed;
        $iSlipId = $request->iSlipId;
        $vSlipNumber = $request->vSlipNumber;
        $iCaseId = $request->iCaseId;
        $vPatientName = $request->vPatientName;
        $vOfficeName = $request->vOfficeName;
        $vCaseNumber = $request->vCaseNumber;


        if(!empty($request->tDescription_Ed)){

            $slip_notes = array();
            $slip_notes['iSlipId']      = $iSlipId;
            $slip_notes['iCaseId']      = $iCaseId;
            // Get Stage Id Start
            $SlipProductData = SlipProduct::get_by_slip_id($iSlipId);
           
            if(count($SlipProductData) == 1) 
            {
                $slip_notes['iStageId']     = $SlipProductData[0]->iProductStageId;
                $slip_notes['vStageName']     =  $SlipProductData[0]->vName;
            }
            elseif(count($SlipProductData)>1)
            {
                $slip_notes['iStageId']     = $SlipProductData[0]->iProductStageId."/".$SlipProductData[1]->iProductStageId;
                $slip_notes['vStageName']     =  $SlipProductData[0]->vName."/". $SlipProductData[1]->vName;
            }

            $upper_dilevery_date =  $request->upper_delivery_date;
            $lower_dilevery_date =  $request->lower_delivery_date;

            if($upper_dilevery_date > $lower_dilevery_date){
                
                $slip_notes['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
            }

            if($upper_dilevery_date == $lower_dilevery_date){
                                
                    $slip_notes['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
            }

            if($upper_dilevery_date < $lower_dilevery_date){
                                
                $slip_notes['dDeliveryDate'] =  date("Y-m-d",strtotime($request->lower_delivery_date));
            }

            $slip_notes['vCaseNumber']  = $vCaseNumber;
            $slip_notes['vCreatedByName'] = General::admin_info()['vName'];
            $slip_notes['tStagesNotes']  =  $tDescription;
            $slip_notes['vSlipNumber']   = $vSlipNumber;
            $slip_notes['dtAddedDate'] = date("Y-m-d H:i:s");
            $slip_notes['tAddedTime'] = date("H:i");
            
            $slip_note = SlipNote::add($slip_notes);
           
        }


    }
    // For Edit Satge End

  
    public function view_call_log(Request $request)
    {
        $iSlipId = $request->iSlipId;
        $iCaseId = $request->iCaseId;
        if(isset($iSlipId) && $iSlipId!='' && isset($iCaseId) && $iCaseId !='')
        {
            $vPatientName = $request->vPatientName;
            $vOfficeName = $request->vOfficeName;
            $vCaseNumber = $request->vCaseNumber;
            $vCasePanNumber = $request->vCasePanNumber;
            $vStageName = $request->vStageName;
            $data['data'] = array(
                'iSlipId'=>$iSlipId,
                'vPatientName'=>$vPatientName,
                'vOfficeName'=>$vOfficeName,
                'vCaseNumber'=>$vCaseNumber,
                'vCasePanNumber'=>$vCasePanNumber,
                'vStageName'=>$vStageName
            );
            $where_array_slip = array(
                'iSlipId'=>$iSlipId
            );
            $data['slip_number']= DriverHistory::get_slip_data($where_array_slip)->vSlipNumber;
            $data['callHistory'] = Call::get_by_ids($iSlipId,$iCaseId);
            // dd($data);
            $data['iSlipId'] = $iSlipId;
            $data['iCaseId'] = $iCaseId;
            return view('admin.labcase.call_log_history_modal')->with($data);
        }
    }
    public function store_call_log(Request $request)
    {
       
        $iSlipId = $request->iSlipId;
        $iCaseId = $request->iCaseId;
        
        if(isset($iSlipId) && $iSlipId!='' && isset($iCaseId) && $iCaseId !='')
        {
            $data['callHistory'] = Call::get_by_ids($iSlipId,$iCaseId);
            $data['iSlipId'] = $iSlipId;
            $data['iCaseId'] = $iCaseId;
            
            $name = session('data');
           
            $data['username'] = $name['vName'];

            // view history
            $vPatientName = $request->vPatientName;
            $vOfficeName = $request->vOfficeName;
            $vCaseNumber = $request->vCaseNumber;
            $vCasePanNumber = $request->vCasePanNumber;
            $vStageName = $request->vStageName;
            $data['data'] = array(
                'iSlipId'=>$iSlipId,
                'vPatientName'=>$vPatientName,
                'vOfficeName'=>$vOfficeName,
                'vCaseNumber'=>$vCaseNumber,
                'vCasePanNumber'=>$vCasePanNumber,
                'vStageName'=>$vStageName
            );
            $where_array_slip = array(
                'iSlipId'=>$iSlipId
            );
            $data['slip_number']= DriverHistory::get_slip_data($where_array_slip)->vSlipNumber;
            $data['callHistory'] = Call::get_by_ids($iSlipId,$iCaseId);
            // dd($data);
            $data['iSlipId'] = $iSlipId;
            $data['iCaseId'] = $iCaseId;
            return view('admin.labcase.call_log_model')->with($data);
        }
    }
    
    public function insert_call_log(Request $request)
    {
        $iCallId = $request->id;
        $data['tTime'] 	= $request->tTime;
        $data['dDate'] 	= $request->dDate;
        $data['vName']     = $request->vName;
        $data['tDescription']= $request->tDescription;
        $data['vCallTaken']= $request->vCallTaken;
        $data['iSlipId'] = $request->iSlipId;
        $data['iCaseId'] = $request->iCaseId;
        $vSlipNumber = $request->vSlipNumber;
        $SlipProductData = SlipProduct::get_by_slip_id($request->iSlipId);
        if(count($SlipProductData) == 1) 
        {
            $data['iStageId']     = $SlipProductData[0]->iProductStageId;
            $data['vStageName']     =  $SlipProductData[0]->vName;
        }
        elseif(count($SlipProductData)>1)
        {
            $data['iStageId']     = $SlipProductData[0]->iProductStageId."/".$SlipProductData[1]->iProductStageId;
            $data['vStageName']     =  $SlipProductData[0]->vName."/". $SlipProductData[1]->vName;
        }
        $data['vSlipNumber'] = $vSlipNumber;

        if($iCallId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iCallId)){
            $where                      = array();
            $where['iCallId']       = $iCallId;

            $Call_id_update = new Call();
            $Call_id_update->update($where, $data);

            return redirect()->back()->withSuccess('Call updated successfully.');
        }
        else{
            $Call_id = Call::add($data);
            return redirect()->back()->withSuccess('Call created successfully.');
        }
    }


    public function new_stage_add(Request $request)
    {
        $Case_id = $request->iCaseId;
        
        //  Set Session For Print Slip Start
        if(!empty($request->information_checkbox) && $request->information_checkbox == "Yes" && isset($Case_id) && $Case_id !='')
        {
            Session::put('Sess_iCaseId', $Case_id);
        }
        //  Set Session For Print Slip End

        // Get Slip Data Start
        $where_slip = array(
            'iCaseId'=>$Case_id
        );
        $OldSlipData =  Labcase::get_old_slip_data($where_slip);
        $where_slip_product = array(
            'iSlipId'=>$OldSlipData->iSlipId
        );
        $OldSlipProductData =  Labcase::get_old_slip_product_data($where_slip_product);
        // Get Case Data End
        
        $slip_data['iCaseId']       = $request->iCaseId;
        $slip_data['vSlipNumber']   = $request->vSlipNumber;
        $slip_data['iLabId']        = isset($OldSlipData->iLabId)?$OldSlipData->iLabId:null;;
        $slip_data['iOfficeId']     = isset($OldSlipData->iOfficeId)?$OldSlipData->iOfficeId:null;
        $slip_data['iDoctorId']     = isset($OldSlipData->iDoctorId)?$OldSlipData->iDoctorId:null;
        $slip_data['vPatientName']  = isset($OldSlipData->vPatientName)?$OldSlipData->vPatientName:null;
        $slip_data['vCasePanNumber']= isset($OldSlipData->vCasePanNumber)?$OldSlipData->vCasePanNumber:null;
        $slip_data['vCaseNumber']   = isset($OldSlipData->vCaseNumber)?$OldSlipData->vCaseNumber:null;
        $slip_data['iCreatedById']  = isset($OldSlipData->iCreatedById)?$OldSlipData->iCreatedById:null;
        $slip_data['iCasepanId']  = isset($OldSlipData->iCasepanId)?$OldSlipData->iCasepanId:null;
        $slip_data['vCreatedByName']= $request->vCreatedByName;
        $slip_data['eCreatedByType']= $request->eCreatedByType;
        
        if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
            $slip_data['eStatus']       = "On Process";
        }else{
            $slip_data['eStatus']       = $request->eStatus;
        }
        if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
            $slip_data['vLocation']     = "In office ready to pickup";
        }else{
            $slip_data['vLocation']     = $request->vLocation;
        }
        $slip_data['tStagesNotes']  =  $request->tDescription;
        
        $slip_data['dtAddedDate'] = date("Y-m-d H:i:s");
        $slip_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        if(isset($request->upper_delivery_date) && !empty($request->upper_delivery_date) && isset($request->upper_pickup_date) && !empty($request->upper_pickup_date))
        {
            $slip_data['dPickUpDate']= $request->upper_pickup_date;
            $slip_data['dDeliveryDate']= $request->upper_delivery_date;
            $slip_data['tDeliveryTime']= $request->upper_delivery_time;
        }
        else if(isset($request->lower_delivery_date) && !empty($request->lower_delivery_date) && isset($request->lower_pickup_date) && !empty($request->lower_pickup_date))
        {
            $slip_data['dPickUpDate']= $request->lower_pickup_date;
            $slip_data['dDeliveryDate']= $request->lower_delivery_date;
            $slip_data['tDeliveryTime']= $request->lower_delivery_time;
        }
        $iSlipId = Slip::add($slip_data);
        
        //  Set Session For Print Slip Start
        if(!empty($request->information_checkbox) && $request->information_checkbox == "Yes" && isset($iSlipId) && $iSlipId !='')
        {
            Session::put('Sess_iSlipId', $iSlipId);
        }
        //  Set Session For Print Slip End
        
        /* Attachment Upload*/
        $iTampId = $request->iTampId;
        $criteria 				 = array();
        $criteria['iTampId'] 	 = $iTampId;
        

        /* Attachment Upload*/
        /* Slip Upper Product Update*/
        // dd($request);
        if (!empty($request->iUpperCategoryId)) {
            
            $slip_product_data['iSlipId']       = $iSlipId;
            $slip_product_data['eType']         = "Upper";
            $slip_product_data['iCategoryId']   = $request->iUpperCategoryId;
            $slip_product_data['iProductId']    = isset($OldSlipProductData->iProductId)?$OldSlipProductData->iProductId:null;
            $slip_product_data['iGradeId']      = isset($OldSlipProductData->iGradeId)?$OldSlipProductData->iGradeId:null;
            $slip_product_data['iStageId']      = $request->iUpperStageId;
            $slip_product_data['eExtraction']   = $request->upper_extraction_stage;
            $slip_product_data['eGumShade']     = $request->upper_gumshade_stage;
            $slip_product_data['eTeethShade']   = $request->upper_teethshade_stage;
            $slip_product_data['eImpression']   = $request->upper_Impression_stage;
            // $slip_product_data['eRushDate']     = $request->upper_rush_date_stage;  
            $slip_product_data['iToothBrandId']  = $request->upper_tsbrand;
            $slip_product_data['iToothShadesId'] = $request->upper_tshade;
            $slip_product_data['iGumBrandId']    = $request->upper_gsbrand;
            $slip_product_data['iGumShadesId']   = $request->upper_gshade;
            $slip_product_data['vTeethInMouth']  = $request->upper_in_mouth_value;
            $slip_product_data['vMissingTeeth']  = $request->upper_missing_teeth_value;
            $slip_product_data['vWillExtractOnDelivery']= $request->upper_ectract_delivery_value;
            $slip_product_data['vHasBeenExtracted']= $request->upper_been_extracted_value;
            $slip_product_data['vFixOrAdd']      = $request->upper_fix_value;
            $slip_product_data['vClasps']        = $request->upper_clasps_value;
            $slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
            $slip_product_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
           
            $iSlipProductId = SlipProduct::add($slip_product_data);           
            
            /*Upper Impression Start*/
            $impression_data_array = $request->upper_impression_checkbox;
            if(!empty($impression_data_array)){
                foreach ($impression_data_array as $key => $value) 
                {
                    $impression_data = array();
                    $impression_data['iSlipId']         = $iSlipId;
                    $impression_data['iImpressionId']   = $value;
                    $impression_data['iQuantity']       = $request->input("upper_impression_qty_".$value);
                    $impression_data['eType']           = "Upper";
                    SlipImpression::add($impression_data);
                }
            }
            /*Upper Impression End*/
            
            /*Upper Addons Start*/
            $upper_add_on_cat_id = $request->upper_add_on_cat_id;
            $upper_add_on_id = $request->upper_add_on_id;
            $upper_add_on_qty = $request->upper_add_on_qty;
            if(!empty($upper_add_on_cat_id)){
                foreach ($upper_add_on_cat_id as $key => $value) 
                {
                    $upper_slip_product_addons = array();
                    $upper_slip_product_addons['iSlipId']      = $iSlipId;
                    $upper_slip_product_addons['iAddCategoryId'] = $value;
                    $upper_slip_product_addons['iCaseId'] = $Case_id;
                    $upper_slip_product_addons['iSubAddCategoryId'] = $upper_add_on_id[$key];
                    $upper_slip_product_addons['iQuantity'] = $upper_add_on_qty[$key];
                    $upper_slip_product_addons['eType'] = "Upper";
                    SlipAddons::add($upper_slip_product_addons);
                }
            }
            /*Upper Addons End*/
            

            /*Opposite Impressions Start*/
            
            if (empty($request->iLowerCategoryId)) {
                $lowerimpression_data_array = $request->lower_impression_checkbox;
                if(!empty($lowerimpression_data_array)){
                    foreach ($lowerimpression_data_array as $key => $value) 
                    {
                        $lower_impression_qty = $request->input("lower_impression_qty_".$value);
                        $lower_impression_data = array();
                        $lower_impression_data['iSlipId']      = $iSlipId;
                        $lower_impression_data['iImpressionId'] = $value;
                        $lower_impression_data['iQuantity'] = $lower_impression_qty;
                        $lower_impression_data['eType'] = "Opposite";
                        SlipImpression::add($lower_impression_data);
                    }
                }
            }
            /*Opposite Impressions End*/
            /*Opposite Extractions Start*/
            
            if (empty($request->iLowerCategoryId)) {
                $lower_opposite_extraction = array();
                $lower_opposite_extraction['vTeethInMouthOp']  = $request->lower_in_mouth_value;
                $lower_opposite_extraction['vMissingTeethOp']  = $request->lower_missing_teeth_value;
                // dd($request);
                $lower_opposite_extraction['vWillExtractOnDeliveryOp']= $request->lower_ectract_delivery_value;
                $lower_opposite_extraction['vHasBeenExtractedOp']= $request->lower_been_extracted_value;
                $lower_opposite_extraction['vFixOrAddOp']      = $request->lower_fix_value;
                $lower_opposite_extraction['vClaspsOp']        = $request->lower_clasps_value;
                $where                      = array();
                $where['iSlipProductId']       = $iSlipProductId;
                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $lower_opposite_extraction);
            }
            /*Opposite Extractions End*/

            
            /* Upper Dates Settings */

            $upper_without_rush = $this->product_dates($request->iUpperCategoryId,$request->iUpperStageId);
            $upper_date_update = array();
            if(isset($upper_without_rush) && count($upper_without_rush)>0)
            {
                $upper_date_update['dPickUpDate'] = $upper_without_rush['pic_date'];
                $upper_date_update['dDeliveryDate'] = $upper_without_rush['del_date'];
                $upper_date_update['dDeliveryDateFinal'] = $upper_without_rush['del_date'];
                $upper_date_update['tDeliveryTime'] = $upper_without_rush['delivery_time'];
                $where                         = array();
                $where['iSlipProductId']       = $iSlipProductId;
                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $upper_date_update);
            }

            
            if(!empty($request->upper_rush_feesid))
            {    
                $fees_data = Fees::get_by_id($request->upper_rush_feesid);
                $output = $this->product_dates($request->iUpperCategoryId,$request->iUpperStageId, $fees_data->iWorkingTime);
                $upper_rushdate_update = array();
                $upper_rushdate_update['dDeliveryRushDate'] = $output['del_date'];
                $upper_rushdate_update['dDeliveryDateFinal'] = $output['del_date'];
                $upper_rushdate_update['eRushDate'] = "Yes";
                $upper_rushdate_update['iFeesId'] = $request->upper_rush_feesid;
                $where                         = array();
                $where['iSlipProductId']       = $iSlipProductId;
                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $upper_rushdate_update);
                $upper_slip_rushdate_update['dUpperRushDate'] = $output['del_date'];

                $where                  = array();
                $where['iSlipId']       = $iSlipId;
                $Slip_id_Update = new Slip();
                $Slip_id_Update->update($where, $upper_slip_rushdate_update);
            }
            /* Upper Dates Settings */

            /*Lower Product Start*/
        }
     
        if (!empty($request->iLowerCategoryId)) {
            $lower_slip_product_data['iSlipId']       = $iSlipId;
            $lower_slip_product_data['eType']         = "Lower";
            $lower_slip_product_data['iCategoryId']   = $request->iLowerCategoryId;
            $lower_slip_product_data['iProductId']    = $request->iLowerProductId;
            $lower_slip_product_data['iGradeId']      = $request->iLowerGradeId;
            $lower_slip_product_data['iStageId']      = $request->iLowerStageId;
            $lower_slip_product_data['eExtraction']   = $request->lower_extraction_stage;
            $lower_slip_product_data['eGumShade']     = $request->lower_gumshade_stage;
            $lower_slip_product_data['eTeethShade']   = $request->lower_teethshade_stage;
            $lower_slip_product_data['eImpression']   = $request->lower_Impression_stage;
            // $lower_slip_product_data['eRushDate']     = $request->lower_rush_date_stage;
            $lower_slip_product_data['iToothBrandId']  = $request->lower_tsbrand;
            $lower_slip_product_data['iToothShadesId'] = $request->lower_tshade;
            $lower_slip_product_data['iGumBrandId']    = $request->lower_gsbrand;
            $lower_slip_product_data['iGumShadesId']   = $request->lower_gshade;
            $lower_slip_product_data['vTeethInMouth']  = $request->lower_in_mouth_value;
            $lower_slip_product_data['vMissingTeeth']  = $request->lower_missing_teeth_value;
            $lower_slip_product_data['vWillExtractOnDelivery']= $request->lower_ectract_delivery_value;
            $lower_slip_product_data['vHasBeenExtracted']= $request->lower_been_extracted_value;
            $lower_slip_product_data['vFixOrAdd']      = $request->lower_fix_value;
            $lower_slip_product_data['vClasps']        = $request->lower_clasps_value;
            // $lower_slip_product_data['eStatus']       =  $request->lower_status;
            $lower_slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
            $lower_slip_product_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            // dd($lower_slip_product_data);
            $iSlipProductId = SlipProduct::add($lower_slip_product_data);
            
            /*Lower Impression Start*/
            $lowerimpression_data_array = $request->lower_impression_checkbox;

            
            if(!empty($lowerimpression_data_array)){
                foreach ($lowerimpression_data_array as $key => $value) 
                {
                    $lower_impression_qty = $request->input("lower_impression_qty_".$value);
                    $lower_impression_data = array();
                    $lower_impression_data['iSlipId']      = $iSlipId;
                    $lower_impression_data['iImpressionId'] = $value;
                    $lower_impression_data['iQuantity'] = $lower_impression_qty;
                    $lower_impression_data['eType'] = "Lower";
                    SlipImpression::add($lower_impression_data);
                }
            }
            /*Lower Impression End*/

            /*Lower Addons Start*/
            $lower_add_on_cat_id = $request->lower_add_on_cat_id;
            $lower_add_on_id = $request->lower_add_on_id;
            $lower_add_on_qty = $request->lower_add_on_qty;
            if(!empty($lower_add_on_cat_id)){
                foreach ($lower_add_on_cat_id as $key => $value) 
                {
                    $lower_slip_product_addons = array();
                    $lower_slip_product_addons['iSlipId']      = $iSlipId;
                    $lower_slip_product_addons['iCaseId']      = $Case_id;
                    $lower_slip_product_addons['iAddCategoryId'] = $value;
                    $lower_slip_product_addons['iSubAddCategoryId'] = $lower_add_on_id[$key];
                    $lower_slip_product_addons['iQuantity'] = $lower_add_on_qty[$key];
                    $lower_slip_product_addons['eType'] = "Lower";
                    SlipAddons::add($lower_slip_product_addons);
                }
            }
            /*Lower Addons End*/


            /*Opposite Impressions Start*/
            if (empty($request->iUpperCategoryId)) {
                $impression_data_array = $request->upper_impression_checkbox;
                if(!empty($impression_data_array)){
                    foreach ($impression_data_array as $key => $value) 
                    {   
                        $impression_data = array();
                        $impression_data['iSlipId']      = $iSlipId;
                        $impression_data['iImpressionId'] = $value;
                        $impression_data['iQuantity'] = $request->input("upper_impression_qty_".$value);
                        $impression_data['eType'] = "Opposite";
                        SlipImpression::add($impression_data); 
                    }
                }
            }
            /*Opposite Impressions End*/
            /*Opposite Extractions Start*/
            if (empty($request->iUpperCategoryId)) {
                $upper_opposite_extraction = array();
                $upper_opposite_extraction['vTeethInMouthOp']  = $request->upper_in_mouth_value;
                $upper_opposite_extraction['vMissingTeethOp']  = $request->upper_missing_teeth_value;
                $upper_opposite_extraction['vWillExtractOnDeliveryOp']= $request->upper_ectract_delivery_value;
                $upper_opposite_extraction['vHasBeenExtractedOp']= $request->upper_been_extracted_value;
                $upper_opposite_extraction['vFixOrAddOp']      = $request->upper_fix_value;
                $upper_opposite_extraction['vClaspsOp']        = $request->upper_clasps_value;

                $where                      = array();
                $where['iSlipProductId']    = $iSlipProductId;

                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $upper_opposite_extraction);


            }
            /*Opposite Extractions End*/

             /* Upper Dates Settings */
             if(isset($request->iLowerCategoryId) && $request->iLowerCategoryId !=null && isset($request->iLowerStageId) && $request->iLowerStageId !=null)   
             {
                $lower_without_rush = $this->product_dates($request->iLowerCategoryId,$request->iLowerStageId);
                $lower_date_update = array();
                $lower_date_update['dPickUpDate']   = $lower_without_rush['pic_date'];
                $lower_date_update['dDeliveryDate'] = $lower_without_rush['del_date'];
                $lower_date_update['dDeliveryDateFinal'] = $lower_without_rush['del_date'];
                $lower_date_update['tDeliveryTime'] = $lower_without_rush['delivery_time'];
                $where                      = array();
                $where['iSlipProductId']       = $iSlipProductId;
                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $lower_date_update);
             }
            
            
             if(!empty($request->lower_rush_feesid))
             {    
                 $fees_data = Fees::get_by_id($request->lower_rush_feesid);
                 $output = $this->product_dates($request->iLowerCategoryId,$request->iLowerStageId, $fees_data->iWorkingTime);
                 
                 $lower_rushdate_update = array();
                 $lower_rushdate_update['dDeliveryRushDate'] = $output['del_date'];
                 $lower_rushdate_update['dDeliveryDateFinal'] = $output['del_date'];
                 $lower_rushdate_update['eRushDate'] = "Yes";
                 $lower_rushdate_update['iFeesId'] = $request->lower_rush_feesid;
                    $where                      = array();
                    $where['iSlipProductId']       = $iSlipProductId;
                    $Slip_product_obj = new SlipProduct();
                    $Slip_product_obj->update($where, $lower_rushdate_update);
                    $lower_slip_rushdate_update['dLowerRushDate'] = $output['del_date'];
                    $where                  = array();
                    $where['iSlipId']       = $iSlipId;
                    $Slip_id_Update = new Slip();
                    $Slip_id_Update->update($where, $lower_slip_rushdate_update);
             }
             /* Upper Dates Settings */

        }
        /*Lower Product End*/

        // /*Upper Stage Notes End*/
        // dd($request,$slip_data['iCaseId']);
        if(!empty($request->tDescription)){

            $slip_notes = array();
            $slip_notes['iSlipId']      = $iSlipId;
            $slip_notes['iCaseId']      = $slip_data['iCaseId'];
            if(!empty($request->iUpperCategoryId) AND !empty($request->iLowerCategoryId) && (isset($request->iLowerStageId) && $request->iLowerStageId !='' && isset($request->iUpperStageId) && $request->iUpperStageId !='')){
                $slip_notes['iStageId']     = $request->iUpperStageId."/".$request->iLowerStageId;

                $upper_stage = ProductStage::get_by_id($request->iUpperStageId);
                $upper_stage_name = $upper_stage->vName;

                $lower_stage = ProductStage::get_by_id($request->iLowerStageId);
                $lower_stage_name = $lower_stage->vName;

                $slip_notes['vStageName']     =  $upper_stage_name."/". $lower_stage_name;

            }elseif(!empty($request->iUpperCategoryId) AND empty($request->iLowerCategoryId)){
                
                $slip_notes['iStageId']     = $request->iUpperStageId;

                $upper_stage = ProductStage::get_by_id($request->iUpperStageId);
                $upper_stage_name = $upper_stage->vName;
                $slip_notes['vStageName']     =  $upper_stage_name;

            }elseif((empty($request->iUpperCategoryId) AND !empty($request->iLowerCategoryId))){
                
                $slip_notes['iStageId']     = $request->iLowerStageId;

                $lower_stage = ProductStage::get_by_id($request->iLowerStageId);
                $lower_stage_name = $lower_stage->vName;

                $slip_notes['vStageName']     = $lower_stage_name;
            }
            $upper_dilevery_date =  $request->upper_delivery_date;
            $lower_dilevery_date =  $request->lower_delivery_date;

            if($upper_dilevery_date > $lower_dilevery_date){
                
                $slip_notes['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
            }

            if($upper_dilevery_date == $lower_dilevery_date){
                                
                    $slip_notes['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
            }

            if($upper_dilevery_date < $lower_dilevery_date){
                                
                $slip_notes['dDeliveryDate'] =  date("Y-m-d",strtotime($request->lower_delivery_date));
            }
            $slip_notes['vCaseNumber']  = $request->vCaseNumber;
            $slip_notes['vCreatedByName'] = $request->vCreatedByName;
            $slip_notes['tStagesNotes']  =  $request->tDescription;
            $slip_notes['vSlipNumber']   = $request->vSlipNumber;
            $slip_notes['dtAddedDate'] = date("Y-m-d H:i:s");
            $slip_notes['tAddedTime'] = date("h:i");
            $slip_notes['eNewSlipAdded'] = 'Yes';
            
            $slip_note = SlipNote::add($slip_notes);
           
        }
        // Update Case status start
        $where_slip_product = array(
            'iSlipId'=>$OldSlipData->iSlipId
        );
        if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
            $case_data['vLocation']     = "In office ready to pickup";
        }else{
            $case_data['vLocation']     = $request->vLocation;
        }
        $where_case_data['iCaseId'] =$Case_id;
        $Case_status_update = new Labcase();
        $Case_status_update->update($where_case_data, $case_data);
        // Update Case status end

        // Update CaseId & LabId start
        $vCaseNumber = 'C'."0".str_pad($Case_id,3,"0",STR_PAD_LEFT);
        $vSlipNumber = 'S'."0".str_pad($iSlipId,3,"0",STR_PAD_LEFT);
    
        $where_Slip['iSlipId']= $iSlipId;
        $data_Slip['vCaseNumber']= $vCaseNumber;
        $data_Slip['vSlipNumber']= $vSlipNumber;
        Slip::update_new($where_Slip,$data_Slip);
        // Update CaseId & LabId start
        
        if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == '6')
        {
            return redirect()->route('admin.labcase.office_admin_listing')->withSuccess('New stage successfully.');
        }
        else
        {
            return redirect()->route('admin.labcase')->withSuccess('New stage successfully.');
        }
    }

    public function next_previous_exist(Request $request)
    {
        $CuurentiCaseId = $request->iCaseId;
        $CuurentiSlipId = $request->iSlipId;
        $data['next_stage'] = Slip::get_next_stage($CuurentiSlipId,$CuurentiCaseId);
        $data['previous_stage'] = Slip::get_previous_stage($CuurentiSlipId,$CuurentiCaseId);
     
        $middel_stage = Slip::get_middel_stage($data,$CuurentiCaseId );

        $middel_stage_array = json_decode(json_encode($middel_stage), true);
        $iSlipIds = array();

        foreach($middel_stage_array as $key=>$middel_stage_array_val)
        {
            if(in_array($middel_stage_array_val['iSlipId'],$iSlipIds))
            {
                $get_key = $this->searchForId($middel_stage_array_val['iSlipId'],$middel_stage_array);
                $middel_stage_array[$get_key]['vProductStageName']= $middel_stage_array[$get_key]['vProductStageName'].'/'.$middel_stage_array[$key]['vProductStageName'];
                unset($middel_stage_array[$key]);
            }
            array_push($iSlipIds,$middel_stage_array_val['iSlipId']);
        }
        $data['middel_stage'] = $middel_stage_array;
        $data['CuurentiSlipId'] = $CuurentiSlipId;
        return view('admin.labcase.next_previous_slip')->with($data);
  
    }
    
    public function searchForId($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['iSlipId'] === $id) {
                return $key;
            }
        }
        return null;
    }
    public function edit_stage($iCaseId,$iSlipId)
    {
        $iCustomerId = General::admin_info()['iCustomerId'];

        $labOffice = LabOffice::get_active_lab_by_officeId($iCustomerId);

        $labs = array();

        foreach ($labOffice as $key => $value) {
            $labs[] = Customer::get_by_id($value->iLabId);
        }
        $data['labs'] = $labs;

        $data['office'] = Office::get_office_by_customerid();
        $data['doctor'] = Doctor::get_all_doctor();
        // $data["Office"] = Office::get_office_by_customerid();
        // $data['Upper_category'] = Category::get_category_by_type("Upper");
        // $data['Lower_category'] = Category::get_category_by_type("Lower");
        $data['toothshade'] = ToothShades::get_all_toothshades();
        // $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        // $data['toothbrand'] = ToothBrand::get_all_brand();
        // $data['impression'] = Impression::get_all_impressions();
        // dd($data['impression']);

        // $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        // $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

        $schedule = Schedule::get_all_schedule();
        foreach($schedule as $value){
            if (!empty($value->dtHolidays) AND empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                $holiday_arr = explode("-",$value->dtHolidays);
                $holidays[] = $holiday_arr[1]."/".$holiday_arr[2]."/".$holiday_arr[0];
            }
        }
        if(isset($holidays) && !empty($holidays)){
            $data['holidays'] = implode(",",$holidays);
        }

        $admin = Session::get('data');
        // dd($admin);
        if(empty($admin['user_id']))
        {
            Redirect::to('/admin/login')->send();
        }else{
            // $iAdminId = $admin['user_id'];
            // $admin    = Admin::get_by_id($iAdminId);
           if(isset($admin['vFirstName']) && !empty($admin['vFirstName']) && isset($admin['vLastName']) && !empty($admin['vLastName']))
              {
                $data['user_name'] = $admin['vFirstName'].' '.$admin['vLastName'];
              }
              else
              {
                $data['user_name'] = $admin['vName'];
              }
                           
            // $data['AdminId'] = $iAdminId;
            $data['AdminId'] = $admin['user_id'];
        }

        $caseid = Labcase::get_office_id();
        if(!empty($caseid->iCaseId)) {
            $id = $caseid->iCaseId+1;
        }else{           
            $id = 1;
        }
        $data['case_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $slipid = Labcase::get_slip_id();
        if(!empty($slipid->iSlipId)) {
            $id = $slipid->iSlipId+1;
        }else{           
            $id = 1;
        }
        $data['slip_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $data['tampid'] =  str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);

        $data['Case'] = Labcase::get_by_id($iCaseId);
        $data['Slip'] = Slip::get_by_id($iSlipId);

        // For Security purpose start
        $iCustomerId = General::admin_info()['iCustomerId'];
        $customerType =General::admin_info()['customerType'];
        if(isset($customerType) && $customerType =='Office Admin')
        {
            if(isset($data['Slip']->iOfficeId) && !empty($data['Slip']->iOfficeId))
            {
                $MatchId = $data['Slip']->iOfficeId;
            }
            else
            {
                $MatchId = '';
            }
        }
        if(isset($customerType) && $customerType =='Lab Admin')
        {
            if(isset($data['Slip']->iLabId) && !empty($data['Slip']->iLabId))
            {
                $MatchId = $data['Slip']->iLabId;
            }
            else
            {
                $MatchId = '';
            }
        }
        if((isset($MatchId) && $MatchId != $iCustomerId && !empty($iCustomerId)) || (empty($MatchId)) || (empty($data['Case'])))
        {
            if(isset($customerType) && $customerType =='Office Admin')
            {
                return redirect()->route('admin.labcase.office_admin_listing')->withError('Invalid Input');
            }
            elseif(isset($customerType) && $customerType =='Lab Admin')
            {
                return redirect()->route('admin.labcase')->withError('Invalid Input');
            }
        }
        // For Security purpose end

        $data['upper_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Upper");
        $data['lower_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Lower");

        $data["rush"] = "No";

        $iLabId = $data['Slip']->iLabId;
        $data["Upper_category"] =   Category::get_category_by_type_customer('Upper',$iLabId);
        $data["Lower_category"] =   Category::get_category_by_type_customer('Lower',$iLabId);

        $data['toothbrand'] = ToothBrand::get_all_brand_by_customerid($iLabId);

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type('Upper',$iLabId);
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type('Lower',$iLabId);

        $criteria_new = array();
        $criteria_new['iLabId'] = $iLabId;
        $data["gumshade"] =   GumShades::get_product_by_gumbrand($criteria_new);

        $data["impression"] = Impression::get_all_impressions_by_customer($iLabId);


        if (!empty($data['upper_product'])) {

            $data['upper_vTeethInMouth'] = explode(",",$data['upper_product']->vTeethInMouth);
            $data['upper_vMissingTeeth'] = explode(",",$data['upper_product']->vMissingTeeth);
            $data['upper_vWillExtractOnDelivery'] = explode(",",$data['upper_product']->vWillExtractOnDelivery);
            $data['upper_vHasBeenExtracted'] = explode(",",$data['upper_product']->vHasBeenExtracted);
            $data['upper_vFixOrAdd'] = explode(",",$data['upper_product']->vFixOrAdd);
            $data['upper_vClasps'] = explode(",",$data['upper_product']->vClasps);

            $upper_teeth_in_mouth = array();
            $upper_extract= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

            $upper_merged_array = array_merge(
                $data['upper_vMissingTeeth'],
                $data['upper_vWillExtractOnDelivery'],
                $data['upper_vHasBeenExtracted'],
                $data['upper_vFixOrAdd'],
                $data['upper_vClasps']
            );

            foreach ($upper_extract as $value) {
                if (!in_array($value,$upper_merged_array)) {
                    $upper_teeth_in_mouth[] = $value;
                }
            }
            
            $data['upper_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['upper_vTeethInMouth'],
                    $upper_teeth_in_mouth
                )
            );

            $data['lower_vTeethInMouthOp'] = explode(",",$data['upper_product']->vTeethInMouthOp);
            $data['lower_vMissingTeethOp'] = explode(",",$data['upper_product']->vMissingTeethOp);
            $data['lower_vWillExtractOnDeliveryOp'] = explode(",",$data['upper_product']->vWillExtractOnDeliveryOp);
            $data['lower_vHasBeenExtractedOp'] = explode(",",$data['upper_product']->vHasBeenExtractedOp);
            $data['lower_vFixOrAddOp'] = explode(",",$data['upper_product']->vFixOrAddOp);
            $data['lower_vClaspsOp'] = explode(",",$data['upper_product']->vClaspsOp);

             if (!empty($data['upper_product']->lower_vTeethInMouthOp) AND !empty($data['upper_product']->lower_vMissingTeethOp) AND !empty($data['upper_product']->lower_vWillExtractOnDeliveryOp) AND !empty($data['upper_product']->lower_vHasBeenExtractedOp) AND !empty($data['upper_product']->lower_vFixOrAddOp) AND !empty($data['upper_product']->lower_vClaspsOp)) {

                $lower_teeth_in_mouthOp = array();
                $lower_extractOp= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");

                $lower_merged_arrayOp = array_merge(
                    $data['lower_vMissingTeethOp'],
                    $data['lower_vWillExtractOnDeliveryOp'],
                    $data['lower_vHasBeenExtractedOp'],
                    $data['lower_vFixOrAddOp'],
                    $data['lower_vClaspsOp']
                );

                foreach ($lower_extractOp as $value) {
                    if (!in_array($value,$lower_merged_arrayOp)) {
                        $lower_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['lower_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['lower_vTeethInMouthOp'],
                        $lower_teeth_in_mouthOp
                    )
                );
            }
            // dd($data['lower_vTeethInMouthOp']);

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['lower_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['lower_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['lower_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $upper_delivery_date = $data['upper_product']->dDeliveryDate;
            if ($data['upper_product']->eRushDate == "Yes") {
                $data["rush"] = "Yes";
            }
        }

        if (!empty($data['lower_product'])) {
            $data['lower_vTeethInMouth'] = explode(",",$data['lower_product']->vTeethInMouth);
            $data['lower_vMissingTeeth'] = explode(",",$data['lower_product']->vMissingTeeth);
            $data['lower_vWillExtractOnDelivery'] = explode(",",$data['lower_product']->vWillExtractOnDelivery);
            $data['lower_vHasBeenExtracted'] = explode(",",$data['lower_product']->vHasBeenExtracted);
            $data['lower_vFixOrAdd'] = explode(",",$data['lower_product']->vFixOrAdd);
            $data['lower_vClasps'] = explode(",",$data['lower_product']->vClasps);

            
            $teeth_in_mouth = array();
            $lower_extract= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");
            
            $merged_array = array_merge(
                $data['lower_vMissingTeeth'],
                $data['lower_vWillExtractOnDelivery'],
                $data['lower_vHasBeenExtracted'],
                $data['lower_vFixOrAdd'],
                $data['lower_vClasps']
            );
            
            foreach ($lower_extract as $value) {
                if (!in_array($value,$merged_array)) {
                    $teeth_in_mouth[] = $value;
                }
            }

            $data['lower_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['lower_vTeethInMouth'],
                    $teeth_in_mouth
                )
            );


            $data['upper_vTeethInMouthOp'] = explode(",",$data['lower_product']->vTeethInMouthOp);
            $data['upper_vMissingTeethOp'] = explode(",",$data['lower_product']->vMissingTeethOp);
            $data['upper_vWillExtractOnDeliveryOp'] = explode(",",$data['lower_product']->vWillExtractOnDeliveryOp);
            $data['upper_vHasBeenExtractedOp'] = explode(",",$data['lower_product']->vHasBeenExtractedOp);
            $data['upper_vFixOrAddOp'] = explode(",",$data['lower_product']->vFixOrAddOp);
            $data['upper_vClaspsOp'] = explode(",",$data['lower_product']->vClaspsOp);

            if (!empty($data['lower_product']->upper_vTeethInMouthOp) AND !empty($data['lower_product']->upper_vMissingTeethOp) AND !empty($data['lower_product']->upper_vWillExtractOnDeliveryOp) AND !empty($data['lower_product']->upper_vHasBeenExtractedOp) AND !empty($data['lower_product']->upper_vFixOrAddOp) AND !empty($data['lower_product']->upper_vClaspsOp)) {

                $upper_teeth_in_mouthOp = array();
                $upper_extractOp= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

                $upper_merged_arrayOp = array_merge(
                    $data['upper_vMissingTeethOp'],
                    $data['upper_vWillExtractOnDeliveryOp'],
                    $data['upper_vHasBeenExtractedOp'],
                    $data['upper_vFixOrAddOp'],
                    $data['upper_vClaspsOp']
                );

                foreach ($upper_extractOp as $value) {
                    if (!in_array($value,$upper_merged_arrayOp)) {
                        $upper_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['upper_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['upper_vTeethInMouthOp'],
                        $upper_teeth_in_mouthOp
                    )
                );
            }


            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['upper_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['upper_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['upper_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $lower_delivery_date = $data['lower_product']->dDeliveryDate;
            if ($data['lower_product']->eRushDate == "Yes") {
                $data["rush"] = "Yes";
            }
        }
        
        if ($data["rush"] == "No") {
            if (!empty($upper_delivery_date) AND empty($lower_delivery_date)) {
                $data['final_delivery_date'] = $upper_delivery_date;
                $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
            }else if(!empty($lower_dilevery_date) AND empty($upper_delivery_date)){
                $data['final_delivery_date'] = $lower_delivery_date;
                $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
            }else if (!empty($upper_delivery_date) && !empty($lower_delivery_date)) {
                // dd($lower_delivery_date);
                if ($upper_delivery_date > $lower_delivery_date) {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                } else if ($upper_delivery_date < $lower_delivery_date) {
                    $data['final_delivery_date'] = $lower_delivery_date;
                    $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
                } else if ($upper_delivery_date == $lower_delivery_date) {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                }
            }
        }else{
            
        }
        $data["Office"] = Office::get_office_by_customerid();
        $data['Upper_category'] = Category::get_category_by_type("Upper");
        $data['Lower_category'] = Category::get_category_by_type("Lower");
        $data['toothshade'] = ToothShades::get_all_toothshades();
        $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        $data['toothbrand'] = ToothBrand::get_all_brand();
        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");
        // dd($data);
        return view('admin.labcase.edit_stage')->with($data);
    }
    public function edit_stage_lab($iCaseId,$iSlipId)
    {
        $iCustomerId = General::admin_info()['iCustomerId'];
        $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);
        $office = array();
        foreach ($labOffice as $key => $value) {
            $office[] = Customer::get_by_id($value->iOfficeId);
        }
        $data['office'] = $office;
        $iOfficeId = $labOffice[0]->iOfficeId;
        $data["doctor"] =   Doctor::get_doc_by_office($iOfficeId);
        $data['iLoggedinId']=$iCustomerId;
        // $data["Office"] = Office::get_office_by_customerid();
        // $data['Upper_category'] = Category::get_category_by_type("Upper");
        // $data['Lower_category'] = Category::get_category_by_type("Lower");
        $data['toothshade'] = ToothShades::get_all_toothshades();
        // $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        // $data['toothbrand'] = ToothBrand::get_all_brand();
        // $data['impression'] = Impression::get_all_impressions();
        // dd($data['impression']);

        // $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        // $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");

        $schedule = Schedule::get_all_schedule();
        foreach($schedule as $value){
            if (!empty($value->dtHolidays) AND empty($value->eWeek) AND $value->eStatus == "CLOSED") {
                $holiday_arr = explode("-",$value->dtHolidays);
                $holidays[] = $holiday_arr[1]."/".$holiday_arr[2]."/".$holiday_arr[0];
            }
        }
        if(isset($holidays) && !empty($holidays)){
            $data['holidays'] = implode(",",$holidays);
        }

        $admin = Session::get('data');
        // dd($admin);
        if(empty($admin['user_id']))
        {
            Redirect::to('/admin/login')->send();
        }else{
            // $iAdminId = $admin['user_id'];
            // $admin    = Admin::get_by_id($iAdminId);
           if(isset($admin['vFirstName']) && !empty($admin['vFirstName']) && isset($admin['vLastName']) && !empty($admin['vLastName']))
              {
                $data['user_name'] = $admin['vFirstName'].' '.$admin['vLastName'];
              }
              else
              {
                $data['user_name'] = $admin['vName'];
              }
                
            // $data['AdminId'] = $iAdminId;
            $data['AdminId'] = $admin['user_id'];
        }

        $caseid = Labcase::get_office_id();
        if(!empty($caseid->iCaseId)) {
            $id = $caseid->iCaseId+1;
        }else{           
            $id = 1;
        }
        $data['case_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $slipid = Labcase::get_slip_id();
        if(!empty($slipid->iSlipId)) {
            $id = $slipid->iSlipId+1;
        }else{           
            $id = 1;
        }
        $data['slip_id'] = "0".str_pad($id,3,"0",STR_PAD_LEFT);

        $data['tampid'] =  str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);

        $data['Case'] = Labcase::get_by_id($iCaseId);
        $data['Slip'] = Slip::get_by_id($iSlipId);

        // For Security purpose start
        $iCustomerId = General::admin_info()['iCustomerId'];
        $customerType =General::admin_info()['customerType'];
        if(isset($customerType) && $customerType =='Office Admin')
        {
            if(isset($data['Slip']->iOfficeId) && !empty($data['Slip']->iOfficeId))
            {
                $MatchId = $data['Slip']->iOfficeId;
            }
            else
            {
                $MatchId = '';
            }
        }
        if(isset($customerType) && $customerType =='Lab Admin')
        {
            if(isset($data['Slip']->iLabId) && !empty($data['Slip']->iLabId))
            {
                $MatchId = $data['Slip']->iLabId;
            }
            else
            {
                $MatchId = '';
            }
        }
        if((isset($MatchId) && $MatchId != $iCustomerId && !empty($iCustomerId)) || (empty($MatchId)) || (empty($data['Case'])))
        {
            if(isset($customerType) && $customerType =='Office Admin')
            {
                return redirect()->route('admin.labcase.office_admin_listing')->withError('Invalid Input');
            }
            elseif(isset($customerType) && $customerType =='Lab Admin')
            {
                return redirect()->route('admin.labcase')->withError('Invalid Input');
            }
        }
        // For Security purpose end

        $data['upper_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Upper");
        $data['lower_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Lower");

        $data["rush"] = "No";

        $iLabId = $data['Slip']->iLabId;
        $data["Upper_category"] =   Category::get_category_by_type_customer('Upper',$iLabId);
        $data["Lower_category"] =   Category::get_category_by_type_customer('Lower',$iLabId);

        $data['toothbrand'] = ToothBrand::get_all_brand_by_customerid($iLabId);

        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type('Upper',$iLabId);
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type('Lower',$iLabId);


        $criteria_new = array();
        $criteria_new['iLabId'] = $iLabId;
        $data["gumshade"] =   GumShades::get_product_by_gumbrand($criteria_new);

        $data["impression"] = Impression::get_all_impressions_by_customer($iLabId);


        if (!empty($data['upper_product'])) {

            $data['upper_vTeethInMouth'] = explode(",",$data['upper_product']->vTeethInMouth);
            $data['upper_vMissingTeeth'] = explode(",",$data['upper_product']->vMissingTeeth);
            $data['upper_vWillExtractOnDelivery'] = explode(",",$data['upper_product']->vWillExtractOnDelivery);
            $data['upper_vHasBeenExtracted'] = explode(",",$data['upper_product']->vHasBeenExtracted);
            $data['upper_vFixOrAdd'] = explode(",",$data['upper_product']->vFixOrAdd);
            $data['upper_vClasps'] = explode(",",$data['upper_product']->vClasps);

            $upper_teeth_in_mouth = array();
            $upper_extract= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

            $upper_merged_array = array_merge(
                $data['upper_vMissingTeeth'],
                $data['upper_vWillExtractOnDelivery'],
                $data['upper_vHasBeenExtracted'],
                $data['upper_vFixOrAdd'],
                $data['upper_vClasps']
            );

            foreach ($upper_extract as $value) {
                if (!in_array($value,$upper_merged_array)) {
                    $upper_teeth_in_mouth[] = $value;
                }
            }
            
            $data['upper_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['upper_vTeethInMouth'],
                    $upper_teeth_in_mouth
                )
            );

            $data['lower_vTeethInMouthOp'] = explode(",",$data['upper_product']->vTeethInMouthOp);
            $data['lower_vMissingTeethOp'] = explode(",",$data['upper_product']->vMissingTeethOp);
            $data['lower_vWillExtractOnDeliveryOp'] = explode(",",$data['upper_product']->vWillExtractOnDeliveryOp);
            $data['lower_vHasBeenExtractedOp'] = explode(",",$data['upper_product']->vHasBeenExtractedOp);
            $data['lower_vFixOrAddOp'] = explode(",",$data['upper_product']->vFixOrAddOp);
            $data['lower_vClaspsOp'] = explode(",",$data['upper_product']->vClaspsOp);

            if (!empty($data['upper_product']->lower_vTeethInMouthOp) AND !empty($data['upper_product']->lower_vMissingTeethOp) AND !empty($data['upper_product']->lower_vWillExtractOnDeliveryOp) AND !empty($data['upper_product']->lower_vHasBeenExtractedOp) AND !empty($data['upper_product']->lower_vFixOrAddOp) AND !empty($data['upper_product']->lower_vClaspsOp)) {

                $lower_teeth_in_mouthOp = array();
                $lower_extractOp= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");

                $lower_merged_arrayOp = array_merge(
                    $data['lower_vMissingTeethOp'],
                    $data['lower_vWillExtractOnDeliveryOp'],
                    $data['lower_vHasBeenExtractedOp'],
                    $data['lower_vFixOrAddOp'],
                    $data['lower_vClaspsOp']
                );

                foreach ($lower_extractOp as $value) {
                    if (!in_array($value,$lower_merged_arrayOp)) {
                        $lower_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['lower_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['lower_vTeethInMouthOp'],
                        $lower_teeth_in_mouthOp
                    )
                );
            }
            // dd($data['lower_vTeethInMouthOp']);

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['lower_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['lower_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['lower_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $upper_delivery_date = $data['upper_product']->dDeliveryDate;
            if ($data['upper_product']->eRushDate == "Yes") {
                $data["rush"] = "Yes";
            }
        }

        if (!empty($data['lower_product'])) {
            $data['lower_vTeethInMouth'] = explode(",",$data['lower_product']->vTeethInMouth);
            $data['lower_vMissingTeeth'] = explode(",",$data['lower_product']->vMissingTeeth);
            $data['lower_vWillExtractOnDelivery'] = explode(",",$data['lower_product']->vWillExtractOnDelivery);
            $data['lower_vHasBeenExtracted'] = explode(",",$data['lower_product']->vHasBeenExtracted);
            $data['lower_vFixOrAdd'] = explode(",",$data['lower_product']->vFixOrAdd);
            $data['lower_vClasps'] = explode(",",$data['lower_product']->vClasps);

            
            $teeth_in_mouth = array();
            $lower_extract= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");
            
            $merged_array = array_merge(
                $data['lower_vMissingTeeth'],
                $data['lower_vWillExtractOnDelivery'],
                $data['lower_vHasBeenExtracted'],
                $data['lower_vFixOrAdd'],
                $data['lower_vClasps']
            );
            
            foreach ($lower_extract as $value) {
                if (!in_array($value,$merged_array)) {
                    $teeth_in_mouth[] = $value;
                }
            }

            $data['lower_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['lower_vTeethInMouth'],
                    $teeth_in_mouth
                )
            );


            $data['upper_vTeethInMouthOp'] = explode(",",$data['lower_product']->vTeethInMouthOp);
            $data['upper_vMissingTeethOp'] = explode(",",$data['lower_product']->vMissingTeethOp);
            $data['upper_vWillExtractOnDeliveryOp'] = explode(",",$data['lower_product']->vWillExtractOnDeliveryOp);
            $data['upper_vHasBeenExtractedOp'] = explode(",",$data['lower_product']->vHasBeenExtractedOp);
            $data['upper_vFixOrAddOp'] = explode(",",$data['lower_product']->vFixOrAddOp);
            $data['upper_vClaspsOp'] = explode(",",$data['lower_product']->vClaspsOp);

            if (!empty($data['lower_product']->upper_vTeethInMouthOp) AND !empty($data['lower_product']->upper_vMissingTeethOp) AND !empty($data['lower_product']->upper_vWillExtractOnDeliveryOp) AND !empty($data['lower_product']->upper_vHasBeenExtractedOp) AND !empty($data['lower_product']->upper_vFixOrAddOp) AND !empty($data['lower_product']->upper_vClaspsOp)) {

                $upper_teeth_in_mouthOp = array();
                $upper_extractOp= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

                $upper_merged_arrayOp = array_merge(
                    $data['upper_vMissingTeethOp'],
                    $data['upper_vWillExtractOnDeliveryOp'],
                    $data['upper_vHasBeenExtractedOp'],
                    $data['upper_vFixOrAddOp'],
                    $data['upper_vClaspsOp']
                );

                foreach ($upper_extractOp as $value) {
                    if (!in_array($value,$upper_merged_arrayOp)) {
                        $upper_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['upper_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['upper_vTeethInMouthOp'],
                        $upper_teeth_in_mouthOp
                    )
                );
            }


            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['upper_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['upper_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['upper_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $lower_delivery_date = $data['lower_product']->dDeliveryDate;
            if ($data['lower_product']->eRushDate == "Yes") {
                $data["rush"] = "Yes";
            }
        }
        
        if ($data["rush"] == "No") {
            if (!empty($upper_delivery_date) AND empty($lower_delivery_date)) {
                $data['final_delivery_date'] = $upper_delivery_date;
                $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
            }else if(!empty($lower_dilevery_date) AND empty($upper_delivery_date)){
                $data['final_delivery_date'] = $lower_delivery_date;
                $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
            }else if (!empty($upper_delivery_date) && !empty($lower_delivery_date)) {
                // dd($lower_delivery_date);
                if ($upper_delivery_date > $lower_delivery_date) {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                } else if ($upper_delivery_date < $lower_delivery_date) {
                    $data['final_delivery_date'] = $lower_delivery_date;
                    $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
                } else if ($upper_delivery_date == $lower_delivery_date) {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                }
            }
        }else{
            
        }
        $data["Office"] = Office::get_office_by_customerid();
        $data['Upper_category'] = Category::get_category_by_type("Upper");
        $data['Lower_category'] = Category::get_category_by_type("Lower");
        $data['toothshade'] = ToothShades::get_all_toothshades();
        $data['gumshade'] = GumShades::get_all_gumshades();
        $data['gumbrand'] = GumBrand::get_all_brand();
        $data['toothbrand'] = ToothBrand::get_all_brand();
        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type("Upper");
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type("Lower");
        $schedule = Schedule::get_all_schedule();
        $get_holiday = Schedule::get_holiday($iCustomerId);
        $data['holidays'] = "";
        $holidays = array();
        foreach($get_holiday as $value){
            if (!empty($value->dtHolidays) && $value->dtHolidays!=null) {
                $holiday_arr = explode("-",$value->dtHolidays);
                $holidays[] = $holiday_arr[1]."/".$holiday_arr[2]."/".$holiday_arr[0];
            }
        }
        if(!empty($holidays)){
            $data['holidays'] = implode(",",$holidays);
        }
        $weekEnd = array();
        foreach($get_holiday as $key=> $value){
            if (empty($value->dtHolidays)) {
                // $weekend_arr = explode("-",$value->dtHolidays);
                if(isset($value->eWeek) && $value->eWeek =='Sunday')
                {
                    $WeekNumber = 0;
                }
                if(isset($value->eWeek) && $value->eWeek =='Monday')
                {
                    $WeekNumber = 1;
                }
                if(isset($value->eWeek) && $value->eWeek =='Tuesday')
                {
                    $WeekNumber = 2;
                }
                if(isset($value->eWeek) && $value->eWeek =='Wednesday')
                {
                    $WeekNumber = 3;
                }
                if(isset($value->eWeek) && $value->eWeek =='Thursday')
                {
                    $WeekNumber = 4;
                }
                if(isset($value->eWeek) && $value->eWeek =='Friday')
                {
                    $WeekNumber = 5;
                }
                if(isset($value->eWeek) && $value->eWeek =='Saturday')
                {
                    $WeekNumber = 6;
                }
                $weekEnd[] = $WeekNumber;
            }
        }
        if(!empty($weekEnd) && $weekEnd !=null){
            $data['disablesDays'] = implode(",",$weekEnd);
        }
        // dd($data['disablesDays']);
        return view('admin.labcase.edit_stage_lab')->with($data);
    }

    public function UpdateStageData(Request $request)
    {
        $Case_id = $request->iCaseId;

        if(isset($request->iLabId) && !empty($request->iLabId))
        {
            $iLabId = $request->iLabId;
        }
        else
        {
            $iLabId = General::admin_info()['iCustomerId'];
        }
        if(isset($request->iOfficeId) && !empty($request->iOfficeId))
        {
            $iOfficeId = $request->iOfficeId;
        }
        else
        {
            $iOfficeId = General::admin_info()['iCustomerId'];
        }

        $iSlipId                    = $request->iSlipId;
        $slip_data['iCaseId']       = $request->iCaseId;
        // $slip_data['vSlipNumber']   = $request->vSlipNumber;
        $slip_data['iOfficeId']     = $iOfficeId;
        $slip_data['iLabId']        = $iLabId;
        $slip_data['iDoctorId']     = $request->iDoctorId;
        $slip_data['vPatientName']  = $request->vPatientName;
        $slip_data['vCasePanNumber']= $request->vCasePanNumber;
        // $slip_data['vCaseNumber']   = $request->vCaseNumber;
        $slip_data['iCreatedById']  = $request->iCreatedById;
        $slip_data['vCreatedByName']= $request->vCreatedByName;
        $slip_data['eCreatedByType']= $request->eCreatedByType;
        
        // if (!empty($request->information_checkbox) && $request->information_checkbox == "Yes") {
        if(isset($request->eStatus) && !empty($request->eStatus))
        {
            $slip_data['eStatus']       = $request->eStatus;
        }
        else
        {
            $slip_data['eStatus']       = "On Process";
        }
        
        $slip_data['vLocation'] =  $request->vLocation;
        $slip_data['tStagesNotes']  =  $request->tDescription;

        
        $upper_dilevery_date =  $request->upper_delivery_date;
        $lower_dilevery_date =  $request->lower_delivery_date;
        if($upper_dilevery_date > $lower_dilevery_date){
            
            $slip_data['dPickUpDate']   =  date("Y-m-d ",strtotime($request->upper_pickup_date));
            $slip_data['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
            $slip_data['tDeliveryTime'] =  date("h:i",strtotime($request->upper_delivery_time));

            $iCategoryId = $request->iUpperCategoryId;
            $data =  array();
            $where = array();

            if(isset($iCategoryId) && $iCategoryId !='')
            {
                $category_info = Category::get_by_id($iCategoryId);
                $slip_data['iCasepanId']    = $category_info->iCasepanId;
                $data['iCasepanId'] = $slip_data['iCasepanId'];
                $data['vLocation'] = isset($request->vLocation)?$request->vLocation:'';
                $data['eStatus'] = isset($request->eStatus)?$request->eStatus:'';
                $data['vPatientName'] = isset($request->vPatientName)?$request->vPatientName:'';
                $where['iCaseId']   = $Case_id;
              
                $Case_id_new = new Labcase();
                $Case_id_new->update($where, $data);
                
                // Check stage id exist or not start
                $existUpperStage = 0;
                if(isset($request->iUpperStageId) && !empty($request->iUpperStageId))
                {
                    $iUpperStageId =  $request->iUpperStageId;
                    $existUpperStage = SlipProduct::get_by_iStageId($iUpperStageId);
                }    
                // Check stage id exist or not end
                if(count($existUpperStage) ==0) 
                {
                    $Casepan = Casepan::get_by_id($data['iCasepanId']);
                    $Casepannumber = CasepanNumber::get_by_casepan_use($Casepan->iCasepanId);
                    
                    if (!empty($Casepannumber)) {
                        $data =  array();
                        $where = array();
    
                        $data['vCasePanNumber'] = $Casepannumber->vNumber;
                        $where['iCaseId']       = $Case_id;
                        
                        $Case_update_data = new Labcase();
                        $Case_update_data->update($where, $data);
    
                        $data =  array();
                        $where = array();
    
                        $data['eUse']                =  "Yes";
                        $where['iCasepanNumberId']   =  $Casepannumber->iCasepanNumberId;
                        
                        $iCasepanNumberId = new CasepanNumber();
                        $iCasepanNumberId->update($where, $data);
                    }
                }
            }
            else
            {
                $data['vLocation'] = isset($request->vLocation)?$request->vLocation:'';
                $data['eStatus'] = isset($request->eStatus)?$request->eStatus:'';
                $where['iCaseId']   = $Case_id;
              
                $Case_id_new = new Labcase();
                $Case_id_new->update($where, $data);
            }

                    
        }
    
        if($upper_dilevery_date == $lower_dilevery_date){
            // dd($upper_dilevery_date,$lower_dilevery_date);
            $slip_data['dPickUpDate']   =  date("Y-m-d ",strtotime($request->upper_pickup_date));
            $slip_data['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
            $slip_data['tDeliveryTime'] =  date("h:i",strtotime($request->upper_delivery_time));

            $iCategoryId = $request->iUpperCategoryId;
            if(isset($iCategoryId) && !empty($iCategoryId))
            {
                $category_info = Category::get_by_id($iCategoryId);

                $slip_data['iCasepanId']    = $category_info->iCasepanId;
    
                    $data =  array();
                    $where = array();
    
                    $data['vLocation'] = isset($request->vLocation)?$request->vLocation:'';
                    $data['eStatus'] = isset($request->eStatus)?$request->eStatus:'';
                    $data['vPatientName'] = isset($request->vPatientName)?$request->vPatientName:'';
                    $data['iCasepanId'] = $slip_data['iCasepanId'];
                    $where['iCaseId']   = $Case_id;
                    
                    $Case_id_new = new Labcase();
                    $Case_id_new->update($where, $data);
                    // Check stage id exist or not start
                    $existLowerStage = 0;
                    if(isset($request->iLowerStageId) && !empty($request->iLowerStageId))
                    {
                        $iLowerStageId =  $request->iLowerStageId;
                        $existLowerStage = SlipProduct::get_by_iStageId($iLowerStageId);
                    }
                    // Check stage id exist or not end
                    if(count($existLowerStage) ==0) 
                    {
                        $Casepan = Casepan::get_by_id($data['iCasepanId']);
                        $Casepannumber = CasepanNumber::get_by_casepan_use($Casepan->iCasepanId);
                        
                        if (!empty($Casepannumber)) {
                            $data =  array();
                            $where = array();

                            $data['vCasePanNumber'] = $Casepannumber->vNumber;
                            $where['iCaseId']       = $Case_id;
                            
                            $Case_id_new = new Labcase();
                            $Case_id_new->update($where, $data);

                            $data =  array();
                            $where = array();

                            $data['eUse']                =  "Yes";
                            $where['iCasepanNumberId']   =  $Casepannumber->iCasepanNumberId;
                            
                            $iCasepanNumberId = new CasepanNumber();
                            $iCasepanNumberId->update($where, $data);
                        }
                    }    
            }
           
        }
        if($upper_dilevery_date < $lower_dilevery_date){
            
            $slip_data['dPickUpDate']   =  date("Y-m-d ",strtotime($request->lower_pickup_date));
            $slip_data['dDeliveryDate'] =  date("Y-m-d",strtotime($request->lower_delivery_date));
            $slip_data['tDeliveryTime'] =  date("h:i",strtotime($request->lower_delivery_time));
            
            $iCategoryId = $request->iLowerCategoryId;
            if(isset($iCategoryId) && !empty($iCategoryId))
            {
                $category_info = Category::get_by_id($iCategoryId);
                
                $slip_data['iCasepanId']    = $category_info->iCasepanId;
    
                $data =  array();
                $where = array();
                $data['vLocation'] = isset($request->vLocation)?$request->vLocation:'';
                $data['eStatus'] = isset($request->eStatus)?$request->eStatus:'';
                $data['vPatientName'] = isset($request->vPatientName)?$request->vPatientName:'';
                $data['iCasepanId'] = $slip_data['iCasepanId'];
                $where['iCaseId']   = $Case_id;
                    
                $Case_id_new = new Labcase();
                $Case_id_new->update($where, $data);
    
                $Casepan = Casepan::get_by_id($data['iCasepanId']);
                $Casepannumber = CasepanNumber::get_by_casepan_use($Casepan->iCasepanId);
    
                if (!empty($Casepannumber)) {
                    $data =  array();
                    $where = array();
    
                    $data['vCasePanNumber'] = $Casepannumber->vNumber;
                    $where['iCaseId']       = $Case_id;
                    
                    $Case_id_new = new Labcase();
                    $Case_id_new->update($where, $data);
    
                    $data =  array();
                    $where = array();
    
                    $data['eUse']                =  "Yes";
                    $where['iCasepanNumberId']   =  $Casepannumber->iCasepanNumberId;
                    
                    $iCasepanNumberId = new CasepanNumber();
                    $iCasepanNumberId->update($where, $data);
                }
            }
            
        }
        
        if($iSlipId){
            $slip_data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $slip_data['dtAddedDate'] = date("Y-m-d H:i:s");
            $slip_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        $where                      = array();
        $where['iSlipId']       = $iSlipId;
        // dd($slip_data);

        $Slip_id_update = new Slip();
        $Slip_id_update->update($where, $slip_data);
        // dd($slip_data['dDeliveryDate']);
        $data =  array();
        $where = array();

        $data['vLocation']     = $slip_data['vLocation'];
        $data['eStatus']     = $slip_data['eStatus'];
        $where['iCaseId']      = $Case_id;
        
        $Case_id_update = new Labcase();
        $Case_id_update->update($where, $data);


        /* Attachment Upload*/
        $iTampId = $request->iTampId;
        $criteria 				 = array();
        $criteria['iTampId'] 	 = $iTampId;
        
        $gallary = SlipGallery::get_all_data($criteria);
        
        if(!empty($gallary))
        {     
            $data = array();           
            foreach ($gallary as $key => $value) 
            {
                if($value->iTampId == $iTampId)
                {
                    $data['iSlipId']     = $request->iSlipId;
                    $data['iTampId'] 	 = null;
                    $where                      = array();
                    $where['iSlipAttachmentId']= $value->iSlipAttachmentId;
                    
                    $SlipGallery_id = new SlipGallery();
                    $SlipGallery_id->update($where, $data);
                }
            }
        }


        /* Attachment Upload*/
        /* Slip Upper Product Update*/
        if (!empty($request->iUpperCategoryId)) {

            $iSlipProductId                     = $request->iUpperSlipProductId;
            $slip_product_data['iSlipId']       = $request->iSlipId;
            $slip_product_data['eType']         = "Upper";
            $slip_product_data['iCategoryId']   = $request->iUpperCategoryId;
            $slip_product_data['iProductId']    = $request->iUpperProductId;
            $slip_product_data['iGradeId']      = $request->iUpperGradeId;
            $slip_product_data['iStageId']      = $request->iUpperStageId;
            $slip_product_data['eExtraction']   = $request->upper_extraction_stage;
            $slip_product_data['eGumShade']     = $request->upper_gumshade_stage;
            $slip_product_data['eTeethShade']   = $request->upper_teethshade_stage;
            $slip_product_data['eImpression']   = $request->upper_Impression_stage;
            $slip_product_data['eRushDate']     = $request->upper_rush_date_stage;  
            $slip_product_data['iToothBrandId']  = $request->upper_tsbrand;
            $slip_product_data['iToothShadesId'] = $request->upper_tshade;
            $slip_product_data['iGumBrandId']    = $request->upper_gsbrand;
            $slip_product_data['iGumShadesId']   = $request->upper_gshade;
            $slip_product_data['vTeethInMouth']  = $request->upper_in_mouth_value;
            $slip_product_data['vMissingTeeth']  = $request->upper_missing_teeth_value;
            $slip_product_data['vWillExtractOnDelivery']= $request->upper_ectract_delivery_value;
            $slip_product_data['vHasBeenExtracted']= $request->upper_been_extracted_value;
            $slip_product_data['vFixOrAdd']      = $request->upper_fix_value;
            $slip_product_data['vClasps']        = $request->upper_clasps_value;
            $slip_product_data['eStatus']        = $request->upper_status;
            
            if($iSlipProductId){
                $slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
            }else{
                $slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
                $slip_product_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            }

            if(!empty($iSlipProductId)){
                $where                      = array();
                $where['iSlipProductId']           = $iSlipProductId;
                $SlipProduct_obj = new SlipProduct();
                $SlipProduct_obj->update($where, $slip_product_data);
                $SlipProduct_id = $iSlipProductId;
            }else{
                $SlipProduct_id = SlipProduct::add($slip_product_data);
            }
           
            
            /*Upper Impression Start*/
            $impression_data_array = $request->upper_impression_checkbox;
            if(!empty($impression_data_array)){
                $where                      = array();
                $where['iSlipId']           = $iSlipId;
                $where['eType']             = "Upper";
                SlipImpression::delete_by_id($where);
                foreach ($impression_data_array as $key => $value) 
                {
                    $impression_data = array();
                    $impression_data['iSlipId']      = $request->iSlipId;
                    $impression_data['iImpressionId'] = $value;
                    $impression_data['iQuantity'] = $request->input("upper_impression_qty_".$value);
                    $impression_data['eType'] = "Upper";    
                    SlipImpression::add($impression_data);
                }
            }
            /*Upper Impression End*/
            
            /*Upper Addons Start*/
            $upper_add_on_cat_id = $request->upper_add_on_cat_id;
            $upper_add_on_id = $request->upper_add_on_id;
            $upper_add_on_qty = $request->upper_add_on_qty;
            if(!empty($upper_add_on_cat_id)){
                    $where                      = array();
                    $where['iSlipId']           = $iSlipId;
                    $where['eType']             = "Upper";
                    SlipAddons::delete_by_id($where);
                    
                foreach ($upper_add_on_cat_id as $key => $value) 
                {
                    $upper_slip_product_addons = array();
                    $upper_slip_product_addons['iSlipId']      = $iSlipId;
                    $upper_slip_product_addons['iAddCategoryId'] = $value;
                    
                    $upper_slip_product_addons['iSubAddCategoryId'] = $upper_add_on_id[$key];
                    $upper_slip_product_addons['iQuantity'] = $upper_add_on_qty[$key];
                    $upper_slip_product_addons['eType'] = "Upper";
                     SlipAddons::add($upper_slip_product_addons);
                }
            }
            
            /*Upper Addons End*/
            

            /*Opposite Impressions Start*/
           
            if (empty($request->iLowerCategoryId) AND $request->lower_eOpposingImpressions == "Yes") {
                $lowerimpression_data_array = $request->lower_impression_checkbox;

                if(!empty($lowerimpression_data_array)){
                    foreach ($lowerimpression_data_array as $key => $value) 
                    {

                        $lower_impression_qty = $request->input("lower_impression_qty_".$value);
                        $lower_impression_data = array();
                        $lower_impression_data['iSlipId']      = $iSlipId;
                        $lower_impression_data['iImpressionId'] = $value;

                        $lower_impression_data['iQuantity'] = $lower_impression_qty;
                        $lower_impression_data['eType'] = "Opposite";
                        if(!empty($iSlipId)){
                            $where                      = array();
                            $where['iSlipId']           = $iSlipId;
                            
                            $Slip_product_obj = new SlipImpression();
                            $Slip_product_obj->update($where, $lower_impression_data);
                        }
                    }
                }
            }
            /*Opposite Impressions End*/
            /*Opposite Extractions Start*/
            if (empty($request->iLowerCategoryId) AND $request->lower_eOpposingExtractions == "Yes") {
                $lower_opposite_extraction = array();
                $lower_opposite_extraction['vTeethInMouthOp']  = $request->lower_in_mouth_value;
                $lower_opposite_extraction['vMissingTeethOp']  = $request->lower_missing_teeth_value;
                // dd($request);
                $lower_opposite_extraction['vWillExtractOnDeliveryOp']= $request->lower_ectract_delivery_value;
                $lower_opposite_extraction['vHasBeenExtractedOp']= $request->lower_been_extracted_value;
                $lower_opposite_extraction['vFixOrAddOp']      = $request->lower_fix_value;
                $lower_opposite_extraction['vClaspsOp']        = $request->lower_clasps_value;

                $where                      = array();
               
                $where['iSlipProductId']       = $iSlipProductId;

                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $lower_opposite_extraction);

            }
            /*Opposite Extractions End*/

            // dd($request);
            /* Upper Dates Settings */
            
            if(isset($request->iOldPickupDate) && $request->iOldPickupDate!='')
            {
                $upper_without_rush = $this->old_product_dates($request->iUpperCategoryId,$request->iUpperStageId,'',$request->iOldPickupDate);
            }
            else
            {
                $upper_without_rush = $this->product_dates($request->iUpperCategoryId,$request->iUpperStageId);
            }

            if(isset($upper_without_rush) && !empty($upper_without_rush))
            {
                $upper_date_update = array();
                $upper_date_update['dPickUpDate'] = $upper_without_rush['pic_date'];
                $upper_date_update['dDeliveryDate'] = $upper_without_rush['del_date'];
                $upper_date_update['tDeliveryTime'] = $upper_without_rush['delivery_time'];
    
    
                $upper_date_update['dDeliveryDateFinal'] = $upper_without_rush['del_date'];
    
                $where                         = array();
                $where['iSlipProductId']       = $iSlipProductId;
    
                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $upper_date_update);
                
            }


            if(!empty($request->upper_rush_feesid))
            {    
                $fees_data = Fees::get_by_id($request->upper_rush_feesid);
                if(isset($request->iOldPickupDate) && $request->iOldPickupDate!='')
                {
                    $output = $this->old_product_dates($request->iUpperCategoryId,$request->iUpperStageId, $fees_data->iWorkingTime,$request->iOldPickupDate);
                }
                else
                {
                    $output = $this->product_dates($request->iUpperCategoryId,$request->iUpperStageId, $fees_data->iWorkingTime);
                }
              
                $upper_rushdate_update = array();
                $upper_rushdate_update['dDeliveryRushDate']     = $output['del_date'];
                $upper_rushdate_update['dDeliveryDateFinal']    = $output['del_date'];
                $upper_rushdate_update['eRushDate'] = "Yes";
                $upper_rushdate_update['iFeesId'] = $request->upper_rush_feesid;
                // dd($upper_rushdate_update);
                $where                         = array();
                $where['iSlipProductId']       = $SlipProduct_id;
                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $upper_rushdate_update);

                $upper_slip_rushdate_update['dUpperRushDate'] = $output['del_date'];

                $where                  = array();
                $where['iSlipId']       = $iSlipId;
                $Slip_id_Update = new Slip();
                $Slip_id_Update->update($where, $upper_slip_rushdate_update);
            }else{
                $upper_rushdate_update = array();
                $upper_rushdate_update['dDeliveryRushDate'] = null;
                $upper_rushdate_update['eRushDate'] = "No";
                $upper_rushdate_update['iFeesId'] = null;
                // dd($upper_rushdate_update);
                $where                         = array();
                $where['iSlipProductId']       = $SlipProduct_id;
                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $upper_rushdate_update);


                // $upper_slip_rushdate_update['dUpperRushDate'] = null;

                // $where                  = array();
                // $where['iSlipId']       = $iSlipId;
                // $Slip_id_Update = new Slip();
                // $Slip_id_Update->update($where, $upper_slip_rushdate_update);
            }
            /* Upper Dates Settings */

            /*Lower Product Start*/
        }
     
        if (!empty($request->iLowerCategoryId)) {
            $iSlipProductId                           = $request->iLowerSlipProductId;
            $lower_slip_product_data['iSlipId']       = $iSlipId;
            $lower_slip_product_data['eType']         = "Lower";
            $lower_slip_product_data['iCategoryId']   = $request->iLowerCategoryId;
            $lower_slip_product_data['iProductId']    = $request->iLowerProductId;
            $lower_slip_product_data['iGradeId']      = $request->iLowerGradeId;
            $lower_slip_product_data['iStageId']      = $request->iLowerStageId;
            $lower_slip_product_data['eExtraction']   = $request->lower_extraction_stage;
            $lower_slip_product_data['eGumShade']     = $request->lower_gumshade_stage;
            $lower_slip_product_data['eTeethShade']   = $request->lower_teethshade_stage;
            $lower_slip_product_data['eImpression']   = $request->lower_Impression_stage;
            $lower_slip_product_data['eRushDate']     = $request->lower_rush_date_stage;
            $lower_slip_product_data['iToothBrandId']  = $request->lower_tsbrand;
            $lower_slip_product_data['iToothShadesId'] = $request->lower_tshade;
            $lower_slip_product_data['iGumBrandId']    = $request->lower_gsbrand;
            $lower_slip_product_data['iGumShadesId']   = $request->lower_gshade;
            $lower_slip_product_data['vTeethInMouth']  = $request->lower_in_mouth_value;
            $lower_slip_product_data['vMissingTeeth']  = $request->lower_missing_teeth_value;
            $lower_slip_product_data['vWillExtractOnDelivery']= $request->lower_ectract_delivery_value;
            $lower_slip_product_data['vHasBeenExtracted']= $request->lower_been_extracted_value;
            $lower_slip_product_data['vFixOrAdd']      = $request->lower_fix_value;
            $lower_slip_product_data['vClasps']        = $request->lower_clasps_value;
            $lower_slip_product_data['eStatus']       =  $request->lower_status;

            if($iSlipProductId){
                $lower_slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
            }else{
                $lower_slip_product_data['dtAddedDate'] = date("Y-m-d H:i:s");
                $lower_slip_product_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
            }
            if(!empty($iSlipProductId)){
                $where                      = array();
                $where['iSlipProductId']       = $iSlipProductId;
                $SlipProduct_obj = new SlipProduct();
                $SlipProduct_obj->update($where, $lower_slip_product_data);
            }else{
                $SlipProduct_id = SlipProduct::add($lower_slip_product_data);
            }
            /*Lower Impression Start*/
            $lowerimpression_data_array = $request->lower_impression_checkbox;
         
            
            if(!empty($lowerimpression_data_array)){
                $where                      = array();
                $where['iSlipId']           = $iSlipId;
                $where['eType']             = "Lower";
                
                SlipImpression::delete_by_id($where);
                foreach ($lowerimpression_data_array as $key => $value) 
                {
                    $lower_impression_qty = $request->input("lower_impression_qty_".$value);
                    $lower_impression_data = array();
                    $lower_impression_data['iSlipId']      = $iSlipId;
                    $lower_impression_data['iImpressionId'] = $value;
                    $lower_impression_data['iQuantity'] = $lower_impression_qty;
                    $lower_impression_data['eType'] = "Lower";
                    SlipImpression::add($lower_impression_data);
                }
            }
            /*Lower Impression End*/

            /*Lower Addons Start*/
            $lower_add_on_cat_id = $request->lower_add_on_cat_id;
            $lower_add_on_id = $request->lower_add_on_id;
            $lower_add_on_qty = $request->lower_add_on_qty;
            if(!empty($lower_add_on_cat_id)){
                $where                      = array();
                $where['iSlipId']           = $iSlipId;
                $where['eType']             = "Lower";

                SlipAddons::delete_by_id($where);

                foreach ($lower_add_on_cat_id as $key => $value) 
                {
                    $lower_slip_product_addons = array();
                    $lower_slip_product_addons['iSlipId']      = $iSlipId;
                    $lower_slip_product_addons['iAddCategoryId'] = $value;
                    $lower_slip_product_addons['iSubAddCategoryId'] = $lower_add_on_id[$key];
                    $lower_slip_product_addons['iQuantity'] = $lower_add_on_qty[$key];
                    $lower_slip_product_addons['eType'] = "Lower";
                    SlipAddons::add($lower_slip_product_addons);
                }
            }
            /*Lower Addons End*/


            /*Opposite Impressions Start*/
          
            if (empty($request->iUpperCategoryId) AND $request->upper_eOpposingImpressions == "Yes") {
                $impression_data_array = $request->upper_impression_checkbox;
                if(!empty($impression_data_array)){
                    foreach ($impression_data_array as $key => $value) 
                    {   
                        $impression_data = array();
                        $impression_data['iSlipId']      = $iSlipId;
                        $impression_data['iImpressionId'] = $value;
                        
                        $impression_data['iQuantity'] = $request->input("upper_impression_qty_".$value);
                        $impression_data['eType'] = "Opposite";        
                        
                        if(!empty($iSlipId)){
                            $where                      = array();
                            $where['iSlipId']           = $iSlipId;
                            
                            $SlipProduct_obj = new SlipImpression();
                            $SlipProduct_obj->update($where, $impression_data);
                        }
                    }
                }
            }
            /*Opposite Impressions End*/
            /*Opposite Extractions Start*/
            if (empty($request->iUpperCategoryId) AND $request->upper_eOpposingExtractions == "Yes") {
                $upper_opposite_extraction = array();
                $upper_opposite_extraction['vTeethInMouthOp']  = $request->upper_in_mouth_value;
                $upper_opposite_extraction['vMissingTeethOp']  = $request->upper_missing_teeth_value;
                $upper_opposite_extraction['vWillExtractOnDeliveryOp']= $request->upper_ectract_delivery_value;
                $upper_opposite_extraction['vHasBeenExtractedOp']= $request->upper_been_extracted_value;
                $upper_opposite_extraction['vFixOrAddOp']      = $request->upper_fix_value;
                $upper_opposite_extraction['vClaspsOp']        = $request->upper_clasps_value;

                $where                      = array();
                $where['iSlipProductId']    = $iSlipProductId;

                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $upper_opposite_extraction);


            }
            /*Opposite Extractions End*/

             /* Lower Dates Settings */
            if(isset($request->iOldPickupDate) && $request->iOldPickupDate!='')
            {
                $lower_without_rush = $this->old_product_dates($request->iLowerCategoryId,$request->iLowerStageId,'',$request->iOldPickupDate);
            }
            else
            {
                $lower_without_rush = $this->product_dates($request->iLowerCategoryId,$request->iLowerStageId);
            }
            if(isset($lower_without_rush) && !empty($lower_without_rush))
            {
                $lower_date_update = array();
                $lower_date_update['dPickUpDate']   = $lower_without_rush['pic_date'];
                $lower_date_update['dDeliveryDate'] = $lower_without_rush['del_date'];
                $lower_date_update['tDeliveryTime'] = $lower_without_rush['delivery_time'];
                
                $lower_date_update['dDeliveryDateFinal'] = $lower_without_rush['del_date'];
   
                $where                      = array();
                $where['iSlipProductId']       = $iSlipProductId;
   
                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $lower_date_update);
            }

             if(!empty($request->lower_rush_feesid))
             {    
                 $fees_data = Fees::get_by_id($request->lower_rush_feesid);
                 if(isset($request->iOldPickupDate) && $request->iOldPickupDate!='')
                 {
                    $output = $this->old_product_dates($request->iLowerCategoryId,$request->iLowerStageId, $fees_data->iWorkingTime,$request->iOldPickupDate);
                 }
                 else
                 {
                     $output = $this->product_dates($request->iLowerCategoryId,$request->iLowerStageId, $fees_data->iWorkingTime);
                 }
            
                $lower_rushdate_update = array();
                $lower_rushdate_update['dDeliveryRushDate'] = $output['del_date'];
                $lower_rushdate_update['dDeliveryDateFinal'] = $output['del_date'];
                $lower_rushdate_update['eRushDate'] = "Yes";
                $lower_rushdate_update['iFeesId'] = $request->lower_rush_feesid;
                
                $where                      = array();
                $where['iSlipProductId']       = $iSlipProductId;

                $Slip_product_obj = new SlipProduct();
              
                $Slip_product_obj->update($where, $lower_rushdate_update);

                $lower_slip_rushdate_update['dLowerRushDate'] = $output['del_date'];

                $where                  = array();
                $where['iSlipId']       = $iSlipId;
                $Slip_id_Update = new Slip();
                $Slip_id_Update->update($where, $lower_slip_rushdate_update);
             }else{
                $lower_rushdate_update = array();
                $lower_rushdate_update['dDeliveryRushDate'] = null;
                $lower_rushdate_update['eRushDate'] = "No";
                $lower_rushdate_update['iFeesId'] = null;
                
                $where                      = array();
                $where['iSlipProductId']       = $iSlipProductId;
                $Slip_product_obj = new SlipProduct();
                $Slip_product_obj->update($where, $lower_rushdate_update);

                // $lower_slip_rushdate_update['dLowerRushDate'] = null;

                // $where                  = array();
                // $where['iSlipId']       = $iSlipId;
                // $Slip_id_Update = new Slip();
                // $Slip_id_Update->update($where, $lower_slip_rushdate_update);
             }
             /* Lower Dates Settings */
        }
        /*Lower Product End*/
        // On hold status update start
        $upper_status = $request->upper_status;
        $lower_status = $request->lower_status;
        if(isset($upper_status) && !empty($upper_status) )
        {
            $Slip_upper_product_data['eStatus'] = $upper_status;
            // $Slip_upper_product_data['eCustomDate'] = $request->upper_custom_date;
            if(isset($request->upper_delivery_date) && !empty($request->upper_delivery_date))
            {
                $Slip_upper_product_data['dDeliveryRushDate'] = null;
                $Slip_upper_product_data['eRushDate'] = 'No';
                $Slip_upper_product_data['dDeliveryDate'] = date("Y-m-d",strtotime($request->upper_delivery_date));
                $Slip_upper_product_data['dDeliveryDateFinal'] = date("Y-m-d",strtotime($request->upper_delivery_date));
            }
            $where                      = array();
            $where['iSlipProductId']       = $request->iUpperSlipProductId;
            $Slip_product_obj = new SlipProduct();
            $Slip_product_obj->update($where, $Slip_upper_product_data);
        }
        if(isset($lower_status) && !empty($lower_status))
        {
            $Slip_lower_product_data['eStatus'] = $lower_status;
            // $Slip_lower_product_data['eCustomDate'] = $request->lower_custom_date;
            if(isset($request->lower_delivery_date) && !empty($request->lower_delivery_date))
            {
                $Slip_lower_product_data['dDeliveryRushDate'] = null;
                $Slip_lower_product_data['eRushDate'] = 'No';
                $Slip_lower_product_data['dDeliveryDate'] = date("Y-m-d",strtotime($request->lower_delivery_date));

                $Slip_lower_product_data['dDeliveryDateFinal'] = date("Y-m-d",strtotime($request->lower_delivery_date));
            }
           
            $where                      = array();
            $where['iSlipProductId']       = $request->iLowerSlipProductId;
            $Slip_product_obj = new SlipProduct();
            $Slip_product_obj->update($where, $Slip_lower_product_data);
        }
        // On hold status update end
        // dd($request->tDescription);
        if(!empty($request->tDescription)){

            $slip_notes = array();
            $slip_notes['iSlipId']      = $iSlipId;
            $slip_notes['iCaseId']      = $slip_data['iCaseId'];
            if(!empty($request->iUpperCategoryId) AND !empty($request->iLowerCategoryId)){
                $slip_notes['iStageId']     = $request->iUpperStageId."/".$request->iLowerStageId;

                $upper_stage = ProductStage::get_by_id($request->iUpperStageId);
                $upper_stage_name = $upper_stage->vName;

                $lower_stage = ProductStage::get_by_id($request->iLowerStageId);
                $lower_stage_name = $lower_stage->vName;

                $slip_notes['vStageName']     =  $upper_stage_name."/". $lower_stage_name;

            }elseif(!empty($request->iUpperCategoryId) AND empty($request->iLowerCategoryId)){
                
                $slip_notes['iStageId']     = $request->iUpperStageId;

                $upper_stage = ProductStage::get_by_id($request->iUpperStageId);
                $upper_stage_name = $upper_stage->vName;
                $slip_notes['vStageName']     =  $upper_stage_name;

            }elseif((empty($request->iUpperCategoryId) AND !empty($request->iLowerCategoryId))){
                
                $slip_notes['iStageId']     = $request->iLowerStageId;

                $lower_stage = ProductStage::get_by_id($request->iLowerStageId);
                $lower_stage_name = $lower_stage->vName;

                $slip_notes['vStageName']     = $lower_stage_name;
            }
            $upper_dilevery_date =  $request->upper_delivery_date;
            $lower_dilevery_date =  $request->lower_delivery_date;

            if($upper_dilevery_date > $lower_dilevery_date){
                
                $slip_notes['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
            }

            if($upper_dilevery_date == $lower_dilevery_date){
                                
                    $slip_notes['dDeliveryDate'] =  date("Y-m-d",strtotime($request->upper_delivery_date));
            }

            if($upper_dilevery_date < $lower_dilevery_date){
                                
                $slip_notes['dDeliveryDate'] =  date("Y-m-d",strtotime($request->lower_delivery_date));
            }
            $slip_notes['vCaseNumber']  = $request->vCaseNumber;
            $slip_notes['vCreatedByName'] = $request->vCreatedByName;
            $slip_notes['tStagesNotes']  =  $request->tDescription;
            $slip_notes['vSlipNumber']   = $request->vSlipNumber;
            $slip_notes['dtAddedDate'] = date("Y-m-d H:i:s");
            $slip_notes['tAddedTime'] = date("h:i");
            $SlipData = SlipNote::where('iSlipId',$iSlipId)->get()->first();
            if(!empty($SlipData))
            {
                $where_array = array('iSlipNotesId'=>$SlipData->iSlipNotesId);
                $slip_note = SlipNote::update_new($where_array,$slip_notes);
            }
            else
            {
                $slip_note = SlipNote::add($slip_notes);
            }

     
           
        }
        if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == '6')
        {
            return redirect()->route('admin.labcase.office_admin_listing')->withSuccess('Satge updated successfully.');
        }
        else
        {
            return redirect()->route('admin.labcase')->withSuccess('Satge updated successfully.');
        }
    }

    // Update Billing data start
    public function update_billing($iCaseId,$iSlipId)
    {
        // Check entry exist on billing start
        $exist_slip = Labcase::exist_slip_in_billing($iCaseId,$iSlipId);
        if($exist_slip == 0)
        {
            $BillingData = array();
            $AllData = $this->fetch_slip_data($iCaseId,$iSlipId);
            $DoctorData = Doctor::get_by_id($AllData['Slip']->iDoctorId);
            // Add on billing table start
            $BillingData['iCaseId'] = $iCaseId;
            $BillingData['iSlipId'] = $iSlipId;
            $BillingData['vSlipNumber'] = isset($AllData['Slip']->vSlipNumber)?$AllData['Slip']->vSlipNumber:'';
            $BillingData['vCaseNumber'] = isset($AllData['Case']->vCaseNumber)?$AllData['Case']->vCaseNumber:'';
            $BillingData['iOfficeId'] = isset($AllData['Slip']->iOfficeId)?$AllData['Slip']->iOfficeId:'';
            $BillingData['vOfficeName'] = isset($AllData['CustomerOfficeData']->vOfficeName)?$AllData['CustomerOfficeData']->vOfficeName:'';
            $BillingData['vOfficeCode'] = isset($AllData['CustomerOfficeData']->vOfficeName)?$AllData['CustomerOfficeData']->vOfficeName:'';
            $BillingData['iLabId'] = isset($AllData['CustomerLabData']->iCustomerId)?$AllData['CustomerLabData']->iCustomerId:'';
            $BillingData['vLabName'] = isset($AllData['CustomerLabData']->vOfficeName)?$AllData['CustomerLabData']->vOfficeName:'';
            $BillingData['iDoctorId'] = isset($AllData['Slip']->iDoctorId)?$AllData['Slip']->iDoctorId:'';
            $BillingData['vDoctorName'] = isset($DoctorData->vFirstName)?$DoctorData->vFirstName.' '.$DoctorData->vLastName:'';
            $BillingData['vPatientName'] = isset($AllData['Slip']->vPatientName)?$AllData['Slip']->vPatientName:'';
            $BillingData['vCasePanNumber'] = isset($AllData['Slip']->vCasePanNumber)?$AllData['Slip']->vCasePanNumber:'';
            $BillingData['dDeliveryDate'] = isset($AllData['Slip']->dDeliveryDate)?$AllData['Slip']->dDeliveryDate:'';
            $BillingData['eStatus'] = 'UnPaid';
            $BillingData['dtAddedDate'] = date("Y-m-d H:i:s");
            $BillingData['dtUpdatedDate'] = date("Y-m-d H:i:s");
            $iBillingId = Labcase::add_billing($BillingData);
            // Add on billing table end
            // Add on product table start
            $BillingUpperProductData = array();
            if(isset($AllData['upper_product']->iProductId) && !empty($AllData['upper_product']->iProductId))
            {
                $BillingUpperProductData['iBillingId'] = $iBillingId;
                $BillingUpperProductData['eType'] = "Upper";
                $BillingUpperProductData['iCategoryId'] = isset($AllData['upper_product']->iCategoryId)?$AllData['upper_product']->iCategoryId:'';
                $BillingUpperProductData['vCategoryName'] = isset($AllData['upper_product']->vCategoryName)?$AllData['upper_product']->vCategoryName:'';
                $BillingUpperProductData['iProductId'] = isset($AllData['upper_product']->iProductId)?$AllData['upper_product']->iProductId:'';
                $BillingUpperProductData['vProductName'] = isset($AllData['upper_product']->vProductName)?$AllData['upper_product']->vProductName:'';
                $BillingUpperProductData['iStageId'] = isset($AllData['upper_product']->iStageId)?$AllData['upper_product']->iStageId:'';
                if(isset($AllData['upper_product']->iStageId) && !empty($AllData['upper_product']->iStageId))
                {
                    $ProductUpperStageData = ProductStage::get_by_id($AllData['upper_product']->iProductId);
                    $BillingUpperProductData['vStageCode'] = isset($ProductUpperStageData->vCode)?$ProductUpperStageData->vCode:'';
                }
                if(isset($AllData['upper_product']->iProductId) && !empty($AllData['upper_product']->iProductId))
                {
                    $CategoryProductData = CategoryProduct::get_by_id($AllData['upper_product']->iProductId);

                    $iLoginAdminId = General::admin_info()['iCustomerId'];
                    
                    // Office product price exist start grade vise
                    if(isset($AllData['upper_product']->iGradeId) && !empty($AllData['upper_product']->iGradeId) && $AllData['upper_product']->iGradeId != null)
                    {
                        $FetchProductGradeOfficePrice = OfficeProductPrice::FetchProductGradeOfficePrice($AllData['upper_product']->iProductId,$AllData['upper_product']->iGradeId);
                        
                        $FetchProductGradePrice = ProductGrade::FetchProductGradePrice($AllData['upper_product']->iProductId,$AllData['upper_product']->iGradeId);
                        
                        $FetchProductOfficePrice = OfficeProductPrice::FetchProductOfficePrice($AllData['upper_product']->iProductId);

                        
                        if(isset($FetchProductGradeOfficePrice->vPrice) && !empty($FetchProductGradeOfficePrice->vPrice) && $FetchProductGradeOfficePrice->vPrice != 00)
                        {
                           $CategoryProductPrice = $FetchProductGradeOfficePrice->vPrice;
                        }
                        else if(isset($FetchProductGradePrice->vPrice) && !empty($FetchProductGradePrice->vPrice) && $FetchProductGradePrice->vPrice != 00)
                        {
                           $CategoryProductPrice = $FetchProductGradePrice->vPrice;
                        }
                        else if(isset($FetchProductOfficePrice->vPrice) && !empty($FetchProductOfficePrice->vPrice) && $FetchProductOfficePrice->vPrice != 00)
                        {
                           $CategoryProductPrice = $FetchProductOfficePrice->vPrice;
                        }
                        else
                        {
                            $CategoryProductPrice = isset($CategoryProductData->vPrice)?$CategoryProductData->vPrice:'';
                        }
                    }
                    else
                    {
                        $FetchProductOfficePrice = OfficeProductPrice::FetchProductOfficePrice($AllData['upper_product']->iProductId);
                        if(isset($FetchProductOfficePrice->vPrice) && !empty($FetchProductOfficePrice->vPrice) && $FetchProductOfficePrice->vPrice != 00)
                        {
                           $CategoryProductPrice = $FetchProductOfficePrice->vPrice;
                        }
                        else
                        {
                            $CategoryProductPrice = isset($CategoryProductData->vPrice)?$CategoryProductData->vPrice:'';
                        }
                    }
                     // Office product price exist end grade vise

                    // $ExistUpperProductOffcice = OfficePrices::checkExistOffice($iLoginAdminId,$AllData['upper_product']->iProductId);
                    // if(isset($ExistUpperProductOffcice) && !empty($ExistUpperProductOffcice) && count($ExistUpperProductOffcice)>0)
                    // {
                    //    $CategoryProductPrice = $ExistUpperProductOffcice[0]->vOfficePrice;
                    // }
                    // else
                    // {
                    //     $CategoryProductPrice = isset($CategoryProductData->vPrice)?$CategoryProductData->vPrice:'';
                    // }

                }
                $BillingUpperProductData['iProductPrice'] = isset($CategoryProductPrice)?$CategoryProductPrice:'';
                if(isset($AllData['upper_product']->iStageId) && !empty($AllData['upper_product']->iStageId))
                {
                    $StageData = ProductStage::get_by_id($AllData['upper_product']->iStageId);

                    $iLoginAdminId = General::admin_info()['iCustomerId'];

                    // check stage grade price start
                    if(isset($AllData['upper_product']->iGradeId) && !empty($AllData['upper_product']->iGradeId) && $AllData['upper_product']->iGradeId != null)
                    {
                        $FetchStageGradeOfficePrice = OfficeStagePrice::FetchStageGradeOfficePrice($AllData['upper_product']->iProductId,$AllData['upper_product']->iGradeId,$AllData['upper_product']->iStageId);
                        
                        $FetchStageGradePrice = StageGrade::FetchStageGradePrice($AllData['upper_product']->iStageId,$AllData['upper_product']->iGradeId);
                        
                        $FetchStageOfficePrice = OfficeStagePrice::FetchStageOfficePrice($AllData['upper_product']->iProductId,$AllData['upper_product']->iStageId);

                        
                        if(isset($FetchStageGradeOfficePrice->vPrice) && !empty($FetchStageGradeOfficePrice->vPrice) && $FetchStageGradeOfficePrice->vPrice != 00)
                        {
                           $CategoryStagePrice = $FetchStageGradeOfficePrice->vPrice;
                        }
                        else if(isset($FetchStageGradePrice->vPrice) && !empty($FetchStageGradePrice->vPrice) && $FetchStageGradePrice->vPrice != 00)
                        {
                           $CategoryStagePrice = $FetchStageGradePrice->vPrice;
                        }
                        else if(isset($FetchStageOfficePrice->vPrice) && !empty($FetchStageOfficePrice->vPrice) && $FetchStageOfficePrice->vPrice != 00)
                        {
                           $CategoryStagePrice = $FetchStageOfficePrice->vPrice;
                        }
                        else
                        {
                            $CategoryStagePrice = isset($StageData->vPrice)?$StageData->vPrice:'';
                        }
                    }
                    else
                    {
                        $FetchProductOfficePrice = OfficeProductPrice::FetchProductOfficePrice($AllData['upper_product']->iProductId);
                        if(isset($FetchProductOfficePrice->vPrice) && !empty($FetchProductOfficePrice->vPrice) && $FetchProductOfficePrice->vPrice != 00)
                        {
                            $CategoryProductPrice = $FetchProductOfficePrice->vPrice;
                        }
                        else
                        {
                            $CategoryProductPrice = isset($StageData->vPrice)?$StageData->vPrice:'';
                        }
                    }
                    // check stage grade price end

                    // $ExistUpperStageOffcice = OfficePrices::checkExistOffice($iLoginAdminId,$AllData['upper_product']->iStageId);
                    // if(isset($ExistUpperStageOffcice) && !empty($ExistUpperStageOffcice) && count($ExistUpperStageOffcice)>0)
                    // {
                    //    $CategoryStagePrice = $ExistUpperStageOffcice[0]->vOfficePrice;
                    // }
                    // else
                    // {
                    //     $CategoryStagePrice = isset($StageData->vPrice)?$StageData->vPrice:'';
                    // }
                }
                if(isset($AllData['upper_product']->iGradeId) && !empty($AllData['upper_product']->iGradeId))
                {
                    $BillingUpperProductData['iStagePrice'] = Labcase::get_grade_price($AllData['upper_product']->iGradeId,$AllData['Slip']->iOfficeId,$AllData['upper_product']->iStageId)->vPrice;
                    if(isset($BillingUpperProductData['iStagePrice']) && $BillingUpperProductData['iStagePrice'] == '00')
                    {
                        $BillingUpperProductData['iStagePrice'] = isset($StageData->vPrice)?$StageData->vPrice:'';
                    }
                }
                else
                {
                    $BillingUpperProductData['iStagePrice'] =  isset($StageData->vPrice)?$StageData->vPrice:'';
                }
                
                $BillingUpperProductData['vGradeName'] = isset($AllData['upper_product']->vGradeName)?$AllData['upper_product']->vGradeName:'';
                $BillingUpperProductData['vStageName'] = isset($StageData->vName)?$StageData->vName:'';
                $BillingUpperProductData['iToothBrandId'] = isset($AllData['upper_product']->iToothBrandId)?$AllData['upper_product']->iToothBrandId:'';
                if(isset($AllData['upper_product']->iToothBrandId) && !empty($AllData['upper_product']->iToothBrandId))
                {
                    $toothBranData = ToothBrand::get_by_id($AllData['upper_product']->iToothBrandId);
                }
                $BillingUpperProductData['vToothBrandName'] = isset($toothBranData->vName)?$toothBranData->vName:'';
                $BillingUpperProductData['iToothShadesId'] = isset($AllData['upper_product']->iToothShadesId)?$AllData['upper_product']->iToothShadesId:'';
                if(isset($AllData['upper_product']->iToothShadesId) && !empty($AllData['upper_product']->iToothShadesId))
                {
                    $toothShadesData = ToothShades::get_by_id($AllData['upper_product']->iToothShadesId);
                }
                $BillingUpperProductData['vtoothShadesCode'] = isset($toothShadesData->vCode)?$toothShadesData->vCode:'';
                $BillingUpperProductData['vToothShadesName'] = isset($toothShadesData->vToothShade)?$toothShadesData->vToothShade:'';
                $BillingUpperProductData['iGumBrandId'] = isset($AllData['upper_product']->iGumBrandId)?$AllData['upper_product']->iGumBrandId:'';
                if(isset($AllData['upper_product']->iGumBrandId) && !empty($AllData['upper_product']->iGumBrandId))
                {
                    $GumBrandData = GumBrand::get_by_id($AllData['upper_product']->iGumBrandId);
                }
                $BillingUpperProductData['vGumBrandName'] = isset($GumBrandData->vName)?$GumBrandData->vName:'';
                $BillingUpperProductData['iGumShadesId'] = $AllData['upper_product']->iGumShadesId;
                if(isset($AllData['upper_product']->iGumShadesId) && !empty($AllData['upper_product']->iGumShadesId))
                {
                    $GumShadesData = GumShades::get_by_id($AllData['upper_product']->iGumShadesId);
                }
                $BillingUpperProductData['vGumShadesName'] = isset($GumShadesData->vGumShade)?$GumShadesData->vGumShade:'';
                $BillingUpperProductData['vGumShadesCode'] = isset($GumShadesData->vCode)?$GumShadesData->vCode:'';
                $BillingUpperProductData['eRushDate'] = isset($AllData['upper_product']->eRushDate)?$AllData['upper_product']->eRushDate:'';
                $BillingUpperProductData['iFeesId'] = $AllData['upper_product']->iFeesId;
                if(isset($AllData['upper_product']->iFeesId) && !empty($AllData['upper_product']->iFeesId))
                {
                    $FeesData = Fees::get_by_id($AllData['upper_product']->iFeesId);
                    $CategoryStagePrice = isset($CategoryStagePrice)?$CategoryStagePrice:0;
                    // $vUpperRushFees = isset($FeesData->vRushFee)?$FeesData->vRushFee:0;
                    // New calculation rush fees start
                    $percentFeesUpper = 1;
                   
                    if(isset($FeesData->vFee) && !empty($FeesData->vFee))
                    {
                        $vUpperRushFees = $CategoryStagePrice * $FeesData->vFee/100;
                    }
                    // New calculation rush fees end
                    $BillingUpperProductData['iStageTotalWithRush'] = $CategoryStagePrice+$vUpperRushFees;
                }
                $BillingUpperProductData['iRushDay'] = isset($FeesData->iWorkingTime)?$FeesData->iWorkingTime:'';
                $BillingUpperProductData['iRushFeesAmount'] = isset($FeesData->vRushFee)?$FeesData->vRushFee:'';
                $BillingUpperProductData['iRushFeesPercentage'] = isset($FeesData->vFee)?$FeesData->vFee:'';
                if(isset($FeesData->vFee) && !empty($FeesData->vFee) && isset($CategoryStagePrice) && !empty($CategoryStagePrice))
                {
                    $BillingUpperProductData['iTotalWithoutAddOnsAmount'] = $FeesData->vFee + $CategoryStagePrice;
                }
                else if(isset($FeesData->vFee) && !empty($FeesData->vFee))
                {
                    $BillingUpperProductData['iTotalWithoutAddOnsAmount'] = $FeesData->vFee;
                }
                else if(isset($CategoryStagePrice) && !empty($CategoryStagePrice))
                {
                    $BillingUpperProductData['iTotalWithoutAddOnsAmount'] = $CategoryStagePrice;
                }
                else
                {
                    $BillingUpperProductData['iTotalWithoutAddOnsAmount'] = 0;
                }
                $iTotalUpperWithoutAddOnsAmount =$BillingUpperProductData['iTotalWithoutAddOnsAmount'];
                $BillingUpperProductData['iToalAddOnsValue'] = '';
                $BillingUpperProductData['dDeliveryDate'] = isset($AllData['upper_product']->dDeliveryDate)?$AllData['upper_product']->dDeliveryDate:'';
                $BillingUpperProductData['tDeliveryTime'] = isset($AllData['upper_product']->tDeliveryTime)?$AllData['upper_product']->tDeliveryTime:'';
                $BillingUpperProductData['dtAddedDate'] = date("Y-m-d H:i:s");
                $BillingUpperProductData['dtUpdatedDate'] = date("Y-m-d H:i:s");
                
                $iBillingUpperProductId = BillingProduct::add_billing_product($BillingUpperProductData);
            }

            if(isset($AllData['lower_product']->iProductId) && !empty($AllData['lower_product']->iProductId))
            {
                $BillingLowerProductData['iBillingId'] = $iBillingId;
                $BillingLowerProductData['eType'] = "Lower";
                $BillingLowerProductData['iCategoryId'] = isset($AllData['lower_product']->iCategoryId)?$AllData['lower_product']->iCategoryId:'';
                $BillingLowerProductData['vCategoryName'] = isset($AllData['lower_product']->vCategoryName)?$AllData['lower_product']->vCategoryName:'';
                $BillingLowerProductData['iProductId'] = isset($AllData['lower_product']->iProductId)?$AllData['lower_product']->iProductId:'';
                $BillingLowerProductData['vProductName'] = isset($AllData['lower_product']->vProductName)?$AllData['lower_product']->vProductName:'';
                $BillingLowerProductData['iStageId'] = isset($AllData['lower_product']->iStageId)?$AllData['lower_product']->iStageId:'';
                if(isset($AllData['lower_product']->iStageId) && !empty($AllData['lower_product']->iStageId))
                {
                    $ProductLowerStageData = ProductStage::get_by_id($AllData['lower_product']->iProductId);
                    $BillingLowerProductData['vStageCode'] = isset($ProductLowerStageData->vCode)?$ProductLowerStageData->vCode:'';
                }
                if(isset($AllData['lower_product']->iProductId) && !empty($AllData['lower_product']->iProductId))
                {

                    $CategoryLowerProductData = CategoryProduct::get_by_id($AllData['lower_product']->iProductId);
                    $iLoginAdminId = General::admin_info()['iCustomerId'];

                    // Office product price exist start grade vise
                    if(isset($AllData['lower_product']->iGradeId) && !empty($AllData['lower_product']->iGradeId) && $AllData['lower_product']->iGradeId != null)
                    {
                        $FetchProductGradeOfficePrice = OfficeProductPrice::FetchProductGradeOfficePrice($AllData['lower_product']->iProductId,$AllData['lower_product']->iGradeId);
                        
                        $FetchProductGradePrice = ProductGrade::FetchProductGradePrice($AllData['lower_product']->iProductId,$AllData['lower_product']->iGradeId);
                        
                        $FetchProductOfficePrice = OfficeProductPrice::FetchProductOfficePrice($AllData['lower_product']->iProductId);

                        
                        if(isset($FetchProductGradeOfficePrice->vPrice) && !empty($FetchProductGradeOfficePrice->vPrice) && $FetchProductGradeOfficePrice->vPrice != 00)
                        {
                           $CategoryLowerProductPrice = $FetchProductGradeOfficePrice->vPrice;
                        }
                        else if(isset($FetchProductGradePrice->vPrice) && !empty($FetchProductGradePrice->vPrice) && $FetchProductGradePrice->vPrice != 00)
                        {
                           $CategoryLowerProductPrice = $FetchProductGradePrice->vPrice;
                        }
                        else if(isset($FetchProductOfficePrice->vPrice) && !empty($FetchProductOfficePrice->vPrice) && $FetchProductOfficePrice->vPrice != 00)
                        {
                           $CategoryLowerProductPrice = $FetchProductOfficePrice->vPrice;
                        }
                        else
                        {
                            $CategoryLowerProductPrice = isset($CategoryLowerProductData->vPrice)?$CategoryLowerProductData->vPrice:'';
                        }
                    }
                    else
                    {
                        $FetchProductOfficePrice = OfficeProductPrice::FetchProductOfficePrice($AllData['lower_product']->iProductId);
                        if(isset($FetchProductOfficePrice->vPrice) && !empty($FetchProductOfficePrice->vPrice) && $FetchProductOfficePrice->vPrice != 00)
                        {
                           $CategoryLowerProductPrice = $FetchProductOfficePrice->vPrice;
                        }
                        else
                        {
                            $CategoryLowerProductPrice = isset($CategoryLowerProductData->vPrice)?$CategoryLowerProductData->vPrice:'';
                        }
                    }
                     // Office product price exist end grade vise

                    // $ExistUpperStageOffcice = OfficePrices::checkExistOffice($iLoginAdminId,$AllData['lower_product']->iProductId);
                    // if(isset($ExistUpperStageOffcice) && !empty($ExistUpperStageOffcice) && count($ExistUpperStageOffcice)>0)
                    // {
                    //    $CategoryLowerStagePrice = $ExistUpperStageOffcice[0]->vOfficePrice;
                    // }
                    // else
                    // {
                    //     $CategoryLowerStagePrice = isset($CategoryLowerProductData->vPrice)?$CategoryLowerProductData->vPrice:'';
                    // }
                }
                $BillingLowerProductData['iProductPrice'] = isset($CategoryLowerProductPrice)?$CategoryLowerProductPrice :'';
                if(isset($AllData['lower_product']->iStageId) && !empty($AllData['lower_product']->iStageId))
                {
                    $LowerStageData = ProductStage::get_by_id($AllData['lower_product']->iStageId);
                    $iLoginAdminId = General::admin_info()['iCustomerId'];

                     // check stage grade price start
                     if(isset($AllData['lower_product']->iGradeId) && !empty($AllData['lower_product']->iGradeId) && $AllData['lower_product']->iGradeId != null)
                     {
                         $FetchStageGradeOfficePrice = OfficeStagePrice::FetchStageGradeOfficePrice($AllData['lower_product']->iProductId,$AllData['lower_product']->iGradeId,$AllData['lower_product']->iStageId);
                         
                         $FetchStageGradePrice = StageGrade::FetchStageGradePrice($AllData['lower_product']->iStageId,$AllData['lower_product']->iGradeId);
                         
                         $FetchStageOfficePrice = OfficeStagePrice::FetchStageOfficePrice($AllData['lower_product']->iProductId,$AllData['lower_product']->iStageId);
 
                         
                         if(isset($FetchStageGradeOfficePrice->vPrice) && !empty($FetchStageGradeOfficePrice->vPrice) && $FetchStageGradeOfficePrice->vPrice != 00)
                         {
                            $LowerStagePrice = $FetchStageGradeOfficePrice->vPrice;
                         }
                         else if(isset($FetchStageGradePrice->vPrice) && !empty($FetchStageGradePrice->vPrice) && $FetchStageGradePrice->vPrice != 00)
                         {
                            $LowerStagePrice = $FetchStageGradePrice->vPrice;
                         }
                         else if(isset($FetchStageOfficePrice->vPrice) && !empty($FetchStageOfficePrice->vPrice) && $FetchStageOfficePrice->vPrice != 00)
                         {
                            $LowerStagePrice = $FetchStageOfficePrice->vPrice;
                         }
                         else
                         {
                             $LowerStagePrice = isset($StageData->vPrice)?$StageData->vPrice:'';
                         }
                     }
                     else
                     {
                         $FetchProductOfficePrice = OfficeProductPrice::FetchProductOfficePrice($AllData['upper_product']->iProductId);
                         if(isset($FetchProductOfficePrice->vPrice) && !empty($FetchProductOfficePrice->vPrice) && $FetchProductOfficePrice->vPrice != 00)
                         {
                             $LowerStagePrice = $FetchProductOfficePrice->vPrice;
                         }
                         else
                         {
                             $LowerStagePrice = isset($StageData->vPrice)?$StageData->vPrice:'';
                         }
                     }
                     // check stage grade price end

                    // $ExistUpperStageOffcice = OfficePrices::checkExistOffice($iLoginAdminId,$AllData['lower_product']->iProductId);
                    // if(isset($ExistUpperStageOffcice) && !empty($ExistUpperStageOffcice) && count($ExistUpperStageOffcice)>0)
                    // {
                    //    $LowerStagePrice = $ExistUpperStageOffcice[0]->vOfficePrice;
                    // }
                    // else
                    // {
                    //     $LowerStagePrice = isset($LowerStageData->vPrice)?$LowerStageData->vPrice:'';
                    // }
                }

              
                if(isset($AllData['lower_product']->iGradeId) && !empty($AllData['lower_product']->iGradeId))
                {
                    $BillingLowerProductData['iStagePrice'] = Labcase::get_grade_price($AllData['lower_product']->iGradeId,$AllData['Slip']->iOfficeId,$AllData['lower_product']->iStageId)->vPrice;
                    if(isset($BillingLowerProductData['iStagePrice']) && $BillingLowerProductData['iStagePrice'] =='00')
                    {
                        $BillingLowerProductData['iStagePrice'] = isset($StageData->vPrice)?$StageData->vPrice:'';
                    }
                }
                else
                {
                    $BillingLowerProductData['iStagePrice'] = isset($StageData->vPrice)?$StageData->vPrice:'';
                }
                $BillingLowerProductData['vGradeName'] = isset($AllData['lower_product']->vGradeName)?$AllData['lower_product']->vGradeName:'';
                $BillingLowerProductData['vStageName'] = isset($LowerStageData->vName)?$LowerStageData->vName:'';
                $BillingLowerProductData['iToothBrandId'] = isset($AllData['lower_product']->iToothBrandId)?$AllData['lower_product']->iToothBrandId:'';
                if(isset($AllData['lower_product']->iToothBrandId) && !empty($AllData['lower_product']->iToothBrandId))
                {
                    $toothBranData = ToothBrand::get_by_id($AllData['lower_product']->iToothBrandId);
                }
                $BillingLowerProductData['vToothBrandName'] = isset($toothBranData->vName)?$toothBranData->vName:'';
                $BillingLowerProductData['iToothShadesId'] = isset($AllData['lower_product']->iToothShadesId)?$AllData['lower_product']->iToothShadesId:'';
                if(isset($AllData['lower_product']->iToothShadesId) && !empty($AllData['lower_product']->iToothShadesId))
                {
                    $toothShadesData = ToothShades::get_by_id($AllData['lower_product']->iToothShadesId);
                }
                $BillingLowerProductData['vToothShadesName'] = isset($toothShadesData->vToothShade)?$toothShadesData->vToothShade:'';
                $BillingLowerProductData['vtoothShadesCode'] = isset($toothShadesData->vCode)?$toothShadesData->vCode:'';
                $BillingLowerProductData['iGumBrandId'] = isset($AllData['lower_product']->iGumBrandId)?$AllData['lower_product']->iGumBrandId:'';
                if(isset($AllData['lower_product']->iGumBrandId) && !empty($AllData['lower_product']->iGumBrandId))
                {
                    $GumBrandData = GumBrand::get_by_id($AllData['lower_product']->iGumBrandId);
                }
                $BillingLowerProductData['vGumBrandName'] = isset($GumBrandData->vName)?$GumBrandData->vName:'';
                $BillingLowerProductData['iGumShadesId'] = $AllData['lower_product']->iGumShadesId;
                if(isset($AllData['lower_product']->iGumShadesId) && !empty($AllData['lower_product']->iGumShadesId))
                {
                    $GumShadesData = GumShades::get_by_id($AllData['lower_product']->iGumShadesId);
                }
                $BillingLowerProductData['vGumShadesCode'] = isset($GumShadesData->vCode)?$GumShadesData->vCode:'';
                $BillingLowerProductData['vGumShadesName'] = isset($GumShadesData->vGumShade)?$GumShadesData->vGumShade:'';
                $BillingLowerProductData['eRushDate'] = isset($AllData['lower_product']->eRushDate)?$AllData['lower_product']->eRushDate:'';
                $BillingLowerProductData['iFeesId'] = $AllData['lower_product']->iFeesId;
                if(isset($AllData['lower_product']->iFeesId) && !empty($AllData['lower_product']->iFeesId))
                {
                    $FeesDataLower = Fees::get_by_id($AllData['lower_product']->iFeesId);
                    $LowerStagePrice = isset($LowerStagePrice)?$LowerStagePrice:0;
                    // $vLowerRushFees = isset($FeesDataLower->vRushFee)?$FeesDataLower->vRushFee:0;
                    // New calculation rush fees start
                    $percentFeesLower = 1;
                    if(isset($FeesDataLower->vFee) && !empty($FeesDataLower->vFee))
                    {
                        $vLowerRushFees = $LowerStagePrice*$FeesDataLower->vFee/100;
                    }
                    // New calculation rush fees end
                    $BillingUpperProductData['iStageTotalWithRush'] = $LowerStagePrice+$vLowerRushFees;
                }
                $BillingLowerProductData['iRushDay'] = isset($FeesDataLower->iWorkingTime)?$FeesDataLower->iWorkingTime:'';
                $BillingLowerProductData['iRushFeesAmount'] = isset($FeesDataLower->vRushFee)?$FeesDataLower->vRushFee:'';
                $BillingLowerProductData['iRushFeesPercentage'] = isset($FeesDataLower->vFee)?$FeesDataLower->vFee:'';
                if(isset($FeesDataLower->vFee) && !empty($FeesDataLower->vFee) && isset($LowerStagePrice) && !empty($LowerStagePrice))
                {
                    $BillingLowerProductData['iTotalWithoutAddOnsAmount'] = $FeesDataLower->vFee + $LowerStagePrice;
                }
                else if(isset($FeesDataLower->vFee) && !empty($FeesDataLower->vFee))
                {
                    $BillingLowerProductData['iTotalWithoutAddOnsAmount'] = $FeesDataLower->vFee;
                }
                else if(isset($LowerStagePrice) && !empty($LowerStagePrice))
                {
                    $BillingLowerProductData['iTotalWithoutAddOnsAmount'] = $LowerStagePrice;
                }
                else
                {
                    $BillingLowerProductData['iTotalWithoutAddOnsAmount'] = 0;
                }
                $iTotalLowerWithoutAddOnsAmount =$BillingLowerProductData['iTotalWithoutAddOnsAmount'];
                $BillingLowerProductData['iToalAddOnsValue'] = '';
                $BillingLowerProductData['dDeliveryDate'] = isset($AllData['lower_product']->dDeliveryDate)?$AllData['lower_product']->dDeliveryDate:'';
                $BillingLowerProductData['tDeliveryTime'] = isset($AllData['lower_product']->tDeliveryTime)?$AllData['lower_product']->tDeliveryTime:'';
                $BillingLowerProductData['dtAddedDate'] = date("Y-m-d H:i:s");
                $BillingLowerProductData['dtUpdatedDate'] = date("Y-m-d H:i:s");
                
                $iBillingLowerProductId = BillingProduct::add_billing_product($BillingLowerProductData);
            }
            // Add on product table end
            // Add on billing addons table start
            if(isset($AllData['upper_addons']) && !empty($AllData['upper_addons']))
            {
                $iUpperAddOnsTotal = 0;
                foreach($AllData['upper_addons'] as $upper_addons_val )
                {
                    $BillingAddonsData['iBillingId'] = $iBillingId;
                    $BillingAddonsData['iBillingProductId'] = isset($iBillingUpperProductId)?$iBillingUpperProductId:'';
                    $BillingAddonsData['iAddCategoryId'] = isset($upper_addons_val->iAddCategoryId)?$upper_addons_val->iAddCategoryId:'';
                    $BillingAddonsData['vAddCategoryName'] = isset($upper_addons_val->vAddonCategoryName)?$upper_addons_val->vAddonCategoryName:'';
                    $BillingAddonsData['iSlipAddonsId'] = isset($upper_addons_val->iSlipAddonsId)?$upper_addons_val->iSlipAddonsId:'';
                    $BillingAddonsData['vSlipAddonsName'] = isset($upper_addons_val->vAddonName)?$upper_addons_val->vAddonName:'';
                    $BillingAddonsData['iAddOnsQty'] =isset($upper_addons_val->iQuantity)?$upper_addons_val->iQuantity:'';

                    if(isset($upper_addons_val->iSubAddCategoryId) && $upper_addons_val->iAddCategoryId)
                    {
                        $SubaddoncategoryData = Subaddoncategory::get_by_id($upper_addons_val->iSubAddCategoryId);
                        // Check price exist in office price
                        $iLoginAdminId = General::admin_info()['iCustomerId'];
                        $ExistUpperOffcice = OfficePrices::checkExistOffice($iLoginAdminId,$upper_addons_val->iSubAddCategoryId);
                        if(isset($ExistUpperOffcice) && !empty($ExistUpperOffcice) && count($ExistUpperOffcice)>0)
                        {
                            $BillingAddonsData['iAddonsPrice'] = $ExistUpperOffcice[0]->vOfficePrice;
                        }
                        else
                        {
                            $BillingAddonsData['iAddonsPrice'] = isset($SubaddoncategoryData->iPrice)?$SubaddoncategoryData->iPrice:'';
                        }
                        // Update total price start
                        if(isset($BillingAddonsData['iAddonsPrice']) && !empty($BillingAddonsData['iAddonsPrice']))
                        {
                            $iAddonsPrice = $BillingAddonsData['iAddonsPrice'];
                            if(isset($BillingAddonsData['iAddOnsQty']) && !empty($BillingAddonsData['iAddOnsQty']))
                            {
                                $BillingAddonsData['iAddOnsTotal'] = $iAddonsPrice*$BillingAddonsData['iAddOnsQty'];
                            }
                            else
                            {
                                $BillingAddonsData['iAddOnsTotal'] = $iAddonsPrice;
                            }
                        }
                        // Update total price end
                        $iUpperAddOnsTotal+= $BillingAddonsData['iAddOnsTotal'];
                        $BillingAddonsData['vAddonCode'] = $SubaddoncategoryData->vCode;
                    }
                    $BillingAddonsData['dtAddedDate'] = date("Y-m-d H:i:s");
                    $BillingAddonsData['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    $BillingAddonsId = BillingAddons::add_billing_addons($BillingAddonsData);
                }
            }
            if(isset($AllData['lower_addons']) && !empty($AllData['lower_addons']))
            {
                $iLowerAddOnsTotal = 0;
                foreach($AllData['lower_addons'] as $lower_addons_val )
                {
                    $BillingAddonsLowerData['iBillingId'] = $iBillingId;
                    $BillingAddonsLowerData['iBillingProductId'] = isset($iBillingLowerProductId)?$iBillingLowerProductId:'';
                    $BillingAddonsLowerData['iAddCategoryId'] = isset($lower_addons_val->iAddCategoryId)?$lower_addons_val->iAddCategoryId:'';
                    $BillingAddonsLowerData['vAddCategoryName'] = isset($lower_addons_val->vAddonCategoryName)?$lower_addons_val->vAddonCategoryName:'';
                    $BillingAddonsLowerData['iSlipAddonsId'] = isset($lower_addons_val->iSlipAddonsId)?$lower_addons_val->iSlipAddonsId:'';
                    $BillingAddonsLowerData['vSlipAddonsName'] = isset($lower_addons_val->vAddonName)?$lower_addons_val->vAddonName:'';
                    $BillingAddonsLowerData['iAddOnsQty'] =isset($lower_addons_val->iQuantity)?$lower_addons_val->iQuantity:'';

                    if(isset($lower_addons_val->iSubAddCategoryId) && $lower_addons_val->iAddCategoryId)
                    {
                        $SubaddoncategoryData = Subaddoncategory::get_by_id($lower_addons_val->iSubAddCategoryId);
                        // Check price exist in office price
                        $iLoginAdminId = General::admin_info()['iCustomerId'];
                        $ExistLowerOffcice = OfficePrices::checkExistOffice($iLoginAdminId,$lower_addons_val->iSubAddCategoryId);
                        if(isset($ExistLowerOffcice) && !empty($ExistLowerOffcice) && count($ExistLowerOffcice)>0)
                        {
                            $BillingAddonsLowerData['iAddonsPrice'] = $ExistLowerOffcice[0]->vOfficePrice;
                        }
                        else
                        {
                            $BillingAddonsLowerData['iAddonsPrice'] = isset($SubaddoncategoryData->iPrice)?$SubaddoncategoryData->iPrice:'';
                        }
                        // Update total price start
                        if(isset($BillingAddonsLowerData['iAddonsPrice']) && !empty($BillingAddonsLowerData['iAddonsPrice']))
                        {
                            $iAddonsPrice = $BillingAddonsLowerData['iAddonsPrice'];
                            if(isset($BillingAddonsLowerData['iAddOnsQty']) && !empty($BillingAddonsLowerData['iAddOnsQty']))
                            {
                                $BillingAddonsLowerData['iAddOnsTotal'] = $iAddonsPrice*$BillingAddonsLowerData['iAddOnsQty'];
                            }
                            else
                            {
                                $BillingAddonsLowerData['iAddOnsTotal'] = $iAddonsPrice;
                            }
                        }
                        $iLowerAddOnsTotal+= $BillingAddonsLowerData['iAddOnsTotal'];
                        $BillingAddonsLowerData['vAddonCode'] = $SubaddoncategoryData->vCode;
                        // Update total price end
                    }
                    $BillingAddonsLowerData['dtAddedDate'] = date("Y-m-d H:i:s");
                    $BillingAddonsLowerData['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    $BillingAddonsId = BillingAddons::add_billing_addons($BillingAddonsLowerData);
                }
            }
            // Add on billing addons table end

            // Update billing product total addons price start
            $dataUpperBillingProduct['iToalAddOnsValue']= 0;
            $dataLowerBillingProduct['iToalAddOnsValue']= 0;
            if(isset($iBillingUpperProductId) && !empty($iBillingUpperProductId) )
            {
                $dataUpperBillingProduct['iToalAddOnsValue'] = $iUpperAddOnsTotal+$iTotalUpperWithoutAddOnsAmount;
                $whereUpperBilingProduct['iBillingProductId'] = $iBillingUpperProductId;
                BillingProduct::update_billing_product($whereUpperBilingProduct,$dataUpperBillingProduct);
            }
            if(isset($iBillingLowerProductId) && !empty($iBillingLowerProductId))
            {
                $dataLowerBillingProduct['iToalAddOnsValue'] = $iLowerAddOnsTotal+$iTotalLowerWithoutAddOnsAmount;
                $whereLowerBilingProduct['iBillingProductId'] = $iBillingLowerProductId;
                BillingProduct::update_billing_product($whereLowerBilingProduct,$dataLowerBillingProduct);
            }
            // Update billing product total addons price end

            // Update Billing Price start
            $data_billing['iUpperTotal'] = $dataUpperBillingProduct['iToalAddOnsValue'];
            $data_billing['iLowerTotal'] = $dataLowerBillingProduct['iToalAddOnsValue'];
            $data_billing['iMainTotal'] = $dataUpperBillingProduct['iToalAddOnsValue']+$dataLowerBillingProduct['iToalAddOnsValue'];
            $where_billing['iBillingId'] = $iBillingId;
            Labcase::update_billing($where_billing,$data_billing);
            // Update Billing Price end
           
        }
        // Check entry exist on billing end
    }
    public function update_billing_ref(Request $request)
    {
        // Check entry exist on billing start
        $iCaseId = $request->iCaseId;
        $iSlipId = $request->iSlipId;
        $exist_slip = Labcase::exist_slip_in_billing($iCaseId,$iSlipId);
        
        if($exist_slip > 0)
        {
            $this->undo_status_billing($iCaseId,$iSlipId);

            $BillingData = array();
            $AllData = $this->fetch_slip_data($iCaseId,$iSlipId);
            $DoctorData = Doctor::get_by_id($AllData['Slip']->iDoctorId);
            // Add on billing table start
            $BillingData['iCaseId'] = $iCaseId;
            $BillingData['iSlipId'] = $iSlipId;
            $BillingData['vSlipNumber'] = isset($AllData['Slip']->vSlipNumber)?$AllData['Slip']->vSlipNumber:'';
            $BillingData['vCaseNumber'] = isset($AllData['Case']->vCaseNumber)?$AllData['Case']->vCaseNumber:'';
            $BillingData['iOfficeId'] = isset($AllData['Slip']->iOfficeId)?$AllData['Slip']->iOfficeId:'';
            $BillingData['vOfficeName'] = isset($AllData['CustomerOfficeData']->vOfficeName)?$AllData['CustomerOfficeData']->vOfficeName:'';
            $BillingData['vOfficeCode'] = isset($AllData['CustomerOfficeData']->vOfficeName)?$AllData['CustomerOfficeData']->vOfficeName:'';
            $BillingData['iLabId'] = isset($AllData['CustomerLabData']->iCustomerId)?$AllData['CustomerLabData']->iCustomerId:'';
            $BillingData['vLabName'] = isset($AllData['CustomerLabData']->vOfficeName)?$AllData['CustomerLabData']->vOfficeName:'';
            $BillingData['iDoctorId'] = isset($AllData['Slip']->iDoctorId)?$AllData['Slip']->iDoctorId:'';
            $BillingData['vDoctorName'] = isset($DoctorData->vFirstName)?$DoctorData->vFirstName.' '.$DoctorData->vLastName:'';
            $BillingData['vPatientName'] = isset($AllData['Slip']->vPatientName)?$AllData['Slip']->vPatientName:'';
            $BillingData['vCasePanNumber'] = isset($AllData['Slip']->vCasePanNumber)?$AllData['Slip']->vCasePanNumber:'';
            $BillingData['dDeliveryDate'] = isset($AllData['Slip']->dDeliveryDate)?$AllData['Slip']->dDeliveryDate:'';
            $BillingData['eStatus'] = 'UnPaid';
            $BillingData['dtAddedDate'] = date("Y-m-d H:i:s");
            $BillingData['dtUpdatedDate'] = date("Y-m-d H:i:s");
            $iBillingId = Labcase::add_billing($BillingData);
            // Add on billing table end
            // Add on product table start
            $BillingUpperProductData = array();
            if(isset($AllData['upper_product']->iProductId) && !empty($AllData['upper_product']->iProductId))
            {
                $BillingUpperProductData['iBillingId'] = $iBillingId;
                $BillingUpperProductData['eType'] = "Upper";
                $BillingUpperProductData['iCategoryId'] = isset($AllData['upper_product']->iCategoryId)?$AllData['upper_product']->iCategoryId:'';
                $BillingUpperProductData['vCategoryName'] = isset($AllData['upper_product']->vCategoryName)?$AllData['upper_product']->vCategoryName:'';
                $BillingUpperProductData['iProductId'] = isset($AllData['upper_product']->iProductId)?$AllData['upper_product']->iProductId:'';
                $BillingUpperProductData['vProductName'] = isset($AllData['upper_product']->vProductName)?$AllData['upper_product']->vProductName:'';
                $BillingUpperProductData['iStageId'] = isset($AllData['upper_product']->iStageId)?$AllData['upper_product']->iStageId:'';
                if(isset($AllData['upper_product']->iStageId) && !empty($AllData['upper_product']->iStageId))
                {
                    $ProductUpperStageData = ProductStage::get_by_id($AllData['upper_product']->iProductId);
                    $BillingUpperProductData['vStageCode'] = isset($ProductUpperStageData->vCode)?$ProductUpperStageData->vCode:'';
                }
                if(isset($AllData['upper_product']->iProductId) && !empty($AllData['upper_product']->iProductId))
                {
                    $CategoryProductData = CategoryProduct::get_by_id($AllData['upper_product']->iProductId);

                    $iLoginAdminId = General::admin_info()['iCustomerId'];
                    
                    // Office product price exist start grade vise
                    if(isset($AllData['upper_product']->iGradeId) && !empty($AllData['upper_product']->iGradeId) && $AllData['upper_product']->iGradeId != null)
                    {
                        $FetchProductGradeOfficePrice = OfficeProductPrice::FetchProductGradeOfficePrice($AllData['upper_product']->iProductId,$AllData['upper_product']->iGradeId);
                        
                        $FetchProductGradePrice = ProductGrade::FetchProductGradePrice($AllData['upper_product']->iProductId,$AllData['upper_product']->iGradeId);
                        
                        $FetchProductOfficePrice = OfficeProductPrice::FetchProductOfficePrice($AllData['upper_product']->iProductId);

                        
                        if(isset($FetchProductGradeOfficePrice->vPrice) && !empty($FetchProductGradeOfficePrice->vPrice) && $FetchProductGradeOfficePrice->vPrice != 00)
                        {
                           $CategoryProductPrice = $FetchProductGradeOfficePrice->vPrice;
                        }
                        else if(isset($FetchProductGradePrice->vPrice) && !empty($FetchProductGradePrice->vPrice) && $FetchProductGradePrice->vPrice != 00)
                        {
                           $CategoryProductPrice = $FetchProductGradePrice->vPrice;
                        }
                        else if(isset($FetchProductOfficePrice->vPrice) && !empty($FetchProductOfficePrice->vPrice) && $FetchProductOfficePrice->vPrice != 00)
                        {
                           $CategoryProductPrice = $FetchProductOfficePrice->vPrice;
                        }
                        else
                        {
                            $CategoryProductPrice = isset($CategoryProductData->vPrice)?$CategoryProductData->vPrice:'';
                        }
                    }
                    else
                    {
                        $FetchProductOfficePrice = OfficeProductPrice::FetchProductOfficePrice($AllData['upper_product']->iProductId);
                        if(isset($FetchProductOfficePrice->vPrice) && !empty($FetchProductOfficePrice->vPrice) && $FetchProductOfficePrice->vPrice != 00)
                        {
                           $CategoryProductPrice = $FetchProductOfficePrice->vPrice;
                        }
                        else
                        {
                            $CategoryProductPrice = isset($CategoryProductData->vPrice)?$CategoryProductData->vPrice:'';
                        }
                    }
                     // Office product price exist end grade vise

                    // $ExistUpperProductOffcice = OfficePrices::checkExistOffice($iLoginAdminId,$AllData['upper_product']->iProductId);
                    // if(isset($ExistUpperProductOffcice) && !empty($ExistUpperProductOffcice) && count($ExistUpperProductOffcice)>0)
                    // {
                    //    $CategoryProductPrice = $ExistUpperProductOffcice[0]->vOfficePrice;
                    // }
                    // else
                    // {
                    //     $CategoryProductPrice = isset($CategoryProductData->vPrice)?$CategoryProductData->vPrice:'';
                    // }

                }
                $BillingUpperProductData['iProductPrice'] = isset($CategoryProductPrice)?$CategoryProductPrice:'';
                if(isset($AllData['upper_product']->iStageId) && !empty($AllData['upper_product']->iStageId))
                {
                    $StageData = ProductStage::get_by_id($AllData['upper_product']->iStageId);
               
                    $iLoginAdminId = General::admin_info()['iCustomerId'];

                    // check stage grade price start
                    if(isset($AllData['upper_product']->iGradeId) && !empty($AllData['upper_product']->iGradeId) && $AllData['upper_product']->iGradeId != null)
                    {
                        $FetchStageGradeOfficePrice = OfficeStagePrice::FetchStageGradeOfficePrice($AllData['upper_product']->iProductId,$AllData['upper_product']->iGradeId,$AllData['upper_product']->iStageId);
                        
                        $FetchStageGradePrice = StageGrade::FetchStageGradePrice($AllData['upper_product']->iStageId,$AllData['upper_product']->iGradeId);
                        
                        $FetchStageOfficePrice = OfficeStagePrice::FetchStageOfficePrice($AllData['upper_product']->iProductId,$AllData['upper_product']->iStageId);

                        
                        if(isset($FetchStageGradeOfficePrice->vPrice) && !empty($FetchStageGradeOfficePrice->vPrice) && $FetchStageGradeOfficePrice->vPrice != 00)
                        {
                           $CategoryStagePrice = $FetchStageGradeOfficePrice->vPrice;
                        }
                        else if(isset($FetchStageGradePrice->vPrice) && !empty($FetchStageGradePrice->vPrice) && $FetchStageGradePrice->vPrice != 00)
                        {
                           $CategoryStagePrice = $FetchStageGradePrice->vPrice;
                        }
                        else if(isset($FetchStageOfficePrice->vPrice) && !empty($FetchStageOfficePrice->vPrice) && $FetchStageOfficePrice->vPrice != 00)
                        {
                           $CategoryStagePrice = $FetchStageOfficePrice->vPrice;
                        }
                        else
                        {
                            $CategoryStagePrice = isset($StageData->vPrice)?$StageData->vPrice:'';
                        }
                    }
                    else
                    {
                        $FetchProductOfficePrice = OfficeProductPrice::FetchProductOfficePrice($AllData['upper_product']->iProductId);
                        if(isset($FetchProductOfficePrice->vPrice) && !empty($FetchProductOfficePrice->vPrice) && $FetchProductOfficePrice->vPrice != 00)
                        {
                            $CategoryProductPrice = $FetchProductOfficePrice->vPrice;
                        }
                        else
                        {
                            $CategoryProductPrice = isset($StageData->vPrice)?$StageData->vPrice:'';
                        }
                    }
                  
                    // check stage grade price end

                    // $ExistUpperStageOffcice = OfficePrices::checkExistOffice($iLoginAdminId,$AllData['upper_product']->iStageId);
                    // if(isset($ExistUpperStageOffcice) && !empty($ExistUpperStageOffcice) && count($ExistUpperStageOffcice)>0)
                    // {
                    //    $CategoryStagePrice = $ExistUpperStageOffcice[0]->vOfficePrice;
                    // }
                    // else
                    // {
                    //     $CategoryStagePrice = isset($StageData->vPrice)?$StageData->vPrice:'';
                    // }
                }
              
                if(isset($AllData['upper_product']->iGradeId) && !empty($AllData['upper_product']->iGradeId))
                {
                    $BillingUpperProductData['iStagePrice'] = Labcase::get_grade_price($AllData['upper_product']->iGradeId,$AllData['Slip']->iOfficeId,$AllData['upper_product']->iStageId)->vPrice;
                   
                    if(isset($BillingUpperProductData['iStagePrice']) && $BillingUpperProductData['iStagePrice'] == '00')
                    {
                        $BillingUpperProductData['iStagePrice'] = isset($StageData->vPrice)?$StageData->vPrice:'';
                    }
                  
                }
                else
                {
                    $BillingUpperProductData['iStagePrice'] = isset($StageData->vPrice)?$StageData->vPrice:'';
                }
                $BillingUpperProductData['vGradeName'] = isset($AllData['upper_product']->vGradeName)?$AllData['upper_product']->vGradeName:'';
                $BillingUpperProductData['vStageName'] = isset($StageData->vName)?$StageData->vName:'';
                $BillingUpperProductData['iToothBrandId'] = isset($AllData['upper_product']->iToothBrandId)?$AllData['upper_product']->iToothBrandId:'';
                if(isset($AllData['upper_product']->iToothBrandId) && !empty($AllData['upper_product']->iToothBrandId))
                {
                    $toothBranData = ToothBrand::get_by_id($AllData['upper_product']->iToothBrandId);
                }
                $BillingUpperProductData['vToothBrandName'] = isset($toothBranData->vName)?$toothBranData->vName:'';
                $BillingUpperProductData['iToothShadesId'] = isset($AllData['upper_product']->iToothShadesId)?$AllData['upper_product']->iToothShadesId:'';
                if(isset($AllData['upper_product']->iToothShadesId) && !empty($AllData['upper_product']->iToothShadesId))
                {
                    $toothShadesData = ToothShades::get_by_id($AllData['upper_product']->iToothShadesId);
                }
                $BillingUpperProductData['vtoothShadesCode'] = isset($toothShadesData->vCode)?$toothShadesData->vCode:'';
                $BillingUpperProductData['vToothShadesName'] = isset($toothShadesData->vToothShade)?$toothShadesData->vToothShade:'';
                $BillingUpperProductData['iGumBrandId'] = isset($AllData['upper_product']->iGumBrandId)?$AllData['upper_product']->iGumBrandId:'';
                if(isset($AllData['upper_product']->iGumBrandId) && !empty($AllData['upper_product']->iGumBrandId))
                {
                    $GumBrandData = GumBrand::get_by_id($AllData['upper_product']->iGumBrandId);
                }
                $BillingUpperProductData['vGumBrandName'] = isset($GumBrandData->vName)?$GumBrandData->vName:'';
                $BillingUpperProductData['iGumShadesId'] = $AllData['upper_product']->iGumShadesId;
                if(isset($AllData['upper_product']->iGumShadesId) && !empty($AllData['upper_product']->iGumShadesId))
                {
                    $GumShadesData = GumShades::get_by_id($AllData['upper_product']->iGumShadesId);
                }
                $BillingUpperProductData['vGumShadesName'] = isset($GumShadesData->vGumShade)?$GumShadesData->vGumShade:'';
                $BillingUpperProductData['vGumShadesCode'] = isset($GumShadesData->vCode)?$GumShadesData->vCode:'';
                $BillingUpperProductData['eRushDate'] = isset($AllData['upper_product']->eRushDate)?$AllData['upper_product']->eRushDate:'';
                $BillingUpperProductData['iFeesId'] = $AllData['upper_product']->iFeesId;
                if(isset($AllData['upper_product']->iFeesId) && !empty($AllData['upper_product']->iFeesId))
                {
                    $FeesData = Fees::get_by_id($AllData['upper_product']->iFeesId);
                    $CategoryStagePrice = isset($CategoryStagePrice)?$CategoryStagePrice:0;
                    // $vUpperRushFees = isset($FeesData->vRushFee)?$FeesData->vRushFee:0;
                    // New calculation rush fees start
                    $percentFeesUpper = 1;
                   
                    if(isset($FeesData->vFee) && !empty($FeesData->vFee))
                    {
                        $vUpperRushFees = $CategoryStagePrice * $FeesData->vFee/100;
                    }
                    // New calculation rush fees end
                    $BillingUpperProductData['iStageTotalWithRush'] = $CategoryStagePrice+$vUpperRushFees;
                }
                $BillingUpperProductData['iRushDay'] = isset($FeesData->iWorkingTime)?$FeesData->iWorkingTime:'';
                $BillingUpperProductData['iRushFeesAmount'] = isset($FeesData->vRushFee)?$FeesData->vRushFee:'';
                $BillingUpperProductData['iRushFeesPercentage'] = isset($FeesData->vFee)?$FeesData->vFee:'';
                if(isset($FeesData->vFee) && !empty($FeesData->vFee) && isset($CategoryStagePrice) && !empty($CategoryStagePrice))
                {
                    $BillingUpperProductData['iTotalWithoutAddOnsAmount'] = $FeesData->vFee + $CategoryStagePrice;
                }
                else if(isset($FeesData->vFee) && !empty($FeesData->vFee))
                {
                    $BillingUpperProductData['iTotalWithoutAddOnsAmount'] = $FeesData->vFee;
                }
                else if(isset($CategoryStagePrice) && !empty($CategoryStagePrice))
                {
                    $BillingUpperProductData['iTotalWithoutAddOnsAmount'] = $CategoryStagePrice;
                }
                else
                {
                    $BillingUpperProductData['iTotalWithoutAddOnsAmount'] = 0;
                }
                $iTotalUpperWithoutAddOnsAmount =$BillingUpperProductData['iTotalWithoutAddOnsAmount'];
                $BillingUpperProductData['iToalAddOnsValue'] = '';
                $BillingUpperProductData['dDeliveryDate'] = isset($AllData['upper_product']->dDeliveryDate)?$AllData['upper_product']->dDeliveryDate:'';
                $BillingUpperProductData['tDeliveryTime'] = isset($AllData['upper_product']->tDeliveryTime)?$AllData['upper_product']->tDeliveryTime:'';
                $BillingUpperProductData['dtAddedDate'] = date("Y-m-d H:i:s");
                $BillingUpperProductData['dtUpdatedDate'] = date("Y-m-d H:i:s");
                
                $iBillingUpperProductId = BillingProduct::add_billing_product($BillingUpperProductData);
            }

            if(isset($AllData['lower_product']->iProductId) && !empty($AllData['lower_product']->iProductId))
            {
                $BillingLowerProductData['iBillingId'] = $iBillingId;
                $BillingLowerProductData['eType'] = "Lower";
                $BillingLowerProductData['iCategoryId'] = isset($AllData['lower_product']->iCategoryId)?$AllData['lower_product']->iCategoryId:'';
                $BillingLowerProductData['vCategoryName'] = isset($AllData['lower_product']->vCategoryName)?$AllData['lower_product']->vCategoryName:'';
                $BillingLowerProductData['iProductId'] = isset($AllData['lower_product']->iProductId)?$AllData['lower_product']->iProductId:'';
                $BillingLowerProductData['vProductName'] = isset($AllData['lower_product']->vProductName)?$AllData['lower_product']->vProductName:'';
                $BillingLowerProductData['iStageId'] = isset($AllData['lower_product']->iStageId)?$AllData['lower_product']->iStageId:'';
                if(isset($AllData['lower_product']->iStageId) && !empty($AllData['lower_product']->iStageId))
                {
                    $ProductLowerStageData = ProductStage::get_by_id($AllData['lower_product']->iProductId);
                    $BillingLowerProductData['vStageCode'] = isset($ProductLowerStageData->vCode)?$ProductLowerStageData->vCode:'';
                }
                if(isset($AllData['lower_product']->iProductId) && !empty($AllData['lower_product']->iProductId))
                {

                    $CategoryLowerProductData = CategoryProduct::get_by_id($AllData['lower_product']->iProductId);
                    $iLoginAdminId = General::admin_info()['iCustomerId'];

                    // Office product price exist start grade vise
                    if(isset($AllData['lower_product']->iGradeId) && !empty($AllData['lower_product']->iGradeId) && $AllData['lower_product']->iGradeId != null)
                    {
                        $FetchProductGradeOfficePrice = OfficeProductPrice::FetchProductGradeOfficePrice($AllData['lower_product']->iProductId,$AllData['lower_product']->iGradeId);
                        
                        $FetchProductGradePrice = ProductGrade::FetchProductGradePrice($AllData['lower_product']->iProductId,$AllData['lower_product']->iGradeId);
                        
                        $FetchProductOfficePrice = OfficeProductPrice::FetchProductOfficePrice($AllData['lower_product']->iProductId);

                        
                        if(isset($FetchProductGradeOfficePrice->vPrice) && !empty($FetchProductGradeOfficePrice->vPrice) && $FetchProductGradeOfficePrice->vPrice != 00)
                        {
                           $CategoryLowerProductPrice = $FetchProductGradeOfficePrice->vPrice;
                        }
                        else if(isset($FetchProductGradePrice->vPrice) && !empty($FetchProductGradePrice->vPrice) && $FetchProductGradePrice->vPrice != 00)
                        {
                           $CategoryLowerProductPrice = $FetchProductGradePrice->vPrice;
                        }
                        else if(isset($FetchProductOfficePrice->vPrice) && !empty($FetchProductOfficePrice->vPrice) && $FetchProductOfficePrice->vPrice != 00)
                        {
                           $CategoryLowerProductPrice = $FetchProductOfficePrice->vPrice;
                        }
                        else
                        {
                            $CategoryLowerProductPrice = isset($CategoryLowerProductData->vPrice)?$CategoryLowerProductData->vPrice:'';
                        }
                    }
                    else
                    {
                        $FetchProductOfficePrice = OfficeProductPrice::FetchProductOfficePrice($AllData['lower_product']->iProductId);
                        if(isset($FetchProductOfficePrice->vPrice) && !empty($FetchProductOfficePrice->vPrice) && $FetchProductOfficePrice->vPrice != 00)
                        {
                           $CategoryLowerProductPrice = $FetchProductOfficePrice->vPrice;
                        }
                        else
                        {
                            $CategoryLowerProductPrice = isset($CategoryLowerProductData->vPrice)?$CategoryLowerProductData->vPrice:'';
                        }
                    }
                     // Office product price exist end grade vise

                    // $ExistUpperStageOffcice = OfficePrices::checkExistOffice($iLoginAdminId,$AllData['lower_product']->iProductId);
                    // if(isset($ExistUpperStageOffcice) && !empty($ExistUpperStageOffcice) && count($ExistUpperStageOffcice)>0)
                    // {
                    //    $CategoryLowerStagePrice = $ExistUpperStageOffcice[0]->vOfficePrice;
                    // }
                    // else
                    // {
                    //     $CategoryLowerStagePrice = isset($CategoryLowerProductData->vPrice)?$CategoryLowerProductData->vPrice:'';
                    // }
                }
                $BillingLowerProductData['iProductPrice'] = isset($CategoryLowerProductPrice)?$CategoryLowerProductPrice :'';
                if(isset($AllData['lower_product']->iStageId) && !empty($AllData['lower_product']->iStageId))
                {
                    $LowerStageData = ProductStage::get_by_id($AllData['lower_product']->iStageId);
                    $iLoginAdminId = General::admin_info()['iCustomerId'];

                     // check stage grade price start
                     if(isset($AllData['lower_product']->iGradeId) && !empty($AllData['lower_product']->iGradeId) && $AllData['lower_product']->iGradeId != null)
                     {
                         $FetchStageGradeOfficePrice = OfficeStagePrice::FetchStageGradeOfficePrice($AllData['lower_product']->iProductId,$AllData['lower_product']->iGradeId,$AllData['lower_product']->iStageId);
                         
                         $FetchStageGradePrice = StageGrade::FetchStageGradePrice($AllData['lower_product']->iStageId,$AllData['lower_product']->iGradeId);
                         
                         $FetchStageOfficePrice = OfficeStagePrice::FetchStageOfficePrice($AllData['lower_product']->iProductId,$AllData['lower_product']->iStageId);
 
                         
                         if(isset($FetchStageGradeOfficePrice->vPrice) && !empty($FetchStageGradeOfficePrice->vPrice) && $FetchStageGradeOfficePrice->vPrice != 00)
                         {
                            $LowerStagePrice = $FetchStageGradeOfficePrice->vPrice;
                         }
                         else if(isset($FetchStageGradePrice->vPrice) && !empty($FetchStageGradePrice->vPrice) && $FetchStageGradePrice->vPrice != 00)
                         {
                            $LowerStagePrice = $FetchStageGradePrice->vPrice;
                         }
                         else if(isset($FetchStageOfficePrice->vPrice) && !empty($FetchStageOfficePrice->vPrice) && $FetchStageOfficePrice->vPrice != 00)
                         {
                            $LowerStagePrice = $FetchStageOfficePrice->vPrice;
                         }
                         else
                         {
                             $LowerStagePrice = isset($StageData->vPrice)?$StageData->vPrice:'';
                         }
                     }
                     else
                     {
                         $FetchProductOfficePrice = OfficeProductPrice::FetchProductOfficePrice($AllData['upper_product']->iProductId);
                         if(isset($FetchProductOfficePrice->vPrice) && !empty($FetchProductOfficePrice->vPrice) && $FetchProductOfficePrice->vPrice != 00)
                         {
                             $LowerStagePrice = $FetchProductOfficePrice->vPrice;
                         }
                         else
                         {
                             $LowerStagePrice = isset($StageData->vPrice)?$StageData->vPrice:'';
                         }
                     }
                     // check stage grade price end

                    // $ExistUpperStageOffcice = OfficePrices::checkExistOffice($iLoginAdminId,$AllData['lower_product']->iProductId);
                    // if(isset($ExistUpperStageOffcice) && !empty($ExistUpperStageOffcice) && count($ExistUpperStageOffcice)>0)
                    // {
                    //    $LowerStagePrice = $ExistUpperStageOffcice[0]->vOfficePrice;
                    // }
                    // else
                    // {
                    //     $LowerStagePrice = isset($LowerStageData->vPrice)?$LowerStageData->vPrice:'';
                    // }
                }

              
                if(isset($AllData['lower_product']->iGradeId) && !empty($AllData['lower_product']->iGradeId))
                {
                    $BillingLowerProductData['iStagePrice'] = Labcase::get_grade_price($AllData['lower_product']->iGradeId,$AllData['Slip']->iOfficeId,$AllData['lower_product']->iStageId)->vPrice;
                    if(isset($BillingLowerProductData['iStagePrice']) && $BillingLowerProductData['iStagePrice'] =='00')
                    {
                        $BillingLowerProductData['iStagePrice'] = isset($StageData->vPrice)?$StageData->vPrice:'';
                    }
                }
                else
                {
                    $BillingLowerProductData['iStagePrice'] = isset($LowerStageData->vPrice)?$LowerStageData->vPrice:'';
                }
                $BillingLowerProductData['vGradeName'] = isset($AllData['lower_product']->vGradeName)?$AllData['lower_product']->vGradeName:'';
                $BillingLowerProductData['vStageName'] = isset($LowerStageData->vName)?$LowerStageData->vName:'';
                $BillingLowerProductData['iToothBrandId'] = isset($AllData['lower_product']->iToothBrandId)?$AllData['lower_product']->iToothBrandId:'';
                if(isset($AllData['lower_product']->iToothBrandId) && !empty($AllData['lower_product']->iToothBrandId))
                {
                    $toothBranData = ToothBrand::get_by_id($AllData['lower_product']->iToothBrandId);
                }
                $BillingLowerProductData['vToothBrandName'] = isset($toothBranData->vName)?$toothBranData->vName:'';
                $BillingLowerProductData['iToothShadesId'] = isset($AllData['lower_product']->iToothShadesId)?$AllData['lower_product']->iToothShadesId:'';
                if(isset($AllData['lower_product']->iToothShadesId) && !empty($AllData['lower_product']->iToothShadesId))
                {
                    $toothShadesData = ToothShades::get_by_id($AllData['lower_product']->iToothShadesId);
                }
                $BillingLowerProductData['vToothShadesName'] = isset($toothShadesData->vToothShade)?$toothShadesData->vToothShade:'';
                $BillingLowerProductData['vtoothShadesCode'] = isset($toothShadesData->vCode)?$toothShadesData->vCode:'';
                $BillingLowerProductData['iGumBrandId'] = isset($AllData['lower_product']->iGumBrandId)?$AllData['lower_product']->iGumBrandId:'';
                if(isset($AllData['lower_product']->iGumBrandId) && !empty($AllData['lower_product']->iGumBrandId))
                {
                    $GumBrandData = GumBrand::get_by_id($AllData['lower_product']->iGumBrandId);
                }
                $BillingLowerProductData['vGumBrandName'] = isset($GumBrandData->vName)?$GumBrandData->vName:'';
                $BillingLowerProductData['iGumShadesId'] = $AllData['lower_product']->iGumShadesId;
                if(isset($AllData['lower_product']->iGumShadesId) && !empty($AllData['lower_product']->iGumShadesId))
                {
                    $GumShadesData = GumShades::get_by_id($AllData['lower_product']->iGumShadesId);
                }
                $BillingLowerProductData['vGumShadesCode'] = isset($GumShadesData->vCode)?$GumShadesData->vCode:'';
                $BillingLowerProductData['vGumShadesName'] = isset($GumShadesData->vGumShade)?$GumShadesData->vGumShade:'';
                $BillingLowerProductData['eRushDate'] = isset($AllData['lower_product']->eRushDate)?$AllData['lower_product']->eRushDate:'';
                $BillingLowerProductData['iFeesId'] = $AllData['lower_product']->iFeesId;
                if(isset($AllData['lower_product']->iFeesId) && !empty($AllData['lower_product']->iFeesId))
                {
                    $FeesDataLower = Fees::get_by_id($AllData['lower_product']->iFeesId);
                    $LowerStagePrice = isset($LowerStagePrice)?$LowerStagePrice:0;
                    // $vLowerRushFees = isset($FeesDataLower->vRushFee)?$FeesDataLower->vRushFee:0;
                    // New calculation rush fees start
                    $percentFeesLower = 1;
                    if(isset($FeesDataLower->vFee) && !empty($FeesDataLower->vFee))
                    {
                        $vLowerRushFees = $LowerStagePrice*$FeesDataLower->vFee/100;
                    }
                    // New calculation rush fees end
                    $BillingUpperProductData['iStageTotalWithRush'] = $LowerStagePrice+$vLowerRushFees;
                }
                $BillingLowerProductData['iRushDay'] = isset($FeesDataLower->iWorkingTime)?$FeesDataLower->iWorkingTime:'';
                $BillingLowerProductData['iRushFeesAmount'] = isset($FeesDataLower->vRushFee)?$FeesDataLower->vRushFee:'';
                $BillingLowerProductData['iRushFeesPercentage'] = isset($FeesDataLower->vFee)?$FeesDataLower->vFee:'';
                if(isset($FeesDataLower->vFee) && !empty($FeesDataLower->vFee) && isset($LowerStagePrice) && !empty($LowerStagePrice))
                {
                    $BillingLowerProductData['iTotalWithoutAddOnsAmount'] = $FeesDataLower->vFee + $LowerStagePrice;
                }
                else if(isset($FeesDataLower->vFee) && !empty($FeesDataLower->vFee))
                {
                    $BillingLowerProductData['iTotalWithoutAddOnsAmount'] = $FeesDataLower->vFee;
                }
                else if(isset($LowerStagePrice) && !empty($LowerStagePrice))
                {
                    $BillingLowerProductData['iTotalWithoutAddOnsAmount'] = $LowerStagePrice;
                }
                else
                {
                    $BillingLowerProductData['iTotalWithoutAddOnsAmount'] = 0;
                }
                $iTotalLowerWithoutAddOnsAmount =$BillingLowerProductData['iTotalWithoutAddOnsAmount'];
                $BillingLowerProductData['iToalAddOnsValue'] = '';
                $BillingLowerProductData['dDeliveryDate'] = isset($AllData['lower_product']->dDeliveryDate)?$AllData['lower_product']->dDeliveryDate:'';
                $BillingLowerProductData['tDeliveryTime'] = isset($AllData['lower_product']->tDeliveryTime)?$AllData['lower_product']->tDeliveryTime:'';
                $BillingLowerProductData['dtAddedDate'] = date("Y-m-d H:i:s");
                $BillingLowerProductData['dtUpdatedDate'] = date("Y-m-d H:i:s");
                
                $iBillingLowerProductId = BillingProduct::add_billing_product($BillingLowerProductData);
            }
            // Add on product table end
            // Add on billing addons table start
            if(isset($AllData['upper_addons']) && !empty($AllData['upper_addons']))
            {
                $iUpperAddOnsTotal = 0;
                foreach($AllData['upper_addons'] as $upper_addons_val )
                {
                    $BillingAddonsData['iBillingId'] = $iBillingId;
                    $BillingAddonsData['iBillingProductId'] = isset($iBillingUpperProductId)?$iBillingUpperProductId:'';
                    $BillingAddonsData['iAddCategoryId'] = isset($upper_addons_val->iAddCategoryId)?$upper_addons_val->iAddCategoryId:'';
                    $BillingAddonsData['vAddCategoryName'] = isset($upper_addons_val->vAddonCategoryName)?$upper_addons_val->vAddonCategoryName:'';
                    $BillingAddonsData['iSlipAddonsId'] = isset($upper_addons_val->iSlipAddonsId)?$upper_addons_val->iSlipAddonsId:'';
                    $BillingAddonsData['vSlipAddonsName'] = isset($upper_addons_val->vAddonName)?$upper_addons_val->vAddonName:'';
                    $BillingAddonsData['iAddOnsQty'] =isset($upper_addons_val->iQuantity)?$upper_addons_val->iQuantity:'';

                    if(isset($upper_addons_val->iSubAddCategoryId) && $upper_addons_val->iAddCategoryId)
                    {
                        $SubaddoncategoryData = Subaddoncategory::get_by_id($upper_addons_val->iSubAddCategoryId);
                        // Check price exist in office price
                        $iLoginAdminId = General::admin_info()['iCustomerId'];
                        $ExistUpperOffcice = OfficePrices::checkExistOffice($iLoginAdminId,$upper_addons_val->iSubAddCategoryId);
                        if(isset($ExistUpperOffcice) && !empty($ExistUpperOffcice) && count($ExistUpperOffcice)>0)
                        {
                            $BillingAddonsData['iAddonsPrice'] = $ExistUpperOffcice[0]->vOfficePrice;
                        }
                        else
                        {
                            $BillingAddonsData['iAddonsPrice'] = isset($SubaddoncategoryData->iPrice)?$SubaddoncategoryData->iPrice:'';
                        }
                        // Update total price start
                        if(isset($BillingAddonsData['iAddonsPrice']) && !empty($BillingAddonsData['iAddonsPrice']))
                        {
                            $iAddonsPrice = $BillingAddonsData['iAddonsPrice'];
                            if(isset($BillingAddonsData['iAddOnsQty']) && !empty($BillingAddonsData['iAddOnsQty']))
                            {
                                $BillingAddonsData['iAddOnsTotal'] = $iAddonsPrice*$BillingAddonsData['iAddOnsQty'];
                            }
                            else
                            {
                                $BillingAddonsData['iAddOnsTotal'] = $iAddonsPrice;
                            }
                        }
                        // Update total price end
                        $iUpperAddOnsTotal+= $BillingAddonsData['iAddOnsTotal'];
                        $BillingAddonsData['vAddonCode'] = $SubaddoncategoryData->vCode;
                    }
                    $BillingAddonsData['dtAddedDate'] = date("Y-m-d H:i:s");
                    $BillingAddonsData['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    $BillingAddonsId = BillingAddons::add_billing_addons($BillingAddonsData);
                }
            }
            if(isset($AllData['lower_addons']) && !empty($AllData['lower_addons']))
            {
                $iLowerAddOnsTotal = 0;
                foreach($AllData['lower_addons'] as $lower_addons_val )
                {
                    $BillingAddonsLowerData['iBillingId'] = $iBillingId;
                    $BillingAddonsLowerData['iBillingProductId'] = isset($iBillingLowerProductId)?$iBillingLowerProductId:'';
                    $BillingAddonsLowerData['iAddCategoryId'] = isset($lower_addons_val->iAddCategoryId)?$lower_addons_val->iAddCategoryId:'';
                    $BillingAddonsLowerData['vAddCategoryName'] = isset($lower_addons_val->vAddonCategoryName)?$lower_addons_val->vAddonCategoryName:'';
                    $BillingAddonsLowerData['iSlipAddonsId'] = isset($lower_addons_val->iSlipAddonsId)?$lower_addons_val->iSlipAddonsId:'';
                    $BillingAddonsLowerData['vSlipAddonsName'] = isset($lower_addons_val->vAddonName)?$lower_addons_val->vAddonName:'';
                    $BillingAddonsLowerData['iAddOnsQty'] =isset($lower_addons_val->iQuantity)?$lower_addons_val->iQuantity:'';

                    if(isset($lower_addons_val->iSubAddCategoryId) && $lower_addons_val->iAddCategoryId)
                    {
                        $SubaddoncategoryData = Subaddoncategory::get_by_id($lower_addons_val->iSubAddCategoryId);
                        // Check price exist in office price
                        $iLoginAdminId = General::admin_info()['iCustomerId'];
                        $ExistLowerOffcice = OfficePrices::checkExistOffice($iLoginAdminId,$lower_addons_val->iSubAddCategoryId);
                        if(isset($ExistLowerOffcice) && !empty($ExistLowerOffcice) && count($ExistLowerOffcice)>0)
                        {
                            $BillingAddonsLowerData['iAddonsPrice'] = $ExistLowerOffcice[0]->vOfficePrice;
                        }
                        else
                        {
                            $BillingAddonsLowerData['iAddonsPrice'] = isset($SubaddoncategoryData->iPrice)?$SubaddoncategoryData->iPrice:'';
                        }
                        // Update total price start
                        if(isset($BillingAddonsLowerData['iAddonsPrice']) && !empty($BillingAddonsLowerData['iAddonsPrice']))
                        {
                            $iAddonsPrice = $BillingAddonsLowerData['iAddonsPrice'];
                            if(isset($BillingAddonsLowerData['iAddOnsQty']) && !empty($BillingAddonsLowerData['iAddOnsQty']))
                            {
                                $BillingAddonsLowerData['iAddOnsTotal'] = $iAddonsPrice*$BillingAddonsLowerData['iAddOnsQty'];
                            }
                            else
                            {
                                $BillingAddonsLowerData['iAddOnsTotal'] = $iAddonsPrice;
                            }
                        }
                        $iLowerAddOnsTotal+= $BillingAddonsLowerData['iAddOnsTotal'];
                        $BillingAddonsLowerData['vAddonCode'] = $SubaddoncategoryData->vCode;
                        // Update total price end
                    }
                    $BillingAddonsLowerData['dtAddedDate'] = date("Y-m-d H:i:s");
                    $BillingAddonsLowerData['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    $BillingAddonsId = BillingAddons::add_billing_addons($BillingAddonsLowerData);
                }
            }
            // Add on billing addons table end

            // Update billing product total addons price start
            $dataUpperBillingProduct['iToalAddOnsValue']= 0;
            $dataLowerBillingProduct['iToalAddOnsValue']= 0;
            if(isset($iBillingUpperProductId) && !empty($iBillingUpperProductId) )
            {
                $dataUpperBillingProduct['iToalAddOnsValue'] = $iUpperAddOnsTotal+$iTotalUpperWithoutAddOnsAmount;
                $whereUpperBilingProduct['iBillingProductId'] = $iBillingUpperProductId;
                BillingProduct::update_billing_product($whereUpperBilingProduct,$dataUpperBillingProduct);
            }
            if(isset($iBillingLowerProductId) && !empty($iBillingLowerProductId))
            {
                $dataLowerBillingProduct['iToalAddOnsValue'] = $iLowerAddOnsTotal+$iTotalLowerWithoutAddOnsAmount;
                $whereLowerBilingProduct['iBillingProductId'] = $iBillingLowerProductId;
                BillingProduct::update_billing_product($whereLowerBilingProduct,$dataLowerBillingProduct);
            }
            // Update billing product total addons price end

            // Update Billing Price start
            $data_billing['iUpperTotal'] = $dataUpperBillingProduct['iToalAddOnsValue'];
            $data_billing['iLowerTotal'] = $dataLowerBillingProduct['iToalAddOnsValue'];
            $data_billing['iMainTotal'] = $dataUpperBillingProduct['iToalAddOnsValue']+$dataLowerBillingProduct['iToalAddOnsValue'];
            $where_billing['iBillingId'] = $iBillingId;
            Labcase::update_billing($where_billing,$data_billing);
            // Update Billing Price end
            return 'success'; 
            
        }
        else
        {
            return 'failed';
        }
        // Check entry exist on billing end
    }

    public function fetch_slip_data($iCaseId,$iSlipId)
    {
        $data['Case'] = Labcase::get_by_id($iCaseId);
        $data['Slip'] = Slip::get_by_id($iSlipId);
     
        // For Security purpose start
        $iCustomerId = General::admin_info()['iCustomerId'];
        $customerType =General::admin_info()['customerType'];
        if(isset($customerType) && $customerType =='Office Admin')
        {
            if(isset($data['Slip']->iOfficeId) && !empty($data['Slip']->iOfficeId))
            {
                $MatchId = $data['Slip']->iOfficeId;
            }
            else
            {
                $MatchId = '';
            }
        }
        if(isset($customerType) && $customerType =='Lab Admin')
        {
            if(isset($data['Slip']->iLabId) && !empty($data['Slip']->iLabId))
            {
                $MatchId = $data['Slip']->iLabId;
            }
            else
            {
                $MatchId = '';
            }
        }
      
        if((isset($MatchId) && $MatchId != $iCustomerId && !empty($iCustomerId)) || (empty($MatchId)) || (empty($data['Case'])))
        {
            if(isset($customerType) && $customerType =='Office Admin')
            {
                return redirect()->route('admin.labcase.office_admin_listing')->withError('Invalid Input');
            }
            elseif(isset($customerType) && $customerType =='Lab Admin')
            {
                return redirect()->route('admin.labcase')->withError('Invalid Input');
            }
        }
        // For Security purpose end

        $iOfficeId = $data['Slip']->iOfficeId;
        $iLabId = $data['Slip']->iLabId;
        
      
        $data['CustomerOfficeData'] = Customer::get_by_office_id($iOfficeId);
        $data['CustomerLabData'] = Customer::get_by_office_id($iLabId);
        $data['upper_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Upper");
        $data['lower_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Lower");

        $data["rush"] = "No";

        if (!empty($data['upper_product'])) {

            $data['upper_vTeethInMouth'] = explode(",",$data['upper_product']->vTeethInMouth);
            $data['upper_vMissingTeeth'] = explode(",",$data['upper_product']->vMissingTeeth);
            $data['upper_vWillExtractOnDelivery'] = explode(",",$data['upper_product']->vWillExtractOnDelivery);
            $data['upper_vHasBeenExtracted'] = explode(",",$data['upper_product']->vHasBeenExtracted);
            $data['upper_vFixOrAdd'] = explode(",",$data['upper_product']->vFixOrAdd);
            $data['upper_vClasps'] = explode(",",$data['upper_product']->vClasps);

            $upper_teeth_in_mouth = array();
            $upper_extract= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

            $upper_merged_array = array_merge(
                $data['upper_vMissingTeeth'],
                $data['upper_vWillExtractOnDelivery'],
                $data['upper_vHasBeenExtracted'],
                $data['upper_vFixOrAdd'],
                $data['upper_vClasps']
            );

            foreach ($upper_extract as $value) {
                if (!in_array($value,$upper_merged_array)) {
                    $upper_teeth_in_mouth[] = $value;
                }
            }
            
            $data['upper_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['upper_vTeethInMouth'],
                    $upper_teeth_in_mouth
                )
            );

            $data['lower_vTeethInMouthOp'] = explode(",",$data['upper_product']->vTeethInMouthOp);
            $data['lower_vMissingTeethOp'] = explode(",",$data['upper_product']->vMissingTeethOp);
            $data['lower_vWillExtractOnDeliveryOp'] = explode(",",$data['upper_product']->vWillExtractOnDeliveryOp);
            $data['lower_vHasBeenExtractedOp'] = explode(",",$data['upper_product']->vHasBeenExtractedOp);
            $data['lower_vFixOrAddOp'] = explode(",",$data['upper_product']->vFixOrAddOp);
            $data['lower_vClaspsOp'] = explode(",",$data['upper_product']->vClaspsOp);

            if (!empty($data['lower_vTeethInMouthOp']) AND !empty($data['lower_vMissingTeethOp']) AND !empty($data['lower_vWillExtractOnDeliveryOp']) AND !empty($data['lower_vHasBeenExtractedOp']) AND !empty($data['lower_vFixOrAddOp']) AND !empty($data['lower_vClaspsOp'])) {

                $lower_teeth_in_mouthOp = array();
                $lower_extractOp= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");

                $lower_merged_arrayOp = array_merge(
                    $data['lower_vMissingTeethOp'],
                    $data['lower_vWillExtractOnDeliveryOp'],
                    $data['lower_vHasBeenExtractedOp'],
                    $data['lower_vFixOrAddOp'],
                    $data['lower_vClaspsOp']
                );

                foreach ($lower_extractOp as $value) {
                    if (!in_array($value,$lower_merged_arrayOp)) {
                        $lower_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['lower_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['lower_vTeethInMouthOp'],
                        $lower_teeth_in_mouthOp
                    )
                );
            }
            // dd($data['lower_vTeethInMouthOp']);

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['lower_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['lower_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['lower_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_addons'] = SlipAddons::get_by_iSlipId($criteria);

            $upper_delivery_date = $data['upper_product']->dDeliveryDate;

            
            if ($data['upper_product']->eRushDate == "Yes" AND !empty($data['upper_product']->eRushDate == "Yes")) {
                // if(!empty($data['lower_product'])){

                    $data["rush"] = "Yes";
                // }
            }
        }

        if (!empty($data['lower_product'])) {
            $data['lower_vTeethInMouth'] = explode(",",$data['lower_product']->vTeethInMouth);
            $data['lower_vMissingTeeth'] = explode(",",$data['lower_product']->vMissingTeeth);
            $data['lower_vWillExtractOnDelivery'] = explode(",",$data['lower_product']->vWillExtractOnDelivery);
            $data['lower_vHasBeenExtracted'] = explode(",",$data['lower_product']->vHasBeenExtracted);
            $data['lower_vFixOrAdd'] = explode(",",$data['lower_product']->vFixOrAdd);
            $data['lower_vClasps'] = explode(",",$data['lower_product']->vClasps);

            
            $teeth_in_mouth = array();
            $lower_extract= array("17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32");
            
            $merged_array = array_merge(
                $data['lower_vMissingTeeth'],
                $data['lower_vWillExtractOnDelivery'],
                $data['lower_vHasBeenExtracted'],
                $data['lower_vFixOrAdd'],
                $data['lower_vClasps']
            );
            
            foreach ($lower_extract as $value) {
                if (!in_array($value,$merged_array)) {
                    $teeth_in_mouth[] = $value;
                }
            }

            $data['lower_vTeethInMouth'] = array_unique(
                array_merge(
                    $data['lower_vTeethInMouth'],
                    $teeth_in_mouth
                )
            );


            $data['upper_vTeethInMouthOp'] = explode(",",$data['lower_product']->vTeethInMouthOp);
            $data['upper_vMissingTeethOp'] = explode(",",$data['lower_product']->vMissingTeethOp);
            $data['upper_vWillExtractOnDeliveryOp'] = explode(",",$data['lower_product']->vWillExtractOnDeliveryOp);
            $data['upper_vHasBeenExtractedOp'] = explode(",",$data['lower_product']->vHasBeenExtractedOp);
            $data['upper_vFixOrAddOp'] = explode(",",$data['lower_product']->vFixOrAddOp);
            $data['upper_vClaspsOp'] = explode(",",$data['lower_product']->vClaspsOp);

            if (!empty($data['upper_vTeethInMouthOp']) AND !empty($data['upper_vMissingTeethOp']) AND !empty($data['upper_vWillExtractOnDeliveryOp']) AND !empty($data['upper_vHasBeenExtractedOp']) AND !empty($data['upper_vFixOrAddOp']) AND !empty($data['upper_vClaspsOp'])) {

                $upper_teeth_in_mouthOp = array();
                $upper_extractOp= array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16");

                $upper_merged_arrayOp = array_merge(
                    $data['upper_vMissingTeethOp'],
                    $data['upper_vWillExtractOnDeliveryOp'],
                    $data['upper_vHasBeenExtractedOp'],
                    $data['upper_vFixOrAddOp'],
                    $data['upper_vClaspsOp']
                );

                foreach ($upper_extractOp as $value) {
                    if (!in_array($value,$upper_merged_arrayOp)) {
                        $upper_teeth_in_mouthOp[] = $value;
                    }
                }
                
                $data['upper_vTeethInMouthOp'] = array_unique(
                    array_merge(
                        $data['upper_vTeethInMouthOp'],
                        $upper_teeth_in_mouthOp
                    )
                );
            }


            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_impression'] = SlipImpression::get_by_iSlipId($criteria);

            $Oposite_impressions = "No";
            if (empty($data['upper_product'])) {
                $Oposite_impressions = "Yes";
            }else if($data['upper_product']->eImpression != "Yes"){
                $Oposite_impressions = "Yes";
            }

            if ($Oposite_impressions == "Yes") {
                $criteria[]  = "";
                $criteria['eType'] = "Opposite";
                $criteria['iSlipId'] = $iSlipId;
                $data['upper_impressionOp'] = SlipImpression::get_by_iSlipId($criteria);
            }

            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_addons'] = SlipAddons::get_by_iSlipId($criteria);



            $lower_delivery_date = $data['lower_product']->dDeliveryDate;
            if ($data['lower_product']->eRushDate == "Yes" AND !empty($data['lower_product']->eRushDate == "Yes")) {
                // if(!empty($data['upper_product'])){
                    $data["rush"] = "Yes";
                // }
            }
        }

        $criteria[]  = "";
        $criteria['iSlipId'] = $iSlipId;
        $data['stage_note'] = SlipNote::get_by_iSlipId($criteria);
        $data['rush_product_both'] = 'no';
        $data['rush_product'] = 'no';
        
        if((isset($data['upper_product']) && $data['upper_product'] !='') && (isset($data['lower_product']) && $data['lower_product']!=''))
        {
            $data['is_single_product'] ='no';
        }
        else
        {
            $data['is_single_product'] ='yes';
        }
        if ($data["rush"] == "No") {  
            
            if ((isset($upper_delivery_date) && !empty($upper_delivery_date)) && (isset($lower_delivery_date) && !empty($lower_delivery_date))) 
            {
                if($upper_delivery_date >= $lower_delivery_date)
                {
                    $data['final_delivery_date'] = $upper_delivery_date;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                    $data['is_single_product'] ='yes';
                }
                else
                {
                    $data['final_delivery_date'] = $lower_delivery_date;
                    $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
                    $data['is_single_product'] ='yes';
                }
            }
            else if ((isset($upper_delivery_date) && !empty($upper_delivery_date))) 
            {
                $data['final_delivery_date'] = $upper_delivery_date;
                $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
            }
            else if(isset($lower_delivery_date) && !empty($lower_delivery_date))
            {
                $data['final_delivery_date'] = $lower_delivery_date;
                $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
            }
            
        }else {
            // For Show rush date && delivery date start
            if((isset($data['upper_product']) && $data['upper_product'] !='') && (isset($data['lower_product']) && $data['lower_product']!=''))
            {
                $UpperDeliveryRushDate = $data['upper_product']->dDeliveryRushDate;
                $LowerDeliveryRushDate = $data['lower_product']->dDeliveryRushDate;
             
                if ((isset($UpperDeliveryRushDate) && !empty($UpperDeliveryRushDate)) && (isset($LowerDeliveryRushDate) && !empty($LowerDeliveryRushDate)))
                {
                    if(isset($UpperDeliveryRushDate) && !empty($UpperDeliveryRushDate))
                    {
                        $data['final_delivery_date_upper'] = $UpperDeliveryRushDate;
                        $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                        $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                        $data['rush_product'] = 'yes';
                        $data['rush_product_both'] = 'yes';
                    }
                    if(isset($LowerDeliveryRushDate) && !empty($LowerDeliveryRushDate))
                    {
                        $data['final_delivery_date_lower'] = $LowerDeliveryRushDate;
                        $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                        $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                        $data['rush_product'] = 'yes';
                        $data['rush_product_both'] = 'yes';
                    }
                }
                else if(isset($data['upper_product']) && $data['upper_product'] !='')
                {                
                    $UpperDeliveryRushDate = $data['upper_product']->dDeliveryRushDate;
                    if (isset($UpperDeliveryRushDate) && !empty($UpperDeliveryRushDate)) {
                        $data['final_delivery_date'] = $UpperDeliveryRushDate;
                        $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                        $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                        $data['rush_product'] = 'yes';
                        $data['upper_rush_product'] = 'yes';
                    }
                }
                else if(isset($data['lower_product']) && $data['lower_product']!='')
                {
                    $LowerDeliveryRushDate = $data['lower_product']->dDeliveryRushDate;
                    if (isset($LowerDeliveryRushDate) && !empty($LowerDeliveryRushDate)) {
                        $data['final_delivery_date'] = $LowerDeliveryRushDate;
                        $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                        $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                        $data['rush_product'] = 'yes';
                        $data['lower_rush_product'] = 'yes';
                    }
                }
                // dd($data['final_delivery_date_lower']);
            }
            else if(isset($data['upper_product']) && $data['upper_product'] !='')
            {                
                $UpperDeliveryRushDate = $data['upper_product']->dDeliveryRushDate;
                if (isset($UpperDeliveryRushDate) && !empty($UpperDeliveryRushDate)) {
                    $data['final_delivery_date'] = $UpperDeliveryRushDate;
                    $data['final_delivery_time'] = $data['upper_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['upper_product']->dPickUpDate;
                    $data['rush_product'] = 'yes';
                    $data['upper_rush_product'] = 'yes';
                }
            }
            else if(isset($data['lower_product']) && $data['lower_product']!='')
            {
                $LowerDeliveryRushDate = $data['lower_product']->dDeliveryRushDate;
                if (isset($LowerDeliveryRushDate) && !empty($LowerDeliveryRushDate)) {
                    $data['final_delivery_date'] = $LowerDeliveryRushDate;
                    $data['final_delivery_time'] = $data['lower_product']->tDeliveryTime;
                    $data['final_Pickup_date'] = $data['lower_product']->dPickUpDate;
                    $data['rush_product'] = 'yes';
                    $data['lower_rush_product'] = 'yes';
                }
            }
            // For Show rush date && delivery date end
        } 
        // dd($data);
        $data['tampid'] =  str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
        return $data;
    }
    // Update Billing data end

    public function soft_delete_slip(Request $request)
    {   
        $where['iCaseId'] = $request->id;
        $data['eIsDeleted'] = 'Yes';
        $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        Labcase::update_data($where,$data);
        return view('admin.labcase.ajax_listing_lab')->with($data);  
    }
    public function undo_delete_slip($iCaseId,$iSlipId)
    {   
        $where['iCaseId'] = $iCaseId;
        $data['eIsDeleted'] = 'No';
        $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        Labcase::update_data($where,$data);
        return redirect()->back()->withSuccess('Slip undo successfully.');
    }
    public function view_driverhistory_by_qr($iCaseId,$iSlipId)
    {
        if(isset($iSlipId) && !empty($iSlipId) && isset($iCaseId) && !empty($iCaseId))
        {
            $slipData = Labcase::get_slip_data($iCaseId,$iSlipId);    
            $OfficeSlipData =array();
            if(isset($slipData->vLocation) && ($slipData->vLocation =='In office ready to pickup' || $slipData->vLocation =='On route to the office' || $slipData->vLocation =='In lab ready to pickup' || $slipData->vLocation =='On route to the lab' ))
            {
                $criteria['iOfficeId_list'] = $slipData->iOfficeId;
                $criteria['eStatus_list'] = 'On Process';
                $criteria['vLocation_array'] = array('In office ready to pickup','On route to the office','In lab ready to pickup','On route to the lab');
                $OfficeSlipData['data'] =Labcase::get_all_data($criteria);
                return view('admin.labcase.driver_slip_model_popup')->with($OfficeSlipData);
            }
            else{
                return view('admin.labcase.driver_slip_model_popup')->with($OfficeSlipData);
            }
        }
    }
    public function view_driverhistory_by_listing($iCaseId,$iSlipId,$title)
    {
        if(isset($iSlipId) && !empty($iSlipId) && isset($iCaseId) && !empty($iCaseId))
        {
            $slipData = Labcase::get_slip_data($iCaseId,$iSlipId);    
            // dd($slipData);
            $OfficeSlipData =array();
            if($title =='Office')
            {
                $criteria['iOfficeId_list'] = $slipData->iOfficeId;
            }
            $criteria['vLocation_list'] = $slipData->vLocation;
            $criteria['eStatus_list'] = 'On Process';
            $OfficeSlipData['data'] =Labcase::get_all_data($criteria);
         
            return view('admin.labcase.driver_slip_model_popup')->with($OfficeSlipData);
        }
    }
    public function view_driverhistory_all_lab_listing($vLocation)
    {
        if(isset($vLocation) && !empty($vLocation))
        {   
            $vLocation = str_replace('-',' ',$vLocation);
            $OfficeSlipData =array();   
            $criteria['vLocation_list'] = $vLocation;
            $criteria['eStatus_list'] = 'On Process';
            $OfficeSlipData['data'] =Labcase::get_all_data($criteria);
         
            return view('admin.labcase.driver_slip_model_popup')->with($OfficeSlipData);
        }
    }
    public function undo_status(Request $request)
    {
        try{
            $iSlipId = $request->iSlipId;
            $iCaseId = $request->iCaseId;
            // Update case table start
            $where_case['iCaseId'] =  $iCaseId;
            $where_case['vLocation']='In lab ready to pickup';
            $data_case['vLocation']='In lab';
            $data_case['dtUpdatedDate']=date("Y-m-d H:i:s");
            Labcase::update_data($where_case,$data_case);
            // Update case table end
            
            // Update slip table start
            $where_slip['iSlipId'] =  $iSlipId;
            $where_slip['iCaseId'] =  $iCaseId;
            $where_slip['vLocation']='In lab ready to pickup';
            $data_slip['vLocation']='In lab';
            $data_slip['dtUpdatedDate']=date("Y-m-d H:i:s");
            Slip::update_new($where_slip,$data_slip);
            // Update case table end
    
            // Update driver history table start
            $where_driver_history['iSlipId'] =  $iSlipId;
            $where_driver_history['iCaseId'] =  $iCaseId;
            $where_driver_history['vLocation']='In lab ready to pickup';
            DriverHistory::undo_status($where_driver_history);
            // Update driver history table end
    
            $where_billing['iSlipId'] =  $iSlipId;
            $where_billing['iCaseId'] =  $iCaseId;
            $billigData = Billing::get_billing_data($where_billing);
            $iBillingId = $billigData->iBillingId;
    
            // Update billing product table start
            $where_billing_data['iBillingId'] =  $iBillingId;
            BillingProduct::delete_billing_product($where_billing_data);
            // Update billing product table end
    
            // Update billing addons table start
            $where_billing_data['iBillingId'] =  $iBillingId;
            BillingAddons::delete_billing_addons($where_billing_data);
            // Update billing addons table end
    
            // Update billing table start
            $where_billing_data['iBillingId'] =  $iBillingId;
             Billing::delete_billing($where_billing_data);
            // Update billing table end
            return 'success';
        }
        catch (\Exception $e) {
            return $e;
        }
    }
    public function undo_status_billing($iCaseId,$iSlipId)
    {
        try{
           
            // Update case table start
            $where_case['iCaseId'] =  $iCaseId;
            $where_case['vLocation']='In lab ready to pickup';
            $data_case['vLocation']='In lab';
            $data_case['dtUpdatedDate']=date("Y-m-d H:i:s");
            Labcase::update_data($where_case,$data_case);
            // Update case table end
            
            // Update slip table start
            $where_slip['iSlipId'] =  $iSlipId;
            $where_slip['iCaseId'] =  $iCaseId;
            $where_slip['vLocation']='In lab ready to pickup';
            $data_slip['vLocation']='In lab';
            $data_slip['dtUpdatedDate']=date("Y-m-d H:i:s");
            Slip::update_new($where_slip,$data_slip);
            // Update case table end
    
            // Update driver history table start
            $where_driver_history['iSlipId'] =  $iSlipId;
            $where_driver_history['iCaseId'] =  $iCaseId;
            $where_driver_history['vLocation']='In lab ready to pickup';
            DriverHistory::undo_status($where_driver_history);
            // Update driver history table end
    
            $where_billing['iSlipId'] =  $iSlipId;
            $where_billing['iCaseId'] =  $iCaseId;
            $billigData = Billing::get_billing_data($where_billing);
            $iBillingId = $billigData->iBillingId;
    
            // Update billing product table start
            $where_billing_data['iBillingId'] =  $iBillingId;
            BillingProduct::delete_billing_product($where_billing_data);
            // Update billing product table end
    
            // Update billing addons table start
            $where_billing_data['iBillingId'] =  $iBillingId;
            BillingAddons::delete_billing_addons($where_billing_data);
            // Update billing addons table end
    
            // Update billing table start
            $where_billing_data['iBillingId'] =  $iBillingId;
             Billing::delete_billing($where_billing_data);
            // Update billing table end
            return 'success';
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    public function send_email_to_lab_admin($email_array = array())
    {
       
            $toEmail = General::admin_info()['vEmail'];
            // Email send start
            $criteria = array();
            $criteria['vEmailCode'] = 'LAB_ADMIN_ALERT';
            $email = SystemEmail::get_all_data($criteria);
            // dd($email);
            $subject = str_replace("#SYSTEM.COMPANY_NAME#", 'Labslips Online', $email[0]->vEmailSubject);
            // $message = $email[0]->tEmailMessage;

            $constant   = array('#SITE_NAME#','#patient#','#Deliverydate#','#SlipNumber#',"#OFFICE_NAME#","#LAB_NAME#");
            $value      = array('Labslips Online',$email_array['vPatientName'],date('m/d/Y',strtotime($email_array['dDeliveryDate'])),$email_array['vSlipNumber'],$email_array['OfficeName'],$email_array['LabName']);
            $message = str_replace($constant, $value, $email[0]->tEmailMessage);
            $company_setting = General::setting_info('Email');
            $company_email = $company_setting['ADMIN_EMAIL']['vValue'];
            // $email_data['to']       = $toEmail;
            $email_data['bccEmails']       = 'krupesh.thakkar@isyncevolution.com';  
            $email_data['to']       = 'hmcofficeinnovs@gmail.com';
            $email_data['subject']  = $subject;
            $email_data['msg']      = $message;
            $email_data['from']     = $email[0]->vFromEmail;
            
            General::send('LAB_ADMIN_ALERT', $email_data);
            // Email send end
    }
    public function merge_name()
    {

        $fetch_data = DB::table('customer')->get();
        foreach ($fetch_data as $key => $value) {
            if(isset($value->vName) && !empty($value->vName) && $value->vName != null)
            {
                $vName = explode(" ",$value->vName);
                if(count($vName) == 1)
                {
                    $data['vFirstName'] = $vName[0];
                    $data['vLastName'] =  $vName[0];
                }
                elseif(count($vName) == 2)
                {
                    $data['vFirstName'] = current($vName);
                    $data['vLastName'] =  end($vName);
                }
                elseif(count($vName) == 3)
                {
                    $data['vFirstName'] = $vName[0].' '.$vName[1];
                    $data['vLastName'] =  end($vName);
                }
                $where['iCustomerId']= $value->iCustomerId;
                DB::table('customer')->where($where)->update($data);
            }
        }

      
        $fetch_data_lab = DB::table('lab_admin')->get();
        foreach ($fetch_data_lab as $key => $value) {
            if(isset($value->vName) && !empty($value->vName) && $value->vName != null)
            {
                $vLabName = explode(" ",$value->vName);
                if(count($vLabName) == 1)
                {
                    $data_lab['vFirstName'] = $vLabName[0];
                    $data_lab['vLastName'] =  $vLabName[0];
                }
                elseif(count($vLabName) == 2)
                {
                    $data_lab['vFirstName'] = current($vLabName);
                    $data_lab['vLastName'] =  end($vLabName);
                }
                elseif(count($vLabName) == 3)
                {
                    $data_lab['vFirstName'] = $vLabName[0].' '.$vLabName[1];
                    $data_lab['vLastName'] =  end($vLabName);
                }
                $where_lab['iLabAdminId']= $value->iLabAdminId;
                DB::table('lab_admin')->where($where_lab)->update($data_lab);
            }
        }
        
        $fetch_data_office = DB::table('office_admin')->get();
        foreach ($fetch_data_office as $key => $value) {
            if(isset($value->vName) && !empty($value->vName) && $value->vName != null)
            {
                $vOfficeName = explode(" ",$value->vName);
                if(count($vOfficeName) == 1)
                {
                    $data_office['vFirstName'] = $vOfficeName[0];
                    $data_office['vLastName'] =  $vOfficeName[0];
                }
                elseif(count($vOfficeName) == 2)
                {
                    $data_office['vFirstName'] = current($vOfficeName);
                    $data_office['vLastName'] =  end($vOfficeName);
                }
                elseif(count($vOfficeName) == 3)
                {
                    $data_office['vFirstName'] = $vOfficeName[0].' '.$vOfficeName[1];
                    $data_office['vLastName'] =  end($vOfficeName);
                }
                $where_office['iOfficeAdminId']= $value->iOfficeAdminId;
                DB::table('office_admin')->where($where_office)->update($data_office);
            }
        }
    }

    public function fetch_addons(Request $request)
    {
        $iLabId = $request->iLabId;
        $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type('Upper',$iLabId);
        $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type('Lower',$iLabId);
        $data['iLabId'] = $iLabId;
      
        return view('admin.labcase.addons_modal')->with($data);
    }  
    
    public function UploadUserImage(Request $request)
    {
        if ($request->hasFile('vImage') && !empty($request->user_id)) {
            $user_id = $request->user_id;
            
            $StaffData = Staff::get_by_id($user_id);
            $UserId = $StaffData->iLoginAdminId;
            $image = $request->file('vImage');
            if($request->customerType =='Lab Admin')
            {
                $imageName      = time().'.'.$request->vImage->getClientOriginalName();
                $path           = public_path('uploads/lab_office');
                $img            = Image::make($image->getRealPath()); 
                $img->resize(50, 50, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.'/'.$imageName);
                $request->vImage->move($path, $imageName);
                $data['vImage']    = $imageName;
                $where               = array();
                $where['iLabAdminId']         = $UserId;
                $Doctor_ID = new LabAdmin();
                $Doctor_ID->update($where, $data);
            }
            elseif($request->customerType =='Office Admin')
            {
                $imageName      = time().'.'.$request->vImage->getClientOriginalName();
                $path           = public_path('uploads/lab_office');
                $img            = Image::make($image->getRealPath()); 
                $img->resize(50, 50, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.'/'.$imageName);
                $request->vImage->move($path, $imageName);
                $data['vImage']    = $imageName;
                $where               = array();
                $where['iOfficeAdminId']         = $UserId;
                $Doctor_ID = new OfficeAdmin();
                $Doctor_ID->update($where, $data);
            }
            elseif($request->customerType =='Super Admin')
            {
                $imageName      = time().'.'.$request->vImage->getClientOriginalName();
                $path           = public_path('uploads/admin');
                $img            = Image::make($image->getRealPath()); 
                $img->resize(50, 50, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.'/'.$imageName);
                $request->vImage->move($path, $imageName);
                $data['vImage']    = $imageName;
                $where               = array();
                $where['iAdminId']         = $UserId;
                $Doctor_ID = new Admin();
                $Doctor_ID->update($where, $data);
            }
            elseif($request->customerType =='Doctor')
            {
                $imageName      = time().'.'.$request->vImage->getClientOriginalName();
                $path           = public_path('uploads/doctor');
                $img            = Image::make($image->getRealPath()); 
                $img->resize(50, 50, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.'/'.$imageName);
                $request->vImage->move($path, $imageName);
                $data['vImage']    = $imageName;
                $where               = array();
                $where['iDoctorId']         = $UserId;
              
                $Doctor_ID = new Doctor();
                $Doctor_ID->update($where, $data);
            }
            elseif($request->customerType =='Office User' || $request->customerType =='Lab User')
            {
                $imageName      = time().'.'.$request->vImage->getClientOriginalName();
                $path           = public_path('uploads/lab_office');
                $img            = Image::make($image->getRealPath()); 
                $img->resize(50, 50, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.'/'.$imageName);
                $request->vImage->move($path, $imageName);
                $data['vImage']    = $imageName;
                $where               = array();
                $where['iUserId']         = $UserId;
                $Doctor_ID = new User();
                $Doctor_ID->update($where, $data);
            }
            else
            {
                $imageName      = time().'.'.$request->vImage->getClientOriginalName();
                $path           = public_path('uploads/staff');
                $img            = Image::make($image->getRealPath()); 
                $img->resize(50, 50, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.'/'.$imageName);
                $request->vImage->move($path, $imageName);
                $data['vImage']    = $imageName;
                $where               = array();
                $where['iStaffId']         = $request->user_id;
                $Doctor_ID = new Staff();
                $Doctor_ID->update($where, $data);

            }
        }
        else
        {
            return redirect()->back()->withError('something went wrong');
        }
    }

   

    public function addon_listing_modal($iCaseId,$iSlipId,$iLabId)
    {
        if(isset($iSlipId) && !empty($iSlipId) && isset($iCaseId) && !empty($iCaseId))
        {
            $data['upper_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Upper");
            $data['lower_product'] = SlipProduct::get_by_iSlipId_eType($iSlipId,"Lower");
            
            $data['Upper_category_addons'] = Addoncategory::get_category_addons_by_type('Upper',$iLabId);
            $data['Lower_category_addons'] = Addoncategory::get_category_addons_by_type('Lower',$iLabId);
           
            $data['iLabId'] = $iLabId;

            $criteria[]  = "";
            $criteria['eType'] = "Upper";
            $criteria['iSlipId'] = $iSlipId;
            $data['upper_addons'] = SlipAddons::get_by_iSlipId($criteria);
    
            $criteria[]  = "";
            $criteria['eType'] = "Lower";
            $criteria['iSlipId'] = $iSlipId;
            $data['lower_addons'] = SlipAddons::get_by_iSlipId($criteria);
            $data['iSlipId'] = $iSlipId;


            if($iSlipId !='' && isset($iSlipId))
            {
                $criteria['iSlipId'] = $iSlipId;
                $SlipData = Labcase::get_all_data($criteria)->first();
            }
            $vPatientName = $SlipData->vPatientName;
            $vOfficeName = $SlipData->office_name__slip;
            $vCaseNumber = $SlipData->vCaseNumber;
            $vCasePanNumber = $SlipData->vCasePanNumber;
            $vSlipNumber = $SlipData->vSlipNumber;
            $data['data'] = array(
                'iSlipId'=>$iSlipId,
                'vPatientName'=>$vPatientName,
                'vOfficeName'=>$vOfficeName,
                'vCaseNumber'=>$vCaseNumber,
                'vCasePanNumber'=>$vCasePanNumber,
                'vSlipNumber'=>$vSlipNumber,
            );
            // dd($data);
            $data['iCaseId'] = $iCaseId;

            return view('admin.labcase.addons_modal')->with($data);
        }
        else
        {
            return redirect()->back()->withError('Something went wrong');
        }
    }

    public function store_addons(Request $request)
    {
           /*Upper Addons Start*/
           $upper_add_on_cat_id = $request->upper_add_on_cat_id;
           $upper_add_on_id = $request->upper_add_on_id;
           $upper_add_on_qty = $request->upper_add_on_qty;
           $iSlipId = $request->iSlipId;
           if(!empty($upper_add_on_cat_id)){
                   $where                      = array();
                   $where['iSlipId']           = $iSlipId;
                   $where['eType']             = "Upper";
                   SlipAddons::delete_by_id($where);
                   
               foreach ($upper_add_on_cat_id as $key => $value) 
               {
                   $upper_slip_product_addons = array();
                   $upper_slip_product_addons['iSlipId']      = $iSlipId;
                   $upper_slip_product_addons['iAddCategoryId'] = $value;
                   
                   $upper_slip_product_addons['iSubAddCategoryId'] = $upper_add_on_id[$key];
                   $upper_slip_product_addons['iQuantity'] = $upper_add_on_qty[$key];
                   $upper_slip_product_addons['eType'] = "Upper";
                    SlipAddons::add($upper_slip_product_addons);
               }
           }
           
           /*Upper Addons End*/

            /*Lower Addons Start*/
            $lower_add_on_cat_id = $request->lower_add_on_cat_id;
            $lower_add_on_id = $request->lower_add_on_id;
            $lower_add_on_qty = $request->lower_add_on_qty;
            if(!empty($lower_add_on_cat_id)){
                $where                      = array();
                $where['iSlipId']           = $iSlipId;
                $where['eType']             = "Lower";

                SlipAddons::delete_by_id($where);

                foreach ($lower_add_on_cat_id as $key => $value) 
                {
                    $lower_slip_product_addons = array();
                    $lower_slip_product_addons['iSlipId']      = $iSlipId;
                    $lower_slip_product_addons['iAddCategoryId'] = $value;
                    $lower_slip_product_addons['iSubAddCategoryId'] = $lower_add_on_id[$key];
                    $lower_slip_product_addons['iQuantity'] = $lower_add_on_qty[$key];
                    $lower_slip_product_addons['eType'] = "Lower";
                    SlipAddons::add($lower_slip_product_addons);
                }
            }
            /*Lower Addons End*/
            return redirect()->route('admin.labcase')->withSuccess('Addons added successfully.');
    }
  
    public function add_layouts(Request $request)
    {
        if(isset($request->layout_num) && !empty($request->layout_num))
        {
            $data['layout_num'] = $request->layout_num+1;
           
            $data['start_num'] = $request->layout_num*8;
            $data['end_num'] = ($request->layout_num*8)+8;
            
            return view('admin.labcase.add_layouts')->with($data);
        }
        else
        {
            return 'error';
        }
    }
}
