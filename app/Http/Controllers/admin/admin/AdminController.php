<?php

namespace App\Http\Controllers\admin\admin;

use App\Http\Controllers\Controller;
use App\Models\admin\admin\Admin;
use App\Models\admin\staff\Staff;
use Illuminate\Http\Request;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use App\Libraries\Paginator;
use App\Libraries\General;
use Validator;
use Auth;
use Session;
use Illuminate\Support\Facades\Route;


class AdminController extends Controller
{


    public function index()
    {   
        return view('admin.admin.listing');
    }
    
    public function ajax_listing(Request $request)
    {
        $action = $request->action;
  
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iAdminId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iAdminId']    = $request->id;

            Admin::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Admin_ID = (explode(",",$request->id));

            foreach ($Admin_ID as $key => $value) {
                $where                 = array();
                $where['iAdminId']    = $value;

                Admin::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $Admin_data = Admin::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = Admin::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        
        return view('admin.admin.ajax_listing')->with($data);   
    }

    public function create()
    {
        return view('admin.admin.create');
    }

    public function store(Request $request){

        $iAdminId = $request->id;

        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/admin'); 
            $request->vImage->move($path, $imageName);
        }

        $data['vFirstName']       = $request->vFirstName;
        $data['vLastName']       = $request->vLastName;
        $data['email']       = $request->email;
        $data['vMobile']     = $request->vMobile;
        if(empty($iAdminId)){
          $data['password']    = md5($request->vPassword);
        }
        if($iAdminId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if ($request->hasFile('vImage')) {   
            $data['vImage']    = $imageName;
        }
        if(!empty($iAdminId)){
            $where                      = array();
            $where['iAdminId']         = $iAdminId;

            $Admin_id = new Admin();
            $Admin_id->update($where, $data);
            // Update in staff table
            if(isset($iAdminId) && !empty($iAdminId) )
            {
                // Check this user exist in lab admin table 
                $exist_labAdmin = Admin::user_exit_in_staff($iAdminId,1);
               
                if($exist_labAdmin>0)
                {
                    $labData = array(
                        'vFirstName' => $request->vFirstName,
                        'vLastName' => $request->vLastName,
                        'vUserName'=>$request->vFirstName.' '.$request->vLastName,
                        'vEmail'=>$request->email,
                        'vMobile'=>$request->vMobile,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    // dd($labData);
                    $whereLabData = array(
                        'iLoginAdminId'=>$iAdminId,
                        'iDepartmentId'=>1
                    );
                    Admin::update_staff_data($labData,$whereLabData);
                }else
                {
                    $data_staff['iDepartmentId']= 1;
                    $data_staff['vMiddleInitial']= $request->vFirstName;
                    $data_staff['vFirstName']= $request->vFirstName;
                    $data_staff['vLastName']= $request->vLastName;
                    $data_staff['vUserName']= $request->vFirstName.' '.$request->vLastName;
                    $data_staff['vEmail']= $request->email;
                    $data_staff['vPassword']= md5($request->password);
                    $data_staff['vMobile']= $request->vMobile;
                    $data_staff['eStatus']= $request->eStatus;
                    $data_staff['iLoginAdminId']= $iAdminId;
                    $data_staff['dtAddedDate'] = date("Y-m-d H:i:s");
                    $data_staff['iCustomerId'] = $request->iCustomerId;
                    Staff::add($data_staff);
                }
                
            }
            return redirect()->route('admin.admin')->withSuccess('Admin updated successfully.');
        }
        else{
            $iAdminId = Admin::add($data);
            $data_staff['iDepartmentId']= 1;
            $data_staff['vMiddleInitial']= $request->vFirstName;
            $data_staff['vFirstName']= $request->vFirstName;
            $data_staff['vLastName']= $request->vLastName;
            $data_staff['vUserName']= $request->vFirstName.' '.$request->vLastName;
            $data_staff['vEmail']= $request->email;
            $data_staff['vPassword']= md5($request->password);
            $data_staff['vMobile']= $request->vMobile;
            $data_staff['eStatus']= $request->eStatus;
            $data_staff['iLoginAdminId']= $iAdminId;
            $data_staff['dtAddedDate'] = date("Y-m-d H:i:s");
            $data_staff['iCustomerId'] = $request->iCustomerId;
            Staff::add($data_staff);
            // Mail::to($data['email'])->send(new WelcomeMail($data));

            return redirect()->route('admin.admin')->withSuccess('Admin created successfully.');
        }
    }

    public function edit($iAdminId)
    {
        $data['admins'] = Admin::get_by_id($iAdminId);

        return view('admin.admin.create')->with($data);
    }
    public function showChangePasswordForm($iAdminId)
    {
        $data['admins'] = Admin::get_by_id($iAdminId);;
        return view('admin.admin.showChangePasswordForm')->with($data);
    }
    
    public function changePassword(Request $request)
    {
        $iAdminId = $request->id;
        $where                     = array();
        $data['password']          = md5($request->vPassword);
        $where['iAdminId']         = $iAdminId;

        $Admin_id = new Admin();
        $Admin_id->update($where, $data);
        
        return redirect()->back()->with("success","Password changed successfully !");

    }

    public function check_permission()
    {
        $route = Route::currentRouteAction();

        $methodName = substr($route, strrpos($route, '@' )+1);
        if(isset($methodName) && $methodName =='delete')
        {
            $methodName = 'eDelete';
        }
        $routeArray = app('request')->route()->getAction();
        $controllerAction = class_basename($routeArray['controller']);
        list($controller, $action) = explode('@', $controllerAction);
        $controllername =  $controller;
        $adminData = General::admin_info();
        $getPermission = General::get_module_permission($adminData['iDepartmentId'], $controllername);
        // dd($methodName);
        if(isset($getPermission) && !empty($getPermission))
        {  
            if($methodName =='ajax_listing')
            {
                $action = $request->query('action');
                if(isset($action) && $action == 'delete' && $getPermission->eDelete =='No')
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }   
    }
}
