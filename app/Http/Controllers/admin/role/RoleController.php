<?php

namespace App\Http\Controllers\admin\role;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\role\Role;
use App\Libraries\Paginator;
use Validator;

class RoleController extends Controller
{
    public function index(){
         return view('admin.role.listing');
    }

    public function ajax_listing(Request $request){
        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iRoleId";
            $order = "DESC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iRoleId']    = $request->iRoleId;

            Role::delete_by_id($where);
           
        }

        if($action == "multiple_delete"){
            $Role_ID = (explode(",",$request->roleID));

            foreach ($Role_ID as $key => $value) {
                $where                 = array();
                $where['iRoleId']    = $value;

                Role::delete_by_id($where);
              
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $role_data = Role::get_all_data($criteria);

        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($role_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        $paginator->is_ajax = true;
        $paging = true;
        $data['data'] = Role::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();

        return view('admin.role.ajax_listing')->with($data);  
    }

    public function create()
    {  
    
        return view('admin.role.create');
    }

    public function store(Request $request)
    {
        $iRoleId                        = $request->id;  

        $data['vRole']                  = $request->vRole;
        $data['eStatus']                = $request->eStatus;
       
        if(!empty($iRoleId)){
            $where                      = array();
            $where['iRoleId']           = $iRoleId;
           
            $data['dtUpdatedDate']      = date("Y-m-d H:i:s"); 
            Role::update_data($where, $data);

            return redirect()->route('admin.role')->withSuccess('Role updated successfully.');    
        }
        else{
            $data['dtAddedDate']        = date("Y-m-d H:i:s");
            Role::add($data);  
        }
       
        return redirect()->route('admin.role')->withSuccess('Role created successfully.');
    }

    public function edit($iRoleId)
    {
        $data['roles'] = Role::get_by_id($iRoleId);
        
        return view('admin.role.create')->with($data);
    }
}
