<?php

namespace App\Http\Controllers\admin\categoryproduct;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\categoryproduct\CategoryProduct;
use App\Models\admin\office_product_price\OfficeProductPrice;
use App\Models\admin\gumshades\GumShades;
use App\Models\admin\productgumshades\ProductGumShades;
use App\Models\admin\category\Category;
use App\Models\admin\grade\Grade;
use App\Models\admin\productgrade\ProductGrade;
use App\Models\admin\labOffice\LabOffice;
use App\Models\admin\customer\Customer;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;

class CategoryProductController extends Controller
{
    public function index()
    {
        Session::forget('per_page_lan');
        $criteria['eType'] = 'Upper';
        $data['uppercategory'] = CategoryProduct::get_all_category($criteria);
        
        $criteria['eType'] = 'Lower';
        $data['lowercategory'] = CategoryProduct::get_all_category($criteria);
        return view('admin.categoryproduct.listing')->with($data);
    }
    
    public function categoryproductselect($iCategoryId)
    {
        $data['categorys'] = CategoryProduct::get_by_once_categoryid($iCategoryId);
        if(isset($data['categorys']))
        {
            $eType = $data['categorys']->eType;
            if($eType=='Upper')
            {
                $criteria['iCategoryId'] = $data['categorys']->iCategoryId;
            }
            else if($eType=='Lower')
            {
            $criteria['iCategoryId'] = $data['categorys']->iCategoryId;
            }
            else{
                $criteria['iCategoryId'] = $data['categorys']->iCategoryId;
            }
        }
        $criteria['eType'] = 'Upper';
        $data['uppercategory'] = CategoryProduct::get_all_category($criteria);
        
        $criteria['eType'] = 'Lower';
        $data['lowercategory'] = CategoryProduct::get_all_category($criteria);
        return view('admin.categoryproduct.listing')->with($data);
    }

    public function ajax_listing(Request $request)
    {
        $action = $request->action;
        $iCategoryId = $request->iCategoryId;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->upperkeyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iCategoryProductId']    = $request->id;

            CategoryProduct::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $CategoryProduct_ID = (explode(",",$request->id));

            foreach ($CategoryProduct_ID as $key => $value) {
                $where                 = array();
                $where['iCategoryProductId']    = $value;

                CategoryProduct::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eType'] = 'Upper';
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $CategoryProduct_data = CategoryProduct::get_all_data($criteria);

        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($CategoryProduct_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->upper_limit_page !='')
        {
            $per_page = $request->upper_limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['eType'] = 'Upper';
        $criteria['iCategoryId'] = $iCategoryId;
        $data['data'] = 'Upper';
        $data['data'] = CategoryProduct::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        $data['paging'] = str_replace('class="ajax_page','class="ajax_page_upper',$paginator->paginate());
        
        return view('admin.categoryproduct.ajax_listing')->with($data);   
    }
    
    public function lower_ajax_listing(Request $request)
    {
        $action = $request->action;
        $CategoryProduct = $request->CategoryProduct;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->lower_keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iCategoryProductId']    = $request->id;

            CategoryProduct::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $CategoryProduct_ID = (explode(",",$request->id));

            foreach ($CategoryProduct_ID as $key => $value) {
                $where                 = array();
                $where['iCategoryProductId']    = $value;

                CategoryProduct::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eType'] = 'Lower';
         // For Perticular customer show start
         $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
         // For Perticular customer show end
        $CategoryProduct_data = CategoryProduct::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($CategoryProduct_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->lower_limit_page !='')
        {
            $per_page = $request->lower_limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }
        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['eType'] = 'Lower';
        $criteria['iCategoryId'] = $CategoryProduct;
       
        $data['data'] = CategoryProduct::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = str_replace('class="ajax_page','class="ajax_page_lower',$paginator->paginate());
        
        return view('admin.categoryproduct.lower_ajax_listing')->with($data);   
    }
    
    public function create()
    {
        // For Get customer Office satrt
        $iCustomerId = General::admin_info()['iCustomerId'];
        $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);
        $office = array();
        foreach ($labOffice as $key => $value) {
            $office[] = Customer::get_by_id($value->iOfficeId);
        }
        $data['office'] = $office;
        // For Get customer Office end
        $criteria['eType']  = 'Upper';
        $data['category']   = CategoryProduct::get_all_category($criteria); 
        $data['grades']     = CategoryProduct::get_all_grade(); 
        // gum brand start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        $data['gumshades'] = ProductGumShades::get_brand_shades_data();
        $data['proshades'] = array();
        // dd($data['gumshades']);
        // gum brand end
        return view('admin.categoryproduct.create')->with($data);
    }

   public function store(Request $request)
    {
        $iCategoryProductId = $request->id;

        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/categoryproduct'); 
            $request->vImage->move($path, $imageName);
        }
        if ($request->hasFile('vLowerImage')) {
            $imageName1      = time().'.'.$request->vLowerImage->getClientOriginalName();
            $path           = public_path('uploads/categoryproduct'); 
            $request->vLowerImage->move($path, $imageName1);
        }

        $data['vCode'] 	    = $request->vCode;
        $data['vName']      = $request->vName;
        $data['iCategoryId']= $request->iCategoryId;
        $data['iSequence']  = $request->iSequence;
        $data['vPrice']     = $request->vPrice;
        $data['eType']      = $request->eType;
        $data['eStatus']    = $request->eStatus;
        $data['eClaspAdd']  = $request->eClaspAdd;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        if($iCategoryProductId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        
        if ($request->hasFile('vImage')) {   
            $data['vImage']    = $imageName;
        }
        sleep(1);
        if ($request->hasFile('vLowerImage')) {   
            $data['vLowerImage']    = $imageName1;
        }
        
        if(!empty($iCategoryProductId)){
            $where                      = array();
            $where['iCategoryProductId']       = $iCategoryProductId;
            
            $CategoryProduct_id = new CategoryProduct();
            $CategoryProduct_id->update($where, $data);
            
            $iGradeId    = $request->iGradeId;
            $vGradePrice    = $request->vGradePrice;
            if(!empty($iGradeId)){
                $where['iCategoryProductId'] = $iCategoryProductId;
                ProductGrade::delete_by_id($where);
                for ($i=0; $i < count($iGradeId) ; $i++) 
                {   
                    if(isset($vGradePrice[$i]) && !empty($vGradePrice[$i]))
                    {
                        $datas['iCategoryProductId'] = $iCategoryProductId;
                        $datas['iGradeId'] 	         = $iGradeId[$i];
                        $datas['vPrice'] 	         = $vGradePrice[$i];
                        $ProductGrade_id = ProductGrade::add($datas);
                    }
                }
            }else{
                $where['iCategoryProductId'] = $iCategoryProductId;
                ProductGrade::delete_by_id($where);
            }

            // Gumshades start
            $iGumShadesId    = $request->iGumShadesId;
            
            if(!empty($iGumShadesId)){
                $where['iCategoryProductId'] = $iCategoryProductId;
                ProductGumShades::delete_by_id($where);
                for ($i=0; $i < count($iGumShadesId) ; $i++) 
                {   
                    if(isset($iGumShadesId[$i]) && !empty($iGumShadesId[$i]))
                    {
                        $datashade['iCategoryProductId'] = $iCategoryProductId;
                        $datashade['iGumShadesId'] 	         = $iGumShadesId[$i];
                
                        $ProductGumShades_id = ProductGumShades::add($datashade);
                    }
                }
            }else{
                $where['iCategoryProductId'] = $iCategoryProductId;
                ProductGumShades::delete_by_id($where);
            }
            // Gumshades end
            
              //Store Office Prices start
                $iCustomerId = General::admin_info()['iCustomerId'];
                
                $OfficeExist = CategoryProduct::checkExistOffice($iCategoryProductId);
             
                if(isset($OfficeExist) && count($OfficeExist) >0)
                {
                    CategoryProduct::deleteExistOffice($iCategoryProductId);
                }
                //Store Office Prices start
               
                if(isset($request->Office) && !empty($request->Office))
                {
                    foreach($request->Office as $key=> $office_val)   
                    {
                        // For Grade price start
                        if($office_val['vOfficePrice'] == null)
                        {
                            if(isset($office_val['OfficeGrade']))
                            {
                                foreach ($office_val['OfficeGrade'] as $key_grade => $value_grade) 
                                {
                                    if(isset($value_grade['vOfficePrice']) && !empty($value_grade['vOfficePrice']) && isset($value_grade['iGradeId']) && !empty($value_grade['iGradeId']) && $value_grade['vOfficePrice'] !=null)
                                    {
                                        $office_data['iGradeId'] = $value_grade['iGradeId'];
                                        $office_data['vPrice'] = $value_grade['vOfficePrice'];
                                        // $office_data['vPrice'] = $request['vPrice'];
                                        $office_data['iOfficeId'] = $office_val['iOfficeId'];
                                        $office_data['iLabId'] = General::admin_info()['iCustomerId'];
                                        $office_data['vLabName'] = General::admin_info()['vName'];
                                        $office_data['vOfficeName'] = $office_val['vOfficeName'];
                                        $office_data['iProductId'] = $iCategoryProductId;
                                        $office_data['dtAddedDate'] = date("Y-m-d H:i:s");
                                        $office_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                                        $LastOfficeId = CategoryProduct::AddOfficePrice($office_data);
                                    }
                                }
                            }
                        }
                        else
                        {
                                $office_data['vPrice'] = $office_val['vOfficePrice'];
                                // $office_data['vPrice'] = $request['vPrice'];
                                $office_data['iOfficeId'] = $office_val['iOfficeId'];
                                $office_data['iLabId'] = General::admin_info()['iCustomerId'];
                                $office_data['vLabName'] = General::admin_info()['vName'];
                                $office_data['vOfficeName'] = $office_val['vOfficeName'];
                                $office_data['iProductId'] = $iCategoryProductId;
                                $office_data['dtAddedDate'] = date("Y-m-d H:i:s");
                                $office_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                                $LastOfficeId = CategoryProduct::AddOfficePrice($office_data);
                        
                        }
                        // For Grade price end
                    
                    }
                }
                // dd($iCategoryProductId);
              //Store Office Prices end

            return redirect()->route('admin.categoryproduct')->withSuccess('Category Product updated successfully.');
        }
        else{
            $where['iCategoryProductId'] = $iCategoryProductId;
            
                CategoryProduct::delete_by_id($where);

            $CategoryProduct_id = CategoryProduct::add($data);
            $iGradeId    = $request->iGradeId;
            $vGradePrice    = $request->vGradePrice;
            if(!empty($iGradeId)){

                $where['iCategoryProductId'] = $iCategoryProductId;
                ProductGrade::delete_by_id($where);
                for ($i=0; $i < count($iGradeId) ; $i++) 
                {   
                    $datas['iCategoryProductId'] = $CategoryProduct_id;
                    $datas['iGradeId'] 	         = $iGradeId[$i];
                    if($vGradePrice[$i] ==null)
                    {
                        $datas['vPrice'] 	         = '';
                    }
                    else
                    {
                        $datas['vPrice'] = $vGradePrice[$i];
                    }
                    $ProductGrade_id = ProductGrade::add($datas);
                }
            }
            
             // Gumshades start
             $iGumShadesId    = $request->iGumShadesId;
         
             if(!empty($iGumShadesId)){
                 $where['iCategoryProductId'] = $CategoryProduct_id;
                 ProductGumShades::delete_by_id($where);
                 for ($i=0; $i < count($iGumShadesId) ; $i++) 
                 {   
                     if(isset($iGumShadesId[$i]) && !empty($iGumShadesId[$i]))
                     {
                         $datashade['iCategoryProductId'] = $CategoryProduct_id;
                         $datashade['iGumShadesId'] 	         = $iGumShadesId[$i];
                         $ProductGumShades_id = ProductGumShades::add($datashade);
                     }
                 }
             }else{
                 $where['iCategoryProductId'] = $CategoryProduct_id;
                 ProductGumShades::delete_by_id($where);
             }
             // Gumshades end
             
            //Store Office Prices start
            // dd($request);
            if(isset($request->Office) && !empty($request->Office))
            {
                foreach($request->Office as $key=> $office_val)   
                {
                    // For Grade price start
                  
                    if(!isset($office_val['vOfficePrice']) && $office_val['vOfficePrice'] == null)
                    {
                        if(isset($office_val['OfficeGrade']))
                        {
                            foreach ($office_val['OfficeGrade'] as $key_grade => $value_grade) 
                            {
                                if(isset($value_grade['vOfficePrice']) && $value_grade['vOfficePrice'] !=null && !empty($value_grade['vOfficePrice']))
                                {
                                    $office_data['vPrice'] = $value_grade['vOfficePrice'];
                                    $office_data['iGradeId'] = $value_grade['iGradeId'];
                                    $office_data['iOfficeId'] = $office_val['iOfficeId'];
                                    $office_data['iLabId'] = General::admin_info()['iCustomerId'];
                                    $office_data['vLabName'] = General::admin_info()['vName'];
                                    $office_data['vOfficeName'] = $office_val['vOfficeName'];
                                    $office_data['iProductId'] = $CategoryProduct_id;
                                    $office_data['dtAddedDate'] = date("Y-m-d H:i:s");
                                    $office_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                                    $LastOfficeId = CategoryProduct::AddOfficePrice($office_data);
                                }
                            }
                        }
                    }
                    else
                    {
                        $office_data['vPrice'] = $office_val['vOfficePrice'];
                        if(empty($office_val['vOfficePrice']))
                        {
                            $office_data['vPrice'] = $request->vPrice;
                        }
                        $office_data['iOfficeId'] = $office_val['iOfficeId'];
                        $office_data['iLabId'] = General::admin_info()['iCustomerId'];
                        $office_data['vLabName'] = General::admin_info()['vName'];
                        $office_data['vOfficeName'] = $office_val['vOfficeName'];
                        $office_data['iProductId'] = $CategoryProduct_id;
                        $office_data['dtAddedDate'] = date("Y-m-d H:i:s");
                        $office_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                        $LastOfficeId = CategoryProduct::AddOfficePrice($office_data);
                    }
                    // For Grade price end
                   
                }
            }
            //Store Office Prices end
            return redirect()->route('admin.categoryproduct')->withSuccess('Category Product created successfully.');
        }
    }

    public function edit($iCategoryProductId)
    {
        $data['categoryproducts'] = CategoryProduct::get_by_id($iCategoryProductId);

        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['categoryproducts']->iCustomerId) && $data['categoryproducts']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['categoryproducts']->iCustomerId)))
        {
            return redirect()->route('admin.category')->withError('Invalid Input');
        }
        $criteria["eType"] = $data['categoryproducts']->eType;
       
        $data['category'] = CategoryProduct::get_all_category($criteria);
        $data['grades']     = CategoryProduct::get_all_grade(); 
        $product_grade     = ProductGrade::get_by_id($iCategoryProductId);
        $data['product_grade'] = $product_grade;

        foreach ($product_grade as $key => $value) {
            $data['prograde'][] = $value->iGradeId;
            // $data['prices'][] = $value->vPrice;
        
        }
      
         // For Get customer Office satrt
        $iCustomerId = General::admin_info()['iCustomerId'];
        $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);
        $office = array();
        foreach ($labOffice as $key => $value) {
            $office[] = Customer::get_by_id($value->iOfficeId);
        }
        $new_array = array();
        foreach ($office as $key => $value_office) {
           $new_array[$key]['vOfficeName'] = $value_office->vOfficeName;
           $new_array[$key]['iOfficeId'] = $value_office->iCustomerId;
           $new_array[$key]['iCategoryProductId'] = $iCategoryProductId;
           if(isset($data['prograde']) && !empty($data['prograde']))
           {
               foreach ($data['prograde'] as $key_grade => $value_grade) {
                    $new_array[$key]['grades'][$key_grade]['iGradeid'] = $value_grade;
                    $new_array[$key]['grades'][$key_grade]['vQualityName'] = Grade::get_by_id($value_grade)->vQualityName;
                    $criteria_office = array();
                    $criteria_office['iOfficeId'] = $value_office->iCustomerId;
                    $criteria_office['iProductId'] = $iCategoryProductId;
                    $criteria_office['iGradeId'] = $value_grade;
                    $data_office_product_grade_price = OfficeProductPrice::get_by_product_grade_office($criteria_office);
                    foreach ($data_office_product_grade_price as $key_grade_price => $value_grade_price) {
                        $new_array[$key]['grades'][$key_grade]['vPrice']=$value_grade_price->vPrice;
                    }
               }
               foreach ($data['grades'] as $key_new_grade => $value_new_grade) {
                    if(!in_array($value_new_grade->iGradeId,$data['prograde']))
                    {
                        $new_array[$key]['grades'][$key_new_grade]['iGradeid'] = $value_new_grade->iGradeId;
                    }
               }
           }
           else
           {
                $criteria_office['iOfficeId'] = $value_office->iCustomerId;
                $criteria_office['iProductId'] = $iCategoryProductId;
                $data_office_product_grade_price = OfficeProductPrice::get_by_product_grade_office($criteria_office);
                if(isset($data_office_product_grade_price) && count($data_office_product_grade_price)>0)
                {
                    $new_array[$key]['vPrice'] = $data_office_product_grade_price[0]->vPrice;
                }
           }
           
        }
        $data['office']= $new_array;
        $data['edit']= 'Yes';
        // dd($new_array);
         // For Get customer Office end
        // gum brand start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        $data['gumshades'] = ProductGumShades::get_brand_shades_data();
        // dd( $data['gumshades']);
        $product_shades     = ProductGumShades::get_by_id($iCategoryProductId);
        $data['product_shades'] = $product_shades;
        // dd($product_shades);
        $data['proshades'] = array();
        foreach ($product_shades as $key => $value) {
            $data['proshades'][] = $value->iGumShadesId;
        }
        // gum brand end
        return view('admin.categoryproduct.create')->with($data);
    }
 
    public function destroy($id)
    {
        $CategoryProduct = CategoryProduct::find($id);
        $CategoryProduct->delete();

        return redirect()->route('admin.categoryproduct')->withSuccess('Category Product deleted successfully.');
    }

    public function multiDelete(Request $request)
    {
        $id = $request->id;

        CategoryProduct::whereIn('id',explode(",",$id))->delete();
    }
    public function fetch_category(Request $request)
    {
        $eType = $request->eType;
        $data['data'] = Category::where("eType",$eType)
                                    ->orderBy("iSequence", "ASC")
                                    ->get();
        return view('admin.categoryproduct.ajax_fetch_category_listing')->with($data);
    }
    
}
