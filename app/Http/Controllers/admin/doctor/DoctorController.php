<?php

namespace App\Http\Controllers\admin\doctor;

use App\Http\Controllers\Controller;
use App\Models\admin\doctor\Doctor;
use App\Models\admin\office\Office;
use App\Models\admin\customer\Customer;
use App\Models\admin\staff\Staff;
use Illuminate\Http\Request;
use App\Libraries\Paginator;
use Validator;
use Session;
use App\Libraries\General;
use Image;

class DoctorController extends Controller
{
    public function index($iCustomerId = "")
    {    
        $per_page = Session::forget('per_page_lan');
        if (!empty($iCustomerId)) {
           $data['iCustomerId'] = $iCustomerId;
        }
        $data['office'] = Doctor::get_all_office();
        $data['customer'] = Doctor::get_all_customer();
  
        return view('admin.doctor.listing')->with($data);
    }
    
    public function ajax_listing(Request $request)
    {
        $action = $request->action;
        $iOfficeId = $request->iOfficeId;
        if(isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $iCustomerId = $request->iCustomerId;
        }
        else
        {
            $iCustomerId = General::admin_info()['iCustomerId'];
        }
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iDoctorId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iDoctorId']    = $request->id;

            Doctor::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Doctor_ID = (explode(",",$request->id));

            foreach ($Doctor_ID as $key => $value) {
                $where                 = array();
                $where['iDoctorId']    = $value;

                Doctor::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        // For Perticular customer show start
        $criteria['iCustomerId'] = $iCustomerId;
   
        // For Perticular customer show end
        $Admin_data = Doctor::get_all_data($criteria);
      
        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);
        

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }
        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iOfficeId'] = $iOfficeId;

        $data['data'] = Doctor::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        
        return view('admin.doctor.ajax_listing')->with($data);   
    }

    public function create()
    {
        // $data['office'] = Doctor::get_all_office();
        $data['customer'] = Customer::get_all_customer();
        return view('admin.doctor.create',$data);
    }
    
    public function store(Request $request){
        $iDoctorId = $request->id;
        
        $imageLogo = $request->file('vImage_Logo');
        if ($request->hasFile('vImage_Logo')) {
            $image_Logo      = time().'.'.$request->vImage_Logo->getClientOriginalName();
            $path           = public_path('uploads/lab_office');
            $img            = Image::make($imageLogo->getRealPath()); 
            $img->resize(50, 50, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.'/'.$image_Logo);
            $request->vImage_Logo->move($path, $image_Logo);
            $data['vImage_Logo']    = $image_Logo;
        }

        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/doctor'); 
            $request->vImage->move($path, $imageName);
            
            $data['vImage']    = $imageName;
            
        }
        if ($request->image) {
            $folderPath = public_path('uploads/doctor');
            
            $base64Image = explode(";base64,", $request->image);
            $explodeImage = explode("image/", $base64Image[0]);
            $imageType = $explodeImage[1];
            $image_base64 = base64_decode($base64Image[1]);
            $imagename = uniqid(). '.'.$imageType;
            $file = $folderPath ."/". $imagename;
            file_put_contents($file, $image_base64);

            $data['vImage']    = $imagename;
        }

        if($request->iCustomerId)
        {
            $iCustomerId    = $request->iCustomerId;
        }else{
               // For Perticular customer show start
            $iCustomerId = General::admin_info()['iCustomerId'];
               // For Perticular customer show end
        }
   
        $data['vFirstName']   = $request->vFirstName;
        $data['vLastName']    = $request->vLastName;
        $data['tSigned']      = $request->signed;
        $data['iOfficeId']    = $iCustomerId;
        $data['iCustomerId']    = $iCustomerId;
        $data['vEmail']       = $request->vEmail;
        $data['vPassword']    = md5($request->vPassword);
        $data['vMobile']      = $request->vMobile;
        $data['vCellulor']    = $request->vCellulor;
        $data['tDescription'] = $request->tDescription;
        $data['vLicence']     = $request->vLicence;   
        $data['eStatus']      = $request->eStatus;        

        if($iDoctorId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        // if ($request->hasFile('vImage')) {   
        //     $data['vImage']    = $imageName;
        // } 
        
        if(!empty($iDoctorId)){
            $where               = array();
            $where['iDoctorId']         = $iDoctorId;
            $Doctor_ID = new Doctor();
            $Doctor_ID->update($where, $data);
            // Update in staff table
            if(isset($iDoctorId) && !empty($iDoctorId) )
            {
                // Check this user exist in lab admin table 
                $exist_labAdmin = Doctor::user_exit_in_staff($iDoctorId,$iCustomerId);
                if($exist_labAdmin>0)
                {
                    $labData = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        'vEmail'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $whereLabData = array(
                        'iLoginAdminId'=>$iDoctorId,
                        'iCustomerId'=>$iCustomerId
                    );
                    Doctor::update_staff_data($labData,$whereLabData);
                }
            }
            return redirect()->route('admin.doctor')->withSuccess('Doctor updated successfully.');
        }
        else{
            $iLoginAdminId = Doctor::add($data);
            // Add doctor into staff
            $data_staff['iDepartmentId']= 4;
            $data_staff['vFirstName']= $request->vFirstName;
            $data_staff['vLastName']= $request->vLastName;
            $data_staff['vEmail']= $request->vEmail;
            $data_staff['vPassword']= md5($request->vPassword);
            $data_staff['vMobile']= $request->vMobile;
            $data_staff['eStatus']= $request->eStatus;
            $data_staff['iLoginAdminId']= $iLoginAdminId;
            $data_staff['iCustomerId'] = $iCustomerId;
            $data_staff['dtAddedDate'] = date("Y-m-d H:i:s");
            Staff::add($data_staff);   
            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            return redirect()->route('admin.doctor')->withSuccess('Doctor created successfully.');
        }
    }

    public function edit($iDoctorId)
    {
        $data['office'] = Doctor::get_all_office();
        $data['customer'] = Doctor::get_all_customer();
        $data['doctors'] = Doctor::get_by_id($iDoctorId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['doctors']->iCustomerId) && $data['doctors']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['doctors']->iCustomerId)))
        {
            return redirect()->route('admin.doctor')->withError('Invalid Input');
        }

        return view('admin.doctor.create')->with($data);
    }
    public function fetch_office(Request $request)
    {
        $iCustomerId = $request->iCustomerId;
         
        $data['data'] = Office::where("iCustomerId",$iCustomerId)
                                ->orderBy("iOfficeId", "desc")
                                ->get();
        return view('admin.user.ajax_fetch_office_listing')->with($data);
    }
    public function doctorlist($iCustomerId,$iOfficeId)
    {   
        $data['iOfficeId'] = $iOfficeId;
        $data['iCustomerId'] = $iCustomerId;
        $data['office'] = Doctor::get_all_office();
        return view('admin.doctor.doctor_listing')->with($data);
    }
    
    public function doctor_ajax_listing(Request $request)
    {
        $action = $request->action;
        $iOfficeId = $request->iOfficeId;
        $iCustomerId = $request->iCustomerId;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iDoctorId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iDoctorId']    = $request->id;

            Doctor::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Doctor_ID = (explode(",",$request->id));

            foreach ($Doctor_ID as $key => $value) {
                $where                 = array();
                $where['iDoctorId']    = $value;

                Doctor::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $Admin_data = Doctor::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iOfficeId'] = $iOfficeId;

        $data['data'] = Doctor::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        
        return view('admin.doctor.doctor_ajax_listing')->with($data);   
    }

    public function doctorcreate($iCustomerId,$iOfficeId)
    {
        $data['iCustomerId'] = $iCustomerId;
        $data['iOfficeId']   = $iOfficeId;
        $data['office'] = Doctor::get_all_office();
        return view('admin.doctor.doctor_create')->with($data);
    }

    public function doctorstore(Request $request)
    {
        $iDoctorId = $request->id;
        $iOfficeId = $request->iOfficeId;
        $iCustomerId = $request->iCustomerId;

        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/doctor'); 
            $request->vImage->move($path, $imageName);
            
            $data['vImage']    = $imageName;
            
        }
        if ($request->image) {
            $folderPath = public_path('uploads/doctor');
            
            $base64Image = explode(";base64,", $request->image);
            $explodeImage = explode("image/", $base64Image[0]);
            $imageType = $explodeImage[1];
            $image_base64 = base64_decode($base64Image[1]);
            $imagename = uniqid(). '.'.$imageType;
            $file = $folderPath ."/". $imagename;
            file_put_contents($file, $image_base64);

            $data['vImage']    = $imagename;
        }
        
        $data['vFirstName']   = $request->vFirstName;
        $data['vLastName']    = $request->vLastName;
        $data['iOfficeId']    = $iOfficeId;
        $data['iCustomerId']  = $iCustomerId;
        $data['vEmail']       = $request->vEmail;
        $data['vMobile']      = $request->vMobile;
        $data['vCellulor']    = $request->vCellulor;
        $data['tDescription'] = $request->tDescription;
        $data['vLicence']     = $request->vLicence;   
        $data['vMiddleInitial']= $request->vMiddleInitial;
        $data['eStatus']      = $request->eStatus;         

        if(empty($iDoctorId)){
            $data['vPassword']    = md5($request->vPassword);
          }
        if($iDoctorId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iDoctorId)){
            $where               = array();
            $where['iDoctorId']         = $iDoctorId;

            $Doctor_ID = new Doctor();
            $Doctor_ID->update($where, $data);

            return redirect()->route('admin.doctorlist',[$iCustomerId,$iOfficeId])->withSuccess('Doctor updated successfully.');
        }
        else{
            $data = Doctor::add($data);

            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            return redirect()->route('admin.doctorlist',[$iCustomerId,$iOfficeId])->withSuccess('Doctor created successfully.');
        }
    }

    public function doctoredit($iCustomerId,$iOfficeId,$iDoctorId)
    {
        $data['iCustomerId'] = $iCustomerId;
        $data['iOfficeId']    = $iOfficeId;
        $data['office'] = Doctor::get_all_office();
        $data['doctors'] = Doctor::get_by_id($iDoctorId);
        return view('admin.doctor.doctor_create')->with($data);
    }
    public function showChangePasswordForm($iDoctorId)
    {
        $data['doctors'] = Doctor::get_by_id($iDoctorId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['doctors']->iCustomerId) && $data['doctors']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['doctors']->iCustomerId)))
        {
            return redirect()->route('admin.doctor')->withError('Invalid Input');
        }
        return view('admin.doctor.showChangePasswordForm')->with($data);
    }
    
    public function changePassword(Request $request)
    {
        $iDoctorId = $request->id;
        $where                     = array();
        $data['vPassword']        = md5($request->vPassword);
        $where['iDoctorId']     = $iDoctorId;

        $Doctor_id = new Doctor();
        $Doctor_id->update($where, $data);
        if(isset($iDoctorId) && !empty($iDoctorId) && isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $iLabAdminId = $iDoctorId;
            $iCustomerId = $request->iCustomerId;
            // Check this user exist in lab admin table 
            $exist_doctor = Doctor::user_exit_in_staff($iDoctorId,$iCustomerId);
            
            if($exist_doctor>0)
            {
                $labData = array(
                    'vPassword'=>md5($request->vPassword),
                    'dtUpdatedDate'=>date('Y-m-d H:i:s')
                );
                $whereLabData = array(
                    'iLoginAdminId'=>$iDoctorId,
                    'iCustomerId'=>$iCustomerId
                );
                Staff::update_staff_data($labData,$whereLabData);
            }
            
        }
        return redirect()->back()->with("success","Password changed successfully !");

    }
}
