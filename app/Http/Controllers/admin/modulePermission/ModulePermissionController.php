<?php

namespace App\Http\Controllers\admin\modulePermission;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\Paginator;
use App\Models\admin\modulePermission\ModulePermission;
use App\Models\admin\department\Department;
use App\Models\admin\moduleMaster\ModuleMaster;
use App\Libraries\General;

class ModulePermissionController extends Controller
{
    public function index()
    {
        return view('admin.module_master.listing');
    }

    public function ajax_listing(Request $request)
    {
        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iPermissionId";
            $order = "DESC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iPermissionId']    = $request->iPermissionId;

            ModulePermission::delete_by_id($where); 
        }

        if($action == "multiple_delete"){
            $iPermissionId = (explode(",",$request->iPermissionId));

            foreach ($iPermissionId as $key => $value) {
                $where                 = array();
                $where['iPermissionId']    = $value;

                ModulePermission::delete_by_id($where);  
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $banner_data = ModulePermission::get_all_data($criteria);

        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($banner_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        $paginator->is_ajax = true;
        $paging = true;
        $data['data'] = ModulePermission::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();

        return view('admin.module_master.ajax_listing')->with($data);  
    }

    public function create()
    {
        $iCustomerId = General::admin_info()['iCustomerId'];
        if(isset($iCustomerId) && !empty($iCustomerId))
        {
            $data['department'] = Department::get_by_cust_id($iCustomerId);
        }
        else
        {
            $data['department'] = Department::get_all_type();
        }

        return view('admin.module_permission.create')->with($data);
    }

    public function store(Request $request)
    {
        $iPermissionId          = $request->id;
        $where                  = array();
        $where['iDepartmentId']       = $request->iDepartmentId;
        ModulePermission::delete_by_id($where);
        if(isset($request->permission) && !empty($request->permission) && count($request->permission)>0 && isset($request->permission_slip) && !empty($request->permission_slip) && count($request->permission_slip)>0)
        {
            $permission_array = $request->permission;
            $permission_array += $request->permission_slip;
            ksort($permission_array);
        }
        elseif(isset($request->permission_slip) && !empty($request->permission_slip) && count($request->permission_slip)>0)
        {
            $permission_array = $request->permission_slip;
        }
        elseif(isset($request->permission) && !empty($request->permission) && count($request->permission)>0)
        {
            $permission_array = $request->permission;
        }
        
        foreach ($permission_array as $key => $value) {
            $value['iDepartmentId'] = $request->iDepartmentId;
            ModulePermission::create($value);
        }
        return redirect()->back()->withSuccess('Module Permission created successfully.');
    }

    public function edit($iDepartmentId)
    {
        $iCustomerId = General::admin_info()['iCustomerId'];
        $MyDepartment = array();
        if(isset($iCustomerId) && !empty($iCustomerId))
        {
            $data['department'] = Department::get_by_cust_id($iCustomerId);
            if(isset($data['department']) && !empty($data['department']))
            {
                foreach($data['department'] as $department)
                {
                    array_push($MyDepartment,$department->iDepartmentId);
                }
            }
            else
            {
                return redirect()->back()->withError('Department not found');
            }

            if(!in_array($iDepartmentId,$MyDepartment))
            {
                return redirect()->back()->withError('Department not found');
            }
        }
        else
        {
            $data['department'] = Department::get_all_type();
        }

        $data['iPermissionId']      = $iDepartmentId;
        $data['modules']            = ModuleMaster::get_all();
        $data['modulesPermission']  = ModulePermission::get_by_department_id($iDepartmentId);

        
        // dd($data);
        
        return view('admin.module_permission.create')->with($data);
    }  
}
