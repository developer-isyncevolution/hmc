<?php

namespace App\Http\Controllers\admin\setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\setting\Setting;

class SettingController extends Controller
{
    public function index()
    {   
        return view('admin.setting.listing');
    }

    public function store(Request $request)
    {
        
        $settings = Setting::where(['eConfigType' => $request->eConfigType, 'eStatus'=>'Active'])->get()->toArray();

        foreach ($settings as $key => $value) {
            if($value['vName'] == 'COMPANY_FAVICON'){
                $image = 'favicon.png';
                $setting = Setting::find($value['id']);

                if ($request->hasFile($value['vName'])) {
                    $imageName      = $image;
                    $path           = public_path('uploads/logo');
                    $request[$value['vName']]->move($path, $imageName);
                }
                $setting['vValue'] = $image;

                $setting->save();
                
            }
            else if($value['vName'] == 'COMPANY_LOGO'){
                $image = 'logo.png';
                $setting = Setting::find($value['id']);

                if($request->hasFile($value['vName']) == 'true'){
                    if ($request->hasFile($value['vName'])) {
                        $imageName      = $image;
                        $path           = public_path('uploads/logo');
                        $request[$value['vName']]->move($path, $imageName);
                    }
                    $setting['vValue'] = $imageName;

                    $setting->save();
                }
            }
            else{
                $setting = Setting::find($value['id']);
                $setting['vValue'] = $request[$value['vName']];

                $setting->save();
            }

            
        }
        return redirect()->back()->withSuccess('Record updated successfully.');;
    }

    public function edit($eConfigType)
    {
        $data['eConfigType'] = $eConfigType;

        $data['settings'] = Setting::where(['eConfigType' => $eConfigType, 'eStatus'=>'Active'])->get()->toArray();

        return view('admin.setting.create')->with($data);
    }


}
