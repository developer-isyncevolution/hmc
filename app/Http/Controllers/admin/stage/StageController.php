<?php

namespace App\Http\Controllers\admin\stage;

use App\Helper\GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\admin\stage\Stage;
use App\Models\admin\impression\Impression;
use App\Models\admin\categoryproduct\CategoryProduct;
use App\Models\admin\productstage\ProductStage;
use App\Models\admin\toothshades\ToothShades;
use App\Models\admin\customer\Customer;
use App\Models\admin\gumshades\GumShades;
use App\Models\admin\toothbrand\ToothBrand;
use App\Models\admin\gumbrand\GumBrand;
use App\Models\admin\fees\Fees;
use Illuminate\Http\Request;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;
use Validator;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use App\Imports\ImportTothshades;
use Maatwebsite\Excel\Facades\Excel;

class StageController extends Controller
{
    public function index()
    {    
        return view('admin.stage.listing');
    }
    
    public function ajax_listing(Request $request)
    {
        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iStageId";
            $order = "DESC";
        }

        if($action == "search" || $action == "sort"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iStageId']    = $request->id;

            Stage::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Stage_ID = (explode(",",$request->id));

            foreach ($Stage_ID as $key => $value) {
                $where                 = array();
                $where['iStageId']    = $value;

                Stage::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $Admin_data = Stage::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = Stage::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        return view('admin.stage.ajax_listing')->with($data);   
    }

    public function create()
    {
        return view('admin.stage.create');
    }

    public function store(Request $request){

        $iStageId = $request->id;
      
        $image = $request->file('vImage');
        if ($request->hasFile('vImage')) {
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/stage');
            $img            = Image::make($image->getRealPath()); 
            $img->resize(50, 50, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.'/'.$imageName);
            $request->vImage->move($path, $imageName);
        }
        
        $data['vName']        = $request->vName;
        $data['iCustomerTypeId']= $request->iCustomerTypeId;
        $data['vEmail']       = $request->vEmail;
        $data['vMobile']      = $request->vMobile;
        $data['vCellulor']    = $request->vCellulor;
        $data['vPassword']    = $request->vPassword;
        $data['vTitle']       = $request->vTitle;
        $data['vWebsiteName'] = $request->vWebsiteName;
        $data['tDescription'] = $request->tDescription;
        $data['vOfficeName']  = $request->vOfficeName;
        $data['vAddress']     = $request->vAddress;
        $data['vCity']        = $request->vCity;
        $data['iStateId']     = $request->iStateId;
        $data['vZipCode']     = $request->vZipCode;
        $data['eStatus']      = $request->eStatus;
        

        if($iStageId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if ($request->hasFile('vImage')) {   
            $data['vImage']    = $imageName;
        }

        if(!empty($iStageId)){
            $where               = array();
            $where['iStageId']         = $iStageId;

            $Stage_ID = new Stage();
            $Stage_ID->update($where, $data);

            return redirect()->route('admin.stage')->withSuccess('Stage updated successfully.');
        }
        else{
            $data = Stage::add($data);

            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            return redirect()->route('admin.stage')->withSuccess('Stage created successfully.');
        }
    }

    public function edit($iStageId)
    {
        $data['stages'] = Stage::get_by_id($iStageId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['stages']->iCustomerId) && $data['stages']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['stages']->iCustomerId)))
        {
            return redirect()->route('admin.stage')->withError('Invalid Input');
        }
        return view('admin.stage.create')->with($data);
    }

    public function impression()
    {    
        $data['customer'] = Customer::get_all_customer();
        return view('admin.stage.impression_listing',$data);
    }
    
    public function impression_ajax_listing(Request $request)
    {
        // dd($request);
        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search" || $action == "sort"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iImpressionId']    = $request->id;

            Impression::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Impression_ID = (explode(",",$request->id));

            foreach ($Impression_ID as $key => $value) {
                $where                 = array();
                $where['iImpressionId']    = $value;

                Impression::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        // For Perticular customer show start
        if(isset($request->iCustomerId) && !empty($request->iCustomerId) )
        {
            $criteria['iCustomerId'] =  $request->iCustomerId;
        }
        else
        {
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        }
        // dd($criteria);
        // For Perticular customer show end
        $Impression_data = Impression::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Impression_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = Impression::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        return view('admin.stage.impression_ajax_listing')->with($data);   
    }

    public function impression_create()
    {
        return view('admin.stage.impression_create');
    }

    public function impression_store(Request $request){

        $iImpressionId = $request->id;
        
        $data['vName']        = $request->vName;
        $data['vCode']        = $request->vCode;
        $data['iSequence']    = $request->iSequence;
        $data['eStatus']      = $request->eStatus;
        
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end

        if($iImpressionId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        if(!empty($iImpressionId)){
            $where               = array();
            $where['iImpressionId']         = $iImpressionId;

            $Impression_ID = new Impression();
            $Impression_ID->update($where, $data);

            return redirect()->route('admin.impression')->withSuccess('Impression updated successfully.');
        }
        else{
            $data = Impression::add($data);

            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            return redirect()->route('admin.impression')->withSuccess('Impression created successfully.');
        }
    }

    public function impression_edit($iImpressionId)
    {
        $data['impressions'] = Impression::get_by_id($iImpressionId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['impressions']->iCustomerId) && $data['impressions']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['impressions']->iCustomerId)))
        {
            return redirect()->route('admin.stage')->withError('Invalid Input');
        }
        return view('admin.stage.impression_create')->with($data);
    }
    public function toothshades()
    {    
        $data['brand'] = ToothShades::get_all_brand();
        $data['customer'] = Customer::get_all_customer();
        return view('admin.stage.toothshades_listing')->with($data);
    }
    
    public function toothshades_ajax_listing(Request $request)
    {

        $action = $request->action;
        $iToothBrandId = $request->iToothBrandId;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search" || $action == "sort"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iToothShadesId']    = $request->id;

            ToothShades::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $ToothShades_ID = (explode(",",$request->id));

            foreach ($ToothShades_ID as $key => $value) {
                $where                 = array();
                $where['iToothShadesId']    = $value;

                ToothShades::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        // For Perticular customer show start
       
        if(isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $criteria['iCustomerId'] =  $request->iCustomerId;
        }
        else
        {
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        }
        // For Perticular customer show end
        $ToothShades_data = ToothShades::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($ToothShades_data);

        // $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        // $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }
        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        // dd($paginator->itemsPerPage);
        if($request->limit_page !='')
        {
            $limit = $request->limit_page;
        }
        else
        {
            $limit = $paginator->itemsPerPage;
        }

        $paginator->is_ajax = true;
        $paging = true;
        // dd($limit);
        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iToothBrandId'] = $iToothBrandId;

        $data['data'] = ToothShades::get_all_data($criteria, $start, $limit, $paging);
        
        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        return view('admin.stage.toothshades_ajax_listing')->with($data);   
    }
    
    public function toothshades_create()
    {
        
        $data['brand'] = ToothShades::get_all_brand();
        return view('admin.stage.toothshades_create')->with($data);
    }

    public function toothshades_store(Request $request){

        $iToothShadesId = $request->id;
        
        $data['iToothBrandId']= $request->iToothBrandId;
        $data['vCode']        = $request->vCode;
        $data['iSequence']    = $request->iSequence;
        $data['eStatus']      = $request->eStatus;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end

        if($iToothShadesId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        if(!empty($iToothShadesId)){
            $where               = array();
            $where['iToothShadesId']         = $iToothShadesId;

            $ToothShades_ID = new ToothShades();
            $ToothShades_ID->update($where, $data);

            return redirect()->route('admin.toothshades')->withSuccess('ToothShades updated successfully.');
        }
        else{
            $data = ToothShades::add($data);

            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            return redirect()->route('admin.toothshades')->withSuccess('ToothShades created successfully.');
        }
    }

    public function toothshades_edit($iToothShadesId)
    {
        $data['brand'] = ToothShades::get_all_brand();
        $data['toothshades'] = ToothShades::get_by_id($iToothShadesId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['toothshades']->iCustomerId) && $data['toothshades']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['toothshades']->iCustomerId)))
        {
            return redirect()->route('admin.stage')->withError('Invalid Input');
        }
        return view('admin.stage.toothshades_create')->with($data);
    }
    public function toothshadesbrand()
    {    
        $data['customer'] = Customer::get_all_customer();
        return view('admin.stage.toothshadesbrand_listing',$data);
    }
    
    public function toothshadesbrand_ajax_listing(Request $request)
    {
        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search" || $action == "sort"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iToothBrandId']    = $request->id;

            ToothBrand::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $ToothBrand_ID = (explode(",",$request->id));

            foreach ($ToothBrand_ID as $key => $value) {
                $where                 = array();
                $where['iToothBrandId']    = $value;

                ToothBrand::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        // For Perticular customer show start
        if(isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $criteria['iCustomerId'] = $request->iCustomerId;
        }
        else
        {
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        }
        
        // For Perticular customer show end
       
        $Admin_data = ToothBrand::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = ToothBrand::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        return view('admin.stage.toothshadesbrand_ajax_listing')->with($data);   
    }

    public function toothshadesbrand_create()
    {
        return view('admin.stage.toothshadesbrand_create');
    }
    public function toothshadesbrand_store(Request $request){
        $iToothBrandId = $request->id;
        
        $data['vName']        = $request->vName;
        $data['iSequence']    = $request->iSequence;
        $data['eStatus']      = $request->eStatus;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end

        if($iToothBrandId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        if(!empty($iToothBrandId)){
            $where               = array();
            $where['iToothBrandId']         = $iToothBrandId;

            $ToothBrand_ID = new ToothBrand();
            $ToothBrand_ID->update($where, $data);

            return redirect()->route('admin.toothshadesbrand')->withSuccess('toothshadesbrand updated successfully.');
        }
        else{
            $data = ToothBrand::add($data);

            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            if(empty($request->iSequence)){
                return redirect()->route('admin.stage.toothshades_create')->withSuccess('toothshadesbrand created successfully.');
            }else{

                return redirect()->route('admin.toothshadesbrand')->withSuccess('toothshadesbrand created successfully.');
            }

            // return redirect()->route('admin.toothshadesbrand')->withSuccess('toothshadesbrand created successfully.');
        }
    }
    public function toothshadesbrand_edit($iToothBrandId)
    {
        $data['toothBrands'] = ToothBrand::get_by_id($iToothBrandId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['toothBrands']->iCustomerId) && $data['toothBrands']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['toothBrands']->iCustomerId)))
        {
            return redirect()->route('admin.stage')->withError('Invalid Input');
        }
        return view('admin.stage.toothshadesbrand_create')->with($data);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $toothbrand = ToothBrand::find($id);
        $toothbrand->delete();

        return redirect()->route('admin.toothshades')->withSuccess('ToothBrand deleted successfully.');
    }
    public function gumshades()
    {    
        $data['brand'] = GumShades::get_all_brand();
        $data['customer'] = Customer::get_all_customer();
        return view('admin.stage.gumshades_listing')->with($data);
    }
    
    public function gumshades_ajax_listing(Request $request)
    {
        $action = $request->action;
        $iGumBrandId = $request->iGumBrandId;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search" || $action == "sort"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iGumShadesId']    = $request->id;

            GumShades::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $GumShades_ID = (explode(",",$request->id));

            foreach ($GumShades_ID as $key => $value) {
                $where                 = array();
                $where['iGumShadesId']    = $value;

                GumShades::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        // For Perticular customer show start
        if(isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $criteria['iCustomerId'] = $request->iCustomerId;
        }
        else
        {
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        }
        // For Perticular customer show end
        $GumShades_data = GumShades::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($GumShades_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iGumBrandId'] = $iGumBrandId;
        
        $data['data'] = GumShades::get_all_data($criteria, $start, $limit, $paging);
        
        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        return view('admin.stage.gumshades_ajax_listing')->with($data);   
    }
    
    public function gumshades_create()
    {
        
        $data['brand'] = GumShades::get_all_brand();
        return view('admin.stage.gumshades_create')->with($data);
    }

    public function gumshades_store(Request $request){

        $iGumShadesId = $request->id;
        
        $data['iGumBrandId'] = $request->iGumBrandId;
        $data['vCode']        = $request->vCode;
        $data['vGumShade']        = $request->vGumShade;
        $data['iSequence']    = $request->iSequence;
        $data['eStatus']      = $request->eStatus;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end

        if($iGumShadesId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        if(!empty($iGumShadesId)){
            $where               = array();
            $where['iGumShadesId']         = $iGumShadesId;

            $GumShades_ID = new GumShades();
            $GumShades_ID->update($where, $data);

            return redirect()->route('admin.gumshades')->withSuccess('GumShades updated successfully.');
        }
        else{
            $data = GumShades::add($data);

            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            return redirect()->route('admin.gumshades')->withSuccess('GumShades created successfully.');
        }
    }

    public function gumshades_edit($iGumShadesId)
    {
        $data['brand'] = GumShades::get_all_brand();
        $data['gumshades'] = GumShades::get_by_id($iGumShadesId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['gumshades']->iCustomerId) && $data['gumshades']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['gumshades']->iCustomerId)))
        {
            return redirect()->route('admin.stage')->withError('Invalid Input');
        }
        return view('admin.stage.gumshades_create')->with($data);
    }
    public function gumbrand()
    {    
        $data['customer'] = Customer::get_all_customer();
        return view('admin.stage.gumbrand_listing',$data);
    }
    
    public function gumbrand_ajax_listing(Request $request)
    {
        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search" || $action == "sort"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iGumBrandId']    = $request->id;

            GumBrand::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $GumBrand_ID = (explode(",",$request->id));

            foreach ($GumBrand_ID as $key => $value) {
                $where                 = array();
                $where['iGumBrandId']    = $value;

                GumBrand::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        // For Perticular customer show start
        if(isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $criteria['iCustomerId'] = $request->iCustomerId;
        }
        else
        {
            $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        }
        // For Perticular customer show end
        $Admin_data = GumBrand::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = GumBrand::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        return view('admin.stage.gumbrand_ajax_listing')->with($data);   
    }

    public function gumbrand_create()
    {
        return view('admin.stage.gumbrand_create');
    }
    public function gumbrand_store(Request $request){

        $iGumBrandId = $request->id;
        
        $data['vName']        = $request->vName;
        $data['iSequence']    = $request->iSequence;
        $data['eStatus']      = 'Active';
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end

        if($iGumBrandId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        if(!empty($iGumBrandId)){
            $where               = array();
            $where['iGumBrandId']         = $iGumBrandId;

            $GumBrand_ID = new GumBrand();
            $GumBrand_ID->update($where, $data);

            return redirect()->route('admin.gumbrand')->withSuccess('Gumbrand updated successfully.');
        }
        else{
            $data = GumBrand::add($data);

            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));

            if(empty($request->iSequence)){
                return redirect()->route('admin.stage.gumshades_create')->withSuccess('Gumbrand created successfully.');
            }else{

                return redirect()->route('admin.gumbrand')->withSuccess('Gumbrand created successfully.');
            }
        }
    }
    public function gumbrand_edit($iToothBrandId)
    {
        $data['gumBrands'] = GumBrand::get_by_id($iToothBrandId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['gumBrands']->iCustomerId) && $data['gumBrands']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['gumBrands']->iCustomerId)))
        {
            return redirect()->route('admin.stage')->withError('Invalid Input');
        }
        return view('admin.stage.gumbrand_create')->with($data);
    }
    public function gumbrand_delete(Request $request)
    {
        $id = $request->id;
        $Gumbrand = GumBrand::find($id);
        $Gumbrand->delete();

        return redirect()->route('admin.toothshades')->withSuccess('GumBrand deleted successfully.');
    }
    public function notes()
    {    
        return view('admin.stage.notes_listing');
    }
    public function rushdate()
    {    
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $criteria['eType'] = 'Upper';
        $data['uppercategory'] = CategoryProduct::get_all_category($criteria);
        
        $criteria['eType'] = 'Lower';
        $data['lowercategory'] = CategoryProduct::get_all_category($criteria);

        $criteria['eType'] = 'Upper';
        $data['upperproduct'] = CategoryProduct::get_all_product($criteria);
        
        $criteria['eType'] = 'Lower';
        $data['lowerproduct'] = CategoryProduct::get_all_product($criteria);

        $criteria['eType'] = 'Upper';
        $data['upperstage'] = CategoryProduct::get_all_stage($criteria);
        
        $criteria['eType'] = 'Lower';
        $data['lowerstage'] = CategoryProduct::get_all_stage($criteria);
        return view('admin.stage.rushdate_listing')->with($data);
    }
    
    public function rushdate_ajax_listing(Request $request)
    {
        $action = $request->action;
        $iProductStageId = $request->iProductStageId;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iProductStageId";
            $order = "DESC";
        }

        if($action == "search" || $action == "sort"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iProductStageId']    = $request->id;

            ProductStage::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $ProductStage_ID = (explode(",",$request->id));

            foreach ($ProductStage_ID as $key => $value) {
                $where                 = array();
                $where['iProductStageId']    = $value;

                ProductStage::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $Admin_data = ProductStage::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iProductStageId'] = $iProductStageId;

        $data['data'] = ProductStage::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        return view('admin.stage.rushdate_ajax_listing')->with($data);   
    }
    public function fetch_categoryproduct(Request $request)
    {
        $iCategoryId = $request->iCategoryId;
        // For Perticular customer show start
        $iCustomerId = General::admin_info()['iCustomerId'];
        if(isset($iCustomerId) && !empty($iCustomerId ))
        {
            $where_array = [
                'iCategoryId'=>$iCategoryId,
                'iCustomerId'=>$iCustomerId,
            ];    
        }
        else
        {
            $where_array = [
                'iCategoryId'=>$iCategoryId
            ];
        }
        // For Perticular customer show end
        $data['data'] = CategoryProduct::where($where_array)
                                ->orderBy("iSequence", "ASC")
                                ->get();
        return view('admin.stage.ajax_fetch_categoryproduct_listing')->with($data);
    }
    public function fetch_productstage(Request $request)
    {
        $iCategoryProductId = $request->iCategoryProductId;
         // For Perticular customer show start
         $iCustomerId = General::admin_info()['iCustomerId'];
         if(isset($iCustomerId) && !empty($iCustomerId ))
         {
             $where_array = [
                 'iCategoryProductId'=>$iCategoryProductId,
                 'iCustomerId'=>$iCustomerId,
             ];    
         }
         else
         {
             $where_array = [
                 'iCategoryProductId'=>$iCategoryProductId
             ];
         }
         // For Perticular customer show end
        $data['data'] = ProductStage::where($where_array)
                                    ->orderBy("iSequence", "ASC")
                                    ->get();
        return view('admin.stage.ajax_fetch_productstage_listing')->with($data);
    }

    public function rushdate_count_ajax_listing(Request $request)
    {
        $action = $request->action;
        $iProductStageId = $request->iProductStageId;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iFeesId";
            $order = "DESC";
        }

        if($action == "search" || $action == "sort"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iFeesId']    = $request->id;

            Fees::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Fees_ID = (explode(",",$request->id));

            foreach ($Fees_ID as $key => $value) {
                $where                 = array();
                $where['iFeesId']    = $value;

                Fees::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $Admin_data = Fees::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iProductStageId'] = $iProductStageId;

        $data['data'] = Fees::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        return view('admin.stage.rushdate_count_ajax_listing')->with($data);   
    }
    public function uppercount_create(Request $request)
    {
        $iProductStageId         = $request->iProductStageId;
        $data['productstage']    = ProductStage::get_by_id($iProductStageId);
        // dd($data['productstage']);
        return view('admin.stage.uppercount_create')->with($data);
    }
    public function uppercount_store(Request $request){

        
        $iFeesId = $request->id;
        // $data['iStagePrice']         = $request->iStagePrice;
        $data['vFee']         = $request->vFee;
        $data['iWorkingTime'] = $request->iWorkingTime;
        // $data['vRushFee']     = $request->vRushFee;
        // $data['vTotalRush']   = $request->vTotalRush;
        $data['eStatus']      = $request->eStatus;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end

        if($iFeesId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        if(!empty($iFeesId)){
            $where               = array();
            $where['iFeesId']         = $iFeesId;

            $Fees_ID = new Fees();
            $Fees_ID->update($where, $data);

            return redirect()->route('admin.rushdate')->withSuccess('Fees updated successfully.');
        }
        else{
            $data['iProductStageId']         = $request->iProductStageId;
            $data = Fees::add($data);
             return redirect()->route('admin.rushdate')->withSuccess('Fees created successfully.');
        }
    }
    public function uppercount_edit($iFeesId)
    {
        $data['uppercounts'] = Fees::get_by_id($iFeesId);
        $iProductStageId = $data['uppercounts']->iProductStageId;
        $data['productstage']    = ProductStage::get_by_id($iProductStageId);
        return view('admin.stage.uppercount_create')->with($data);
    }
    public function toothshades_import(Request $request)
    {
        $validatorRules = [
            'vBrandFile'            => 'required|mimes:csv,xlsx,xls',
        ];
        $validatorCustomMsg = [
            'vBrandFile.required'           => 'Please select file.',
            'vBrandFile.mimes'              => 'Please upload file in these format only (xlx, csv).',
        ];
        
        $validator = Validator::make($request->all(), $validatorRules, $validatorCustomMsg);

        if ($validator->fails()) {
            // return redirect()->back()>withError($validator);
            return redirect()->route('admin.toothshades')->withError('Invalid file type');
        }
        else
        {
            $iToothBrandId = $request->iToothBrandIdModal;
            $vBrandFile      = $request->vBrandFile->getClientOriginalName();
            $path           = public_path('uploads/toothshades_files'); 
            $request->vBrandFile->move($path, $vBrandFile);
            $iCustomerId = General::admin_info()['iCustomerId'];
            $import = new ImportTothshades($vBrandFile,$iToothBrandId,$iCustomerId);
            $importRes = Excel::import($import,  public_path('uploads/toothshades_files/'.$vBrandFile));  
            return redirect()->back()->withSuccess('Import sheet successfully.');
            
        }
    }
}



