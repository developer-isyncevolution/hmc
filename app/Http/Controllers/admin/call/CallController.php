<?php

namespace App\Http\Controllers\admin\call;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\call\Call;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;

class CallController extends Controller
{
    public function index()
    {
        return view('admin.call.listing');
    }

    public function ajax_listing(Request $request)
    {

        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iCallId";
            $order = "DESC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iCallId']    = $request->id;

            Call::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Call_ID = (explode(",",$request->id));

            foreach ($Call_ID as $key => $value) {
                $where                 = array();
                $where['iCallId']    = $value;

                Call::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $Call_data = Call::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Call_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = Call::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        $name = session('data');
        $data['username'] = $name['vName'];
        
        return view('admin.call.ajax_listing')->with($data);   
    }

    public function create()
    {
        $name = session('data');
        $data['username'] = $name['vName'];
        // $data['username'] = Session::get('name');
        return view('admin.call.create')->with($data);
    }

   public function store(Request $request)
    {
        $iCallId = $request->id;
        $data['tTime'] 	= $request->tTime;
        $data['dDate'] 	= $request->dDate;
        $data['vName']     = $request->vName;
        $data['tDescription']= $request->tDescription;
        // $data['eStatus']   = $request->eStatus;
                
        if($iCallId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iCallId)){
            $where                      = array();
            $where['iCallId']       = $iCallId;

            $Call_id = new Call();
            $Call_id->update($where, $data);

            return redirect()->route('admin.call')->withSuccess('Call updated successfully.');
        }
        else{
            $Call_id = Call::add($data);

            return redirect()->route('admin.call')->withSuccess('Call created successfully.');
        }
    }

    public function edit($iCallId)
    {
        $name = session('data');
        $data['username'] = $name['vName'];
        $data['calls'] = Call::get_by_id($iCallId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['calls']->iCustomerId) && $data['calls']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['calls']->iCustomerId)))
        {
            return redirect()->route('admin.call')->withError('Invalid Input');
        }
        return view('admin.call.create')->with($data);
    }

    public function destroy($id)
    {
        $Call = Call::find($id);
        $Call->delete();

        return redirect()->route('admin.call')->withSuccess('Call deleted successfully.');
    }

    public function multiDelete(Request $request)
    {
        $id = $request->id;

        Call::whereIn('id',explode(",",$id))->delete();
    }
}
