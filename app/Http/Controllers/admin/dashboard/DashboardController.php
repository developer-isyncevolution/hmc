<?php

namespace App\Http\Controllers\admin\dashboard;

use App\Http\Controllers\Controller;
use App\Models\admin\user\User;
use App\Models\admin\admin\Admin;
use App\Models\admin\state\State;
use App\Models\admin\customer\Customer;
use App\Models\admin\customertype\CustomerType;
use App\Models\admin\schedule\Schedule;
use App\Models\admin\office\Office;
use App\Models\admin\doctor\Doctor;
use Illuminate\Http\Request;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;
use App\Models\admin\labOffice\LabOffice;
use App\Models\admin\office_admin\OfficeAdmin;
use App\Models\admin\lab_admin\LabAdmin;
use App\Models\admin\staff\Staff;
use App\Models\admin\system_email\SystemEmail;

class DashboardController extends Controller
{
	// public function __construct()
	// {
	//     $this->middleware('auth');
	// }
    public function index()
    {
        // Office Request Start
        $per_page = Session::forget('per_page_lan');
        $iDepartmentId = General::admin_info()['iDepartmentId'];
        $iCustomerId = General::admin_info()['iCustomerId'];
        $iCountryId = "223";
        $data['states'] = State::get_by_iCountryId($iCountryId);
        $data['data'] = Customer::get_by_id($iCustomerId);
        $data['Labs'] = Customer::get_all_lab();
        $data['LabOffice'] = LabOffice::get_by_officeid($iCustomerId);
        $LabOffice_labids = array();
        foreach ($data['LabOffice'] as $key => $value) {
            $LabOffice_labids[] = $value->iLabId;
        }
        $data['LabOffice_labids'] = $LabOffice_labids;
        // Office Request End

        // Lab Request Start
        $data['data'] = Customer::get_by_id($iCustomerId);
        $data['Office_request'] = LabOffice::get_by_labid($iCustomerId);
        // Lab Request End

    	$data['customers']  = Customer::get_all_active_customer();
    	$data['users'] 		= User::get_all_active_user();
    	$data['offices'] 	= Office::get_all_active_office();
    	$data['doctors'] 	= Doctor::get_all_active_doctor();
    	return view('admin.dashboard.dashboard')->with($data);
    }

	public function ajax_listing(Request $request)
    {
		$action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iCustomerId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }
        
        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
		$criteria['eStatus'] = 'Pending';
		$criteria['iCustomerTypeId'] = $request->iCustomerTypeId;

        $Admin_data = Customer::get_all_data($criteria);
        $pages = 1;
        if($request->pages != "")
        {
            $pages = $request->pages;
        }
        $paginator = new Paginator($pages);
     
        
        if($request->limit_page !='')
        {
            $per_page = $request->limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->total = count($Admin_data);
        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;


        $paginator->is_ajax = true;
        $paging = true;
        
        $criteria['start']  = $start;
        $criteria['limit_page']  = $limit;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['eStatus'] = 'Pending';
		$criteria['iCustomerTypeId'] = $request->iCustomerTypeId;
        // dd($criteria['iCustomerTypeId']);
        $data['data'] = Customer::get_all_data($criteria, $start, $limit, $paging);
        // dd($paginator->paginate());
        if(isset($request->iCustomerTypeId) && $request->iCustomerTypeId ==1)
        {
            $data['paging'] = str_replace('ajax_page','ajax_page_lab',$paginator->paginate());
        }
        else
        {
            $data['paging'] = $paginator->paginate();
        }
        $data['start']  = $start;
        $data['limit']  = $limit;
        return view('admin.dashboard.customer_ajax_listing')->with($data);   
    }

	public function status_change($id = '') {
		
		$data['eStatus'] = 'Active';
		$where = array("iCustomerId" => $id);
		$customer = new Customer();
		$customer->update($where,$data);
		return redirect()->route('admin.dashboard')->withSuccess('Customer Activated successfully.');
	}


    public function viewCustomerRequest($id) {
        $data['data'] = Customer::get_by_id($id);
        
        $customerTypeData = CustomerType::get_by_id($data['data']->iCustomerTypeId);
        $data['data']->vCustomerType = $customerTypeData->vTypeName;

        $stateData = State::get_by_id($data['data']->iStateId);
        $data['data']->vState = $stateData->vState;
        
        return view('admin.dashboard.viewCustomerRequest')->with($data);   
    }

    public function approveCustomer($id) {


        $customerData = Customer::get_by_id($id);

        $data['eStatus'] = 'Active';
        $where = array("iCustomerId" => $id);
        $customer = new Customer();
        $customer->update($where,$data);


          // Customer type lab start
          if(isset($customerData->iCustomerTypeId) && $customerData->iCustomerTypeId ==1)
          {
              $dataLabAdmin['iCustomerId']   = $id;
              $dataLabAdmin['vName']      = $customerData->vName;
              $dataLabAdmin['vEmail']    = $customerData->vEmail;
              $dataLabAdmin['vMobile']    = $customerData->vMobile;
              $dataLabAdmin['vCellulor']  = isset($customerData->vCellulor)?$customerData->vCellulor:'';
              $dataLabAdmin['vPassword']    = '';
              $dataLabAdmin['eStatus']       = 'Active';
              $dataLabAdmin['dtAddedDate'] = date("Y-m-d H:i:s");
              $iLabAdminId = LabAdmin::add($dataLabAdmin);
              
              $data_staff['iDepartmentId']= 2;
              $data_staff['vMiddleInitial']= $customerData->vName;
              $data_staff['vFirstName']= $customerData->vName;
              $data_staff['vUserName']= $customerData->vName;
              $data_staff['vEmail']= $customerData->vEmail;
              $data_staff['vPassword']= '';
              $data_staff['vMobile']= $customerData->vMobile;
              $data_staff['eStatus']= 'Active';
              $data_staff['iLoginAdminId']= $iLabAdminId;
              $data_staff['dtAddedDate'] = date("Y-m-d H:i:s");
              $data_staff['iCustomerId'] = $id;
            
              Staff::add($data_staff);
          }   
          elseif(isset($customerData->iCustomerTypeId) && $customerData->iCustomerTypeId ==6)
          {
              $dataOfficeAdmin['iCustomerId']   = $id;
              $dataOfficeAdmin['vName']      = $customerData->vName;
              $dataOfficeAdmin['vEmail']    = $customerData->vEmail;
              $dataOfficeAdmin['vMobile']    = $customerData->vMobile;
              $dataOfficeAdmin['vCellulor']  = isset($customerData->vCellulor)?$customerData->vCellulor:'';
              $dataOfficeAdmin['vPassword']    = '';
              $dataOfficeAdmin['eStatus']       = 'Active';
              $dataOfficeAdmin['dtAddedDate'] = date("Y-m-d H:i:s");
              $iOfficeAdminId = OfficeAdmin::add($dataOfficeAdmin);
  
              $data_staff['iDepartmentId']= 3;
              $data_staff['vMiddleInitial']= $customerData->vName;
              $data_staff['vFirstName']= $customerData->vName;
              $data_staff['vUserName']= $customerData->vName;
              $data_staff['vEmail']= $customerData->vEmail;
              $data_staff['vPassword']= '';
              $data_staff['vMobile']= $customerData->vMobile;
              $data_staff['eStatus']= 'Active';
              $data_staff['iLoginAdminId']= $iOfficeAdminId;
              $data_staff['dtAddedDate'] = date("Y-m-d H:i:s");
              $data_staff['iCustomerId'] = $id;
              Staff::add($data_staff);
               // Customer type office start
          }   
          // Email send start

            $auth_code = md5($customerData->vEmail);
            // / EMAIL To Forgot Password /
            $criteria = array();
            $criteria['vEmailCode'] = 'USER_SET_PASSWORD';
            $email = SystemEmail::get_email_by_code($criteria);
            $company_setting = General::setting_info('company');
            $subject = str_replace("#SYSTEM.COMPANY_NAME#", $company_setting['COMPANY_NAME']['vValue'], $email->vEmailSubject);
            $constant   = array('#vName#','#activation_link#');
            $value      = array($customerData->vName, route('admin.reset_password',$auth_code));
            
            $message = str_replace($constant, $value, $email->tEmailMessage);
            
            $email_data['to']       = $customerData->vEmail;
            // $email_data['to']       = 'kasim.abbas@isyncevolution.com';
            $email_data['subject']  = $subject;
            $email_data['msg']      = $message;
            $email_data['from']     = $email->vFromEmail;
            // dd($email_data);
            General::send('USER_SET_PASSWORD', $email_data);
            // / EMAIL To Forgot Password /

        // Update schedule end
        if($customerData->iCustomerTypeId == '1')
        {
            $WeekData = array ('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
            $NotWeekData = array ('Sunday','Friday','Saturday');
            foreach($WeekData as $key=>$WeekDataVal)
            {
                $schedule_data['iCustomerId'] =  $id;
                $schedule_data['eMonth']         = 'All';
                $schedule_data['eWeek']          = $WeekDataVal;
                $schedule_data['eWeekMonth']     = 'All';
                if(in_array($WeekDataVal,$NotWeekData))
                {     
                    $schedule_data['eStatus']        = 'CLOSED';
                    $schedule_data['vTitle']         = 'Weekend';
                    $schedule_data['tiOpen']         = Null;
                    $schedule_data['tiClose']        = Null;
                }
                else
                {
                    $schedule_data['eStatus']        = 'OPEN';
                    $schedule_data['vTitle']         = 'Open';
                    $schedule_data['tiOpen']         = '10:00:00';
                    $schedule_data['tiClose']        = '16:00:00';
                }
                 Schedule::add($schedule_data);
            }
            
            // return redirect()->route('admin.lab_admin.create_staff', ['iCustomerId' => $id]);
        }
        elseif($customerData->iCustomerTypeId == '6')
        {
            // return redirect()->route('admin.office_admin.create_staff', ['iCustomerId' => $id]);
        }


        return redirect()->route('admin.dashboard')->withSuccess('Customer Activated successfully.');
    }

    public function rejectCustomer($id) {

        $data['eStatus'] = 'Active';
        $where = array("iCustomerId" => $id);
        $customer = new Customer();
        $customer->update($where,$data);
        
        return redirect()->route('admin.dashboard')->withSuccess('Request rejected');
    }

    public function LabProfileAction(Request $request)
    {
        $iLabOfficeId  = $request->iLabOfficeId;
        $action  = $request->action;
        $iCustomerId = General::admin_info()['iCustomerId'];
        if (isset($iLabOfficeId) && !empty($iLabOfficeId) && !empty($action)) {
            $lab_office_data = array();
            $lab_office_data['eStatus']      = $action;
            $where['iLabOfficeId']     = $iLabOfficeId;
            labOffice::update_data($where,$lab_office_data);
            return 'success';
        }
        else
        {
            return 'error';
        }

        //   if ((isset($request->reject_list) && !empty($request->reject_list)) || (isset($request->approve_list) && !empty($request->approve_list))) {
        //     if (isset($request->reject_list) && !empty($request->reject_list)) {
        //         foreach ($request->reject_list as $key => $value) {
        //             $LabOfficeData = array();
        //             $LabOfficeData['eStatus'] = "Inactive"; 

        //             $where = array();
        //             $where['iLabOfficeId']     = $value;

        //             LabOffice::update_data($where,$LabOfficeData);
        //         }
        //     }

        //     if (isset($request->approve_list) && !empty($request->approve_list)) {
        //         foreach ($request->approve_list as $key => $value) {
        //             $LabOfficeData = array();
        //             $LabOfficeData['eStatus'] = "Active"; 

        //             $where = array();
        //             $where['iLabOfficeId']     = $value;

        //             LabOffice::update_data($where,$LabOfficeData);
        //         }
        //     }
        //     return redirect()->route('admin.dashboard')->withSuccess('Request updated successfully.');
        // }
        // else
        // {
        //     return redirect()->route('admin.dashboard')->withError('Request not selected');
        // }
        // dd($data);
    }

    public function OfficeProfileAction(Request $request)
    {
        $iLabOfficeId  = $request->iLabOfficeId;
        $action  = $request->action;
        // dd($action);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if(isset($action) && $action =='Resend')
        {
            $dataResend = array();
            $dataResend['dtUpdatedDate'] = date("Y-m-d H:i:s");
            $whereResend['iOfficeId']   = $iCustomerId;
            $whereResend['iLabId']      = $iLabOfficeId;
            labOffice::update_data($whereResend,$dataResend);
            return 'success';
        }
        elseif (isset($iLabOfficeId) && !empty($iLabOfficeId) && !empty($action)) {
            $lab_office_data = array();
            $lab_office_data['eStatus']      = $action;
            $lab_office_data['iOfficeId']   = $iCustomerId;
            $lab_office_data['iLabId']      = $iLabOfficeId;
            $lab_office_data['dtAddedDate']      = date("Y-m-d H:i:s");
            $lab_office_data['dtUpdatedDate']      = date("Y-m-d H:i:s");
            labOffice::add($lab_office_data);
            return 'success';
        }
        else
        {
            return 'error';
        }
    }

    public function show_lab_office_details(Request $request)
    {
        $type  = $request->type;
        $iCustomerId  = $request->id;
        if(isset($type) && !empty($type) && isset($iCustomerId) && !empty($iCustomerId))
        {
            $data['data'] = Customer::get_by_id($iCustomerId);
            return view('admin.dashboard.customer_details')->with($data);
        }
        else
        {
            $data =  'error';
        }
        
        
    }
	


}
