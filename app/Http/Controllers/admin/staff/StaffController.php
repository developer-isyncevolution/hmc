<?php

namespace App\Http\Controllers\admin\staff;

use App\Http\Controllers\Controller;
use App\Models\admin\staff\Staff;
use App\Models\admin\customer\Customer;
use App\Models\admin\office_admin\OfficeAdmin;
use App\Models\admin\lab_admin\LabAdmin;
use App\Models\admin\doctor\Doctor;
use App\Models\admin\user_lab\UserLab;
use App\Models\admin\user\User;
use Illuminate\Http\Request;
use App\Libraries\Paginator;
use Validator;
use Session;
use App\Libraries\General;
use Image;

class StaffController extends Controller
{
    public function index()
    {   
        Session::forget('per_page_lan');
        $criteria = array();
        $iCustomerId = \App\Libraries\General::admin_info()['iCustomerId'];
        if(isset($iCustomerId) && $iCustomerId !=0)
        {
            $criteria['iCustomerId'] = $iCustomerId;
        }
        $data['department'] = Staff::get_all_department($criteria);
        $data['customer'] = Customer::get_all_customer();
        return view('admin.staff.listing')->with($data);
    }    
    public function ajax_listing(Request $request)
    {
        $action = $request->action;
        $iDepartmentId = $request->iDepartmentId;
        if(isset($request->iCustomerId) && $request->iCustomerId !=null)
        {
            $iCustomerId = $request->iCustomerId;
        }
        else
        {
            $iCustomerId =General::admin_info()['iCustomerId'];
        }
       
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iStaffId";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iStaffId']    = $request->id;

            Staff::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Staff_ID = (explode(",",$request->id));

            foreach ($Staff_ID as $key => $value) {
                $where                 = array();
                $where['iStaffId']    = $value;

                Staff::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['iDepartmentId'] = $iDepartmentId;
        $criteria['iCustomerId'] = $iCustomerId;

        // $Admin_data = Staff::all();
        // For Perticular customer show start
        // $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $Admin_data = Staff::get_all_data($criteria);
        $pages = 1;  

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($Admin_data);

        if($request->limit_page !='')
         {
             $per_page = $request->limit_page;
             Session::put('per_page_lan', $per_page);
             $paginator->itemsPerPage = $per_page;
             //$paginator->range = $per_page;
         }
         else
         {
             $per_page = Session::get('per_page_lan');
           
             if($per_page !='')
             {
                 $paginator->itemsPerPage = $per_page;
                //  //$paginator->range = $per_page;
                //  $limit = $per_page;
             }
             else
             {
                 $paginator->itemsPerPage = 50;
                //  //$paginator->range = 50;
             }
             
         }

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        // dd($paginator->itemsPerPage);
        if($request->limit_page !='')
        {
            $limit = $request->limit_page;
        }
        else
        {
            $limit = $paginator->itemsPerPage;
        }

        $paginator->is_ajax = true;
        $paging = true;
       
        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['iDepartmentId'] = $iDepartmentId;
        $criteria['iCustomerId'] = $iCustomerId;
        // dd($criteria);
        $data['data'] = Staff::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = $paginator->paginate();
        $data['limit_page']  = $limit;
        // dd($data);
        return view('admin.staff.ajax_listing')->with($data);   
    }

    public function create()
    {   
        $data['customer'] = Customer::get_all_customer();
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        $data['department'] = Staff::get_all_department($criteria);
        return view('admin.staff.create')->with($data);
    }

    public function store(Request $request){
   
        $iStaffId = $request->id;
        
        $data['vFirstName']   = $request->vFirstName;
        $data['vLastName']    = $request->vLastName;
        $data['vUserName']    = $request->vUserName;
        $data['iDepartmentId'] = $request->iDepartmentId;
        $data['vEmail']       = $request->vEmail;
        $data['vMobile']      = $request->vMobile;
        $data['tDescription'] = $request->tDescription;
        $data['vPassword']    = md5($request->vPassword);   
        $data['eStatus']      = $request->eStatus;
        if ($request->hasFile('vImage')) {
            $image = $request->file('vImage');
            $imageName      = time().'.'.$request->vImage->getClientOriginalName();
            $path           = public_path('uploads/staff');
            $img            = Image::make($image->getRealPath()); 
            $img->resize(50, 50, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.'/'.$imageName);
            $request->vImage->move($path, $imageName);
            $data['vImage']    = $imageName;
        }
        // For Perticular customer show start
        if(isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $data['iCustomerId'] = $request->iCustomerId;
        }
        else
        {
            $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        }
        
        // For Perticular customer show end
        if($iStaffId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iStaffId)){
            $where               = array();
            $where['iStaffId']         = $iStaffId;

            $Staff_ID = new Staff();
            $Staff_ID->update($where, $data);
            // dd($request);

            $iLoginAdminId = $request->iLoginAdminId;
            if(isset($iLoginAdminId) && !empty($iLoginAdminId) && isset($request->iCustomerId) && !empty($request->iCustomerId))
            {
                $iCustomerId = $request->iCustomerId;
                // Check this user exist in lab admin table 
                
                $exist_labAdmin = Staff::user_exit_in_labAdmin($iLoginAdminId,$iCustomerId);
                $exist_doctor = Staff::user_exit_in_doctor($iLoginAdminId,$iCustomerId);
                $exist_users = Staff::user_exit_in_users($iLoginAdminId,$iCustomerId);
             
             
                if($exist_labAdmin>0)
                {
                    $labData = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        'vEmail'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'vCellulor'=>$request->vCellulor,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $whereLabData = [
                        'iLabAdminId'=>$iStaffId,
                        'iCustomerId'=>$iCustomerId
                    ];
                    Staff::update_lab_data($labData,$whereLabData);
                }
                else if($exist_doctor>0)
                {
                    $labData = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        'vEmail'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'vCellulor'=>$request->vCellulor,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $whereDoctorData = array(
                        'iDoctorId'=>$iLoginAdminId,
                        'iCustomerId'=>$iCustomerId
                    );
                    Staff::update_doctor_data($labData,$whereDoctorData);
                }
                else if($exist_users>0)
                {
                    $labData = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        'vEmail'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'tDescription'=>$request->tDescription,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $whereStaffData = array(
                        'iUserId'=>$iLoginAdminId,
                        'iCustomerId'=>$iCustomerId
                    );
                    Staff::update_user_data($labData,$whereStaffData);
                }
               
            }
            if(isset($iLoginAdminId) && !empty($iLoginAdminId))
            {
                // Check this user exist in main admin table 
                // dd($request);
                $exist_superAdmin = '';
                $exist_office = '';
                $exist_lab = '';
                $exist_doc = '';
                $exist_lab_user = '';
                $exist_office_user = '';
                if($request->iDepartmentId ==3)
                {
                    $where_staff = [
                        'iOfficeAdminId'=>$iLoginAdminId,
                        'iCustomerId'=>$request->iCustomerId,
                    ];
                    $exist_office = OfficeAdmin::where($where_staff)->count();
                }
                elseif($request->iDepartmentId ==2)
                {
                    $where_lab = [
                        'iLabAdminId'=>$iLoginAdminId,
                        'iCustomerId'=>$request->iCustomerId,
                    ];
                    $exist_lab = LabAdmin::where($where_lab)->count();
                }
                elseif($request->iDepartmentId ==1)
                {
                    $exist_superAdmin = Staff::user_exit_in_superAdmin($iLoginAdminId);
                }
                elseif($request->iDepartmentId ==4)
                {
                    $where_doc = [
                        'iDoctorId'=>$iLoginAdminId,
                        'iCustomerId'=>$request->iCustomerId,
                    ];
                    $exist_doc = Doctor::where($where_doc)->count();
                }
                elseif($request->iDepartmentId ==5)
                {
                    $where_lab_user = [
                        'iUserId'=>$iLoginAdminId,
                        'iCustomerId'=>$request->iCustomerId,
                    ];
                    $exist_lab_user = UserLab::where($where_lab_user)->count();
                }
                elseif($request->iDepartmentId ==6)
                {
                    $where_office_user = [
                        'iUserId'=>$iLoginAdminId,
                        'iCustomerId'=>$request->iCustomerId,
                    ];
                    $exist_office_user = User::where($where_office_user)->count();
                }
                
               
               
               
                
                if($exist_superAdmin>0)
                {
                    $admindata = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        'email'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $whereadmindata = array(
                        'iAdminId'=>$iLoginAdminId,
                    );
                    Staff::update_superAdmin_data($admindata,$whereadmindata);
                }
                else if($exist_office>0)
                {
                    $officeData = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        'vEmail'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $whereOfficeData = [
                        'iOfficeAdminId'=>$iLoginAdminId,
                    ];
                    OfficeAdmin::where($whereOfficeData)->update($officeData);
                }
                else if($exist_lab > 0)
                {
                    $labData = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        'vEmail'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $wherlabData = [
                        'iLabAdminId'=>$iLoginAdminId,
                    ];
                    $LabAdmin = new LabAdmin ;
                    $LabAdmin->where($wherlabData);
                    $LabAdmin->update_lab_admin($wherlabData,$labData);
                    // LabAdmin::where($wherlabData)->update($labData);
                }
                else if($exist_doc > 0)
                {
                    $docData = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        'vEmail'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $docData = [
                        'iDoctorId'=>$iLoginAdminId,
                    ];
                    Doctor::where($docData)->update($docData);
                }
                else if($exist_lab > 0)
                {
                    $labuserData = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        'vEmail'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $labuserData = [
                        'iLabAdminId'=>$iLoginAdminId,
                    ];
                    UserLab::where($labuserData)->update($labuserData);
                }
                else if($exist_lab > 0)
                {
                    $officeuserData = array(
                        'vFirstName'=>$request->vFirstName,
                        'vLastName'=>$request->vLastName,
                        'vEmail'=>$request->vEmail,
                        'vMobile'=>$request->vMobile,
                        'dtUpdatedDate'=>date('Y-m-d H:i:s')
                    );
                    $officeuserData = [
                        'iLabAdminId'=>$iLoginAdminId,
                    ];
                    User::where($officeuserData)->update($officeuserData);
                }
            }
                
            if(isset($request->UserStaffModal) && $request->UserStaffModal == 'Yes')
            {
                return redirect()->back()->withSuccess('Staff updated successfully.');    
            }
            else
            {
                return redirect()->route('admin.staff')->withSuccess('Staff updated successfully.');
            }
        }
        else{
            $data = Staff::add($data);

            // Mail::to($data['vEmail'])->send(new WelcomeMail($data));
            if(isset($request->UserStaffModal) && $request->UserStaffModal == 'Yes')
            {
                return redirect()->back()->withSuccess('Staff created successfully.'); 
            }
            else
            {
                return redirect()->route('admin.staff')->withSuccess('Staff created successfully.');
            }
            
        }
    }

    public function edit($iStaffId)
    {
        $data['customer'] = Customer::get_all_customer();
        $data['department'] = Staff::get_all_department();
        $data['staffs'] = Staff::get_by_id($iStaffId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['staffs']->iCustomerId) && $data['staffs']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['staffs']->iCustomerId)))
        {
            return redirect()->route('admin.staff')->withError('Invalid Input');
        }
        return view('admin.staff.create')->with($data);
    }
    
    public function generatepassword()
    {
        $number = mt_rand(1000000000, 9999999999);    
        return $number;
        // $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        // // $number = substr(str_shuffle($permitted_chars), 0, 6);
        // return substr(str_shuffle($permitted_chars), 0, 6);
        // $str_result = 'mt_rand(0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz)';  
        
        // $number = substr(str_shuffle($str_result),0,6);
    }
    public function showChangePasswordForm($iStaffId)
    {
        $data['staffs'] = Staff::get_by_id($iStaffId);
        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['staffs']->iCustomerId) && $data['staffs']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['staffs']->iCustomerId)))
        {
            return redirect()->back()->withError('Invalid Input');
        }
        return view('admin.staff.showChangePasswordForm')->with($data);
    }
    
    public function changePassword(Request $request)
    {
        $iStaffId = $request->id;
        $where                  = array();
        $data['vPassword']      = md5($request->vPassword);
        $where['iStaffId']     = $iStaffId;
        
        $Staff_id = new Staff();
        $Staff_id->update($where, $data);
        
        if(isset($request->iLoginAdminId) && !empty($request->iLoginAdminId) && isset($request->iCustomerId) && !empty($request->iCustomerId))
        {
            $iOfficeAdminId = $request->iLoginAdminId;
            $iCustomerId = $request->iCustomerId;
            // Check this user exist in lab admin table 
            $exist_labAdmin = Staff::user_exit_in_labAdmin($iOfficeAdminId,$iCustomerId);
           
            if($exist_labAdmin>0)
            {
                $labData = array(
                    'vPassword'=>md5($request->vPassword),
                    'dtUpdatedDate'=>date('Y-m-d H:i:s')
                );
                $whereLabData = array(
                    'iLabAdminId'=>$iOfficeAdminId,
                    'iCustomerId'=>$iCustomerId
                );
              
                Staff::update_lab_data($labData,$whereLabData);
            }
        }
        $where_staff = [
            'iOfficeAdminId'=>$iStaffId,
        ];
        $exist_office = OfficeAdmin::where($where_staff)->count();
        if($exist_office>0)
        {
            $officeData = array(
                'vPassword'=>md5($request->vPassword),
                'dtUpdatedDate'=>date('Y-m-d H:i:s')
            );
            $whereOfficeData = [
                'iOfficeAdminId'=>$iStaffId,
            ];
            OfficeAdmin::where($whereOfficeData)->update($officeData);
        }
        return redirect()->back()->with("success","Password changed successfully !");

    }

    public function CheckExistEmail(Request $request)
    {
        $vEmail = $request->vEmail;
        $existEmail = Staff::get_by_email($vEmail);
   
        if($existEmail>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function ShowUSerDetail(Request $request)
    {
        $data['customer'] = Customer::get_all_customer();
        $data['department'] = Staff::get_all_department();
        $iStaffId = $request->iStaffId;
        $data['staffs'] = Staff::get_by_id($iStaffId);
        return view('admin.staff.staff_modal_data')->with($data);
    }
}
