<?php

namespace App\Http\Controllers\admin\email;

use App\Http\Controllers\Controller;
use App\Models\admin\email\Email;
use Illuminate\Http\Request;
use Validator;
use App\Libraries\Paginator;

class EmailController extends Controller
{
    public function index()
    {
        $data['emails'] = Email::get();
        return view('admin.email.listing')->with($data);
    }

    public function ajax_listing(Request $request)
    {

        $action = $request->action;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iEmailId";
            $order = "DESC";
        }

        if($action == "search"){
            $vKeyword = $request->keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iEmailId']    = $request->id;

            Email::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Email_ID = (explode(",",$request->id));

            foreach ($Email_ID as $key => $value) {
                $where                 = array();
                $where['iEmailId']    = $value;

                Email::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;

        $email_data = Email::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($email_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;

        $data['data'] = Email::get_all_data($criteria, $start, $limit, $paging);

        $data['paging'] = $paginator->paginate();
        
        return view('admin.email.ajax_listing')->with($data);   
    }

    public function create()
    {
        return view('admin.email.create');
    }
    public function store(Request $request)
    {
        $iEmailId = $request->id;

        $data['vEmailCode']    = $request->vEmailCode;
        $data['vTitle']        = $request->vTitle;
        $data['vFromName']     = $request->vFromName;
        $data['vFromEmail']    = $request->vFromEmail;
        $data['vSubject']      = $request->vSubject;
        $data['tMessage']      = $request->tMessage;
        $data['eStatus']       = $request->eStatus;

        if($iEmailId){
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }

        if(!empty($iEmailId)){
            $where                      = array();
            $where['iEmailId']         = $iEmailId;

            $Email_id = new Email();
            $Email_id->update($where, $data);

            return redirect()->route('admin.email')->withSuccess('Email updated successfully.');
        }
        else{
            $Email_id = Email::add($data);

            return redirect()->route('admin.email')->withSuccess('Email created successfully.');
        }
    }

    public function edit($iEmailId)
    {
        $data['emails'] = Email::get_by_id($iEmailId);

        return view('admin.email.create')->with($data);
    }


    public function changeStatus(Request $request)
    {  
        $id                     = $request->id;
        $email                  = Email::find($id);
        $email->status          = $request->status;
        $email->save();

        return response()->json(['message'=>"Status changed successfully."]);
    }

}
