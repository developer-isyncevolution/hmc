<?php

namespace App\Http\Controllers\admin\subaddoncategory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\Paginator;
use App\Models\admin\subaddoncategory\Subaddoncategory;
use App\Models\admin\addoncategory\Addoncategory;
use App\Models\admin\labOffice\LabOffice;
use App\Models\admin\customer\Customer;
use Session;
use App\Libraries\General;

class SubaddoncategoryController extends Controller
{
    public function index()
    {
        $criteria['eType'] = 'Upper';
        $data['category_upper'] = Subaddoncategory::get_all_upper_category($criteria);
        
        $criteria['eType'] = 'Lower';
        $data['category_lower'] = Subaddoncategory::get_all_upper_category($criteria);
        return view('admin.subaddoncategory.listing')->with($data);
    }
    public function ajax_listing(Request $request)
    {
        $action = $request->action;
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "vSequence";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->upperkeyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iSubAddCategoryId']    = $request->id;

            Subaddoncategory::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Grade_ID = (explode(",",$request->id));

            foreach ($Grade_ID as $key => $value) {
                $where                 = array();
                $where['iSubAddCategoryId']    = $value;

                Subaddoncategory::delete_by_id($where);
            }
        }
        $iAddCategoryId = $request->iAddCategoryId;
        if(isset($iAddCategoryId) && $iAddCategoryId!='')
        {
            $criteria['iAddCategoryId']   = $iAddCategoryId;
        }
        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eType']      = "Upper";

        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end       
        $SubaddonCategory_data = Subaddoncategory::get_all_data($criteria);
        
        
        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($SubaddonCategory_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->upper_limit_page !='')
        {
            $per_page = $request->upper_limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
        }
        
        $paginator->is_ajax = true;
        $paging = true;
        $iAddCategoryId = $request->iAddCategoryId;
        if(isset($iAddCategoryId) && $iAddCategoryId!='')
        {
            $criteria['iAddCategoryId']   = $iAddCategoryId;
        }
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['eType']      = "Upper";
        $data['data'] = Subaddoncategory::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = str_replace('class="ajax_page','class="ajax_page_upper',$paginator->paginate());        
        return view('admin.subaddoncategory.ajax_listing')->with($data);   
    }
    public function lower_ajax_listing(Request $request)
    {
        
        
        $action = $request->action;
        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "vSequence";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->lower_keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iSubAddCategoryId']    = $request->id;

            Subaddoncategory::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $Grade_ID = (explode(",",$request->id));

            foreach ($Grade_ID as $key => $value) {
                $where                 = array();
                $where['iSubAddCategoryId']    = $value;

                Subaddoncategory::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eType']      = "Lower";
        $iAddCategoryId = $request->iAddCategoryId;
        if(isset($iAddCategoryId) && $iAddCategoryId!='')
        {
            $criteria['iAddCategoryId']   = $iAddCategoryId;
        }
        // For Perticular customer show start
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $SubaddonCategory_data = Subaddoncategory::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($SubaddonCategory_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->lower_limit_page !='')
        {
            $per_page = $request->lower_limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }

        $paginator->is_ajax = true;
        $paging = true;
        $iAddCategoryId = $request->iAddCategoryId;
        if(isset($iAddCategoryId) && $iAddCategoryId!='')
        {
            $criteria['iAddCategoryId']   = $iAddCategoryId;
        }
        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['eType']      = "Lower";
        $data['data'] = Subaddoncategory::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = str_replace('class="ajax_page','class="ajax_page_lower',$paginator->paginate());
        
        return view('admin.subaddoncategory.lower_ajax_listing')->with($data);   
    }
    public function create()
    {
         // For Get customer Office satrt
         $iCustomerId = General::admin_info()['iCustomerId'];
         $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);
         $office = array();
         foreach ($labOffice as $key => $value) {
             $office[] = Customer::get_by_id($value->iOfficeId);
         }
         $data['office'] = $office;
         // For Get customer Office end
        $data['category'] = Subaddoncategory::get_all_upper_category();
        return view('admin.subaddoncategory.create')->with($data);
    }

   public function store(Request $request)
    {
        
        $iSubAddCategoryId = $request->iSubAddCategoryId;
        $data['iAddCategoryId']    = $request->iAddCategoryId;
        $data['vAddonName']          = $request->vAddonName;
        $data['vCode']          = $request->vCode;
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        if(isset($request->vJaws))
        {
            $data['vJaws']          = $request->input('vJaws');
            $data['vJaws']          = implode(",", $data['vJaws']);
        }
        else
        {
            $data['vJaws'] = '';
        }
        $data['iPrice']         = $request->iPrice;
        $data['vSequence']      = $request->vSequence;
        $data['eStatus']        = $request->eStatus;
        $data['eType']          = $request->eType;
        if($iSubAddCategoryId){
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }else{
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        if(!empty($iSubAddCategoryId)){
            $where                      = array();
            $where['iSubAddCategoryId']       = $iSubAddCategoryId;

            $iSubAddCategoryUpdate = new Subaddoncategory();
            $iSubAddCategoryUpdate->update($where, $data);
            //Store Office Prices start
            $iCustomerId = General::admin_info()['iCustomerId'];
            $iTypeId = $iSubAddCategoryId;
            $OfficeExist = Subaddoncategory::checkExistOffice($iCustomerId,$iTypeId);
            
            if(isset($OfficeExist) && count($OfficeExist) >0)
            {
                if(isset($request->Office) && !empty($request->Office))
                {
                    Subaddoncategory::delete_by_iTypeId_id($iSubAddCategoryId);
                 
                    foreach($request->Office as $key=> $office_val)   
                    {
                        if(!empty($office_val['vOfficePrice']))
                        {
                            $office_data['vOfficePrice'] = $office_val['vOfficePrice'];
                            $office_data['iOfficeId'] = $office_val['iOfficeId'];
                            $office_data['iLoginAdminId'] = General::admin_info()['iCustomerId'];;
                            $office_data['iTypeId'] = $iSubAddCategoryId;
                            $office_data['vOfficeName'] = $office_val['vOfficeName'];
                            $office_data['eType'] = 'subaddoncategory';
                            $office_data['dtAddedDate'] = date("Y-m-d H:i:s");
                            $office_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                            $LastOfficeId = Subaddoncategory::AddOfficePrice($office_data);
                        }
                    }
                }
            }else
            {
                if(isset($request->Office) && !empty($request->Office))
                {
                    foreach($request->Office as $key=> $office_val)   
                    {
                        $office_data['vOfficePrice'] = $office_val['vOfficePrice'];
                        if(empty($office_val['vOfficePrice']))
                        {
                            $office_data['vOfficePrice'] = $request['vPrice'];
                        }
                        $office_data['iOfficeId'] = $office_val['iOfficeId'];
                        $office_data['iLoginAdminId'] = General::admin_info()['iCustomerId'];;
                        $office_data['iTypeId'] = $iSubAddCategoryId;
                        $office_data['vOfficeName'] = $office_val['vOfficeName'];
                        $office_data['eType'] = 'subaddoncategory';
                        $office_data['dtAddedDate'] = date("Y-m-d H:i:s");
                        $office_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                        $LastOfficeId = Subaddoncategory::AddOfficePrice($office_data);
                    }
                }
            }    
            //Store Office Prices end
            return redirect()->route('admin.subaddoncategory')->withSuccess('Sub Add ons updated successfully.');
        }
        else{
            $iSubAddCategoryId = Subaddoncategory::add($data);
            //Store Office Prices start
            if(isset($request->Office) && !empty($request->Office))
            {
                foreach($request->Office as $key=> $office_val)   
                {
                    $office_data['vOfficePrice'] = $office_val['vOfficePrice'];
                    if(empty($office_val['vOfficePrice']))
                    {
                        $office_data['vOfficePrice'] = $request['vPrice'];
                    }
                    $office_data['iOfficeId'] = $office_val['iOfficeId'];
                    $office_data['iLoginAdminId'] = General::admin_info()['iCustomerId'];;
                    $office_data['iTypeId'] = $iSubAddCategoryId;
                    $office_data['vOfficeName'] = $office_val['vOfficeName'];
                    $office_data['eType'] = 'subaddoncategory';
                    $office_data['dtAddedDate'] = date("Y-m-d H:i:s");
                    $office_data['dtUpdatedDate'] = date("Y-m-d H:i:s");
                    $LastOfficeId = Subaddoncategory::AddOfficePrice($office_data);
                }
            }
            //Store Office Prices end
            return redirect()->route('admin.subaddoncategory')->withSuccess('Sub Add ons created successfully.');
        }
    }

    public function edit($iSubAddCategoryId)
    {
        $data['category'] = Subaddoncategory::get_all_upper_category();
        $data['subaddon'] = Subaddoncategory::get_by_id($iSubAddCategoryId);

        $iCustomerId = General::admin_info()['iCustomerId'];
        if((isset($data['subaddon']->iCustomerId) && $data['subaddon']->iCustomerId != $iCustomerId && !empty($iCustomerId)) || (empty($data['subaddon']->iCustomerId)))
        {
            return redirect()->route('admin.category')->withError('Invalid Input');
        }

        $eType = $data['subaddon']->eType;
        $data['category'] = Addoncategory::where("eType",$eType)
                                    ->orderBy("vSequenc", "ASC")
                                    ->get();

         // For Get customer Office satrt
         $iCustomerId = General::admin_info()['iCustomerId'];
         $iTypeId = $iSubAddCategoryId;
         $OfficeExist = Subaddoncategory::checkExistOffice($iCustomerId,$iTypeId);
       
         if(isset($OfficeExist) && count($OfficeExist) >0)
         {
             $Office_array = array();
             foreach($OfficeExist as $OfficeExist_val)
             {
                $Office_array[] = $OfficeExist_val->iOfficeId;
             }

             $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);
            //  dd($labOffice);
             $office = array();
             foreach ($labOffice as $key => $value) {
                if(in_array($value->iOfficeId,$Office_array))
                {
                    $office[] = Customer::get_by_id($value->iOfficeId);
                    $where_ofc = array(
                                        'iOfficeId'=>$value->iOfficeId,
                                        'iTypeId'=>$iTypeId
                                        );
                    $OfficeExistData = Subaddoncategory::checkExistOfficeById($where_ofc);
                  
                    if(!empty($OfficeExistData))
                    {
                        $office[$key]->vOfficePrice = $OfficeExistData->vOfficePrice;
                    }
                }
                else
                {
                    $office[] = Customer::get_by_id($value->iOfficeId);
                }
             }
             $data['office'] = $office;
         }
         else
         {
             $labOffice = LabOffice::get_active_office_by_labId($iCustomerId);
             $office = array();
             foreach ($labOffice as $key => $value) {
                 $office[] = Customer::get_by_id($value->iOfficeId);
             }
             $data['office'] = $office;
         }
         // For Get customer Office end

        return view('admin.subaddoncategory.create')->with($data);
    }
    public function fetch_category(Request $request)
    {
        $eType = $request->eType;
        $data['data'] = Addoncategory::where("eType",$eType)
                                    ->orderBy("vSequenc", "ASC")
                                    ->get();
        return view('admin.subaddoncategory.ajax_fetch_category_listing')->with($data);
    }
}
