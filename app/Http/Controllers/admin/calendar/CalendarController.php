<?php

namespace App\Http\Controllers\admin\calendar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\calendar\calendar;
use App\Models\admin\office\Office;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;

class CalendarController extends Controller
{
    public function index(Request $request)
    {
        // Session::get('vEmail');
        $data['calendar'] = calendar::get_all_calendar_event();
        $data['office_data'] = calendar::get_all_office_data();
        // dd($data['office_data']);
        $data['customer'] = Office::get_all_customer();
        return view('admin.calendar.listing')->with($data);
    }
    public function store(Request $request)
    {
        // For Perticular customer show start
        $data['iCustomerId'] = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $iEventId = $request->iEventId;
        $data['iCustomerId']    = $request->iCustomerId;
        $data['iOfficeId']      = $request->iOfficeId;
        $data['vColor']         = $request->vColor;
        $data['vTitle']         = $request->vTitle;
        $data['tDescription']   = $request->tDescription;
        $data['dtStartDate']    = $request->dtStartDate;
        $data['tiStartTime']    = $request->tiStartTime;
        if($iEventId){
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }else{
            
            // $data['vCreatedBy'] = Session::get('per_page_lan');
            // $data['vUpdatedBy'] = Session::get('per_page_lan');
            $data['dtAddedDate'] = date("Y-m-d H:i:s");
            $data['dtUpdatedDate'] = date("Y-m-d H:i:s");
        }
        if(!empty($iEventId)){
            $where                      = array();
            $where['iEventId']       = $iEventId;
            $iEventId = new calendar();
            $iEventId->update($where, $data);
            return redirect()->route('admin.calendar')->withSuccess('Event updated successfully.');
        }
        else{
            $iEventId = calendar::add($data);
            return redirect()->route('admin.calendar')->withSuccess('Event created successfully.');
        }
        
    }
    public function edit($iEventId)
    {
        $data['office_data'] = calendar::get_all_office_data();
        $data['customer'] = Office::get_all_customer();
        $data['calendar'] = calendar::get_all_calendar_event();
        $data['edit_event'] = calendar::get_by_id($iEventId);
        return view('admin.calendar.listing')->with($data);
    }
    public function getofficedata(Request $request){
       
        $data['office']   = calendar::get_by_id_office($request->iCustomerId);   
        return response()->json($data);   
      
     }
}
