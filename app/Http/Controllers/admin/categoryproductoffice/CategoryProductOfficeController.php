<?php

namespace App\Http\Controllers\admin\categoryproductoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\categoryproductoffice\CategoryProductOffice;
use App\Models\admin\category\Category;
use App\Models\admin\productgrade\ProductGrade;
use App\Models\admin\labOffice\LabOffice;
use App\Models\admin\customer\Customer;
use Validator;
use App\Helper\RandomHelper;
use App\Libraries\Paginator;
use Session;
use App\Libraries\General;

class CategoryProductOfficeController extends Controller
{
    public function index()
    {
        Session::forget('per_page_lan');
        $iCustomerId = General::admin_info()['iCustomerId'];
        $labOffice = LabOffice::get_active_lab_by_officeId($iCustomerId);
        $labs = array();
        foreach ($labOffice as $key => $value) {
            $labs[] = Customer::get_by_id($value->iLabId);
        }
        // dd($labOffice); 
        $data['labs'] = $labs;
        return view('admin.categoryproductoffice.listing')->with($data);
    }
    public function categoryproductoffice_list($iCustomerId)
    {
        Session::forget('per_page_lan');
        $criteria['eType'] = 'Upper';
        $criteria['iCustomerId'] = $iCustomerId;
        $data['uppercategory'] = CategoryProductOffice::get_all_product($criteria);

        $criteria['eType'] = 'Lower';
        $criteria['iCustomerId'] = $iCustomerId;
        $data['iCustomerId'] = $iCustomerId;
        $data['lowercategory'] = CategoryProductOffice::get_all_product($criteria);
  
        return view('admin.categoryproductoffice.listing_office')->with($data);
    }    

    public function categoryproductselect($iCategoryId)
    {
        $data['categorys'] = CategoryProductOffice::get_by_once_categoryid($iCategoryId);
        if(isset($data['categorys']))
        {
            $eType = $data['categorys']->eType;
            if($eType=='Upper')
            {
                $criteria['iCategoryId'] = $data['categorys']->iCategoryId;
            }
            else if($eType=='Lower')
            {
            $criteria['iCategoryId'] = $data['categorys']->iCategoryId;
            }
            else{
                $criteria['iCategoryId'] = $data['categorys']->iCategoryId;
            }
        }
        $criteria['eType'] = 'Upper';
        $criteria['iCustomerId'] = General::admin_info()['iCustomerId'];
        $data['uppercategory'] = CategoryProductOffice::get_all_category($criteria);
        
        $criteria['eType'] = 'Lower';
        $data['lowercategory'] = CategoryProductOffice::get_all_category($criteria);
        return view('admin.categoryproductoffice.listing_office')->with($data);
    }

    public function ajax_listing(Request $request)
    {
        $iCustomerId = General::admin_info()['iCustomerId'];
        $labOffice = LabOffice::get_active_lab_by_officeId($iCustomerId);
        $labs = array();
        foreach ($labOffice as $key => $value) {
            $labs[] = Customer::get_by_id($value->iLabId);
        }
        $data['data'] = $labs;
        // dd($data);
        return view('admin.categoryproductoffice.ajax_listing')->with($data);   
    }
    public function ajax_listing_upper(Request $request)
    {
        $action = $request->action;
        $iCategoryId = $request->iCategoryId;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->upperkeyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iCategoryProductId']    = $request->id;

            CategoryProductOffice::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $CategoryProduct_ID = (explode(",",$request->id));

            foreach ($CategoryProduct_ID as $key => $value) {
                $where                 = array();
                $where['iCategoryProductId']    = $value;

                CategoryProductOffice::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eType'] = 'Upper';
        // For Perticular customer show start
        $criteria['iCustomerId'] = $request->iCustomerId;
        // For Perticular customer show end
        $CategoryProduct_data = CategoryProductOffice::get_all_data($criteria);

        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($CategoryProduct_data);

       

        if($request->upper_limit_page !='')
        {
            $per_page = $request->upper_limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }
        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        // dd($paginator->itemsPerPage);
        if($request->limit_page !='')
        {
            $limit = $request->limit_page;
        }
        else
        {
            $limit = $paginator->itemsPerPage;
        }

       
        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['eType'] = 'Upper';
        $criteria['iCategoryId'] = $iCategoryId;
        $data['data'] = 'Upper';
        $data['data'] = CategoryProductOffice::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        
        $data['paging'] = str_replace('class="ajax_page','class="ajax_page_upper',$paginator->paginate());
        
        return view('admin.categoryproductoffice.ajax_listing_upper')->with($data);   
    }
    public function lower_ajax_listing(Request $request)
    {
        $action = $request->action;
        $CategoryProduct = $request->CategoryProduct;

        if($action == "sort"){
            $column = $request->column;
            $order = $request->order;
        } else{
            $column = "iSequence";
            $order = "ASC";
        }

        if($action == "search"){
            $vKeyword = $request->lower_keyword;
        } else {
            $vKeyword = "";
        }

        if($action == "delete"){
            $where                 = array();
            $where['iCategoryProductId']    = $request->id;

            CategoryProductOffice::delete_by_id($where);
        }

        if($action == "multiple_delete"){
            $CategoryProduct_ID = (explode(",",$request->id));

            foreach ($CategoryProduct_ID as $key => $value) {
                $where                 = array();
                $where['iCategoryProductId']    = $value;

                CategoryProductOffice::delete_by_id($where);
            }
        }

        $criteria = array();
        $criteria['vKeyword']   = $vKeyword;
        $criteria['column']     = $column;
        $criteria['order']      = $order;
        $criteria['eType'] = 'Lower';
         // For Perticular customer show start
         $criteria['iCustomerId'] = $request->iCustomerId;
         // For Perticular customer show end
        $CategoryProduct_data = CategoryProductOffice::get_all_data($criteria);


        $pages = 1;

        if($request->pages != "")
        {
            $pages = $request->pages;
        }

        $paginator = new Paginator($pages);
        $paginator->total = count($CategoryProduct_data);

        $start = ($paginator->currentPage - 1) * $paginator->itemsPerPage;
        $limit = $paginator->itemsPerPage;

        if($request->lower_limit_page !='')
        {
            $per_page = $request->lower_limit_page;
            Session::put('per_page_lan', $per_page);
            $paginator->itemsPerPage = $per_page;
            //$paginator->range = $per_page;
            $limit =  $per_page;
        }
        else
        {
            $per_page = Session::get('per_page_lan');
            if($per_page !='')
            {
                $paginator->itemsPerPage = $per_page;
                //$paginator->range = $per_page;
                $limit =  $per_page;
            }
            else
            {
                $paginator->itemsPerPage = 50;
                 //$paginator->range = 50;
            }
            
        }
        $paginator->is_ajax = true;
        $paging = true;

        $criteria['start']  = $start;
        $criteria['limit']  = $limit;
        $criteria['paging'] = $paging;
        $criteria['eType'] = 'Lower';
        $criteria['iCategoryId'] = $CategoryProduct;
       
        $data['data'] = CategoryProductOffice::get_all_data($criteria, $start, $limit, $paging);
        $data['start']  = $start;
        $data['limit']  = $limit;
        $data['paging'] = str_replace('class="ajax_page','class="ajax_page_lower',$paginator->paginate());
        
        return view('admin.categoryproductoffice.lower_ajax_listing')->with($data);   
    }
    
  
    public function fetch_category(Request $request)
    {
        $eType = $request->eType;
        $data['data'] = Category::where("eType",$eType)
                                    ->orderBy("iSequence", "ASC")
                                    ->get();
        return view('admin.categoryproduct.ajax_fetch_category_listing')->with($data);
    }
    
}
