<?php

namespace App\Models\admin\user_lab;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class UserLab extends Model
{
    use HasFactory;

    public $table           = 'user';

    protected $primaryKey   = 'iUserId';

    public $timestamps      = false;

    protected $fillable     = ['iUserId', 'iDoctorId','vFirstName', 'vLastName', 'vUserName', 'vTitle', 'vPassword', 'tDescription', 'vEmail', 'vLicence', 'vMiddleInitial', 'vPassword','vImage', 'vMobile', 'vCellulor', 'eStatus', 'dtAddedDate', 'dtUpdatedDate'];
    
    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        // $SQL = DB::table("user")->select("user.*", DB::raw("(SELECT doctor.vUserName FROM doctor WHERE doctor.iDoctorId = user.iDoctorId) as vUserName"))->orderBy("iDoctorId", "DESC"); 
        $SQL = DB::table("user");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where(DB::raw('CONCAT_WS(" ", vFirstName, vLastName)'), 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->orWhere('vUserName', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        if(!empty($criteria["iDoctorId"]))
        {
            $SQL->where("iDoctorId", $criteria["iDoctorId"]);
        }
        if(!empty($criteria["iCustomerId"]))
        {
            $SQL->where("iCustomerId", $criteria["iCustomerId"]);
        }
        if(!empty($criteria["iOfficeId"]))
        {
            $SQL->where("iOfficeId", $criteria["iOfficeId"]);
        }
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   
        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }
    public static function get_all_doctor(){
        
        $SQL = DB::table("doctor");
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_customer(){
        
        $SQL = DB::table("customer");
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_office(){
        
        $SQL = DB::table("office");
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_active_user(){
        
        $SQL = DB::table("user");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }    

    public static function get_by_id($iUserId)
    {
        $SQL = DB::table("user");
        $SQL->where("iUserId", $iUserId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('user')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('user');
        $id->where('iUserId',$where['iUserId'])->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('user')->where('iUserId',$where['iUserId'])->delete();
    }
    
    public static function user_exit_in_staff($iLoginAdminId,$iCustomerId)
    {
        return DB::table('staff')->where('iLoginAdminId',$iLoginAdminId)
                              ->where('iCustomerId',$iCustomerId)
                              ->get()->count();  
    }

    public static function update_staff_data($data,$where_array)
    {
        DB::table('staff')->where($where_array)->update($data);  
    }
}
