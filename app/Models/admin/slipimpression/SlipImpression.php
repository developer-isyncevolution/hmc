<?php

namespace App\Models\admin\slipimpression;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class SlipImpression extends Model
{
    use HasFactory;

    protected $table        = 'slip_impression';

    protected $primaryKey   = 'iSlipImpressionId';

    public $timestamps      = false;

    protected $fillable     = ['iSlipId', 'iImpressionId','iQuantity','eType'];



    public static function add($data)
    {
        $add = DB::table('slip_impression')->insertGetId($data);
        return $add;
    
    }
    public static function get_by_id($iCategoryProductId)
    {
        $SQL = DB::table("slip_impression");
        $SQL->where("iCategoryProductId", $iCategoryProductId);
        $result = $SQL->get();
        return $result; 
    }
    public static function get_by_iSlipId($criteria = array())
    {
        $SQL = DB::table("slip_impression")->select("slip_impression.*", DB::raw("(SELECT impression.vName FROM impression WHERE impression.iImpressionId = slip_impression.iImpressionId) as vImpressionName"))->orderBy("iSlipImpressionId", "DESC");
        
        $SQL->where("iSlipId", $criteria['iSlipId']);
        $SQL->where("eType", $criteria['eType']);
        $SQL->orderBy("iSlipImpressionId", "DESC");
        $result = $SQL->get();
        return $result;
    }
    public static function delete_by_id(array $where = [])
    {
        $SQL = DB::table('slip_impression');
        $SQL->where('iSlipId',$where['iSlipId']);
        $SQL->where('eType',$where['eType']);
        $SQL->delete();
    }
    
}
