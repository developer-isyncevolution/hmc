<?php

namespace App\Models\admin\productgrade;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ProductGrade extends Model
{
    use HasFactory;

    protected $table        = 'product_grade';

    protected $primaryKey   = 'iProductGradeId';

    public $timestamps      = false;

    protected $fillable     = ['iGradeId', 'iCategoryProductId'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {

        $SQL = DB::table('productgrade');
        $SQL->join('category', 'category.iCategoryId', '=', 'productgrade.iCategoryId');

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('productgrade.vCode', 'like', '%' . $criteria['vKeyword'] . '%');
        }
        
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy('productgrade.iSequence',$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get(['productgrade.*', 'category.vName as vCategoryName']);
        return $result;
        // $SQL = DB::table("productgrade")->select("productgrade.*", DB::raw("(SELECT category.vName FROM category WHERE category.iCategoryId = productgrade.iCategoryId) as vCategoryName"))->orderBy("iCategoryProductId", "DESC");

        // if($criteria['vKeyword'] != "")
        // {
        //     $SQL->where('vName', 'like', '%' . $criteria['vKeyword'] . '%');
        // }

        // if(!empty($criteria["eStatus"]))
        // {
        //     $SQL->where("eStatus", $criteria["eStatus"]);
        // }
        // if(!empty($criteria["eType"]))
        // {
        //     $SQL->where("eType", $criteria["eType"]);
        // }

        // if($criteria['column'] || $criteria['order'] != "")
        // {
        //     $SQL->orderBy('iSequence',$criteria['order']);
        // }   

        // if($paging == true)
        // {
        //     $SQL->limit($limit);
        //     $SQL->skip($start);
        // }

        // $result = $SQL->get();
        // return $result;
    }
    public static function get_all_category($criteria = array())
    {
        $SQL = DB::table("category");
        if(!empty($criteria["eType"]))
        {
            $SQL->where("eType", $criteria["eType"]);
        }
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_grade()
    {
        $SQL = DB::table("grade");
        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iCategoryProductId)
    {
        $SQL = DB::table("product_grade");
        $SQL->where("iCategoryProductId", $iCategoryProductId);
        $result = $SQL->get();
        return $result; 
    }
    public static function get_by_product_id($iCategoryProductId)
    {
        $SQL = DB::table("product_grade");
        $SQL->join('grade','grade.iGradeId','product_grade.iGradeId');
        $SQL->where("product_grade.iCategoryProductId", $iCategoryProductId);
        $result = $SQL->get(['product_grade.*','grade.vQualityName']);
        return $result; 
    }
    
    public static function get_grade_by_product($iCategoryProductId)
    {
        $SQL = DB::table("product_grade");
        $SQL->where("iCategoryProductId", $iCategoryProductId);
        $result = $SQL->get();
        return $result; 
    }
    public static function FetchProductGradePrice($iCategoryProductId,$iGradeId)
    {
        $SQL = DB::table("product_grade");
        $SQL->where('iCategoryProductId',$iCategoryProductId);
        $SQL->where('iGradeId',$iGradeId);
        $result = $SQL->get()->first();
        return $result;
    }
    public static function add($data)
    {
        $add = DB::table('product_grade')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = [])
    {
        $iCategoryProductId = DB::table('product_grade');
        $iCategoryProductId->where('iCategoryProductId',$where['iCategoryProductId'])->update($data);
        return $iCategoryProductId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('product_grade')->where('iCategoryProductId',$where['iCategoryProductId'])->delete();
    }
}
