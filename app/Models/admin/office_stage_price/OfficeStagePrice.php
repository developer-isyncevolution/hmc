<?php

namespace App\Models\admin\office_stage_price;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class OfficeStagePrice extends Model
{
    use HasFactory;

    protected $table        = 'office_stage_price';

    protected $primaryKey   = 'iOfficeStagePriceId';

    public $timestamps      = false;

    protected $fillable     = ['iOfficeStagePriceId', 'vOfficeName'];

    public static function get_by_product_grade_office($criteria)
    {
      
        $SQL = DB::table('office_stage_price');
        $SQL->where($criteria);
        $result = $SQL->get();
        return $result;
    }

    // Office Data update start
    public static function AddOfficePrice($data)
    {
        $add = DB::table('office_stage_price')->insertGetId($data);
        return $add;
    }

    public static function UpdatedOfficePrice($office_data,$office_where)
    {
        $sql = DB::table('office_stage_price');
        $sql->where($office_where);
        $update = $sql->update($office_data);
        return $update;
    }

    public static function checkExistOffice($iCategoryProductId)
    {
        $SQL = DB::table('office_stage_price');
        $SQL->where('iStageId',$iCategoryProductId);
        // $SQL->groupBy('iOfficeId');
        $result = $SQL->get();
        return $result;
    }
    public static function FetchEditOffice($office_data,$iCategoryProductId)
    {
        $SQL = DB::table('office_stage_price');
        $SQL->where('iProductId',$iCategoryProductId);
        // $SQL->groupBy('iOfficeId');
        $result = $SQL->get();
        return $result;
    }
    public static function deleteExistOffice($iCategoryProductId)
    {
        DB::table('office_stage_price')->where('iStageId', $iCategoryProductId)->delete();
    }
    // Office Data update end
    
    public static function FetchStageGradeOfficePrice($iProductId,$iGradeId,$iStageId)
    {
        $SQL = DB::table('office_stage_price');
        $SQL->where('iProductId',$iProductId);
        $SQL->where('iGradeId',$iGradeId);
        $SQL->where('iStageId',$iStageId);
        $result = $SQL->get()->first();
        return $result;
    }
    public static function FetchStageOfficePrice($iProductId,$iStageId)
    {
        $SQL = DB::table('office_stage_price');
        $SQL->where('iProductId',$iProductId);
        $SQL->where('iGradeId',0);
        $result = $SQL->get()->first();
        return $result;
    }
}
