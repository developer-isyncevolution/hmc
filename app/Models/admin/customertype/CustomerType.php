<?php

namespace App\Models\admin\customertype;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class CustomerType extends Model
{
    use HasFactory;

    protected $table        = 'customertype';

    protected $primaryKey   = 'iCustomerTypeId';

    public $timestamps      = false;

    protected $fillable     = ['iCustomerTypeId','vTypeName', 'eStatus','dtAddedDate','dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        $SQL = DB::table("customertype");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vTypeName', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }

        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }
    public static function get_all_type()
    {
        $SQL = DB::table("customertype");

        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iCustomerTypeId)
    {
        $SQL = DB::table("customertype");
        $SQL->where("iCustomerTypeId", $iCustomerTypeId);
        $result = $SQL->get();
        return $result->first(); 
        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
    }
    public static function add($data)
    {
        $add = DB::table('customertype')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iCustomerTypeId = DB::table('customertype');
        $iCustomerTypeId->where('iCustomerTypeId',$where['iCustomerTypeId'])->update($data);
        return $iCustomerTypeId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('customertype')->where('iCustomerTypeId',$where['iCustomerTypeId'])->delete();
    }
}
