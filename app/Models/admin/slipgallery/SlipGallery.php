<?php

namespace App\Models\admin\slipgallery;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class SlipGallery extends Model
{
    use HasFactory;

    protected $table        = 'slip_attachment';

    protected $primaryKey   = 'iSlipAttachmentId';

    public $timestamps      = false;

    protected $fillable     = ['iSlipId', 'iSlipAttachmentId', 'iTampId','vName','eType'];



    public static function add($data)
    {
        $add = DB::table('slip_attachment')->insertGetId($data);
        return $add;
    
    }
    public static function get_all_data($criteria = array())
    {
        $SQL = DB::table("slip_attachment");
        $SQL->where("iTampId", $criteria['iTampId']);
        $result = $SQL->get();
        return $result;
    }
    public static function get_by_iTampId($iTampId)
    {
        $SQL = DB::table("slip_attachment");
        $SQL->where("iTampId", $iTampId);
        $result = $SQL->get();
        return $result;
    }
    public static function get_by_iCaseId($iCaseId)
    {
        $SQL = DB::table("slip_attachment");
        $SQL->join('case', 'slip_attachment.iCaseId', '=', 'case.iCaseId');
        $SQL->join('customer', 'case.iOfficeId', '=', 'customer.iCustomerId');
        $SQL->join('slip', 'slip.iSlipId', '=', 'slip_attachment.iSlipId');
        $SQL->join('slip_product', 'slip.iSlipId', '=', 'slip_product.iSlipId');
        $SQL->join('productstage', 'slip_product.iStageId', '=', 'productstage.iProductStageId');
        $SQL->where("slip_attachment.iCaseId", $iCaseId);
        $SQL->groupBy('slip_attachment.iSlipId');
        $result = $SQL->get(['slip_attachment.*','case.vPatientName','case.vCaseNumber','case.vCasePanNumber','customer.vOfficeName','slip.vSlipNumber','productstage.vName AS vStageName']);
    
        foreach ($result as $key => $value) 
        {
            $SQLB = DB::table("slip_attachment");
            $SQLB->where('iCaseId',$value->iCaseId);
            $SQLB->where('iSlipId',$value->iSlipId);
            $result[$key]->slip_attachment = $SQLB->get();
        } 
        return $result;
    }
    public static function get_by_id($iSlipAttachmentId)
    {
        $SQL = DB::table("slip_attachment");
        $SQL->where("iSlipAttachmentId", $iSlipAttachmentId);
        $result = $SQL->get();
        return $result;
    }
    public static function get_by_iSlipId($iSlipId)
    {
        $SQL = DB::table("slip_attachment");
        $SQL->where("iSlipId", $iSlipId);
        $result = $SQL->get();
        return $result;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('slip_attachment');
        $id->where('iSlipAttachmentId',$where['iSlipAttachmentId'])->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('slip_attachment')->where('iSlipAttachmentId',$where['iSlipAttachmentId'])->delete();
    }
    
}
