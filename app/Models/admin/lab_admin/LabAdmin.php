<?php

namespace App\Models\admin\lab_admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class LabAdmin extends Model
{
    use HasFactory;

    public $table           = 'lab_admin';

    protected $primaryKey   = 'iLabAdminId';


    protected $fillable     = ['iLabAdminId', 'iCustomerId','vName', 'vEmail', 'vPassword', 'vvMobile', 'vCellulor', 'eStatus', 'dtAddedDate', 'dtUpdatedDate'];
    

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
     
        $SQL = DB::table("lab_admin");
        $SQL->join('customer','lab_admin.iCustomerId','customer.iCustomerId');
        $SQL->join('staff','lab_admin.iLabAdminId','staff.iLoginAdminId');
        if(isset($criteria['vKeyword']) && $criteria['vKeyword'] != "")
        {
            $SQL->where(DB::raw('CONCAT_WS(" ", lab_admin.vFirstName, lab_admin.vLastName)'), 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->orWhere('lab_admin.vEmail', 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->groupBy('lab_admin.iLabAdminId');
        }
        if(isset($criteria['iCustomerId']) && $criteria['iCustomerId'] != "")
        {
            $SQL->where('lab_admin.iCustomerId',$criteria['iCustomerId']);
        }

        if(isset($criteria["eStatus"]) && !empty($criteria["eStatus"]))
        {
            $SQL->where("lab_admin.eStatus", $criteria["eStatus"]);
        }
        $SQL->where("staff.iDepartmentId", 2);

        if(isset($criteria["column"]) && ($criteria['column'] || $criteria['order'] != ""))
        {
            $SQL->orderBy('lab_admin.iLabAdminId',$criteria['order']);
        }    

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get(['lab_admin.*','customer.vOfficeName','staff.iStaffId']);
        return $result;
      
    }

  
    public static function get_by_id($iLabAdminId)
    {
        $SQL = DB::table("lab_admin");
        $SQL->where("iLabAdminId", $iLabAdminId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_by_iCustomerId($iCustomerId)
    {
        $SQL = DB::table("lab_admin");
        $SQL->where("iCustomerId", $iCustomerId);
        $result = $SQL->get('vImage');
        return $result->first();
    }
  

    public static function add($data)
    {
        $add = DB::table('lab_admin')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('lab_admin');
        $id->where('iLabAdminId',$where['iLabAdminId'])->update($data);
        return $id;
    }
    public static function update_lab_admin($where=array() ,$data=array() ){
        $id = DB::table('lab_admin');
        $id->where($where)->update($data);
        return $id;
    }
    public static function get_lab_admin($where=array() ){
        
        $id = DB::table('lab_admin')->where($where)->get();

        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('lab_admin')->where('iLabAdminId',$where['iLabAdminId'])->delete();
    }

    public static function user_exit_in_staff($iLabAdminId,$iCustomerId)
    {
        return DB::table('staff')->where('iLoginAdminId',$iLabAdminId)
                              ->where('iCustomerId',$iCustomerId)
                              ->get()->count();  
    }

    public static function update_staff_data($data,$where_array)
    {
        DB::table('staff')->where($where_array)->update($data);  
    }
}
