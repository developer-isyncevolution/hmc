<?php

namespace App\Models\admin\staff;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class Staff extends Model
{
    use HasFactory;

    public $table           = 'staff';

    protected $primaryKey   = 'iStaffId';

    public $timestamps      = false;

    protected $fillable     = ['iStaffId', 'iDepartmentId','vFirstName', 'vLastName', 'vUserName', 'vPassword', 'tDescription', 'vEmail', 'vLicence', 'vMiddleInitial','vImage', 'vMobile', 'eStatus', 'dtAddedDate', 'dtUpdatedDate','iCustomerId'];
    

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        // $SQL = DB::table("staff")->select("staff.*", DB::raw("(SELECT department.vName FROM department WHERE department.iDepartmentId = staff.iDepartmentId) as vName"))->orderBy("iDepartmentId", "DESC"); 
        $SQL = DB::table("staff");
        $SQL->join('department','staff.iDepartmentId','department.iDepartmentId');
        $SQL->leftJoin('customer','staff.iCustomerId','customer.iCustomerId');
        // $SQL = DB::table("staff");
        if($criteria['vKeyword'] != "")
        {
            $SQL->where('staff.vFirstName', 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->orWhere('staff.vLastName', 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->orWhere('staff.vUserName', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("staff.eStatus", $criteria["eStatus"]);
        }
        if(!empty($criteria["iDepartmentId"]))
        {
            $SQL->where("staff.iDepartmentId", $criteria["iDepartmentId"]);
        }
        if(!empty($criteria["iCustomerId"]))
        {
            $SQL->where("staff.iCustomerId", $criteria["iCustomerId"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('staff.iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy('staff.'.$criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get(['staff.*','department.vName','customer.vOfficeName','customer.iCustomerTypeId']);
        return $result;
    }
    public static function get_all_department($criteria = array()){
        
        $SQL = DB::table("department");
        if(!empty($criteria["iCustomerId"]))
        {
            $SQL->where("iCustomerId", $criteria["iCustomerId"]);
        }
        $result = $SQL->get();
        return $result;
    }
    public static function get_by_id($iStaffId)
    {
        $SQL = DB::table("staff");
        $SQL->where("iStaffId", $iStaffId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_by_email($vEmail)
    {
        $SQL = DB::table("staff");
        $SQL->where("vEmail", $vEmail);
        $result = $SQL->get()->count();
        return $result;
    }
    public static function get_data_by_email($vEmail)
    {
        $SQL = DB::table("staff");
        $SQL->where("vEmail", $vEmail);
        $result = $SQL->get()->first();
        return $result;
    }

    public static function add($data)
    {
        $add = DB::table('staff')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('staff');
        $id->where('iStaffId',$where['iStaffId'])->update($data);
        return $id;
    }
    public static function update_staff($where= array() ,$data = array()){
        // dd($where,$data);
        $id = DB::table('staff');
        $id->where($where)->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('staff')->where('iStaffId',$where['iStaffId'])->delete();
    }

    public static function login($vEmail, $vPassword)
    {
        $SQL = DB::table("staff")->where('vEmail',$vEmail)->where('vPassword',$vPassword);
        $result = $SQL->first();
        return $result;
    }
    public static function login_master_user($vEmail)
    {
        $SQL = DB::table("staff")->where('vEmail',$vEmail);
        $result = $SQL->first();
        return $result;
    }

    public static function user_exit_in_labAdmin($iLoginAdminId,$iCustomerId)
    {
        return DB::table('lab_admin')->where('iLabAdminId',$iLoginAdminId)
                              ->where('iCustomerId',$iCustomerId)
                              ->get()->count();  
    }
    public static function user_exit_in_officeAdmin($iLoginAdminId,$iCustomerId)
    {
        return DB::table('office_admin')->where('iOfficeAdminId',$iLoginAdminId)
                              ->where('iCustomerId',$iCustomerId)
                              ->get()->count();  
    }
    public static function user_exit_in_doctor($iLoginAdminId,$iCustomerId)
    {
        return DB::table('doctor')->where('iDoctorId',$iLoginAdminId)
                              ->where('iCustomerId',$iCustomerId)
                              ->get()->count();  
    }
    public static function user_exit_in_users($iLoginAdminId,$iCustomerId)
    {
        return DB::table('user')->where('iUserId',$iLoginAdminId)
                              ->where('iCustomerId',$iCustomerId)
                              ->get()->count();  
    }

    public static function update_lab_data($data,$where_array)
    {
        DB::table('lab_admin')->where($where_array)->update($data);  
    }
    public static function update_office_data($data,$where_array)
    {
        DB::table('office_admin')->where($where_array)->update($data);  
    }
    public static function update_doctor_data($data,$where_array)
    {
        DB::table('doctor')->where($where_array)->update($data);  
    }
    public static function update_staff_data($data,$where_array)
    {
        DB::table('staff')->where($where_array)->update($data);  
    }

    public static function update_user_data($data,$where_array)
    {
        DB::table('user')->where($where_array)->update($data);  
    }

    public static function user_exit_in_superAdmin($iAdminId)
    {
        return DB::table('admin')->where('iAdminId',$iAdminId)
                              ->get()->count();  
    }

    public static function update_superAdmin_data($data,$where_array)
    {
        DB::table('admin')->where($where_array)->update($data);  
    }

    public static function authentication($criteria = array())
    {
        $SQL = DB::table("staff");
        if($criteria['vAuthCode'])
        {
            $SQL->where("vUniqueCode", $criteria["vAuthCode"]);
        }
        $result = $SQL->get();
        return $result->first();
    }
}
