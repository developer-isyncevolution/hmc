<?php

namespace App\Models\admin\reset_password;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ResetPassword extends Model
{
    use HasFactory;

    protected $table        = 'password_resets';


    public $timestamps      = false;

    public static function add($data)
    {
        $add = DB::table('password_resets')->insertGetId($data);
        return $add;
    
    }
    public static function exist_token($vUniqueCode)
    {
        $SQL = DB::table('password_resets');
        $SQL->where('vUniqueCode',$vUniqueCode);
        return $SQL->get()->first();
    }
    public static function get_data_by_token($vUniqueCode)
    {
        $SQL = DB::table('password_resets');
        $SQL->where('vUniqueCode',$vUniqueCode);
        return $SQL->get()->first();
    }
    public static function exist_email($vEmail)
    {
        $SQL = DB::table('password_resets');
        $SQL->where('vEmail',$vEmail);
        return $SQL->get()->count();
    }

    public static function delete_by_code($vUniqueCode)
    {
        $SQL = DB::table('password_resets');
        $SQL->where('vUniqueCode',$vUniqueCode);
        $SQL->delete();
    }
    public static function delete_by_email($vEmail)
    {
        $SQL = DB::table('password_resets');
        $SQL->where('vEmail',$vEmail);
        $SQL->delete();
    }
    
}
