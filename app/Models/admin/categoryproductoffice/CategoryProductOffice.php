<?php

namespace App\Models\admin\categoryproductoffice;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class CategoryProductOffice extends Model
{
    use HasFactory;

    protected $table        = 'categoryproduct';

    protected $primaryKey   = 'iCategoryProductId';

    public $timestamps      = false;

    protected $fillable     = ['iCategoryProductId', 'vImage', 'vName', 'vCode', 'iCategoryId', 'eType', 'iSequence', 'eStatus','dtAddedDate','dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        $SQL = DB::table('categoryproduct');
        $SQL->join('category', 'category.iCategoryId', '=', 'categoryproduct.iCategoryId');
        if($criteria['vKeyword'] != "")
        {
            $SQL->where('categoryproduct.vName', 'like', '%' . $criteria['vKeyword'] . '%');
        }
        
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }else{
            $SQL->orderBy('categoryproduct.iSequence','DISC');
        }   
        if(!empty($criteria["iCategoryId"]))
        {
            $SQL->where('categoryproduct.iCategoryId', $criteria["iCategoryId"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('categoryproduct.iCustomerId', $criteria["iCustomerId"]);
        }
        $SQL->select('categoryproduct.*', 'category.vName as vCategoryName',DB::raw("(SELECT office_prices.vOfficePrice FROM office_prices WHERE office_prices.iTypeId=categoryproduct.iCategoryProductId and office_prices.eType ='categoryproduct' limit 1) AS vOfficePrice"));
        // For Perticular customer show end
        if(!empty($criteria["eType"]))
        {
            $SQL->where(function ($query) use ($criteria) {
                $query->where("categoryproduct.eType", $criteria["eType"]);
                $query->orWhere('categoryproduct.eType', 'Both');
            });
        }
        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }
        $result = $SQL->get(['categoryproduct.*', 'category.vName as vCategoryName']);
        return $result;
       
    }
    public static function get_all_category($criteria = array())
    {
        $SQL = DB::table("category");
        if(!empty($criteria["eType"]))
        {
            $SQL->where("eType", $criteria["eType"]);
            $SQL->orWhere('eType', 'Both');
        }
        if(!empty($criteria["iCategoryId"]))
        {
            $SQL->where("iCategoryId", $criteria["iCategoryId"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();

        return $result;
    }
    public static function get_all_product($criteria = array())
    {
        $SQL = DB::table("categoryproduct");
        if(!empty($criteria["eType"]))
        {
            $SQL->where("eType", $criteria["eType"]);
            $SQL->orWhere('eType', 'Both');
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('iCustomerId', $criteria["iCustomerId"]);
        }
         // For Perticular customer show end
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_stage($criteria = array())
    {
        $SQL = DB::table("productstage");
        if(!empty($criteria["eType"]))
        {
            $SQL->where("eType", $criteria["eType"]);
        }
         // For Perticular customer show start
         if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
         {
             $SQL->where('iCustomerId', $criteria["iCustomerId"]);
         }
         // For Perticular customer show end
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_grade()
    {
        $SQL = DB::table("grade")->where('eStatus','Active');
        $result = $SQL->get();
        return $result;
    }
    
    public static function get_product_by_category($iCategoryId,$eType)
    {
        $SQL = DB::table("categoryproduct");
        $SQL->where("iCategoryId", $iCategoryId);
        $SQL->where(function ($query) use ($eType) {
            $query->where("eType", $eType);
            $query->orWhere('eType', 'Both');
        });
        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iCategoryProductId)
    {
        $SQL = DB::table("categoryproduct");
        $SQL->where("iCategoryProductId", $iCategoryProductId);
        $result = $SQL->get();
        return $result->first(); 
        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
    }
    public static function get_by_categoryid($iCategoryId)
    {
        $SQL = DB::table("categoryproduct");
        $SQL->where("iCategoryId", $iCategoryId);
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }
    public static function get_by_once_categoryid($iCategoryId)
    {
        $SQL = DB::table("categoryproduct");
        $SQL->where("iCategoryId", $iCategoryId);
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_by_categoryid_edit($iCategoryProductId)
    {
        $SQL = DB::table("categoryproduct");
        $SQL->where("iCategoryProductId", $iCategoryProductId);
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }
    public static function add($data)
    {
        $add = DB::table('categoryproduct')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = [])
    {
        $iCategoryProductId = DB::table('categoryproduct');
        $iCategoryProductId->where('iCategoryProductId',$where['iCategoryProductId'])->update($data);
        return $iCategoryProductId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('categoryproduct')->where('iCategoryProductId',$where['iCategoryProductId'])->delete();
    }
    
    // Office Data update start
    public static function AddOfficePrice($data)
    {
        $add = DB::table('office_prices')->insertGetId($data);
        return $add;
    }

    public static function UpdatedOfficePrice($office_data,$office_where)
    {
        $sql = DB::table('office_prices');
        $sql->where($office_where);
        $update = $sql->update($office_data);
        return $update;
    }

    public static function checkExistOffice($iLoginAdminId,$iTypeId)
    {
        $SQL = DB::table('office_prices');
        $SQL->where('iLoginAdminId',$iLoginAdminId);
        $SQL->where('iTypeId',$iTypeId);
        return $SQL->get();
    }
    // Office Data update end
}
