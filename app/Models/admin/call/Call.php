<?php

namespace App\Models\admin\call;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Call extends Model
{
    use HasFactory;

    protected $table        = 'call';

    protected $primaryKey   = 'iCallId';

    public $timestamps      = false;

    protected $fillable     = ['iCallId','vName', 'vColor', 'iQuantity', 'iSequence', 'eStatus','dtAddedDate','dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        $SQL = DB::table("call");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vName', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }

        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }
    public static function get_all_call()
    {
        $SQL = DB::table("call");

        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iCallId)
    {
        $SQL = DB::table("call");
        $SQL->where("iCallId", $iCallId);
        $result = $SQL->get();
        return $result->first(); if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
    }
    public static function add($data)
    {
        $add = DB::table('call')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iCallId = DB::table('call');
        $iCallId->where('iCallId',$where['iCallId'])->update($data);
        return $iCallId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('call')->where('iCallId',$where['iCallId'])->delete();
    }

    public static function get_by_ids($iSlipId,$iCaseId)
    {
        $SQL = DB::table("call");
        // $SQL->where("iSlipId", $iSlipId);
        $SQL->where("iCaseId", $iCaseId);
        $SQL->groupBy('iSlipId');
        $result = $SQL->get();

        foreach ($result as $key => $value) 
        {
            $SQL = DB::table("call");
            $SQL->where('iCaseId',$value->iCaseId);
            $SQL->where('iSlipId',$value->iSlipId);
            $result[$key]->call_log = $SQL->get();

        } 
        return $result; 
    }
}
