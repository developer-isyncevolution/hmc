<?php

namespace App\Models\admin\login;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Login extends Model
{
    use HasFactory;
    
    protected $table    = 'admin';

     public static function login($vEmail, $vPassword)
    {
        $SQL = DB::table("admin")->where('email',$vEmail)->where('password',$vPassword);
        $result = $SQL->first();
        return $result;
    }
    public static function authentication($criteria = array())
    {
        $SQL = DB::table("customer");
        if($criteria['vAuthCode'])
        {
            $SQL->where("vUniqueCode", $criteria["vAuthCode"]);
        }
        $result = $SQL->get();
        return $result->first();
    }

}
