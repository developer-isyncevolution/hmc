<?php

namespace App\Models\admin\stage_grade;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class StageGrade extends Model
{
    use HasFactory;

    protected $table        = 'stage_grade';

    protected $primaryKey   = 'iProductGradeId';

    public $timestamps      = false;

    protected $fillable     = ['iGradeId', 'iCategoryStageId'];


    public static function get_all_data($criteria = array())
    {

        $SQL = DB::table('stage_grade');
       
        $SQL->where($criteria);

        $result = $SQL->get();
        return $result;
    }
    public static function get_all_category($criteria = array())
    {
        $SQL = DB::table("category");
        if(!empty($criteria["eType"]))
        {
            $SQL->where("eType", $criteria["eType"]);
        }
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_grade()
    {
        $SQL = DB::table("grade");
        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iCategoryStageId)
    {
        $SQL = DB::table("stage_grade");
        $SQL->where("iCategoryStageId", $iCategoryStageId);
        $result = $SQL->get();
        return $result; 
    }
    public static function get_by_product_id($iCategoryStageId)
    {
        $SQL = DB::table("stage_grade");
        $SQL->join('grade','grade.iGradeId','stage_grade.iGradeId');
        $SQL->where("stage_grade.iCategoryStageId", $iCategoryStageId);
        $result = $SQL->get(['stage_grade.*','grade.vQualityName']);
        return $result; 
    }
    
    public static function get_grade_by_product($iCategoryStageId)
    {
        $SQL = DB::table("stage_grade");
        $SQL->where("iCategoryStageId", $iCategoryStageId);
        $result = $SQL->get();
        return $result; 
    }
    public static function add($data)
    {
        $add = DB::table('stage_grade')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = [])
    {
        $iCategoryStageId = DB::table('stage_grade');
        $iCategoryStageId->where('iCategoryStageId',$where['iCategoryStageId'])->update($data);
        return $iCategoryStageId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('stage_grade')->where('iCategoryStageId',$where['iCategoryStageId'])->delete();
    }
    
    public static function FetchStageGradePrice($iStageId,$iGradeId)
    {
        $SQL = DB::table("stage_grade");
        $SQL->where('iCategoryStageId',$iStageId);
        $SQL->where('iGradeId',$iGradeId);
        $result = $SQL->get()->first();
        return $result;
    }
}
