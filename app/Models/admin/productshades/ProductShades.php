<?php

namespace App\Models\admin\productshades;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ProductShades extends Model
{
    use HasFactory;

    protected $table        = 'product_gumshades';

    protected $primaryKey   = 'iProductGradeId';

    public $timestamps      = false;

    protected $fillable     = ['iGradeId', 'iCategoryProductId'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {

        $SQL = DB::table('product_gumshades');
        $SQL->join('gumshades', 'gumshades.iGumShadesId', '=', 'product_gumshades.iGumShadesId');

        $result = $SQL->get(['product_gumshades.*', 'gumshades.vGumShade as vGumShadeName']);
        return $result;

    }
    public static function get_all_category($criteria = array())
    {
        $SQL = DB::table("category");
        if(!empty($criteria["eType"]))
        {
            $SQL->where("eType", $criteria["eType"]);
        }
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_grade()
    {
        $SQL = DB::table("grade");
        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iCategoryProductId)
    {
        $SQL = DB::table("product_gumshades");
        $SQL->where("iCategoryProductId", $iCategoryProductId);
        $result = $SQL->get();
        return $result; 
    }
    public static function get_by_product_id($iCategoryProductId)
    {
        $SQL = DB::table("product_gumshades");
        $SQL->join('grade','grade.iGradeId','product_gumshades.iGradeId');
        $SQL->where("product_gumshades.iCategoryProductId", $iCategoryProductId);
        $result = $SQL->get(['product_gumshades.*','grade.vQualityName']);
        return $result; 
    }
    
    public static function get_grade_by_product($iCategoryProductId)
    {
        $SQL = DB::table("product_gumshades");
        $SQL->where("iCategoryProductId", $iCategoryProductId);
        $result = $SQL->get();
        return $result; 
    }
    public static function add($data)
    {
        $add = DB::table('product_gumshades')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = [])
    {
        $iCategoryProductId = DB::table('product_gumshades');
        $iCategoryProductId->where('iCategoryProductId',$where['iCategoryProductId'])->update($data);
        return $iCategoryProductId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('product_gumshades')->where('iCategoryProductId',$where['iCategoryProductId'])->delete();
    }
}
