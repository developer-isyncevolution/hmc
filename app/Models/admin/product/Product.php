<?php

namespace App\Models\admin\product;

use App\Models\admin\category;
use App\Models\admin\subcategory;
use App\Models\admin\productgallery;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model

{
    use HasFactory;

    protected $table                    = 'product';
    protected $table_cat                = 'category';
    protected $table_subcat             = 'subcategory';
    protected $table_product_gallery    = 'product_gallery';
    protected $table_product_attributes = 'product_attributes';
    protected $table_product_attributes_option = 'product_attribute_options';
    protected $table_attribute          = 'attribute';
    protected $table_product_category   = 'product_category';

    protected $primaryKey = 'iProductId';

    public $timestamps = false;

    protected $fillable = ['iProductId','vTitle', 'tDescription','iStock','iPrice', 'vLocation', 'vImage', 'dtAddedDate', 'dtUpdatedDate', 'eStatus'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("product");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vTitle', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }

        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }

    public static function get_by_id($iProductId)
    {
        $SQL = DB::table("product");
        $SQL->where("iProductId", $iProductId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_by_vPgaeCode($vPageCode)
    {
        $SQL = DB::table("product");
        $SQL->where("vPageCode", $vPageCode);
        $result = $SQL->get();
        
        foreach ($result as $key => $value) 
        {
            $SQL = DB::table("product_gallery");
            $SQL->where('iProductId',$value->iProductId);
            
            $result[$key]->product_gallery = $SQL->get();
        }
        return $result->first();
    }
    public static function get_subcat_data($vPageCode)
    {
        $SQL = DB::table("subcategory");
        $SQL->where("vPageCode", $vPageCode);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_by_iSubId($iSubCategoryId)
    {
        $SQL = DB::table("product_category");
        $SQL->where("iSubCategoryId", $iSubCategoryId);
        $result = $SQL->get();
        return $result;
    }
    public static function get_by_iProductId($iProductId)
    {
        $SQL = DB::table("product_attributes");
        $SQL->where("iProductId", $iProductId);
        $result = $SQL->get();

        foreach ($result as $key => $value) 
        {
            $SQL = DB::table("attribute");
            $SQL->where('iAttributeId',$value->iAttributeId);
            
            $result[$key]->product_attributes = $SQL->get();

            foreach ($result[$key]->product_attributes as $k1 => $val) 
            {                
                $SQL = DB::table("product_attribute_options");
                $SQL->where('iAttributeId',$val->iAttributeId);
                
                $result[$key]->product_attributes[$k1]->product_attribute_option = $SQL->get();
            }
        }
        return $result->first();
    }
    public static function get_by_iCategoryId()
    {
        $SQL = DB::table("category");
        $result = $SQL->get();
        return $result;
    }
    public static function get_by_iSubCategoryId()
    {
        $SQL = DB::table("subcategory");
        $result = $SQL->get();
        return $result;
    }

    public static function add($data)
    {
        $add = DB::table('product')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iProductId = DB::table('product');
        $iProductId->where('iProductId',$where['iProductId'])->update($data);
        return $iProductId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('product')->where('iProductId',$where['iProductId'])->delete();
    }

    public static function get_all_product(){
        $SQL = DB::table("product");

        $result = $SQL->get();
        return $result;
    }
}
