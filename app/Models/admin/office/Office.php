<?php

namespace App\Models\admin\office;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class Office extends Model
{
    use HasFactory;

    public $table           = 'office';

    public $table_customer = 'customer';

    protected $primaryKey   = 'iOfficeId';

    public $timestamps      = false;

    protected $fillable     = ['iOfficeId', 'iCustomerTypeId','vName', 'vTitle', 'vWebsiteName', 'tDescription', 'vOfficeName', 'vOfficeCode', 'vAddress', 'vCity', 'iStateId', 'vZipCode', 'vEmail', 'vPassword','vImage', 'vMobile', 'vCellulor', 'eStatus', 'dtAddedDate', 'dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        // $SQL = DB::table("office");

         $SQL = DB::table('office');
        $SQL->join('customer', 'customer.iCustomerId', '=', 'office.iCustomerId');

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('office.vName', 'like', '%' . $criteria['vKeyword'] . '%')->orWhere('office.vOfficeName', 'like', '%' . $criteria['vKeyword'] . '%')->orWhere('office.vTitle', 'like', '%' . $criteria['vKeyword'] . '%');
        }
        
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy('office.'.$criteria['column'],$criteria['order']);
        }   
         if(!empty($criteria["iCustomerId"]))
        {
            $SQL->where("office.iCustomerId", $criteria["iCustomerId"]);
        }  

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get(['office.*', 'customer.vOfficeName as vCustomerName']);
        return $result;


        // $SQL = DB::table("office")->select("office.*", DB::raw("(SELECT customer.vOfficeName FROM customer WHERE customer.iCustomerId = office.iCustomerId) as vCustomerName"))->orderBy("iOfficeId", "DESC"); 
        // if($criteria['vKeyword'] != "")
        // {
        //     $SQL->where('vName', 'like', '%' . $criteria['vKeyword'] . '%');
        //     $SQL->orWhere('vTitle', 'like', '%' . $criteria['vKeyword'] . '%');
        //     $SQL->orWhere('vOfficeName', 'like', '%' . $criteria['vKeyword'] . '%');
        // }

        // if(!empty($criteria["eStatus"]))
        // {
        //     $SQL->where("eStatus", $criteria["eStatus"]);
        // }

        // if($criteria['column'] || $criteria['order'] != "")
        // {
        //     $SQL->orderBy($criteria['column'],$criteria['order']);
        // }
        // if(!empty($criteria["iCustomerId"]))
        // {
        //     $SQL->where("iCustomerId", $criteria["iCustomerId"]);
        // }    

        // if($paging == true)
        // {
        //     $SQL->limit($limit);
        //     $SQL->skip($start);
        // }

        // $result = $SQL->get();
        // return $result;
    }

    public static function get_office_id()
    {
        $SQL = DB::table("office");
        $SQL->orderBy("iOfficeId", "DESC");
        $result = $SQL->get();
        return $result->first();
    }

    public static function get_office_by_customerid()
    {
        $SQL = DB::table("office");
        $SQL->orderBy("iOfficeId", "DESC");
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_customer()
    {
        $SQL = DB::table("customer");
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_active_office(){
        
        $SQL = DB::table("office");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }
    
    public static function get_by_id($iOfficeId)
    {
        $SQL = DB::table("office");
        $SQL->where("iOfficeId", $iOfficeId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('office')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('office');
        $id->where('iOfficeId',$where['iOfficeId'])->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('office')->where('iOfficeId',$where['iOfficeId'])->delete();
    }
}
