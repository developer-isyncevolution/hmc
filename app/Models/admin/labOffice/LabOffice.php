<?php

namespace App\Models\admin\labOffice;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Session;
use DB;

class LabOffice extends Model
{
    use HasFactory;

    public $table           = 'lab_office';

    protected $primaryKey   = 'iLabOfficeId';

    public $timestamps      = false;

    protected $fillable     = ['iLabOfficeId','iOfficeId', 'iLabId','eStatus','dtUpdatedDate','dtAddedDate'];

    public static function get_by_officeid($id)
    {
        $SQL = DB::table("lab_office");
        $SQL->where("iOfficeId", $id);
        $result = $SQL->get();
        return $result;
    }
    public static function get_active_office_by_labId($id)
    {
        $SQL = DB::table("lab_office");
        $SQL->where("iLabId", $id);
        $result = $SQL->get();
        return $result;
    }
    public static function get_active_lab_by_officeId($id)
    {
        $SQL = DB::table("lab_office");
        $SQL->where("iOfficeId", $id);
        $SQL->where("eStatus", "Active");
        $result = $SQL->get();
        return $result;
    }
    public static function get_by_labid($id)
    {
        $SQL = DB::table("lab_office")->select("lab_office.*", DB::raw("(SELECT customer.vOfficeName FROM customer WHERE lab_office.iOfficeId = customer.iCustomerId LIMIT 1) as vOfficeName"));
        $SQL->where("iLabId", $id);
        $SQL->orderBy('vOfficeName','asc');
        $result = $SQL->get();
        return $result;
    }

    public static function add($data)
    {
        $add = DB::table('lab_office')->insertGetId($data);
        return $add;
    }
    
    public function update(array $where = [], array $data = []){
        $id = DB::table('case');
        $id->where('iCaseId',$where['iCaseId'])->update($data);
        return $id;
    }
    public static function update_data($where = array(),$data = array()){
        $id = DB::table('lab_office');
        $id->where($where)->update($data);
        return $id;
    }
    public static function delete_by_id(array $where = [])
    {
        DB::table('case')->where('iCaseId',$where['iCaseId'])->delete();
    }
}
