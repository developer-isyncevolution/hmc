<?php

namespace App\Models\admin\toothbrand;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class ToothBrand extends Model
{
    use HasFactory;

    public $table           = 'toothbrand';

    protected $primaryKey   = 'iToothBrandId';

    public $timestamps      = false;

    protected $fillable     = ['iToothBrandId', 'iToothBrandId', 'vCode', 'iSequence',  'eStatus', 'dtAddedDate', 'dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("toothbrand"); 
        if($criteria['vKeyword'] != "")
        {
            $SQL->orWhere('vName', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy('iSequence',$criteria['order']);
        }    
        
        $SQL->orderBy('iSequence','ASC');
        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }

    public static function get_all_brand()
    {
        $SQL = DB::table("toothbrand");
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_brand_by_customerid($id) // Krupesh Thakkar New Change
    {
        $SQL = DB::table("toothbrand");
        $SQL->where('iCustomerId',$id);
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_active_toothbrand(){
        
        $SQL = DB::table("toothbrand");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }
    public static function get_by_id($iToothBrandId)
    {
        $SQL = DB::table("toothbrand");
        $SQL->where("iToothBrandId", $iToothBrandId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('toothbrand')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('toothbrand');
        $id->where('iToothBrandId',$where['iToothBrandId'])->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('toothbrand')->where('iToothBrandId',$where['iToothBrandId'])->delete();
    }
}
