<?php

namespace App\Models\admin\driver_history;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Libraries\General;
use DB;

class DriverHistory extends Model
{
    use HasFactory;

    public $table           = 'driver_history';

    protected $primaryKey   = 'iDriverHistoryId';

    public $timestamps      = false;

    
    public static function get_history_data($iSlipId='')
    {
        $SQL= DB::table('slip');
        $SQL->join('customer', 'slip.iOfficeId', '=', 'customer.iCustomerId');
        $SQL->where('slip.iSlipId',$iSlipId);
        $data = $SQL->get(['slip.iCaseId','slip.vPatientName','slip.vCaseNumber','slip.iOfficeId','slip.iCasepanId','slip.vCasePanNumber','slip.vLocation','customer.vOfficeName','customer.vOfficeCode'])->first();
        return $data;
    }

    public static function insert($data=array())
    {
        $ReturnData = DB::table('driver_history')->insertGetId($data);
        return $ReturnData;
    }

    public static function update_slip_location($where_array=array(),$data=array())
    {
        if($data!='')
        {
            DB::table('slip')->where($where_array)->update($data);
            $result = DB::table('case')->where('iCaseId',$where_array['iCaseId'])->update($data);
            return $result;
        }
    }

    public static function update_product_status($where_array=array(),$data=array())
    {
        if($data!='')
        {
            $result = DB::table('slip_product')->where('iSlipId',$where_array['iSlipId'])->update($data);
            return $result;
        }
    }

    public static function show_history($where_array= array())
    {
        $result = DB::table('driver_history')
            ->where($where_array)
            ->groupBy('iSlipId')
            ->get();
        foreach ($result as $key => $value) 
        {
            $SQL = DB::table("driver_history");
            $SQL->where('iCaseId',$value->iCaseId);
            $SQL->where('iSlipId',$value->iSlipId);
            $result[$key]->driver_history = $SQL->get();

        } 
        return $result;
    }

    public static function show_paper_slip_history($criteria= array())
    {
        // dd($criteria);
        $SQL = DB::table('driver_history');
        $SQL->leftJoin('slip', 'slip.iSlipId', '=', 'driver_history.iSlipId');
        $SQL->leftJoin('case', 'slip.iCaseId', '=', 'case.iCaseId');
        $SQL->leftJoin('productstage', 'driver_history.iStageId', '=', 'productstage.iProductStageId');

        // $SQL->leftJoin('doctor', 'doctor.iDoctorId', '=', 'slip.iDoctorId');
        $SQL->leftJoin('customer', 'slip.iOfficeId', '=', 'customer.iCustomerId');
        // $SQL->leftJoin('doctor', 'doctor.iCustomerId', '=', 'customer.iCustomerId');
        if(isset($criteria['startdate']) && !empty($criteria['startdate']) && isset($criteria['enddate']) && !empty($criteria['enddate']))
        {
            $SQL->whereBetween('slip.dDeliveryDate',[$criteria['startdate'], $criteria['enddate']]);
        }
        elseif(isset($criteria['startdate']) && !empty($criteria['startdate']))
        {
            $SQL->where('slip.dDeliveryDate',$criteria['startdate']);
        }
        elseif(isset($criteria['enddate']) && !empty($criteria['enddate']))
        {
            $SQL->where('slip.dDeliveryDate',$criteria['enddate']);
        }
        // $SQL->where('slip.dDeliveryDate',date('Y-m-d'));
        $CustomerType = General::admin_info()['customerType'];
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']) && $CustomerType =='Lab Admin')
        {
            $SQL->where('slip.iLabId',$criteria['iCustomerId']);
        }
        elseif(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']) && $CustomerType =='Office Admin')
        {
            $SQL->where('slip.iOfficeId',$criteria['iCustomerId']);
        }
        if(isset($criteria['iSlipId']) && !empty($criteria['iSlipId']))
        {
            $SQL->whereIn('slip.iSlipId',$criteria['iSlipId']);
        }
        if(isset($criteria['deliveryDateArray']) && !empty($criteria['deliveryDateArray']))
        {
            // $SQL->whereIn('slip.dDeliveryDate',$criteria['deliveryDateArray']);
            // $SQL->where('slip.dDeliveryDate',date('Y-m-d'));
            $MaxdeliveryDate = max($criteria['deliveryDateArray']);
            // $SQL->where('slip.dDeliveryDate','=<',date('Y-m-d',($MaxdeliveryDate)));
            $SQL->whereBetween('slip.dDeliveryDate', [date('Y-m-d'),date('Y-m-d',($MaxdeliveryDate))]);
        }
        $SQL->select('driver_history.*','productstage.vCode','slip.dPickUpDate','slip.dPickUpDate','slip.dDeliveryDate','slip.tDeliveryTime','slip.vCaseNumber','customer.vOfficeCode', DB::raw("(SELECT doctor.vFirstName FROM doctor WHERE doctor.iDoctorId = case.iDoctorId LIMIT 1) as vDoctorFirstName"),DB::raw("(SELECT doctor.vLastName FROM doctor WHERE doctor.iDoctorId = case.iDoctorId LIMIT 1) as vDoctorLastName"))->orderBy("iCaseId", "DESC");
        $SQL->where('slip.vLocation','!=','In office');

        $SQL->groupBy('slip.iSlipId');
        // 'doctor.vFirstName As vDoctorFirstName','doctor.vFirstName As vDoctorLastName',
        $result = $SQL->get(['driver_history.*','slip.dPickUpDate','slip.dPickUpDate','slip.dDeliveryDate','slip.tDeliveryTime','slip.vCaseNumber','customer.vOfficeCode','productstage.vCode']);

        return $result;
    }
    public static function get_slip_data($where_array= array())
    {
        $result = DB::table('slip')->where($where_array)->get()->first();
        return $result;
    }
    public static function undo_status($where_array= array())
    {
        DB::table('driver_history')->where($where_array)->delete();
    }

    public static function get_slip_product_by_slipid($iSlipId)
    {
        $SQL = DB::table("slip_product");
        $SQL->where('iSlipId',$iSlipId);
        $result = $SQL->get()->first();
        return $result;
    }


    
}
