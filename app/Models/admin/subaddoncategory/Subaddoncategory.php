<?php

namespace App\Models\admin\subaddoncategory;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class Subaddoncategory extends Model
{
    use HasFactory;
    protected $table        = 'subaddon_category';

    protected $primaryKey   = 'iSubAddCategoryId';

    public $timestamps      = false;

    protected $fillable     = ['iSubAddCategoryId','iAddCategoryId', 'vAddonName', 'vCode','vJaws','iPrice','vSequence','eStatus'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        $SQL = DB::table("subaddon_category");
        if($criteria['vKeyword'] != "")
        {
            $SQL->where(function ($query) use ($criteria) {
            $query->where('vAddonName','LIKE','%'.$criteria['vKeyword'].'%');
            $query->orWhere('vCode', 'like', '%' . $criteria['vKeyword'] . '%');
            $query->orWhere('vJaws', 'like', '%' . $criteria['vKeyword'] . '%');
            $query->orWhere('iPrice', 'like', '%' . $criteria['vKeyword'] . '%');
            $query->orWhere('vSequence', 'like', '%' . $criteria['vKeyword'] . '%');
            $query->orWhere('eStatus', 'like', '%' . $criteria['vKeyword'] . '%');
            });
        }
        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        if(!empty($criteria["iAddCategoryId"]))
        {
            $SQL->where("iAddCategoryId", $criteria["iAddCategoryId"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end
        if($criteria['column'] != '' || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }
        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }
        if(!empty($criteria["eType"]))
        {
            if($criteria["eType"] == "Upper"){
                $SQL->where(function ($query) use ($criteria) {
                    $query->where('eType',"Upper");
                    $query->orWhere('eType',"Both");
                }); 
            }
            if($criteria["eType"] == "Lower"){
                $SQL->where(function ($query) use ($criteria) {
                    $query->where('eType',"Lower");
                    $query->orWhere('eType',"Both");
                }); 
            }
        }
        $result = $SQL->get();
        return $result;
    }
    
    public static function get_by_id($iAddCategoryId)
    {
        $SQL = DB::table("subaddon_category");
        $SQL->where("iSubAddCategoryId", $iAddCategoryId);
        $result = $SQL->get();
        return $result->first(); if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
    }
    public static function get_addons_by_category($iAddCategoryId,$eType)
    {
        $SQL = DB::table("subaddon_category");
        $SQL->where("iAddCategoryId", $iAddCategoryId);
        $SQL->where(function ($query) use ($eType) {
            $query->where("eType", $eType);
            $query->orWhere('eType', 'Both');
        });
        $result = $SQL->get();
        return $result;
    }
    public static function add($data)
    {
        
        $add = DB::table('subaddon_category')->insertGetId($data);
      
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iSubAddCategoryId = DB::table('subaddon_category');
        $iSubAddCategoryId->where('iSubAddCategoryId',$where['iSubAddCategoryId'])->update($data);
        return $iSubAddCategoryId;
    }
    public static function get_all_category()
    {
        $SQL = DB::table("addon_category");
        
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_upper_category($criteria = array())
    {
        $SQL = DB::table("addon_category");
        if(!empty($criteria["eType"]))
        {
            if($criteria["eType"] == "Upper"){
                $SQL->where(function ($query) use ($criteria) {
                    $query->where('eType',"Upper");
                    $query->orWhere('eType',"Both");
                }); 
            }
            if($criteria["eType"] == "Lower"){
                $SQL->where(function ($query) use ($criteria) {
                    $query->where('eType',"Lower");
                    $query->orWhere('eType',"Both");
                }); 
            }
        }
        $SQL->orderBy('vSequenc','ASC');
        $result = $SQL->get();
        return $result;
    }
    public static function delete_by_id(array $where = [])
    {
        DB::table('subaddon_category')->where('iSubAddCategoryId',$where['iSubAddCategoryId'])->delete();
    }
    public static function delete_by_iTypeId_id($iSubAddCategoryId)
    {
        DB::table('office_prices')->where('iTypeId',$iSubAddCategoryId)->delete();
    }

      // Office Data update start
      public static function AddOfficePrice($data)
      {
          $add = DB::table('office_prices')->insertGetId($data);
          return $add;
      }
  
      public static function UpdatedOfficePrice($office_data,$office_where)
      {
          $sql = DB::table('office_prices');
          $sql->where($office_where);
          $update = $sql->update($office_data);
          return $update;
      }
  
      public static function checkExistOffice($iLoginAdminId,$iTypeId)
      {
          $SQL = DB::table('office_prices');
          $SQL->where('iLoginAdminId',$iLoginAdminId);
          $SQL->where('iTypeId',$iTypeId);
          return $SQL->get();
      }
      public static function checkExistOfficeById($where_ofc)
      {
          $SQL = DB::table('office_prices');
          $SQL->where($where_ofc);
          return $SQL->get()->first();
      }
      // Office Data update end
}
