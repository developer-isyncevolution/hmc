<?php

namespace App\Models\admin\schedule;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Libraries\General;

class Schedule extends Model
{
    use HasFactory;
    protected $table        = 'schedule';

    protected $primaryKey   = 'iScheduleId';

    public $timestamps      = false;

    protected $fillable     = ['iScheduleId','iYear', 'vMonth', 'iDay','vWeek','tiOpen','tiClose','vTitle','eStatus'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        // \DB::enableQueryLog();
        $SQL = DB::table("schedule");
        $SQL->Join("customer", "customer.iCustomerId", "=", "schedule.iCustomerId")
            // ->where('schedule.iYear','LIKE','%'.$criteria['vKeyword'].'%')
            // ->orWhere('schedule.eMonth', 'like', '%' . $criteria['vKeyword'] . '%')
            // ->orWhere('schedule.iDay', 'like', '%' . $criteria['vKeyword'] . '%')
            // ->orWhere('schedule.eWeek', 'like', '%' . $criteria['vKeyword'] . '%')
            // ->orWhere('schedule.vTitle', 'like', '%' . $criteria['vKeyword'] . '%')
            // ->orWhere('schedule.eStatus', 'like', '%' . $criteria['vKeyword'] . '%')
            // ->orderBy("iScheduleId", "ASC")
            ->select('schedule.*', 'customer.vOfficeName as vCustomerName');

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('schedule.iYear', 'like', '%' . $criteria['vKeyword'] . '%')
            ->where('schedule.iYear','LIKE','%'.$criteria['vKeyword'].'%')
            ->orWhere('schedule.eMonth', 'like', '%' . $criteria['vKeyword'] . '%')
            ->orWhere('schedule.iDay', 'like', '%' . $criteria['vKeyword'] . '%')
            ->orWhere('schedule.eWeek', 'like', '%' . $criteria['vKeyword'] . '%')
            ->orWhere('schedule.vTitle', 'like', '%' . $criteria['vKeyword'] . '%')
            ->orWhere('schedule.eStatus', 'like', '%' . $criteria['vKeyword'] . '%')
            ->orWhere('customer.vOfficeName', 'like', '%' . $criteria['vKeyword'] . '%');
        }
        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("schedule.eStatus", $criteria["eStatus"]);
        }
         // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('schedule.iCustomerId', $criteria["iCustomerId"]);
        }
         // For Perticular customer show end
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   
        
        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }
        
        $result = $SQL->get();
        // dd(\DB::getQueryLog());
        return $result;
    }
    public static function get_all_schedule()
    {
        // For Perticular customer show start
        $iCustomerId = General::admin_info()['iCustomerId'];
        // For Perticular customer show end
        $SQL = DB::table("schedule");
        $SQL->where('iCustomerId',$iCustomerId);
        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iScheduleId)
    {
        $SQL = DB::table("schedule");
        $SQL->where("iScheduleId", $iScheduleId);
        $result = $SQL->get();
        return $result->first(); if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
    }
    
    public static function get_by_customerid($iCustomerId)
    {
        $SQL = DB::table("schedule");
        $SQL->where("iCustomerId", $iCustomerId);
        return $SQL->get();
    }
    public static function get_holiday($iCustomerId)
    {
        $SQL = DB::table("schedule");
        $SQL->where("eStatus", 'CLOSED');
        $SQL->where("iCustomerId", $iCustomerId);
        return $SQL->get();
    }
    
    public static function add($data)
    {
        $add = DB::table('schedule')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iScheduleId = DB::table('schedule');
        $iScheduleId->where('iScheduleId',$where['iScheduleId'])->update($data);
        return $iScheduleId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('schedule')->where('iScheduleId',$where['iScheduleId'])->delete();
    }
}
