<?php

namespace App\Models\admin\notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Notification extends Model

{
    use HasFactory;

    protected $table        = 'notification';

    protected $primaryKey   = 'iNotificationId';

    public $timestamps      = false;

    protected $fillable     = ['iNotificationId', 'vNotificationCode', 'eEmail', 'eSms', 'dtAddedDate', 'dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("notification");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vNotificationCode', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }

        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }

    public static function get_by_id($iNotificationId)
    {
        $SQL = DB::table("notification");
        $SQL->where("iNotificationId", $iNotificationId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('notification')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iNotificationId = DB::table('notification');
        $iNotificationId->where('iNotificationId',$where['iNotificationId'])->update($data);
        return $iNotificationId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('notification')->where('iNotificationId',$where['iNotificationId'])->delete();
    }
}
