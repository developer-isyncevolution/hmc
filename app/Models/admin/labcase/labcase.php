<?php

namespace App\Models\admin\labcase;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\admin\customer\Customer;
use App\Models\admin\category\Category;
use Session;
use DB;
use App\Libraries\General;
use Carbon\Carbon; 
use Carbon\CarbonPeriod;

class Labcase extends Model
{
    use HasFactory;

    public $table           = 'case';

    protected $primaryKey   = 'iCaseId';

    public $timestamps      = false;

    protected $fillable     = ['iCaseId', 'iOfficeId', 'iDoctorId', 'vPatientName',  'iCasePanId', 'vCasePanNumber', 'vCaseNumber','iCreatedById','vCreatedByName','eCreatedByType','eStatus','dtAddedDate','dtUpdatedDate','iLabId'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
    
        $SQL = DB::table('case');
        $SQL->join('slip', 'slip.iCaseId', '=', 'case.iCaseId');
        $SQL->join('customer', 'customer.iCustomerId', '=', 'slip.iLabId');
        $SQL->leftJoin('casepan', 'casepan.iCasepanId', '=', 'case.iCasepanId');
        
        // For Search Start 
        if(isset($criteria['vKeyword']) && $criteria['vKeyword'] != "" )
        {
                $vKeyword = $criteria['vKeyword'];
                $SQL->where(function ($query) use ($vKeyword) {
                    $query->where('case.vPatientName', 'like', '%' . $vKeyword . '%');
                    $query->orWhere('case.vCasePanNumber', 'like', '%' . $vKeyword . '%');
                    $query->orWhere('case.vCaseNumber', 'like', '%' . $vKeyword . '%');
                    $query->orWhere('slip.vSlipNumber', 'like', '%' . $vKeyword . '%');
                    $query->orWhere('slip.tStagesNotes', 'like', '%' . $vKeyword . '%');
                });
             
        }
        if(isset($criteria['iSlipId']) && !empty($criteria['iSlipId']))
        {
            $SQL->where("slip.iSlipId", $criteria["iSlipId"]); 
        }
        if(isset($criteria['iOfficeId_list']) && $criteria['iOfficeId_list'] != "" && $criteria['iOfficeId_list'] != 'null')
        {
            $SQL->where("case.iOfficeId", $criteria["iOfficeId_list"]); 
        }
        if(isset($criteria['iDoctorId_list']) && $criteria['iDoctorId_list'] != "" && $criteria['iDoctorId_list'] != 'null')
        {
            $SQL->where("case.iDoctorId", $criteria["iDoctorId_list"]); 
        }
        if(isset($criteria['vLocation_list']) && $criteria['vLocation_list'] != "" && $criteria['vLocation_list'] != 'ALL')
        {
            $SQL->where("case.vLocation", $criteria["vLocation_list"]); 
        }
        if(isset($criteria['vLocation_array']) && $criteria['vLocation_array'] != "" )
        {
            $SQL->whereIn("case.vLocation", $criteria["vLocation_array"]); 
        }

        if(isset($criteria['eStatus_list']) && $criteria['eStatus_list'] == 'Trash')
        {
            $SQL->where("case.eIsDeleted", 'Yes'); 
        }
        elseif(isset($criteria['eStatus_list']) && $criteria['eStatus_list'] != "" && $criteria['eStatus_list'] != 'ALL')
        {
            $SQL->where("case.eStatus", $criteria["eStatus_list"]); 
            $SQL->where("case.eIsDeleted",'No');
        }  
        else
        {
            $SQL->where("case.eIsDeleted",'No');
        }
        
        if(isset($criteria['iLabId']) && $criteria['iLabId'] != "" )
        {
            $SQL->where("slip.iLabId", $criteria["iLabId"]); 
        }  
        
        
       
        // // For Search End 
        $SQL->select('case.*','slip.dtAddedDate','slip.vSlipNumber','slip.dDeliveryDate','slip.dPickUpDate','slip.iSlipId','customer.vOfficeName as vOfficeName','casepan.vName AS vCasePanName','casepan.vColor','case.vCasePanNumber', DB::raw('max(slip.iSlipId) iSlipId'), DB::raw('max(slip.dDeliveryDate) dDeliveryDate'),DB::raw('max(slip.dtAddedDate) dtAddedDate'),DB::raw("(SELECT customer.vOfficeName FROM customer WHERE slip.iOfficeId=customer.iCustomerId limit 1) as office_name__slip"),DB::raw("(SELECT customer.vOfficeCode FROM customer WHERE slip.iOfficeId=customer.iCustomerId limit 1) as vOffice__Code"));
        $SQL->groupBy('case.iCaseId');

        $SQL->orderByRaw("FIELD(case.vLocation , 'In lab', 'In office ready to pickup', 'On route to the lab','In lab ready to pickup','On route to the office','In office','Draft') ASC");

        $SQL->orderBy('dDeliveryDate', 'asc');


        // $total_record = $SQL->get()->count();
        // Session::put('limit_filter_labcase', $total_record);

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get(['case.*','slip.vSlipNumber','slip.dDeliveryDate','slip.iSlipId','customer.vOfficeName','casepan.vName AS vCasePanName','casepan.vColor','case.vCasePanNumber','customer.vOfficeCode','slip.dtAddedDate']);

        return $result;
    }
    public static function get_all_data_date($criteria = array(), $start = '', $limit = '', $paging = false){


        $SQL = DB::table('case');
        $SQL->join('slip', 'slip.iCaseId', '=', 'case.iCaseId');
        $SQL->join('customer', 'customer.iCustomerId', '=', 'slip.iLabId');
        $SQL->leftJoin('casepan', 'casepan.iCasepanId', '=', 'case.iCasepanId');
        
        // For Search Start 
        if(isset($criteria['vKeyword']) && $criteria['vKeyword'] != "" )
        {
                $vKeyword = $criteria['vKeyword'];
                $SQL->where(function ($query) use ($vKeyword) {
                    $query->where('case.vPatientName', 'like', '%' . $vKeyword . '%');
                    $query->orWhere('case.vCasePanNumber', 'like', '%' . $vKeyword . '%');
                    $query->orWhere('case.vCaseNumber', 'like', '%' . $vKeyword . '%');
                    $query->orWhere('slip.vSlipNumber', 'like', '%' . $vKeyword . '%');
                    $query->orWhere('slip.tStagesNotes', 'like', '%' . $vKeyword . '%');
                });
             
        }
        if(isset($criteria['iOfficeId_list']) && $criteria['iOfficeId_list'] != "" && $criteria['iOfficeId_list'] != 'null')
        {
            $SQL->where("case.iOfficeId", $criteria["iOfficeId_list"]); 
        }
        if(isset($criteria['iDoctorId_list']) && $criteria['iDoctorId_list'] != "" && $criteria['iDoctorId_list'] != 'null')
        {
            $SQL->where("case.iDoctorId", $criteria["iDoctorId_list"]); 
        }
        if(isset($criteria['vLocation_list']) && $criteria['vLocation_list'] != "" && $criteria['vLocation_list'] != 'ALL')
        {
            $SQL->where("case.vLocation", $criteria["vLocation_list"]); 
        }

        if(isset($criteria['eStatus_list']) && $criteria['eStatus_list'] == 'Trash')
        {
            $SQL->where("case.eIsDeleted", 'Yes'); 
        }
        elseif(isset($criteria['eStatus_list']) && $criteria['eStatus_list'] != "" && $criteria['eStatus_list'] != 'ALL')
        {
            $SQL->where("case.eStatus", $criteria["eStatus_list"]); 
            $SQL->where("case.eIsDeleted",'No');
        }  
        else
        {
            $SQL->where("case.eIsDeleted",'No');
        }
        
        if(isset($criteria['iLabId']) && $criteria['iLabId'] != "" )
        {
            $SQL->where("slip.iLabId", $criteria["iLabId"]); 
        }  
        
        if(isset($criteria['vKeyword']) && $criteria['vKeyword'] != "" )
        {
            $SQL->groupBy('slip.iCaseId');
        }    
        $SQL->where("slip.dDeliveryDate",'>=',date('Y-m-d')); 
        // // For Search End 
        $SQL->select('slip.dDeliveryDate');
        $SQL->groupBy('case.iCaseId');
        
        

        $SQL->orderBy('slip.dDeliveryDate', 'asc');


        $total_record = $SQL->get()->count();
        Session::put('limit_filter_labcase', $total_record);

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get(['slip.dDeliveryDate']);

        return $result;
    }

    public static function get_all_data_office($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table('case');
        $SQL->join('slip', 'slip.iCaseId', '=', 'case.iCaseId');
        $SQL->join('customer', 'customer.iCustomerId', '=', 'slip.iLabId');
        $SQL->leftJoin('casepan', 'casepan.iCasepanId', '=', 'case.iCasepanId');
        
        // For Search Start 
        if(isset($criteria['vKeyword']) && $criteria['vKeyword'] != "" )
        {
                $vKeyword = $criteria['vKeyword'];
                $SQL->where(function ($query) use ($vKeyword) {
                    $query->where('case.vPatientName', 'like', '%' . $vKeyword . '%');
                    $query->orWhere('case.vCasePanNumber', 'like', '%' . $vKeyword . '%');
                    $query->orWhere('case.vCaseNumber', 'like', '%' . $vKeyword . '%');
                    $query->orWhere('slip.vSlipNumber', 'like', '%' . $vKeyword . '%');
                    $query->orWhere('slip.tStagesNotes', 'like', '%' . $vKeyword . '%');
                });
             
        }
        if(isset($criteria['iLabId_list']) && $criteria['iLabId_list'] != "" && $criteria['iLabId_list'] != 'null')
        {
            $SQL->where("slip.iLabId", $criteria["iLabId_list"]); 
        }
        if(isset($criteria['iDoctorId_list']) && $criteria['iDoctorId_list'] != "" && $criteria['iDoctorId_list'] != 'null')
        {
            $SQL->where("case.iDoctorId", $criteria["iDoctorId_list"]); 
        }
        if(isset($criteria['vLocation_list']) && $criteria['vLocation_list'] != "" && $criteria['vLocation_list'] != 'ALL')
        {
            $SQL->where("case.vLocation", $criteria["vLocation_list"]); 
        }
       
        if(isset($criteria['eStatus_list']) && $criteria['eStatus_list'] == 'Trash')
        {
            $SQL->where("case.eIsDeleted", 'Yes'); 
        }
        elseif(isset($criteria['eStatus_list']) && $criteria['eStatus_list'] != "" && $criteria['eStatus_list'] != 'ALL')
        {
            $SQL->where("case.eStatus", $criteria["eStatus_list"]); 
            $SQL->where("case.eIsDeleted",'No');
        }  
        else
        {
            $SQL->where("case.eIsDeleted",'No');
        }
        if(isset($criteria['iOfficeId']) && $criteria['iOfficeId'] != "" && $criteria['iOfficeId'] != 'null')
        {
            $SQL->where("case.iOfficeId", $criteria["iOfficeId"]); 
        }
        if(isset($criteria['vKeyword']) && $criteria['vKeyword'] != "" )
        {
            $SQL->groupBy('slip.iCaseId');
        }    
        // // For Search End 
        $SQL->select('case.*','slip.dDeliveryDate','slip.dtAddedDate','slip.iSlipId','customer.vOfficeName as vOfficeName','casepan.vName AS vCasePanName','casepan.vColor','case.vCasePanNumber', DB::raw('max(slip.iSlipId) iSlipId'),DB::raw('max(slip.dtAddedDate) dtAddedDate'),DB::raw('max(slip.dDeliveryDate) dDeliveryDate'));
        $SQL->groupBy('case.iCaseId');
        $SQL->orderByRaw("FIELD(case.vLocation , 'In lab', 'In office ready to pickup', 'On route to the lab','In lab ready to pickup','On route to the office','In office','Draft') ASC");
        $SQL->orderBy('dDeliveryDate', 'asc');
        

        $total_record = $SQL->get()->count();
        Session::put('limit_filter_labcase', $total_record);

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get(['case.*','slip.dDeliveryDate','slip.iSlipId','customer.vOfficeName','casepan.vName AS vCasePanName','casepan.vColor','case.vCasePanNumber','slip.dtAddedDate']);
        // dd($result);
        return $result;
    }

    public static function get_office_id()
    {
        $SQL = DB::table("case");
        $SQL->orderBy("iCaseId", "DESC");
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_slip_id()
    {
        $SQL = DB::table("slip");
        $SQL->orderBy("iSlipId", "DESC");
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_case_id($iSlipId)
    {
        $SQL = DB::table("slip");
        $SQL->where('iSlipId',$iSlipId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_old_slip_data($where_slip)
    {
        $SQL = DB::table("slip");
        $SQL->where($where_slip);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_old_slip_product_data($where_slip)
    {
        $SQL = DB::table("slip_product");
        $SQL->where($where_slip);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_all_active_case(){
        
        $SQL = DB::table("case");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }
    public static function get_by_id($iCaseId)
    {
        $SQL = DB::table("case")->select("case.*", DB::raw("(SELECT office.vOfficeName FROM office WHERE office.iOfficeId = case.iOfficeId LIMIT 1) as vOfficeName"),DB::raw("(SELECT doctor.vFirstName FROM doctor WHERE doctor.iDoctorId = case.iDoctorId LIMIT 1) as vFirstName"),DB::raw("(SELECT doctor.vLastName FROM doctor WHERE doctor.iDoctorId = case.iDoctorId LIMIT 1) as vLastName"))->orderBy("iCaseId", "DESC");
        
        $SQL->where("iCaseId", $iCaseId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function add($data)
    {
        $add = DB::table('case')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('case');
        $id->where('iCaseId',$where['iCaseId'])->update($data);
        return $id;
    }
    public static function update_data($where = array(),$data = array()){
        $id = DB::table('case');
        $id->where($where)->update($data);
        return $id;
    }
    public static function delete_by_id(array $where = [])
    {
        DB::table('case')->where('iCaseId',$where['iCaseId'])->delete();
    }
    public static function exist_slip_in_billing($iCaseId,$iSlipId)
    {
        return DB::table('billing')->where('iCaseId',$iCaseId)
                            ->where('iSlipId',$iSlipId)
                            ->get()->count();
    }
    public static function add_billing($data)
    {
        $add = DB::table('billing')->insertGetId($data);
        return $add;
    }


    
 

    public static function get_grade_price($iGradeId,$iOfficeId,$iStageId)
    {
        $SQL = DB::table('stage_grade');
        $SQL->where('iGradeId',$iGradeId);
        $SQL->where('iCategoryStageId',$iStageId);
        $gradePrice = $SQL->get('vPrice')->first();
        if(isset($gradePrice) && !empty($gradePrice))
        {
            return $gradePrice;
        }
        else
        {
            $SQL = DB::table('office_stage_price');
            $SQL->where('iGradeId',$iGradeId);
            $SQL->where('iOfficeId',$iOfficeId);
            $gradePrice = $SQL->get('vPrice')->first();            
            return $gradePrice;
        }
    }

    public static function update_billing($where = array(),$data = array())
    {
        $add = DB::table('billing');
        $add->where($where)->update($data);
        return $add;
    }
    
    public static function get_slip_data($iCaseId,$iSlipId)
    {
        return DB::table('slip')->where('iCaseId',$iCaseId)
                            ->where('iSlipId',$iSlipId)
                            ->get()->first();
    }
    public static function find_location_by_slip_id($iSlipIds)
    {
        $result =  DB::table('slip')->whereIn('iSlipId',$iSlipIds)->get(['iSlipId','vLocation']);
        return $result;
    }
    public static function get_same_day_delivery_data($time)
    {
        $labData = array();
        $labData = Customer::get_all_lab();
        
        if(count($labData)>0)
        {
            foreach ($labData as $key_lab => $value_lab) {
                $SlipData_By_Lab = Labcase::get_slip_data_by_lab_id($value_lab->iCustomerId,$time);
                $labData[$key_lab]->SlipData =$SlipData_By_Lab;
                if(count($SlipData_By_Lab)==0)
                {
                    unset($labData[$key_lab]);
                }
            }
            // dd($labData);
            return $labData;
        }
        else
        {
            return false;
        }
        
    }

    public static function get_slip_data_by_lab_id($iLabId,$time)
    {
        $SQL = DB::table('case');
        $SQL->join('slip', 'slip.iCaseId', '=', 'case.iCaseId');
        $SQL->join('customer', 'customer.iCustomerId', '=', 'slip.iLabId');
        $SQL->join('slip_product', 'slip.iSlipId', '=', 'slip_product.iSlipId');
        $SQL->where("case.eIsDeleted",'No');
        $SQL->where('slip.iLabId',$iLabId);
        $SQL->where('slip.vLocation','In office ready to pickup');
        $SQL->where('slip.eStatus','On Process');

        $SQL->select('case.*','slip.dtAddedDate','slip.vSlipNumber','slip.dDeliveryDate','slip.dPickUpDate','slip.iSlipId','customer.vOfficeName as vOfficeName','case.vCasePanNumber', DB::raw('max(slip.iSlipId) iSlipId'), DB::raw('max(slip.dDeliveryDate) dDeliveryDate'),DB::raw('max(slip.dtAddedDate) dtAddedDate'),DB::raw("(SELECT customer.vOfficeName FROM customer WHERE slip.iOfficeId=customer.iCustomerId limit 1) as office_name__slip"),DB::raw("(SELECT customer.vOfficeCode FROM customer WHERE slip.iOfficeId=customer.iCustomerId limit 1) as vOffice__Code"),'slip_product.iCategoryId');

        $SQL->groupBy('case.iCaseId');
        $SQL->orderBy('customer.vOfficeName', 'asc');    
        $result = $SQL->get(['case.*','slip.*','customer.*','slip_product.iCategoryId']);

      

        if(!empty($result))
        {
            foreach ($result as $key => $res_value) {
                $SQL = DB::table("category");
                $SQL->where('iCategoryId',$res_value->iCategoryId);
                $SQL->where('tPickTime', 'like', $time . '%');
                $Category_count =  $SQL->get()->count();
                
                if($Category_count==0)
                {
                    unset($result[$key]);
                }
            }
        }
        
        return $result;
    }
}
