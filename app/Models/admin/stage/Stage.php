<?php

namespace App\Models\admin\stage;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class Stage extends Model
{
    use HasFactory;

    public $table           = 'stage';

    protected $primaryKey   = 'iStageId';

    public $timestamps      = false;

    protected $fillable     = ['iStageId', 'vName', 'vTitle', 'vWebsiteName', 'tDescription', 'vOfficeName', 'vOfficeCode', 'vAddress', 'vCity', 'iStateId', 'vZipCode', 'vEmail', 'vPassword','vImage', 'vMobile', 'vCellulor', 'eStatus', 'dtAddedDate', 'dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("stage");
        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vName', 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->orWhere('vTitle', 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->orWhere('vOfficeName', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }

        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }    

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }

    public static function get_office_id()
    {
        $SQL = DB::table("stage");
        $SQL->orderBy("iStageId", "DESC");
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_all_active_stage(){
        
        $SQL = DB::table("stage");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }
    public static function get_by_id($iStageId)
    {
        $SQL = DB::table("stage");
        $SQL->where("iStageId", $iStageId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('stage')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('stage');
        $id->where('iStageId',$where['iStageId'])->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('stage')->where('iStageId',$where['iStageId'])->delete();
    }
}
