<?php

namespace App\Models\admin\gumbrand;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class GumBrand extends Model
{
    use HasFactory;

    public $table           = 'gumbrand';

    protected $primaryKey   = 'iGumBrandId';

    public $timestamps      = false;

    protected $fillable     = ['iGumBrandId', 'iGumBrandId', 'vName', 'iSequence',  'eStatus', 'dtAddedDate', 'dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("gumbrand"); 
        if($criteria['vKeyword'] != "")
        {
            $SQL->orWhere('vName', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy('iSequence',$criteria['order']);
        }    

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }

    public static function get_all_brand()
    {
        $SQL = DB::table("gumbrand");
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_brand_by_customerid($id='') // Krupesh Thakkar New Change
    {
        $SQL = DB::table("gumbrand");
        $SQL->where('iCustomerId',$id);
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_by_id($id) // Krupesh Thakkar New Change
    {
        $SQL = DB::table("gumbrand");
        $SQL->whereIn('iGumBrandId',$id);
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_active_gumbrand(){
        
        $SQL = DB::table("gumbrand");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }
    public static function get_by_id($iGumBrandId)
    {
        $SQL = DB::table("gumbrand");
        $SQL->where("iGumBrandId", $iGumBrandId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('gumbrand')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('gumbrand');
        $id->where('iGumBrandId',$where['iGumBrandId'])->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('gumbrand')->where('iGumBrandId',$where['iGumBrandId'])->delete();
    }
}
