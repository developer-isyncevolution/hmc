<?php

namespace App\Models\admin\slipaddons;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class SlipAddons extends Model
{
    use HasFactory;

    protected $table        = 'slip_addons';

    protected $primaryKey   = 'iSlipAddonsId';

    public $timestamps      = false;

    protected $fillable     = ['iSlipId', 'iAddCategoryId', 'iSubAddCategoryId','iQuantity','eType'];



    public static function add($data)
    {
        $add = DB::table('slip_addons')->insertGetId($data);
        return $add;
    
    }
    public static function get_by_iSlipId($criteria = array())
    {
        $SQL = DB::table("slip_addons")->select("slip_addons.*", DB::raw("(SELECT addon_category.vCategoryName FROM addon_category WHERE addon_category.iAddCategoryId = slip_addons.iAddCategoryId) as vAddonCategoryName"),DB::raw("(SELECT subaddon_category.vAddonName FROM subaddon_category WHERE subaddon_category.iSubAddCategoryId = slip_addons.iSubAddCategoryId) as vAddonName"))->orderBy("iSlipAddonsId", "DESC");
        
        $SQL->where("iSlipId", $criteria['iSlipId']);
        $SQL->where("eType", $criteria['eType']);
        $SQL->orderBy("iSlipAddonsId", "DESC");
        $result = $SQL->get();
        return $result;
    }
    public static function delete_by_id(array $where = [])
    {
        $SQL = DB::table('slip_addons');
        $SQL->where('iSlipId',$where['iSlipId']);
        $SQL->where('eType',$where['eType']);
        $SQL->delete();
    }
    
}
