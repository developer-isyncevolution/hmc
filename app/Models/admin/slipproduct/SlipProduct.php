<?php

namespace App\Models\admin\slipproduct;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class SlipProduct extends Model
{
    use HasFactory;

    public $table           = 'slip_product';

    protected $primaryKey   = 'iSlipProductId';

    public $timestamps      = false;

    protected $fillable     = ['iSlipId', 'eType', 'iCategoryId', 'iProductId', 'iGradeId', 'iStageId', 'eExtraction', 'eGumShade', 'eTeethShade', 'eImpression','eAlginate','iAlginateNumber','ePVS','eStatus', 'iPVSNumber', 'eRelined', 'iRelinedNumber' ,'ePickUp', 'iPickUpNumber' ,'eRelinedAndPickUp', 'iRelinedAndPickUpNumber', 'eOtherImpression' ,'iOtherImpressionNumber' ,'eDigitalFile' , 'iDigitalFileNumber' ,'iToothBrandId', 'iToothShadesId', 'iGumBrandId', 'iGumShadesId' ,'vTeethInMouth' ,'vMissingTeeth' ,'vWillExtractOnDelivery','vHasBeenExtracted','vFixOrAdd', 'vClasps','dtAddedDate','dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("slip_product");
        if($criteria['vKeyword'] != "")
        {
            $SQL->where(function ($query) use ($criteria) {
                $query->where('vPatientName', 'like', '%' . $criteria['vKeyword'] . '%');
                $query->orWhere('vCasePanNumber', 'like', '%' . $criteria['vKeyword'] . '%');
            }); 
            
        }
        if(!empty($criteria["iOfficeId"]))
        {
            $SQL->where("iOfficeId", $criteria["iOfficeId"]);
        }
        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        if(!empty($criteria["iDoctorId"]))
        {
            $SQL->where("iDoctorId", $criteria["iDoctorId"]);
        }
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }
        $result = $SQL->get();
        return $result;
    }

    public static function get_office_id()
    {
        $SQL = DB::table("slip_product");
        $SQL->orderBy("iSlipProductId", "DESC");
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_all_active_case(){
        
        $SQL = DB::table("slip_product");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }
    public static function get_by_id($iSlipProductId)
    {
        $SQL = DB::table("slip_product");
        $SQL->where("iSlipProductId", $iSlipProductId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_by_slip_id($iSlipId)
    {
        $SQL = DB::table("slip_product");
        $SQL->join('productstage', 'slip_product.iStageId', '=', 'productstage.iProductStageId');
        $SQL->where("slip_product.iSlipId", $iSlipId);
        $result = $SQL->get('productstage.*');
        return $result;
    }
    public static function get_for_all_slip($iSlipId,$eType)
    {
        $SQL = DB::table("slip_product");
        $SQL->whereIn("iSlipId",$iSlipId);
        $SQL->where("eType",$eType);
        $SQL->orderBy("iSlipId","DESC");
        $result = $SQL->get();
        return $result;
    }
    public static function get_for_all_for_old_slips($iSlipId,$eType)
    {
        $SQL = DB::table("slip_product");
        $SQL->whereIn("iSlipId",$iSlipId);
        $SQL->where("eType",$eType);
        $SQL->orderBy("iSlipId","ASC");
        $result = $SQL->get();
        return $result;
    }
    public static function get_for_last_one_slip($iSlipId,$eType)
    {
        $SQL = DB::table("slip_product");
        $SQL->where("iSlipId",$iSlipId);
        $SQL->where("eType",$eType);
        $result = $SQL->get();
        return $result;
    }
    public static function get_by_iSlipId($iSlipId)
    {
        $SQL = DB::table("slip_product")->select("slip_product.*", DB::raw("(SELECT category.vName FROM category WHERE category.iCategoryId = slip_product.iCategoryId) as vCategoryName"),DB::raw("(SELECT categoryproduct.vName FROM categoryproduct WHERE categoryproduct.iCategoryProductId = slip_product.iProductId) as vProductName"),DB::raw("(SELECT grade.vQualityName FROM grade WHERE grade.iGradeId = slip_product.iGradeId) as vGradeName"),DB::raw("(SELECT productstage.vName FROM productstage WHERE productstage.iProductStageId = slip_product.iStageId) as vStageName"),DB::raw("(SELECT toothbrand.vName FROM toothbrand WHERE toothbrand.iToothBrandId = slip_product.iToothBrandId) as vBrandName"),DB::raw("(SELECT toothshades.vToothShade FROM toothshades WHERE toothshades.iToothShadesId = slip_product.iToothShadesId) as vToothShade"),DB::raw("(SELECT gumbrand.vName FROM gumbrand WHERE gumbrand.iGumBrandId = slip_product.iGumBrandId) as vGumBrand"),DB::raw("(SELECT gumshades.vGumShade FROM gumshades WHERE gumshades.iGumShadesId = slip_product.iGumShadesId) as vGumShade"))->orderBy("iSlipProductId", "DESC");
        $SQL->where("iSlipId", $iSlipId);
        $SQL->orderBy("iSlipProductId", "DESC");
        $result = $SQL->get();
        return $result;
    }

    public static function get_by_iSlipId_eType($iSlipId,$eType)
    {
        $SQL = DB::table("slip_product")->select("slip_product.*", DB::raw("(SELECT category.vName FROM category WHERE category.iCategoryId = slip_product.iCategoryId) as vCategoryName"),DB::raw("(SELECT categoryproduct.vName FROM categoryproduct WHERE categoryproduct.iCategoryProductId = slip_product.iProductId) as vProductName"),DB::raw("(SELECT grade.vQualityName FROM grade WHERE grade.iGradeId = slip_product.iGradeId) as vGradeName"),DB::raw("(SELECT productstage.vName FROM productstage WHERE productstage.iProductStageId = slip_product.iStageId) as vStageName"),DB::raw("(SELECT toothbrand.vName FROM toothbrand WHERE toothbrand.iToothBrandId = slip_product.iToothBrandId) as vBrandName"),DB::raw("(SELECT toothshades.vToothShade FROM toothshades WHERE toothshades.iToothShadesId = slip_product.iToothShadesId) as vToothShade"),DB::raw("(SELECT toothshades.vCode FROM toothshades WHERE toothshades.iToothShadesId = slip_product.iToothShadesId) as vCode"),DB::raw("(SELECT gumbrand.vName FROM gumbrand WHERE gumbrand.iGumBrandId = slip_product.iGumBrandId) as vGumBrand"),DB::raw("(SELECT gumshades.vGumShade FROM gumshades WHERE gumshades.iGumShadesId = slip_product.iGumShadesId) as vGumShade"))->orderBy("iSlipProductId", "DESC");
        
        $SQL->where("iSlipId", $iSlipId);
        $SQL->where("eType", $eType);
        $result = $SQL->get();
        return $result->first();
    }
    public static function add($data)
    {
        $add = DB::table('slip_product')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('slip_product');
        $id->where('iSlipProductId',$where['iSlipProductId'])->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('slip')->where('iCaseId',$where['iCaseId'])->delete();
    }

    public static function get_by_iStageId($iStageId)
    {
        $SQL = DB::table("slip_product");
        $SQL->where("iStageId", $iStageId);
        $result = $SQL->get();
        return $result;
    }
}
