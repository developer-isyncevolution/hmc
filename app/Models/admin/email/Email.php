<?php

namespace App\Models\admin\email;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Email extends Model
{
    use HasFactory;

    protected $table        = 'email';

    protected $primaryKey   = 'iEmailId';

    public $timestamps      = false;

    protected $fillable     = ['iEmailId','vEmailCode', 'vTitle', 'vFromName', 'vFromEmail', 'vSubject', 'tMessage', 'eStatus', 'dtAddedDate', 'dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("email");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vTitle', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }

        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }

    public static function get_by_id($iEmailId)
    {
        $SQL = DB::table("email");
        $SQL->where("iEmailId", $iEmailId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('email')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iEmailId = DB::table('email');
        $iEmailId->where('iEmailId',$where['iEmailId'])->update($data);
        return $iEmailId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('email')->where('iEmailId',$where['iEmailId'])->delete();
    }
}
