<?php

namespace App\Models\admin\billing;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon; 
use Carbon\CarbonPeriod;
use DB;
use App\Libraries\General;

class Billing extends Model
{
    use HasFactory;

    protected $table        = 'billing';

    protected $primaryKey   = 'iBillingId';

    public $timestamps      = false;

   


 
    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        $SQLBR = DB::table('billing');
        $SQLBR->rightJoin('billing_product','billing.iBillingId','billing_product.iBillingId');
        $SQLBR->leftJoin('fees','billing_product.iFeesId','fees.iFeesId');
        // Carbon::now('UTC'); 
        if(isset($criteria['vKeyword']) && $criteria['vKeyword'] != "" && $criteria['vKeyword'] !=null)
        {
            $SQLBR->where('billing.vPatientName', 'like', '%' . $criteria['vKeyword'] . '%')->orWhere('billing.vSlipNumber', 'like', '%' . $criteria['vKeyword'] . '%');
        }
        $CustomerType = General::admin_info()['customerType'];
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']) && $CustomerType =='Lab Admin')
        {
            $SQLBR->where('billing.iLabId',$criteria['iCustomerId']);
        }
        elseif(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']) && $CustomerType =='Office Admin')
        {
            $SQLBR->where('billing.iOfficeId',$criteria['iCustomerId']);
        }
        if(isset($criteria['iChangeId']) && !empty($criteria['iChangeId']) && $CustomerType =='Lab Admin' && $criteria['iChangeId'] !='All')
        {
            $SQLBR->where('billing.iOfficeId',$criteria['iChangeId']);
        }
        elseif(isset($criteria['iChangeId']) && !empty($criteria['iChangeId']) && $CustomerType =='Office Admin' && $criteria['iChangeId'] !='All')
        {
            $SQLBR->where('billing.iLabId',$criteria['iChangeId']);
        }
        if(isset($criteria['iSlipId']) && !empty($criteria['iSlipId']))
        {
            $SQLBR->where('billing.iSlipId',$criteria['iSlipId']);
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'Today')
        {
            $SQLBR->whereDate('billing.dDeliveryDate',Carbon::today());
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'Yesterday')
        {
            $SQLBR->whereDate('billing.dDeliveryDate',Carbon::yesterday());
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'ThisWeek')
        {
            $SQLBR->whereBetween('billing.dDeliveryDate', 
            [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]
            );
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'LastWeek')
        {
            $SQLBR->whereBetween('billing.dDeliveryDate', 
            [Carbon::now()->subWeek()->startOfWeek(), Carbon::now()->subWeek()->endOfWeek()]
            );
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'ThisMonth')
        {
            $SQLBR->whereMonth('billing.dDeliveryDate', Carbon::now()->month);
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'LastMonth')
        {
            $SQLBR->whereBetween('billing.dDeliveryDate', 
            [Carbon::now()->subMonth()->startOfMonth(), Carbon::now()->subMonth()->endOfMonth()]);
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'ThisYear')
        {
            $SQLBR->whereBetween('billing.dDeliveryDate', [
                Carbon::now()->startOfYear(),
                Carbon::now()->endOfYear(),
            ]);
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'LastYear')
        {
            $SQLBR->whereYear('billing.dDeliveryDate', date('Y', strtotime('-1 year')));
        }
        if(isset($criteria['startdate']) && !empty($criteria['startdate']) && isset($criteria['enddate']) && !empty($criteria['enddate']))
        {
            $SQLBR->whereBetween('billing.dDeliveryDate', 
            [$criteria['startdate'], $criteria['enddate']]);
        }
        if(isset($criteria['iBillingIds']) && !empty($criteria['iBillingIds']))
        {
            $SQLBR->whereIn('billing.iBillingId',$criteria['iBillingIds']);
        }
        if(isset($criteria['iBillingId']) && !empty($criteria['iBillingId']))
        {
            $SQLBR->where('billing.iBillingId',$criteria['iBillingId']);
        }

        if(isset($criteria['column']) && ($criteria['column'] || $criteria['order'] != ""))
        {
            $SQLBR->orderBy('billing.'.$criteria['column'],$criteria['order']);
        }   
        // if($paging == true)
        // {
        //     $SQLBR->limit($limit);
        //     $SQLBR->skip($start);
        // }   
        
        // $SQLBR->sum('iMainTotal');
        $result = $SQLBR->get(['billing.*','billing_product.*','fees.vFee']);
        foreach ($result as $key => $value) 
        {
            $SQLB = DB::table("billing_addons");
            $SQLB->where('iBillingProductId',$value->iBillingProductId);
            $result[$key]->billing_addons = $SQLB->get();
        } 
        // dd($result);
        return $result;
    }
    public static function get_all_data_edit($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        // DB::enableQueryLog();        
        // dd($criteria);
        $SQLBR = DB::table('billing');
        if(isset($criteria['vKeyword']) && $criteria['vKeyword'] != "" && $criteria['vKeyword'] !=null)
        {
            $SQLBR->where('vPatientName', 'like', '%' . $criteria['vKeyword'] . '%')->orWhere('vSlipNumber', 'like', '%' . $criteria['vKeyword'] . '%');
        }
        $CustomerType = General::admin_info()['customerType'];
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']) && $CustomerType =='Lab Admin')
        {
            $SQLBR->where('iLabId',$criteria['iCustomerId']);
        }
        elseif(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']) && $CustomerType =='Office Admin')
        {
            $SQLBR->where('iOfficeId',$criteria['iCustomerId']);
        }
        if(isset($criteria['iChangeId']) && !empty($criteria['iChangeId']) && $CustomerType =='Lab Admin')
        {
            $SQLBR->where('iOfficeId',$criteria['iChangeId']);
        }
        elseif(isset($criteria['iChangeId']) && !empty($criteria['iChangeId']) && $CustomerType =='Office Admin')
        {
            $SQLBR->where('iLabId',$criteria['iChangeId']);
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'Today')
        {
            $SQLBR->whereDate('dtAddedDate',Carbon::today());
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'Yesterday')
        {
            $SQLBR->whereDate('dtAddedDate',Carbon::yesterday());
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'ThisWeek')
        {
            $SQLBR->whereBetween('dtAddedDate', 
            [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]
            );
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'LastWeek')
        {
            $SQLBR->whereBetween('dtAddedDate', 
            [Carbon::now()->subWeek()->startOfWeek(), Carbon::now()->subWeek()->endOfWeek()]
            );
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'ThisMonth')
        {
            $SQLBR->whereMonth('dtAddedDate', Carbon::now()->month);
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'LastMonth')
        {
            $SQLBR->whereBetween('dtAddedDate', 
            [Carbon::now()->subMonth()->startOfMonth(), Carbon::now()->subMonth()->endOfMonth()]);
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'ThisYear')
        {
            $SQLBR->whereBetween('dtAddedDate', [
                Carbon::now()->startOfYear(),
                Carbon::now()->endOfYear(),
            ]);
        }
        if(isset($criteria['duration']) && $criteria['duration'] == 'LastYear')
        {
            $SQLBR->whereYear('dtAddedDate', date('Y', strtotime('-1 year')));
        }
        if(isset($criteria['startdate']) && !empty($criteria['startdate']) && isset($criteria['enddate']) && !empty($criteria['enddate']))
        {
            $SQLBR->whereBetween('dtAddedDate', 
            [$criteria['startdate'], $criteria['enddate']]);
        }
        if(isset($criteria['iBillingIds']) && !empty($criteria['iBillingIds']))
        {
            $SQLBR->whereIn('iBillingId',$criteria['iBillingIds']);
        }
        if(isset($criteria['iBillingId']) && !empty($criteria['iBillingId']))
        {
            $SQLBR->where('iBillingId',$criteria['iBillingId']);
        }

        if(isset($criteria['column']) && ($criteria['column'] || $criteria['order'] != ""))
        {
            $SQLBR->orderBy($criteria['column'],$criteria['order']);
        }   
        if($paging == true)
        {
            $SQLBR->limit($limit);
            $SQLBR->skip($start);
        }   
        
        // $SQLBR->sum('iMainTotal');
        $SQLBR->select('*');
        $result = $SQLBR->get();

        
        foreach ($result as $key => $value) 
        {
            $SQLP = DB::table("billing_product");
            $SQLP->where('iBillingId',$value->iBillingId);
            $result[$key]->billing_product = $SQLP->get();
            $result[$key]->MainTotalAll = array_sum(array_column($SQLBR->get()->toArray(),'iMainTotal'));
            foreach ($result[$key]->billing_product as $key_b => $value_b) 
            {
                $SQLB = DB::table("billing_addons");
                $SQLB->where('iBillingProductId',$value_b->iBillingProductId);
                $result[$key]->billing_product[$key_b]->billing_addons = $SQLB->get();
            } 
        } 

       
        return $result;
    }
    public static function get_billing_data($where)
    {
        $SQL = DB::table("billing");
        $SQL->where($where);
        $result = $SQL->get()->first();
        return $result;
    }

    public static function get_last_record()
    {
        return DB::table('billing')->latest('dtUpdatedDate')->first();
    }
    public static function get_all_grade()
    {
        $SQL = DB::table("billing");

        $result = $SQL->get();
        return $result;
    }
    
    public static function get_grade_by_ids($iBillingId)
    {
        $SQL = DB::table("billing");
        $SQL->whereIn("iBillingId", $iBillingId);
        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iBillingId)
    {
        $SQL = DB::table("billing");
        $SQL->where("iBillingId", $iBillingId);
        $result = $SQL->get();
        return $result->first(); 
    }
    public static function get_billing_product($where)
    {
        $SQL = DB::table("billing_product");
        $SQL->where($where);
        $result = $SQL->get();
        return $result->first(); 
    }
    public static function add($data)
    {
        $add = DB::table('billing')->insertGetId($data);
        return $add;
    }

    public function update($where = array(),$data = array()){
        $iBillingId = DB::table('billing');
        $iBillingId->where('iBillingId',$where)->update($data);
        return $iBillingId;
    }
    public function update_stage($where = array(),$data = array()){
        $iBillingId = DB::table('billing_product');
        $iBillingId->where($where)->update($data);
        return $iBillingId;
    }
    public function update_billing($where = array(),$data = array()){
        $iBillingId = DB::table('billing_addons');
        $iBillingId->where($where)->update($data);
        return $iBillingId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('billing')->where('iBillingId',$where['iBillingId'])->delete();
    }
    
  
    public static function delete_billing($where_array= array())
    {
        DB::table('billing')->where($where_array)->delete();
    }
}
