<?php

namespace App\Models\admin\billing;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon; 
use Carbon\CarbonPeriod;
use DB;
use App\Libraries\General;

class BillingProduct extends Model
{
    use HasFactory;

    protected $table        = 'billing_product';

    protected $primaryKey   = 'iBillingProductId';

    public $timestamps      = false;

   

    public static function add_billing_product($data)
    {
        $add = DB::table('billing_product')->insertGetId($data);
        return $add;
    }

    public static function update_billing_product($where = array(),$data = array())
    {
        $add = DB::table('billing_product');
        $add->where($where)->update($data);
        return $add;
    }

    public static function get_billing_product_data($iBillingProductId)
    {
        $SQL = DB::table('billing_product');
        $SQL->where('iBillingProductId',$iBillingProductId);
        return $SQL->get();
    }

    public static function delete_billing_product($where_array= array())
    {
        DB::table('billing_product')->where($where_array)->delete();
    }
}
