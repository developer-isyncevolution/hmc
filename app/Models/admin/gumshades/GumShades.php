<?php

namespace App\Models\admin\gumshades;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class GumShades extends Model
{
    use HasFactory;

    public $table           = 'gumshades';

    protected $primaryKey   = 'iGumShadesId';

    public $timestamps      = false;

    protected $fillable     = ['iGumShadesId', 'iGumBrandId', 'vCode', 'vGumShade', 'iSequence',  'eStatus', 'dtAddedDate', 'dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){

        $SQL = DB::table('gumshades');
        $SQL->join('gumbrand', 'gumbrand.iGumBrandId', '=', 'gumshades.iGumBrandId');

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('gumshades.vCode', 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->orWhere('gumshades.vGumShade', 'like', '%' . $criteria['vKeyword'] . '%');
        }
        
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy('gumshades.'.$criteria['column'],$criteria['order']);
        }   
        if(!empty($criteria["iGumBrandId"]))
        {
            $SQL->where("gumbrand.iGumBrandId", $criteria["iGumBrandId"]);
        }
         // For Perticular customer show start
         if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
         {
             $SQL->where('gumshades.iCustomerId', $criteria["iCustomerId"]);
         }
         // For Perticular customer show end
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy('gumshades.iSequence',$criteria['order']);
        }
        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get(['gumshades.*', 'gumbrand.vName as vName']);
        return $result;
        // $SQL = DB::table("gumshades")->select("gumshades.*", DB::raw("(SELECT gumbrand.vName FROM gumbrand WHERE gumbrand.iGumBrandId = gumshades.iGumBrandId) as vName"))->orderBy("iGumShadesId", "DESC"); 
        // if($criteria['vKeyword'] != "")
        // {
        //     $SQL->orWhere('vCode', 'like', '%' . $criteria['vKeyword'] . '%');
        // }

        // if(!empty($criteria["eStatus"]))
        // {
        //     $SQL->where("eStatus", $criteria["eStatus"]);
        // }
        // if(!empty($criteria["iGumBrandId"]))
        // {
        //     $SQL->where("iGumBrandId", $criteria["iGumBrandId"]);
        // }

        // if($criteria['column'] || $criteria['order'] != "")
        // {
        //     $SQL->orderBy('iSequence',$criteria['order']);
        // }    

        // if($paging == true)
        // {
        //     $SQL->limit($limit);
        //     $SQL->skip($start);
        // }

        // $result = $SQL->get();
        // return $result;
    }

    public static function get_all_brand()
    {
        $SQL = DB::table("gumbrand");
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_by_id($ids)
    {
        $SQL = DB::table("gumshades");
        $SQL->whereIn('iGumShadesId',$ids);
        $SQL->groupBy('iGumBrandId');
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_active_gumshades(){
        
        $SQL = DB::table("gumshades");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }
    public static function get_all_gumshades(){
        
        $SQL = DB::table("gumshades");
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }
    public static function get_by_id($iGumShadesId)
    {
        $SQL = DB::table("gumshades");
        $SQL->where("iGumShadesId", $iGumShadesId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_product_by_gumbrand($criteria = array())
    {
        // dd($criteria);
        $SQL = DB::table("gumshades");

        if(isset($criteria['iGumBrandId']) && !empty($criteria['iGumBrandId']))
        {
            $SQL->where("iGumBrandId", $criteria['iGumBrandId']);
        }

        if (isset($criteria['iLabId']) && !empty($criteria['iLabId'])) {
            
            $SQL->where("iCustomerId", $criteria['iLabId']);
            // updated by krupesh thakkar
        }
        // Added by kasimabbas 12-08-2022 start
        // Added by kasimabbas 12-08-2022 end
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        // dd($result);

        if(isset($criteria['iProductId']) && !empty($criteria['iProductId']))
        {
            foreach ($result as $key => $value) {
                $SQL = DB::table("product_gumshades");
                $SQL->where('iGumShadesId',$value->iGumShadesId);
                $SQL->where('iCategoryProductId',$criteria['iProductId']);
                $product_gumshade = $SQL->get()->count();

                if ($product_gumshade == 0) {
                    unset($result[$key]);
                }
            }
        }

        // dd($result);
        return $result;
    }
    public static function add($data)
    {
        $add = DB::table('gumshades')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('gumshades');
        $id->where('iGumShadesId',$where['iGumShadesId'])->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('gumshades')->where('iGumShadesId',$where['iGumShadesId'])->delete();
    }
}
