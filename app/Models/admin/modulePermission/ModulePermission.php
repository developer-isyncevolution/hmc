<?php

namespace App\Models\admin\modulePermission;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ModulePermission extends Model
{
    use HasFactory;

    protected $table = 'module_permission';

    protected $primaryKey = 'iPermissionId';

    public $timestamps = false;

    protected $fillable = [
        'iPermissionId',
        'iDepartmentId',
        'iModuleId',
        'eCreate', 
        'eRead',
        'eEdit',
        'eDelete',
        'eAddSlip',
        'eEditStage',
        'eReadSlip',
        'eVirualSlip',
        'ePaperSlip',
        'eNewStage',
        'eReadyToSend',
        'eViewDriverHistory',
        'ePickUp',
        'eDropOff',
        'eDirection',
        'eAddCallLog',
        'eHistoryCall',
        'eLastModified',
    ];


    public static function get_all_data(){
        $SQL = DB::table("module_permission")->get();

        $result = $SQL;
        
        return $result;
    }

    public static function get_by_id($iPermissionId)
    {
        $SQL = DB::table("module_permission");
        $SQL->where("iPermissionId", $iPermissionId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('module_permission')->insertGetId($data);
        return $add;
    }

    public static function update_data(array $where = [], array $data = []){
        $iPermissionId = DB::table('module_permission');
        $iPermissionId->where('iPermissionId',$where['iPermissionId'])->update($data);
        return $iPermissionId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('module_permission')->where('iDepartmentId',$where['iDepartmentId'])->delete();
    }

    public static function get_by_department_id($iDepartmentId)
    {
        $SQL = DB::table("module_permission");
        $SQL->where("iDepartmentId", $iDepartmentId);
        $result = $SQL->get()->toArray();
        return $result;
    }
}
