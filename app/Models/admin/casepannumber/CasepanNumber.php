<?php

namespace App\Models\admin\casepannumber;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class CasepanNumber extends Model
{
    use HasFactory;

    protected $table        = 'casepannumber';

    protected $primaryKey   = 'iCasepanNumberId';

    public $timestamps      = false;

    protected $fillable     = ['iCasepanNumberId','iCasepanId', 'vNumber', 'eStatus','dtAddedDate','dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {

        $SQL = DB::table('casepannumber');
        $SQL->join('casepan', 'casepan.iCasepanId', '=', 'casepannumber.iCasepanId');

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('casepannumber.vNumber', 'like', '%' . $criteria['vKeyword'] . '%');
        }
        
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy('casepannumber.'.$criteria['column'],$criteria['order']);
        }   
        if(!empty($criteria["iCasepanId"]))
        {
            $SQL->where('casepannumber.iCasepanId', $criteria["iCasepanId"]);
        }

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get(['casepannumber.*', 'casepan.vColor as vColor']);
        return $result;
    //     $SQL = DB::table("casepannumber")->select("casepannumber.*", DB::raw("(SELECT casepan.vColor FROM casepan WHERE casepan.iCasepanId = casepannumber.iCasepanId) as vColor"))->orderBy("iCasepanNumberId", "DESC"); 
    //     // $SQL = DB::table("casepannumber");

    //     if($criteria['vKeyword'] != "")
    //     {
    //         $SQL->where('vTypeName', 'like', '%' . $criteria['vKeyword'] . '%');
    //     }

    //     if(!empty($criteria["iCasepanId"]))
    //     {
    //         $SQL->where("iCasepanId", $criteria["iCasepanId"]);
    //     }
    //     if(!empty($criteria["eStatus"]))
    //     {
    //         $SQL->where("eStatus", $criteria["eStatus"]);
    //     }

    //     if($criteria['column'] || $criteria['order'] != "")
    //     {
    //         $SQL->orderBy($criteria['column'],$criteria['order']);
    //     }   

    //     if($paging == true)
    //     {
    //         $SQL->limit($limit);
    //         $SQL->skip($start);
    //     }

    //     $result = $SQL->get();
    //     return $result;
    }
    public static function get_all_type()
    {
        $SQL = DB::table("casepannumber");

        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iCasepanNumberId)
    {
        $SQL = DB::table("casepannumber");
        $SQL->where("iCasepanNumberId", $iCasepanNumberId);
        $result = $SQL->get();
        return $result->first(); if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
    }
    public static function get_by_casepan_id($iCasepanId)
    {
        $SQL = DB::table("casepannumber");
        $SQL->where("iCasepanId", $iCasepanId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_by_casepan_use($iCasepanId)
    {
        $SQL = DB::table("casepannumber");
        $SQL->where("iCasepanId", $iCasepanId);
        $SQL->where("eUse", "No");
        $result = $SQL->get();
        return $result->first();
    }
    public static function add($data)
    {
        $add = DB::table('casepannumber')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iCasepanNumberId = DB::table('casepannumber');
        $iCasepanNumberId->where('iCasepanNumberId',$where['iCasepanNumberId'])->update($data);
        return $iCasepanNumberId;
    }
    public static function update_data($where = array(),$data = array()){
        $iCasepanNumberId = DB::table('casepannumber');
        $iCasepanNumberId->where($where)->update($data);
        return $iCasepanNumberId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('casepannumber')->where('iCasepanNumberId',$where['iCasepanNumberId'])->delete();
    }
}
