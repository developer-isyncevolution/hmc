<?php

namespace App\Models\admin\office_admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class OfficeAdmin extends Model
{
    use HasFactory;

    public $table           = 'office_admin';

    protected $primaryKey   = 'iOfficeAdminId';

    public $timestamps = false;

    protected $fillable     = ['iOfficeAdminId', 'iCustomerId','vName', 'vEmail', 'vPassword', 'vvMobile', 'vCellulor', 'eStatus', 'dtAddedDate', 'dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
     
        $SQL = DB::table("office_admin");
        $SQL->join('customer','office_admin.iCustomerId','customer.iCustomerId');
        $SQL->join('staff','office_admin.iOfficeAdminId','staff.iLoginAdminId');
       if(isset($criteria['vKeyword']) && $criteria['vKeyword'] != "")
        {
            $SQL->where(DB::raw('CONCAT_WS(" ", office_admin.vFirstName, office_admin.vLastName)'), 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->orWhere('office_admin.vEmail', 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->groupBy('office_admin.iOfficeAdminId');
        }
        if(isset($criteria['iCustomerId']) && $criteria['iCustomerId'] != "")
        {
            $SQL->where('office_admin.iCustomerId',$criteria['iCustomerId']);
        }
        
        if(isset($criteria["eStatus"]) && !empty($criteria["eStatus"]))
        {
            $SQL->where("office_admin.eStatus", $criteria["eStatus"]);
        }
        if(isset($criteria["column"]) && ($criteria['column'] || $criteria['order'] != ""))
        {
            $SQL->orderBy('office_admin.iOfficeAdminId',$criteria['order']);
        }    
        $SQL->where("staff.iDepartmentId", 3);
        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get(['office_admin.*','customer.vOfficeName','customer.vOfficeCode','staff.iStaffId']);
        return $result;
      
    }

  
    public static function get_by_id($iOfficeAdminId)
    {
        $SQL = DB::table("office_admin");
        $SQL->where("iOfficeAdminId", $iOfficeAdminId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_by_iCustomerId($iCustomerId)
    {
        $SQL = DB::table("office_admin");
        $SQL->where("iCustomerId", $iCustomerId);
        $result = $SQL->get('vImage');
        return $result->first();
    }
  

    public static function add($data)
    {
        $add = DB::table('office_admin')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('office_admin');
        $id->where('iOfficeAdminId',$where['iOfficeAdminId'])->update($data);
        return $id;
    }
    public static function update_office_admin($where = array(),$data = array()){
        $id = DB::table('office_admin');
        $id->where($where)->update($data);
        return $id;
    }
    public static function get_office_admin($where = array()){
        $id = DB::table('office_admin')->where($where)->get();
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('office_admin')->where('iOfficeAdminId',$where['iOfficeAdminId'])->delete();
    }

    public static function user_exit_in_staff($iOfficeAdminId,$iCustomerId)
    {
        return DB::table('staff')->where('iLoginAdminId',$iOfficeAdminId)
                              ->where('iCustomerId',$iCustomerId)
                              ->get()->count();  
    }

    public static function update_staff_data($data,$where_array)
    {
        DB::table('staff')->where($where_array)->update($data);  
    }
}
