<?php

namespace App\Models\admin\office_prices;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class OfficePrices extends Model
{
    use HasFactory;

    public $table           = 'office_prices';

    protected $primaryKey   = 'iOfficePriceId';

    public $timestamps = false;
    
    public static function checkExistOffice($iLoginAdminId,$iTypeId)
    {
        $SQL = DB::table('office_prices');
        $SQL->where('iLoginAdminId',$iLoginAdminId);
        $SQL->where('iTypeId',$iTypeId);
        return $SQL->get();
    }

  
}
