<?php

namespace App\Models\admin\customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class Customer extends Model
{
    use HasFactory;

    public $table           = 'customer';

    public $table_customertype = 'customertype';

    protected $primaryKey   = 'iCustomerId';

    public $timestamps      = false;

    protected $fillable     = ['iCustomerId', 'iCustomerTypeId','vName', 'vTitle', 'vWebsiteName', 'tDescription', 'vOfficeName', 'vOfficeCode', 'vAddress', 'vCity', 'iStateId', 'vZipCode', 'vEmail', 'vPassword','vImage', 'vMobile', 'vCellulor', 'eStatus', 'dtAddedDate', 'dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table('customer');
        $SQL->join('customertype', 'customertype.iCustomerTypeId', '=', 'customer.iCustomerTypeId');

        if(isset($criteria['vKeyword']) && $criteria['vKeyword'] != "")
        {
            $SQL->where(DB::raw('CONCAT_WS(" ", customer.vFirstName, customer.vFirstName)'), 'like', '%' . $criteria['vKeyword'] . '%')
                ->orWhere('customer.vTitle', 'like', '%' . $criteria['vKeyword'] . '%')
                ->orWhere('customer.vOfficeName', 'like', '%' . $criteria['vKeyword'] . '%');
        }
        if(isset($criteria['eStatus']) && !empty($criteria['eStatus']))
        {
            $SQL->where('customer.eStatus',$criteria['eStatus']);
        }
        if(isset($criteria['iCustomerTypeId']) && !empty($criteria['iCustomerTypeId']))
        {
            $SQL->where('customer.iCustomerTypeId',$criteria['iCustomerTypeId'] );
        }

        
        if((isset($criteria['column']) && ($criteria['column']) || (isset($criteria['order']) && $criteria['order'] != "")))
        {
            $SQL->orderBy('customer.'.$criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }
        $result = $SQL->get(['customer.*', 'customertype.vTypeName as vTypeName']);
        return $result;
      
    }

    public static function get_office_id()
    {
        $SQL = DB::table("customer");
        $SQL->orderBy("iCustomerId", "DESC");
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_all_active_customer(){
        
        $SQL = DB::table("customer");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }
    public static function get_by_id($iCustomerId)
    {
        $SQL = DB::table("customer");
        $SQL->where("iCustomerId", $iCustomerId);
        
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_by_office_id($iCustomerId)
    {
        $SQL = DB::table("customer");
        $SQL->where("iCustomerId", $iCustomerId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_all_lab()
    {
        $SQL = DB::table("customer");
        $SQL->where("iCustomerTypeId",1);
        $SQL->where("eStatus",'Active');
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_customer($criteria = array())
    {
        $SQL = DB::table("customer");
        
        if(isset($criteria['iCustomerId']) && $criteria['iCustomerId'] != "")
        {
            $SQL->where('iCustomerId',$criteria['iCustomerId']);
        }

        $result = $SQL->get();
        return $result;
    }
    public static function get_customer($criteria = array())
    {
        $SQL = DB::table("customer");
        $SQL->where($criteria);
        $result = $SQL->get()->first();
        return $result;
    }

    public static function add($data)
    {
        $add = DB::table('customer')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('customer');
        $id->where('iCustomerId',$where['iCustomerId'])->update($data);
        return $id;
    }
    public static function update_data($where, $data){
        $id = DB::table('customer');
        $id->where($where)->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('customer')->where('iCustomerId',$where['iCustomerId'])->delete();
    }

   
}
