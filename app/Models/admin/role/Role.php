<?php

namespace App\Models\admin\role;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class Role extends Model
{
    use HasFactory;


    protected $table = 'role';

    protected $primaryKey = 'iRoleId';

    public $timestamps = false;

    protected $fillable = [
        'iRoleId',
        'vRole',
        'eStatus',
        'dtAddedDate',
        'dtUpdatedDate'
    ];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("role");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vRole', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   

        // if($criteria["eStatus"] != "")
        // {
        //     $SQL->where("eStatus", $criteria["eStatus"]);
        // }

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }

    public static function get_by_id($iRoleId)
    {
        $SQL = DB::table("role");
        $SQL->where("iRoleId", $iRoleId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('role')->insertGetId($data);
        return $add;
    }

    public static function update_data(array $where = [], array $data = []){
        $iRoleId = DB::table('role');
        $iRoleId->where('iRoleId',$where['iRoleId'])->update($data);
        return $iRoleId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('role')->where('iRoleId',$where['iRoleId'])->delete();
    }

     public static function get_role_data(){
        $SQL = DB::table("role");

        $result = $SQL->get();
        return $result;
    }
}
