<?php

namespace App\Models\admin\productstage;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ProductStage extends Model
{
    use HasFactory;

    protected $table        = 'productstage';

    protected $primaryKey   = 'iProductStageId';

    public $timestamps      = false;

    protected $fillable     = ['iProductStageId', 'vName', 'vCode',  'vPrice', 'iCategoryId', 'iCategoryProductId', 'iPickUpDay', 'iProcessDay', 'iDeliverDay', 'eType', 'iSequence', 'eStatus','dtAddedDate','dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        $SQL = DB::table("productstage");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where(function ($query) use ($criteria) {
                $query->where('vName', 'like', '%' . $criteria['vKeyword'] . '%');
                $query->orWhere('vCode', 'like', '%' . $criteria['vKeyword'] . '%');
            }); 
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        if(!empty($criteria["eType"]))
        {
            if($criteria["eType"] == "Upper"){
                $SQL->where(function ($query) use ($criteria) {
                    $query->where('eType',"Upper");
                    $query->orWhere('eType',"Both");
                }); 
            }
            if($criteria["eType"] == "Lower"){
                $SQL->where(function ($query) use ($criteria) {
                    $query->where('eType',"Lower");
                    $query->orWhere('eType',"Both");
                }); 
            }
        }
        if(!empty($criteria["iCategoryId"]))
        {
            $SQL->where("iCategoryId", $criteria["iCategoryId"]);
        }
        if(!empty($criteria["iCategoryProductId"]))
        {
            $SQL->where("iCategoryProductId", intval($criteria["iCategoryProductId"]));
        }
        if(!empty($criteria["iProductStageId"]))
        {
            $SQL->where("iProductStageId", $criteria["iProductStageId"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }else{
            $SQL->orderBy('iSequence','ASC');
        }
        
        // if($paging == true)
        // {
        //     $SQL->limit($limit);
        //     $SQL->skip($start);
        // }
        
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_category($criteria = array())
    {
        $SQL = DB::table("category");
        if(!empty($criteria["eType"]))
        {
            $SQL->where("eType", $criteria["eType"]);
        }
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_by_stageIds($ids)
    {
        $SQL = DB::table("productstage");
        $SQL->whereIn("iProductStageId", $ids);
        $SQL->orderBy("iSequence", "DESC");
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_all_categoryproduct($criteria = array())
    {
        $SQL = DB::table("categoryproduct");
        if(!empty($criteria["eType"]))
        {
            $SQL->where("eType", $criteria["eType"]);
        }
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_grade()
    {
        $SQL = DB::table("grade");
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_for_slip($criteria = array())
    {
        $SQL = DB::table("productstage");
        $SQL->where(function ($query) use ($criteria) {
            $query->where('eType',$criteria['eType']);
            $query->orWhere('eType',"Both");
        }); 
        if(!empty($criteria["iCategoryId"]))
        {
            $SQL->where("iCategoryId", $criteria["iCategoryId"]);
        }
        if(!empty($criteria["iCategoryProductId"]))
        {
            $SQL->where("iCategoryProductId", $criteria["iCategoryProductId"]);
        }
        if(!empty($criteria["iProductStageId"]))
        {
            $SQL->where("iProductStageId", $criteria["iProductStageId"]);
        }
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_for_new_stage($criteria = array())
    {
        $SQL = DB::table("productstage");
        
        $SQL->where(function ($query) use ($criteria) {
            $query->where('eType',$criteria['eType']);
            $query->orWhere('eType',"Both");
        }); 
       
        $SQL->where("iCategoryId", $criteria["iCategoryId"]);
        $SQL->where("iCategoryProductId", $criteria["iCategoryProductId"]);
        $SQL->where("iSequence",'>=', $criteria["iSequence"]);
        $SQL->whereNotIn("iProductStageId",$criteria["iProductStageId"]);
        
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_for_old_stage($criteria = array())
    {
        $SQL = DB::table("productstage");    
        $SQL->where(function ($query) use ($criteria) {
            $query->where('eType',$criteria['eType']);
            $query->orWhere('eType',"Both");
        }); 
        $SQL->where("iCategoryId", $criteria["iCategoryId"]);
        $SQL->where("iCategoryProductId", $criteria["iCategoryProductId"]);
        $SQL->where("iProductStageId",$criteria["iProductStageId"]);
        
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get(['vName','dtAddedDate']);
        return $result->first();
    }
    public static function get_data_for_finish_stage($criteria = array())
    {
        $SQL = DB::table("productstage");
        
        $SQL->where(function ($query) use ($criteria) {
            $query->where('eType',$criteria['eType']);
            $query->orWhere('eType',"Both");
        }); 
        $SQL->where("iCategoryId", $criteria["iCategoryId"]);
        $SQL->where("iCategoryProductId", $criteria["iCategoryProductId"]);
        $SQL->whereIn("iProductStageId",$criteria["iProductStageId"]);
        $SQL->where("vName",'Finish');
        
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }

    public static function get_used_stages_for_case($iCategoryId,$iCategoryProductId,$iProductStageId)
    {
        $SQL = DB::table("productstage");
        $SQL->where(function ($query) use ($criteria) {
            $query->where('eType',$criteria['eType']);
            $query->orWhere('eType',"Both");
        }); 
       
        $SQL->where("iCategoryId", $criteria["iCategoryId"]);
        $SQL->where("iCategoryProductId", $criteria["iCategoryProductId"]);
        $SQL->where("iProductStageId",'!=',$criteria["iProductStageId"]);
        
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }


    public static function get_by_id($iProductStageId)
    {
        $SQL = DB::table("productstage");
        $SQL->where("iProductStageId", $iProductStageId);
        $result = $SQL->get();
        return $result->first(); 
        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
    }
    public static function get_by_once_productcategoryid($iCategoryProductId)
    {
        $SQL = DB::table("productstage");
        $SQL->where("iCategoryProductId", $iCategoryProductId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_by_type($criteria = array())
    {
        $SQL = DB::table("productstage");
        if(!empty($criteria["iProductStageId"]))
        {
            $SQL->where("iProductStageId", $criteria["iProductStageId"]);
        }
        $SQL->where(function ($query) use ($criteria) {
            $query->where('eType',$criteria['eType']);
            $query->orWhere('eType',"Both");
        }); 
        $result = $SQL->get();
        return $result->first();
    }
    public static function add($data)
    {
        $add = DB::table('productstage')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = [])
    {
        $iProductStageId = DB::table('productstage');
        $iProductStageId->where('iProductStageId',$where['iProductStageId'])->update($data);
        return $iProductStageId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('productstage')->where('iProductStageId',$where['iProductStageId'])->delete();
    }

     // Office Data update start
     public static function AddOfficePrice($data)
     {
         $add = DB::table('office_prices')->insertGetId($data);
         return $add;
     }
 
     public static function UpdatedOfficePrice($office_data,$office_where)
     {
         $sql = DB::table('office_prices');
         $sql->where($office_where);
         $update = $sql->update($office_data);
         return $update;
     }
 
     public static function checkExistOffice($iLoginAdminId,$iTypeId)
     {
         $SQL = DB::table('office_prices');
         $SQL->where('iLoginAdminId',$iLoginAdminId);
         $SQL->where('iTypeId',$iTypeId);
         return $SQL->get();
     }
     // Office Data update end
}
