<?php

namespace App\Models\admin\category;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    use HasFactory;

    protected $table        = 'category';

    protected $primaryKey   = 'iCategoryId';

    public $timestamps      = false;

    protected $fillable     = ['iCategoryId','vName', 'iCasepanId','tPickTime', 'tDeliveryTime','eType', 'iSequence', 'eStatus','dtAddedDate','dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        
        $SQL = DB::table('category');
        $SQL->join('casepan', 'casepan.iCasepanId', '=', 'category.iCasepanId');

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('category.vName', 'like', '%' . $criteria['vKeyword'] . '%');
        }
        if(!empty($criteria["eType"]))
        {
            $SQL->where(function ($query) use ($criteria) {
                $query->where("eType", $criteria["eType"]);
                $query->orWhere('eType', 'Both');
            });
           
        }
        
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy('category.iSequence',$criteria['order']);
        }   
        if(!empty($criteria["iCasepanId"]))
        {
            $SQL->where('category.iCasepanId', $criteria["iCasepanId"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('category.iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end
        $SQL->orderBy('category.iSequence','ASC');

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }
        $result = $SQL->get(['category.*', 'casepan.vColor as vColor']);
        return $result;
        // $SQL = DB::table("category")->select("category.*", DB::raw("(SELECT casepan.vColor FROM casepan WHERE casepan.iCasepanId = category.iCasepanId) as vColor"))->orderBy("iCategoryId", "DESC");
        // // $SQL = DB::table("category");

        // if($criteria['vKeyword'] != "")
        // {
        //     $SQL->where('vName', 'like', '%' . $criteria['vKeyword'] . '%');
        // }

        // if(!empty($criteria["eStatus"]))
        // {
        //     $SQL->where("eStatus", $criteria["eStatus"]);
        // }
        // if(!empty($criteria["eType"]))
        // {
        //     $SQL->where("eType", $criteria["eType"]);
        //     $SQL->orWhere('eType', 'Both');
        // }

        // if($criteria['column'] || $criteria['order'] != "")
        // {
        //     $SQL->orderBy('iSequence',$criteria['order']);
        // }   

        // if($paging == true)
        // {
        //     $SQL->limit($limit);
        //     $SQL->skip($start);
        // }

        // $result = $SQL->get();
        // return $result;
    }
    public static function get_all_casepan()
    {
        $SQL = DB::table("casepan");
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iCategoryId)
    {
        $SQL = DB::table("category");
        $SQL->where("iCategoryId", $iCategoryId);
        $result = $SQL->get();
        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        return $result->first(); 
    }
    public static function get_category($where)
    {
        $SQL = DB::table("category");
        $SQL->where($where);
        return $SQL->get()->count();
        
    }

    public static function get_category_by_type($type){
        $SQL = DB::table("category");
        $SQL->where(function ($query) use ($type) {
            $query->where("eType", $type);
            $query->orWhere('eType', 'Both');
        });
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }

    public static function get_category_by_type_customer($type,$id){ // Krupesh Thakkar New Change
        $SQL = DB::table("category");
        $SQL->where(function ($query) use ($type) {
            $query->where("eType", $type);
            $query->orWhere('eType', 'Both');
        });
        $SQL->where("iCustomerId",$id);
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }

    public static function add($data)
    {
        $add = DB::table('category')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iCategoryId = DB::table('category');
        $iCategoryId->where('iCategoryId',$where['iCategoryId'])->update($data);
        return $iCategoryId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('category')->where('iCategoryId',$where['iCategoryId'])->delete();
    }
}
