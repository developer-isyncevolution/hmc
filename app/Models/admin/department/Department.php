<?php

namespace App\Models\admin\department;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Department extends Model
{
    use HasFactory;

    protected $table        = 'department';

    protected $primaryKey   = 'iDepartmentId';

    public $timestamps      = false;

    protected $fillable     = ['iDepartmentId','vName', 'eStatus','dtAddedDate','dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        $SQL = DB::table("department");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vName', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }
    public static function get_all_type()
    {
        $SQL = DB::table("department");

        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iDepartmentId)
    {
        $SQL = DB::table("department");
        $SQL->where("iDepartmentId", $iDepartmentId);
        $result = $SQL->get();
        return $result->first(); 
        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
    }
    public static function get_by_cust_id($iCustomerId)
    {
        $SQL = DB::table("department");
        $SQL->where("iCustomerId", $iCustomerId);
        $result = $SQL->get();
        return $result; 
      
    }
    public static function add($data)
    {
        $add = DB::table('department')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iDepartmentId = DB::table('department');
        $iDepartmentId->where('iDepartmentId',$where['iDepartmentId'])->update($data);
        return $iDepartmentId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('department')->where('iDepartmentId',$where['iDepartmentId'])->delete();
    }
}
