<?php

namespace App\Models\admin\billing_addons;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon; 
use Carbon\CarbonPeriod;
use DB;
use App\Libraries\General;

class BillingAddons extends Model
{
    use HasFactory;

    protected $table        = 'billing_addons';

    protected $primaryKey   = 'iBillingAddOnsId';

    public $timestamps      = false;

   


    public static function get_billing_addons_data($iBillingProductId)
    {
        $SQL = DB::table('billing_addons');
        $SQL->where('iBillingProductId',$iBillingProductId);
        return $SQL->get();
    }

    public static function add_billing_addons($data)
    {
        $add = DB::table('billing_addons')->insertGetId($data);
        return $add;
    }

    public static function delete_billing_addons($where_array= array())
    {
        DB::table('billing_addons')->where($where_array)->delete();
    }

}
