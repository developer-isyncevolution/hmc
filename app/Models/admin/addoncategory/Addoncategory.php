<?php

namespace App\Models\admin\addoncategory;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Addoncategory extends Model
{
    use HasFactory;
    protected $table        = 'addon_category';

    protected $primaryKey   = 'iAddCategoryId';

    public $timestamps      = false;

    protected $fillable     = ['iAddCategoryId','vCategoryName', 'vSequenc', 'dtAddedDate','dtUpdatedDate','eStatus'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        $SQL = DB::table("addon_category");
        if($criteria['vKeyword'] != "")
        {
            $SQL->where(function ($query) use ($criteria) {
                $query->where('vCategoryName', 'like', '%' . $criteria['vKeyword'] . '%');
                $query->orwhere('vSequenc','LIKE','%'.$criteria['vKeyword'].'%');
                $query->orWhere('eStatus', 'like', '%' . $criteria['vKeyword'] . '%');
            });
        }
        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end
        if($criteria['column'] != '' || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   
        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }
        if(!empty($criteria["eType"]))
        {
            $SQL->where(function ($query) use ($criteria) {
                $query->where("eType", $criteria["eType"]);
                $query->orWhere('eType', 'Both');
                });
        }
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_schedule()
    {
        $SQL = DB::table("addon_category");

        $result = $SQL->get();
        return $result;
    }
    public static function get_all_category($criteria = array())
    {
        $SQL = DB::table("addon_category");
        if(!empty($criteria["eType"]))
        {
            $SQL->where("eType", $criteria["eType"]);
        }
        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iAddCategoryId)
    {
        $SQL = DB::table("addon_category");
        $SQL->where("iAddCategoryId", $iAddCategoryId);
        $result = $SQL->get();
        return $result->first(); if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
    }
    public static function get_category_addons_by_type($type,$iCustomerId=''){
        $SQL = DB::table("addon_category");
        $SQL->where(function ($query) use ($type) {
            $query->where("eType", $type);
            $query->orWhere('eType', 'Both');
        });
        if(isset($iCustomerId) && !empty($iCustomerId))
        {
            $SQL->where("iCustomerId", $iCustomerId);
            $SQL->orderBy('vSequenc','ASC');
            $result = $SQL->get();
            return $result;
        }
    }
    public static function add($data)
    {
        $add = DB::table('addon_category')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iAddCategoryId = DB::table('addon_category');
        $iAddCategoryId->where('iAddCategoryId',$where['iAddCategoryId'])->update($data);
        return $iAddCategoryId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('addon_category')->where('iAddCategoryId',$where['iAddCategoryId'])->delete();
    }
}
