<?php

namespace App\Models\admin\state;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class State extends Model
{
    use HasFactory;

    public $table           = 'state';

    protected $primaryKey   = 'iStateId';

    public $timestamps      = false;

    protected $fillable     = ['iStateId','vName', 'email', 'password','vImage', 'vMobile', 'eStatus', 'dtAddedDate', 'dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("state");

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }

        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit, $start);
        }

        $result = $SQL->get();
        return $result;
    }

    public static function get_by_iCountryId($iCountryId)
    {
        $SQL = DB::table("state");
        $SQL->where("iCountryId", $iCountryId);
        $result = $SQL->get();
        return $result;
    }
    public static function get_by_id($id)
    {
        $SQL = DB::table("state");
        $SQL->where("iStateId", $id);
        $result = $SQL->get();
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('state')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iStateId = DB::table('state');
        $iStateId->where('iStateId',$where['iStateId'])->update($data);
        return $iStateId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('state')->where('iStateId',$where['iStateId'])->delete();
    }
}
