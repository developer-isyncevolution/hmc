<?php

namespace App\Models\admin\calendar;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class calendar extends Model
{
    use HasFactory;
    public static function add($data)
    {
        $add = DB::table('calendar_event')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iEventId = DB::table('calendar_event');
        $iEventId->where('iEventId',$where['iEventId'])->update($data);
        return $iEventId;
    }
    public static function get_all_calendar_event()
    {
        $SQL = DB::table("calendar_event");

        $result = $SQL->get();
        return $result;
    }
    public static function get_by_id($iEventId)
    {
        // $SQL = DB::table("calendar_event");
        $SQL = DB::table("calendar_event")->select("calendar_event.*", DB::raw("(SELECT customer.vOfficeName FROM customer WHERE customer.iCustomerId = calendar_event.iCustomerId) as vCustomerName"))->orderBy("iEventId", "DESC"); 
        $SQL->where("iEventId", $iEventId);
        $result = $SQL->get();
        return $result->first();
        
    }
    public static function get_by_id_office($iCustomerId)
    {
        $SQL = DB::table("customer");
        $SQL->where("iCustomerId", $iCustomerId);
        $result = $SQL->get();
        return $result;
        
    }
    public static function get_all_office_data()
    {
        $SQL = DB::table("office");
        $result = $SQL->get();
        return $result;
    }
   

}

