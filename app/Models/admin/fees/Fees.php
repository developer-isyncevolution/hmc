<?php

namespace App\Models\admin\fees;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Fees extends Model
{
    use HasFactory;

    protected $table        = 'fees';

    protected $primaryKey   = 'iFeesId';

    public $timestamps      = false;

    protected $fillable     = ['iFeesId', 'iProductStageId', 'vRushFee', 'vFee', 'eStatus','dtAddedDate','dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        $SQL = DB::table("fees");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vName', 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->orWhere('vCode', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        if(!empty($criteria["eType"]))
        {
            $SQL->where("eType", $criteria["eType"]);
        }
        if(!empty($criteria["iCategoryId"]))
        {
            $SQL->where("iCategoryId", $criteria["iCategoryId"]);
        }
        if(!empty($criteria["iCategoryProductId"]))
        {
            $SQL->where("iCategoryProductId", $criteria["iCategoryProductId"]);
        }
        if(!empty($criteria["iProductStageId"]))
        {
            $SQL->where("iProductStageId", $criteria["iProductStageId"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }
    public static function get_all_category($criteria = array())
    {
        $SQL = DB::table("category");
        if(!empty($criteria["eType"]))
        {
            $SQL->where("eType", $criteria["eType"]);
        }
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_categoryproduct($criteria = array())
    {
        $SQL = DB::table("categoryproduct");
        if(!empty($criteria["eType"]))
        {
            $SQL->where("eType", $criteria["eType"]);
        }
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_grade()
    {
        $SQL = DB::table("grade");
        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iFeesId)
    {
        $SQL = DB::table("fees");
        $SQL->where("iFeesId", $iFeesId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_by_iProductStageId($iProductStageId)
    {
        $SQL = DB::table("fees");
        $SQL->where("iProductStageId", $iProductStageId);
        $SQL->orderBy("iWorkingTime","ASC");
        $result = $SQL->get();
        return $result; 
    }
    public static function add($data)
    {
        $add = DB::table('fees')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = [])
    {
        $iFeesId = DB::table('fees');
        $iFeesId->where('iFeesId',$where['iFeesId'])->update($data);
        return $iFeesId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('fees')->where('iFeesId',$where['iFeesId'])->delete();
    }
}
