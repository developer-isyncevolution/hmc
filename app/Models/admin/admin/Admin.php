<?php

namespace App\Models\admin\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\Admin as Authenticatable;
use DB;

class Admin extends Model
{
    use HasFactory;

    public $table           = 'admin';

    protected $primaryKey   = 'iAdminId';

    public $timestamps      = false;

    protected $fillable     = ['iAdminId','vName', 'email', 'password','vImage', 'vMobile', 'eStatus', 'dtAddedDate', 'dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("admin");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vName', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }

        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit, $start);
        }

        $result = $SQL->get();
        return $result;
    }

    public static function get_by_id($iAdminId)
    {
        $SQL = DB::table("admin");
        $SQL->where("iAdminId", $iAdminId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('admin')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iAdminId = DB::table('admin');
        $iAdminId->where('iAdminId',$where['iAdminId'])->update($data);
        return $iAdminId;
    }

    public static function update_super_admin($where = array(),$data = array()){
        $id = DB::table('admin');
        $id->where($where)->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('admin')->where('iAdminId',$where['iAdminId'])->delete();
    }

    public static function user_exit_in_staff($iLabAdminId,$iDepartmentId)
    {
        return DB::table('staff')->where('iLoginAdminId',$iLabAdminId)
                              ->where('iDepartmentId',$iDepartmentId)
                              ->get()->count();  
    }

    public static function update_staff_data($data,$where_array)
    {
        DB::table('staff')->where($where_array)->update($data);  
    }
}
