<?php

namespace App\Models\admin\grade;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Grade extends Model
{
    use HasFactory;

    protected $table        = 'grade';

    protected $primaryKey   = 'iGradeId';

    public $timestamps      = false;

    protected $fillable     = ['iGradeId','vQualityName', 'iSequence', 'eStatus','dtAddedDate','dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        $SQL = DB::table("grade");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vQualityName', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end

        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy('iSequence',$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }
    public static function get_all_grade()
    {
        $SQL = DB::table("grade");

        $result = $SQL->get();
        return $result;
    }
    
    public static function get_grade_by_ids($iGradeId)
    {
        $SQL = DB::table("grade");
        $SQL->whereIn("iGradeId", $iGradeId);
        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iGradeId)
    {
        $SQL = DB::table("grade");
        $SQL->where("iGradeId", $iGradeId);
        $result = $SQL->get();
        return $result->first(); 
    }
    public static function add($data)
    {
        $add = DB::table('grade')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iGradeId = DB::table('grade');
        $iGradeId->where('iGradeId',$where['iGradeId'])->update($data);
        return $iGradeId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('grade')->where('iGradeId',$where['iGradeId'])->delete();
    }
}
