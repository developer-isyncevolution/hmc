<?php

namespace App\Models\admin\banner;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Banner extends Model
{
    use HasFactory;

    protected $table        = 'banner';

    protected $primaryKey   = 'iBannerId';

    public $timestamps      = false;

    protected $fillable     = ['iBannerId','vBannerTitle', 'tDescription', 'vImage', 'eStatus','iCustomerId','dtStartDate','dtEndDate','dtAddedDate','dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("banner");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vTitle', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }

        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   
        if(isset($criteria['iCustomerId']) && $criteria['iCustomerId'] != "")
        {
            $SQL->where('iCustomerId',$criteria['iCustomerId']);
        }

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }

    public static function get_by_id($iBannerId)
    {
        $SQL = DB::table("banner");
        $SQL->where("iBannerId", $iBannerId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function get_banner_data($customer_array)
    {
        $SQL = DB::table("banner");
        $SQL->whereIn("iCustomerId", $customer_array);
        $SQL->where('dtStartDate','<=', date('Y-m-d'));
        $SQL->where('dtEndDate','>=',date('Y-m-d'));
        $SQL->where('eStatus','Active');
        $result = $SQL->get();
        return $result;
    }

    public static function add($data)
    {
        $add = DB::table('banner')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iBannerId = DB::table('banner');
        $iBannerId->where('iBannerId',$where['iBannerId'])->update($data);
        return $iBannerId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('banner')->where('iBannerId',$where['iBannerId'])->delete();
    }
}
