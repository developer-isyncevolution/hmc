<?php

namespace App\Models\admin\slip;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class Slip extends Model
{
    use HasFactory;

    public $table           = 'slip';

    protected $primaryKey   = 'iSlipId';

    public $timestamps      = false;

    protected $fillable     = ['iCaseId', 'iOfficeId', 'iDoctorId', 'vPatientName',  'iCasePanId', 'vCasePanNumber', 'vCaseNumber','iCreatedById','vCreatedByName','eCreatedByType','eStatus', 'vLocation', 'dPickUpDate', 'dDeliveryDate' ,'tDeliveryTime', 'tStagesNotes','dtAddedDate','dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("slip");
        if($criteria['vKeyword'] != "")
        {
            $SQL->where(function ($query) use ($criteria) {
                $query->where('vPatientName', 'like', '%' . $criteria['vKeyword'] . '%');
                $query->orWhere('vCasePanNumber', 'like', '%' . $criteria['vKeyword'] . '%');
            }); 
            
        }
        if(!empty($criteria["iOfficeId"]))
        {
            $SQL->where("iOfficeId", $criteria["iOfficeId"]);
        }
        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        if(!empty($criteria["iDoctorId"]))
        {
            $SQL->where("iDoctorId", $criteria["iDoctorId"]);
        }
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }
        $result = $SQL->get();
        return $result;
    }

    public static function get_office_id()
    {
        $SQL = DB::table("slip");
        $SQL->orderBy("iSlipId", "DESC");
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_all_active_case(){
        
        $SQL = DB::table("slip");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }
    public static function get_by_id($iSlipId)
    {
        $SQL = DB::table("slip");

        $SQL = DB::table("slip")->select("slip.*", 
        DB::raw("(SELECT casepan.vColor FROM casepan WHERE casepan.iCasepanId = slip.iCasepanId LIMIT 1) as vColor"),
        DB::raw("(SELECT casepannumber.vNumber FROM casepannumber WHERE casepannumber.iCasepanId = slip.iCasepanId AND casepannumber.eUse = 'No' LIMIT 1) as vNumber"))->orderBy("iSlipId", "DESC"); 
        $SQL->where("iSlipId", $iSlipId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_by_iCaseId($iCaseId)
    {
        $SQL = DB::table("slip");
        $SQL->where("iCaseId", $iCaseId);
        $SQL->orderBy("iSlipId", "DESC");
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_all_by_case($iCaseId)
    {
        $SQL = DB::table("slip");
        $SQL->where("iCaseId", $iCaseId);
        $SQL->orderBy("iSlipId", "DESC");
        $result = $SQL->get();
        return $result;
    }
    public static function add($data)
    {
        $add = DB::table('slip')->insertGetId($data);
        return $add;
    }

    public  function update(array $where = [], array $data = []){
        $id = DB::table('slip');
        $id->where('iSlipId',$where['iSlipId'])->update($data);
        return $id;
    }
    public static function update_new($where = array(), $data = array()){
        $id = DB::table('slip');
        $id->where($where)->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('slip')->where('iCaseId',$where['iCaseId'])->delete();
    }
    public static function get_next_stage($CuurentiSlipId,$CuurentiCaseId)
    {
        $next_data = DB::table('slip')
                ->where('iCaseId', $CuurentiCaseId)
                ->where('iSlipId', '>', $CuurentiSlipId)
                ->get(['iSlipId','iCaseId'])->first();
        return $next_data;
    }
    public static function get_previous_stage($CuurentiSlipId,$CuurentiCaseId)
    {
        $previous_data = DB::table('slip')
                ->where('iCaseId', $CuurentiCaseId)
                ->where('iSlipId', '<', $CuurentiSlipId)
                ->orderBy('iSlipId','DESC')
                ->get(['iSlipId','iCaseId'])->first();
        return $previous_data;
    }
    public static function get_middel_stage($data,$CuurentiCaseId )
    {
        $next_data = array();
        $productUpperStage = array();
        $productLowerStage = array();
        $productStageUpperArray = array();
        $productStageLowerArray = array();
        $next_data = DB::table('slip')
                ->join('slip_product', 'slip.iSlipId', '=', 'slip_product.iSlipId')
                ->join('productstage', 'slip_product.iStageId', '=', 'productstage.iProductStageId')
                ->where('slip.iCaseId', $CuurentiCaseId)
                ->get(['slip.iSlipId','slip.iCaseId','productstage.vCode AS vProductStageName','slip_product.eType']);

        foreach($next_data as $key=> $next_data_val)
        {
            if($next_data_val->eType =='Upper')
            {
                array_push($productStageUpperArray,$next_data_val->vProductStageName);
            }
            else
            {
                array_push($productStageLowerArray,$next_data_val->vProductStageName);
            }    
        }
        foreach($next_data as $key=> $next_data_val)
        {
            if(in_array($next_data_val->vProductStageName,$productStageUpperArray))
            {
                $SkipUpperFirststage = array_count_values($productStageUpperArray);
                $SkipUpperFirststageCount = $SkipUpperFirststage[$next_data_val->vProductStageName];
            }
            else
            {
                $SkipUpperFirststage = 0;
                $SkipUpperFirststageCount = 0;
            }    
            if(in_array($next_data_val->vProductStageName,$productStageLowerArray))
            {
                $SkipLowerFirststage = array_count_values($productStageLowerArray);
                $SkipLowerFirststageCount = $SkipLowerFirststage[$next_data_val->vProductStageName];
            }
            else
            {
                $SkipLowerFirststage = 0;
                $SkipLowerFirststageCount = 0;
            }
            
            if($next_data_val->eType =='Upper')
            {
                if($SkipUpperFirststageCount>1)
                {
                    array_push($productUpperStage,$next_data_val->vProductStageName);
                    $counts = array_count_values($productUpperStage);
                    $stageCount = $counts[$next_data_val->vProductStageName];
                    $next_data[$key]->vProductStageName= $next_data_val->vProductStageName.$stageCount;
                }
                else
                {
                    $next_data[$key]->vProductStageName= $next_data_val->vProductStageName;
                }    
            }
            else
            {
                if($SkipLowerFirststageCount>1)
                {
                    array_push($productLowerStage,$next_data_val->vProductStageName);
                    $counts = array_count_values($productLowerStage);
                    $stageCount = $counts[$next_data_val->vProductStageName];
                    $next_data[$key]->vProductStageName= $next_data_val->vProductStageName.$stageCount;
                }
                else
                {
                    $next_data[$key]->vProductStageName= $next_data_val->vProductStageName;
                }    
            }
           
        }
           
        return $next_data;
    }
    public static function get_attatchment_data($iCaseId)
    {
        $SQL = DB::table("slip");
        $SQL->where("iCaseId", $iCaseId);
        $SQL->join('customer', 'slip.iOfficeId', '=', 'customer.iCustomerId');
        $result = $SQL->get(['customer.vOfficeName','slip.vPatientName','slip.vCaseNumber','slip.vCasePanNumber','slip.vLocation']);
        return $result;
    }
}
