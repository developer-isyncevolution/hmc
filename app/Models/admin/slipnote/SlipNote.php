<?php

namespace App\Models\admin\slipnote;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class SlipNote extends Model
{
    use HasFactory;

    protected $table        = 'slip_notes';

    protected $primaryKey   = 'iSlipAddonsId';

    public $timestamps      = false;

    protected $fillable     = ['iSlipId', 'iStageId', 'iCaseId','vStageName','tStagesNotes','vCaseNumber','dDeliveryDate','vCreatedByName','dtAddedDate','tAddedTime'];



    public static function add($data)
    {
        $add = DB::table('slip_notes')->insertGetId($data);
        return $add;
    
    }
    public static function get_by_iSlipId($criteria = array())
    {
        $result = array();
        $SQL = DB::table("slip_notes")->select("slip_notes.*", DB::raw("(SELECT slip.dDeliveryDate FROM slip WHERE slip_notes.iSlipId = slip.iSlipId LIMIT 1) as dDeliveryDate"));
        $SQL->where("iSlipId", $criteria['iSlipId']);
        $SQL->groupBy('iSlipId');
        $result = $SQL->get();

        foreach ($result as $key => $value) 
        {
            $SQL = DB::table("slip_notes");
            $SQL->where('iSlipId',$value->iSlipId);
            
            $result[$key]->stage_note = $SQL->get();

        }
        // dd($result);
        return $result;
    }
    public static function get_by_iCaseId($criteria = array())
    {
        $result = array();
        $SQL = DB::table("slip_notes")->select("slip_notes.*", DB::raw("(SELECT slip.dDeliveryDate FROM slip WHERE slip_notes.iSlipId = slip.iSlipId LIMIT 1) as dDeliveryDate"));
        $SQL->where("iCaseId", $criteria['iCaseId']);
        $SQL->groupBy('iSlipId');
        $result = $SQL->get();

        foreach ($result as $key => $value) 
        {
            $SQL = DB::table("slip_notes");
            $SQL->where('iCaseId',$value->iCaseId);
            $SQL->where('iSlipId',$value->iSlipId);
            
            $result[$key]->stage_note = $SQL->get();

        }
        // dd($result);
        return $result;
    }
    public static function delete_by_id(array $where = [])
    {
        $SQL = DB::table('slip_notes');
        $SQL->where('iSlipId',$where['iSlipId']);
        $SQL->where('eType',$where['eType']);
        $SQL->delete();
    }
    public static function update_new($where = array(), $data = array()){
        $id = DB::table('slip_notes');
        $id->where($where)->update($data);
        return $id;
    }

  
    
}
