<?php

namespace App\Models\admin\casepan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Casepan extends Model
{
    use HasFactory;

    protected $table        = 'casepan';

    protected $primaryKey   = 'iCasepanId';

    public $timestamps      = false;

    protected $fillable     = ['iCasepanId','vName', 'vColor', 'iQuantity', 'iSequence', 'eStatus','dtAddedDate','dtUpdatedDate'];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = '')
    {
        $SQL = DB::table("casepan");

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vName', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end
        if($criteria['column'] !='' || $criteria['order'] != "")
        {
            $SQL->orderBy('iSequence',$criteria['order']);
        }
        
        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }
        
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_casepan()
    {
        $SQL = DB::table("casepan");

        $result = $SQL->get();
        return $result;
    }
  
    public static function get_by_id($iCasepanId)
    {
        $SQL = DB::table("casepan");
        $SQL->where("iCasepanId", $iCasepanId);
        $result = $SQL->get();
        return $result->first(); if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
    }
    public static function add($data)
    {
        $add = DB::table('casepan')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $iCasepanId = DB::table('casepan');
        $iCasepanId->where('iCasepanId',$where['iCasepanId'])->update($data);
        return $iCasepanId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('casepan')->where('iCasepanId',$where['iCasepanId'])->delete();
    }
}
