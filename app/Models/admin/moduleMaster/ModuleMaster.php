<?php

namespace App\Models\admin\moduleMaster;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ModuleMaster extends Model
{
    use HasFactory;

    protected $table = 'module_master';

    protected $primaryKey = 'iModuleId';

    public $timestamps = false;

    protected $fillable = [
        'iModuleId',
        'vTitle',
        'vModuleName',
        'eStatus', 
        'dtAddedDate',
        'dtUpdatedDate'
    ];


    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("module_master");
        
        if(!empty($criteria['vKeyword']))
        {
            $SQL->where('vTitle', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }

        if(!empty($criteria['column'] || $criteria['order']))
        {
            $SQL->orderBy($criteria['column'],$criteria['order']);
        }   
        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }
        $result = $SQL->get();
        
        return $result;
    }

    public static function get_by_id($iModuleId)
    {
        $SQL = DB::table("module_master");
        $SQL->where("iModuleId", $iModuleId);
        $result = $SQL->get();
        // dd($result);
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('module_master')->insertGetId($data);
        return $add;
    }

    public static function update_data(array $where = [], array $data = []){
        $iModuleId = DB::table('module_master');
        $iModuleId->where('iModuleId',$where['iModuleId'])->update($data);
        return $iModuleId;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('module_master')->where('iModuleId',$where['iModuleId'])->delete();
    }

    public static function get_all_data_count()
    {
        $count = DB::table("module_master")->count();
        
        return $count;
    }

    public static function get_all()
    {
        $count = DB::table("module_master")->where('eStatus', 'Active')->get();
        
        return $count;
    }
}
