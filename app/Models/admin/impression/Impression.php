<?php

namespace App\Models\admin\impression;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class Impression extends Model
{
    use HasFactory;

    public $table           = 'impression';

    protected $primaryKey   = 'iImpressionId';

    public $timestamps      = false;

    protected $fillable     = ['iImpressionId', 'vName', 'vCode', 'iSequence',  'eStatus', 'dtAddedDate', 'dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        // dd($criteria);
        $SQL = DB::table("impression");
        if($criteria['vKeyword'] != "")
        {
            $SQL->where('vName', 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->orWhere('vCode', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("eStatus", $criteria["eStatus"]);
        }
        
        // For Perticular customer show start
        if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
        {
            $SQL->where('iCustomerId', $criteria["iCustomerId"]);
        }
        // For Perticular customer show end

        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy('iSequence',$criteria['order']);
        }    

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get();
        return $result;
    }

    public static function get_office_id()
    {
        $SQL = DB::table("impression");
        $SQL->orderBy("iImpressionId", "DESC");
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_all_active_impression(){
        
        $SQL = DB::table("impression");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }

    public static function get_all_impressions(){
        
        $SQL = DB::table("impression");
        $SQL->where("eStatus", 'Active');
        $SQL->orderBy('iSequence',"ASC");
        $result = $SQL->get();
        return $result;
    }

    public static function get_all_impressions_by_customer($id){ //Krupesh New Change
        
        $SQL = DB::table("impression");
        $SQL->where("eStatus", 'Active');
        $SQL->where("iCustomerId", $id);
        $SQL->orderBy('iSequence',"ASC");
        $result = $SQL->get();
        return $result;
    }

    public static function get_by_id($iImpressionId)
    {
        $SQL = DB::table("impression");
        $SQL->where("iImpressionId", $iImpressionId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function add($data)
    {
        $add = DB::table('impression')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('impression');
        $id->where('iImpressionId',$where['iImpressionId'])->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('impression')->where('iImpressionId',$where['iImpressionId'])->delete();
    }
}
