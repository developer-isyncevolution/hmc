<?php

namespace App\Models\admin\toothshades;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class ToothShades extends Model
{
    use HasFactory;

    public $table           = 'toothshades';

    protected $primaryKey   = 'iToothShadesId';

    public $timestamps      = false;

    protected $fillable     = ['iToothShadesId','iCustomerId', 'iToothBrandId', 'vCode', 'iSequence',  'eStatus', 'dtAddedDate', 'dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){
        $SQL = DB::table("toothshades"); 

        $SQL->join('toothbrand', 'toothbrand.iToothBrandId', '=', 'toothshades.iToothBrandId');
        if($criteria['vKeyword'] != "")
        {
            $SQL->where('toothshades.vToothShade', 'like', '%' . $criteria['vKeyword'] . '%');
            $SQL->orWhere('toothshades.vCode', 'like', '%' . $criteria['vKeyword'] . '%')->orWhere('toothbrand.vName', 'like', '%' . $criteria['vKeyword'] . '%');
        }

        if(!empty($criteria["eStatus"]))
        {
            $SQL->where("toothshades.eStatus", $criteria["eStatus"]);
        }
        if(!empty($criteria["iToothBrandId"]))
        {
            $SQL->where("toothshades.iToothBrandId", $criteria["iToothBrandId"]);
        }
         // For Perticular customer show start
        
         if(isset($criteria['iCustomerId']) && !empty($criteria['iCustomerId']))
         {
             $SQL->where('toothshades.iCustomerId', $criteria["iCustomerId"]);
         }
         // For Perticular customer show end

        if($criteria['column'] != "" || $criteria['order'] != "")
        {
            $SQL->orderBy('toothshades.iSequence',$criteria['order']);
        }    

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get(['toothshades.*', 'toothbrand.vName as vName']);
        return $result;
    }

    public static function get_all_brand()
    {
        $SQL = DB::table("toothbrand");
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_active_toothshades(){
        
        $SQL = DB::table("toothshades");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }
    public static function get_all_toothshades(){
        
        $SQL = DB::table("toothshades");
        $SQL->where("eStatus", 'Active');
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }
    public static function get_by_id($iToothShadesId)
    {
        $SQL = DB::table("toothshades");
        $SQL->where("iToothShadesId", $iToothShadesId);
        $result = $SQL->get();
        return $result->first();
    }
    public static function get_product_by_toothbrand($iToothBrandId)
    {
        $SQL = DB::table("toothshades");
        $SQL->where("iToothBrandId", $iToothBrandId);
        $SQL->orderBy('iSequence','ASC');
        $result = $SQL->get();
        return $result;
    }

    public static function add($data)
    {
        $add = DB::table('toothshades')->insertGetId($data);
        return $add;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('toothshades');
        $id->where('iToothShadesId',$where['iToothShadesId'])->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('toothshades')->where('iToothShadesId',$where['iToothShadesId'])->delete();
    }
}
