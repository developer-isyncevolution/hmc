<?php

namespace App\Models\admin\doctor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class Doctor extends Model
{
    use HasFactory;

    public $table           = 'doctor';

    protected $primaryKey   = 'iDoctorId';

    public $timestamps      = false;

    protected $fillable     = ['iDoctorId', 'iOfficeId','vFirstName', 'vLastName', 'tDescription', 'vEmail', 'vLicence', 'vMiddleInitial', 'vPassword','vImage', 'vMobile', 'vCellulor', 'eStatus', 'dtAddedDate', 'dtUpdatedDate'];

    public static function get_all_data($criteria = array(), $start = '', $limit = '', $paging = false){

        $SQL = DB::table('doctor');
        $SQL->join('customer','doctor.iCustomerId','customer.iCustomerId');
        $SQL->join('staff','doctor.iDoctorId','staff.iLoginAdminId');

        if($criteria['vKeyword'] != "")
        {
            $SQL->where('doctor.vFirstName', 'like', '%' . $criteria['vKeyword'] . '%')->orWhere('doctor.vLastName', 'like', '%' . $criteria['vKeyword'] . '%')->orWhere('doctor.vLicence', 'like', '%' . $criteria['vKeyword'] . '%')->orWhere('customer.vOfficeName', 'like', '%' . $criteria['vKeyword'] . '%');
        }
       
        if(!empty($criteria["iCustomerId"]))
        {
            $SQL->where("doctor.iCustomerId", $criteria["iCustomerId"]);
        }
        
        if($criteria['column'] || $criteria['order'] != "")
        {
            $SQL->orderBy('doctor.'.$criteria['column'],$criteria['order']);
        }   
        $SQL->where("staff.iDepartmentId", 4);
        $SQL->distinct("customer.iCustomerId");

        if($paging == true)
        {
            $SQL->limit($limit);
            $SQL->skip($start);
        }

        $result = $SQL->get(['doctor.*', 'customer.vOfficeName as vOfficeName']);
        return $result;
        // $SQL = DB::table("doctor")->select("doctor.*", DB::raw("(SELECT office.vOfficeName FROM office WHERE office.iOfficeId = doctor.iOfficeId) as vOfficeName"))->orderBy("iDoctorId", "DESC"); 
        
        // if($criteria['vKeyword'] != "")
        // {
        //     $SQL->where('vFirstName', 'like', '%' . $criteria['vKeyword'] . '%');
        //     $SQL->orWhere('vLastName', 'like', '%' . $criteria['vKeyword'] . '%');
        //     $SQL->orWhere('vLicence', 'like', '%' . $criteria['vKeyword'] . '%');
        // }

        // if(!empty($criteria["eStatus"]))
        // {
        //     $SQL->where("eStatus", $criteria["eStatus"]);
        // }
        // if($criteria['column'] || $criteria['order'] != "")
        // {
        //     $SQL->orderBy($criteria['column'],$criteria['order']);
        // }   
        // if(!empty($criteria["iOfficeId"]))
        // {
        //     $SQL->where("iOfficeId", $criteria["iOfficeId"]);
        // }
        // if(!empty($criteria["iCustomerId"]))
        // {
        //     $SQL->where("iCustomerId", $criteria["iCustomerId"]);
        // }

        // if($paging == true)
        // {
        //     $SQL->limit($limit);
        //     $SQL->skip($start);
        // }

        // $result = $SQL->get();
        // dd($result);        
        // return $result;
    }
    public static function get_all_office(){
        
        $SQL = DB::table("office");
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_doctor(){
        
        $SQL = DB::table("doctor");
        $result = $SQL->get();
        return $result;
    }
    public static function get_all_customer(){
        
        $SQL = DB::table("customer");
        $result = $SQL->get();
        return $result;
    }

    public static function get_by_id($iDoctorId)
    {
        $SQL = DB::table("doctor");
        $SQL->where("iDoctorId", $iDoctorId);
        $result = $SQL->get();
        return $result->first();
    }

    public static function get_doc_by_office($iOfficeId)
    {
        $SQL = DB::table("doctor");
        $SQL->where("iOfficeId", $iOfficeId);
        $result = $SQL->get();
        return $result;
    }

    public static function get_doc_by_customerid($id)
    {
        $SQL = DB::table("doctor");
        $SQL->where("iCustomerId", $id);
        $result = $SQL->get();
        return $result;
    }

    public static function add($data)
    {
        $add = DB::table('doctor')->insertGetId($data);
        return $add;
    }
    public static function get_all_active_doctor(){
        
        $SQL = DB::table("doctor");
        $SQL->where("eStatus", 'Active');
        $data = $SQL->get();
        $result = $data->count();
        return $result;
    }

    public function update(array $where = [], array $data = []){
        $id = DB::table('doctor');
        $id->where('iDoctorId',$where['iDoctorId'])->update($data);
        return $id;
    }

    public static function delete_by_id(array $where = [])
    {
        DB::table('doctor')->where('iDoctorId',$where['iDoctorId'])->delete();
    }

    public static function user_exit_in_staff($iLoginAdminId,$iCustomerId)
    {
        return DB::table('staff')->where('iLoginAdminId',$iLoginAdminId)
                              ->where('iCustomerId',$iCustomerId)
                              ->get()->count();  
    }

    public static function update_staff_data($data,$where_array)
    {
        DB::table('staff')->where($where_array)->update($data);  
    }
}
