<?php

namespace App\Helper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\admin\setting\Setting;
use App\Models\front\category\Category;
use App\Models\admin\meta\Meta;
use Auth;
use Session;

class GeneralHelper extends Controller
{
	static function replaceContent($vTitle){
        $rs_catname = trim(strtolower(($vTitle)));
        $rs_catname = str_replace("/","",$rs_catname);
        $rs_catname = str_replace("G��","",$rs_catname);
        $rs_catname = str_replace("(","",$rs_catname);
       
        $rs_catname = trim(strtolower(($vTitle)));
        $rs_catname = str_replace("/","",$rs_catname);
        $rs_catname = str_replace("G��","",$rs_catname);
        $rs_catname = str_replace("(","",$rs_catname);
        $rs_catname = str_replace(")","",$rs_catname);
        $rs_catname = str_replace("?","",$rs_catname);
        $rs_catname = str_replace("-","-",$rs_catname);
        $rs_catname = str_replace("#","",$rs_catname);
        $rs_catname = str_replace(",","",$rs_catname);
        $rs_catname = str_replace(";","",$rs_catname);
        $rs_catname = str_replace(":","",$rs_catname);
        $rs_catname = str_replace("'","",$rs_catname);
        $rs_catname = str_replace("\"","",$rs_catname);
        $rs_catname = str_replace("++","-",$rs_catname);
        $rs_catname = str_replace("+","-",$rs_catname);
        $rs_catname = str_replace("+","-",$rs_catname);
        $rs_catname = str_replace("+�","-",$rs_catname);
        //$rs_catname = str_replace("s","_",$rs_catname);

        $rs_catname = str_replace(" ","-",str_replace("&","and",$rs_catname));
        return $rs_catname;
    }

    static function get_config_type(){
        $config_type = Setting::groupBy('eConfigType')->pluck('eConfigType');
        return $config_type;
    }

    static function company_info(){
        $company_info = Setting::where('eConfigType','Company')->get();
        return $company_info;
    }

    static function user_info()
    {
        $user = Auth::user();
        return $user;
    }

    static function category_info()
    {
        $category = Category::get_all_header_data();
        
        return $category;
    }
    // function meta_info()
    // {
    //     $meta_data  = Meta::get_by_controller($controller,$method);
    //     if (!empty($meta_data)) {
    //         $meta_info['META_TITLE']['vValue']        =  $meta_data->vTitle;
    //         $meta_info['META_KEYWORD']['vValue']      =  $meta_data->vKeyword;
    //         $meta_info['META_DESCRIPTION']['vValue']  =  $meta_data->tDescription;
    //         return $meta_info;
    //     }else{
    //         $meta_info['META_TITLE']['vValue']        =  $setting['META_TITLE']['vValue'];
    //         $meta_info['META_KEYWORD']['vValue']      =  $setting['META_KEYWORD']['vValue'];
    //         $meta_info['META_DESCRIPTION']['vValue']  =  $setting['META_DESCRIPTION']['vValue'];
    //          return $meta_info;
    //     }
    // }
}