<?php

namespace App\Imports;

use App\Models\admin\toothshades\ToothShades;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Facades\Excel;
use App\Libraries\General;
use Maatwebsite\Excel\Concerns\WithStartRow;
use DB;


class ImportTothshades implements ToModel,WithStartRow
{
    public $filename;
    public $iToothBrandId;
    public $iCustomerId;
    public function  __construct($filename,$iToothBrandId,$iCustomerId)
    {
        $this->filename = $filename;
        $this->iCustomerId = $iCustomerId;
        $this->iToothBrandId = $iToothBrandId;
    }
    private $rows = 0;

    public function model(array $row)
    {
        ++$this->rows; 
        return new ToothShades([
                'iToothBrandId' => $this->iToothBrandId,
                'iCustomerId' => 1,
                'vCode' => $row[0],
                'iSequence' => $row[1],
                'dtAddedDate'=>date('Y-m-d h:i:s'),
                'dtUpdatedDate'=>date('Y-m-d h:i:s'),
        ]);
    }

    public function getRowCount(): int
    {
        return $this->rows;
    }

    public function startRow(): int
    {
        return 2;
    }
}
