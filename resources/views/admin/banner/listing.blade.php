@extends('layouts.admin.index')
@section('content')
<div class="main-panel">
    <div class="col-lg-11 mx-auto">
        <div class="listing-page">
            <div class="card mb-5 mb-xl-8">
                <div class="page-title text-center" bis_skin_checked="1">
                    <h3>
                       Banner
                    </h3>
                </div>
                {{-- <div class="card-header border-0 py-5">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bolder fs-3 mb-1">Banner</span>
                    </h3> --}}
{{-- 
                    <ul class="d-flex justify-content-between my-2">
                        <li class="d-flex">
                            <div class="d-flex mx-2">
                                <input type="text" class="form-control" id="keyword" name="search" class="search" placeholder="Search">
                            </div>

                            <a href="javascript:;" id="delete_btn" class="btn btn-danger delete_permission mx-2"> Multiple Delete</a>

                            <a href="{{route('admin.banner.create')}}" class="btn btn-primary create_permission mx-2">
                                Create Banner
                            </a>
                        </li>
                    </ul> --}}

                {{-- </div>
            </div> --}}
            <div class="card mb-5 mb-xl-4" bis_skin_checked="1">

                <div class="row" bis_skin_checked="1">
                    <div class="col-lg-12" bis_skin_checked="1">
                        <div class="card" bis_skin_checked="1">
                            <div class="card-mid" bis_skin_checked="1">
                                <ul class="d-flex list-unstyled">
                                    <li>
                                        <a href="{{route('admin.banner.create')}}" class="btn create_permission add-btn me-2">
                                            <i class="fa fa-plus"></i> Add
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                                        <div class="col-lg-6 mx-auto" bis_skin_checked="1">
                        <div class="custome-serce-box" bis_skin_checked="1">
                            <input type="text" class="form-control" id="keyword" name="search" class="search" placeholder="Search">
                            <span class="search-btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-data table-responsive">
                <table class="table">
                    <thead>
                        <tr class="fw-bolder text-muted">
                            <th class="w-25px" data-orderable="false">
                                <div class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" id="selectall" type="checkbox" name="selectall">
                                    <label for="selectall">&nbsp;</label>
                                </div>
                            </th>
                            <th class="min-w-150px">Image</th>
                            <th class="min-w-140px">
                                <a id="vTitle" class="sort" data-column="vTitle" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Title</span>
                                </a>
                            </th>
                            <th class="min-w-120px">
                                <a id="eStatus" class="sort" data-column="eStatus" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Status</span>
                                </a>
                            </th>
                            <th class="min-w-100px">
                                <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="table_record">
                    </tbody>
                </table>
                <div class="text-center loaderimg">
                    <div class="loaderinner">
                        <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom-css')
<style></style>
@endsection

@section('custom-js')

<script>

    $(document).ready(function() {

        setTimeout(function(){
            $.ajax({
                url: "{{route('admin.banner.ajaxListing')}}",
                type: "get",
                data:  {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();   
                }
            });
        }, 1000);
    });

    $("#selectall").click(function()
    {
        if(this.checked){
            $('.checkboxall').each(function(){
                $(".checkboxall").prop('checked', true);
            });
        }else{
            $('.checkboxall').each(function(){
                $(".checkboxall").prop('checked', false);
            });
        }
    });


    $("#keyword").keyup(function(){
        var keyword = $("#keyword").val();

        $("#ajax-loader").show();
        setTimeout(function(){
            $.ajax({
                url: "{{route('admin.banner.ajaxListing')}}",
                type: "get",
                data:  {keyword:keyword,action:'search'}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        }, 1000);
    });

    $(document).on('click','#delete_btn',function(){
        var id = [];

        $("input[name='Banner_ID[]']:checked").each( function () {
            id.push($(this).val());
        });

        var id = id.join(",");

        if (id.length == 0) {
            alert('Please select records.')
        }else{
                $.ajax({
                    url: "{{route('admin.banner.ajaxListing')}}",
                    type: "get",
                    data:  {id:id,action:'multiple_delete'}, 
                    success: function(response) {
                        $("#table_record").html(response);
                       $("#ajax-loader").hide();
                    }
                });
            }
        });
     $(document).on('click','#delete',function(){
            swal({
              title: "Are you sure delete this Banner.?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
                if(willDelete)
                {
                    id = $(this).data("id");

                    $("#ajax-loader").show();

                    setTimeout(function(){
                        $.ajax({
                            url: "{{route('admin.banner.ajaxListing')}}",
                            type: "get",
                            data:  {id:id,action:'delete'}, 
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                            }
                        });
                    }, 1000);
                }
            })
        });
    $(document).on('click','.sort',function(){
        column = $(this).data("column");
        order = $(this).attr('data-order');


        if(order == "ASC"){
            $(this).attr('data-order','DESC');
        } else{
            $(this).attr('data-order','ASC');
        }

        $("#ajax-loader").show();

        setTimeout(function(){
            $.ajax({
                url: "{{route('admin.banner.ajaxListing')}}",
                type: "get",
                data:  {column:column,order,order,action:'sort'}, 
                success: function(response) {
                    console.log(response);
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        }, 1000);
    });

    $(document).on('click','.ajax_page',function()
    {
        pages = $(this).data("pages");

        $("#table_record").html('');
        $("#ajax-loader").show();

        setTimeout(function(){
            $.ajax({
                url: "{{route('admin.banner.ajaxListing')}}",
                type: "POST",
                data:  {pages: pages}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        }, 500);
    });
</script>
@endsection
