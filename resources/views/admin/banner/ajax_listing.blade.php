@if(count($data) > 0)
@foreach($data as $key => $value)
<tr>
	<td>
        <div class="form-check form-check-sm form-check-custom form-check-solid">
            <input id="Banner_ID{{$value->iBannerId}}" type="checkbox" name="Banner_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iBannerId}}">
            <label for="Banner_ID{{$value->iBannerId}}">&nbsp;</label>
        </div>
    </td>
	<td><img alt="{{$value->vImage}}" style="height: 35px;" src="{{asset('uploads/banner/'.$value->vImage)}}"></td>	
	<td>{{$value->vTitle}}</td>
	<td>
		<span class="badge badge-light-success">{{$value->eStatus}}</span>
	</td>
	<td>
		<a href="{{route('admin.banner.edit',$value->iBannerId)}}" class="btn-icon me-4   edit-icon">
			<i class="fad fa-pencil"></i>
		</a>
		
		<a href="javascript:;" id="delete" data-id="{{$value->iBannerId}}" class="btn-icon delete-icon delete_permission me-4">
			<i class="fad fa-trash-alt"></i>
		</a>
	</td>
</tr>
@endforeach
<tr class="text-center">
	<td colspan="7">{!! $paging !!}</td>
</tr>
@else
<tr class="text-center">
	<td colspan="9">No Record Found</td>
</tr>
@endif