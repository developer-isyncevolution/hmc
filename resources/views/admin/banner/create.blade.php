@extends('layouts.admin.index')
@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
<div class="main-panel">
  <div class="card">
    <div class="col-lg-12 mx-auto">
        <div class="card-header">
          <h4 class="card-title" id="row-separator-basic-form">{{isset($banners->iBannerId) ? 'Edit' : 'Add'}} banner</h4>
        </div>
        <form action="{{route('admin.banner.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($banners)) {{$banners->iBannerId}} @endif">
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Banner title</label>
                <input type="text" class="form-control" id="vTitle" name="vTitle" placeholder="Title" value="@if(old('vTitle')!=''){{old('vTitle')}}@elseif(isset($banners->vTitle)){{$banners->vTitle}}@else{{old('vTitle')}}@endif">
                <div class="text-danger" style="display: none;" id="vTitle_error">Enter banner title</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Banner start date</label>
                <input type="text" name="dtStartDate" placeholder="Start date" id="dtStartDate" class="form-control datepicker" value="@if(isset($banners->dtStartDate)){{date('m/d/Y',strtotime($banners->dtStartDate))}}@else{{old('dtStartDate')}}@endif" autocomplete="off">
                <div class="text-danger" style="display: none;" id="dtStartDate_error">Enter Start Date title</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Banner end date</label>
                <input type="text" name="dtEndDate" placeholder="Start date" id="dtEndDate" class="form-control datepicker" value="@if(isset($banners->dtEndDate)){{date('m/d/Y',strtotime($banners->dtEndDate))}}@else{{old('dtEndDate')}}@endif" autocomplete="off">
                <div class="text-danger" style="display: none;" id="dtEndDate_error">Enter End Date title</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Banner image</label>
                <input class="form-control" type="file" id="vImage" name="vImage" accept="image/*">
                @if(isset($banners))
                   <img style="width: 100px;" id="img" value="@if(old('vImage') == 'vImage') selected @endif" src="{{asset('uploads/banner/'.$banners->vImage)}}">
                @else 
                   <img style="width: 100px;" id="img" value="" src="{{asset('images/no-image.gif')}}">
                @endif
                <div id="vImage_error" class="text-danger" style="display: none;">Select banner image</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Status</label>
                <select id="eStatus" name="eStatus" class="form-control">
                    <option value="Active" @if(isset($banners)) @if($banners->eStatus == 'Active') selected @endif @endif>Active</option>
                    <option value="Inactive" @if(isset($banners)) @if($banners->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
                </select>
            </div>
            {{-- <div class="col-lg-12">
                <label class="required fs-5 fw-bold mb-2">Description</label>
                <div id="toolbar-container"></div>
                <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Description">@if(old('tDescription')!=''){{old('tDescription')}}@elseif(isset($banners->tDescription)){{$banners->tDescription}}@else{{old('tDescription')}}@endif</textarea>
                <div id="tDescription_error" class="text-danger" style="display: none;">Enter banner description </div>
            </div>             --}}
            <div class="col-12 align-self-end d-inline-block text-center">
              <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
              <a href="{{route('admin.banner')}}" class="btn back-btn">Back</a>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection
@section('custom-js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<script>
   $('.datepicker').datepicker({
      format: 'mm/dd/yyyy'
  });
    tinymce.init({
        selector: 'textarea',
        height: 500,
        menubar: true,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help wordcount'
        ],
        toolbar: 'insert | undo redo | formatselect fontselect fontsizeselect | bold italic underline backcolor forecolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat | blockquote | help | code',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        theme_advanced_fonts : "Andale Mono=andale mono,times;"+
            "Arial=arial,helvetica,sans-serif;"+
            "Arial Black=arial black,avant garde;"+
            "Book Antiqua=book antiqua,palatino;"+
            "Comic Sans MS=comic sans ms,sans-serif;"+
            "Courier New=courier new,courier;"+
            "Georgia=georgia,palatino;"+
            "Helvetica=helvetica;"+
            "Impact=impact,chicago;"+
            "Symbol=symbol;"+
            "Tahoma=tahoma,arial,helvetica,sans-serif;"+
            "Terminal=terminal,monaco;"+
            "Times New Roman=times new roman,times;"+
            "Trebuchet MS=trebuchet ms,geneva;"+
            "Verdana=verdana,geneva;"+
            "Webdings=webdings;"+
            "Wingdings=wingdings,zapf dingbats",
        fontsize_formats: '11px 12px 14px 16px 18px 24px 36px 48px',
        textcolor_map: [
            "000000", "Black",
            "993300", "Burnt orange",
            "333300", "Dark olive",
            "003300", "Dark green",
            "003366", "Dark azure",
            "000080", "Navy Blue",
            "333399", "Indigo",
            "333333", "Very dark gray",
            "800000", "Maroon",
            "FF6600", "Orange",
            "808000", "Olive",
            "008000", "Green",
            "008080", "Teal",
            "0000FF", "Blue",
            "666699", "Grayish blue",
            "808080", "Gray",
            "FF0000", "Red",
            "FF9900", "Amber",
            "99CC00", "Yellow green",
            "339966", "Sea green",
            "33CCCC", "Turquoise",
            "3366FF", "Royal blue",
            "800080", "Purple",
            "999999", "Medium gray",
            "FF00FF", "Magenta",
            "FFCC00", "Gold",
            "FFFF00", "Yellow",
            "00FF00", "Lime",
            "00FFFF", "Aqua",
            "00CCFF", "Sky blue",
            "993366", "Red violet",
            "FFFFFF", "White",
            "FF99CC", "Pink",
            "FFCC99", "Peach",
            "FFFF99", "Light yellow",
            "CCFFCC", "Pale green",
            "CCFFFF", "Pale cyan",
            "99CCFF", "Light sky blue",
            "CC99FF", "Plum"
          ],
          file_picker_callback: function(callback, value, meta) 
            {
            if (meta.filetype == 'image') {
            $('#upload').trigger('click');
            $('#upload').on('change', function() {
            var file = this.files[0];
            var reader = new FileReader();
            reader.onload = function(e) {
            callback(e.target.result, {
            alt: ''
            });
            };
            reader.readAsDataURL(file);
            });
            }
            },
    });

    $(document).on('change','#vImage',function()
    {
        if (this.files && this.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
            $('#img').attr('src', e.target.result);
          };
          reader.readAsDataURL(this.files[0]);
        }
    });
  $(document).on('click','#submit',function()
  {
    iBannerId         = $("#id").val();
    vTitle          = $("#vTitle").val();
    vLocation       = $("#vLocation").val();
    dtStartDate       = $("#dtStartDate").val();
    dtEndDate       = $("#dtEndDate").val();
    vImage       = $("#vImage").val();
    
    // tDescription    = tinyMCE.get('tDescription').getContent();
    vImage          = $("#vImage").val();

    var error = false;

    if(vTitle.length == 0){
      $("#vTitle_error").show();
      error = true;
    } else {
      $("#vTitle_error").hide();
    }
    if(dtStartDate.length == 0){
      $("#dtStartDate_error").show();
      error = true;
    } else {
      $("#dtStartDate_error").hide();
    }
    if(dtEndDate.length == 0){
      $("#dtEndDate_error").show();
      error = true;
    } else {
      $("#dtEndDate_error").hide();
    }
    
   

   
    
    <?php if(!isset($banners->iBannerId)){?>
      if(vImage.length == 0){
        $("#vImage_error").show();
        error = true;
      } else {
        $("#vImage_error").hide();
      } 
    <?php } ?>

    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });

</script>
@endsection
