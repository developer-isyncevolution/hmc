@extends('layouts.admin.index')
@section('content')
    <div class="main-panel">
        <div class="page-title text-center">
            <h3>
                Role
            </h3>
        </div>
        <div class="col-lg-12 mx-auto">
            <div class="listing-page">
                <div class="card mb-5 mb-xl-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-mid">
                                    <ul class="d-flex list-unstyled">
                                        @if(\App\Libraries\General::check_permission('','eCreate') == true)
                                        <li>
                                            <a href="{{ route('admin.role.create') }}" class="btn create_permission add-btn mx-2">
                                                <i class="fa fa-plus"></i> Add
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mx-auto">
                            <div class="custome-serce-box">
                                <input type="text" class="form-control" id="keyword" name="search" placeholder="Search">
                                <span class="search-btn"><i class="fas fa-search"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr class="fw-bolder text-muted">
                                <th class="w-25px text-center" data-orderable="false">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input class="form-check-input" type="checkbox" value="1" data-kt-check="true"
                                            data-kt-check-target=".widget-13-check" id="selectall" type="checkbox"
                                            name="selectall">
                                        <label for="selectall">&nbsp;</label>
                                    </div>
                                </th>
                                <th class="min-w-100px">
                                    <a id="vRole" class="sort" data-column="vRole" data-order="ASC" href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Role Title</span>
                                    </a>
                                </th>

                                <th class="max-w-100px">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader" width="250px"
                                height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function() {

            setTimeout(function() {
                $.ajax({
                    url: "{{ route('admin.role.ajax_listing') }}",
                    type: "get",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
            }, 1000);
        });

        $("#selectall").click(function() {
            if (this.checked) {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', true);
                });
            } else {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', false);
                });
            }
        });

        $(document).on('click', '#delete_btn', function() {
                    var roleID = [];

                    $("input[name='roleID[]']:checked").each(function() {
                        roleID.push($(this).val());
                    });

                    var roleID = roleID.join(",");

                    if (roleID.length == 0) {
                        alert('Please select records.')
                    } else {
                        swal({
                                title: "Are you sure delete all Role ?",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            })
                            .then((willDelete) => {
                                if (willDelete) {
                                    $.ajax({
                                        url: "{{ route('admin.role.ajax_listing') }}",
                                        type: "GET",
                                        data: {
                                            roleID: roleID,
                                            action: 'multiple_delete'
                                        },
                                        success: function(response) {
                                            $("#table_record").html(response);
                                            $("#ajax-loader").hide();
                                            notification_error("Role Deleted Successfully");
                                            setTimeout(function() {
                                                location.reload();
                                            }, 1000);
                                        }
                                    });
                                }
                            });
                        }
                    });

                        $("#keyword").keyup(function() {
                            var keyword = $("#keyword").val();

                            setTimeout(function() {
                                $.ajax({
                                    url: "{{ route('admin.role.ajax_listing') }}",
                                    type: "get",
                                    data: {
                                        keyword: keyword,
                                        action: 'search'
                                    },
                                    success: function(response) {
                                        $("#table_record").html(response);
                                        $("#ajax-loader").hide();
                                        $("#table_record").show();
                                    }
                                });
                            }, 1000);
                        });

                        $(document).on('click', '.sort', function() {
                            column = $(this).data("column");
                            order = $(this).attr('data-order');


                            if (order == "ASC") {
                                $(this).attr('data-order', 'DESC');
                            } else {
                                $(this).attr('data-order', 'ASC');
                            }

                            // $("#ajax-loader").show();

                            setTimeout(function() {
                                $.ajax({
                                    url: "{{ route('admin.role.ajax_listing') }}",
                                    type: "get",
                                    data: {
                                        column: column,
                                        order,
                                        order,
                                        action: 'sort'
                                    },
                                    success: function(response) {
                                        console.log(response);
                                        $("#table_record").html(response);
                                        $("#ajax-loader").hide();
                                    }
                                });
                            }, 1000);
                        });

                        $(document).on('click', '.delete', function() {
                            if (confirm('Are you sure delete this data ?')) {
                                id = $(this).data("id");

                                $("#ajax-loader").show();

                                setTimeout(function() {
                                    $.ajax({
                                        url: "{{ route('admin.role.ajax_listing') }}",
                                        type: "GET",
                                        data: {
                                            iRoleId: id,
                                            action: 'delete'
                                        },
                                        success: function(response) {
                                            $("#table_record").html(response);
                                            $("#ajax-loader").hide();
                                        }
                                    });
                                }, 1000);
                            }
                        });

                        $(document).on('click', '.ajax_page', function() {
                            pages = $(this).data("pages");
                            keyword = $("#keyword").val();

                            $("#table_record").html('');
                            $("#ajax-loader").show();

                            url = "{{ route('admin.role.ajax_listing') }}";

                            setTimeout(function() {
                                $.ajax({
                                    url: url,
                                    type: "GET",
                                    data: {
                                        pages: pages,
                                        keyword: keyword,
                                        action: 'search'
                                    },
                                    success: function(response) {
                                        $("#table_record").html(response);
                                        $("#ajax-loader").hide();
                                    }
                                });
                            }, 500);
                        });
    </script>
@endsection
