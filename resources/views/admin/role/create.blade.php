@extends('layouts.admin.index')
@section('content')

<div class="main-panel">

  <div class="page-title text-center">
    <h3>
      Add role
    </h3>
</div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
      {{-- <div class="card-header">
        @if(isset($roles))
        <h4 class="card-title" id="row-separator-basic-form">Edit role</h4>
        @else
        <h4 class="card-title" id="row-separator-basic-form">Add role</h4>
        @endif
      </div> --}}
        <form action="{{route('admin.role.store')}}" name="frm" id="frm" class="row g-5 add-sport mt-0" method="post" enctype="multipart/form-data">
            @csrf

            <input type="hidden" id="id" name="id" value="@if(isset($roles)) {{$roles->iRoleId}} @endif">
            <div class="col-xxl-4 col-lg-6 col-md-12">
              <label>Role Name</label>
              <input type="text" class="form-control" id="vRole" name="vRole" placeholder="Role Name" value="@if(old('vRole')!=''){{old('vRole')}}@elseif(isset($roles->vRole)){{$roles->vRole}}@else{{old('vRole')}}@endif">
              <div class="text-danger" style="display: none;" id="vRole_error">Please enter Role Name</div>
            </div>      

            <div class="col-xxl-4 col-lg-6 col-md-12">
              <label>Status</label>
              <select id="eStatus" name="eStatus" class="form-control">
                <option value="Active" @if(isset($roles)) @if($roles->eStatus == 'Active') selected @endif @endif>Active</option>
                <option value="Inactive" @if(isset($roles)) @if($roles->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
              </select>
            </div>

            <hr class="mx-0">
            <div class="col-12 align-self-end d-inline-block">
              <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
              <a href="{{route('admin.role')}}" class="btn back-btn">Back</a>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script>
 $('#eStatus').select2({
        width: '100%',
        placeholder: "Select status",
        allowClear: true
    });
$(document).on('click','#submit',function(){
    iRoleId     = $("#id").val();
    
   
      vRole = $("#vRole").val();
  

    var error = false;

   
      if(vRole.length == 0){
        $("#vRole_error").show();
        error = true;
      } else{
        $("#vRole_error").hide();
      }
    
    

    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

});

</script>
@endsection
