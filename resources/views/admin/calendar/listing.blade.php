@extends('layouts.admin.index')
@section('content')
<div class="main-panel">

   <div class="page-title text-center">
      <h3>
         Calendar
      </h3>
  </div>


<div class="col-lg-12 mx-auto">
<div class="card mb-5 mb-xl-4">

   <div class="card-header border-0 p-0">
      <ul class="c-nav-tab">
          <li>
            <a href="{{route('admin.schedule')}}" class="add-btn btn mx-2 w-100">
               Lab Schedule
               </a>
          </li>
          <li>
            <a href="" class="active add-btn btn mx-2 w-100">
               Calendar
               </a>
          </li>      
      </ul>
  </div>

   <div class="row">
      <div class="col-lg-12">
          <div class="card">
              <div class="card-mid">
                  <ul class="d-flex list-unstyled">
                     @if(\App\Libraries\General::check_permission('','eCreate') == true)
                      <li>
                          <a href="#" class="btn create_permission add-btn me-2" data-bs-toggle="modal" data-bs-target="#kt_modal_new_target">  <i class="fa fa-plus"></i> Add Event</a>                                                       
                      </li>
                      @endif
                  </ul>
              </div>
          </div>
      </div>
  </div>


</div>
   
      <div class="card-header">         
      
      <div id="calendar">
      </div>
      <div class="modal fade" id="kt_modal_new_target" tabindex="-1" aria-hidden="true">
         <!--begin::Modal dialog-->
         <div class="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div class="modal-content">
               <!--begin::Form-->
               <form class="form" method="POST" action="{{route('admin.calendar.store')}}" id="kt_modal_add_event_form">
                  <input type="hidden" id="iEventId" name="iEventId" value="@if(isset($edit_event)) {{$edit_event->iEventId}} @endif">
                  <!--begin::Modal header-->
                  @csrf
                  <div class="modal-header">
                     <!--begin::Modal title-->
                     <h2 class="fw-bolder" data-kt-calendar="title">Add Event</h2>
                     <!--end::Modal title-->
                     <!--begin::Close-->
                     <div class="btn btn-icon btn-sm btn-active-icon-primary" id="kt_modal_add_event_close">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                        @if(isset($edit_event))
                            <a href="{{route('admin.calendar')}}">
                                <span class="svg-icon svg-icon-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" id="close" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                        <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                                    </svg>
                                </span>
                            </a>
                        @else
                        <span class="svg-icon svg-icon-1">
                           <svg xmlns="http://www.w3.org/2000/svg" id="close" width="24" height="24" viewBox="0 0 24 24" fill="none">
                              <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                              <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                           </svg>
                        </span>
                        @endif
                        <!--end::Svg Icon-->
                     </div>
                     <!--end::Close-->
                  </div>
                  <!--end::Modal header-->
                  <!--begin::Modal body-->
                  <div class="modal-body py-10 px-lg-17">
                     <!--begin::Input group-->
                     <div class="fv-row mb-9">
                        <!--begin::Label-->
                        <label class="fs-6 fw-bold mb-2">Customer</label>
                        <select name="iCustomerId" id="iCustomerId" class="form-control">
                           <option value="none">Select Customer</option>
                           @foreach($customer as $key => $customers)
                           <option value="{{$customers->iCustomerId}}" @if(isset($edit_event)){{$customers->iCustomerId == $edit_event->iCustomerId  ? 'selected' : ''}} @endif>{{$customers->vOfficeName}}</option>
                           @endforeach
                        </select>
                     </div>
                     <div class="fv-row mb-9">
                        <label class="fs-6 fw-bold mb-2">Office</label>
                        <select name="iOfficeId" id="iOfficeId" class="form-control">
                           <option value="none">Select First Customer</option>
                           @if(isset($edit_event->iOfficeId))
                           @foreach($office_data as $key => $data)
                           <option value = "{{$data->iOfficeId }}"@if(isset($edit_event))@if($edit_event->iOfficeId == $data->iOfficeId) selected @endif @endif  >{{ $data->vOfficeName}}</option>
                           @endforeach
                           @endif
                        </select>
                       
                        <!--end::Input-->
                     </div>
                     <!--end::Input group-->
                     <!--begin::Input group-->
                     <div class="fv-row mb-9">
                        <!--begin::Label-->
                        <label class="fs-6 fw-bold mb-2">Event Color</label>
                        <!--end::Label-->
                        <!--begin::Input-->
                        <input type="color" name="vColor" class="w-25 form-control form-control-color" id="exampleColorInput"  value="@if(old('vColor')!=''){{old('vColor')}}@elseif(isset($edit_event->vColor)){{$edit_event->vColor}}@else{{old('vColor')}}@endif" title="Choose your color">

                       
                        
                        <!--end::Input-->
                     </div>
                     <div class="fv-row mb-9">
                        <!--begin::Label-->
                        <label class="fs-6 fw-bold mb-2">Event Title</label>
                        <!--end::Label-->
                        <!--begin::Input-->
                        <input type="text" class="form-control form-control-solid" id="vTitle" name="vTitle" placeholder="Event Title" value="@if(old('vTitle')!=''){{old('vTitle')}}@elseif(isset($edit_event->vTitle)){{$edit_event->vTitle}}@else{{old('vTitle')}}@endif">
                      
                        <!--end::Input-->
                     </div>
                     <!--end::Input group-->
                     <!--begin::Input group-->
                     <div class="fv-row mb-9">
                        <!--begin::Label-->
                        <label class="fs-6 fw-bold mb-2">Event Discription</label>
                        <!--end::Label-->
                        <!--begin::Input-->
                        <input type="text" class="form-control form-control-solid" id="tDescription" name="tDescription" placeholder="Event Discription" value="@if(old('tDescription')!=''){{old('tDescription')}}@elseif(isset($edit_event->tDescription)){{$edit_event->tDescription}}@else{{old('tDescription')}}@endif">
                       
                        <!--end::Input-->
                     </div>
                     <!--end::Input group-->
                     <!--begin::Input group-->
                     <!--end::Input group-->
                     <!--begin::Input group-->
                     <div class="row row-cols-lg-2 g-10">
                        <div class="col">
                           <div class="fv-row mb-9">
                              <!--begin::Label-->
                              <label class="fs-6 fw-bold mb-2 required">Event Start Date</label>
                              <!--end::Label-->
                              <!--begin::Input-->
                              <input type="date" class="form-control form-control-solid" id="dtStartDate" name="dtStartDate" placeholder="Day Title" value="@if(old('dtStartDate')!=''){{old('dtStartDate')}}@elseif(isset($edit_event->dtStartDate)){{$edit_event->dtStartDate}}@else{{old('dtStartDate')}}@endif">

                            
                              <!--end::Input-->
                           </div>
                        </div>
                        <div class="col" data-kt-calendar="datepicker">
                           <div class="fv-row mb-9">
                              <!--begin::Label-->
                              <label class="fs-6 fw-bold mb-2">Event Start Time</label>
                              <!--end::Label-->
                              <!--begin::Input-->
                              <input type="time" class="form-control form-control-solid" id="tiStartTime" name="tiStartTime" placeholder="Day Title" value="@if(old('tiStartTime')!=''){{old('tiStartTime')}}@elseif(isset($edit_event->tiStartTime)){{$edit_event->tiStartTime}}@else{{old('tiStartTime')}}@endif">

                             
                              <!--end::Input-->
                           </div>
                        </div>
                     </div>
                     <!--end::Input group-->
                     <!--begin::Input group-->
                     <div class="row row-cols-lg-2 		g-10" style="display: none;		">
                        <div class="col">
                           <div class="fv-row mb-9">
                              <!--begin::Label-->
                              <label class="fs-6 fw-bold mb-2 required">Event End Date</label>
                              <!--end::Label-->
                              <!--begin::Input-->
                              <input class="form-control form-control-solid" name="calendar_event_end_date" placeholder="Pick a end date" id="kt_calendar_datepicker_end_date" />
                              <!--end::Input-->
                           </div>
                        </div>
                        <div class="col" data-kt-calendar="datepicker">
                           <div class="fv-row mb-9">
                              <!--begin::Label-->
                              <label class="fs-6 fw-bold mb-2">Event End Time</label>
                              <!--end::Label-->
                              <!--begin::Input-->
                              <input class="form-control form-control-solid" name="calendar_event_end_time" placeholder="Pick a end time" id="kt_calendar_datepicker_end_time" />
                              <!--end::Input-->
                           </div>
                        </div>
                     </div>
                     <!--end::Input group-->
                  </div>
                  <!--end::Modal body-->
                  <!--begin::Modal footer-->
                  <div class="modal-footer flex-center">
                     <!--begin::Button-->
                     @if(isset($edit_event))
                    
                         <a href="{{route('admin.calendar')}}" id="#" class="btn btn-light me-3">Cancel</a>
                
                     @else
                     <button type="reset" id="kt_modal_add_event_cancel" class="btn btn-light me-3">Cancel</button>
                     @endif
                     <!--end::Button-->
                     <!--begin::Button-->
                     <button type="submit" id="kt_modal_add_event_submitss" class="btn btn-primary">
                     <span class="indicator-label">Submit</span>
                     <span class="indicator-progress">Please wait...
                     <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                     </button>
                     <!--end::Button-->
                  </div>
                  <!--end::Modal footer-->
               </form>
               <!--end::Form-->
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('custom-css')
<style></style>
@endsection
@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>
<script>
   $('#iCustomerId').selectize();
   $('#iOfficeId').selectize();
    $('#iCustomerId').on('change', function() {
        var iCustomerId = $("#iCustomerId").val();
       
            $.ajax({
                url:"{{route('admin.calendar.getofficedata')}}",
                type: "get",
                data: {
                    iCustomerId:iCustomerId,
                    _token: '{{csrf_token()}}' 
                },

                dataType : 'json',
                success: function(result){
                    console.log(result);
                    $('#iOfficeId').html('<option value="">Select Office</option>'); 
                    $.each(result.office,function(key,value){
                        $("#iOfficeId").append('<option selected value="'+value.iOfficeId+'">'+value.vOfficeName+'</option>');
                    });
                   
                }
            });
        
    });

   document.addEventListener('DOMContentLoaded', function() {
   var calendarEl = document.getElementById('calendar');
   
   var calendar = new FullCalendar.Calendar(calendarEl, {
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth'
    },
    initialDate: '<?php echo date('Y-m-d'); ?>',
    navLinks: true, // can click day/week names to navigate views
    selectable: true,
    selectMirror: true,
    // select: function(arg) {
    //   var title = prompt('Event Title:');
    //   if (title) {
    //     calendar.addEvent({
    //       title: title,
    //       start: arg.start,
    //       end: arg.end,
    //       allDay: arg.allDay
    //     })
    //   }
    //   calendar.unselect()
    // },
    eventClick: function(arg) {
      alert("clicked: " + arg.e.id());
    },
    editable: true,
    dayMaxEvents: true, // allow "more" link when too many events
    events: [
      <?php foreach ($calendar as $key => $value) { ?>
          {
              title: '<?php echo $value->vTitle; ?>',
              url: '<?php echo route('admin.calendar.edit',$value->iEventId); ?>',
              start: '<?php echo $value->dtStartDate.'T'.$value->tiStartTime; ?>',
              color: '<?php echo $value->vColor; ?>',
          },
      <?php } ?>
    ]
   });
   
   calendar.render();
   });
   
   $(document).ready(function(){
    var iEventId = $('#iEventId').val();
  
    if(iEventId!='')
    {
        $("#kt_modal_new_target").modal('show');
        
    }
    else
    {
        $("#kt_modal_add_event_cancel,#close").click(function(){
             $("#kt_modal_new_target").modal('hide');
        });
    }
      
   });
</script>
@endsection