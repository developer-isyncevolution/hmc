@php
    $eEditStageAccess = \App\Libraries\General::check_permission_slip('eEditStage');
    $eNewStageAccess = \App\Libraries\General::check_permission_slip('eNewStage');
    $eReadyToSendAccess = \App\Libraries\General::check_permission_slip('eReadyToSend');
    $eViewDriverHistoryAccess = \App\Libraries\General::check_permission_slip('eViewDriverHistory');
    $ePickUpAccess = \App\Libraries\General::check_permission_slip('ePickUp');
    $eDropOffAccess = \App\Libraries\General::check_permission_slip('eDropOff');
    $eDirectionAccess = \App\Libraries\General::check_permission_slip('eDirection');
    $eAddCallLogAccess = \App\Libraries\General::check_permission_slip('eAddCallLog');
    $eHistoryCallAccess = \App\Libraries\General::check_permission_slip('eHistoryCall');
    $eLastModifiedAccess = \App\Libraries\General::check_permission_slip('eLastModified');
@endphp

@extends('layouts.admin.index')
@section('content')
<style>
    .rush-a::before {
        background:url("{{asset('admin/assets/images/rush-bg.png')}}");
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center;
    }

    .error-icon i {
        font-size: 18px;
        color: orange;
    }


    .tooltip-inner {
        background-color: orange;
        color: #fff;
    }


    .tooltip-arrow::before {
        border-right-color: orange !important;
        border-left-color: orange !important;
    }
    .swal-title{
        font-size: 20px;
    }
    .swal-icon{
        width: 50px;
        height: 50px;
    }
    .swal-icon--warning__body {
    width: 5px;
    height: 20px;
    top: 10px;
    border-radius: 2px;
    margin-left: -2px;
    }

    .swal-button--danger {
        color: #fff;
        font-size: 14px;
        min-width: 140px;
        border: 1px solid #2a78cf !important;
        background-image: linear-gradient(to right, black, #0e66b2 400px, black 800px);
        background-size: 800px 100%;
        background-position: 50% 100%;
        background-repeat: no-repeat;
        border-radius: 5px;
        padding: 5px !important;
    }

    .swal-button--cancel {
        border-radius: 5px;
        padding: 5px !important;
        font-size: 14px;
        min-width: 140px;
    }

    .swal-footer {
        text-align: center;
    }

    .swal-text
	{
		color: #0060a2 !important;
        font-size: 28px !important;
	}
    #tDescription_Reff .form-control{
        position: relative;
    width: 100%;
    padding-top: 0px;    
    background-color: #eff2f5;
    }

    .virual-slip-wrapper .modal-content table th {
    color: #101220 !important;
    }
</style>
<!-- Modal -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">
<div class="virual-slip-wrapper">
    <div class="modal fade" id="virtual_slip_model" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="">
        <div class="modal-dialog" style="clear: both;
    background-color: rgb(255, 255, 255);
    color: rgb(27, 56, 82);
    width: 1000px;
    max-width: 1000px;
    max-height: 820px;
    box-shadow: rgba(0, 0, 0, 0.1) 0px 0.5rem 1rem;
    margin: 0px auto;
    padding: 0px;
    overflow: auto;
    border-width: 1px;
    border-style: solid;
    border-color: rgba(0, 0, 0, 0.2);
    border-image: initial;
    position: relative;">
        
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@if(isset($CustomerLabData->vOfficeName) && !empty($CustomerLabData->vOfficeName)){{$CustomerLabData->vOfficeName}}@endif</h5>
                    <button type="button" class="btn-close d-none" data-bs-dismiss="modal" id="back" aria-label="Close"></button>
                    <ul class="icons">
                        <li class="print_paper" data-case="{{$Case->iCaseId}}" data-slip="{{$Slip->iSlipId}}"><a  href="#" class="me-3"><i class="fal fa-print" style="font-size: 20px;color: white;"></i></a></li>
                        <li class="me-3 d-none"><a href="#"><img src="{{asset('admin/assets/images/Asset 2.png')}}" alt=""></a></li>
                        <li><a href="{{route('admin.billing.PrintBillingSingleSlipInvoice',$Slip->iSlipId)}}"><i class="fal fa-file-invoice-dollar" style="font-size: 20px;color: white;"></i></a></li>
                        <li class="me-3 d-none"><a href="#"><img src="{{asset('admin/assets/images/Asset 4.png')}}" alt=""></a></li>
                        <li class="me-3 d-none"><a href="javascript:;" id="image_popup"><img src="{{asset('admin/assets/images/Asset 5.png')}}" alt=""></a></li>
                        <li class="d-none"><a href="#"><img src="{{asset('admin/assets/images/Asset 6.png')}}" alt=""></a></li>
                    </ul>
                </div>

                <div style="width:100%;padding:0;margin:0 0 3px 0; border-bottom:1px solid #e5e5e5;">
                    <table class="tslip">
                        <tbody>
                            <tr>
                                <th>Office:</th>
                                <td><span id="LabContent_lbcaseid">@if(isset($CustomerOfficeData->vOfficeName) && !empty($CustomerOfficeData->vOfficeName)){{$CustomerOfficeData->vOfficeName}}@endif</span></td>
                                <th>Pan #</th>
                                <td><span class="badge d-inline-block" style="background-color:{{ $Slip->vColor }}">@if(!empty($Case->vCasePanNumber)){{$Case->vCasePanNumber}}@else{{"----"}}@endif </span></td>
                                <th>Created by</th>
                                <td><span id="LabContent_lboffice">{{$Case->vCreatedByName}}</span></td>
                                <th>Pick up Date</th>
                                <td>
                                    <span id="LabContent_lbduedate">
                                        @if(isset($final_Pickup_date))
                                            {{ date('m/d/Y', strtotime($final_Pickup_date)) }} 
                                        @else 
                                            @if (!empty($lower_product) AND empty($upper_product))
                                                {{date('m/d/Y', strtotime($lower_product->dPickUpDate))}}
                                            @elseif(!empty($upper_product) AND empty($lower_product))
                                                {{date('m/d/Y', strtotime($upper_product->dPickUpDate))}}
                                            @elseif(!empty($lower_product) && !empty($upper_product))
                                                @if($upper_product->dPickUpDate > $lower_product->dPickUpDate)
                                                    {{date('m/d/Y', strtotime($upper_product->dPickUpDate))}}
                                                @elseif($upper_product->dPickUpDate < $lower_product->dPickUpDate)
                                                    {{date('m/d/Y', strtotime($lower_product->dPickUpDate))}}
                                                @elseif($upper_product->dPickUpDate == $lower_product->dPickUpDate)
                                                    {{date('m/d/Y', strtotime($upper_product->dPickUpDate))}}
                                                @endif
                                            @endif
                                        @endif
                                    </span>
                                </td>
                                <td>
                                    <a href="#">
                                        <i class="fad fa-calendar-alt"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Dr:</th>
                                <td><span>{{$Case->vFirstName." ".$Case->vLastName}}</span></td>

                                <th>Case #</th>
                                <td><span>{{$Case->vCaseNumber}} </span></td>
                                <th>Location</th>
                                <td><span>{{$Slip->vLocation}}</span></td>
                                <th>Delivery Date </th>
                                <td><span>

                                    @if(isset($upper_product->dDeliveryDateFinal) && !isset($lower_product->dDeliveryDateFinal))

                                        {{ date('m/d/Y', strtotime($upper_product->dDeliveryDateFinal)) }} 

                                    @elseif(isset($lower_product->dDeliveryDateFinal) && !isset($upper_product->dDeliveryDateFinal))

                                        {{ date('m/d/Y', strtotime($lower_product->dDeliveryDateFinal)) }} 

                                    @elseif((isset($final_delivery_date) && (isset($is_single_product) && $is_single_product =='yes')) )

                                        {{ date('m/d/Y', strtotime($final_delivery_date)) }} 

                                    @else 
                                        
                                        {{"-----"}} 

                                    @endif
                                    </span>
                                </td>
                                <td>
                                    <a href="#">
                                        <i class="fal fa-random"></i>
                                        <!-- <i class="fas fa-bacon"></i> -->
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Patient:</th>
                                <td><span>{{$Case->vPatientName}}</span></td>

                                <th>Slip #</th>
                                <td><span>{{$Slip->vSlipNumber}} </span></td>
                                <th>Case status</th>
                                <td><span>{{$Case->eStatus}}</span></td>
                                <th>Delivery Time </th>
                                <td>
                                    <span>
                                        @if((isset($final_delivery_time) && (isset($is_single_product) && $is_single_product =='yes')) )
                                            {{ date("h:i",strtotime($final_delivery_time))}} 
                                        @else 
                                            {{"-----"}} 
                                        @endif
                                    </span>
                                </td>
                                <td>
                                    <a href="#">
                                        <i class="fal fa-clock"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="modal-body">
                    @php
                    $hide_product = 'no';    
                    @endphp
                    @if(isset($Case->eStatus) && $Case->eStatus == "Canceled")
                    @php
                    $hide_product = 'yes';    
                    @endphp
                    <div class="cen-section full" style="transform:rotate(0deg)!important;width: 100%;top: 30%;text-align: center;">
                        <p class="m-0 py-3">
                            This slip was canceled 
                        </p>
                    </div>
                    @elseif(isset($Case->eStatus) && $Case->eStatus == "On Hold")
                    @php
                    $hide_product = 'yes';    
                    @endphp
                    <div class="cen-section full" style="transform:rotate(0deg)!important;width: 100%;top: 30%;text-align: center;">
                        <p class="m-0 py-3">
                            This slip is on hold 
                        </p>
                    </div>
                    @endif

                    <table class="tslip">

                        <tbody>
                            <tr>
                                <!-- view upper teeth start -->
                                @if((isset($upper_vTeethInMouth) AND isset($upper_vMissingTeeth) AND isset($upper_vWillExtractOnDelivery) AND isset($upper_vHasBeenExtracted) AND isset($upper_vFixOrAdd)) OR (isset($upper_vTeethInMouthOp) AND isset($upper_vMissingTeethOp) AND isset($upper_vWillExtractOnDeliveryOp) AND isset($upper_vHasBeenExtractedOp) AND isset($upper_vFixOrAddOp)))

                                @if((isset($upper_vTeethInMouth) AND isset($upper_vMissingTeeth) AND isset($upper_vWillExtractOnDelivery) AND isset($upper_vHasBeenExtracted) AND isset($upper_vFixOrAdd)))
                                @else
                                @php
                                $upper_vTeethInMouth = $upper_vTeethInMouthOp;
                                $upper_vMissingTeeth = $upper_vMissingTeethOp;
                                $upper_vWillExtractOnDelivery = $upper_vWillExtractOnDeliveryOp;
                                $upper_vHasBeenExtracted = $upper_vHasBeenExtractedOp;
                                $upper_vFixOrAdd = $upper_vFixOrAddOp;
                                $upper_vClasps = $upper_vClaspsOp;

                                @endphp
                                @endif
                                <td style="width:290px;text-align:center;vertical-align:top;"> 
                                @if((isset($upper_product->eStatus) && $upper_product->eStatus == "Canceled") && $hide_product == 'no')
                                
                                    <div class="cen-section upper">
                                        <p class="m-0">
                                        This stage was canceled 
                                        </p>
                                    </div>
                                @elseif((isset($upper_product->eStatus) && $upper_product->eStatus == "On Hold") && $hide_product == 'no')
                                    <div class="cen-section upper">
                                        <p class="m-0">
                                        This stage is on hold
                                        </p>
                                    </div>
                                @endif





                                    <div class="teeth-wrapper upper" id="upper_extraction">

                                        <h3 class="card-title mb-0">
                                            MAXILLARY
                                        </h3>

                                        <svg width="100%" height="100%" viewBox="0 0 1500 822" class="teeth-svg">

                                            <g>

                                                <g>
                                                    <!-- upper-teeth 5 -->
                                                    <a href="javascript:;">
                                                        <image id="uppper-5-teeth" data-id="teeths3" data-name="5" x="325" y="202" width="149" height="152" xlink:href="
                                                        
                                                        @if (in_array('5', $upper_vTeethInMouth))
                                                        {{asset('admin/assets/images/yellow-teeth/up-5.png')}}
                                                        @endif
                                                        @if (in_array('5', $upper_vMissingTeeth))
                                                        {{asset('admin/assets/images/teeth/up-5.png')}}
                                                        @endif
                                                        @if (in_array('5', $upper_vWillExtractOnDelivery))
                                                        {{asset('admin/assets/images/red-teeth/up-5.png')}}
                                                        @endif
                                                        @if (in_array('5', $upper_vHasBeenExtracted))
                                                        {{asset('admin/assets/images/grey-teeth/up-5.png')}}
                                                        @endif
                                                        @if (in_array('5', $upper_vFixOrAdd))
                                                        {{asset('admin/assets/images/green-teeth/up-5.png')}}
                                                        @endif

                                                        " class="img-for-teeth teeth_uppper"></image>

                                                        <text id="uppper-5-num" data-name="5" class="cls-1 teeth_uppper" transform="translate(405.696 287.468) scale(1.305)">
                                                            <tspan x="0">5</tspan>
                                                        </text>
                                                        <image id="uppper-5-claps" data-id="teeths3" data-name="5" x="315" y="190" xlink:href="{{asset('admin/assets/images/teeth/updc-5.png')}}" class=" clap-up-5 teeth_uppper upper-claps" style="
                                                @if (in_array('5', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>
                                                    </a>
                                                </g>
                                                <g>
                                                    <!-- upper-teeth 4 -->
                                                    <a href="javascript:;">
                                                        <image id="uppper-4-teeth" data-id="teeths4" data-name="4" x="266" y="285" width="153" height="149" xlink:href="

                                                        @if (in_array('4', $upper_vTeethInMouth))
                                                        {{asset('admin/assets/images/yellow-teeth/up-4.png')}}
                                                        @endif
                                                        @if (in_array('4', $upper_vMissingTeeth))
                                                        {{asset('admin/assets/images/teeth/up-4.png')}}
                                                        @endif
                                                        @if (in_array('4', $upper_vWillExtractOnDelivery))
                                                        {{asset('admin/assets/images/red-teeth/up-4.png')}}
                                                        @endif
                                                        @if (in_array('4', $upper_vHasBeenExtracted))
                                                        {{asset('admin/assets/images/grey-teeth/up-4.png')}}
                                                        @endif
                                                        @if (in_array('4', $upper_vFixOrAdd))
                                                        {{asset('admin/assets/images/green-teeth/up-4.png')}}
                                                        @endif
                                                        " class="img-for-teeth teeth_uppper"></image>

                                                        <text id="uppper-4-num" data-name="4" class="cls-1 teeth_uppper" transform="translate(346.65 357.771) scale(1.305)">
                                                            <tspan x="0">4</tspan>
                                                        </text>
                                                        <image id="uppper-4-claps" data-id="teeths4" data-name="4" x="250" y="280" xlink:href="{{asset('admin/assets/images/teeth/updc-4.png')}}" class=" clap-up-4 teeth_uppper upper-claps" style="

                                                @if (in_array('4', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>
                                                    </a>
                                                </g>
                                                <g>
                                                    <!-- upper-teeth 3 -->
                                                    <a href="javascript:;">
                                                        <image id="uppper-3-teeth" data-id="teeths7" data-name="3" x="176" y="367" width="220" height="199" xlink:href="
                                                        
                                                        @if (in_array('3', $upper_vTeethInMouth))
                                                        {{asset('admin/assets/images/yellow-teeth/up-3.png')}}
                                                        @endif
                                                        @if (in_array('3', $upper_vMissingTeeth))
                                                        {{asset('admin/assets/images/teeth/up-3.png')}}
                                                        @endif
                                                        @if (in_array('3', $upper_vWillExtractOnDelivery))
                                                        {{asset('admin/assets/images/red-teeth/up-3.png')}}
                                                        @endif
                                                        @if (in_array('3', $upper_vHasBeenExtracted))
                                                        {{asset('admin/assets/images/grey-teeth/up-3.png')}}
                                                        @endif
                                                        @if (in_array('3', $upper_vFixOrAdd))
                                                        {{asset('admin/assets/images/green-teeth/up-3.png')}}
                                                        @endif
                                                        
                                                        " class="img-for-teeth teeth_uppper"></image>

                                                        <text id="uppper-3-num" data-name="3" class="cls-1 teeth_uppper" transform="translate(289.371 467.923) scale(1.305)">
                                                            <tspan x="0">3</tspan>
                                                        </text>

                                                        <image id="uppper-3-claps" data-id="teeths7" data-name="3" x="160" y="367" xlink:href="{{asset('admin/assets/images/teeth/updc-3.png')}}" class=" clap-up-3 teeth_uppper upper-claps" style="
                                                
                                                @if (in_array('3', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>
                                                    </a>
                                                </g>
                                                <g>
                                                    <!-- upper-teeth 2 -->
                                                    <a href="javascript:;" class="test">
                                                        <image id="uppper-2-teeth" data-id="teeths6" data-name="2" x="115" y="500" width="203" height="188" xlink:href="
                                                        
                                                        @if (in_array('2', $upper_vTeethInMouth))
                                                        {{asset('admin/assets/images/yellow-teeth/up-2.png')}}
                                                        @endif
                                                        @if (in_array('2', $upper_vMissingTeeth))
                                                        {{asset('admin/assets/images/teeth/up-2.png')}}
                                                        @endif
                                                        @if (in_array('2', $upper_vWillExtractOnDelivery))
                                                        {{asset('admin/assets/images/red-teeth/up-2.png')}}
                                                        @endif
                                                        @if (in_array('2', $upper_vHasBeenExtracted))
                                                        {{asset('admin/assets/images/grey-teeth/up-2.png')}}
                                                        @endif
                                                        @if (in_array('2', $upper_vFixOrAdd))
                                                        {{asset('admin/assets/images/green-teeth/up-2.png')}}
                                                        @endif
                                                        
                                                        " class="img-for-teeth teeth_uppper"></image>

                                                        <image id="uppper-2-claps" data-id="teeths6" data-name="2" x="100" y="500" xlink:href="{{asset('admin/assets/images/teeth/updc-2.png')}}" class=" clap-up-2 teeth_uppper upper-claps" style="
                                                
                                                @if (in_array('2', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>

                                                        <text id="uppper-2-num" data-name="2" class="cls-1 teeth_uppper" transform="translate(224.87 609.191) scale(1.305)">
                                                            <tspan x="0">2</tspan>
                                                        </text>
                                                    </a>
                                                </g>

                                                <g>
                                                    <!-- upper-teeth 1 -->
                                                    <a href="javascript:;" class="test">

                                                        <image id="uppper-1-teeth" data-id="teeths5" data-name="1" x="57" y="640" width="192" height="156" xlink:href="
                                                        
                                                        @if (in_array('1', $upper_vTeethInMouth))
                                                        {{asset('admin/assets/images/yellow-teeth/up-1.png')}}
                                                        @endif
                                                        @if (in_array('1', $upper_vMissingTeeth))
                                                        {{asset('admin/assets/images/teeth/up-1.png')}}
                                                        @endif
                                                        @if (in_array('1', $upper_vWillExtractOnDelivery))
                                                        {{asset('admin/assets/images/red-teeth/up-1.png')}}
                                                        @endif
                                                        @if (in_array('1', $upper_vHasBeenExtracted))
                                                        {{asset('admin/assets/images/grey-teeth/up-1.png')}}
                                                        @endif
                                                        @if (in_array('1', $upper_vFixOrAdd))
                                                        {{asset('admin/assets/images/green-teeth/up-1.png')}}
                                                        @endif
                                                        
                                                        
                                                        " class="img-for-teeth teeth_uppper"></image>


                                                        <text id="uppper-1-num" data-name="1" data-id="teeths5" class="cls-1 teeth_uppper" transform="translate(163.9 742.356) scale(1.305)">
                                                            <tspan x="0">1</tspan>
                                                        </text>

                                                        <image id="uppper-1-claps" data-id="teeths5" data-name="1" x="40" y="640" xlink:href="{{asset('admin/assets/images/teeth/up-dc-1.png')}}" class=" clap-up-1 teeth_uppper upper-claps" style="
                                                @if (in_array('1', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>

                                                    </a>
                                                    <!-- upper-teeth 1 end-->
                                                </g>


                                                <g>
                                                    <!-- upper-teeth 6 -->
                                                    <a href="javascript:;">
                                                        <image id="uppper-6-teeth" data-id="teeths2" data-name="6" x="403" y="123" width="136" height="178" xlink:href="
                                                        
                                                        @if (in_array('6', $upper_vTeethInMouth))
                                                        {{asset('admin/assets/images/yellow-teeth/up-6.png')}}
                                                        @endif
                                                        @if (in_array('6', $upper_vMissingTeeth))
                                                        {{asset('admin/assets/images/teeth/up-6.png')}}
                                                        @endif
                                                        @if (in_array('6', $upper_vWillExtractOnDelivery))
                                                        {{asset('admin/assets/images/red-teeth/up-6.png')}}
                                                        @endif
                                                        @if (in_array('6', $upper_vHasBeenExtracted))
                                                        {{asset('admin/assets/images/grey-teeth/up-6.png')}}
                                                        @endif
                                                        @if (in_array('6', $upper_vFixOrAdd))
                                                        {{asset('admin/assets/images/green-teeth/up-6.png')}}
                                                        @endif
                                                        
                                                        " class="img-for-teeth teeth_uppper"></image>
                                                        <image id="uppper-6-claps" data-id="teeths2" data-name="6" x="400" y="105" xlink:href="{{asset('admin/assets/images/teeth/updc-6.png')}}" class=" clap-up-6 teeth_uppper upper-claps" style="
                                                @if (in_array('6', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>

                                                        <text id="uppper-6-num" data-name="6" class="cls-1 teeth_uppper" transform="translate(476.76 240.092) scale(1.305)">
                                                            <tspan x="0">6</tspan>
                                                        </text>
                                                    </a>
                                                </g>



                                                <!-- upper-teeth 7 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-7-teeth" data-id="teeths1" data-name="7" x="498" y="72" width="125" height="189" xlink:href="
                                                    
                                                    @if (in_array('7', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-7.png')}}
                                                    @endif
                                                    @if (in_array('7', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-7.png')}}
                                                    @endif
                                                    @if (in_array('7', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-7.png')}}
                                                    @endif
                                                    @if (in_array('7', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-7.png')}}
                                                    @endif
                                                    @if (in_array('7', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-7.png')}}
                                                    @endif

                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-7-claps" data-id="teeths1" data-name="7" x="498" y="50" xlink:href="{{asset('admin/assets/images/teeth/updc-7.png')}}" class=" clap-up-7 teeth_uppper upper-claps" style="
                                        @if (in_array('7', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif

                                        "></image>
                                                <text id="uppper-7-num" data-name="7" class="cls-1 teeth_uppper" transform="translate(563.111 198.624) scale(1.305)">
                                                    <tspan x="0">7</tspan>
                                                </text>

                                                <!-- upper-teeth 8 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-8-teeth" data-id="teeths8" data-name="8" x="607" y="35" xlink:href="
                                                    
                                                    @if (in_array('8', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-8.png')}}
                                                    @endif
                                                    @if (in_array('8', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-8.png')}}
                                                    @endif
                                                    @if (in_array('8', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-8.png')}}
                                                    @endif
                                                    @if (in_array('8', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-8.png')}}
                                                    @endif
                                                    @if (in_array('8', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-8.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-8-claps" data-id="teeths8" data-name="8" x="607" y="10" xlink:href="{{asset('admin/assets/images/teeth/updc-8.png')}}" class=" clap-up-7 teeth_uppper upper-claps" style="
                                        @if (in_array('8', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-8-num" data-name="8" class="cls-1 teeth_uppper" transform="translate(697.646 179.813) scale(1.305)">
                                                    <tspan x="0">8</tspan>
                                                </text>

                                                <!-- upper-teeth 9 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-9-teeth" data-id="teeths16" data-name="9" x="755" y="35" width="150" height="210" xlink:href="

                                                    @if (in_array('9', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-9.png')}}
                                                    @endif
                                                    @if (in_array('9', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-9.png')}}
                                                    @endif
                                                    @if (in_array('9', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-9.png')}}
                                                    @endif
                                                    @if (in_array('9', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-9.png')}}
                                                    @endif
                                                    @if (in_array('9', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-9.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper "></image>
                                                </a>

                                                <image id="uppper-9-claps" data-id="teeths16" data-name="9" x="765" y="10" xlink:href="{{asset('admin/assets/images/teeth/updc-9.png')}}" class=" clap-up-9 teeth_uppper upper-claps" style="

                                        @if (in_array('9', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-9-num" data-name="9" class="cls-1 teeth_uppper" transform="translate(821.604 176.548) scale(1.305)">
                                                    <tspan x="0">9</tspan>
                                                </text>

                                                <!-- upper-teeth 10 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-10-teeth" data-id="teeths9" data-name="10" x="889" y="72" width="125" height="189" xlink:href="

                                                    @if (in_array('10', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-10.png')}}
                                                    @endif
                                                    @if (in_array('10', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-10.png')}}
                                                    @endif
                                                    @if (in_array('10', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-10.png')}}
                                                    @endif
                                                    @if (in_array('10', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-10.png')}}
                                                    @endif
                                                    @if (in_array('10', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-10.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-10-claps" data-id="teeths9" data-name="10" x="910" y="50" xlink:href="{{asset('admin/assets/images/teeth/updc-10.png')}}" class=" clap-up-10 teeth_uppper upper-claps" style="
                                        @if (in_array('10', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif

                                        "></image>
                                                <text id="uppper-10-num" data-name="10" class="cls-1 teeth_uppper" transform="translate(934.785 195.164) scale(1.305)">
                                                    <tspan x="0">10</tspan>
                                                </text>

                                                <!-- upper-teeth 11 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-11-teeth" data-id="teeths10" data-name="11" x="972" y="123" width="137" height="178" xlink:href=" 
                                                    
                                                    @if (in_array('11', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-11.png')}}
                                                    @endif
                                                    @if (in_array('11', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-11.png')}}
                                                    @endif
                                                    @if (in_array('11', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-11.png')}}
                                                    @endif
                                                    @if (in_array('11', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-11.png')}}
                                                    @endif
                                                    @if (in_array('11', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-11.png')}}
                                                    @endif
                                                    
                                                    "class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-11-claps" data-id="teeths10" data-name="11" x="1030" y="100" xlink:href="{{asset('admin/assets/images/teeth/updc-11.png')}}" class=" clap-up-11 teeth_uppper upper-claps" style="
                                        
                                        @if (in_array('11', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-11-num" data-name="11" class="cls-1 teeth_uppper" transform="translate(1031.677 240.099) scale(1.305)">
                                                    <tspan x="0">11</tspan>
                                                </text>

                                                <!-- upper-teeth 12 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-12-teeth" data-id="teeths11" data-name="12" x="1040" y="198" width="149" height="152" xlink:href="
                                                    
                                                    @if (in_array('12', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-12.png')}}
                                                    @endif
                                                    @if (in_array('12', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-12.png')}}
                                                    @endif
                                                    @if (in_array('12', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-12.png')}}
                                                    @endif
                                                    @if (in_array('12', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-12.png')}}
                                                    @endif
                                                    @if (in_array('12', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-12.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-12-claps" data-id="teeths11" data-name="12" x="1115" y="175" xlink:href="{{asset('admin/assets/images/teeth/updc-12.png')}}" class=" clap-up-12 teeth_uppper upper-claps upper-claps" style="
                                            
                                            @if (in_array('12', $upper_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif
                                            
                                            "></image>
                                                <text id="uppper-12-num" data-name="12" class="cls-1 teeth_uppper" transform="translate(1103.168 282.619) scale(1.305)">
                                                    <tspan x="0">12</tspan>
                                                </text>

                                                <!-- upper-teeth 13 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-13-teeth" data-id="teeths12" data-name="13" x="1095" y="284" width="154" height="149" xlink:href="
                                                    
                                                    @if (in_array('13', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-13.png')}}
                                                    @endif
                                                    @if (in_array('13', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-13.png')}}
                                                    @endif
                                                    @if (in_array('13', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-13.png')}}
                                                    @endif
                                                    @if (in_array('13', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-13.png')}}
                                                    @endif
                                                    @if (in_array('13', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-13.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-13-claps" data-id="teeths12" data-name="13" x="1215" y="270" xlink:href="{{asset('admin/assets/images/teeth/updc-13.png')}}" class=" clap-up-13 teeth_uppper upper-claps" style="
                                        @if (in_array('13', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-13-num" data-name="13" class="cls-1 teeth_uppper" transform="translate(1169.916 345.118) scale(1.305)">
                                                    <tspan x="0">13</tspan>
                                                </text>
                                                <!-- upper-teeth 14 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-14-teeth" data-id="teeths15" data-name="14" x="1115" y="361" width="221" height="199" xlink:href="
                                                    
                                                    @if (in_array('14', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-14.png')}}
                                                    @endif
                                                    @if (in_array('14', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-14.png')}}
                                                    @endif
                                                    @if (in_array('14', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-14.png')}}
                                                    @endif
                                                    @if (in_array('14', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-14.png')}}
                                                    @endif
                                                    @if (in_array('14', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-14.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-14-claps" data-id="teeths15" data-name="14" x="1280" y="361" xlink:href="{{asset('admin/assets/images/teeth/updc-14.png')}}" class=" clap-up-14 teeth_uppper upper-claps" style="

                                        @if (in_array('14', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-14-num" data-name="14" class="cls-1 teeth_uppper" transform="translate(1210.87 477.036) scale(1.305)">
                                                    <tspan x="0">14</tspan>
                                                </text>
                                                <!-- upper-teeth 15 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-15-teeth" data-id="teeths14" data-name="15" x="1194" y="503" width="203" height="188" xlink:href="

                                                    @if (in_array('15', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-15.png')}}
                                                    @endif
                                                    @if (in_array('15', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-15.png')}}
                                                    @endif
                                                    @if (in_array('15', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-15.png')}}
                                                    @endif
                                                    @if (in_array('15', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-15.png')}}
                                                    @endif
                                                    @if (in_array('15', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-15.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-15-claps" data-id="teeths14" data-name="15" x="1350" y="503" xlink:href="{{asset('admin/assets/images/teeth/updc-15.png')}}" class=" clap-up-15 teeth_uppper upper-claps" style="
                                        @if (in_array('15', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-15-nuum" data-name="15" class="cls-1 teeth_uppper" transform="translate(1303.838 603.41) scale(1.305)">
                                                    <tspan x="0">15</tspan>
                                                </text>

                                                <!-- upper-teeth 16 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-16-teeth" data-id="teeths13" data-name="16" x="1262" y="643" width="192" height="156" xlink:href="

                                                    @if (in_array('16', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-16.png')}}
                                                    @endif
                                                    @if (in_array('16', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-16.png')}}
                                                    @endif
                                                    @if (in_array('16', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-16.png')}}
                                                    @endif
                                                    @if (in_array('16', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-16.png')}}
                                                    @endif
                                                    @if (in_array('16', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-16.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-16-claps" data-id="teeths13" data-name="16" x="1422" y="643" xlink:href="{{asset('admin/assets/images/teeth/updc-16.png')}}" class=" clap-up-16 teeth_uppper upper-claps" style="
                                        @if (in_array('16', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-16-num" data-name="16" class="cls-1 teeth_uppper" transform="translate(1370.808 743.76) scale(1.305)">
                                                    <tspan x="0">16</tspan>
                                                </text>

                                                <!--upper teeth numbers  -->
                                            </g>
                                        </svg>

                                        <div class="d-block text-center teeth-text">
                                            <a href="javascript:;" class="add-btn p-2 mx-auto uppper_mising_all d-none" id="uppper_mising_all">
                                                Missing all teeth
                                            </a>
                                        </div>
                                        <div class="teeths-wrapper second-row p-0">
                                            <div class="row">
                                                <div class="col-md-12 col-xl-12 col-lg-12">

                                                    <div class="row gy-2">
                                                        @if(!empty($upper_vTeethInMouth))
                                                        @php
                                                        $parts=array_filter($upper_vTeethInMouth);
                                                        $upper_vTeethInMouth = (implode(",",$parts));
                                                        @endphp
                                                        @endif

                                                        @if(!empty($upper_vTeethInMouth))
                                                        <a href="javascript:;">
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <label class="form-control inner-label te-in-month lbl_btn_all" value="" id="upper_in_mouth" disabled> Teeth in mouth <br> <span id="upper_in_mouth_lbl">{{$upper_vTeethInMouth}}</span></label>
                                                                    <input type="hidden" id="upper_in_mouth_value" value="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                        @endif


                                                        @if (!empty(array_filter($upper_vMissingTeeth)))
                                                        @php
                                                        $parts=array_filter($upper_vMissingTeeth);
                                                        $upper_vMissingTeeth = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lbl_btn_all" value="" id="upper_missing_teeth" disabled>missing teeth <br> <span id="upper_missing_teeth_lbl">{{$upper_vMissingTeeth}}</span></label>
                                                                <input type="hidden" id="upper_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($upper_vMissingTeethOp))
                                                        @if (!empty(array_filter($upper_vMissingTeethOp)))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lbl_btn_all" value="" id="upper_missing_teeth" disabled>missing teeth <br> <span id="upper_missing_teeth_lbl">{{$upper_product->vMissingTeethOp}}</span></label>
                                                                <input type="hidden" id="upper_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif



                                                        @if (!empty(array_filter($upper_vWillExtractOnDelivery)))
                                                        @php
                                                        $parts=array_filter($upper_vWillExtractOnDelivery);
                                                        $upper_vWillExtractOnDelivery = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lbl_btn_all" value="" id="upper_ectract_delivery" disabled>will extract on delivery <br><span id="upper_ectract_delivery_lbl">{{$upper_vWillExtractOnDelivery}}</span></label>
                                                                <input type="hidden" id="upper_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($upper_vWillExtractOnDeliveryOp))
                                                        @if (!empty(array_filter($upper_vWillExtractOnDeliveryOp)))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lbl_btn_all" value="" id="upper_ectract_delivery" disabled>will extract on delivery <br><span id="upper_ectract_delivery_lbl">{{$upper_product->vWillExtractOnDeliveryOp}}</span></label>
                                                                <input type="hidden" id="upper_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif

                                                        @if (!empty(array_filter($upper_vHasBeenExtracted)))
                                                        @php
                                                        $parts=array_filter($upper_vHasBeenExtracted);
                                                        $upper_vHasBeenExtracted = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lbl_btn_all" value="" id="upper_been_extracted" disabled> has been extracted <br><span id="upper_been_extracted_lbl">{{$upper_vHasBeenExtracted}}</span></label>
                                                                <input type="hidden" id="upper_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($upper_vHasBeenExtractedOp))
                                                        @if (!empty(array_filter($upper_vHasBeenExtractedOp)))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lbl_btn_all" value="" id="upper_been_extracted" disabled> has been extracted <br><span id="upper_been_extracted_lbl">{{$upper_product->vHasBeenExtractedOp}}</span></label>
                                                                <input type="hidden" id="upper_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif



                                                        @if (!empty(array_filter($upper_vFixOrAdd)))
                                                        @php
                                                        $parts=array_filter($upper_vFixOrAdd);
                                                        $upper_vFixOrAdd = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lbl_btn_all" value="" id="upper_fix" disabled> fix or add <br> <span id="upper_fix_lbl">{{$upper_vFixOrAdd}}</span></label>
                                                                <input type="hidden" id="upper_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($upper_vFixOrAddOp))
                                                        @if (!empty(array_filter($upper_vFixOrAddOp)))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lbl_btn_all" value="" id="upper_fix" disabled> fix or add <br> <span id="upper_fix_lbl">{{$upper_product->vFixOrAddOp}}</span></label>
                                                                <input type="hidden" id="upper_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif



                                                        @if (!empty(array_filter($upper_vClasps)))
                                                        @php
                                                        $parts=array_filter($upper_vClasps);
                                                        $upper_vClasps = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lbl_btn_all" value="" id="upper_clasps" disabled>clasps<br> <span id="upper_clasps_lbl">{{$upper_vClasps}}</span></label>
                                                                <input type="hidden" id="upper_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($upper_vClaspsOp))
                                                        @if (!empty(array_filter($upper_vClaspsOp)))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lbl_btn_all" value="" id="upper_clasps" disabled>clasps<br> <span id="upper_clasps_lbl">{{$upper_product->vClaspsOp}}</span></label>
                                                                <input type="hidden" id="upper_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif

                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </td>
                                @endif
                                <!-- view upper teeth End -->


                                <!-- middle inforamtion section start -->
                                <td class="position-relative" style="vertical-align:top;vertical-align:top;font-weight:normal;">
                                    <div class="row c-row-text">
                                        <div class="col-lg-12">
                                            <h3 class="card-title mb-0">
                                                CASE DESIGN
                                            </h3>
                                        </div>
                                        <div class="col-lg-12">

                                            <table class="w-100 inner-table" style="table-layout: fixed;">
                                                <tbody>
                                                    @if(!empty($upper_product->vProductName) || !empty($lower_product->vProductName))
                                                    <tr>
                                                        <td colspan="2" style="text-align: right;">
                                                            @if(!empty($upper_product->vProductName))
                                                            <span>{{$upper_product->vProductName}}</span>
                                                            @endif
                                                        </td>
                                                        <td style="text-align:center;"><span style="font-weight: bold;">Product</span>
                                                        </td>
                                                        <td colspan="2">
                                                            @if(!empty($lower_product->vProductName))
                                                            <span> {{$lower_product->vProductName}}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @if(!empty($upper_product->vGradeName) || !empty($lower_product->vGradeName))
                                                    <tr>
                                                        <td colspan="2" style="text-align: right;">
                                                            @if(!empty($upper_product->vGradeName))
                                                            <span>{{$upper_product->vGradeName}}</span>
                                                            @endif
                                                        </td>
                                                        <td style="text-align:center;">
                                                            <span style="font-weight:bold;">Grade</span>
                                                        </td>
                                                        <td colspan="2">
                                                            @if(!empty($lower_product->vGradeName))
                                                            <span> {{$lower_product->vGradeName}}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @if(!empty($upper_product->vStageName) || !empty($lower_product->vStageName))
                                                    <tr>
                                                        <td colspan="2" style="text-align: right;">
                                                            @if(!empty($upper_product->vStageName))
                                                            <span>
                                                                {{$upper_product->vStageName}}
                                                            </span>
                                                            @endif
                                                        </td>
                                                        <td style="text-align:center;"><span style="font-weight: bold;">Stage</span>
                                                        </td>
                                                        <td colspan="2">
                                                            @if(!empty($lower_product->vStageName))
                                                            <span>{{$lower_product->vStageName}}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif


                                                    <!-- TEETH SHADE -->
                                                    @if(!empty($upper_product->vBrandName) || !empty($lower_product->vBrandName))
                                                    <tr>
                                                        <td colspan="2" style="text-align:right;">
                                                            @if(!empty($upper_product->vBrandName))
                                                            <span>{{$upper_product->vBrandName}} - {{$upper_product->vCode}}</span>
                                                            @endif
                                                        </td>


                                                        <td style="font-weight: bold;text-align: center;"><span>Teeth Shade</span></td>

                                                        <td colspan="2">

                                                            @if(!empty($lower_product->vBrandName))
                                                            <span>{{$lower_product->vBrandName}} - {{$lower_product->vCode}}</span>

                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif

                                                    <!--   GUM SHADE -->
                                                    @if(!empty($upper_product->vGumBrand) || !empty($lower_product->vGumBrand))
                                                    <tr>
                                                        <td colspan="2" style="text-align:right;">
                                                            @if(!empty($upper_product->vGumBrand))
                                                            <span>{{$upper_product->vGumBrand}} - {{$upper_product->vGumShade}}</span>
                                                            @endif
                                                        </td>

                                                        <td style="font-weight: bold;text-align:center;"><span>Gum Shade</span></td>

                                                        <td colspan="2">
                                                            @if(!empty($lower_product->vGumBrand))
                                                            <span>{{$lower_product->vGumBrand}} - {{$lower_product->vGumShade}}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    <!--   GUM SHADE -->


                                                     @if(isset($upper_impression) OR isset($upper_impression))

                                                        @if(!isset($upper_impression))
                                                            @php $upper_impression = array(); @endphp
                                                        @endif

                                                        @if(!isset($lower_impression))
                                                            @php $lower_impression = array(); @endphp
                                                        @endif

                                                    @endif

                                                    @if((!empty($upper_impression) && sizeof($upper_impression) > 0) OR (!empty($lower_impression) && sizeof($lower_impression) > 0))
                                                    <tr>
                                                        <td colspan="2" style="vertical-align: top;">
                                                            <table class="w-100">
                                                                @if(empty($upper_impression))
                                                                @php
                                                                $upper_impression = $upper_impressionOp
                                                                @endphp
                                                                @endif

                                                                @foreach ($upper_impression as $value_impression)
                                                                <tr style="vertical-align: top;">
                                                                    <td class="p-0" style=" text-align: right;">
                                                                        <span>{{$value_impression->vImpressionName}}</span>
                                                                        <span style="min-width: 30px; display: inline-block;">Qty {{$value_impression->iQuantity}}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>

                                                        <td style="vertical-align: top;font-weight: bold;text-align:center;">
                                                            <span>Impressions</span>
                                                        </td>

                                                        <td colspan="2" style="vertical-align: top;">
                                                            <table class="w-100">
                                                                @if(empty($lower_impression))
                                                                @php
                                                                $lower_impression = $lower_impressionOp
                                                                @endphp
                                                                @endif
                                                                @foreach ($lower_impression as $lower_impression)
                                                                <tr style="vertical-align: top;">
                                                                    <td class="p-0" style=" text-align: left;">
                                                                        <span style="min-width: 30px; display: inline-block;">Qty {{$lower_impression->iQuantity}}</span>
                                                                        <span>{{$lower_impression->vImpressionName}}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    @endif



                                                    @if(isset($upper_addons) OR isset($lower_addons))

                                                        @if(!isset($upper_addons))
                                                            @php $upper_addons = array(); @endphp
                                                        @endif

                                                        @if(!isset($lower_addons))
                                                            @php $lower_addons = array(); @endphp
                                                        @endif

                                                    @endif

                                                    @if((!empty($upper_addons) && sizeof($upper_addons) > 0) OR (!empty($lower_addons) && sizeof($lower_addons) > 0))
                                                    <tr>
                                                        <td colspan="2" style="vertical-align: top;">
                                                            <table class="w-100">
                                                                @foreach ($upper_addons as $value)
                                                                <tr style="vertical-align: top;">
                                                                    <td class="p-0" style=" text-align: right;">
                                                                        <span>{{$value->vAddonName}}</span>
                                                                        <span style="min-width: 40px; display: inline-block;">Qty {{$value->iQuantity}}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>

                                                        <td style="vertical-align: top;font-weight: bold;text-align:center;">
                                                            <span>Add Ons</span>
                                                        </td>

                                                        <td colspan="2" style="vertical-align: top;">
                                                            <table class="w-100">
                                                                @foreach ($lower_addons as $value)
                                                                <tr>
                                                                    <td class="p-0" style=" text-align: left;">
                                                                        <span style="min-width: 40px; display: inline-block;">Qty {{$value->iQuantity}}</span>
                                                                        <span>{{$value->vAddonName}}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    @endif

                                                    @if(!empty($upper_product->eStatus) || !empty($lower_product->eStatus))
                                                    <tr>
                                                        @if(!empty($upper_product->eStatus))
                                                        <td colspan="2" style="text-align: right;"><span>{{$upper_product->eStatus}}</span></td>
                                                        @else
                                                        <td colspan="2"></td>
                                                        @endif
                                                        <td style="font-weight: bold;text-align: center;"><span>Stage Status</span></td>
                                                        @if(!empty($lower_product->eStatus))
                                                        <td colspan="2"><span>{{$lower_product->eStatus}}</span></td>
                                                        @else
                                                        <td colspan="2"></td>
                                                        @endif
                                                    </tr>
                                                    @endif
                                                   
                                                    
                                                    @if((isset($rush_product) && $rush_product =='yes') && (isset($is_single_product) && $is_single_product =='no') )
                                                
                                                    <tr> 
                                                        @if((isset($upper_rush_product) && $upper_rush_product =='yes') )
                                                            <td colspan="2" style="text-align: right;"><span>{{date('m/d/Y', strtotime($final_delivery_date))}} </span></td>
                                                            @elseif(isset($final_delivery_date_upper) && !empty($final_delivery_date_upper)
                                                            )
                                                             <td colspan="2" style="text-align: right;"><span>{{date('m/d/Y', strtotime($final_delivery_date_upper))}} </span></td>
                                                            @elseif(isset($upper_product->dDeliveryDate) && $upper_product->dDeliveryDate !='')
                                                            <td colspan="2" style="text-align: right;"><span>{{date('m/d/Y', strtotime($upper_product->dDeliveryDate))}} </span></td>
                                                            @else
                                                            <td colspan="2"><span></span></td>
                                                        @endif
                                                        
                                                        <td style="vertical-align: top;font-weight: bold;text-align: center;">
                                                            <span>Delivery Date</span>
                                                        </td>
                                                        
                                                        @if((isset($lower_rush_product) && $lower_rush_product =='yes') )
                                                            <td colspan="2" style=""><span>{{date('m/d/Y', strtotime($final_delivery_date))}} </span></td>
                                                            @elseif(isset($final_delivery_date_lower) && !empty($final_delivery_date_lower)
                                                            )
                                                             <td colspan="2" style=""><span>{{date('m/d/Y', strtotime($final_delivery_date_lower))}} </span></td>
                                                            @elseif(isset($lower_product->dDeliveryDate) && $lower_product->dDeliveryDate !='')
                                                            <td colspan="2" style=""><span>{{date('m/d/Y', strtotime($lower_product->dDeliveryDate))}} </span></td>
                                                            
                                                        @endif
                                                                
                                                    </tr>

                                                    <tr>
                                                        @if(isset($upper_product->dDeliveryDate) && $upper_product->dDeliveryDate !='')
                                                        <td colspan="2" style="text-align:right;"><span>{{date("h:i A",strtotime($final_delivery_time))}}</span></td>
                                                        @else
                                                        <td colspan="2"><span></span></td>
                                                        @endif
                                                        <td style="vertical-align: top;font-weight: bold;text-align: center;">
                                                            <span>Delivery Time</span>
                                                        </td>
                                                        @if(isset($lower_product->dDeliveryDate) && $lower_product->dDeliveryDate)
                                                        <td colspan="2"><span>{{date("h:i A",strtotime($final_delivery_time))}} </span></td>
                                                       
                                                        @endif
                                                    </tr>
                                                    @endif
                                                    <tr>

                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </td>

                                <!-- middle inforamtion section End -->



                                <!-- view Lower teeth start -->
                                <td style="width:290px;text-align:center;vertical-align:top;">
                                @if((isset($lower_product->eStatus) && $lower_product->eStatus == "Canceled") && $hide_product == 'no')
                                    <div class="cen-section lower">
                                        <p class="m-0">
                                        This stage was canceled
                                        </p>
                                    </div>
                                @elseif((isset($lower_product->eStatus) && $lower_product->eStatus == "On Hold") &&  $hide_product == 'no')
                                    <div class="cen-section lower">
                                        <p class="m-0">
                                        This stage is on hold
                                        </p>
                                    </div>
                                @endif
                                    @if((isset($lower_vTeethInMouth) AND isset($lower_vMissingTeeth) AND isset($lower_vWillExtractOnDelivery) AND isset($lower_vHasBeenExtracted) AND isset($lower_vFixOrAdd)) OR (isset($lower_vTeethInMouthOp) AND isset($lower_vMissingTeethOp) AND isset($lower_vWillExtractOnDeliveryOp) AND isset($lower_vHasBeenExtractedOp) AND isset($lower_vFixOrAddOp)))

                                    @if((isset($lower_vTeethInMouth) AND isset($lower_vMissingTeeth) AND isset($lower_vWillExtractOnDelivery) AND isset($lower_vHasBeenExtracted) AND isset($lower_vFixOrAdd)))
                                    @else
                                    @php
                                    $lower_vTeethInMouth = $lower_vTeethInMouthOp;
                                    $lower_vMissingTeeth = $lower_vMissingTeethOp;
                                    $lower_vWillExtractOnDelivery = $lower_vWillExtractOnDeliveryOp;
                                    $lower_vHasBeenExtracted = $lower_vHasBeenExtractedOp;
                                    $lower_vFixOrAdd = $lower_vFixOrAddOp;
                                    $lower_vClasps = $lower_vClaspsOp;

                                    @endphp
                                    @endif
                                    <div class="teeth-wrapper lower" id="lower_extraction">
                                        <h3 class="card-title mb-0">
                                            MANDIBULAR
                                        </h3>
                                        <svg width="100%" height="100%" viewBox="0 0 1500 790" class="teeth-svg">

                                            <g>
                                                <g></g>
                                                <!-- lower-teeth 1 -->
                                                <a href="javascript:;">
                                                    <image id="lower-17-teeth" data-id="teeth15" data-name="17" x="30" y="555" width="252" height="200" xlink:href="
                                                    
                                                    @if (in_array('17', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-17.png')}}
                                                    @endif
                                                    @if (in_array('17', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-17.png')}}
                                                    @endif
                                                    @if (in_array('17', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-17.png')}}
                                                    @endif
                                                    @if (in_array('17', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-17.png')}}
                                                    @endif
                                                    @if (in_array('17', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-17.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth  teeth_lower"></image>
                                                </a>

                                                <!--  image with cliap-1 -->
                                                <image id="lower-17-claps" data-name="17" x="15" y="540" width="80" height="190" xlink:href="{{asset('admin/assets/images/teeth/dc-17.png')}}" class=" clap-17 teeth_lower lower-claps" style="
                                        @if (in_array('17', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="lower-17-num" data-name="17" class="cls-1 teeth_lower" transform="translate(196.632 625.98) scale(1.199)">
                                                    <tspan x="-30">17</tspan>
                                                </text>


                                                <!-- lower-teeth 1 end-->

                                                <g></g>
                                                <!-- lower-teeth 2 -->
                                                <a href="javascript:;">
                                                    <image id="lower-18-teeth" data-id="teeth7" data-name="18" x="95" y="400" width="232" height="194" xlink:href="

                                                    @if (in_array('18', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-18.png')}}
                                                    @endif
                                                    @if (in_array('18', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-18.png')}}
                                                    @endif
                                                    @if (in_array('18', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-18.png')}}
                                                    @endif
                                                    @if (in_array('18', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-18.png')}}
                                                    @endif
                                                    @if (in_array('18', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-18.png')}}
                                                    @endif

                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-2 -->
                                                <image id="lower-18-claps" data-name="18" x="80" y="382" width="85" height="194" xlink:href="{{asset('admin/assets/images/teeth/dc-18.png')}}" class=" clap-18 teeth_lower lower-claps" style="
                                                
                                            @if (in_array('18', $lower_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif
                                        
                                        
                                        "></image>

                                                <text id="lower-18-num" data-name="18" class="cls-1 teeth_lower" transform="translate(263.156 536.61) scale(1.199)">
                                                    <tspan x="-35" y="-30">18</tspan>
                                                </text>

                                                <!-- lower-teeth 2 end-->

                                                <g></g>
                                                <!-- lower-teeth 3 -->
                                                <a href="javascript:;">
                                                    <image id="lower-19-teeth" data-id="teeth6" data-name="19" x="165" y="290" width="235" height="191" xlink:href="
                                                    
                                                    @if (in_array('19', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-19.png')}}
                                                    @endif
                                                    @if (in_array('19', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-19.png')}}
                                                    @endif
                                                    @if (in_array('19', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-19.png')}}
                                                    @endif
                                                    @if (in_array('19', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-19.png')}}
                                                    @endif
                                                    @if (in_array('19', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-19.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>

                                                <text id="lower-19-num" data-name="19" class="cls-1 teeth_lower" transform="translate(329.927 426.297) scale(1.199)">
                                                    <tspan x="-35" y="-35">19</tspan>
                                                </text>

                                                <!--  image with cliap-3 -->
                                                <image id="lower-19-claps" data-name="19" x="155" y="255" width="100" height="191" xlink:href="{{asset('admin/assets/images/teeth/dc-19.png')}}" class=" clap-19 teeth_lower lower-claps" style="
                                            @if (in_array('19', $lower_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif
                                            
                                        "></image>
                                                <!-- lower-teeth 3 end-->


                                                <g></g>
                                                <!-- lower-teeth 4 start-->
                                                <a href="javascript:;">
                                                    <image id="lower-20-teeth" data-id="teeth5" data-name="20" x="265" y="225" width="167" height="159" xlink:href="
                                                    
                                                    @if (in_array('20', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-20.png')}}
                                                    @endif
                                                    @if (in_array('20', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-20.png')}}
                                                    @endif
                                                    @if (in_array('20', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-20.png')}}
                                                    @endif
                                                    @if (in_array('20', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-20.png')}}
                                                    @endif
                                                    @if (in_array('20', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-20.png')}}
                                                    @endif

                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>

                                                <text id="lower-20-num" data-name="20" class="cls-1 teeth_lower" transform="translate(401.061 332.076) scale(1.199)">
                                                    <tspan x="-30" y="-35">20</tspan>
                                                </text>

                                                <!--  image with cliap-4 -->
                                                <image id="lower-20-claps" data-name="20" x="240" y="175" width="100" height="159" xlink:href="{{asset('admin/assets/images/teeth/dc-20.png')}}" class=" clap-20 teeth_lower lower-claps" style="
                                            @if (in_array('20', $lower_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif

                                            
                                        "></image>
                                                <!-- lower-teeth 4 end-->

                                                <g></g>
                                                <!-- lower-teeth 5 -->
                                                <a href="javascript:;">
                                                    <image id="lower-21-teeth" data-id="teeth3" data-name="21" x="342" y="153" width="150" height="177" xlink:href="
                                                    
                                                    @if (in_array('21', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-21.png')}}
                                                    @endif
                                                    @if (in_array('21', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-21.png')}}
                                                    @endif
                                                    @if (in_array('21', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-21.png')}}
                                                    @endif
                                                    @if (in_array('21', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-21.png')}}
                                                    @endif
                                                    @if (in_array('21', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-21.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-5 -->
                                                <image id="lower-21-claps" data-name="21" x="300" y="135" width="140" height="90" xlink:href="{{asset('admin/assets/images/teeth/dc-21.png')}}" class=" clap-21 teeth_lower lower-claps" style="

                                                @if (in_array('21', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-21-num" data-name="21" class="cls-1 teeth_lower" transform="translate(455.319 283.973) scale(1.199)">
                                                    <tspan x="-30" y="-50">21</tspan>
                                                </text>

                                                <g></g>
                                                <!-- lower-teeth 6 -->
                                                <a href="javascript:;">
                                                    <image id="lower-22-teeth" data-id="teeth4" data-name="22" x="435" y="93" width="135" height="200" xlink:href="

                                                    @if (in_array('22', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-22.png')}}
                                                    @endif
                                                    @if (in_array('22', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-22.png')}}
                                                    @endif
                                                    @if (in_array('22', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-22.png')}}
                                                    @endif
                                                    @if (in_array('22', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-22.png')}}
                                                    @endif
                                                    @if (in_array('22', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-22.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>

                                                <!--  image with cliap-22 -->
                                                <image id="lower-22-claps" data-name="22" x="430" y="15" width="100" height="200" xlink:href="{{asset('admin/assets/images/teeth/dc-22.png')}}" class=" clap-22 teeth_lower lower-claps" style="
                                                
                                        @if (in_array('22', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="lower-22-num" data-name="22" class="cls-1 teeth_lower" transform="translate(517.12 257.438) scale(1.199)">
                                                    <tspan x="0" y="-50">22</tspan>
                                                </text>

                                                <g></g>
                                                <!-- lower-teeth 7 -->
                                                <a href="javascript:;">
                                                    <image id="lower-23-teeth" data-id="teeth2" data-name="23" x="540" y="60" width="115" height="186" xlink:href="

                                                    @if (in_array('23', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-23.png')}}
                                                    @endif
                                                    @if (in_array('23', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-23.png')}}
                                                    @endif
                                                    @if (in_array('23', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-23.png')}}
                                                    @endif
                                                    @if (in_array('23', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-23.png')}}
                                                    @endif
                                                    @if (in_array('23', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-23.png')}}
                                                    @endif

                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!-- image with cliap-23 -->
                                                <image id="lower-23-claps" data-name="23" x="525" y="-33" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/dc-23.png')}}" class=" clap-23 teeth_lower lower-claps" style="
                                            @if (in_array('23', $lower_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif
                                            
                                            "></image>
                                                <text id="lower-23-num" data-name="23" class="cls-1 teeth_lower" transform="translate(602.467 207.332) scale(1.199)">
                                                    <tspan x="0" y="-50">23</tspan>
                                                </text>


                                                <g></g>

                                                <!-- lower-teeth 8 -->
                                                <a href="javascript:;">
                                                    <image id="lower-24-teeth" data-id="teeth1" data-name="24" x="647" y="40" width="111" height="182" xlink:href="
                                                    

                                                    @if (in_array('24', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-24.png')}}
                                                    @endif
                                                    @if (in_array('24', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-24.png')}}
                                                    @endif
                                                    @if (in_array('24', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-24.png')}}
                                                    @endif
                                                    @if (in_array('24', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-24.png')}}
                                                    @endif
                                                    @if (in_array('24', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-24.png')}}
                                                    @endif

                                                    " class="img-for-teeth teeth_lower "></image>
                                                </a>
                                                <!-- image with cliap-8 -->
                                                <image id="lower-24-claps" data-name="24" x="647" y="-60" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/dc-24.png')}}" class=" clap-24 teeth_lower lower-claps" style="
                                                
                                        @if (in_array('24', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="lower-24-num" data-name="24" class="cls-1 teeth_lower" transform="translate(703.596 190.046) scale(1.199)">
                                                    <tspan x="0" y="-40">24</tspan>
                                                </text>

                                                <g></g>
                                                <!-- lower-teeth 9 -->
                                                <a href="javascript:;">
                                                    <image id="lower-25-teeth" data-id="teeth8" data-name="25" x="760" y="42" width="111" height="182" xlink:href="
                                                    
                                                    @if (in_array('25', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-25.png')}}
                                                    @endif
                                                    @if (in_array('25', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-25.png')}}
                                                    @endif
                                                    @if (in_array('25', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-25.png')}}
                                                    @endif
                                                    @if (in_array('25', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-25.png')}}
                                                    @endif
                                                    @if (in_array('25', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-25.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-9 -->
                                                <image id="lower-25-claps" data-name="25" x="762" y="-58" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/dc-25.png')}}" class=" clap-25 teeth_lower lower-claps" style="
                                                
                                            @if (in_array('25', $lower_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif
                                            
                                            "></image>
                                                <text id="lower-25-num" data-name="25" class="cls-1 teeth_lower" transform="translate(789.005 190.046) scale(1.199)">
                                                    <tspan x="10" y="-50">25</tspan>
                                                </text>
                                                <g></g>
                                                <!-- lower-teeth 10 -->
                                                <a href="javascript:;">
                                                    <image id="lower-26-teeth" data-id="teeth9" data-name="26" x="865" y="54" width="115" height="186" xlink:href="
                                                    
                                                    @if (in_array('26', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-26.png')}}
                                                    @endif
                                                    @if (in_array('26', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-26.png')}}
                                                    @endif
                                                    @if (in_array('26', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-26.png')}}
                                                    @endif
                                                    @if (in_array('26', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-26.png')}}
                                                    @endif
                                                    @if (in_array('26', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-26.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-11 -->
                                                <image id="lower-26-claps" data-name="26" x="880" y="-35" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/dc-26.png')}}" class=" clap-26 teeth_lower lower-claps" style="

                                                @if (in_array('26', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-26-num" data-name="26" class="cls-1 teeth_lower" transform="translate(881.008 207.152) scale(1.199)">
                                                    <tspan x="25" y="-50">26</tspan>
                                                </text>
                                                <!-- lower-teeth 11-->
                                                <a href="javascript:;">
                                                    <image id="lower-27-teeth" data-id="teeth11" data-name="27" x="955" y="80" width="135" height="200" xlink:href="
                                                    
                                                    @if (in_array('27', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-27.png')}}
                                                    @endif
                                                    @if (in_array('27', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-27.png')}}
                                                    @endif
                                                    @if (in_array('27', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-27.png')}}
                                                    @endif
                                                    @if (in_array('27', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-27.png')}}
                                                    @endif
                                                    @if (in_array('27', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-27.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-11 -->
                                                <image id="lower-27-claps" data-name="27" x="990" y="-5" width="110" height="200" xlink:href="{{asset('admin/assets/images/teeth/dc-27.png')}}" class=" clap-27 teeth_lower lower-claps" style="

                                        @if (in_array('27', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="lower-27-num" data-name="27" class="cls-1 teeth_lower" transform="translate(982.792 240.944) scale(1.199)">
                                                    <tspan x="25" y="-50">27</tspan>
                                                </text>
                                                <!-- lower-teeth 12 -->
                                                <a href="javascript:;">
                                                    <image id="lower-28-teeth" data-id="teeth10" data-name="28" x="1035" y="134" width="150" height="177" xlink:href="
                                                    

                                                    @if (in_array('28', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-28.png')}}
                                                    @endif
                                                    @if (in_array('28', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-28.png')}}
                                                    @endif
                                                    @if (in_array('28', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-28.png')}}
                                                    @endif
                                                    @if (in_array('28', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-28.png')}}
                                                    @endif
                                                    @if (in_array('28', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-28.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-12 -->
                                                <image id="lower-28-claps" data-name="28" x="1100" y="60" width="100" height="177" xlink:href="{{asset('admin/assets/images/teeth/dc-28.png')}}" class=" clap-28 teeth_lower lower-claps" style="
                                                @if (in_array('28', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-28-num" data-name="28" class="cls-1 teeth_lower" transform="translate(1037.236 283.518) scale(1.199)">
                                                    <tspan x="40" y="-50">28</tspan>
                                                </text>
                                                <!-- lower-teeth 13 -->
                                                <a href="javascript:;">
                                                    <image id="lower-29-teeth" data-id="teeth12" data-name="29" x="1098" y="205" width="167" height="159" xlink:href="

                                                    @if (in_array('29', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-29.png')}}
                                                    @endif
                                                    @if (in_array('29', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-29.png')}}
                                                    @endif
                                                    @if (in_array('29', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-29.png')}}
                                                    @endif
                                                    @if (in_array('29', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-29.png')}}
                                                    @endif
                                                    @if (in_array('29', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-29.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-13 -->
                                                <image id="lower-29-claps" data-name="29" x="1190" y="155" width="100" height="159" xlink:href="{{asset('admin/assets/images/teeth/dc-29.png')}}" class=" clap-29 teeth_lower lower-claps" style="
                                        @if (in_array('29', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        "></image>
                                                <text id="lower-29-num" data-name="29" class="cls-1 teeth_lower" transform="translate(1097.072 329.95) scale(1.199)">
                                                    <tspan x="45" y="-50">29</tspan>
                                                </text>
                                                <!-- lower-teeth 14 -->
                                                <a href="javascript:;">
                                                    <image id="lower-30-teeth" data-id="teeth13" data-name="30" x="1130" y="278" width="234" height="191" xlink:href="
                                                    
                                                    @if (in_array('30', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-30.png')}}
                                                    @endif
                                                    @if (in_array('30', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-30.png')}}
                                                    @endif
                                                    @if (in_array('30', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-30.png')}}
                                                    @endif
                                                    @if (in_array('30', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-30.png')}}
                                                    @endif
                                                    @if (in_array('30', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-30.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-14 -->
                                                <image id="lower-30-claps" data-name="30" x="1280" y="258" width="100" height="191" xlink:href="{{asset('admin/assets/images/teeth/dc-30.png')}}" class=" clap-30 teeth_lower lower-claps" style="
                                        @if (in_array('30', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="lower-30-num" data-name="30" class="cls-1 teeth_lower" transform="translate(1164.707 409.172) scale(1.199)">
                                                    <tspan x="45" y="-45">30</tspan>
                                                </text>
                                                <!-- lower-teeth 15 -->

                                                <a href="javascript:;">
                                                    <image id="lower-31-teeth" data-id="teeth14" data-name="31" x="1210" y="395" width="232" height="194" xlink:href="
                                                    
                                                    @if (in_array('31', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-31.png')}}
                                                    @endif
                                                    @if (in_array('31', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-31.png')}}
                                                    @endif
                                                    @if (in_array('31', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-31.png')}}
                                                    @endif
                                                    @if (in_array('31', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-31.png')}}
                                                    @endif
                                                    @if (in_array('31', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-31.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-15 -->
                                                <image id="lower-31-claps" data-name="31" x="1390" y="420" width="80" height="130" xlink:href="{{asset('admin/assets/images/teeth/dc-30.png')}}" class=" clap-31 teeth_lower lower-claps" style="
                                        @if (in_array('31', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        "></image>
                                                <text id="lower-31-num" data-name="31" class="cls-1 teeth_lower" transform="translate(1238.134 502.8) scale(1.199)">
                                                    <tspan x="50">31</tspan>
                                                </text>
                                                <!-- lower-teeth 16 -->

                                                <a href="javascript:;">
                                                    <image id="lower-32-teeth" data-id="teeth16" data-name="32" x="1273" y="560" width="232" height="193" xlink:href="

                                                    @if (in_array('32', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-32.png')}}
                                                    @endif
                                                    @if (in_array('32', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-32.png')}}
                                                    @endif
                                                    @if (in_array('32', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-32.png')}}
                                                    @endif
                                                    @if (in_array('32', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-32.png')}}
                                                    @endif
                                                    @if (in_array('32', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-32.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-32 -->
                                                <image id="lower-32-claps" data-name="32" x="1440" y="580" width="114" height="92" xlink:href="{{asset('admin/assets/images/teeth/dc-32.png')}}" class=" clap-32 teeth_lower lower-claps" style="
                                                
                                            @if (in_array('32', $lower_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif
                                            "></image>
                                                <text id="lower-32-num" data-name="32" class="cls-1 teeth_lower" transform="translate(1268.177 618.27) scale(1.199)">
                                                    <tspan x="80" y="20">32</tspan>
                                                </text>

                                                <!--lower teeth numbers ------------------------------ -->

                                            </g>

                                        </svg>

                                        <div class="d-block text-center teeth-text">
                                            <a href="javascript:;" class="add-btn p-2 mx-auto lower_mising_all d-none">
                                                Missing all teeth
                                            </a>
                                        </div>
                                        <div class="teeths-wrapper second-row p-0">
                                            <div class="row">
                                                <div class="col-md-12 col-xl-12 col-lg-12">
                                                    <div class="row gy-2">

                                                        @if(!empty($lower_vTeethInMouth))
                                                        @php
                                                        $parts=array_filter($lower_vTeethInMouth);
                                                        $lower_vTeethInMouth = (implode(",",$parts));
                                                        @endphp
                                                        @endif

                                                        @if(!empty($lower_vTeethInMouth))
                                                        <a href="javascript:;">
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <label class="form-control inner-label te-in-month lower_lbl_btn_all" value="" id="lower_in_mouth" disabled> Teeth in mouth <br> <span id="lower_in_mouth_lbl">{{$lower_vTeethInMouth}}</span></label>
                                                                    <input type="hidden" id="lower_in_mouth_value" value="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                        @endif


                                                        @if (!empty(array_filter($lower_vMissingTeeth)))
                                                        @php
                                                        $parts=array_filter($lower_vMissingTeeth);
                                                        $lower_vMissingTeeth = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lower_lbl_btn_all" value="" id="lower_missing_teeth" disabled>missing teeth <br> <span id="lower_missing_teeth_lbl">{{$lower_vMissingTeeth}}</span></label>
                                                                <input type="hidden" id="lower_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($lower_vMissingTeethOp))
                                                        @if (!empty(array_filter($lower_vMissingTeethOp)))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lower_lbl_btn_all" value="" id="lower_missing_teeth" disabled>missing teeth <br> <span id="lower_missing_teeth_lbl">{{$lower_vMissingTeeth}}</span></label>
                                                                <input type="hidden" id="lower_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif


                                                        @if (!empty(array_filter($lower_vWillExtractOnDelivery)))
                                                        @php
                                                        $parts=array_filter($lower_vWillExtractOnDelivery);
                                                        $lower_vWillExtractOnDelivery = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lower_lbl_btn_all" value="" id="lower_ectract_delivery" disabled>will extract on delivery <br><span id="lower_ectract_delivery_lbl">{{$lower_vWillExtractOnDelivery}}</span></label>
                                                                <input type="hidden" id="lower_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($lower_vWillExtractOnDeliveryOp))
                                                        @if (!empty(array_filter($lower_vWillExtractOnDeliveryOp)))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lower_lbl_btn_all" value="" id="lower_ectract_delivery" disabled>will extract on delivery <br><span id="lower_ectract_delivery_lbl">{{$lower_product->vWillExtractOnDeliveryOp}}</span></label>
                                                                <input type="hidden" id="lower_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif



                                                        @if (!empty(array_filter($lower_vHasBeenExtracted)))
                                                        @php
                                                        $parts=array_filter($lower_vHasBeenExtracted);
                                                        $lower_vHasBeenExtracted = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lower_lbl_btn_all" value="" id="lower_been_extracted" disabled> has been extracted <br><span id="lower_been_extracted_lbl">{{$lower_vHasBeenExtracted}}</span></label>
                                                                <input type="hidden" id="lower_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($lower_vHasBeenExtractedOp))
                                                        @if (!empty(array_filter($lower_vHasBeenExtractedOp)))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lower_lbl_btn_all" value="" id="lower_been_extracted" disabled> has been extracted <br><span id="lower_been_extracted_lbl">{{$lower_product->vHasBeenExtractedOp}}</span></label>
                                                                <input type="hidden" id="lower_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif



                                                        @if (!empty(array_filter($lower_vFixOrAdd)))
                                                        @php
                                                        $parts=array_filter($lower_vFixOrAdd);
                                                        $lower_vFixOrAdd = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lower_lbl_btn_all" value="" id="lower_fix" disabled> fix or add <br> <span id="lower_fix_lbl">{{$lower_vFixOrAdd}}</span></label>
                                                                <input type="hidden" id="lower_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($lower_vFixOrAddOp))
                                                        @if (!empty(array_filter($lower_vFixOrAddOp)))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lower_lbl_btn_all" value="" id="lower_fix" disabled> fix or add <br> <span id="lower_fix_lbl">{{$lower_product->vFixOrAddOp}}</span></label>
                                                                <input type="hidden" id="lower_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif


                                                        @if (!empty(array_filter($lower_vClasps)))
                                                        @php
                                                        $parts=array_filter($lower_vClasps);
                                                        $lower_vClasps = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lower_lbl_btn_all" value="" id="lower_clasps" disabled>clasps<br> <span id="lower_clasps_lbl">{{$lower_vClasps}}</span></label>
                                                                <input type="hidden" id="lower_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($lower_vFixOrAddOp))
                                                        @if (!empty(array_filter($lower_vClasps)))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lower_lbl_btn_all" value="" id="lower_clasps" disabled>clasps<br> <span id="lower_clasps_lbl">{{$lower_product->vClaspsOp}}</span></label>
                                                                <input type="hidden" id="lower_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </td>

                            </tr>
                        </tbody>
                    </table>



                    <!-- view Lower teeth end -->









                    @if(empty($Slip->tStagesNotes))
                    @php
                    $tStagesNotes = "";
                    @endphp
                    @else
                    @php
                    $tStagesNotes = $Slip->tStagesNotes;
                    @endphp
                    @endif



                    <div class="col-12 text-center mb-1">
                       
                        <div id="nextStage"></div>
                    </div>

                    <!-- <div class="col-lg-12 stagenote-text-area " id="notes_div">
                        <div class="stage-title-bold" style="background-color:#eff2f5 ;">

                            <div class="d-flex justify-content-center align-items-center">
                                <a href="javascript:;" id="edit_stage" class="btn add-btn p-0 w-auto me-2" title="Add Notes" style="padding: 0px!important;                            
                                display: flex;
                                justify-content: center;
                                align-items: center;
                                height: 30px;
                                min-width: 30px!important;
                                border-radius: 5px;
                                margin-right: 5px;
                                -webkit-border-radius: 5px;
                                border: 1px solid #a1a5b754!important;font-size: 14px;
                                color: #a1a5b7;background: transparent;"> 

                                <i class="fad fa-pencil" style="color: unset;"></i> </a>
                                <h5 class="card-title text-start">
                                    Stage Notes
                                    </h3>
                                    <a href="javascript:;" title="Attachments" id="image_popup" class="btn add-btn p-0 ms-2" style="padding: 0px!important;                            
                                    display: flex;
                                    justify-content: center;
                                    align-items: center;
                                    height: 30px;
                                    min-width: 30px!important;
                                    border-radius: 5px;
                                    margin-right: 5px;
                                    -webkit-border-radius: 5px;
                                    border: 1px solid #a1a5b754!important;font-size: 14px;
                                    color: #a1a5b7;background: transparent;"> <i class="fad fa-link" style="color: unset;"></i> </a>
                                    {{-- <ul class="note-btn-group ms-5 ps-5">
                <li>
                    <a href="javascript:;" id="free_drawing_open">
                        <i class="fad fa-pencil"></i>
                    </a>
                </li>
            </ul> --}}

                            </div>
                        </div>
                        <div id="toolbar-container"></div>
                        <div id="tDescription_Reff">
                            <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Stage notes" disabled=""></textarea>
                        </div>
                    </div> -->



           <div class="col-lg-12 stagenote-text-area " id="notes_div">
                        <div class="stage-title-bold" style="background-color:#fff ;">

                            <div class="d-flex justify-content-center align-items-center">
                               
                                <a href="javascript:;" id="edit_stage" class="btn add-btn p-0 w-auto me-2" title="Add Notes" style="padding: 0px!important;                            
                                display: flex;
                                justify-content: center;
                                align-items: center;
                                height: 30px;
                                min-width: 30px!important;
                                border-radius: 5px;
                                margin-right: 5px;
                                -webkit-border-radius: 5px;
                                border: 1px solid #a1a5b754!important;font-size: 14px;
                                color: #a1a5b7;background: transparent;"> 
                                <i class="fad fa-pencil" style="color: #0E66B2"></i> </a>
                              
                                <h5 class="card-title text-start">
                                    Stage Notes
                                </h5>
                                    <a href="javascript:;" title="Attachments" id="image_popup" class="btn add-btn p-0 ms-2" style="padding: 0px!important;                            
                                    display: flex;
                                    justify-content: center;
                                    align-items: center;
                                    height: 30px;
                                    min-width: 30px!important;
                                    border-radius: 5px;
                                    margin-right: 5px;
                                    -webkit-border-radius: 5px;
                                    border: 1px solid #a1a5b754!important;font-size: 14px;
                                    color: #a1a5b7;background: transparent;"> <i class="fad fa-link" style="color: #0E66B2"></i> </a>
                                    @if(isset($gallary_image_exist) && count($gallary_image_exist))
                                    <a href="javascript:;" title="Attachments" id="image_popup" class="btn add-btn p-0 ms-2" style="padding: 0px!important;                            
                                    display: flex;
                                    justify-content: center;
                                    align-items: center;
                                    height: 30px;
                                    min-width: 30px!important;
                                    border-radius: 5px;
                                    margin-right: 5px;
                                    -webkit-border-radius: 5px;
                                    border: 1px solid #a1a5b754!important;font-size: 14px;
                                    color: #a1a5b7;background: transparent;"><i class="fas fa-images" style="color: #0E66B2"></i></a>
                                    @endif
                                    {{-- <ul class="note-btn-group ms-5 ps-5">
                <li>
                    <a href="javascript:;" id="free_drawing_open">
                        <i class="fad fa-pencil"></i>
                    </a>
                </li>
            </ul> --}}

                            </div>
                        </div>
                        <div id="toolbar-container"></div>
                        <div id="tDescription_Reff">
                            <div class="form-control" style="height: auto;    max-height: 150px; background-color: white;    overflow-y: auto;" id="tDescription" name="tDescription" placeholder="Stage notes"></div>
                        </div>
                    </div>

                    <div style="width:100%;padding:0;">
                        <table class="tslip ">
                        <!-- bg-g-blue -->
                            <tbody>
                                <tr>
                                   
                                    @if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == '6')
                                        <td style="border:1px solid transparent;">
                                            <a href="{{route('admin.labcase.office_admin_listing')}}" class="btn back-btn red-btn">
                                                Close slip
                                            </a>
                                        </td>
                                    @else
                                        <td style="border:1px solid transparent;">
                                            <a href="{{route('admin.labcase')}}" class="btn back-btn red-btn">
                                                Close slip
                                            </a>
                                        </td>
                                    @endif
                                    

                                    <!-- New Stage Start -->
                                 
                                    @if(isset($eNewStageAccess) && $eNewStageAccess =='Yes' && $Slip->vLocation =='In office' )
                                    <td style="border:1px solid transparent;" id="new_stage_visible">
                                        @if((isset($Case->eStatus) && $Case->eStatus == 'On Process') && ((isset($lower_product->eStatus) && $lower_product->eStatus =='Finished') || (isset($upper_product->eStatus) && $upper_product->eStatus =='Finished')) && (isset($Slip->vLocation) && $Slip->vLocation =='In office'))
                                        @if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == '1')
                                        {{-- <a href="{{route('admin.labcase.new_stage_lab',[$Case->iCaseId,$Slip->iSlipId])}}" 
                                            class="btn back-btn gray-btn mb-1">New stage</a>. --}}
                                            {{-- <img src="{{asset('admin/assets/images/new_icons/New stage.svg')}}" alt="Pick up"> --}}
                                            <a href="{{route('admin.labcase.new_stage_lab',[$Case->iCaseId,$Slip->iSlipId])}}" title="New stage" class="">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.75 34.62" style="fill: #0E66B2;height: 30px;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M6.72,29c.82.78,1.64,1.56,2.47,2.33l2.32,2.15a1.14,1.14,0,0,1,.37.49.67.67,0,0,1-.15.55,1,1,0,0,1-.57.08c-.1,0-.19-.14-.28-.22L4.77,28.78a6.53,6.53,0,0,1-.51-.62,8.28,8.28,0,0,1,.63-.75l5.76-5.34.14-.11c.44-.35.81-.39,1-.09s0,.67-.25.93l-4.31,4c-.2.18-.43.33-.68.53.19.13.25.22.32.22l13.54,0c1,0,2,0,3,0A10.06,10.06,0,0,0,33.5,18.22a5.35,5.35,0,0,1,.19-1.06.63.63,0,0,1,.47-.33.6.6,0,0,1,.47.31,2.67,2.67,0,0,1,.12,1,11.27,11.27,0,0,1-9.39,10.45,18.27,18.27,0,0,1-3,.23c-4.88,0-9.75,0-14.63,0h-1Z"/><path d="M28,5.67c-.61-.58-1.21-1.16-1.82-1.72l-2.8-2.56A3.5,3.5,0,0,1,23,1,.57.57,0,0,1,23,.2c.29-.31.6-.22.88,0l1.75,1.57,4.38,4a7.35,7.35,0,0,1,.55.67c-.33.35-.59.67-.9,1-1.86,1.71-3.73,3.4-5.6,5.1a1.8,1.8,0,0,1-.2.19c-.26.22-.54.45-.85.11a.6.6,0,0,1,.1-.89c.82-.8,1.65-1.58,2.48-2.36S27.25,8,28.2,7.13c-.34,0-.54,0-.75,0C22,7.08,16.5,7,11,7.11a10,10,0,0,0-9.66,8.51c-.07.48-.13,1-.18,1.43s-.12.75-.58.79S0,17.53,0,17A11.12,11.12,0,0,1,.83,13a11.16,11.16,0,0,1,9-7,13.2,13.2,0,0,1,2-.07H28Z"/><path d="M16.39,18.22h-2c-.29,0-.52-.06-.54-.39s.17-.42.55-.42c1.35,0,2.69,0,4,0,.3,0,.39-.09.39-.39,0-1.35,0-2.69,0-4a.72.72,0,0,1,.22-.48c.28-.18.58,0,.59.42,0,.57,0,1.13,0,1.69,0,.81,0,1.62,0,2.42,0,.31.09.38.39.38,1.3,0,2.6,0,3.9,0h.31c.25,0,.4.21.34.43a.56.56,0,0,1-.37.35,7.62,7.62,0,0,1-1.07,0c-1,0-2.07,0-3.11,0-.29,0-.4.08-.39.39,0,1.29,0,2.58,0,3.87a1.63,1.63,0,0,1,0,.31c0,.24-.14.42-.41.42s-.4-.19-.4-.46V19.67c0-.34,0-.67,0-1s-.08-.47-.44-.46C17.71,18.24,17.05,18.22,16.39,18.22Z"/></g></g></svg>
                                            </a>
                                        @else
                                            <a title="New stage" href="{{route('admin.labcase.new_stage',[$Case->iCaseId,$Slip->iSlipId])}}" class="">
                                                {{-- btn back-btn gray-btn mb-1 --}}
                                                <!-- New stage -->
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.75 34.62" style="fill: #0E66B2;height: 30px;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M6.72,29c.82.78,1.64,1.56,2.47,2.33l2.32,2.15a1.14,1.14,0,0,1,.37.49.67.67,0,0,1-.15.55,1,1,0,0,1-.57.08c-.1,0-.19-.14-.28-.22L4.77,28.78a6.53,6.53,0,0,1-.51-.62,8.28,8.28,0,0,1,.63-.75l5.76-5.34.14-.11c.44-.35.81-.39,1-.09s0,.67-.25.93l-4.31,4c-.2.18-.43.33-.68.53.19.13.25.22.32.22l13.54,0c1,0,2,0,3,0A10.06,10.06,0,0,0,33.5,18.22a5.35,5.35,0,0,1,.19-1.06.63.63,0,0,1,.47-.33.6.6,0,0,1,.47.31,2.67,2.67,0,0,1,.12,1,11.27,11.27,0,0,1-9.39,10.45,18.27,18.27,0,0,1-3,.23c-4.88,0-9.75,0-14.63,0h-1Z"/><path d="M28,5.67c-.61-.58-1.21-1.16-1.82-1.72l-2.8-2.56A3.5,3.5,0,0,1,23,1,.57.57,0,0,1,23,.2c.29-.31.6-.22.88,0l1.75,1.57,4.38,4a7.35,7.35,0,0,1,.55.67c-.33.35-.59.67-.9,1-1.86,1.71-3.73,3.4-5.6,5.1a1.8,1.8,0,0,1-.2.19c-.26.22-.54.45-.85.11a.6.6,0,0,1,.1-.89c.82-.8,1.65-1.58,2.48-2.36S27.25,8,28.2,7.13c-.34,0-.54,0-.75,0C22,7.08,16.5,7,11,7.11a10,10,0,0,0-9.66,8.51c-.07.48-.13,1-.18,1.43s-.12.75-.58.79S0,17.53,0,17A11.12,11.12,0,0,1,.83,13a11.16,11.16,0,0,1,9-7,13.2,13.2,0,0,1,2-.07H28Z"/><path d="M16.39,18.22h-2c-.29,0-.52-.06-.54-.39s.17-.42.55-.42c1.35,0,2.69,0,4,0,.3,0,.39-.09.39-.39,0-1.35,0-2.69,0-4a.72.72,0,0,1,.22-.48c.28-.18.58,0,.59.42,0,.57,0,1.13,0,1.69,0,.81,0,1.62,0,2.42,0,.31.09.38.39.38,1.3,0,2.6,0,3.9,0h.31c.25,0,.4.21.34.43a.56.56,0,0,1-.37.35,7.62,7.62,0,0,1-1.07,0c-1,0-2.07,0-3.11,0-.29,0-.4.08-.39.39,0,1.29,0,2.58,0,3.87a1.63,1.63,0,0,1,0,.31c0,.24-.14.42-.41.42s-.4-.19-.4-.46V19.67c0-.34,0-.67,0-1s-.08-.47-.44-.46C17.71,18.24,17.05,18.22,16.39,18.22Z"/></g></g></svg>
                                            </a>
                                        @endif
                                        @else
                                        <a title="New stage" href="#" class="btn back-btn gray-btn mb-1 invisible">
                                            <!-- New stage -->
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.75 34.62" style="fill: #0E66B2;height: 30px;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M6.72,29c.82.78,1.64,1.56,2.47,2.33l2.32,2.15a1.14,1.14,0,0,1,.37.49.67.67,0,0,1-.15.55,1,1,0,0,1-.57.08c-.1,0-.19-.14-.28-.22L4.77,28.78a6.53,6.53,0,0,1-.51-.62,8.28,8.28,0,0,1,.63-.75l5.76-5.34.14-.11c.44-.35.81-.39,1-.09s0,.67-.25.93l-4.31,4c-.2.18-.43.33-.68.53.19.13.25.22.32.22l13.54,0c1,0,2,0,3,0A10.06,10.06,0,0,0,33.5,18.22a5.35,5.35,0,0,1,.19-1.06.63.63,0,0,1,.47-.33.6.6,0,0,1,.47.31,2.67,2.67,0,0,1,.12,1,11.27,11.27,0,0,1-9.39,10.45,18.27,18.27,0,0,1-3,.23c-4.88,0-9.75,0-14.63,0h-1Z"/><path d="M28,5.67c-.61-.58-1.21-1.16-1.82-1.72l-2.8-2.56A3.5,3.5,0,0,1,23,1,.57.57,0,0,1,23,.2c.29-.31.6-.22.88,0l1.75,1.57,4.38,4a7.35,7.35,0,0,1,.55.67c-.33.35-.59.67-.9,1-1.86,1.71-3.73,3.4-5.6,5.1a1.8,1.8,0,0,1-.2.19c-.26.22-.54.45-.85.11a.6.6,0,0,1,.1-.89c.82-.8,1.65-1.58,2.48-2.36S27.25,8,28.2,7.13c-.34,0-.54,0-.75,0C22,7.08,16.5,7,11,7.11a10,10,0,0,0-9.66,8.51c-.07.48-.13,1-.18,1.43s-.12.75-.58.79S0,17.53,0,17A11.12,11.12,0,0,1,.83,13a11.16,11.16,0,0,1,9-7,13.2,13.2,0,0,1,2-.07H28Z"/><path d="M16.39,18.22h-2c-.29,0-.52-.06-.54-.39s.17-.42.55-.42c1.35,0,2.69,0,4,0,.3,0,.39-.09.39-.39,0-1.35,0-2.69,0-4a.72.72,0,0,1,.22-.48c.28-.18.58,0,.59.42,0,.57,0,1.13,0,1.69,0,.81,0,1.62,0,2.42,0,.31.09.38.39.38,1.3,0,2.6,0,3.9,0h.31c.25,0,.4.21.34.43a.56.56,0,0,1-.37.35,7.62,7.62,0,0,1-1.07,0c-1,0-2.07,0-3.11,0-.29,0-.4.08-.39.39,0,1.29,0,2.58,0,3.87a1.63,1.63,0,0,1,0,.31c0,.24-.14.42-.41.42s-.4-.19-.4-.46V19.67c0-.34,0-.67,0-1s-.08-.47-.44-.46C17.71,18.24,17.05,18.22,16.39,18.22Z"/></g></g></svg>
                                        </a>
                                        @endif
                                    </td>
                                    @else
                                    <td style="border:1px solid transparent;" class="text-center">
                                        <a href="#" class="invisible mb-1 " title="New stage" >
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.75 34.62" style="fill: #0E66B2;height: 30px;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M6.72,29c.82.78,1.64,1.56,2.47,2.33l2.32,2.15a1.14,1.14,0,0,1,.37.49.67.67,0,0,1-.15.55,1,1,0,0,1-.57.08c-.1,0-.19-.14-.28-.22L4.77,28.78a6.53,6.53,0,0,1-.51-.62,8.28,8.28,0,0,1,.63-.75l5.76-5.34.14-.11c.44-.35.81-.39,1-.09s0,.67-.25.93l-4.31,4c-.2.18-.43.33-.68.53.19.13.25.22.32.22l13.54,0c1,0,2,0,3,0A10.06,10.06,0,0,0,33.5,18.22a5.35,5.35,0,0,1,.19-1.06.63.63,0,0,1,.47-.33.6.6,0,0,1,.47.31,2.67,2.67,0,0,1,.12,1,11.27,11.27,0,0,1-9.39,10.45,18.27,18.27,0,0,1-3,.23c-4.88,0-9.75,0-14.63,0h-1Z"/><path d="M28,5.67c-.61-.58-1.21-1.16-1.82-1.72l-2.8-2.56A3.5,3.5,0,0,1,23,1,.57.57,0,0,1,23,.2c.29-.31.6-.22.88,0l1.75,1.57,4.38,4a7.35,7.35,0,0,1,.55.67c-.33.35-.59.67-.9,1-1.86,1.71-3.73,3.4-5.6,5.1a1.8,1.8,0,0,1-.2.19c-.26.22-.54.45-.85.11a.6.6,0,0,1,.1-.89c.82-.8,1.65-1.58,2.48-2.36S27.25,8,28.2,7.13c-.34,0-.54,0-.75,0C22,7.08,16.5,7,11,7.11a10,10,0,0,0-9.66,8.51c-.07.48-.13,1-.18,1.43s-.12.75-.58.79S0,17.53,0,17A11.12,11.12,0,0,1,.83,13a11.16,11.16,0,0,1,9-7,13.2,13.2,0,0,1,2-.07H28Z"/><path d="M16.39,18.22h-2c-.29,0-.52-.06-.54-.39s.17-.42.55-.42c1.35,0,2.69,0,4,0,.3,0,.39-.09.39-.39,0-1.35,0-2.69,0-4a.72.72,0,0,1,.22-.48c.28-.18.58,0,.59.42,0,.57,0,1.13,0,1.69,0,.81,0,1.62,0,2.42,0,.31.09.38.39.38,1.3,0,2.6,0,3.9,0h.31c.25,0,.4.21.34.43a.56.56,0,0,1-.37.35,7.62,7.62,0,0,1-1.07,0c-1,0-2.07,0-3.11,0-.29,0-.4.08-.39.39,0,1.29,0,2.58,0,3.87a1.63,1.63,0,0,1,0,.31c0,.24-.14.42-.41.42s-.4-.19-.4-.46V19.67c0-.34,0-.67,0-1s-.08-.47-.44-.46C17.71,18.24,17.05,18.22,16.39,18.22Z"/></g></g></svg>
                                            <!-- New Stage -->
                                        </a>
                                    </td>
                                    @endif
                                    <!-- New Stage End -->


                                    <!-- Edit Stage Start -->
                                    @if((isset($eEditStageAccess) && $eEditStageAccess =='Yes' && ($Slip->vLocation =='On route to the lab' || $Slip->vLocation =='In lab')) )
                                    {{-- || (\App\Libraries\General::admin_info()['iCustomerTypeId'] == '1' --}}
                                        @if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == '1')
                                        <td style="border:1px solid transparent; ">
                                            <a href="{{route('admin.labcase.edit_stage_lab',[$Case->iCaseId,$Slip->iSlipId])}}" class="" title="Edit stage"> 
                                                <!-- Edit stage -->
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.75 34.62" style="&#10;    fill: #0E66B2;&#10;    height: 30px;&#10;"><defs><style>.cls-1{stroke:#000;stroke-width:0.3px;}</style></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M6.72,29c.82.78,1.64,1.56,2.47,2.33l2.32,2.15a1.14,1.14,0,0,1,.37.49.67.67,0,0,1-.15.55,1,1,0,0,1-.57.08c-.1,0-.19-.14-.28-.22L4.77,28.78a6.53,6.53,0,0,1-.51-.62,8.28,8.28,0,0,1,.63-.75l5.76-5.34.14-.11c.44-.35.81-.39,1-.09s0,.67-.25.93l-4.31,4c-.2.18-.43.33-.68.53.19.13.25.22.32.22l13.54,0c1,0,2,0,3,0A10.06,10.06,0,0,0,33.5,18.22a5.35,5.35,0,0,1,.19-1.06.63.63,0,0,1,.47-.33.6.6,0,0,1,.47.31,2.67,2.67,0,0,1,.12,1,11.27,11.27,0,0,1-9.39,10.45,18.27,18.27,0,0,1-3,.23c-4.88,0-9.75,0-14.63,0h-1Z"/><path d="M28,5.67c-.61-.58-1.21-1.16-1.82-1.72l-2.8-2.56A3.5,3.5,0,0,1,23,1,.57.57,0,0,1,23,.2c.29-.31.6-.22.88,0l1.75,1.57,4.38,4a7.35,7.35,0,0,1,.55.67c-.33.35-.59.67-.9,1-1.86,1.71-3.73,3.4-5.6,5.1a1.8,1.8,0,0,1-.2.19c-.26.22-.54.45-.85.11a.6.6,0,0,1,.1-.89c.82-.8,1.65-1.58,2.48-2.36S27.25,8,28.2,7.13c-.34,0-.54,0-.75,0C22,7.08,16.5,7,11,7.11a10,10,0,0,0-9.66,8.51c-.07.48-.13,1-.18,1.43s-.12.75-.58.79S0,17.53,0,17A11.12,11.12,0,0,1,.83,13a11.16,11.16,0,0,1,9-7,13.2,13.2,0,0,1,2-.07H28Z"/><path d="M11.81,9.94a1.68,1.68,0,0,1,1.26.43l4.75,3.77c1.85,1.47,3.7,2.95,5.57,4.39a4.6,4.6,0,0,1,1.54,2c.39,1,.82,1.92,1.22,2.88.05.13.1.32,0,.41s-.26.08-.39.06l-3.57-.61a3.55,3.55,0,0,1-1.61-.74c-3.52-2.8-7-5.61-10.54-8.4a1.55,1.55,0,0,1-.28-2.37c.36-.41.68-.85,1-1.27A1.6,1.6,0,0,1,11.81,9.94Zm1,5.86L13,16,21,22.28a3.24,3.24,0,0,0,1.48.65l2.56.43.66.07c-.08-.21-.13-.35-.19-.48-.34-.8-.69-1.59-1-2.4a3.44,3.44,0,0,0-1.08-1.45L15.6,12.92l-.3-.21Zm2.05-3.48c-.79-.61-1.6-1.26-2.45-1.86a1,1,0,0,0-1.18.26c-.3.35-.56.74-.87,1.08-1,1.06-.65,1.68.27,2.33.6.42,1.17.9,1.72,1.32Z" style="&#10;    stroke: #0E66B2;&#10;    stroke-width: 0.3px;&#10;"/></g></g></svg>
                                            </a>
                                        </td>
                                        @else
                                            <td style="border:1px solid transparent; vertical-align: bottom">
                                                <a href="{{route('admin.labcase.edit_stage',[$Case->iCaseId,$Slip->iSlipId])}}" title="Edit stage" > 
                                                    <!-- Edit stage -->
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.75 34.62" style="&#10;    fill: #0E66B2;&#10;    height: 30px;&#10;"><defs><style>.cls-1{stroke:#000;stroke-width:0.3px;}</style></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M6.72,29c.82.78,1.64,1.56,2.47,2.33l2.32,2.15a1.14,1.14,0,0,1,.37.49.67.67,0,0,1-.15.55,1,1,0,0,1-.57.08c-.1,0-.19-.14-.28-.22L4.77,28.78a6.53,6.53,0,0,1-.51-.62,8.28,8.28,0,0,1,.63-.75l5.76-5.34.14-.11c.44-.35.81-.39,1-.09s0,.67-.25.93l-4.31,4c-.2.18-.43.33-.68.53.19.13.25.22.32.22l13.54,0c1,0,2,0,3,0A10.06,10.06,0,0,0,33.5,18.22a5.35,5.35,0,0,1,.19-1.06.63.63,0,0,1,.47-.33.6.6,0,0,1,.47.31,2.67,2.67,0,0,1,.12,1,11.27,11.27,0,0,1-9.39,10.45,18.27,18.27,0,0,1-3,.23c-4.88,0-9.75,0-14.63,0h-1Z"/><path d="M28,5.67c-.61-.58-1.21-1.16-1.82-1.72l-2.8-2.56A3.5,3.5,0,0,1,23,1,.57.57,0,0,1,23,.2c.29-.31.6-.22.88,0l1.75,1.57,4.38,4a7.35,7.35,0,0,1,.55.67c-.33.35-.59.67-.9,1-1.86,1.71-3.73,3.4-5.6,5.1a1.8,1.8,0,0,1-.2.19c-.26.22-.54.45-.85.11a.6.6,0,0,1,.1-.89c.82-.8,1.65-1.58,2.48-2.36S27.25,8,28.2,7.13c-.34,0-.54,0-.75,0C22,7.08,16.5,7,11,7.11a10,10,0,0,0-9.66,8.51c-.07.48-.13,1-.18,1.43s-.12.75-.58.79S0,17.53,0,17A11.12,11.12,0,0,1,.83,13a11.16,11.16,0,0,1,9-7,13.2,13.2,0,0,1,2-.07H28Z"/><path d="M11.81,9.94a1.68,1.68,0,0,1,1.26.43l4.75,3.77c1.85,1.47,3.7,2.95,5.57,4.39a4.6,4.6,0,0,1,1.54,2c.39,1,.82,1.92,1.22,2.88.05.13.1.32,0,.41s-.26.08-.39.06l-3.57-.61a3.55,3.55,0,0,1-1.61-.74c-3.52-2.8-7-5.61-10.54-8.4a1.55,1.55,0,0,1-.28-2.37c.36-.41.68-.85,1-1.27A1.6,1.6,0,0,1,11.81,9.94Zm1,5.86L13,16,21,22.28a3.24,3.24,0,0,0,1.48.65l2.56.43.66.07c-.08-.21-.13-.35-.19-.48-.34-.8-.69-1.59-1-2.4a3.44,3.44,0,0,0-1.08-1.45L15.6,12.92l-.3-.21Zm2.05-3.48c-.79-.61-1.6-1.26-2.45-1.86a1,1,0,0,0-1.18.26c-.3.35-.56.74-.87,1.08-1,1.06-.65,1.68.27,2.33.6.42,1.17.9,1.72,1.32Z" style="&#10;    stroke: #0E66B2;&#10;    stroke-width: 0.3px;&#10;"/></g></g></svg>
                                                </a>
                                            </td>
                                        @endif
                                    @else
                                        <td style="border:1px solid transparent;" class ="text-center">
                                            <a href="#" class="invisible " title="Edit stage">
                                            <!-- btn back-btn gray-btn --> 
                                                <!-- Edit stage -->
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.75 34.62" style="&#10;    fill: #0E66B2;&#10;    height: 30px;&#10;"><defs><style>.cls-1{stroke:#000;stroke-width:0.3px;}</style></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M6.72,29c.82.78,1.64,1.56,2.47,2.33l2.32,2.15a1.14,1.14,0,0,1,.37.49.67.67,0,0,1-.15.55,1,1,0,0,1-.57.08c-.1,0-.19-.14-.28-.22L4.77,28.78a6.53,6.53,0,0,1-.51-.62,8.28,8.28,0,0,1,.63-.75l5.76-5.34.14-.11c.44-.35.81-.39,1-.09s0,.67-.25.93l-4.31,4c-.2.18-.43.33-.68.53.19.13.25.22.32.22l13.54,0c1,0,2,0,3,0A10.06,10.06,0,0,0,33.5,18.22a5.35,5.35,0,0,1,.19-1.06.63.63,0,0,1,.47-.33.6.6,0,0,1,.47.31,2.67,2.67,0,0,1,.12,1,11.27,11.27,0,0,1-9.39,10.45,18.27,18.27,0,0,1-3,.23c-4.88,0-9.75,0-14.63,0h-1Z"/><path d="M28,5.67c-.61-.58-1.21-1.16-1.82-1.72l-2.8-2.56A3.5,3.5,0,0,1,23,1,.57.57,0,0,1,23,.2c.29-.31.6-.22.88,0l1.75,1.57,4.38,4a7.35,7.35,0,0,1,.55.67c-.33.35-.59.67-.9,1-1.86,1.71-3.73,3.4-5.6,5.1a1.8,1.8,0,0,1-.2.19c-.26.22-.54.45-.85.11a.6.6,0,0,1,.1-.89c.82-.8,1.65-1.58,2.48-2.36S27.25,8,28.2,7.13c-.34,0-.54,0-.75,0C22,7.08,16.5,7,11,7.11a10,10,0,0,0-9.66,8.51c-.07.48-.13,1-.18,1.43s-.12.75-.58.79S0,17.53,0,17A11.12,11.12,0,0,1,.83,13a11.16,11.16,0,0,1,9-7,13.2,13.2,0,0,1,2-.07H28Z"/><path d="M11.81,9.94a1.68,1.68,0,0,1,1.26.43l4.75,3.77c1.85,1.47,3.7,2.95,5.57,4.39a4.6,4.6,0,0,1,1.54,2c.39,1,.82,1.92,1.22,2.88.05.13.1.32,0,.41s-.26.08-.39.06l-3.57-.61a3.55,3.55,0,0,1-1.61-.74c-3.52-2.8-7-5.61-10.54-8.4a1.55,1.55,0,0,1-.28-2.37c.36-.41.68-.85,1-1.27A1.6,1.6,0,0,1,11.81,9.94Zm1,5.86L13,16,21,22.28a3.24,3.24,0,0,0,1.48.65l2.56.43.66.07c-.08-.21-.13-.35-.19-.48-.34-.8-.69-1.59-1-2.4a3.44,3.44,0,0,0-1.08-1.45L15.6,12.92l-.3-.21Zm2.05-3.48c-.79-.61-1.6-1.26-2.45-1.86a1,1,0,0,0-1.18.26c-.3.35-.56.74-.87,1.08-1,1.06-.65,1.68.27,2.33.6.42,1.17.9,1.72,1.32Z" style="&#10;    stroke: #0E66B2;&#10;    stroke-width: 0.3px;&#10;"/></g></g></svg>
                                            </a>
                                        </td>
                                    @endif

                                    <!-- Edit Stage End -->
                                    

 
                                    <!-- Last Modified Start -->
                                    @if(isset($eLastModifiedAccess) && $eLastModifiedAccess =='Yes')
                                    <td style="border:1px solid transparent;" class="text-center invisible"> 
                                        <a href="{{route('admin.labcase')}}" class="" title="Last Modified">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.1 29.89" style="fill: #0E66B2;height: 30px;"><defs><style>.cls-1{stroke:#000;stroke-width:0.5px;}</style></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M3.08,10.52l3.29-.65c1.12-.22,2.24-.45,3.37-.66a1.51,1.51,0,0,1,.73.07.46.46,0,0,1,.16.42,1.23,1.23,0,0,1-.55.47c-.86.21-1.74.37-2.61.55-1.6.31-3.2.64-4.8.94-.81.15-1,0-1.16-.71C1,8.54.54,6.12.06,3.71A1,1,0,0,1,0,3.2c.07-.19.26-.49.36-.47A.78.78,0,0,1,1,3.09a8.43,8.43,0,0,1,.31,1.34c.3,1.48.59,3,.89,4.46a2,2,0,0,0,.2.32c.17-.31.29-.55.42-.78,3-5.63,7.6-8.51,14-8.43a14.93,14.93,0,0,1,3,29.43A14.5,14.5,0,0,1,8.17,27.56L8,27.41c-.31-.21-.71-.44-.45-.87s.69-.2,1,0A13.63,13.63,0,0,0,18,28.71a13.9,13.9,0,1,0-14.62-19C3.26,9.88,3.21,10.09,3.08,10.52Z"/><path d="M23.61,9.8a1.76,1.76,0,0,1-.38,1.28Q21.45,13.53,19.67,16c-1.39,1.91-2.78,3.82-4.15,5.75a4.61,4.61,0,0,1-2,1.63c-1,.43-1.88.9-2.83,1.34a.56.56,0,0,1-.41.06c-.08-.06-.09-.27-.08-.4.15-1.2.3-2.39.46-3.59a3.53,3.53,0,0,1,.67-1.64c2.64-3.63,5.3-7.26,7.93-10.9a1.55,1.55,0,0,1,2.36-.37c.42.34.88.64,1.32,1A1.59,1.59,0,0,1,23.61,9.8Zm-5.82,1.27-.19.23-5.92,8.17a3.16,3.16,0,0,0-.59,1.5c-.1.86-.22,1.72-.32,2.58,0,.2,0,.39,0,.66L11.2,24c.78-.37,1.56-.75,2.35-1.11A3.32,3.32,0,0,0,15,21.76l5.83-8a3.15,3.15,0,0,0,.2-.31ZM21.37,13c.56-.82,1.18-1.66,1.74-2.53a1,1,0,0,0-.31-1.17c-.36-.28-.76-.52-1.11-.82-1.1-.92-1.71-.57-2.32.37-.4.63-.85,1.22-1.25,1.78Z" style="&#10;    stroke: #0E66B2;&#10;    stroke-width: 0.5px;&#10;"/></g></g></svg>
                                            <!-- Last Modified -->
                                        </a> 
                                    </td>
                                    @else
                                        <td style="border:1px solid transparent;"> 
                                            <a href="#" class="invisible btn back-btn gray-btn" title="Last Modified">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.1 29.89" style="fill: #0E66B2;height: 30px;"><defs><style>.cls-1{stroke:#000;stroke-width:0.5px;}</style></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M3.08,10.52l3.29-.65c1.12-.22,2.24-.45,3.37-.66a1.51,1.51,0,0,1,.73.07.46.46,0,0,1,.16.42,1.23,1.23,0,0,1-.55.47c-.86.21-1.74.37-2.61.55-1.6.31-3.2.64-4.8.94-.81.15-1,0-1.16-.71C1,8.54.54,6.12.06,3.71A1,1,0,0,1,0,3.2c.07-.19.26-.49.36-.47A.78.78,0,0,1,1,3.09a8.43,8.43,0,0,1,.31,1.34c.3,1.48.59,3,.89,4.46a2,2,0,0,0,.2.32c.17-.31.29-.55.42-.78,3-5.63,7.6-8.51,14-8.43a14.93,14.93,0,0,1,3,29.43A14.5,14.5,0,0,1,8.17,27.56L8,27.41c-.31-.21-.71-.44-.45-.87s.69-.2,1,0A13.63,13.63,0,0,0,18,28.71a13.9,13.9,0,1,0-14.62-19C3.26,9.88,3.21,10.09,3.08,10.52Z"/><path d="M23.61,9.8a1.76,1.76,0,0,1-.38,1.28Q21.45,13.53,19.67,16c-1.39,1.91-2.78,3.82-4.15,5.75a4.61,4.61,0,0,1-2,1.63c-1,.43-1.88.9-2.83,1.34a.56.56,0,0,1-.41.06c-.08-.06-.09-.27-.08-.4.15-1.2.3-2.39.46-3.59a3.53,3.53,0,0,1,.67-1.64c2.64-3.63,5.3-7.26,7.93-10.9a1.55,1.55,0,0,1,2.36-.37c.42.34.88.64,1.32,1A1.59,1.59,0,0,1,23.61,9.8Zm-5.82,1.27-.19.23-5.92,8.17a3.16,3.16,0,0,0-.59,1.5c-.1.86-.22,1.72-.32,2.58,0,.2,0,.39,0,.66L11.2,24c.78-.37,1.56-.75,2.35-1.11A3.32,3.32,0,0,0,15,21.76l5.83-8a3.15,3.15,0,0,0,.2-.31ZM21.37,13c.56-.82,1.18-1.66,1.74-2.53a1,1,0,0,0-.31-1.17c-.36-.28-.76-.52-1.11-.82-1.1-.92-1.71-.57-2.32.37-.4.63-.85,1.22-1.25,1.78Z" style="&#10;    stroke: #0E66B2;&#10;    stroke-width: 0.5px;&#10;"/></g></g></svg>
                                                <!-- Last Modified -->
                                            </a> 
                                        </td>
                                    @endif
                                    <!-- Last Modified End -->

                                    
                                    <!-- History Call Start -->

                                    @if(isset($eAddCallLogAccess) && $eAddCallLogAccess =='Yes')
                                    <td style="border:1px solid transparent; vertical-align: bottom" class="text-center">
                                        <a href="#" onclick="Call_log_store_model()" title="History Call" >
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33.4 33.27" style="fill: #0E66B2;height:30px"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M16.65,33.27H5.07a4.77,4.77,0,0,1-5.07-5V5.07A4.71,4.71,0,0,1,5,0Q16.74,0,28.5,0a4.65,4.65,0,0,1,4.87,4.83q.06,11.78,0,23.55a4.71,4.71,0,0,1-5,4.86Zm0-1.08H28.22c2.66,0,4.14-1.44,4.14-4.11q0-11.41,0-22.85c0-2.67-1.47-4.11-4.15-4.11h-23C2.49,1.11,1,2.54,1,5.22,1,7.3,1,9.39,1,11.48,1,17,1,22.54,1,28.07c0,2.64,1.47,4.11,4,4.11Z"/><path d="M10,26.67c-1.85.11-2.29-.21-2.69-2-.21-1-.43-1.94-.63-2.91A1.72,1.72,0,0,1,7.8,19.61c1.19-.53,2.4-1,3.58-1.51a1.88,1.88,0,0,1,2.46.61c1.33,1.57,1.42,1.62,3,.26a33,33,0,0,0,2.89-3.12c.49-.55.48-1-.21-1.46-.31-.19-.56-.47-.85-.69a1.66,1.66,0,0,1-.56-2.12c.51-1.33,1.08-2.64,1.65-3.94a1.47,1.47,0,0,1,1.68-1c1.44.25,2.88.51,4.29.89.89.25,1.11,1,1.08,1.91A17.48,17.48,0,0,1,10,26.67Zm11.5-12a13.32,13.32,0,0,1-4.59,5.51c-2,1.42-2,1.41-3.56-.46-.06-.08-.13-.15-.19-.24a1.06,1.06,0,0,0-1.54-.38c-1,.48-2.06.93-3.11,1.33A1.12,1.12,0,0,0,7.75,22c.23.89.45,1.79.64,2.7.15.74.54,1,1.33,1a16.09,16.09,0,0,0,7.93-2.36,16.54,16.54,0,0,0,8.1-13.4c.08-.91-.19-1.45-1.16-1.61A26,26,0,0,1,22,7.66a1,1,0,0,0-1.42.69c-.43,1.08-.89,2.15-1.37,3.2a1.07,1.07,0,0,0,.4,1.53C20.24,13.53,20.82,14.08,21.51,14.64Z"/></g></g></svg>
                                            <!-- History call -->
                                        </a>
                                    </td>
                                    @else
                                        <td style="border:1px solid transparent;">
                                            <a href="#" class="invisible btn back-btn gray-btn" title="Last Modified">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33.4 33.27" style="fill: #0E66B2;height:30px"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M16.65,33.27H5.07a4.77,4.77,0,0,1-5.07-5V5.07A4.71,4.71,0,0,1,5,0Q16.74,0,28.5,0a4.65,4.65,0,0,1,4.87,4.83q.06,11.78,0,23.55a4.71,4.71,0,0,1-5,4.86Zm0-1.08H28.22c2.66,0,4.14-1.44,4.14-4.11q0-11.41,0-22.85c0-2.67-1.47-4.11-4.15-4.11h-23C2.49,1.11,1,2.54,1,5.22,1,7.3,1,9.39,1,11.48,1,17,1,22.54,1,28.07c0,2.64,1.47,4.11,4,4.11Z"/><path d="M10,26.67c-1.85.11-2.29-.21-2.69-2-.21-1-.43-1.94-.63-2.91A1.72,1.72,0,0,1,7.8,19.61c1.19-.53,2.4-1,3.58-1.51a1.88,1.88,0,0,1,2.46.61c1.33,1.57,1.42,1.62,3,.26a33,33,0,0,0,2.89-3.12c.49-.55.48-1-.21-1.46-.31-.19-.56-.47-.85-.69a1.66,1.66,0,0,1-.56-2.12c.51-1.33,1.08-2.64,1.65-3.94a1.47,1.47,0,0,1,1.68-1c1.44.25,2.88.51,4.29.89.89.25,1.11,1,1.08,1.91A17.48,17.48,0,0,1,10,26.67Zm11.5-12a13.32,13.32,0,0,1-4.59,5.51c-2,1.42-2,1.41-3.56-.46-.06-.08-.13-.15-.19-.24a1.06,1.06,0,0,0-1.54-.38c-1,.48-2.06.93-3.11,1.33A1.12,1.12,0,0,0,7.75,22c.23.89.45,1.79.64,2.7.15.74.54,1,1.33,1a16.09,16.09,0,0,0,7.93-2.36,16.54,16.54,0,0,0,8.1-13.4c.08-.91-.19-1.45-1.16-1.61A26,26,0,0,1,22,7.66a1,1,0,0,0-1.42.69c-.43,1.08-.89,2.15-1.37,3.2a1.07,1.07,0,0,0,.4,1.53C20.24,13.53,20.82,14.08,21.51,14.64Z"/></g></g></svg>
                                                <!-- History call -->
                                            </a>
                                        </td>
                                    @endif
                                    <!-- History Call END -->



                                    <!-- Ready to send Start -->
                                    @if(isset($eReadyToSendAccess) && $eReadyToSendAccess =='Yes' && isset($Slip->vLocation) && $Slip->vLocation =='In lab')
                                        <td style="border:1px solid transparent;" class="text-center">
                                            <a href="#" title="Ready to send" @if(isset($Slip->vLocation) && $Slip->vLocation =='In lab') id="ReadyToSend" @endif  >
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.85 32" style="&#10;    fill: #0E66B2;&#10;    height: 30px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M11.9,32a13.51,13.51,0,0,1-.69-1.18c-1.08-2.52-2.16-5-3.2-7.56A2,2,0,0,0,6.77,22c-1.74-.7-3.47-1.43-5.2-2.16A2.11,2.11,0,0,1,0,17.52a2.47,2.47,0,0,1,1.37-1.67Q4.85,13.92,8.26,12q10-5.72,20-11.4A3.87,3.87,0,0,1,29.91,0a2,2,0,0,1,1.91,2.45c-.4,2.7-.83,5.4-1.25,8.1q-1.21,8-2.44,15.89c-.07.49-.14,1-.23,1.47a2.11,2.11,0,0,1-3.2,1.7c-2.06-.87-4.13-1.71-6.18-2.63a1.27,1.27,0,0,0-1.67.29c-1.39,1.42-2.84,2.79-4.27,4.17C12.42,31.59,12.25,31.72,11.9,32ZM30.79,1.74l-.25-.09L13,23.47c.26.13.37.21.49.26L25.08,28.6c1.18.5,1.66.2,1.86-1.09q1-6.6,2-13.19.93-6,1.85-12A3.26,3.26,0,0,0,30.79,1.74Zm-1.94,0-.11-.16L27.9,2,8.82,12.89l-7.06,4a1.13,1.13,0,0,0-.71,1,1,1,0,0,0,.71.93c2,.83,4,1.65,6,2.49ZM24.69,7.24l-.12-.11L8.62,22l2.6,6.21.17,0c0-.21,0-.42,0-.63,0-.91.05-1.82,0-2.73a2.71,2.71,0,0,1,.73-2c2-2.46,4-4.95,6-7.43ZM12.54,29.83l.15.08,3.89-3.7-4-1.68Z"/></g></g></svg>
                                                <!-- ready to send -->
                                            </a>
                                        </td>
                                    @else
                                        <td style="border:1px solid transparent;" class="text-center">
                                            <a href="#" title="Ready to send" class="invisible">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.85 32" style="&#10;    fill: #0E66B2;&#10;    height: 30px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M11.9,32a13.51,13.51,0,0,1-.69-1.18c-1.08-2.52-2.16-5-3.2-7.56A2,2,0,0,0,6.77,22c-1.74-.7-3.47-1.43-5.2-2.16A2.11,2.11,0,0,1,0,17.52a2.47,2.47,0,0,1,1.37-1.67Q4.85,13.92,8.26,12q10-5.72,20-11.4A3.87,3.87,0,0,1,29.91,0a2,2,0,0,1,1.91,2.45c-.4,2.7-.83,5.4-1.25,8.1q-1.21,8-2.44,15.89c-.07.49-.14,1-.23,1.47a2.11,2.11,0,0,1-3.2,1.7c-2.06-.87-4.13-1.71-6.18-2.63a1.27,1.27,0,0,0-1.67.29c-1.39,1.42-2.84,2.79-4.27,4.17C12.42,31.59,12.25,31.72,11.9,32ZM30.79,1.74l-.25-.09L13,23.47c.26.13.37.21.49.26L25.08,28.6c1.18.5,1.66.2,1.86-1.09q1-6.6,2-13.19.93-6,1.85-12A3.26,3.26,0,0,0,30.79,1.74Zm-1.94,0-.11-.16L27.9,2,8.82,12.89l-7.06,4a1.13,1.13,0,0,0-.71,1,1,1,0,0,0,.71.93c2,.83,4,1.65,6,2.49ZM24.69,7.24l-.12-.11L8.62,22l2.6,6.21.17,0c0-.21,0-.42,0-.63,0-.91.05-1.82,0-2.73a2.71,2.71,0,0,1,.73-2c2-2.46,4-4.95,6-7.43ZM12.54,29.83l.15.08,3.89-3.7-4-1.68Z"/></g></g></svg>
                                                <!-- ready to send -->
                                            </a>
                                        </td>
                                    @endif
                                    <!-- Ready to send END -->


                                    <!-- Driver History START -->
                                    @if(isset($eViewDriverHistoryAccess) && $eViewDriverHistoryAccess =='Yes')
                                    <td style="border:1px solid transparent;vertical-align: bottom" rowspan="2"  >
                                        <div class="bottom-btn-group text-center my-0 driverModel" data-hname="DriverHistory">
                                            <a href="javascript:;" title="Driver History">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39.23 31.44" style="fill: #0E66B2;height: 30px;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M4,17.79H4.7a1.29,1.29,0,0,1,.08.31c0,1.29,0,2.57,0,3.85v.95a4.38,4.38,0,0,0,.55-.42A5.17,5.17,0,0,1,9.2,20.74,5.07,5.07,0,0,1,14,23.52c.09.17.18.35.26.52l.28.59,10,0c0-.34.05-.58.05-.82q0-9.8-.06-19.58A3,3,0,0,0,21.3.92Q14.66.91,8,1A3.1,3.1,0,0,0,4.75,4.19c0,.54,0,1.08,0,1.7.39,0,.68,0,1,0h6.63a3.28,3.28,0,0,1,.89.08.58.58,0,0,1,.33.4.58.58,0,0,1-.31.4,1.68,1.68,0,0,1-.64.08L1,6.92c-.38,0-.77,0-1-.42C.07,6,.42,6,.79,6c.73,0,1.47,0,2.21,0h.88c0-.39,0-.71,0-1A14.84,14.84,0,0,1,4,3.28,3.84,3.84,0,0,1,7.88.06Q14.65,0,21.4,0a3.79,3.79,0,0,1,3.86,2.82,8.77,8.77,0,0,1,.24,1.78,12.15,12.15,0,0,1,0,1.22l.93,0c1,0,1.91,0,2.87.07a2.35,2.35,0,0,1,1.58.76c1.39,1.39,2.8,2.76,4.2,4.13a8.82,8.82,0,0,0,.83.79,3.87,3.87,0,0,1,1.43,3.32c0,2.87,0,5.73,0,8.6v1l1.43,0a.42.42,0,0,1,.42.42.41.41,0,0,1-.35.47,2.7,2.7,0,0,1-.49,0h-3a5.54,5.54,0,0,1-2.28,4.88,5.1,5.1,0,0,1-3.31,1A5.39,5.39,0,0,1,26,29.54a5.64,5.64,0,0,1-1.35-3.89c-.71-.15-9-.14-9.8,0-.1.57-.17,1.17-.31,1.74a5.32,5.32,0,0,1-10.34,0A5.66,5.66,0,0,1,4,26.07c0-2.59,0-5.18,0-7.78C4,18.16,4,18,4,17.79Zm21.56-3.53c0,2.61,0,5.14,0,7.67,0,.22,0,.43,0,.65a.15.15,0,0,0,0,.06h.08a5.54,5.54,0,0,1,9.51,1.85h1.08c.15-.91.1-9.65,0-10.28ZM9.32,30.53a4.41,4.41,0,0,0,4.49-4.43,4.48,4.48,0,0,0-4.38-4.48,4.39,4.39,0,0,0-4.57,4.44A4.34,4.34,0,0,0,9.32,30.53ZM25.57,26A4.4,4.4,0,0,0,30,30.48a4.46,4.46,0,0,0,.21-8.92A4.41,4.41,0,0,0,25.57,26ZM36,13.18c-.06-.12-.08-.21-.14-.26C34,11.05,32.17,9.19,30.33,7.33a1.6,1.6,0,0,0-1.25-.51c-1,0-2,0-2.95,0a4.52,4.52,0,0,0-.53.06,42,42,0,0,0,.07,6.31Z"/><path d="M2,10.38a.55.55,0,0,1,.54-.47H3l13.5,0a2.33,2.33,0,0,1,.65,0,.65.65,0,0,1,.35.38.4.4,0,0,1-.33.49,2.16,2.16,0,0,1-.57,0L3,10.84C2.58,10.84,2.17,10.9,2,10.38Z"/><path d="M0,14.28a.69.69,0,0,1,.75-.46H2.48l10,0c.22,0,.44,0,.66,0s.48.13.5.44-.21.42-.47.47a2.77,2.77,0,0,1-.41,0l-11.86,0C.52,14.76.16,14.77,0,14.28Z"/></g></g></svg>
                                            </a>
                                        </div>
                                    </td>
                                    @else
                                    <td style="border:1px solid transparent;" rowspan="2">
                                        <div class="bottom-btn-group text-center my-0 invisible">
                                            <!-- Driver History -->
                                            <a href="javascript:;" title="Driver History">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39.23 31.44" style="fill: #0E66B2;height: 30px;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M4,17.79H4.7a1.29,1.29,0,0,1,.08.31c0,1.29,0,2.57,0,3.85v.95a4.38,4.38,0,0,0,.55-.42A5.17,5.17,0,0,1,9.2,20.74,5.07,5.07,0,0,1,14,23.52c.09.17.18.35.26.52l.28.59,10,0c0-.34.05-.58.05-.82q0-9.8-.06-19.58A3,3,0,0,0,21.3.92Q14.66.91,8,1A3.1,3.1,0,0,0,4.75,4.19c0,.54,0,1.08,0,1.7.39,0,.68,0,1,0h6.63a3.28,3.28,0,0,1,.89.08.58.58,0,0,1,.33.4.58.58,0,0,1-.31.4,1.68,1.68,0,0,1-.64.08L1,6.92c-.38,0-.77,0-1-.42C.07,6,.42,6,.79,6c.73,0,1.47,0,2.21,0h.88c0-.39,0-.71,0-1A14.84,14.84,0,0,1,4,3.28,3.84,3.84,0,0,1,7.88.06Q14.65,0,21.4,0a3.79,3.79,0,0,1,3.86,2.82,8.77,8.77,0,0,1,.24,1.78,12.15,12.15,0,0,1,0,1.22l.93,0c1,0,1.91,0,2.87.07a2.35,2.35,0,0,1,1.58.76c1.39,1.39,2.8,2.76,4.2,4.13a8.82,8.82,0,0,0,.83.79,3.87,3.87,0,0,1,1.43,3.32c0,2.87,0,5.73,0,8.6v1l1.43,0a.42.42,0,0,1,.42.42.41.41,0,0,1-.35.47,2.7,2.7,0,0,1-.49,0h-3a5.54,5.54,0,0,1-2.28,4.88,5.1,5.1,0,0,1-3.31,1A5.39,5.39,0,0,1,26,29.54a5.64,5.64,0,0,1-1.35-3.89c-.71-.15-9-.14-9.8,0-.1.57-.17,1.17-.31,1.74a5.32,5.32,0,0,1-10.34,0A5.66,5.66,0,0,1,4,26.07c0-2.59,0-5.18,0-7.78C4,18.16,4,18,4,17.79Zm21.56-3.53c0,2.61,0,5.14,0,7.67,0,.22,0,.43,0,.65a.15.15,0,0,0,0,.06h.08a5.54,5.54,0,0,1,9.51,1.85h1.08c.15-.91.1-9.65,0-10.28ZM9.32,30.53a4.41,4.41,0,0,0,4.49-4.43,4.48,4.48,0,0,0-4.38-4.48,4.39,4.39,0,0,0-4.57,4.44A4.34,4.34,0,0,0,9.32,30.53ZM25.57,26A4.4,4.4,0,0,0,30,30.48a4.46,4.46,0,0,0,.21-8.92A4.41,4.41,0,0,0,25.57,26ZM36,13.18c-.06-.12-.08-.21-.14-.26C34,11.05,32.17,9.19,30.33,7.33a1.6,1.6,0,0,0-1.25-.51c-1,0-2,0-2.95,0a4.52,4.52,0,0,0-.53.06,42,42,0,0,0,.07,6.31Z"/><path d="M2,10.38a.55.55,0,0,1,.54-.47H3l13.5,0a2.33,2.33,0,0,1,.65,0,.65.65,0,0,1,.35.38.4.4,0,0,1-.33.49,2.16,2.16,0,0,1-.57,0L3,10.84C2.58,10.84,2.17,10.9,2,10.38Z"/><path d="M0,14.28a.69.69,0,0,1,.75-.46H2.48l10,0c.22,0,.44,0,.66,0s.48.13.5.44-.21.42-.47.47a2.77,2.77,0,0,1-.41,0l-11.86,0C.52,14.76.16,14.77,0,14.28Z"/></g></g></svg>
                                            </a>
                                        </div>
                                    </td>
                                    @endif
                                    <!-- Driver History END -->

                                    <!-- Pick Up Start -->
                                    @if(isset($ePickUpAccess) && $ePickUpAccess =='Yes' && ($Slip->vLocation =='In office ready to pickup' || $Slip->vLocation =='In lab ready to pickup'))
                                    <td style="border:1px solid transparent;" class="text-center">
                                        @php
                                          $title = '';
                                         @endphp
                                        @if($Slip->vLocation =='In office ready to pickup')
                                         @php
                                             $title = 'Office';
                                         @endphp   
                                        @elseif($Slip->vLocation =='In lab ready to pickup')
                                         @php
                                          $title = 'Lab';
                                         @endphp
                                        @endif
                                        <p class="mb-0" style="color:green;">{{$title}}</p>
                                        <a href="#" title="Pick Up {{$title}}" class="@if(isset($Slip->vLocation) && $Slip->vLocation =='In office ready to pickup' || $Slip->vLocation =='In lab ready to pickup') driverModel @endif "  @if(isset($Slip->vLocation) && $Slip->vLocation =='In lab ready to pickup') data-name="LabPickUp" @endif>
                                            <!-- Pick up -->
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.38 34.12" style="&#10;    fill: green;&#10;    height: 30px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M1.83,23.08A1.1,1.1,0,0,0,3,23.77H5.6l.56,0c0,.24,0,.45,0,.65V34h.6v-.25c0-3.12,0-6.23,0-9.35v-.62H9.9a2.67,2.67,0,0,0,.4,0,1,1,0,0,0,.83-.66.83.83,0,0,0-.24-.9c-.69-.76-1.38-1.51-2.09-2.27-.53-.57-1.05-1.16-1.61-1.71a1,1,0,0,0-1.57,0L2.15,22C1.87,22.29,1.61,22.6,1.83,23.08Zm.63-.48a1.46,1.46,0,0,1,.16-.19c1.09-1.17,2.19-2.32,3.28-3.49.45-.48.61-.48,1.07,0l3.27,3.49a1.3,1.3,0,0,1,.18.23.3.3,0,0,1-.2.49,2.17,2.17,0,0,1-.44,0H3.09a2.48,2.48,0,0,1-.39,0C2.4,23.09,2.31,22.86,2.46,22.6Z" style="&#10;    stroke: green;&#10;    stroke-width: 0.25px;&#10;"/><path d="M14.9,17.24a1.22,1.22,0,0,0,1.23-.73c.38-.73.77-1.45,1.15-2.17.28-.53.57-1,1.29-.83s.72.79.72,1.37c0,1.36,0,2.72,0,4.08a1.77,1.77,0,0,1-.73,1.53,27.36,27.36,0,0,0-2.39,2,3.35,3.35,0,0,0-1,1.53c-.41,2.08-.72,4.18-1,6.28A1,1,0,0,0,15,31.49a1,1,0,0,0,1.24-.62,2.9,2.9,0,0,0,.19-.73c.27-1.67.49-3.36.8-5a2.32,2.32,0,0,1,.68-1.18c1.41-1.27,2.87-2.49,4.32-3.72a3.09,3.09,0,0,0,1.25-2.32c0-2.32.06-4.65,0-7a2,2,0,0,0-2-2c-.48-.05-1,0-1.44,0A3.41,3.41,0,0,0,16.58,11c-.59,1.17-1.25,2.29-1.87,3.43a.9.9,0,0,1-1.42.38,3,3,0,0,1,.23-.57c.27-.39.62-.72.86-1.12.59-1,1.13-2,1.71-3A3.93,3.93,0,0,1,19.58,8c.51,0,1,0,1.53,0a3,3,0,0,1,3.26,3.3c0,2,0,3.91,0,5.87a4.51,4.51,0,0,1-1.66,3.65c-1.3,1.1-2.57,2.22-3.89,3.3A2.36,2.36,0,0,0,18,25.73c-.23,1.62-.5,3.24-.77,4.86a1.93,1.93,0,0,1-2.11,1.85A2,2,0,0,1,13.3,30c.24-1.71.51-3.42.74-5.13a5,5,0,0,1,1.83-3.33c.88-.71,1.7-1.47,2.47-2.14V14.68l-.17-.06c-.39.71-.81,1.41-1.16,2.14a2.21,2.21,0,0,1-2.23,1.35c-4.16,0-8.32,0-12.49,0C.68,18.09,0,17.47,0,15.83c0-2.52,0-5,0-7.56A2,2,0,0,1,2.25,6c2.61,0,5.22,0,7.82,0a1.9,1.9,0,0,1,2,2.1q0,3.78,0,7.56c0,.48,0,.95-.09,1.6C13,17.24,14,17.21,14.9,17.24Zm-5,0c.94,0,1.28-.26,1.29-1.17q.06-3.94,0-7.89C11.2,7.29,10.86,7,10,7Q6.06,7,2.11,7C1.31,7,.94,7.32.93,8.13q0,4,0,8c0,.8.38,1.11,1.18,1.12,1.33,0,2.66,0,4,0Z"/><path d="M19.32,6.45a3.3,3.3,0,0,1-3.26-3.24A3.25,3.25,0,0,1,19.33,0a3.17,3.17,0,0,1,3.24,3.22A3.23,3.23,0,0,1,19.32,6.45Zm2.34-3.33A2.34,2.34,0,0,0,19.32.83,2.41,2.41,0,0,0,17,3.14a2.42,2.42,0,0,0,2.38,2.37A2.33,2.33,0,0,0,21.66,3.12Z"/><path d="M24.37,22.61l-.77-.22c0,.38-.1.68-.11,1v6.28c0,.22,0,.45,0,.68-.07.79-.5,1.23-1.15,1.19s-1-.44-1-1.22c0-1.3,0-2.61,0-3.91a4,4,0,0,0-.22-.94l-.38,0a3.28,3.28,0,0,0-.25.83c0,1.38,0,2.77,0,4.16a2,2,0,0,0,1.92,2.05,2,2,0,0,0,2-2C24.39,27.89,24.37,25.31,24.37,22.61Z"/></g></g></svg>
                                            <!-- Pick Up -->
                                        </a>
                                    </td>
                                    @else
                                        <td style="border:1px solid transparent;">
                                            <a title="Pick Up" class="btn back-btn add-btn mb-1 invisible" >
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.38 34.12" style="&#10;    fill: #0E66B2;&#10;    height: 30px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M1.83,23.08A1.1,1.1,0,0,0,3,23.77H5.6l.56,0c0,.24,0,.45,0,.65V34h.6v-.25c0-3.12,0-6.23,0-9.35v-.62H9.9a2.67,2.67,0,0,0,.4,0,1,1,0,0,0,.83-.66.83.83,0,0,0-.24-.9c-.69-.76-1.38-1.51-2.09-2.27-.53-.57-1.05-1.16-1.61-1.71a1,1,0,0,0-1.57,0L2.15,22C1.87,22.29,1.61,22.6,1.83,23.08Zm.63-.48a1.46,1.46,0,0,1,.16-.19c1.09-1.17,2.19-2.32,3.28-3.49.45-.48.61-.48,1.07,0l3.27,3.49a1.3,1.3,0,0,1,.18.23.3.3,0,0,1-.2.49,2.17,2.17,0,0,1-.44,0H3.09a2.48,2.48,0,0,1-.39,0C2.4,23.09,2.31,22.86,2.46,22.6Z" style="&#10;    stroke: #0E66B2;&#10;    stroke-width: 0.25px;&#10;"/><path d="M14.9,17.24a1.22,1.22,0,0,0,1.23-.73c.38-.73.77-1.45,1.15-2.17.28-.53.57-1,1.29-.83s.72.79.72,1.37c0,1.36,0,2.72,0,4.08a1.77,1.77,0,0,1-.73,1.53,27.36,27.36,0,0,0-2.39,2,3.35,3.35,0,0,0-1,1.53c-.41,2.08-.72,4.18-1,6.28A1,1,0,0,0,15,31.49a1,1,0,0,0,1.24-.62,2.9,2.9,0,0,0,.19-.73c.27-1.67.49-3.36.8-5a2.32,2.32,0,0,1,.68-1.18c1.41-1.27,2.87-2.49,4.32-3.72a3.09,3.09,0,0,0,1.25-2.32c0-2.32.06-4.65,0-7a2,2,0,0,0-2-2c-.48-.05-1,0-1.44,0A3.41,3.41,0,0,0,16.58,11c-.59,1.17-1.25,2.29-1.87,3.43a.9.9,0,0,1-1.42.38,3,3,0,0,1,.23-.57c.27-.39.62-.72.86-1.12.59-1,1.13-2,1.71-3A3.93,3.93,0,0,1,19.58,8c.51,0,1,0,1.53,0a3,3,0,0,1,3.26,3.3c0,2,0,3.91,0,5.87a4.51,4.51,0,0,1-1.66,3.65c-1.3,1.1-2.57,2.22-3.89,3.3A2.36,2.36,0,0,0,18,25.73c-.23,1.62-.5,3.24-.77,4.86a1.93,1.93,0,0,1-2.11,1.85A2,2,0,0,1,13.3,30c.24-1.71.51-3.42.74-5.13a5,5,0,0,1,1.83-3.33c.88-.71,1.7-1.47,2.47-2.14V14.68l-.17-.06c-.39.71-.81,1.41-1.16,2.14a2.21,2.21,0,0,1-2.23,1.35c-4.16,0-8.32,0-12.49,0C.68,18.09,0,17.47,0,15.83c0-2.52,0-5,0-7.56A2,2,0,0,1,2.25,6c2.61,0,5.22,0,7.82,0a1.9,1.9,0,0,1,2,2.1q0,3.78,0,7.56c0,.48,0,.95-.09,1.6C13,17.24,14,17.21,14.9,17.24Zm-5,0c.94,0,1.28-.26,1.29-1.17q.06-3.94,0-7.89C11.2,7.29,10.86,7,10,7Q6.06,7,2.11,7C1.31,7,.94,7.32.93,8.13q0,4,0,8c0,.8.38,1.11,1.18,1.12,1.33,0,2.66,0,4,0Z"/><path d="M19.32,6.45a3.3,3.3,0,0,1-3.26-3.24A3.25,3.25,0,0,1,19.33,0a3.17,3.17,0,0,1,3.24,3.22A3.23,3.23,0,0,1,19.32,6.45Zm2.34-3.33A2.34,2.34,0,0,0,19.32.83,2.41,2.41,0,0,0,17,3.14a2.42,2.42,0,0,0,2.38,2.37A2.33,2.33,0,0,0,21.66,3.12Z"/><path d="M24.37,22.61l-.77-.22c0,.38-.1.68-.11,1v6.28c0,.22,0,.45,0,.68-.07.79-.5,1.23-1.15,1.19s-1-.44-1-1.22c0-1.3,0-2.61,0-3.91a4,4,0,0,0-.22-.94l-.38,0a3.28,3.28,0,0,0-.25.83c0,1.38,0,2.77,0,4.16a2,2,0,0,0,1.92,2.05,2,2,0,0,0,2-2C24.39,27.89,24.37,25.31,24.37,22.61Z"/></g></g></svg>
                                                <!-- Pick up -->
                                            </a>
                                        </td>
                                    @endif

                                    <!-- Pick Up END -->



                                    <!-- Drop Off START -->
                                    @if(isset($eDropOffAccess) && $eDropOffAccess =='Yes' && ($Slip->vLocation =='On route to the lab' || $Slip->vLocation == 'On route to the office'))
                                        @if($Slip->vLocation =='On route to the office')
                                            @php
                                                $title_drop = 'Office';
                                            @endphp   
                                        @elseif($Slip->vLocation =='On route to the lab')
                                            @php
                                                $title_drop = 'Lab';
                                            @endphp
                                        @else
                                            @php
                                                $title_drop = '';
                                            @endphp
                                        @endif
                                        <td style="border:1px solid transparent;">
                                            <p class="mb-0" style="color:red;">{{$title_drop}}</p>
                                            <a href="#" title="Drop Off {{$title_drop}}" @if(isset($Slip->vLocation) && $Slip->vLocation =='On route to the lab') data-name="DropOff" @endif class="driverModel " @if(isset($Slip->vLocation) && $Slip->vLocation =='On route to the office') data-name="LabDropUp" @endif>
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.38 34.13" style="&#10;    fill: red;&#10;    height: 30px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M22.55,28.77a1.1,1.1,0,0,0-1.13-.68c-.87,0-1.76,0-2.64,0l-.56,0a5.28,5.28,0,0,1,0-.66V17.85h-.59v.25c0,3.12,0,6.23,0,9.35v.63H14.47a2.54,2.54,0,0,0-.39,0,1,1,0,0,0-.84.65.85.85,0,0,0,.24.9l2.1,2.27c.53.57,1,1.16,1.6,1.71a1,1,0,0,0,1.58,0l3.47-3.72C22.51,29.57,22.77,29.25,22.55,28.77Zm-.64.48a1,1,0,0,1-.15.19l-3.28,3.49c-.45.48-.61.48-1.07,0-1.09-1.15-2.19-2.32-3.27-3.49a1.06,1.06,0,0,1-.19-.23.31.31,0,0,1,.21-.49,2.12,2.12,0,0,1,.44,0h6.69a2.55,2.55,0,0,1,.39,0C22,28.76,22.07,29,21.91,29.25Z" style="&#10;    stroke: red;&#10;    stroke-width: 0.25px;&#10;"/><path d="M12.36,17.24c0-.65-.09-1.12-.1-1.6V8.08A1.91,1.91,0,0,1,14.31,6c2.6,0,5.21,0,7.81,0a2,2,0,0,1,2.24,2.29q0,3.78,0,7.56c0,1.64-.66,2.26-2.27,2.26-4.17,0-8.33,0-12.5,0a2.19,2.19,0,0,1-2.22-1.35C7,16,6.6,15.33,6.21,14.62L6,14.68v4.73c.76.67,1.59,1.43,2.47,2.14a5,5,0,0,1,1.83,3.33c.23,1.71.49,3.42.73,5.13a2,2,0,0,1-1.76,2.43A1.93,1.93,0,0,1,7.2,30.59c-.27-1.62-.54-3.24-.78-4.86a2.29,2.29,0,0,0-.88-1.57C4.23,23.08,3,22,1.66,20.86A4.48,4.48,0,0,1,0,17.21c0-2,0-3.91,0-5.87A3,3,0,0,1,3.27,8c.51,0,1,0,1.53,0a3.93,3.93,0,0,1,3.49,2.05c.57,1,1.12,2,1.71,3,.24.4.59.73.86,1.12a3,3,0,0,1,.23.57.9.9,0,0,1-1.42-.38C9.05,13.29,8.39,12.17,7.8,11A3.42,3.42,0,0,0,4.32,8.93c-.48,0-1,0-1.44,0a2,2,0,0,0-2,2c-.05,2.32,0,4.65,0,7a3.09,3.09,0,0,0,1.25,2.32c1.45,1.23,2.9,2.45,4.31,3.72a2.25,2.25,0,0,1,.68,1.18c.32,1.67.54,3.36.81,5a2.89,2.89,0,0,0,.18.73,1,1,0,0,0,1.25.62,1,1,0,0,0,.79-1.15c-.3-2.1-.6-4.2-1-6.28a3.33,3.33,0,0,0-1-1.53,27.36,27.36,0,0,0-2.39-2A1.78,1.78,0,0,1,5.07,19c0-1.36,0-2.72,0-4.08,0-.58,0-1.18.73-1.37s1,.3,1.29.83c.37.72.77,1.44,1.15,2.17a1.22,1.22,0,0,0,1.22.73C10.43,17.21,11.39,17.24,12.36,17.24Zm5.92,0c1.33,0,2.66,0,4,0,.8,0,1.17-.32,1.17-1.12q0-4,0-8c0-.81-.37-1.12-1.17-1.12q-3.94,0-7.89,0c-.86,0-1.21.29-1.22,1.17q0,3.95,0,7.89c0,.91.36,1.17,1.3,1.17Z"/><path d="M1.81,3.22A3.16,3.16,0,0,1,5,0,3.25,3.25,0,0,1,8.32,3.21,3.3,3.3,0,0,1,5.05,6.45,3.22,3.22,0,0,1,1.81,3.22ZM5,5.51A2.42,2.42,0,0,0,7.42,3.14,2.42,2.42,0,0,0,5.06.83,2.34,2.34,0,0,0,5,5.51Z"/><path d="M0,22.61l.76-.22a8,8,0,0,1,.11,1q0,3.13,0,6.28c0,.22,0,.45,0,.68.07.79.5,1.23,1.15,1.19s1-.44,1-1.22c0-1.3,0-2.61,0-3.91a4.6,4.6,0,0,1,.23-.94l.38,0a3.75,3.75,0,0,1,.25.83c0,1.38,0,2.77,0,4.16A2,2,0,0,1,2,32.46a2,2,0,0,1-2-2C0,27.89,0,25.31,0,22.61Z"/></g></g></svg>
                                                <!-- Drop Off -->
                                            </a>
                                        </td>
                                    @else
                                        <td style="border:1px solid transparent;" class="text-center">
                                            <a title="Drop Off" class="invisible">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.38 34.13" style="&#10;    fill: #0E66B2;&#10;    height: 30px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M22.55,28.77a1.1,1.1,0,0,0-1.13-.68c-.87,0-1.76,0-2.64,0l-.56,0a5.28,5.28,0,0,1,0-.66V17.85h-.59v.25c0,3.12,0,6.23,0,9.35v.63H14.47a2.54,2.54,0,0,0-.39,0,1,1,0,0,0-.84.65.85.85,0,0,0,.24.9l2.1,2.27c.53.57,1,1.16,1.6,1.71a1,1,0,0,0,1.58,0l3.47-3.72C22.51,29.57,22.77,29.25,22.55,28.77Zm-.64.48a1,1,0,0,1-.15.19l-3.28,3.49c-.45.48-.61.48-1.07,0-1.09-1.15-2.19-2.32-3.27-3.49a1.06,1.06,0,0,1-.19-.23.31.31,0,0,1,.21-.49,2.12,2.12,0,0,1,.44,0h6.69a2.55,2.55,0,0,1,.39,0C22,28.76,22.07,29,21.91,29.25Z" style="&#10;    stroke: #0E66B2;&#10;    stroke-width: 0.25px;&#10;"/><path d="M12.36,17.24c0-.65-.09-1.12-.1-1.6V8.08A1.91,1.91,0,0,1,14.31,6c2.6,0,5.21,0,7.81,0a2,2,0,0,1,2.24,2.29q0,3.78,0,7.56c0,1.64-.66,2.26-2.27,2.26-4.17,0-8.33,0-12.5,0a2.19,2.19,0,0,1-2.22-1.35C7,16,6.6,15.33,6.21,14.62L6,14.68v4.73c.76.67,1.59,1.43,2.47,2.14a5,5,0,0,1,1.83,3.33c.23,1.71.49,3.42.73,5.13a2,2,0,0,1-1.76,2.43A1.93,1.93,0,0,1,7.2,30.59c-.27-1.62-.54-3.24-.78-4.86a2.29,2.29,0,0,0-.88-1.57C4.23,23.08,3,22,1.66,20.86A4.48,4.48,0,0,1,0,17.21c0-2,0-3.91,0-5.87A3,3,0,0,1,3.27,8c.51,0,1,0,1.53,0a3.93,3.93,0,0,1,3.49,2.05c.57,1,1.12,2,1.71,3,.24.4.59.73.86,1.12a3,3,0,0,1,.23.57.9.9,0,0,1-1.42-.38C9.05,13.29,8.39,12.17,7.8,11A3.42,3.42,0,0,0,4.32,8.93c-.48,0-1,0-1.44,0a2,2,0,0,0-2,2c-.05,2.32,0,4.65,0,7a3.09,3.09,0,0,0,1.25,2.32c1.45,1.23,2.9,2.45,4.31,3.72a2.25,2.25,0,0,1,.68,1.18c.32,1.67.54,3.36.81,5a2.89,2.89,0,0,0,.18.73,1,1,0,0,0,1.25.62,1,1,0,0,0,.79-1.15c-.3-2.1-.6-4.2-1-6.28a3.33,3.33,0,0,0-1-1.53,27.36,27.36,0,0,0-2.39-2A1.78,1.78,0,0,1,5.07,19c0-1.36,0-2.72,0-4.08,0-.58,0-1.18.73-1.37s1,.3,1.29.83c.37.72.77,1.44,1.15,2.17a1.22,1.22,0,0,0,1.22.73C10.43,17.21,11.39,17.24,12.36,17.24Zm5.92,0c1.33,0,2.66,0,4,0,.8,0,1.17-.32,1.17-1.12q0-4,0-8c0-.81-.37-1.12-1.17-1.12q-3.94,0-7.89,0c-.86,0-1.21.29-1.22,1.17q0,3.95,0,7.89c0,.91.36,1.17,1.3,1.17Z"/><path d="M1.81,3.22A3.16,3.16,0,0,1,5,0,3.25,3.25,0,0,1,8.32,3.21,3.3,3.3,0,0,1,5.05,6.45,3.22,3.22,0,0,1,1.81,3.22ZM5,5.51A2.42,2.42,0,0,0,7.42,3.14,2.42,2.42,0,0,0,5.06.83,2.34,2.34,0,0,0,5,5.51Z"/><path d="M0,22.61l.76-.22a8,8,0,0,1,.11,1q0,3.13,0,6.28c0,.22,0,.45,0,.68.07.79.5,1.23,1.15,1.19s1-.44,1-1.22c0-1.3,0-2.61,0-3.91a4.6,4.6,0,0,1,.23-.94l.38,0a3.75,3.75,0,0,1,.25.83c0,1.38,0,2.77,0,4.16A2,2,0,0,1,2,32.46a2,2,0,0,1-2-2C0,27.89,0,25.31,0,22.61Z"/></g></g></svg>
                                                <!-- Drop Off -->
                                            </a>
                                        </td>
                                    @endif
                                    <!-- Drop Off END -->

                                    <!-- Direction Start -->
                                    
                                    @if(isset($eDirectionAccess) && $eDirectionAccess =='Yes' && $Slip->vLocation =='On route to the office')
                                        <td style="border:1px solid transparent; vertical-align: bottom " class="text-center" >
                                            <a target="_b" @if(isset($CustomerOfficeData->vAddress) && !empty($CustomerOfficeData->vAddress)) href="https://www.google.com/maps/place/{{$CustomerOfficeData->vAddress}}" @endif  title="Directions" href="{{route('admin.labcase')}}" class="">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.7 34.72" style="&#10;    fill: #0E66B2;&#10;    height: 30px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M26.91,16.23C25.8,14.73,24.76,13.4,23.8,12a12.94,12.94,0,0,1-2-4.41,6.44,6.44,0,0,1,5-7.48,6.63,6.63,0,0,1,7,3,6.21,6.21,0,0,1,.79,4.42,12.35,12.35,0,0,1-2.33,4.85c-1.12,1.52-2.39,2.93-3.58,4.4a1.35,1.35,0,0,1-1.17.52c-1.59,0-3.19,0-4.78,0a6.7,6.7,0,0,0-.9,0,3.77,3.77,0,0,0,.24,7.51c.36,0,.72,0,1.08,0h6.77a4.88,4.88,0,0,1,0,9.76c-2.2,0-4.39,0-6.59,0H11.6a1.86,1.86,0,0,1-.71,0,1,1,0,0,1-.53-.5c-.08-.35.22-.53.55-.57s.54,0,.81,0H29.23a6.76,6.76,0,0,0,1.35-.08,3.7,3.7,0,0,0,3-3.86,3.66,3.66,0,0,0-3.71-3.57c-2.41,0-4.81,0-7.22,0A7.29,7.29,0,0,1,21,25.85a4.86,4.86,0,0,1,0-9.44,7.78,7.78,0,0,1,1.52-.17C24,16.22,25.38,16.23,26.91,16.23Zm1.27-.49c.76-.92,1.46-1.73,2.12-2.58a18.81,18.81,0,0,0,2.79-4.52,5.28,5.28,0,0,0,.35-3.26,5.14,5.14,0,0,0-5.22-4.25,5.15,5.15,0,0,0-5.24,4,6.06,6.06,0,0,0,.69,4.43A34.18,34.18,0,0,0,28.18,15.74Z"/><path d="M6.53,34.72A33.68,33.68,0,0,1,1.31,28,11.84,11.84,0,0,1,.16,25.05a6.39,6.39,0,0,1,5.06-7.6A6.53,6.53,0,0,1,13,23.56a6.67,6.67,0,0,1-.69,3.15,22,22,0,0,1-3.16,4.92C8.36,32.65,7.47,33.62,6.53,34.72Zm0-1.65,1.42-1.68a24,24,0,0,0,3.26-4.86,5.73,5.73,0,0,0,.64-3.48,5.08,5.08,0,0,0-3.36-4.23,5.35,5.35,0,0,0-6.92,7C2.63,28.57,4.55,30.75,6.5,33.07Z"/><path d="M28.17,8.7A2.16,2.16,0,0,1,26,6.45,2.13,2.13,0,0,1,28.2,4.32a2.19,2.19,0,1,1,0,4.38Zm0-3.23a1,1,0,0,0-1,1,1,1,0,0,0,1,1.11,1.11,1.11,0,0,0,1-1A1.06,1.06,0,0,0,28.22,5.47Z"/><path d="M6.51,21.65a2.2,2.2,0,1,1,0,4.39,2.2,2.2,0,1,1,0-4.39Zm0,1.15a1,1,0,0,0-1,1,1,1,0,0,0,1.06,1.08,1,1,0,0,0,1-1.06A.94.94,0,0,0,6.51,22.8Z"/></g></g></svg>
                                                <!-- Directions route -->
                                            </a>
                                        </td>
                                    @else
                                        <td title="Directions" style="border:1px solid transparent;" class="text-center">
                                            <a href="{{route('admin.labcase')}}" class="invisible">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.7 34.72" style="&#10;    fill: #0E66B2;&#10;    height: 30px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M26.91,16.23C25.8,14.73,24.76,13.4,23.8,12a12.94,12.94,0,0,1-2-4.41,6.44,6.44,0,0,1,5-7.48,6.63,6.63,0,0,1,7,3,6.21,6.21,0,0,1,.79,4.42,12.35,12.35,0,0,1-2.33,4.85c-1.12,1.52-2.39,2.93-3.58,4.4a1.35,1.35,0,0,1-1.17.52c-1.59,0-3.19,0-4.78,0a6.7,6.7,0,0,0-.9,0,3.77,3.77,0,0,0,.24,7.51c.36,0,.72,0,1.08,0h6.77a4.88,4.88,0,0,1,0,9.76c-2.2,0-4.39,0-6.59,0H11.6a1.86,1.86,0,0,1-.71,0,1,1,0,0,1-.53-.5c-.08-.35.22-.53.55-.57s.54,0,.81,0H29.23a6.76,6.76,0,0,0,1.35-.08,3.7,3.7,0,0,0,3-3.86,3.66,3.66,0,0,0-3.71-3.57c-2.41,0-4.81,0-7.22,0A7.29,7.29,0,0,1,21,25.85a4.86,4.86,0,0,1,0-9.44,7.78,7.78,0,0,1,1.52-.17C24,16.22,25.38,16.23,26.91,16.23Zm1.27-.49c.76-.92,1.46-1.73,2.12-2.58a18.81,18.81,0,0,0,2.79-4.52,5.28,5.28,0,0,0,.35-3.26,5.14,5.14,0,0,0-5.22-4.25,5.15,5.15,0,0,0-5.24,4,6.06,6.06,0,0,0,.69,4.43A34.18,34.18,0,0,0,28.18,15.74Z"/><path d="M6.53,34.72A33.68,33.68,0,0,1,1.31,28,11.84,11.84,0,0,1,.16,25.05a6.39,6.39,0,0,1,5.06-7.6A6.53,6.53,0,0,1,13,23.56a6.67,6.67,0,0,1-.69,3.15,22,22,0,0,1-3.16,4.92C8.36,32.65,7.47,33.62,6.53,34.72Zm0-1.65,1.42-1.68a24,24,0,0,0,3.26-4.86,5.73,5.73,0,0,0,.64-3.48,5.08,5.08,0,0,0-3.36-4.23,5.35,5.35,0,0,0-6.92,7C2.63,28.57,4.55,30.75,6.5,33.07Z"/><path d="M28.17,8.7A2.16,2.16,0,0,1,26,6.45,2.13,2.13,0,0,1,28.2,4.32a2.19,2.19,0,1,1,0,4.38Zm0-3.23a1,1,0,0,0-1,1,1,1,0,0,0,1,1.11,1.11,1.11,0,0,0,1-1A1.06,1.06,0,0,0,28.22,5.47Z"/><path d="M6.51,21.65a2.2,2.2,0,1,1,0,4.39,2.2,2.2,0,1,1,0-4.39Zm0,1.15a1,1,0,0,0-1,1,1,1,0,0,0,1.06,1.08,1,1,0,0,0,1-1.06A.94.94,0,0,0,6.51,22.8Z"/></g></g></svg>
                                                <!-- Directions route-->
                                            </a>
                                        </td>
                                    @endif
                                    <!-- Direction END -->

                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal fade" id="add_attachments_model">
                <div class="modal-dialog modal-lg modal-dialog-centered" style="">
                    <div class="modal-content" style="background: #f1f1f1;">
                        <div  class="link-model-wrapper all-icon-model" style="max-height: 560px;overflow-y: auto;height: auto;">
                            <div class="card-content collapse show">
                                <div class="c-model-header">
                                    <h5> Attachments</h5>
                                    <a href="javascript:;" id="add_attachments_model_close" class="close-model">
                                        <i class="fal fa-times"></i>
                                    </a>
                                </div>
                                <div class="card-body modal-body" id="attchments" style="max-height: 400px;overflow-y: auto;">
                                    @if(isset($Slip->vLocation) && ($Slip->vLocation =='In office ready to pickup' || $Slip->vLocation == 'On route to the lab' | $Slip->vLocation == 'In lab') || (\App\Libraries\General::admin_info()['iCustomerTypeId'] == '1'))
                                    <form action="{{route('admin.labcase.dropzoneStore')}}" class="dropzone" id="dropzonewidget" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="iTampId" id="iTampId" value="{{$tampid}}">
                                        <input type="hidden" name="iCaseId" id="iCaseId" value="{{$Case->iCaseId}}">
                                        <input type="hidden" name="iSlipId" id="iSlipId" value="{{$Slip->iSlipId}}">
                                    </form>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="attachment_list" class="mt-3">
                                            </div>
                                        </div>
                                    </div>
                                 
                                </div>
                                <div class="c-model-footer py-3">
                                 
                                    @if((isset($Slip->vLocation) && ($Slip->vLocation =='In office ready to pickup' || $Slip->vLocation == 'On route to the lab' || $Slip->vLocation == 'In lab')) || (\App\Libraries\General::admin_info()['iCustomerTypeId'] == '1'))
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-11 mx-auto p-0">
                                            <div class="text-end">
                                                <a href="javascript:;" id="submit_attacement" class="model-btn">
                                                    Submit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

             {{-- For Open Driver Model history Start Jun 1 2022 --}}
             <div class="modal fade mt-10" id="DriverModel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="DriverModelLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl" style="max-width: 1000px;width: 1000px;">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="DriverModelLabel">Driver History</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="{{route('admin.labcase.store_driverHistory')}}" method="post" >
                        @csrf
                        <div class="modal-body" id="DriverRespose" style="max-height:380px ;height: auto;overflow-y: auto;">
                        </div>
                        <div class="modal-footer justify-content-between">
                        <button type="button" class="back-btn red-btn" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="add-btn HideForHistory">@if(isset($Slip->vLocation) && ($Slip->vLocation =='On route to the lab' || $Slip->vLocation=='On route to the office')) Drop Off @else Pick Up @endif</button>
                        <input type="hidden" name="vOfficeCode" value="{{$CustomerOfficeData->vOfficeCode}}">
                        </div>
                    </form>
                  </div>
                </div>
              </div>              
             {{-- For Open Driver Model history End Jun 1 2022 --}}

             {{-- Edit Sage Modal Start --}}
             <div class="modal fade" id="EditSatageModal">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="EditSatageModalLabel">Adding stage notes</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    
                    <div class="modal-body">
                        <table class="tslip driver-inner-model" style="border-bottom: 1px solid #eff2f5;">
                            <tbody>
                              <tr>
                                <th>Office:</th>
                                <td><span>{{$CustomerLabData->vOfficeName}}</span></td> 
                                <th>Patient:</th>
                                <td><span>{{$Case->vPatientName}}</span></td>
                                <th>Case #</th>
                                <td><span>{{$Case->vCaseNumber}}</span></td>
                                <th>Pan #</th>
                                <td><span>{{$Case->vCasePanNumber?$Case->vCasePanNumber:'N/A'}}</span></td>
                              </tr>
                              </tbody>
                          </table>
                          <div id="NotesRespose" style="max-height: 200px;height: auto;overflow-y: auto;">
                              
                          </div>
                    </div>
                   
                    @if((isset($Slip->vLocation) && ($Slip->vLocation =='In office ready to pickup' || $Slip->vLocation == 'On route to the office') ) || (\App\Libraries\General::admin_info()['iCustomerTypeId'] == '1'))
                    
                        <div class="modal-body">
                            <textarea class="form-control" rows="6" id="tDescription_Ed" name="tDescription_Ed" placeholder="Description" ></textarea>
                            <span id="tDescription_Ed_error" style="display: none;color:red;">This field is required</span>
                        </div>
                        <div class="modal-footer justify-content-between">
                        <button type="button" class="back-btn red-btn" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="add-btn EditStageSubmit">Submit</button>
                        </div>
                        @else
                        <br>
                    @endif
                  </div>
                </div>
              </div>        
             {{-- Edit Sage Modal End --}}

            {{-- For Open Call Log history Start Jun 17 2022 --}}
            <div class="modal fade mt-10" id="CallLogModel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="CallLogModelLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="CallLogModelLabel">Call Log History</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                        {{-- <div class="modal-body" id="CallLogRespose">
                        </div> --}}
                        <div class="modal-footer justify-content-between">
                        <button type="button" class="back-btn red-btn" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
                </div>              
                {{-- For Open Call Log history End Jun 17 2022 --}}

                 {{-- For Open Call Log  Start Jun 17 2022 --}}
                <div class="modal fade mt-10" id="CallLogStoreModel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="CallLogStoreModelLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="CallLogStoreModelLabel">Add call</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form action="{{route('admin.labcase.insert_call_log')}}" id="CallLogfrm" method="post" >
                            @csrf
                            <div class="modal-body" id="CallLogStoreRespose">
                            </div>
                            <div class="modal-footer justify-content-between">
                                <input type="hidden" name="vSlipNumber" value="{{$Slip->vSlipNumber}}">
                            <button type="button" class="back-btn red-btn" data-bs-dismiss="modal">Close</button>
                            <button style="display: none;" type="button" class="add-btn SubmitCallLog show_hide_call_log">Submit</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>              
                 {{-- For Open Call Log  End Jun 17 2022 --}}
            </div>
        </div>
    </div>
</div>


<iframe id="printf" name="printf" style="display: none;"></iframe>
@endsection
@section('custom-js')

{{-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.0/dist/jquery.min.js"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>
<script>
    // Edit Stage Start
    $('#edit_stage').click(function(){
        $('#tDescription_Ed_error').hide();
       $('#EditSatageModal').modal('show');
    });
    $('.EditStageSubmit').click(function(){
         event.preventDefault();
           

        tDescription_Ed = $.trim($('#tDescription_Ed').val());
        let iSlipId = "{{$Slip->iSlipId}}";
        let vOfficeName = "{{$CustomerLabData->vOfficeName}}";
        let vPatientName = "{{$Case->vPatientName}}";
        let vCaseNumber = "{{$Case->vCaseNumber}}";
        let vCasePanNumber = "{{$Case->vCasePanNumber}}";
        error = false;
        if(tDescription_Ed.length == 0)
        {
            $('#tDescription_Ed_error').show();
            error = true;
        }
        if(error == false)
        {
            $(this).prop('disabled', true);
            $.ajax({
            url: "{{route('admin.labcase.edit_stage_notes')}}",
            type: "post",
            data: {
                tDescription_Ed:tDescription_Ed,
                iSlipId: iSlipId,
                vSlipNumber: "{{$Slip->vSlipNumber}}",
                iCaseId: "{{$Case->iCaseId}}",
                vPatientName:vPatientName,
                vOfficeName:vOfficeName,
                vCaseNumber:vCaseNumber,
                vCasePanNumber:vCasePanNumber,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {
                    $("#tDescription_Ed").val("");
                    $('.EditStageSubmit').attr("disabled", false);
                    // $('#EditSatageModal').modal('toggle');
                    notification_success("Stage notes updated successfully");
                    stage_notes_history();
                    $("#EditSatageModal #NotesRespose").animate({ scrollTop: $('#EditSatageModal #NotesRespose').prop("scrollHeight")}, 'slow');
                    $('#EditSatageModal').modal('hide');
                }
            });
        }
        else
        {
            return false;
        }
        
    });
    // Edit Stage End
    // For Open Driver Model history Start Jun 1 2022
    $('.driverModel').click(function(){
        let iSlipId = "{{$Slip->iSlipId}}";
        let vOfficeName = "{{$CustomerLabData->vOfficeName}}";
        let vPatientName = "{{$Case->vPatientName}}";
        let vCaseNumber = "{{$Case->vCaseNumber}}";
        let vCasePanNumber = "{{$Case->vCasePanNumber}}";
        let Status = $(this).attr("data-name");
        let HistoryType = $(this).attr("data-hname");
        $('#DriverModel').modal('show');
        if(HistoryType == 'DriverHistory')
        {
            $('.HideForHistory').hide();
        }
        else
        {
            $('.HideForHistory').show();
        }
        $.ajax({
            url: "{{route('admin.labcase.view_driverhistory')}}",
            type: "post",
            data: {
                iCaseId: "{{$Case->iCaseId}}",
                iSlipId: iSlipId,
                vPatientName:vPatientName,
                vOfficeName:vOfficeName,
                vCaseNumber:vCaseNumber,
                vCasePanNumber:vCasePanNumber,
                CurrentStatus:Status,
                HistoryType:HistoryType,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {
                $("#DriverRespose").html(response);
                setTimeout(function(){
                    $("#DriverModel .modal-body").animate({ scrollTop: $('#DriverModel .modal-body').prop("scrollHeight")}, 'slow');
                }, 500);
            }
        });
    });

    $('#edit_stage').click(function(){
        stage_notes_history();
    });
    $(document).ready(function(){
    
        stage_notes_history();
        next_preview_exist()
    })

    function next_preview_exist()
    {
        let iSlipId = "{{$Slip->iSlipId}}";
        let iCaseId = "{{$Case->iCaseId}}";
        $.ajax({
            url: "{{route('admin.labcase.next_previous_exist')}}",
            type: "post",
            data: {
                iCaseId: "{{$Case->iCaseId}}",
                iSlipId: iSlipId,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {  
                $('#nextStage').html(response);
                if($('#next_stage').length)
                {
                    if($('#next_stage').val()=='Yes')
                    {
                        $('#new_stage_visible').css('visibility', 'hidden');
                    }
                }
            }
        });
    }
    function stage_notes_history(){
        let iSlipId = "{{$Slip->iSlipId}}";
        let vOfficeName = "{{$CustomerLabData->vOfficeName}}";
        let vPatientName = "{{$Case->vPatientName}}";
        let vCaseNumber = "{{$Case->vCaseNumber}}";
        let vCasePanNumber = "{{$Case->vCasePanNumber}}";
        $.ajax({
            url: "{{route('admin.labcase.view_notes')}}",
            type: "post",
            data: {
                iCaseId: "{{$Case->iCaseId}}",
                iSlipId: iSlipId,
                vPatientName:vPatientName,
                vOfficeName:vOfficeName,
                vCaseNumber:vCaseNumber,
                vCasePanNumber:vCasePanNumber,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {
                $("#NotesRespose").html(response);
                $("#tDescription").html(response);
                setTimeout(function(){
                    $("#EditSatageModal #NotesRespose").animate({ scrollTop: $('#EditSatageModal #NotesRespose').prop("scrollHeight")}, 'slow');
                    $("#virtual_slip_model #tDescription").animate({ scrollTop: $('#virtual_slip_model #tDescription').prop("scrollHeight")}, 'slow');
                }, 500);
                 
            }
        });
    }


    // For Open Call Log Model history Start
    function Call_log_model()
    {
        let iSlipId = "{{$Slip->iSlipId}}";
        let vOfficeName = "{{$CustomerLabData->vOfficeName}}";
        let vPatientName = "{{$Case->vPatientName}}";
        let vCaseNumber = "{{$Case->vCaseNumber}}";
        let vCasePanNumber = "{{$Case->vCasePanNumber}}";
        
        $('#CallLogModel').modal('show');
    
        $.ajax({
            url: "{{route('admin.labcase.view_call_log')}}",
            type: "post",
            data: {
                iSlipId: iSlipId,
                iCaseId: "{{$Case->iCaseId}}",
               
                vPatientName:vPatientName,
                vOfficeName:vOfficeName,
                vCaseNumber:vCaseNumber,
                vCasePanNumber:vCasePanNumber,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {
                $("#CallLogRespose").html(response);
            }
        });
    }

    
    function Call_log_store_model()
    {
        let iSlipId = "{{$Slip->iSlipId}}";
        let vOfficeName = "{{$CustomerLabData->vOfficeName}}";
        let vPatientName = "{{$Case->vPatientName}}";
        let vCaseNumber = "{{$Case->vCaseNumber}}";
        let vCasePanNumber = "{{$Case->vCasePanNumber}}";
        $('.show_hide_call_log').hide();
        $('#CallLogStoreModel').modal('show');
        $.ajax({
            url: "{{route('admin.labcase.store_call_log')}}",
            type: "post",
            data: {
                iSlipId: iSlipId,
                iCaseId: "{{$Case->iCaseId}}",
             
                vPatientName:vPatientName,
                vOfficeName:vOfficeName,
                vCaseNumber:vCaseNumber,
                vCasePanNumber:vCasePanNumber,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {
                $("#CallLogStoreRespose").html(response);
            }
        });
    }

    $('.SubmitCallLog').click(function(){
    iCallId = $("#id").val();
    dDate           = $("#dDate").val();
    iQuantity       = $("#iQuantity").val();
    vName       = $("#vName").val();
    tDescription  = $("#tDescriptions").val();
        
    var error = false;

       
        if (tDescription.length == 0) 
        {
            $("#tDescription_error").show();
            error = true;
        } 
        else 
        {
            $("#tDescription_error").hide();
        }
        if(vName.length == 0)
        {
            $("#vName_error").show();
            error = true;
        } 
        else 
        {
            $("#vName_error").hide();
        }
        setTimeout(function(){
            if(error == true){
                return false;
            } 
            else {
                $("#CallLogfrm").submit();
                return true;
            }
        }, 1000);
        
    });
    // For Open Call Log Model history End

    $(document).on('click', '#ReadyToSend', function() {
        
        swal({
                title: "You are about to logout ",
				text: '"'+'{{$Case->vPatientName}}'+'"',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((Ok) => {
                if (Ok) {
                    let iSlipId = "{{$Slip->iSlipId}}";
                    let vOfficeName = "{{$CustomerLabData->vOfficeName}}";
                    let vPatientName = "{{$Case->vPatientName}}";
                    let vCaseNumber = "{{$Case->vCaseNumber}}";
                    let vCasePanNumber = "{{$Case->vCasePanNumber}}";
                    setTimeout(function() {
                        $.ajax({
                            url: "{{route('admin.labcase.ready_to_send')}}",
                            type: "post",
                            data: {
                                iSlipId: iSlipId,
                                vPatientName:vPatientName,
                                vOfficeName:vOfficeName,
                                vCaseNumber:vCaseNumber,
                                vCasePanNumber:vCasePanNumber,
                                _token: '{{csrf_token()}}',
                            },
                            success: function(response) {
                                if(response)
                                {
                                    location.reload();
                                }
                            }
                        });
                    }, 1000);
                }
            })
    });
    // For Open Driver Model history End Jun 1 2022

    $(document).ready(function() {
       
        $('#virtual_slip_model').addClass('show d-block');
    });
    
    $(document).on('click', '#back', function() {
        window.location.href = " {{route('admin.labcase')}}";

    });

    $(document).on('click', '#image_popup', function() {
        view_attacement();
    });

    function view_attacement()
    {
        iSlipId = "{{$Slip->iSlipId}}";
        iCaseId= "{{$Case->iCaseId}}";
        $('#add_attachments_model').addClass('show d-block');

        $.ajax({
            url: "{{route('admin.labcase.view_attacement')}}",
            type: "post",
            data: {
                iSlipId: iSlipId,
                iCaseId:iCaseId,
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
                $("#cancel_attacement").trigger("click");
                $("#attachment_list").html(response);
        
                $("#add_attachments_model #attchments").animate({ scrollTop: $('#add_attachments_model #attchments').prop("scrollHeight")}, 'slow');
                Dropzone.forElement('#dropzonewidget').removeAllFiles(true);
                
            }
        });
    }
    $(document).on('click', '#add_attachments_model_close', function() {
        $("#add_attachments_model").removeClass("show d-block");

    });


    $(document).on("click", ".print_paper", function() {
        iCaseId = $(this).data("case");
        iSlipId = $(this).data("slip");

        $.ajax({
            url: "{{route('admin.labcase.paper_slip')}}",
            type: "post",
            data: {
                iCaseId: iCaseId,
                iSlipId: iSlipId,
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
               // alert("{{$_SERVER['HTTP_USER_AGENT']}}");
               @php

                $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
                $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
                $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
                $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
                $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");
                $user_agent = $_SERVER['HTTP_USER_AGENT']; 

                if($iPod || $iPhone || $iPad){
                    

                        if (stripos($_SERVER['HTTP_USER_AGENT'],"safari") && !stripos($_SERVER['HTTP_USER_AGENT'],"CriOS")) 
                        {
                            @endphp
                                var newWin = window.frames["printf"];

                                setTimeout(function(){
                                    newWin.document.write(response);
                                    newWin.document.close();
                                    
                                },500)
                            @php

                        }else{

                            @endphp
                                const wnd = window.open('about:blank', '', '_blank, alwaysRaised=yes');
                                wnd.document.write(response);
                                wnd.focus();

                                setTimeout(function(){
                                    wnd.print();
                                    setTimeout(function(){
                                        wnd.close();
                                    },500)
                                    
                                },500)
                            @php
                        }

                }else{ @endphp
                    var newWin = window.frames["printf"];

                    setTimeout(function(){
                        newWin.document.write(response);
                        newWin.document.close();
                        
                    },500)
                    @php
                }


                @endphp
            }
        });
    });

    // Readmore Read less start
    function ReadMoreLess(change_txt,selected_id) {
       if(change_txt == 'less_read')
       {
           $('#less_read'+selected_id).hide();
           $('#full_read'+selected_id).show();
       }
       else
       {
           $('#less_read'+selected_id).show();
           $('#full_read'+selected_id).hide();
       }
    }
    // Readmore Read less end


    $(document).on('click', '#attachment_delete', function() {

        swal({
                title: "Are you sure delete this items.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");


                    setTimeout(function() {
                        $.ajax({
                            url: "{{route('admin.labcase.delete_gallery_slip')}}",
                            type: "get",
                            data: {
                                id: id
                            },
                            success: function(response) {
                        
                                setTimeout(function(){
                                    view_attacement()
                                },500);
                            }
                        });
                    }, 1000);
                }
            })
    });

    var myimg = "<div class=''><i class='fas fa-cloud-upload-alt upload'></i></div>";
    Dropzone.options.dropzonewidget = {
        paramName: "vImage",
        dictDefaultMessage: myimg + '<br>' + "<h4 class='darg'>Drag and drop your images, STL files, Video and Audio here</h4>" + '<br>' + "<span> Once uploaded, you can select and drag images to recorder below.</span>" + '<br>' + "<a href='javascript:;' class='album-addbtn pass btn mt-5' >Add Image </a>",
        // acceptedFiles: "image/*, video/* , audio/*",
        maxFilesize: 200, // MB,
       
    };

    $(document).on('click', '#submit_attacement', function() {

        $.ajax({
            url: "{{route('admin.labcase.attacement_newstage')}}",
            type: "post",
            data: {
                iCaseId: "{{$Slip->iSlipId}}",
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
                $("#add_attachments_model_close").trigger("click");
                $("#attachment_list").html(response);
                Dropzone.forElement('#dropzonewidget').removeAllFiles(true);
                // setTimeout(function(){
                    // view_attacement()
                $('#add_attachments_model').modal('hide');
                // },500);
            }
        });

    });

  
    // // Update Billing Data Start
    // $( window ).on("load", function() {
    //     @if((isset($upper_product->iSlipProductId) && isset($lower_product->iSlipProductId) && $upper_product->eStatus =='Finished' && $lower_product->eStatus =='Finished') || (isset($upper_product->iSlipProductId) && !isset($lower_product->iSlipProductId) && $upper_product->eStatus =='Finished') || (!isset($upper_product->iSlipProductId) && isset($lower_product->iSlipProductId) && $lower_product->eStatus =='Finished'))

    //         $.ajax({
    //             url: "{{route('admin.labcase.update_billing')}}",
    //             type: "post",
    //             data: {
    //                 iCaseId : "{{isset($Case->iCaseId)?$Case->iCaseId:''}}",
    //                 iSlipId : "{{isset($Slip->iSlipId)?$Slip->iSlipId:''}}",
    //                 _token: '{{csrf_token()}}',
    //             },
    //             success: function(response) {
                    
    //             }
    //         });
    //     @endif 
    // });
   
    // // Update Billing Data End


</script>

@endsection
