<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body  style="color: #000;font-family:'Verdana';">


	<style>

		@import url('https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap');

		@font-face {
			font-family: "Verdana";
			src: url("{{asset('admin/assets/fonts/verdana.ttf')}}");
		}

		 @font-face {
			font-family: "comfortaa";
			src: url("{{asset('admin/assets/fonts/comfortaa.ttf')}}");
			font-weight: normal;
			font-style: normal;
		} 

		body {
			width: 700px;
			max-width: 700px;
			margin: 10px 0px 0px 0px;
			padding: 0px;
			/*transform: rotate(90deg);*/
		}

		.font-size {
			font-size: 9px;
		}

		.inner-font-size {
			font-size: 6px;
		}

		.teeth-counter-text {
			font-size: 7px;
		}

		.comfota-font {
			font-family: 'Comfortaa';
		}
	
	</style>



	<table style="max-width:50%;width:570px;border-collapse: collapse;">
		<tr>
			<td colspan="3" style="text-align: center;">
				<span style="font-size: 14px;font-family: 'Comfortaa';">BEST DENTAL LAB
				</span>
			</td>
		</tr>
	</table>
	<table style="max-width:700px;width:570px; margin: 0 auto 0 0;border-collapse:collapse;">
			<tr>
				<td>
					<table style="width:100%;text-align: left;">
						<tr>
							<td style="font-size:7px;padding: 0px 0px 0px 5px;line-height: 1.2px;height:5px;vertical-align: bottom;" colspan="4"><span style="width:50px;display: inline-block;">Office:</span>
							 <span style="width:100%;display: inline-block;font-size:11px;padding-left: 52px;">{{$Case->vOfficeName}} </span> 															
							</td>
						</tr>
	
	
						<tr>
							<td>
								<table>
									<tr>
										<td style="font-size:7px;vertical-align: top;"><span style="width:50px;">Dr.:</span></td>
										<td style="vertical-align: top;"><span style="font-size:11px;">{{$Case->vFirstName." ".$Case->vLastName}}</span></td>
										<td style="font-size:7px;vertical-align: top;padding-left: 10px;"><span style="width:40px;display: inline-block;">Pan #</span></td>
										<td style="font-size:11px;vertical-align: top;"><span style="border: 1px solid;padding:1px;border-radius:5px;font-size: 10px;">@if(!empty($Case->vCasePanNumber)){{$Case->vCasePanNumber}}@else---@endif</span></td>
									</tr>
									<tr>
										<td style="font-size:7px;vertical-align: top;"><span style="width:50px;display: inline-block;">Patient:</span></td>
										<td style="vertical-align: top;"><span style="font-size:11px;">{{$Case->vPatientName}}</span></td>
										<td style="font-size:7px;vertical-align: top;"><span style="width:50px;display: inline-block;padding-left: 10px;">Case #</span></td>
										<td><span style="font-size:11px;vertical-align: top;">{{$Case->vCaseNumber}}</span></td>
									</tr>
	
									<tr>
									<td style="font-size:7px;vertical-align: top;visibility: hidden;"><span style="width:50px;display: inline-block;">Slip #</span></td>
											<td><span style="font-size:11px;vertical-align: top;visibility: hidden;">{{$Slip->vSlipNumber}}</span></td>
	
											<td style="font-size:7px;vertical-align: top;"><span style="width:50px;display: inline-block;padding-left: 10px;">Slip #</span></td>
											<td><span style="font-size:11px;vertical-align: top;">{{$Slip->vSlipNumber}}</span></td>
									</tr>
	
								</table>
							</td>
						
							<td>
								<table>
									<tr>
										<td><span style="width:75px;display: inline-block;font-size: 7px;">Pick up date</span></td>
										<td><span style="width:100px;display: inline-block;font-size: 11px;">@if(isset($final_Pickup_date)){{ date('m/d/Y', strtotime($final_Pickup_date)) }} @else {{"---"}} @endif</span></td>
									</tr>
	
									<tr>
										<td><span style="width:75px;display: inline-block;font-size:7px;">Delivery date</span></td>
										<td><span style="display: inline-block;font-size:11px;">@if(isset($final_delivery_date)){{ date('m/d/Y', strtotime($final_delivery_date)) }} @else {{"-----"}} @endif</span></td>
									</tr>
									<tr>
										<td><span style="width:75px;display: inline-block;font-size: 7px;">Delivery time</span></td>
										<td><span style="display: inline-block;font-size: 11px;">
												@if(isset($final_delivery_time)){{ date("h:i", strtotime($final_delivery_time)) }} @else {{"-----"}} @endif</span></td>
									</tr>
									<tr></tr>
								</table>
							</td>

						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table style="max-width:570px;width: 550px; margin: 0 auto 0 0;border-collapse: collapse;border-top: 1px solid #000;table-layout: fixed;">
		
				<tr>
					<td style="vertical-align:top;text-align: center;">
						<!-- table 2-2 -->
						<table style="width:100%;text-align: center;width: 550px;">
							<tr>
		
								<td style="vertical-align: top;text-align: center;padding:0px;">
									<span style="font-size:12px;"> MAXILLARY </span>
								</td>
								<td colspan="2" style="vertical-align: top;text-align: center;padding: 0px;">
									<span style="font-size: 11px;"> CASE
										DESIGN</span>
								</td>
								<td style="vertical-align: top;text-align: center;padding:0px;">
									<span style="font-size:12px;"> MANDIBULAR</span>
								</td>
							</tr>
							@if(!empty($upper_product->vProductName) || !empty($lower_product->vProductName))
							<tr>
								<td colspan="2" style="text-align:right;line-height:8px;padding:0px;margin: 0px;" class="font-size">
									@if(!empty($upper_product->vProductName))
									<span class="font-size">{{$upper_product->vProductName}}</span>
									@endif
								</td>
								<td style="text-align:center;line-height:8px;padding:0px;margin: 0px;">
									<span style="display: inline-block; font-size: 7px;padding: 0px;">PRODUCT</span>
								</td>
								<td colspan="2" style="text-align:left;line-height:8px;padding:0px;margin: 0px;" class="font-size">
									@if(!empty($lower_product->vProductName))
									<span> {{$lower_product->vProductName}}</span>
									@endif
								</td>
							</tr>
							@endif
							@if(!empty($upper_product->vGradeName) || !empty($lower_product->vGradeName))
							<tr>
								<td colspan="2" style=" text-align:right;line-height:8px;padding:0px;margin: 0px;" class="font-size">
									@if(!empty($upper_product->vGradeName))
									<span class="font-size">{{$upper_product->vGradeName}}</span>
									@endif
								</td>
								<td style=" text-align:center;line-height:8px;padding:0px;margin: 0px;"><span style="font-size: 7px;">GRADE</span>
								</td>
								<td colspan="2" style="text-align:left;line-height:8px;padding:0px;margin: 0px;">
									@if(!empty($lower_product->vGradeName))
									<span class="font-size"> {{$lower_product->vGradeName}}</span>
									@endif
								</td>
							</tr>
							@endif
							@if(!empty($upper_product->vStageName) || !empty($lower_product->vStageName))
							<tr>
								<td colspan="2" style=" text-align:right;line-height:8px;padding:0px;margin: 0px;">
									@if(!empty($upper_product->vStageName))
									<span class="font-size">
										{{$upper_product->vStageName}}
									</span>
									@endif
								</td>
								<td style=" text-align:center;line-height:8px;padding:0px;margin: 0px;"><span style="font-size: 7px;">STAGE</span>
								</td>
								<td colspan="2" style=" text-align:left;line-height:8px;padding:0px;margin: 0px;">
									@if(!empty($lower_product->vStageName))
									<span class="font-size">{{$lower_product->vStageName}}</span>
									@endif
								</td>
							</tr>
							@endif
							@if(!empty($upper_product->vBrandName) || !empty($lower_product->vBrandName))
							<tr>
								<!-- TEETH SHADE -->
								<td style=" text-align:right;line-height:8px;padding:0px;margin: 0px;" colspan="2" class="font-size">
									@if(!empty($upper_product->vBrandName))
									<span class="font-size">{{$upper_product->vBrandName}}-{{$upper_product->vToothShade}}</span>
									@endif
								</td>
								<td style=" text-align:center;line-height:8px;padding:0px;margin: 0px;">
									<span style="font-size: 7px;">TEETH SHADE</span>
								</td>
		
								<td style=" text-align:left;line-height:8px;padding:0px;margin: 0px;" colspan="2">
									@if(!empty($lower_product->vBrandName))
									<span class="font-size">{{$lower_product->vBrandName}} - {{$lower_product->vToothShade}}</span>
									@endif
								</td>
							</tr>
							@endif
							@if(!empty($upper_product->vGumBrand) || !empty($lower_product->vGumBrand))
							<tr style="padding:0px;margin:0px;">
								<!--   GUM SHADE -->
								<td colspan="2" style="text-align:right;line-height:8px;padding:0px;margin: 0px;">
									@if(!empty($upper_product->vGumBrand))
									<span style="display: inline-block;" class="font-size"> {{$upper_product->vGumBrand}} - {{$upper_product->vGumShade}}</span>
									@endif
								</td>
								<td style=" text-align:center;line-height:8px;padding:0px;margin: 0px;"><span style="font-size: 7px;">GUM SHADE</span></td>
		
								<td colspan="2" style="text-align:left;line-height:8px;padding:0px;margin: 0px;">
									@if(!empty($lower_product->vGumBrand))
									<span class="font-size"> {{$lower_product->vGumBrand}} - {{$lower_product->vGumShade}}</span>
									@endif
								</td>
							</tr>
							@endif
							@if(!empty($upper_impression) && sizeof($upper_impression) > 0 OR !empty($lower_impression) && sizeof($lower_impression) > 0)
							<tr style="padding:0px;margin:0px;">
								<td colspan="2" style="vertical-align: top;line-height:9px;padding:0px;margin:0px;">
									<table style="width:100%;padding:0px;margin:0px;height: 100%;border-spacing: 0;">
										@if(empty($upper_impression))
										@php
										$upper_impression = $upper_impressionOp
										@endphp
										@endif
		
										@foreach ($upper_impression as $value_impression)
										<tr style="vertical-align: top;padding:0px;margin:0px;">
											<td class="p-0" style="text-align: right;line-height:9px;padding:0px;margin:0px;height: 100%;">
												<div style="display:flex;justify-content: flex-end;padding:0px;margin: 0px;">
													<span style="display: inline-block;" class="font-size">{{$value_impression->vImpressionName}}</span>
													<span style="display: inline-block;margin-left:5px;" class="font-size">Qty {{$value_impression->iQuantity}}</span>
												</div>
											</td>
										</tr>
										@endforeach
									</table>
								</td>
		
								<td style="vertical-align: top; text-align:center;line-height:8px;padding:0px;margin:0px;">
									<span style="font-size: 7px;">IMPRESSIONS</span>
								</td>
		
								<td colspan="2" style="vertical-align: top;line-height:9px;padding:0px;margin:0px;">
									<table style="width: 100%;padding:0px;margin: 0px;border-spacing: 0;">
										@if(empty($lower_impression))
										@php
										$lower_impression = $lower_impressionOp
										@endphp
										@endif
										@foreach ($lower_impression as $lower_impression)
										<tr style="vertical-align: top;">
											<td class="p-0" style="text-align: left;line-height:9px;padding:0px;margin:0px;">
												<div style="display:flex;justify-content: flex-start;">
													<span style="display: inline-block;margin-right:5px;" class="font-size">Qty {{$lower_impression->iQuantity}}</span>
													<span style="display: inline-block;" class="font-size">{{$lower_impression->vImpressionName}}</span>
												</div>
											</td>
										</tr>
										@endforeach
									</table>
								</td>
							</tr>
							@endif
							@if(!empty($upper_addons) && sizeof($upper_addons) > 0 && !empty($lower_addons) && sizeof($lower_addons) > 0)
							<tr>
								<td colspan="2" style="vertical-align: top; text-align:right;line-height:9px;padding:0px;margin:0px;">
									<table style="width:100%;border-spacing: 0;">
										@foreach ($upper_addons as $value)
										<tr style="vertical-align: top;">
											<td class="p-0" style="text-align: right;line-height:9px;padding:0px;margin: 0px;">
												<div style="display:flex;justify-content: flex-end;">
													<span style="padding:1px;display:inline-block;" class="font-size">{{$value->vAddonName}}</span>
													<span style="border-radius:5px;padding:1px;display: inline-block;margin-left:5px;" class="font-size">Qty {{$value->iQuantity}}</span>
												</div>
											</td>
										</tr>
										@endforeach
									</table>
								</td>
		
								<td style="vertical-align: top;text-align:center;line-height:8px;padding:0px;margin: 0px;">
									<span style="font-size: 7px;">ADD ONS</span>
								</td>
		
								<td colspan="2" style="vertical-align: top;line-height:9px;padding:0px;margin: 0px;" class="font-size">
									<table style="width:100%;border-spacing: 0;">
										@foreach ($lower_addons as $value)
										<tr>
											<td class="p-0" style=" text-align: left;" class="font-size">
												<div style="display: flex;justify-content: flex-start;">
													<span style="display: inline-block;padding:1px;margin-right:8px;" class="font-size">Qty {{$value->iQuantity}}</span>
													<span style="border-radius:5px;display: inline-block;" class="font-size">{{$value->vAddonName}}</span>
												</div>
											</td>
										</tr>
										@endforeach
									</table>
								</td>
							</tr>
							@endif
							@if($rush == "Yes")
							<tr>
								<!--  DELIVERY DATE-->
								@if(!empty($upper_product))
		
								<td colspan="2" style="text-align:right;line-height:8px;padding:0px;margin: 0px;"><span class="font-size">{{date('m/d/Y', strtotime($upper_product->dDeliveryDate))}} </span></td>
								@else
								<td colspan="2"></td>
								@endif
								<td style="vertical-align: top; text-align:center;line-height:8px;padding:0px;margin: 0px;">
									<span style="display: inline-block;font-size: 7px;">DELIVERY DATE</span>
								</td>
								@if(!empty($lower_product))
								<td style="text-align:left;line-height:8px;padding:0px;margin: 0px;"><span class="font-size">{{date('m/d/Y', strtotime($lower_product->dDeliveryDate))}} </span></td>
								@else
								<td colspan="2"></td>
								@endif
		
							</tr>
							<tr>
								<!--  DELIVERY DATE-->
								@if(!empty($upper_product))
								<td colspan="2" style="text-align:right;line-height:8px;padding:0px;margin: 0px;"><span class="font-size">{{$upper_product->tDeliveryTime}}</span></td>
								@else
								<td colspan="2"></td>
								@endif
								<td style="vertical-align: top; text-align:center;line-height:8px;padding:0px;margin: 0px;">
									<span style="font-size: 7px;">DELIVERY TIME</span>
								</td>
								@if(!empty($lower_product))
								<td colspan="2" style="text-align:left;line-height:8px;padding:0px;margin: 0px;">
									<span class="font-size">{{$lower_product->tDeliveryTime}} </span>
								</td>
		
								@endif
							</tr>
							@endif
						</table>
					</td>
				</tr>
		
			</table>
			<table style="max-width:700px;width:550px; margin: 0 auto 0 0;border-collapse: collapse;border-bottom:1px solid #000;">	
				<tr>
					<td style="vertical-align: top;">
						<table style="padding: 0px;width:550px;table-layout: fixed;max-width: 550px;">
							<tr>
								<td style="vertical-align: top;text-align: left;">
									<table>
										<!-- <tr>
											<td style="vertical-align: top;text-align: center; font-size:15px;">
												<span> MAXILLARY </span>
											</td>
										</tr> -->
										<tr>
											<td>
												<div style="margin: 0px auto;height:auto;width:100%;">
													<img src="{{asset('admin/assets/images/upper-teeth-pdf.png')}}" alt="maxillary-img" style="width:100% ;height:auto;">
												</div>
											</td>
										</tr>
		
										@if((isset($upper_vTeethInMouth) AND isset($upper_vMissingTeeth) AND isset($upper_vWillExtractOnDelivery) AND isset($upper_vHasBeenExtracted) AND isset($upper_vFixOrAdd)) OR (isset($upper_vTeethInMouthOp) AND isset($upper_vMissingTeethOp) AND isset($upper_vWillExtractOnDeliveryOp) AND isset($upper_vHasBeenExtractedOp) AND isset($upper_vFixOrAddOp)))
		
										@if((isset($upper_vTeethInMouth) AND isset($upper_vMissingTeeth) AND isset($upper_vWillExtractOnDelivery) AND isset($upper_vHasBeenExtracted) AND isset($upper_vFixOrAdd)))
										@else
										@php
										$upper_vTeethInMouth = $upper_vTeethInMouthOp;
										$upper_vMissingTeeth = $upper_vMissingTeethOp;
										$upper_vWillExtractOnDelivery = $upper_vWillExtractOnDeliveryOp;
										$upper_vHasBeenExtracted = $upper_vHasBeenExtractedOp;
										$upper_vFixOrAdd = $upper_vFixOrAddOp;
										$upper_vClasps = $upper_vClaspsOp;
		
										@endphp
										@endif
										@endif
		
		
										@if(!empty($upper_vTeethInMouth))
										@php
										$parts=array_filter($upper_vTeethInMouth);
										$upper_vTeethInMouth = (implode(",",$parts));
										@endphp
										@endif
		
										@if(!empty($upper_vTeethInMouth))
										<tr>
											<td style="text-align: center;">
												<div style="width:100%; border-radius:5px;padding:1px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> TEETH IN MOUTH</div>
													<div class="teeth-counter-text">{{$upper_vTeethInMouth}}</div>
												</div>
											</td>
										</tr>
										@endif
										@if (!empty(array_filter($upper_vMissingTeeth)))
										@php
										$parts=array_filter($upper_vMissingTeeth);
										$upper_vMissingTeeth = (implode(",",$parts));
										@endphp
										<tr>
											<td style="text-align: center;">
												<div style="width:100%; border-radius:5px;padding:1px;">
													<div style="margin-bottom: 1px;" class="inner-font-size"> MISSING TEETH</div>
													<div class="teeth-counter-text">{{$upper_vMissingTeeth}}</div>
												</div>
											</td>
										</tr>
										@elseif(isset($upper_vMissingTeethOp))
										@if (!empty(array_filter($upper_vMissingTeethOp)))
										<tr>
											<td style="text-align: center;">
												<div style="width:100%; border-radius:5px;padding:1px;">
													<div style="margin-bottom: 1px;" class="inner-font-size"> MISSING TEETH</div>
													<div class="teeth-counter-text">{{$upper_vMissingTeethOp}}</div>
												</div>
											</td>
										</tr>
										@endif
										@endif
										@if (!empty(array_filter($upper_vWillExtractOnDelivery)))
										@php
										$parts=array_filter($upper_vWillExtractOnDelivery);
										$upper_vWillExtractOnDelivery = (implode(",",$parts));
										@endphp
										<tr>
											<td style=" text-align: center;">
												<div style="width:100%; border-radius:5px;padding:1px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> WILL EXTRACT ON DELIVERY </div>
													<div class="teeth-counter-text">{{$upper_vWillExtractOnDelivery}}</div>
												</div>
											</td>
										</tr>
										@elseif(isset($upper_vWillExtractOnDeliveryOp))
										@if (!empty(array_filter($upper_vWillExtractOnDeliveryOp)))
										<tr>
											<td style=" text-align: center;">
												<div style="width:100%; border-radius:5px;padding:1px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> WILL EXTRACT ON DELIVERY </div>
													<div class="teeth-counter-text">{{$upper_vWillExtractOnDeliveryOp}}</div>
												</div>
											</td>
										</tr>
										@endif
										@endif
										@if (!empty(array_filter($upper_vHasBeenExtracted)))
										@php
										$parts=array_filter($upper_vHasBeenExtracted);
										$upper_vHasBeenExtracted = (implode(",",$parts));
										@endphp
										<tr>
											<td style="text-align: center;">
												<div style="width:100%; border-radius:5px;padding:1px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> HAS BEEN EXTRACTED</div>
													<div class="teeth-counter-text">{{$upper_vHasBeenExtracted}}</div>
												</div>
											</td>
										</tr>
										@elseif(isset($upper_vHasBeenExtractedOp))
										@if (!empty(array_filter($upper_vHasBeenExtractedOp)))
										<tr>
											<td style=" text-align: center;">
												<div style="width:100%; border-radius:5px;padding:1px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> HAS BEEN EXTRACTED</div>
													<div class="teeth-counter-text">{{$upper_vHasBeenExtractedOp}}</div>
												</div>
											</td>
										</tr>
										@endif
										@endif
										<tr>
											<td>
												<table style="margin: 0px;padding: 0px;">
													<tr>
														@if (!empty(array_filter($upper_vFixOrAdd)))
														@php
														$parts=array_filter($upper_vFixOrAdd);
														$upper_vFixOrAdd = (implode(",",$parts));
														@endphp
														<td style="margin-left: 10px;">
															<div style=" border-radius:5px;padding:1px;width:130px;text-align: center;">
																<div style="margin-bottom:1px;" class="inner-font-size"> FIX OR ADD</div>
																<div class="teeth-counter-text" {{$upper_product->vFixOrAdd}}</div>
																</div>
														</td>
														@elseif(isset($upper_vFixOrAddOp))
														@if (!empty(array_filter($upper_vFixOrAddOp)))
														<td style="margin-right: 10px;">
															<div style=" border-radius:5px;padding:1px;width:130px;text-align: center;">
																<div style="margin-bottom:1px;" class="inner-font-size"> FIX OR ADD</div>
																<div class="teeth-counter-text">{{$upper_product->vFixOrAddOp}}</div>
															</div>
														</td>
														@endif
														@endif
		
														@if (!empty(array_filter($upper_vClasps)))
														@php
														$parts=array_filter($upper_vClasps);
														$upper_vClasps = (implode(",",$parts));
														@endphp
														<td>
															<div style=" border-radius:5px;padding:1px;width:115px;text-align: center;">
																<div style="margin-bottom: 1px;" class="inner-font-size">CLASPS</div>
																<div class="teeth-counter-text">{{$upper_vClasps}}</div>
															</div>
														</td>
														@elseif(isset($upper_vClaspsOp))
														@if (!empty(array_filter($upper_vClaspsOp)))
														<td>
															<div style=" border-radius:5px;padding:1px; width:115px;text-align: center;">
																<div style="margin-bottom: 1px;" class="inner-font-size">CLASPS</div>
																<div class="teeth-counter-text">{{$upper_vClaspsOp}}</div>
															</div>
														</td>
														@endif
														@endif
													</tr>
												</table>
											</td>
										</tr>
		
									</table>
								</td>
		
								<td style="padding:0px;vertical-align: top;text-align: center;">
									<?php $qr_code = "sknfknejkfnkenknekjnfkjenfkjn"; ?>
									<a style="text-align:left;display:inline-block;padding: 0px;" target="_blank" href="https://chart.googleapis.com/chart?chs=500x500&cht=qr&choe=UTF-8&chl=<?php echo $qr_code; ?>">
										<img src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&choe=UTF-8&chl=<?php echo $qr_code; ?>" style="height:80px;width:80px;display:inline-block;margin:0px auto 0px auto;padding: 0px;" download>
									</a>
								</td>
		
								<td style=" vertical-align: top;">
									<!-- table 2-3 -->
									<table style="vertical-align: top;">
										<tr>
											<td>
												<div style="margin: 0px auto 0px 0px;height:auto;width:100%;">
													<img src="{{asset('admin/assets/images/lowwer-teeth-pdf.png')}}" alt="mandibular-img" style="height:auto;width:100%;">
												</div>
											</td>
										</tr>
		
										@if((isset($lower_vTeethInMouth) AND isset($lower_vMissingTeeth) AND isset($lower_vWillExtractOnDelivery) AND isset($lower_vHasBeenExtracted) AND isset($lower_vFixOrAdd)) OR (isset($lower_vTeethInMouthOp) AND isset($lower_vMissingTeethOp) AND isset($lower_vWillExtractOnDeliveryOp) AND isset($lower_vHasBeenExtractedOp) AND isset($lower_vFixOrAddOp)))
		
										@if((isset($lower_vTeethInMouth) AND isset($lower_vMissingTeeth) AND isset($lower_vWillExtractOnDelivery) AND isset($lower_vHasBeenExtracted) AND isset($lower_vFixOrAdd)))
										@else
										@php
										$lower_vTeethInMouth = $lower_vTeethInMouthOp;
										$lower_vMissingTeeth = $lower_vMissingTeethOp;
										$lower_vWillExtractOnDelivery = $lower_vWillExtractOnDeliveryOp;
										$lower_vHasBeenExtracted = $lower_vHasBeenExtractedOp;
										$lower_vFixOrAdd = $lower_vFixOrAddOp;
										$lower_vClasps = $lower_vClaspsOp;
		
										@endphp
										@endif
										@endif
		
		
										@if(!empty($lower_vTeethInMouth))
										@php
										$parts=array_filter($lower_vTeethInMouth);
										$lower_vTeethInMouth = (implode(",",$parts));
										@endphp
										@endif
		
										@if(!empty($lower_vTeethInMouth))
										<tr>
											<td style="text-align: center;">
												<div style="width:100%; border-radius:5px;padding:1px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> TEETH IN MOUTH</div>
													<div class="teeth-counter-text">{{$lower_vTeethInMouth}}</div>
												</div>
											</td>
										</tr>
										@elseif(isset($lower_vTeethInMouth))
										@if (!empty(array_filter($upper_vMissingTeethOp)))
										<tr>
											<td style=" text-align: center;">
												<div style="width:100%; border-radius:5px;padding:1px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> TEETH IN MOUTH</div>
													<div class="teeth-counter-text">{{$lower_vTeethInMouthOp}}</div>
												</div>
											</td>
										</tr>
										@endif
										@endif
		
		
										@if (!empty(array_filter($lower_vMissingTeeth)))
										@php
										$parts=array_filter($lower_vMissingTeeth);
										$lower_vMissingTeeth = (implode(",",$parts));
										@endphp
										<tr>
											<td style=" text-align: center;">
												<div style=" width:100%; border-radius:5px;padding:1px;">
													<div class="inner-font-size"> MISSING TEETH</div>
													<div class="teeth-counter-text">{{$lower_vMissingTeeth}}</div>
												</div>
											</td>
										</tr>
										@elseif(isset($lower_vMissingTeethOp))
										@if (!empty(array_filter($lower_vMissingTeethOp)))
										<tr>
											<td style=" text-align: center;">
												<div style="width:100%; border-radius:5px;padding:1px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> MISSING TEETH</div>
													<div class="teeth-counter-text">{{$lower_vMissingTeethOp}}</div>
												</div>
											</td>
										</tr>
										@endif
										@endif
										@if (!empty(array_filter($lower_vWillExtractOnDelivery)))
										@php
										$parts=array_filter($lower_vWillExtractOnDelivery);
										$lower_vWillExtractOnDelivery = (implode(",",$parts));
										@endphp
										<tr>
											<td style=" text-align: center;">
												<div style=" width:100%;font-size: 10px; border-radius:5px;padding:1px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> WILL EXTRACT ON DELIVERY </div>
													<div class="teeth-counter-text">{{$lower_vWillExtractOnDelivery}}</div>
												</div>
											</td>
										</tr>
										@elseif(isset($lower_vWillExtractOnDeliveryOp))
										@if (!empty(array_filter($lower_vWillExtractOnDeliveryOp)))
		
										<tr>
											<td style=" text-align: center;">
												<div style=" width:100%; border-radius:5px;padding:1px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> WILL EXTRACT ON DELIVERY </div>
													<div class="teeth-counter-text">{{$lower_vWillExtractOnDeliveryOp}}</div>
												</div>
											</td>
										</tr>
										@endif
										@endif
										@if (!empty(array_filter($lower_vHasBeenExtracted)))
										@php
										$parts=array_filter($lower_vHasBeenExtracted);
										$lower_vHasBeenExtracted = (implode(",",$parts));
										@endphp
										<tr>
											<td style="text-align: center;">
												<div style="width:100%;padding:1px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> HAS BEEN EXTRACTED</div>
													<div class="teeth-counter-text">{{$lower_vHasBeenExtracted}}</div>
												</div>
											</td>
										</tr>
										@elseif(isset($lower_vHasBeenExtractedOp))
										@if (!empty(array_filter($lower_vHasBeenExtractedOp)))
										<tr>
											<td style="text-align: center;">
												<div style="width:100%;padding:1px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> HAS BEEN EXTRACTED</div>
													<div class="teeth-counter-text">{{$lower_vHasBeenExtractedOp}}</div>
												</div>
											</td>
										</tr>
										@endif
										@endif
										<tr>
											<table style="margin: 0px 0px 0px 0px;padding: 0px;width:100%;">
												<tr>
													@if (!empty(array_filter($lower_vFixOrAdd)))
													@php
													$parts=array_filter($lower_vFixOrAdd);
													$lower_vFixOrAdd = (implode(",",$parts));
													@endphp
													<td>
														<div style=padding:1px; width:120px;text-align: center;">
															<div style="margin-bottom:1px;" class="inner-font-size"> FIX OR ADD</div>
															<div class="teeth-counter-text">{{$lower_vFixOrAdd}}</div>
														</div>
													</td>
													@elseif(isset($lower_vFixOrAddOp))
													@if (!empty(array_filter($lower_vFixOrAddOp)))
													<td>
														<div style="padding:1px; width:120px;text-align: center;">
															<div style="margin-bottom:1px;" class="inner-font-size"> FIX OR ADD</div>
															<div class="teeth-counter-text">{{$lower_vFixOrAddOp}}</div>
														</div>
													</td>
													@endif
													@endif
		
													@if (!empty(array_filter($lower_vClasps)))
													@php
													$parts=array_filter($lower_vClasps);
													$lower_vClasps = (implode(",",$parts));
													@endphp
													<td>
														<div style="padding:1px;width:115px;margin-right: auto;text-align: center;">
															<div style="margin-bottom:1px;" class="inner-font-size">CLASPS</div>
															<div class="teeth-counter-text">{{$lower_vClasps}}</div>
														</div>
													</td>
													@elseif(isset($lower_vClaspsOp))
													@if (!empty(array_filter($lower_vClaspsOp)))
													<td>
														<div style="padding:1px;width:115px;margin-right: auto;text-align: center;">
															<div style="margin-bottom:1px;" class="inner-font-size">CLASPS</div>
															<div class="teeth-counter-text">{{$lower_vClaspsOp}}</div>
														</div>
													</td>
													@endif
													@endif
												</tr>
											</table>
										</tr>
		
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			@if(empty($Slip->tStagesNotes))
			@php
			$tStagesNotes = "";
			@endphp
			@else
			@php
			$tStagesNotes = $Slip->tStagesNotes;
			@endphp
			@endif
			<table style="max-width:700px;width:550px; margin: 0 auto 0px 0px;border-collapse: collapse;">
				<tr>
					<td style="vertical-align:top;padding:0px;border-radius:5px;">
						<table style="margin: 0px;width: 550px;border-bottom:1px solid #000;">
							<!-- <tr>
								<td style="text-align: center;">
									<span>STAGES NOTES </span>
								</td>
							</tr> -->
							<tr>
								<td style="padding: 5px 5px; text-align: center">
									<div style="text-align: center;border-radius:5px;padding:5px 10px;width:98%;">
										<h6 style="margin: 0px;padding: 0px;font-size: 8px;">STAGE NOTES</h6>
										<p style="text-align: justify;font-size: 8px;">
											{{$tStagesNotes}}
										</p>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;padding-top: 20px;" colspan="2">
						<div style="text-align: right;margin-bottom: 5px;">
							@if(isset($Doctor))
							<img alt="dr.'s signature" style="width:150px;height:50px;margin: auto;object-fit: contain;" / id="img" value="@if(old('vImage') == 'vImage') selected @endif" src="{{asset('uploads/doctor/'.$Doctor->vImage)}}">
							@else
							<img alt="dr.'s signature" style="width:150px;height:50px;margin: auto;object-fit: contain;" / id="img" value="" src="{{asset('images/no-image.gif')}}">
							@endif
		
						</div>
						<span style="border-top: 1px solid #000;font-size:11px;">Dr’s signature & license # {{$Doctor->vLicence}}</span>
					</td>
				</tr>
			</table>

	</body>
</html>
