@php
    $eEditStageAccess = \App\Libraries\General::check_permission_slip('eEditStage');
    $eNewStageAccess = \App\Libraries\General::check_permission_slip('eNewStage');
    $eReadyToSendAccess = \App\Libraries\General::check_permission_slip('eReadyToSend');
    $eViewDriverHistoryAccess = \App\Libraries\General::check_permission_slip('eViewDriverHistory');
    $ePickUpAccess = \App\Libraries\General::check_permission_slip('ePickUp');
    $eDropOffAccess = \App\Libraries\General::check_permission_slip('eDropOff');
    $eDirectionAccess = \App\Libraries\General::check_permission_slip('eDirection');
    $eAddCallLogAccess = \App\Libraries\General::check_permission_slip('eAddCallLog');
    $eHistoryCallAccess = \App\Libraries\General::check_permission_slip('eHistoryCall');
    $eLastModifiedAccess = \App\Libraries\General::check_permission_slip('eLastModified');
@endphp

@extends('layouts.admin.index')
@section('content')
<style>
    .rush-a::before {
        background:url("{{asset('admin/assets/images/rush-bg.png')}}");
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center;
    }

    .error-icon i {
        font-size: 18px;
        color: orange;
    }


    .tooltip-inner {
        background-color: orange;
        color: #fff;
    }


    .tooltip-arrow::before {
        border-right-color: orange !important;
        border-left-color: orange !important;
    }
    .swal-title{
        font-size: 20px;
    }
    .swal-icon{
        width: 50px;
        height: 50px;
    }
    .swal-icon--warning__body {
    width: 5px;
    height: 20px;
    top: 10px;
    border-radius: 2px;
    margin-left: -2px;
    }

    .swal-button--danger {
        color: #fff;
        font-size: 14px;
        min-width: 140px;
        border: 1px solid #2a78cf !important;
        background-image: linear-gradient(to right, black, #0e66b2 400px, black 800px);
        background-size: 800px 100%;
        background-position: 50% 100%;
        background-repeat: no-repeat;
        border-radius: 5px;
        padding: 5px !important;
    }

    .swal-button--cancel {
        border-radius: 5px;
        padding: 5px !important;
        font-size: 14px;
        min-width: 140px;
    }

    .swal-footer {
        text-align: center;
    }

    #tDescription_Reff .form-control{
        position: relative;
    width: 100%;
    padding-top: 0px;    
    background-color: #eff2f5;
    }

    .virual-slip-wrapper .modal-content table th {
    color: #101220 !important;
    }
</style>
<!-- Modal -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">
<div class="virual-slip-wrapper">
    <div class="modal fade" id="virtual_slip_model" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="">
        <div class="modal-dialog" style="clear: both;
    background-color: rgb(255, 255, 255);
    color: rgb(27, 56, 82);
    width: 1000px;
    max-width: 1000px;
    max-height: 820px;
    box-shadow: rgba(0, 0, 0, 0.1) 0px 0.5rem 1rem;
    margin: 0px auto;
    padding: 0px;
    overflow: auto;
    border-width: 1px;
    border-style: solid;
    border-color: rgba(0, 0, 0, 0.2);
    border-image: initial;
    position: relative;">


            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@if(isset($CustomerLabData->vOfficeName) && !empty($CustomerLabData->vOfficeName)){{$CustomerLabData->vOfficeName}}@endif</h5>
                    <button type="button" class="btn-close d-none" data-bs-dismiss="modal" id="back" aria-label="Close"></button>
                    <ul class="icons">
                        <li class="print_paper" data-case="{{$Case->iCaseId}}" data-slip="{{$Slip->iSlipId}}"><a  href="#" class="me-3"><i class="fal fa-print" style="font-size: 20px;color: white;"></i></a></li>
                        <li class="me-3 d-none"><a href="#"><img src="{{asset('admin/assets/images/Asset 2.png')}}" alt=""></a></li>
                        <li><a href="#"><i class="fal fa-file-invoice-dollar" style="font-size: 20px;color: white;"></i></a></li>
                        <li class="me-3 d-none"><a href="#"><img src="{{asset('admin/assets/images/Asset 4.png')}}" alt=""></a></li>
                        <li class="me-3 d-none"><a href="javascript:;" id="image_popup"><img src="{{asset('admin/assets/images/Asset 5.png')}}" alt=""></a></li>
                        <li class="d-none"><a href="#"><img src="{{asset('admin/assets/images/Asset 6.png')}}" alt=""></a></li>
                    </ul>
                </div>

                <div style="width:100%;padding:0;margin:0 0 3px 0; border-bottom:1px solid #e5e5e5;">
                    <table class="tslip">
                        <tbody>
                            <tr>
                                <th>Office:</th>
                                <td><span id="LabContent_lbcaseid">@if(isset($CustomerOfficeData->vOfficeName) && !empty($CustomerOfficeData->vOfficeName)){{$CustomerOfficeData->vOfficeName}}@endif</span></td>
                                <th>Pan #</th>
                                <td><span class="badge d-inline-block" style="background-color:{{ $Slip->vColor }}">@if(!empty($Case->vCasePanNumber)){{$Case->vCasePanNumber}}@else{{"----"}}@endif </span></td>
                                <th>Created by</th>
                                <td><span id="LabContent_lboffice">{{$Case->vCreatedByName}}</span></td>
                                <th>Pick up Date</th>
                                <td>
                                    <span id="LabContent_lbduedate">
                                        @if(isset($final_Pickup_date))
                                            {{ date('m/d/Y', strtotime($final_Pickup_date)) }} 
                                        @else 
                                            @if (!empty($lower_product) AND empty($upper_product))
                                                {{date('m/d/Y', strtotime($lower_product->dPickUpDate))}}
                                            @elseif(!empty($upper_product) AND empty($lower_product))
                                                {{date('m/d/Y', strtotime($upper_product->dPickUpDate))}}
                                            @elseif(!empty($lower_product) && !empty($upper_product))
                                                @if($upper_product->dPickUpDate > $lower_product->dPickUpDate)
                                                    {{date('m/d/Y', strtotime($upper_product->dPickUpDate))}}
                                                @elseif($upper_product->dPickUpDate < $lower_product->dPickUpDate)
                                                    {{date('m/d/Y', strtotime($lower_product->dPickUpDate))}}
                                                @elseif($upper_product->dPickUpDate == $lower_product->dPickUpDate)
                                                    {{date('m/d/Y', strtotime($upper_product->dPickUpDate))}}
                                                @endif
                                            @endif
                                        @endif
                                    </span>
                                </td>
                                <td>
                                    <a href="#">
                                        <i class="fad fa-calendar-alt"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Dr:</th>
                                <td><span>{{$Case->vFirstName." ".$Case->vLastName}}</span></td>

                                <th>Case #</th>
                                <td><span>{{$Case->vCaseNumber}} </span></td>
                                <th>Location</th>
                                <td><span>{{$Slip->vLocation}}</span></td>
                                <th>Delivery Date </th>
                                <td><span>
                                    @if((isset($final_delivery_date) && (isset($is_single_product) && $is_single_product =='yes')) && ((isset($upper_product->eCustomDate) && $upper_product->eCustomDate !='Yes') || (isset($lower_product->eCustomDate) && $lower_product->eCustomDate !='Yes')))
                                    {{ date('m/d/Y', strtotime($final_delivery_date)) }} 
                                    @else {{"-----"}} @endif
                                    </span>
                                </td>
                                <td>
                                    <a href="#">
                                        <i class="fal fa-random"></i>
                                        <!-- <i class="fas fa-bacon"></i> -->
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Patient:</th>
                                <td><span>{{$Case->vPatientName}}</span></td>

                                <th>Slip #</th>
                                <td><span>{{$Slip->vSlipNumber}} </span></td>
                                <th>Case status</th>
                                <td><span>{{$Case->eStatus}}</span></td>
                                <th>Delivery Time </th>
                                <td>
                                    <span>
                                        @if((isset($final_delivery_time) && (isset($is_single_product) && $is_single_product =='yes')) && ((isset($upper_product->eCustomDate) && $upper_product->eCustomDate !='Yes') || (isset($lower_product->eCustomDate) && $lower_product->eCustomDate !='Yes')) )
                                            {{ date("h:i",strtotime($final_delivery_time))}} 
                                        @else 
                                            {{"-----"}} 
                                        @endif
                                    </span>
                                </td>
                                <td>
                                    <a href="#">
                                        <i class="fal fa-clock"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="modal-body">
                    @php
                    $hide_product = 'no';    
                    @endphp
                    @if(isset($Case->eStatus) && $Case->eStatus == "Canceled")
                    @php
                    $hide_product = 'yes';    
                    @endphp
                    <div class="cen-section full" style="transform:rotate(0deg)!important;width: 100%;top: 30%;text-align: center;">
                        <p class="m-0 py-3">
                            This slip was canceled 
                        </p>
                    </div>
                    @elseif(isset($Case->eStatus) && $Case->eStatus == "On Hold")
                    @php
                    $hide_product = 'yes';    
                    @endphp
                    <div class="cen-section full" style="transform:rotate(0deg)!important;width: 100%;top: 30%;text-align: center;">
                        <p class="m-0 py-3">
                            This slip is on hold 
                        </p>
                    </div>
                    @endif

                    <table class="tslip">

                        <tbody>
                            <tr>
                                <!-- view upper teeth start -->
                                @if((isset($upper_vTeethInMouth) AND isset($upper_vMissingTeeth) AND isset($upper_vWillExtractOnDelivery) AND isset($upper_vHasBeenExtracted) AND isset($upper_vFixOrAdd)) OR (isset($upper_vTeethInMouthOp) AND isset($upper_vMissingTeethOp) AND isset($upper_vWillExtractOnDeliveryOp) AND isset($upper_vHasBeenExtractedOp) AND isset($upper_vFixOrAddOp)))

                                @if((isset($upper_vTeethInMouth) AND isset($upper_vMissingTeeth) AND isset($upper_vWillExtractOnDelivery) AND isset($upper_vHasBeenExtracted) AND isset($upper_vFixOrAdd)))
                                @else
                                @php
                                $upper_vTeethInMouth = $upper_vTeethInMouthOp;
                                $upper_vMissingTeeth = $upper_vMissingTeethOp;
                                $upper_vWillExtractOnDelivery = $upper_vWillExtractOnDeliveryOp;
                                $upper_vHasBeenExtracted = $upper_vHasBeenExtractedOp;
                                $upper_vFixOrAdd = $upper_vFixOrAddOp;
                                $upper_vClasps = $upper_vClaspsOp;

                                @endphp
                                @endif
                                <td style="width:290px;text-align:center;vertical-align:top;"> 
                                @if((isset($upper_product->eStatus) && $upper_product->eStatus == "Canceled") && $hide_product == 'no')
                                
                                    <div class="cen-section upper">
                                        <p class="m-0">
                                        This stage was canceled 
                                        </p>
                                    </div>
                                @elseif((isset($upper_product->eStatus) && $upper_product->eStatus == "On Hold") && $hide_product == 'no')
                                    <div class="cen-section upper">
                                        <p class="m-0">
                                        This stage is on hold
                                        </p>
                                    </div>
                                @endif





                                    <div class="teeth-wrapper upper" id="upper_extraction">

                                        <h3 class="card-title mb-0">
                                            MAXILLARY
                                        </h3>

                                        <svg width="100%" height="100%" viewBox="0 0 1500 822" class="teeth-svg">

                                            <g>

                                                <g>
                                                    <!-- upper-teeth 5 -->
                                                    <a href="javascript:;">
                                                        <image id="uppper-5-teeth" data-id="teeths3" data-name="5" x="325" y="202" width="149" height="152" xlink:href="
                                                        
                                                        @if (in_array('5', $upper_vTeethInMouth))
                                                        {{asset('admin/assets/images/yellow-teeth/up-5.png')}}
                                                        @endif
                                                        @if (in_array('5', $upper_vMissingTeeth))
                                                        {{asset('admin/assets/images/teeth/up-5.png')}}
                                                        @endif
                                                        @if (in_array('5', $upper_vWillExtractOnDelivery))
                                                        {{asset('admin/assets/images/red-teeth/up-5.png')}}
                                                        @endif
                                                        @if (in_array('5', $upper_vHasBeenExtracted))
                                                        {{asset('admin/assets/images/grey-teeth/up-5.png')}}
                                                        @endif
                                                        @if (in_array('5', $upper_vFixOrAdd))
                                                        {{asset('admin/assets/images/green-teeth/up-5.png')}}
                                                        @endif

                                                        " class="img-for-teeth teeth_uppper"></image>

                                                        <text id="uppper-5-num" data-name="5" class="cls-1 teeth_uppper" transform="translate(405.696 287.468) scale(1.305)">
                                                            <tspan x="0">5</tspan>
                                                        </text>
                                                        <image id="uppper-5-claps" data-id="teeths3" data-name="5" x="315" y="190" xlink:href="{{asset('admin/assets/images/teeth/updc-5.png')}}" class=" clap-up-5 teeth_uppper upper-claps" style="
                                                @if (in_array('5', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>
                                                    </a>
                                                </g>
                                                <g>
                                                    <!-- upper-teeth 4 -->
                                                    <a href="javascript:;">
                                                        <image id="uppper-4-teeth" data-id="teeths4" data-name="4" x="266" y="285" width="153" height="149" xlink:href="

                                                        @if (in_array('4', $upper_vTeethInMouth))
                                                        {{asset('admin/assets/images/yellow-teeth/up-4.png')}}
                                                        @endif
                                                        @if (in_array('4', $upper_vMissingTeeth))
                                                        {{asset('admin/assets/images/teeth/up-4.png')}}
                                                        @endif
                                                        @if (in_array('4', $upper_vWillExtractOnDelivery))
                                                        {{asset('admin/assets/images/red-teeth/up-4.png')}}
                                                        @endif
                                                        @if (in_array('4', $upper_vHasBeenExtracted))
                                                        {{asset('admin/assets/images/grey-teeth/up-4.png')}}
                                                        @endif
                                                        @if (in_array('4', $upper_vFixOrAdd))
                                                        {{asset('admin/assets/images/green-teeth/up-4.png')}}
                                                        @endif
                                                        " class="img-for-teeth teeth_uppper"></image>

                                                        <text id="uppper-4-num" data-name="4" class="cls-1 teeth_uppper" transform="translate(346.65 357.771) scale(1.305)">
                                                            <tspan x="0">4</tspan>
                                                        </text>
                                                        <image id="uppper-4-claps" data-id="teeths4" data-name="4" x="250" y="280" xlink:href="{{asset('admin/assets/images/teeth/updc-4.png')}}" class=" clap-up-4 teeth_uppper upper-claps" style="

                                                @if (in_array('4', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>
                                                    </a>
                                                </g>
                                                <g>
                                                    <!-- upper-teeth 3 -->
                                                    <a href="javascript:;">
                                                        <image id="uppper-3-teeth" data-id="teeths7" data-name="3" x="176" y="367" width="220" height="199" xlink:href="
                                                        
                                                        @if (in_array('3', $upper_vTeethInMouth))
                                                        {{asset('admin/assets/images/yellow-teeth/up-3.png')}}
                                                        @endif
                                                        @if (in_array('3', $upper_vMissingTeeth))
                                                        {{asset('admin/assets/images/teeth/up-3.png')}}
                                                        @endif
                                                        @if (in_array('3', $upper_vWillExtractOnDelivery))
                                                        {{asset('admin/assets/images/red-teeth/up-3.png')}}
                                                        @endif
                                                        @if (in_array('3', $upper_vHasBeenExtracted))
                                                        {{asset('admin/assets/images/grey-teeth/up-3.png')}}
                                                        @endif
                                                        @if (in_array('3', $upper_vFixOrAdd))
                                                        {{asset('admin/assets/images/green-teeth/up-3.png')}}
                                                        @endif
                                                        
                                                        " class="img-for-teeth teeth_uppper"></image>

                                                        <text id="uppper-3-num" data-name="3" class="cls-1 teeth_uppper" transform="translate(289.371 467.923) scale(1.305)">
                                                            <tspan x="0">3</tspan>
                                                        </text>

                                                        <image id="uppper-3-claps" data-id="teeths7" data-name="3" x="160" y="367" xlink:href="{{asset('admin/assets/images/teeth/updc-3.png')}}" class=" clap-up-3 teeth_uppper upper-claps" style="
                                                
                                                @if (in_array('3', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>
                                                    </a>
                                                </g>
                                                <g>
                                                    <!-- upper-teeth 2 -->
                                                    <a href="javascript:;" class="test">
                                                        <image id="uppper-2-teeth" data-id="teeths6" data-name="2" x="115" y="500" width="203" height="188" xlink:href="
                                                        
                                                        @if (in_array('2', $upper_vTeethInMouth))
                                                        {{asset('admin/assets/images/yellow-teeth/up-2.png')}}
                                                        @endif
                                                        @if (in_array('2', $upper_vMissingTeeth))
                                                        {{asset('admin/assets/images/teeth/up-2.png')}}
                                                        @endif
                                                        @if (in_array('2', $upper_vWillExtractOnDelivery))
                                                        {{asset('admin/assets/images/red-teeth/up-2.png')}}
                                                        @endif
                                                        @if (in_array('2', $upper_vHasBeenExtracted))
                                                        {{asset('admin/assets/images/grey-teeth/up-2.png')}}
                                                        @endif
                                                        @if (in_array('2', $upper_vFixOrAdd))
                                                        {{asset('admin/assets/images/green-teeth/up-2.png')}}
                                                        @endif
                                                        
                                                        " class="img-for-teeth teeth_uppper"></image>

                                                        <image id="uppper-2-claps" data-id="teeths6" data-name="2" x="100" y="500" xlink:href="{{asset('admin/assets/images/teeth/updc-2.png')}}" class=" clap-up-2 teeth_uppper upper-claps" style="
                                                
                                                @if (in_array('2', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>

                                                        <text id="uppper-2-num" data-name="2" class="cls-1 teeth_uppper" transform="translate(224.87 609.191) scale(1.305)">
                                                            <tspan x="0">2</tspan>
                                                        </text>
                                                    </a>
                                                </g>

                                                <g>
                                                    <!-- upper-teeth 1 -->
                                                    <a href="javascript:;" class="test">

                                                        <image id="uppper-1-teeth" data-id="teeths5" data-name="1" x="57" y="640" width="192" height="156" xlink:href="
                                                        
                                                        @if (in_array('1', $upper_vTeethInMouth))
                                                        {{asset('admin/assets/images/yellow-teeth/up-1.png')}}
                                                        @endif
                                                        @if (in_array('1', $upper_vMissingTeeth))
                                                        {{asset('admin/assets/images/teeth/up-1.png')}}
                                                        @endif
                                                        @if (in_array('1', $upper_vWillExtractOnDelivery))
                                                        {{asset('admin/assets/images/red-teeth/up-1.png')}}
                                                        @endif
                                                        @if (in_array('1', $upper_vHasBeenExtracted))
                                                        {{asset('admin/assets/images/grey-teeth/up-1.png')}}
                                                        @endif
                                                        @if (in_array('1', $upper_vFixOrAdd))
                                                        {{asset('admin/assets/images/green-teeth/up-1.png')}}
                                                        @endif
                                                        
                                                        
                                                        " class="img-for-teeth teeth_uppper"></image>


                                                        <text id="uppper-1-num" data-name="1" data-id="teeths5" class="cls-1 teeth_uppper" transform="translate(163.9 742.356) scale(1.305)">
                                                            <tspan x="0">1</tspan>
                                                        </text>

                                                        <image id="uppper-1-claps" data-id="teeths5" data-name="1" x="40" y="640" xlink:href="{{asset('admin/assets/images/teeth/up-dc-1.png')}}" class=" clap-up-1 teeth_uppper upper-claps" style="
                                                @if (in_array('1', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>

                                                    </a>
                                                    <!-- upper-teeth 1 end-->
                                                </g>


                                                <g>
                                                    <!-- upper-teeth 6 -->
                                                    <a href="javascript:;">
                                                        <image id="uppper-6-teeth" data-id="teeths2" data-name="6" x="403" y="123" width="136" height="178" xlink:href="
                                                        
                                                        @if (in_array('6', $upper_vTeethInMouth))
                                                        {{asset('admin/assets/images/yellow-teeth/up-6.png')}}
                                                        @endif
                                                        @if (in_array('6', $upper_vMissingTeeth))
                                                        {{asset('admin/assets/images/teeth/up-6.png')}}
                                                        @endif
                                                        @if (in_array('6', $upper_vWillExtractOnDelivery))
                                                        {{asset('admin/assets/images/red-teeth/up-6.png')}}
                                                        @endif
                                                        @if (in_array('6', $upper_vHasBeenExtracted))
                                                        {{asset('admin/assets/images/grey-teeth/up-6.png')}}
                                                        @endif
                                                        @if (in_array('6', $upper_vFixOrAdd))
                                                        {{asset('admin/assets/images/green-teeth/up-6.png')}}
                                                        @endif
                                                        
                                                        " class="img-for-teeth teeth_uppper"></image>
                                                        <image id="uppper-6-claps" data-id="teeths2" data-name="6" x="400" y="105" xlink:href="{{asset('admin/assets/images/teeth/updc-6.png')}}" class=" clap-up-6 teeth_uppper upper-claps" style="
                                                @if (in_array('6', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>

                                                        <text id="uppper-6-num" data-name="6" class="cls-1 teeth_uppper" transform="translate(476.76 240.092) scale(1.305)">
                                                            <tspan x="0">6</tspan>
                                                        </text>
                                                    </a>
                                                </g>



                                                <!-- upper-teeth 7 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-7-teeth" data-id="teeths1" data-name="7" x="498" y="72" width="125" height="189" xlink:href="
                                                    
                                                    @if (in_array('7', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-7.png')}}
                                                    @endif
                                                    @if (in_array('7', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-7.png')}}
                                                    @endif
                                                    @if (in_array('7', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-7.png')}}
                                                    @endif
                                                    @if (in_array('7', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-7.png')}}
                                                    @endif
                                                    @if (in_array('7', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-7.png')}}
                                                    @endif

                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-7-claps" data-id="teeths1" data-name="7" x="498" y="50" xlink:href="{{asset('admin/assets/images/teeth/updc-7.png')}}" class=" clap-up-7 teeth_uppper upper-claps" style="
                                        @if (in_array('7', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif

                                        "></image>
                                                <text id="uppper-7-num" data-name="7" class="cls-1 teeth_uppper" transform="translate(563.111 198.624) scale(1.305)">
                                                    <tspan x="0">7</tspan>
                                                </text>

                                                <!-- upper-teeth 8 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-8-teeth" data-id="teeths8" data-name="8" x="607" y="35" xlink:href="
                                                    
                                                    @if (in_array('8', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-8.png')}}
                                                    @endif
                                                    @if (in_array('8', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-8.png')}}
                                                    @endif
                                                    @if (in_array('8', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-8.png')}}
                                                    @endif
                                                    @if (in_array('8', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-8.png')}}
                                                    @endif
                                                    @if (in_array('8', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-8.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-8-claps" data-id="teeths8" data-name="8" x="607" y="10" xlink:href="{{asset('admin/assets/images/teeth/updc-8.png')}}" class=" clap-up-7 teeth_uppper upper-claps" style="
                                        @if (in_array('8', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-8-num" data-name="8" class="cls-1 teeth_uppper" transform="translate(697.646 179.813) scale(1.305)">
                                                    <tspan x="0">8</tspan>
                                                </text>

                                                <!-- upper-teeth 9 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-9-teeth" data-id="teeths16" data-name="9" x="755" y="35" width="150" height="210" xlink:href="

                                                    @if (in_array('9', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-9.png')}}
                                                    @endif
                                                    @if (in_array('9', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-9.png')}}
                                                    @endif
                                                    @if (in_array('9', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-9.png')}}
                                                    @endif
                                                    @if (in_array('9', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-9.png')}}
                                                    @endif
                                                    @if (in_array('9', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-9.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper "></image>
                                                </a>

                                                <image id="uppper-9-claps" data-id="teeths16" data-name="9" x="765" y="10" xlink:href="{{asset('admin/assets/images/teeth/updc-9.png')}}" class=" clap-up-9 teeth_uppper upper-claps" style="

                                        @if (in_array('9', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-9-num" data-name="9" class="cls-1 teeth_uppper" transform="translate(821.604 176.548) scale(1.305)">
                                                    <tspan x="0">9</tspan>
                                                </text>

                                                <!-- upper-teeth 10 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-10-teeth" data-id="teeths9" data-name="10" x="889" y="72" width="125" height="189" xlink:href="

                                                    @if (in_array('10', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-10.png')}}
                                                    @endif
                                                    @if (in_array('10', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-10.png')}}
                                                    @endif
                                                    @if (in_array('10', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-10.png')}}
                                                    @endif
                                                    @if (in_array('10', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-10.png')}}
                                                    @endif
                                                    @if (in_array('10', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-10.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-10-claps" data-id="teeths9" data-name="10" x="910" y="50" xlink:href="{{asset('admin/assets/images/teeth/updc-10.png')}}" class=" clap-up-10 teeth_uppper upper-claps" style="
                                        @if (in_array('10', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif

                                        "></image>
                                                <text id="uppper-10-num" data-name="10" class="cls-1 teeth_uppper" transform="translate(934.785 195.164) scale(1.305)">
                                                    <tspan x="0">10</tspan>
                                                </text>

                                                <!-- upper-teeth 11 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-11-teeth" data-id="teeths10" data-name="11" x="972" y="123" width="137" height="178" xlink:href=" 
                                                    
                                                    @if (in_array('11', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-11.png')}}
                                                    @endif
                                                    @if (in_array('11', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-11.png')}}
                                                    @endif
                                                    @if (in_array('11', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-11.png')}}
                                                    @endif
                                                    @if (in_array('11', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-11.png')}}
                                                    @endif
                                                    @if (in_array('11', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-11.png')}}
                                                    @endif
                                                    
                                                    "class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-11-claps" data-id="teeths10" data-name="11" x="1030" y="100" xlink:href="{{asset('admin/assets/images/teeth/updc-11.png')}}" class=" clap-up-11 teeth_uppper upper-claps" style="
                                        
                                        @if (in_array('11', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-11-num" data-name="11" class="cls-1 teeth_uppper" transform="translate(1031.677 240.099) scale(1.305)">
                                                    <tspan x="0">11</tspan>
                                                </text>

                                                <!-- upper-teeth 12 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-12-teeth" data-id="teeths11" data-name="12" x="1040" y="198" width="149" height="152" xlink:href="
                                                    
                                                    @if (in_array('12', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-12.png')}}
                                                    @endif
                                                    @if (in_array('12', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-12.png')}}
                                                    @endif
                                                    @if (in_array('12', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-12.png')}}
                                                    @endif
                                                    @if (in_array('12', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-12.png')}}
                                                    @endif
                                                    @if (in_array('12', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-12.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-12-claps" data-id="teeths11" data-name="12" x="1115" y="175" xlink:href="{{asset('admin/assets/images/teeth/updc-12.png')}}" class=" clap-up-12 teeth_uppper upper-claps upper-claps" style="
                                            
                                            @if (in_array('12', $upper_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif
                                            
                                            "></image>
                                                <text id="uppper-12-num" data-name="12" class="cls-1 teeth_uppper" transform="translate(1103.168 282.619) scale(1.305)">
                                                    <tspan x="0">12</tspan>
                                                </text>

                                                <!-- upper-teeth 13 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-13-teeth" data-id="teeths12" data-name="13" x="1095" y="284" width="154" height="149" xlink:href="
                                                    
                                                    @if (in_array('13', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-13.png')}}
                                                    @endif
                                                    @if (in_array('13', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-13.png')}}
                                                    @endif
                                                    @if (in_array('13', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-13.png')}}
                                                    @endif
                                                    @if (in_array('13', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-13.png')}}
                                                    @endif
                                                    @if (in_array('13', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-13.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-13-claps" data-id="teeths12" data-name="13" x="1215" y="270" xlink:href="{{asset('admin/assets/images/teeth/updc-13.png')}}" class=" clap-up-13 teeth_uppper upper-claps" style="
                                        @if (in_array('13', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-13-num" data-name="13" class="cls-1 teeth_uppper" transform="translate(1169.916 345.118) scale(1.305)">
                                                    <tspan x="0">13</tspan>
                                                </text>
                                                <!-- upper-teeth 14 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-14-teeth" data-id="teeths15" data-name="14" x="1115" y="361" width="221" height="199" xlink:href="
                                                    
                                                    @if (in_array('14', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-14.png')}}
                                                    @endif
                                                    @if (in_array('14', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-14.png')}}
                                                    @endif
                                                    @if (in_array('14', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-14.png')}}
                                                    @endif
                                                    @if (in_array('14', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-14.png')}}
                                                    @endif
                                                    @if (in_array('14', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-14.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-14-claps" data-id="teeths15" data-name="14" x="1280" y="361" xlink:href="{{asset('admin/assets/images/teeth/updc-14.png')}}" class=" clap-up-14 teeth_uppper upper-claps" style="

                                        @if (in_array('14', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-14-num" data-name="14" class="cls-1 teeth_uppper" transform="translate(1210.87 477.036) scale(1.305)">
                                                    <tspan x="0">14</tspan>
                                                </text>
                                                <!-- upper-teeth 15 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-15-teeth" data-id="teeths14" data-name="15" x="1194" y="503" width="203" height="188" xlink:href="

                                                    @if (in_array('15', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-15.png')}}
                                                    @endif
                                                    @if (in_array('15', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-15.png')}}
                                                    @endif
                                                    @if (in_array('15', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-15.png')}}
                                                    @endif
                                                    @if (in_array('15', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-15.png')}}
                                                    @endif
                                                    @if (in_array('15', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-15.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-15-claps" data-id="teeths14" data-name="15" x="1350" y="503" xlink:href="{{asset('admin/assets/images/teeth/updc-15.png')}}" class=" clap-up-15 teeth_uppper upper-claps" style="
                                        @if (in_array('15', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-15-nuum" data-name="15" class="cls-1 teeth_uppper" transform="translate(1303.838 603.41) scale(1.305)">
                                                    <tspan x="0">15</tspan>
                                                </text>

                                                <!-- upper-teeth 16 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-16-teeth" data-id="teeths13" data-name="16" x="1262" y="643" width="192" height="156" xlink:href="

                                                    @if (in_array('16', $upper_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/up-16.png')}}
                                                    @endif
                                                    @if (in_array('16', $upper_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/up-16.png')}}
                                                    @endif
                                                    @if (in_array('16', $upper_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/up-16.png')}}
                                                    @endif
                                                    @if (in_array('16', $upper_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/up-16.png')}}
                                                    @endif
                                                    @if (in_array('16', $upper_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/up-16.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_uppper"></image>
                                                </a>

                                                <image id="uppper-16-claps" data-id="teeths13" data-name="16" x="1422" y="643" xlink:href="{{asset('admin/assets/images/teeth/updc-16.png')}}" class=" clap-up-16 teeth_uppper upper-claps" style="
                                        @if (in_array('16', $upper_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="uppper-16-num" data-name="16" class="cls-1 teeth_uppper" transform="translate(1370.808 743.76) scale(1.305)">
                                                    <tspan x="0">16</tspan>
                                                </text>

                                                <!--upper teeth numbers  -->
                                            </g>
                                        </svg>

                                        <div class="d-block text-center teeth-text">
                                            <a href="javascript:;" class="add-btn p-2 mx-auto uppper_mising_all d-none" id="uppper_mising_all">
                                                Missing all teeth
                                            </a>
                                        </div>
                                        <div class="teeths-wrapper second-row p-0">
                                            <div class="row">
                                                <div class="col-md-12 col-xl-12 col-lg-12">

                                                    <div class="row gy-2">
                                                        @if(!empty($upper_vTeethInMouth))
                                                        @php
                                                        $parts=array_filter($upper_vTeethInMouth);
                                                        $upper_vTeethInMouth = (implode(",",$parts));
                                                        @endphp
                                                        @endif

                                                        @if(!empty($upper_vTeethInMouth))
                                                        <a href="javascript:;">
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <label class="form-control inner-label te-in-month lbl_btn_all" value="" id="upper_in_mouth" disabled> Teeth in mouth <br> <span id="upper_in_mouth_lbl">{{$upper_vTeethInMouth}}</span></label>
                                                                    <input type="hidden" id="upper_in_mouth_value" value="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                        @endif


                                                        @if (!empty(array_filter($upper_vMissingTeeth)))
                                                        @php
                                                        $parts=array_filter($upper_vMissingTeeth);
                                                        $upper_vMissingTeeth = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lbl_btn_all" value="" id="upper_missing_teeth" disabled>missing teeth <br> <span id="upper_missing_teeth_lbl">{{$upper_vMissingTeeth}}</span></label>
                                                                <input type="hidden" id="upper_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($upper_vMissingTeethOp))
                                                        @if (!empty(array_filter($upper_vMissingTeethOp)))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lbl_btn_all" value="" id="upper_missing_teeth" disabled>missing teeth <br> <span id="upper_missing_teeth_lbl">{{$upper_product->vMissingTeethOp}}</span></label>
                                                                <input type="hidden" id="upper_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif



                                                        @if (!empty(array_filter($upper_vWillExtractOnDelivery)))
                                                        @php
                                                        $parts=array_filter($upper_vWillExtractOnDelivery);
                                                        $upper_vWillExtractOnDelivery = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lbl_btn_all" value="" id="upper_ectract_delivery" disabled>will extract on delivery <br><span id="upper_ectract_delivery_lbl">{{$upper_vWillExtractOnDelivery}}</span></label>
                                                                <input type="hidden" id="upper_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($upper_vWillExtractOnDeliveryOp))
                                                        @if (!empty(array_filter($upper_vWillExtractOnDeliveryOp)))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lbl_btn_all" value="" id="upper_ectract_delivery" disabled>will extract on delivery <br><span id="upper_ectract_delivery_lbl">{{$upper_product->vWillExtractOnDeliveryOp}}</span></label>
                                                                <input type="hidden" id="upper_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif

                                                        @if (!empty(array_filter($upper_vHasBeenExtracted)))
                                                        @php
                                                        $parts=array_filter($upper_vHasBeenExtracted);
                                                        $upper_vHasBeenExtracted = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lbl_btn_all" value="" id="upper_been_extracted" disabled> has been extracted <br><span id="upper_been_extracted_lbl">{{$upper_vHasBeenExtracted}}</span></label>
                                                                <input type="hidden" id="upper_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($upper_vHasBeenExtractedOp))
                                                        @if (!empty(array_filter($upper_vHasBeenExtractedOp)))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lbl_btn_all" value="" id="upper_been_extracted" disabled> has been extracted <br><span id="upper_been_extracted_lbl">{{$upper_product->vHasBeenExtractedOp}}</span></label>
                                                                <input type="hidden" id="upper_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif



                                                        @if (!empty(array_filter($upper_vFixOrAdd)))
                                                        @php
                                                        $parts=array_filter($upper_vFixOrAdd);
                                                        $upper_vFixOrAdd = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lbl_btn_all" value="" id="upper_fix" disabled> fix or add <br> <span id="upper_fix_lbl">{{$upper_vFixOrAdd}}</span></label>
                                                                <input type="hidden" id="upper_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($upper_vFixOrAddOp))
                                                        @if (!empty(array_filter($upper_vFixOrAddOp)))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lbl_btn_all" value="" id="upper_fix" disabled> fix or add <br> <span id="upper_fix_lbl">{{$upper_product->vFixOrAddOp}}</span></label>
                                                                <input type="hidden" id="upper_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif



                                                        @if (!empty(array_filter($upper_vClasps)))
                                                        @php
                                                        $parts=array_filter($upper_vClasps);
                                                        $upper_vClasps = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lbl_btn_all" value="" id="upper_clasps" disabled>clasps<br> <span id="upper_clasps_lbl">{{$upper_vClasps}}</span></label>
                                                                <input type="hidden" id="upper_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($upper_vClaspsOp))
                                                        @if (!empty(array_filter($upper_vClaspsOp)))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lbl_btn_all" value="" id="upper_clasps" disabled>clasps<br> <span id="upper_clasps_lbl">{{$upper_product->vClaspsOp}}</span></label>
                                                                <input type="hidden" id="upper_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif

                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </td>
                                @endif
                                <!-- view upper teeth End -->


                                <!-- middle inforamtion section start -->
                                <td class="position-relative" style="vertical-align:top;vertical-align:top;font-weight:normal;">
                                    <div class="row c-row-text">
                                        <div class="col-lg-12">
                                            <h3 class="card-title mb-0">
                                                CASE DESIGN
                                            </h3>
                                        </div>
                                        <div class="col-lg-12">

                                            <table class="w-100 inner-table" style="table-layout: fixed;">
                                                <tbody>
                                                    @if(!empty($upper_product->vProductName) || !empty($lower_product->vProductName))
                                                    <tr>
                                                        <td colspan="2" style="text-align: right;">
                                                            @if(!empty($upper_product->vProductName))
                                                            <span>{{$upper_product->vProductName}}</span>
                                                            @endif
                                                        </td>
                                                        <td style="text-align:center;"><span style="font-weight: bold;">Product</span>
                                                        </td>
                                                        <td colspan="2">
                                                            @if(!empty($lower_product->vProductName))
                                                            <span> {{$lower_product->vProductName}}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @if(!empty($upper_product->vGradeName) || !empty($lower_product->vGradeName))
                                                    <tr>
                                                        <td colspan="2" style="text-align: right;">
                                                            @if(!empty($upper_product->vGradeName))
                                                            <span>{{$upper_product->vGradeName}}</span>
                                                            @endif
                                                        </td>
                                                        <td style="text-align:center;">
                                                            <span style="font-weight:bold;">Grade</span>
                                                        </td>
                                                        <td colspan="2">
                                                            @if(!empty($lower_product->vGradeName))
                                                            <span> {{$lower_product->vGradeName}}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @if(!empty($upper_product->vStageName) || !empty($lower_product->vStageName))
                                                    <tr>
                                                        <td colspan="2" style="text-align: right;">
                                                            @if(!empty($upper_product->vStageName))
                                                            <span>
                                                                {{$upper_product->vStageName}}
                                                            </span>
                                                            @endif
                                                        </td>
                                                        <td style="text-align:center;"><span style="font-weight: bold;">Stage</span>
                                                        </td>
                                                        <td colspan="2">
                                                            @if(!empty($lower_product->vStageName))
                                                            <span>{{$lower_product->vStageName}}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif


                                                    <!-- TEETH SHADE -->
                                                    @if(!empty($upper_product->vBrandName) || !empty($lower_product->vBrandName))
                                                    <tr>
                                                        <td colspan="2" style="text-align:right;">
                                                            @if(!empty($upper_product->vBrandName))
                                                            <span>{{$upper_product->vBrandName}} - {{$upper_product->vToothShade}}</span>
                                                            @endif
                                                        </td>


                                                        <td style="font-weight: bold;text-align: center;"><span>Teeth Shade</span></td>

                                                        <td colspan="2">

                                                            @if(!empty($lower_product->vBrandName))
                                                            <span>{{$lower_product->vBrandName}} - {{$lower_product->vToothShade}}</span>

                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif

                                                    <!--   GUM SHADE -->
                                                    @if(!empty($upper_product->vGumBrand) || !empty($lower_product->vGumBrand))
                                                    <tr>
                                                        <td colspan="2" style="text-align:right;">
                                                            @if(!empty($upper_product->vGumBrand))
                                                            <span>{{$upper_product->vGumBrand}} - {{$upper_product->vGumShade}}</span>
                                                            @endif
                                                        </td>

                                                        <td style="font-weight: bold;text-align:center;"><span>Gum Shade</span></td>

                                                        <td colspan="2">
                                                            @if(!empty($lower_product->vGumBrand))
                                                            <span>{{$lower_product->vGumBrand}} - {{$lower_product->vGumShade}}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    <!--   GUM SHADE -->


                                                     @if(isset($upper_impression) OR isset($upper_impression))

                                                        @if(!isset($upper_impression))
                                                            @php $upper_impression = array(); @endphp
                                                        @endif

                                                        @if(!isset($lower_impression))
                                                            @php $lower_impression = array(); @endphp
                                                        @endif

                                                    @endif

                                                    @if((!empty($upper_impression) && sizeof($upper_impression) > 0) OR (!empty($lower_impression) && sizeof($lower_impression) > 0))
                                                    <tr>
                                                        <td colspan="2" style="vertical-align: top;">
                                                            <table class="w-100">
                                                                @if(empty($upper_impression))
                                                                @php
                                                                $upper_impression = $upper_impressionOp
                                                                @endphp
                                                                @endif

                                                                @foreach ($upper_impression as $value_impression)
                                                                <tr style="vertical-align: top;">
                                                                    <td class="p-0" style=" text-align: right;">
                                                                        <span>{{$value_impression->vImpressionName}}</span>
                                                                        <span style="min-width: 30px; display: inline-block;">Qty {{$value_impression->iQuantity}}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>

                                                        <td style="vertical-align: top;font-weight: bold;text-align:center;">
                                                            <span>Impressions</span>
                                                        </td>

                                                        <td colspan="2" style="vertical-align: top;">
                                                            <table class="w-100">
                                                                @if(empty($lower_impression))
                                                                @php
                                                                $lower_impression = $lower_impressionOp
                                                                @endphp
                                                                @endif
                                                                @foreach ($lower_impression as $lower_impression)
                                                                <tr style="vertical-align: top;">
                                                                    <td class="p-0" style=" text-align: left;">
                                                                        <span style="min-width: 30px; display: inline-block;">Qty {{$lower_impression->iQuantity}}</span>
                                                                        <span>{{$lower_impression->vImpressionName}}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    @endif



                                                    @if(isset($upper_addons) OR isset($lower_addons))

                                                        @if(!isset($upper_addons))
                                                            @php $upper_addons = array(); @endphp
                                                        @endif

                                                        @if(!isset($lower_addons))
                                                            @php $lower_addons = array(); @endphp
                                                        @endif

                                                    @endif

                                                    @if((!empty($upper_addons) && sizeof($upper_addons) > 0) OR (!empty($lower_addons) && sizeof($lower_addons) > 0))
                                                    <tr>
                                                        <td colspan="2" style="vertical-align: top;">
                                                            <table class="w-100">
                                                                @foreach ($upper_addons as $value)
                                                                <tr style="vertical-align: top;">
                                                                    <td class="p-0" style=" text-align: right;">
                                                                        <span>{{$value->vAddonName}}</span>
                                                                        <span style="min-width: 40px; display: inline-block;">Qty {{$value->iQuantity}}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>

                                                        <td style="vertical-align: top;font-weight: bold;text-align:center;">
                                                            <span>Add Ons</span>
                                                        </td>

                                                        <td colspan="2" style="vertical-align: top;">
                                                            <table class="w-100">
                                                                @foreach ($lower_addons as $value)
                                                                <tr>
                                                                    <td class="p-0" style=" text-align: left;">
                                                                        <span style="min-width: 40px; display: inline-block;">Qty {{$value->iQuantity}}</span>
                                                                        <span>{{$value->vAddonName}}</span>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    @endif

                                                    @if(!empty($upper_product->eStatus) || !empty($lower_product->eStatus))
                                                    <tr>
                                                        @if(!empty($upper_product->eStatus))
                                                        <td colspan="2" style="text-align: right;"><span>{{$upper_product->eStatus}}</span></td>
                                                        @else
                                                        <td colspan="2"></td>
                                                        @endif
                                                        <td style="font-weight: bold;text-align: center;"><span>Stage Status</span></td>
                                                        @if(!empty($lower_product->eStatus))
                                                        <td colspan="2"><span>{{$lower_product->eStatus}}</span></td>
                                                        @else
                                                        <td colspan="2"></td>
                                                        @endif
                                                    </tr>
                                                    @endif
                                                   
                                                    
                                                    @if((isset($rush_product) && $rush_product =='yes') && (isset($is_single_product) && $is_single_product =='no') ||((isset($upper_product->eCustomDate) && $upper_product->eCustomDate =='Yes')|| (isset($lower_product->eCustomDate) && $lower_product->eCustomDate =='Yes')))
                                                
                                                    <tr> 
                                                        @if((isset($upper_rush_product) && $upper_rush_product =='yes') || (isset($upper_product->eCustomDate) && $upper_product->eCustomDate =='Yes'))
                                                            <td colspan="2" style="text-align: right;"><span>{{date('m/d/Y', strtotime($final_delivery_date))}} </span></td>
                                                            @elseif(isset($final_delivery_date_upper) && !empty($final_delivery_date_upper)
                                                            )
                                                             <td colspan="2" style="text-align: right;"><span>{{date('m/d/Y', strtotime($final_delivery_date_upper))}} </span></td>
                                                            @elseif(isset($upper_product->dDeliveryDate) && $upper_product->dDeliveryDate !='')
                                                            <td colspan="2" style="text-align: right;"><span>{{date('m/d/Y', strtotime($upper_product->dDeliveryDate))}} </span></td>
                                                            @else
                                                            <td colspan="2"><span></span></td>
                                                        @endif
                                                        
                                                        <td style="vertical-align: top;font-weight: bold;text-align: center;">
                                                            <span>Delivery Date</span>
                                                        </td>
                                                        
                                                        @if((isset($lower_rush_product) && $lower_rush_product =='yes') || (isset($lower_product->eCustomDate) && $lower_product->eCustomDate =='Yes') )
                                                            <td colspan="2" style=""><span>{{date('m/d/Y', strtotime($final_delivery_date))}} </span></td>
                                                            @elseif(isset($final_delivery_date_lower) && !empty($final_delivery_date_lower)
                                                            )
                                                             <td colspan="2" style=""><span>{{date('m/d/Y', strtotime($final_delivery_date_lower))}} </span></td>
                                                            @elseif(isset($lower_product->dDeliveryDate) && $lower_product->dDeliveryDate !='')
                                                            <td colspan="2" style=""><span>{{date('m/d/Y', strtotime($lower_product->dDeliveryDate))}} </span></td>
                                                            
                                                        @endif
                                                                
                                                    </tr>

                                                    <tr>
                                                        @if(isset($upper_product->dDeliveryDate) && $upper_product->dDeliveryDate !='')
                                                        <td colspan="2" style="text-align:right;"><span>{{date("h:i A",strtotime($final_delivery_time))}}</span></td>
                                                        @else
                                                        <td colspan="2"><span></span></td>
                                                        @endif
                                                        <td style="vertical-align: top;font-weight: bold;text-align: center;">
                                                            <span>Delivery Time</span>
                                                        </td>
                                                        @if(isset($lower_product->dDeliveryDate) && $lower_product->dDeliveryDate)
                                                        <td colspan="2"><span>{{date("h:i A",strtotime($final_delivery_time))}} </span></td>
                                                       
                                                        @endif
                                                    </tr>
                                                    @endif
                                                    <tr>

                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </td>

                                <!-- middle inforamtion section End -->



                                <!-- view Lower teeth start -->
                                <td style="width:290px;text-align:center;vertical-align:top;">
                                @if((isset($lower_product->eStatus) && $lower_product->eStatus == "Canceled") && $hide_product == 'no')
                                    <div class="cen-section lower">
                                        <p class="m-0">
                                        This stage was canceled
                                        </p>
                                    </div>
                                @elseif((isset($lower_product->eStatus) && $lower_product->eStatus == "On Hold") &&  $hide_product == 'no')
                                    <div class="cen-section lower">
                                        <p class="m-0">
                                        This stage is on hold
                                        </p>
                                    </div>
                                @endif
                                    @if((isset($lower_vTeethInMouth) AND isset($lower_vMissingTeeth) AND isset($lower_vWillExtractOnDelivery) AND isset($lower_vHasBeenExtracted) AND isset($lower_vFixOrAdd)) OR (isset($lower_vTeethInMouthOp) AND isset($lower_vMissingTeethOp) AND isset($lower_vWillExtractOnDeliveryOp) AND isset($lower_vHasBeenExtractedOp) AND isset($lower_vFixOrAddOp)))

                                    @if((isset($lower_vTeethInMouth) AND isset($lower_vMissingTeeth) AND isset($lower_vWillExtractOnDelivery) AND isset($lower_vHasBeenExtracted) AND isset($lower_vFixOrAdd)))
                                    @else
                                    @php
                                    $lower_vTeethInMouth = $lower_vTeethInMouthOp;
                                    $lower_vMissingTeeth = $lower_vMissingTeethOp;
                                    $lower_vWillExtractOnDelivery = $lower_vWillExtractOnDeliveryOp;
                                    $lower_vHasBeenExtracted = $lower_vHasBeenExtractedOp;
                                    $lower_vFixOrAdd = $lower_vFixOrAddOp;
                                    $lower_vClasps = $lower_vClaspsOp;

                                    @endphp
                                    @endif
                                    <div class="teeth-wrapper lower" id="lower_extraction">
                                        <h3 class="card-title mb-0">
                                            MANDIBULAR
                                        </h3>
                                        <svg width="100%" height="100%" viewBox="0 0 1500 790" class="teeth-svg">

                                            <g>
                                                <g></g>
                                                <!-- lower-teeth 1 -->
                                                <a href="javascript:;">
                                                    <image id="lower-17-teeth" data-id="teeth15" data-name="17" x="30" y="555" width="252" height="200" xlink:href="
                                                    
                                                    @if (in_array('17', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-17.png')}}
                                                    @endif
                                                    @if (in_array('17', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-17.png')}}
                                                    @endif
                                                    @if (in_array('17', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-17.png')}}
                                                    @endif
                                                    @if (in_array('17', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-17.png')}}
                                                    @endif
                                                    @if (in_array('17', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-17.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth  teeth_lower"></image>
                                                </a>

                                                <!--  image with cliap-1 -->
                                                <image id="lower-17-claps" data-name="17" x="15" y="540" width="80" height="190" xlink:href="{{asset('admin/assets/images/teeth/dc-17.png')}}" class=" clap-17 teeth_lower lower-claps" style="
                                        @if (in_array('17', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="lower-17-num" data-name="17" class="cls-1 teeth_lower" transform="translate(196.632 625.98) scale(1.199)">
                                                    <tspan x="-30">17</tspan>
                                                </text>


                                                <!-- lower-teeth 1 end-->

                                                <g></g>
                                                <!-- lower-teeth 2 -->
                                                <a href="javascript:;">
                                                    <image id="lower-18-teeth" data-id="teeth7" data-name="18" x="95" y="400" width="232" height="194" xlink:href="

                                                    @if (in_array('18', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-18.png')}}
                                                    @endif
                                                    @if (in_array('18', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-18.png')}}
                                                    @endif
                                                    @if (in_array('18', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-18.png')}}
                                                    @endif
                                                    @if (in_array('18', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-18.png')}}
                                                    @endif
                                                    @if (in_array('18', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-18.png')}}
                                                    @endif

                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-2 -->
                                                <image id="lower-18-claps" data-name="18" x="80" y="382" width="85" height="194" xlink:href="{{asset('admin/assets/images/teeth/dc-18.png')}}" class=" clap-18 teeth_lower lower-claps" style="
                                                
                                            @if (in_array('18', $lower_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif
                                        
                                        
                                        "></image>

                                                <text id="lower-18-num" data-name="18" class="cls-1 teeth_lower" transform="translate(263.156 536.61) scale(1.199)">
                                                    <tspan x="-35" y="-30">18</tspan>
                                                </text>

                                                <!-- lower-teeth 2 end-->

                                                <g></g>
                                                <!-- lower-teeth 3 -->
                                                <a href="javascript:;">
                                                    <image id="lower-19-teeth" data-id="teeth6" data-name="19" x="165" y="290" width="235" height="191" xlink:href="
                                                    
                                                    @if (in_array('19', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-19.png')}}
                                                    @endif
                                                    @if (in_array('19', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-19.png')}}
                                                    @endif
                                                    @if (in_array('19', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-19.png')}}
                                                    @endif
                                                    @if (in_array('19', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-19.png')}}
                                                    @endif
                                                    @if (in_array('19', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-19.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>

                                                <text id="lower-19-num" data-name="19" class="cls-1 teeth_lower" transform="translate(329.927 426.297) scale(1.199)">
                                                    <tspan x="-35" y="-35">19</tspan>
                                                </text>

                                                <!--  image with cliap-3 -->
                                                <image id="lower-19-claps" data-name="19" x="155" y="255" width="100" height="191" xlink:href="{{asset('admin/assets/images/teeth/dc-19.png')}}" class=" clap-19 teeth_lower lower-claps" style="
                                            @if (in_array('19', $lower_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif
                                            
                                        "></image>
                                                <!-- lower-teeth 3 end-->


                                                <g></g>
                                                <!-- lower-teeth 4 start-->
                                                <a href="javascript:;">
                                                    <image id="lower-20-teeth" data-id="teeth5" data-name="20" x="265" y="225" width="167" height="159" xlink:href="
                                                    
                                                    @if (in_array('20', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-20.png')}}
                                                    @endif
                                                    @if (in_array('20', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-20.png')}}
                                                    @endif
                                                    @if (in_array('20', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-20.png')}}
                                                    @endif
                                                    @if (in_array('20', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-20.png')}}
                                                    @endif
                                                    @if (in_array('20', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-20.png')}}
                                                    @endif

                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>

                                                <text id="lower-20-num" data-name="20" class="cls-1 teeth_lower" transform="translate(401.061 332.076) scale(1.199)">
                                                    <tspan x="-30" y="-35">20</tspan>
                                                </text>

                                                <!--  image with cliap-4 -->
                                                <image id="lower-20-claps" data-name="20" x="240" y="175" width="100" height="159" xlink:href="{{asset('admin/assets/images/teeth/dc-20.png')}}" class=" clap-20 teeth_lower lower-claps" style="
                                            @if (in_array('20', $lower_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif

                                            
                                        "></image>
                                                <!-- lower-teeth 4 end-->

                                                <g></g>
                                                <!-- lower-teeth 5 -->
                                                <a href="javascript:;">
                                                    <image id="lower-21-teeth" data-id="teeth3" data-name="21" x="342" y="153" width="150" height="177" xlink:href="
                                                    
                                                    @if (in_array('21', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-21.png')}}
                                                    @endif
                                                    @if (in_array('21', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-21.png')}}
                                                    @endif
                                                    @if (in_array('21', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-21.png')}}
                                                    @endif
                                                    @if (in_array('21', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-21.png')}}
                                                    @endif
                                                    @if (in_array('21', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-21.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-5 -->
                                                <image id="lower-21-claps" data-name="21" x="300" y="135" width="140" height="90" xlink:href="{{asset('admin/assets/images/teeth/dc-21.png')}}" class=" clap-21 teeth_lower lower-claps" style="

                                                @if (in_array('21', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-21-num" data-name="21" class="cls-1 teeth_lower" transform="translate(455.319 283.973) scale(1.199)">
                                                    <tspan x="-30" y="-50">21</tspan>
                                                </text>

                                                <g></g>
                                                <!-- lower-teeth 6 -->
                                                <a href="javascript:;">
                                                    <image id="lower-22-teeth" data-id="teeth4" data-name="22" x="435" y="93" width="135" height="200" xlink:href="

                                                    @if (in_array('22', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-22.png')}}
                                                    @endif
                                                    @if (in_array('22', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-22.png')}}
                                                    @endif
                                                    @if (in_array('22', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-22.png')}}
                                                    @endif
                                                    @if (in_array('22', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-22.png')}}
                                                    @endif
                                                    @if (in_array('22', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-22.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>

                                                <!--  image with cliap-22 -->
                                                <image id="lower-22-claps" data-name="22" x="430" y="15" width="100" height="200" xlink:href="{{asset('admin/assets/images/teeth/dc-22.png')}}" class=" clap-22 teeth_lower lower-claps" style="
                                                
                                        @if (in_array('22', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="lower-22-num" data-name="22" class="cls-1 teeth_lower" transform="translate(517.12 257.438) scale(1.199)">
                                                    <tspan x="0" y="-50">22</tspan>
                                                </text>

                                                <g></g>
                                                <!-- lower-teeth 7 -->
                                                <a href="javascript:;">
                                                    <image id="lower-23-teeth" data-id="teeth2" data-name="23" x="540" y="60" width="115" height="186" xlink:href="

                                                    @if (in_array('23', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-23.png')}}
                                                    @endif
                                                    @if (in_array('23', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-23.png')}}
                                                    @endif
                                                    @if (in_array('23', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-23.png')}}
                                                    @endif
                                                    @if (in_array('23', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-23.png')}}
                                                    @endif
                                                    @if (in_array('23', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-23.png')}}
                                                    @endif

                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!-- image with cliap-23 -->
                                                <image id="lower-23-claps" data-name="23" x="525" y="-33" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/dc-23.png')}}" class=" clap-23 teeth_lower lower-claps" style="
                                            @if (in_array('23', $lower_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif
                                            
                                            "></image>
                                                <text id="lower-23-num" data-name="23" class="cls-1 teeth_lower" transform="translate(602.467 207.332) scale(1.199)">
                                                    <tspan x="0" y="-50">23</tspan>
                                                </text>


                                                <g></g>

                                                <!-- lower-teeth 8 -->
                                                <a href="javascript:;">
                                                    <image id="lower-24-teeth" data-id="teeth1" data-name="24" x="647" y="40" width="111" height="182" xlink:href="
                                                    

                                                    @if (in_array('24', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-24.png')}}
                                                    @endif
                                                    @if (in_array('24', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-24.png')}}
                                                    @endif
                                                    @if (in_array('24', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-24.png')}}
                                                    @endif
                                                    @if (in_array('24', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-24.png')}}
                                                    @endif
                                                    @if (in_array('24', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-24.png')}}
                                                    @endif

                                                    " class="img-for-teeth teeth_lower "></image>
                                                </a>
                                                <!-- image with cliap-8 -->
                                                <image id="lower-24-claps" data-name="24" x="647" y="-60" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/dc-24.png')}}" class=" clap-24 teeth_lower lower-claps" style="
                                                
                                        @if (in_array('24', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="lower-24-num" data-name="24" class="cls-1 teeth_lower" transform="translate(703.596 190.046) scale(1.199)">
                                                    <tspan x="0" y="-40">24</tspan>
                                                </text>

                                                <g></g>
                                                <!-- lower-teeth 9 -->
                                                <a href="javascript:;">
                                                    <image id="lower-25-teeth" data-id="teeth8" data-name="25" x="760" y="42" width="111" height="182" xlink:href="
                                                    
                                                    @if (in_array('25', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-25.png')}}
                                                    @endif
                                                    @if (in_array('25', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-25.png')}}
                                                    @endif
                                                    @if (in_array('25', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-25.png')}}
                                                    @endif
                                                    @if (in_array('25', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-25.png')}}
                                                    @endif
                                                    @if (in_array('25', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-25.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-9 -->
                                                <image id="lower-25-claps" data-name="25" x="762" y="-58" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/dc-25.png')}}" class=" clap-25 teeth_lower lower-claps" style="
                                                
                                            @if (in_array('25', $lower_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif
                                            
                                            "></image>
                                                <text id="lower-25-num" data-name="25" class="cls-1 teeth_lower" transform="translate(789.005 190.046) scale(1.199)">
                                                    <tspan x="10" y="-50">25</tspan>
                                                </text>
                                                <g></g>
                                                <!-- lower-teeth 10 -->
                                                <a href="javascript:;">
                                                    <image id="lower-26-teeth" data-id="teeth9" data-name="26" x="865" y="54" width="115" height="186" xlink:href="
                                                    
                                                    @if (in_array('26', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-26.png')}}
                                                    @endif
                                                    @if (in_array('26', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-26.png')}}
                                                    @endif
                                                    @if (in_array('26', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-26.png')}}
                                                    @endif
                                                    @if (in_array('26', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-26.png')}}
                                                    @endif
                                                    @if (in_array('26', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-26.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-11 -->
                                                <image id="lower-26-claps" data-name="26" x="880" y="-35" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/dc-26.png')}}" class=" clap-26 teeth_lower lower-claps" style="

                                                @if (in_array('26', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-26-num" data-name="26" class="cls-1 teeth_lower" transform="translate(881.008 207.152) scale(1.199)">
                                                    <tspan x="25" y="-50">26</tspan>
                                                </text>
                                                <!-- lower-teeth 11-->
                                                <a href="javascript:;">
                                                    <image id="lower-27-teeth" data-id="teeth11" data-name="27" x="955" y="80" width="135" height="200" xlink:href="
                                                    
                                                    @if (in_array('27', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-27.png')}}
                                                    @endif
                                                    @if (in_array('27', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-27.png')}}
                                                    @endif
                                                    @if (in_array('27', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-27.png')}}
                                                    @endif
                                                    @if (in_array('27', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-27.png')}}
                                                    @endif
                                                    @if (in_array('27', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-27.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-11 -->
                                                <image id="lower-27-claps" data-name="27" x="990" y="-5" width="110" height="200" xlink:href="{{asset('admin/assets/images/teeth/dc-27.png')}}" class=" clap-27 teeth_lower lower-claps" style="

                                        @if (in_array('27', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="lower-27-num" data-name="27" class="cls-1 teeth_lower" transform="translate(982.792 240.944) scale(1.199)">
                                                    <tspan x="25" y="-50">27</tspan>
                                                </text>
                                                <!-- lower-teeth 12 -->
                                                <a href="javascript:;">
                                                    <image id="lower-28-teeth" data-id="teeth10" data-name="28" x="1035" y="134" width="150" height="177" xlink:href="
                                                    

                                                    @if (in_array('28', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-28.png')}}
                                                    @endif
                                                    @if (in_array('28', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-28.png')}}
                                                    @endif
                                                    @if (in_array('28', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-28.png')}}
                                                    @endif
                                                    @if (in_array('28', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-28.png')}}
                                                    @endif
                                                    @if (in_array('28', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-28.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-12 -->
                                                <image id="lower-28-claps" data-name="28" x="1100" y="60" width="100" height="177" xlink:href="{{asset('admin/assets/images/teeth/dc-28.png')}}" class=" clap-28 teeth_lower lower-claps" style="
                                                @if (in_array('28', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-28-num" data-name="28" class="cls-1 teeth_lower" transform="translate(1037.236 283.518) scale(1.199)">
                                                    <tspan x="40" y="-50">28</tspan>
                                                </text>
                                                <!-- lower-teeth 13 -->
                                                <a href="javascript:;">
                                                    <image id="lower-29-teeth" data-id="teeth12" data-name="29" x="1098" y="205" width="167" height="159" xlink:href="

                                                    @if (in_array('29', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-29.png')}}
                                                    @endif
                                                    @if (in_array('29', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-29.png')}}
                                                    @endif
                                                    @if (in_array('29', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-29.png')}}
                                                    @endif
                                                    @if (in_array('29', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-29.png')}}
                                                    @endif
                                                    @if (in_array('29', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-29.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-13 -->
                                                <image id="lower-29-claps" data-name="29" x="1190" y="155" width="100" height="159" xlink:href="{{asset('admin/assets/images/teeth/dc-29.png')}}" class=" clap-29 teeth_lower lower-claps" style="
                                        @if (in_array('29', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        "></image>
                                                <text id="lower-29-num" data-name="29" class="cls-1 teeth_lower" transform="translate(1097.072 329.95) scale(1.199)">
                                                    <tspan x="45" y="-50">29</tspan>
                                                </text>
                                                <!-- lower-teeth 14 -->
                                                <a href="javascript:;">
                                                    <image id="lower-30-teeth" data-id="teeth13" data-name="30" x="1130" y="278" width="234" height="191" xlink:href="
                                                    
                                                    @if (in_array('30', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-30.png')}}
                                                    @endif
                                                    @if (in_array('30', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-30.png')}}
                                                    @endif
                                                    @if (in_array('30', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-30.png')}}
                                                    @endif
                                                    @if (in_array('30', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-30.png')}}
                                                    @endif
                                                    @if (in_array('30', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-30.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-14 -->
                                                <image id="lower-30-claps" data-name="30" x="1280" y="258" width="100" height="191" xlink:href="{{asset('admin/assets/images/teeth/dc-30.png')}}" class=" clap-30 teeth_lower lower-claps" style="
                                        @if (in_array('30', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        
                                        "></image>
                                                <text id="lower-30-num" data-name="30" class="cls-1 teeth_lower" transform="translate(1164.707 409.172) scale(1.199)">
                                                    <tspan x="45" y="-45">30</tspan>
                                                </text>
                                                <!-- lower-teeth 15 -->

                                                <a href="javascript:;">
                                                    <image id="lower-31-teeth" data-id="teeth14" data-name="31" x="1210" y="395" width="232" height="194" xlink:href="
                                                    
                                                    @if (in_array('31', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-31.png')}}
                                                    @endif
                                                    @if (in_array('31', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-31.png')}}
                                                    @endif
                                                    @if (in_array('31', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-31.png')}}
                                                    @endif
                                                    @if (in_array('31', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-31.png')}}
                                                    @endif
                                                    @if (in_array('31', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-31.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-15 -->
                                                <image id="lower-31-claps" data-name="31" x="1390" y="420" width="80" height="130" xlink:href="{{asset('admin/assets/images/teeth/dc-30.png')}}" class=" clap-31 teeth_lower lower-claps" style="
                                        @if (in_array('31', $lower_vClasps))
                                            {{' '}}
                                        @else
                                            {{'display: none; '}}
                                            
                                        @endif
                                        "></image>
                                                <text id="lower-31-num" data-name="31" class="cls-1 teeth_lower" transform="translate(1238.134 502.8) scale(1.199)">
                                                    <tspan x="50">31</tspan>
                                                </text>
                                                <!-- lower-teeth 16 -->

                                                <a href="javascript:;">
                                                    <image id="lower-32-teeth" data-id="teeth16" data-name="32" x="1273" y="560" width="232" height="193" xlink:href="

                                                    @if (in_array('32', $lower_vTeethInMouth))
                                                    {{asset('admin/assets/images/yellow-teeth/d-32.png')}}
                                                    @endif
                                                    @if (in_array('32', $lower_vMissingTeeth))
                                                    {{asset('admin/assets/images/teeth/d-32.png')}}
                                                    @endif
                                                    @if (in_array('32', $lower_vWillExtractOnDelivery))
                                                    {{asset('admin/assets/images/red-teeth/d-32.png')}}
                                                    @endif
                                                    @if (in_array('32', $lower_vHasBeenExtracted))
                                                    {{asset('admin/assets/images/grey-teeth/d-32.png')}}
                                                    @endif
                                                    @if (in_array('32', $lower_vFixOrAdd))
                                                    {{asset('admin/assets/images/green-teeth/d-32.png')}}
                                                    @endif
                                                    
                                                    " class="img-for-teeth teeth_lower"></image>
                                                </a>
                                                <!--  image with cliap-32 -->
                                                <image id="lower-32-claps" data-name="32" x="1440" y="580" width="114" height="92" xlink:href="{{asset('admin/assets/images/teeth/dc-32.png')}}" class=" clap-32 teeth_lower lower-claps" style="
                                                
                                            @if (in_array('32', $lower_vClasps))
                                                {{' '}}
                                            @else
                                                {{'display: none; '}}
                                                
                                            @endif
                                            "></image>
                                                <text id="lower-32-num" data-name="32" class="cls-1 teeth_lower" transform="translate(1268.177 618.27) scale(1.199)">
                                                    <tspan x="80" y="20">32</tspan>
                                                </text>

                                                <!--lower teeth numbers ------------------------------ -->

                                            </g>

                                        </svg>

                                        <div class="d-block text-center teeth-text">
                                            <a href="javascript:;" class="add-btn p-2 mx-auto lower_mising_all d-none">
                                                Missing all teeth
                                            </a>
                                        </div>
                                        <div class="teeths-wrapper second-row p-0">
                                            <div class="row">
                                                <div class="col-md-12 col-xl-12 col-lg-12">
                                                    <div class="row gy-2">

                                                        @if(!empty($lower_vTeethInMouth))
                                                        @php
                                                        $parts=array_filter($lower_vTeethInMouth);
                                                        $lower_vTeethInMouth = (implode(",",$parts));
                                                        @endphp
                                                        @endif

                                                        @if(!empty($lower_vTeethInMouth))
                                                        <a href="javascript:;">
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <label class="form-control inner-label te-in-month lower_lbl_btn_all" value="" id="lower_in_mouth" disabled> Teeth in mouth <br> <span id="lower_in_mouth_lbl">{{$lower_vTeethInMouth}}</span></label>
                                                                    <input type="hidden" id="lower_in_mouth_value" value="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                        @endif


                                                        @if (!empty(array_filter($lower_vMissingTeeth)))
                                                        @php
                                                        $parts=array_filter($lower_vMissingTeeth);
                                                        $lower_vMissingTeeth = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lower_lbl_btn_all" value="" id="lower_missing_teeth" disabled>missing teeth <br> <span id="lower_missing_teeth_lbl">{{$lower_vMissingTeeth}}</span></label>
                                                                <input type="hidden" id="lower_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($lower_vMissingTeethOp))
                                                        @if (!empty(array_filter($lower_vMissingTeethOp)))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lower_lbl_btn_all" value="" id="lower_missing_teeth" disabled>missing teeth <br> <span id="lower_missing_teeth_lbl">{{$lower_vMissingTeeth}}</span></label>
                                                                <input type="hidden" id="lower_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif


                                                        @if (!empty(array_filter($lower_vWillExtractOnDelivery)))
                                                        @php
                                                        $parts=array_filter($lower_vWillExtractOnDelivery);
                                                        $lower_vWillExtractOnDelivery = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lower_lbl_btn_all" value="" id="lower_ectract_delivery" disabled>will extract on delivery <br><span id="lower_ectract_delivery_lbl">{{$lower_vWillExtractOnDelivery}}</span></label>
                                                                <input type="hidden" id="lower_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($lower_vWillExtractOnDeliveryOp))
                                                        @if (!empty(array_filter($lower_vWillExtractOnDeliveryOp)))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lower_lbl_btn_all" value="" id="lower_ectract_delivery" disabled>will extract on delivery <br><span id="lower_ectract_delivery_lbl">{{$lower_product->vWillExtractOnDeliveryOp}}</span></label>
                                                                <input type="hidden" id="lower_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif



                                                        @if (!empty(array_filter($lower_vHasBeenExtracted)))
                                                        @php
                                                        $parts=array_filter($lower_vHasBeenExtracted);
                                                        $lower_vHasBeenExtracted = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lower_lbl_btn_all" value="" id="lower_been_extracted" disabled> has been extracted <br><span id="lower_been_extracted_lbl">{{$lower_vHasBeenExtracted}}</span></label>
                                                                <input type="hidden" id="lower_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($lower_vHasBeenExtractedOp))
                                                        @if (!empty(array_filter($lower_vHasBeenExtractedOp)))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lower_lbl_btn_all" value="" id="lower_been_extracted" disabled> has been extracted <br><span id="lower_been_extracted_lbl">{{$lower_product->vHasBeenExtractedOp}}</span></label>
                                                                <input type="hidden" id="lower_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif



                                                        @if (!empty(array_filter($lower_vFixOrAdd)))
                                                        @php
                                                        $parts=array_filter($lower_vFixOrAdd);
                                                        $lower_vFixOrAdd = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lower_lbl_btn_all" value="" id="lower_fix" disabled> fix or add <br> <span id="lower_fix_lbl">{{$lower_vFixOrAdd}}</span></label>
                                                                <input type="hidden" id="lower_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($lower_vFixOrAddOp))
                                                        @if (!empty(array_filter($lower_vFixOrAddOp)))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lower_lbl_btn_all" value="" id="lower_fix" disabled> fix or add <br> <span id="lower_fix_lbl">{{$lower_product->vFixOrAddOp}}</span></label>
                                                                <input type="hidden" id="lower_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif


                                                        @if (!empty(array_filter($lower_vClasps)))
                                                        @php
                                                        $parts=array_filter($lower_vClasps);
                                                        $lower_vClasps = (implode(",",$parts));
                                                        @endphp
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lower_lbl_btn_all" value="" id="lower_clasps" disabled>clasps<br> <span id="lower_clasps_lbl">{{$lower_vClasps}}</span></label>
                                                                <input type="hidden" id="lower_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif(isset($lower_vFixOrAddOp))
                                                        @if (!empty(array_filter($lower_vClasps)))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lower_lbl_btn_all" value="" id="lower_clasps" disabled>clasps<br> <span id="lower_clasps_lbl">{{$lower_product->vClaspsOp}}</span></label>
                                                                <input type="hidden" id="lower_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @endif

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </td>

                            </tr>
                        </tbody>
                    </table>



                    <!-- view Lower teeth end -->









                    @if(empty($Slip->tStagesNotes))
                    @php
                    $tStagesNotes = "";
                    @endphp
                    @else
                    @php
                    $tStagesNotes = $Slip->tStagesNotes;
                    @endphp
                    @endif



                    <div class="col-12 text-center mb-1">
                       
                        <div id="nextStage"></div>
                    </div>

                    <!-- <div class="col-lg-12 stagenote-text-area " id="notes_div">
                        <div class="stage-title-bold" style="background-color:#eff2f5 ;">

                            <div class="d-flex justify-content-center align-items-center">
                                <a href="javascript:;" id="edit_stage" class="btn add-btn p-0 w-auto me-2" title="Add Notes" style="padding: 0px!important;                            
                                display: flex;
                                justify-content: center;
                                align-items: center;
                                height: 30px;
                                min-width: 30px!important;
                                border-radius: 5px;
                                margin-right: 5px;
                                -webkit-border-radius: 5px;
                                border: 1px solid #a1a5b754!important;font-size: 14px;
                                color: #a1a5b7;background: transparent;"> 

                                <i class="fad fa-pencil" style="color: unset;"></i> </a>
                                <h5 class="card-title text-start">
                                    Stage Notes
                                    </h3>
                                    <a href="javascript:;" title="Attachments" id="image_popup" class="btn add-btn p-0 ms-2" style="padding: 0px!important;                            
                                    display: flex;
                                    justify-content: center;
                                    align-items: center;
                                    height: 30px;
                                    min-width: 30px!important;
                                    border-radius: 5px;
                                    margin-right: 5px;
                                    -webkit-border-radius: 5px;
                                    border: 1px solid #a1a5b754!important;font-size: 14px;
                                    color: #a1a5b7;background: transparent;"> <i class="fad fa-link" style="color: unset;"></i> </a>
                                    {{-- <ul class="note-btn-group ms-5 ps-5">
                <li>
                    <a href="javascript:;" id="free_drawing_open">
                        <i class="fad fa-pencil"></i>
                    </a>
                </li>
            </ul> --}}

                            </div>
                        </div>
                        <div id="toolbar-container"></div>
                        <div id="tDescription_Reff">
                            <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Stage notes" disabled=""></textarea>
                        </div>
                    </div> -->



           <div class="col-lg-12 stagenote-text-area " id="notes_div">
                        <div class="stage-title-bold" style="background-color:#fff ;">

                            <div class="d-flex justify-content-center align-items-center">
                               
                                <a href="javascript:;" id="edit_stage" class="btn add-btn p-0 w-auto me-2" title="Add Notes" style="padding: 0px!important;                            
                                display: flex;
                                justify-content: center;
                                align-items: center;
                                height: 30px;
                                min-width: 30px!important;
                                border-radius: 5px;
                                margin-right: 5px;
                                -webkit-border-radius: 5px;
                                border: 1px solid #a1a5b754!important;font-size: 14px;
                                color: #a1a5b7;background: transparent;"> 
                                <i class="fad fa-pencil" style="color: unset;"></i> </a>
                              
                                <h5 class="card-title text-start">
                                    Stage Notes
                                </h5>
                               
                                    <a href="javascript:;" title="Attachments" id="image_popup" class="btn add-btn p-0 ms-2" style="padding: 0px!important;                            
                                    display: flex;
                                    justify-content: center;
                                    align-items: center;
                                    height: 30px;
                                    min-width: 30px!important;
                                    border-radius: 5px;
                                    margin-right: 5px;
                                    -webkit-border-radius: 5px;
                                    border: 1px solid #a1a5b754!important;font-size: 14px;
                                    color: #a1a5b7;background: transparent;"> <i class="fad fa-link" style="color: unset;"></i> </a>
                               
                                    {{-- <ul class="note-btn-group ms-5 ps-5">
                <li>
                    <a href="javascript:;" id="free_drawing_open">
                        <i class="fad fa-pencil"></i>
                    </a>
                </li>
            </ul> --}}

                            </div>
                        </div>
                        <div id="toolbar-container"></div>
                        <div id="tDescription_Reff">
                            <div class="form-control" style="height: auto;    max-height: 150px; background-color: white;    overflow-y: auto;" id="tDescription" name="tDescription" placeholder="Stage notes"></div>
                        </div>
                    </div>

                    <div style="width:100%;padding:0;">
                        <table class="tslip bg-g-blue">
                            <tbody>
                                <tr>
                                    <td> </td>
                                    @if(isset($eNewStageAccess) && $eNewStageAccess =='Yes' && $Slip->vLocation =='In office')
                                    <td class="text-center">
                                        @if((isset($Case->eStatus) && $Case->eStatus == 'On Process') && ((isset($lower_product->eStatus) && $lower_product->eStatus =='Finished') || (isset($upper_product->eStatus) && $upper_product->eStatus =='Finished')) && (isset($Slip->vLocation) && $Slip->vLocation =='In office'))
                                        @if(\App\Libraries\General::admin_info()['customerType'] == 'Lab Admin')
                                        <a href="{{route('admin.labcase.new_stage_lab',[$Case->iCaseId,$Slip->iSlipId])}}" 
                                            ><i style="font-size:20px;color:#fff" class="fal fa-repeat"></i></a>
                                        @else
                                        <a href="{{route('admin.labcase.new_stage',[$Case->iCaseId,$Slip->iSlipId])}}" 
                                        ><i style="font-size:20px;color:#fff" class="fal fa-repeat"></i></a>
                                        @endif
                                        @else
                                        <a href="#" class=" invisible"><i style="font-size:20px;color:#fff" class="fal fa-repeat"></i></a>
                                        @endif
                                    </td>
                                    @else
                                    <td class="text-center"><a href="#" class="invisible" ><i style="font-size:20px;color:#fff" class="fal fa-repeat"></i></a></td>
                                    @endif
                                    <td></td>
                                    @if(isset($eHistoryCallAccess) && $eHistoryCallAccess =='Yes')
                                    <td><a href="#" onclick="Call_log_model()" class="btn back-btn gray-btn mb-1">History call</a> </td>
                                    @else
                                    <td><a href="#" class="invisible btn back-btn gray-btn mb-1">History call</a> </td>
                                    @endif
                                    <td></td>
                                    <td></td>
                                    @if(isset($eViewDriverHistoryAccess) && $eViewDriverHistoryAccess =='Yes')
                                    <td rowspan="2">
                                        <div class="bottom-btn-group text-center my-0 driverModel" data-hname="DriverHistory"><img src="{{asset('admin/assets/images/Asset-t.png')}}" alt=""></div>
                                        <i class="fa-light fa-truck-fast"></i>
                                    </td>
                                    @else
                                    <td rowspan="2">
                                        <div class="bottom-btn-group text-center my-0 invisible"><img src="{{asset('admin/assets/images/Asset-t.png')}}" alt=""></div>
                                        
                                    </td>
                                    @endif
                                    @if(isset($ePickUpAccess) && $ePickUpAccess =='Yes' && ($Slip->vLocation =='In office ready to pickup' || $Slip->vLocation =='In lab ready to pickup'))
                                    <td><a class="@if(isset($Slip->vLocation) && $Slip->vLocation =='In office ready to pickup' || $Slip->vLocation =='In lab ready to pickup') driverModel @endif btn back-btn add-btn mb-1"  @if(isset($Slip->vLocation) && $Slip->vLocation =='In lab ready to pickup') data-name="LabPickUp" @endif>Pick up</a></td>
                                    {{-- <img src="{{asset('admin/assets/images/new_icons/Pick-up.svg')}}" alt="Pick up"> --}}
                                    @else
                                    <td><a class="btn back-btn add-btn mb-1 invisible" >Pick up</a></td>
                                    @endif

                                    <td></td>
                                    <td></td>
                                    {{-- <td><a href="{{route('admin.labcase')}}" class="btn back-btn add-btn mb-1">Submit slip</a> </td> --}}
                                </tr>

                                <tr>
                                    @if(\App\Libraries\General::admin_info()['customerType'] == 'Office Admin')
                                    <td><a href="{{route('admin.labcase.office_admin_listing')}}" class="btn back-btn red-btn">Close slip</a></td>
                                    @else
                                    <td><a href="{{route('admin.labcase')}}" class="btn back-btn red-btn">Close slip</a></td>
                                    @endif
                                    @if(isset($eEditStageAccess) && $eEditStageAccess =='Yes' && ($Slip->vLocation =='On route to the lab' || $Slip->vLocation =='In lab'))
                                        @if(\App\Libraries\General::admin_info()['customerType'] == 'Lab Admin')
                                        <td><a href="{{route('admin.labcase.edit_stage_lab',[$Case->iCaseId,$Slip->iSlipId])}}" class="btn back-btn gray-btn">Edit stage</a></td>
                                        @else
                                        <td><a href="{{route('admin.labcase.edit_stage',[$Case->iCaseId,$Slip->iSlipId])}}" class="btn back-btn gray-btn">Edit stage</a></td>
                                        @endif
                                    @else
                                    <td><a href="#" class="invisible btn back-btn gray-btn">Edit stage</a></td>
                                    @endif
                                    @if(isset($eLastModifiedAccess) && $eLastModifiedAccess =='Yes')
                                    <td class="text-center"> <a href="{{route('admin.labcase')}}" ><i style="color: #fff;
                                        font-size: 18px;" class="far fa-undo"></i></a> </td>
                                    {{-- class="btn back-btn gray-btn" --}}
                                    @else
                                    <td class="text-center"> <a href="#" class="invisible btn back-btn gray-btn">Last modified</a> </td>
                                    @endif
                                    @if(isset($eAddCallLogAccess) && $eAddCallLogAccess =='Yes')
                                    <td class="text-center"><a href="#" onclick="Call_log_store_model()"><i style="font-size:22px; color:#fff" class="far fa-phone-square"></i></a></td>
                                    @else
                                    <td class="text-center"><a href="#" class="invisible"><i style="font-size:18px; color:#fff" class="far fa-phone-square"></i></a></td>
                                    @endif
                                    @if(isset($eReadyToSendAccess) && $eReadyToSendAccess =='Yes' && isset($Slip->vLocation) && $Slip->vLocation =='In lab')
                                    <td><a href="#" @if(isset($Slip->vLocation) && $Slip->vLocation =='In lab') id="ReadyToSend" @endif  ><i style="font-size: 18px;
                                        color: #fff;" class="fal fa-paper-plane"></i></a></td>
                                    {{-- class="btn back-btn add-btn" --}}
                                    @else
                                    <td><a href="#" class="invisible "><i style="font-size: 18px;
                                        color: #fff;" class="fal fa-paper-plane"></i></a></td>
                                    @endif
                                    <td></td>
                                    @if(isset($eDropOffAccess) && $eDropOffAccess =='Yes' && ($Slip->vLocation =='On route to the lab' || $Slip->vLocation == 'On route to the office'))
                                    <td><a @if(isset($Slip->vLocation) && $Slip->vLocation =='On route to the lab') data-name="DropOff" @endif class="driverModel btn back-btn red-btn" @if(isset($Slip->vLocation) && $Slip->vLocation =='On route to the office') data-name="LabDropUp" @endif>Drop off</a></td>
                                    @else
                                    <td><a class="invisible btn back-btn red-btn">Drop off</a></td>
                                    @endif
                                    @if(isset($eDirectionAccess) && $eDirectionAccess =='Yes' && $Slip->vLocation =='On route to the office'))
                                    <td><a href="{{route('admin.labcase')}}" class="btn back-btn add-btn">Directions</a></td>
                                    @else
                                    <td><a href="#" class="invisible btn back-btn add-btn">Directions</a></td>
                                    @endif
                                    <td></td>
                                    <td><a href="{{route('admin.labcase')}}" class="btn invisible back-btn add-btn">Update slip</a></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal fade" id="add_attachments_model">
                <div class="modal-dialog modal-lg modal-dialog-centered" style="">
                    <div class="modal-content" style="background: #f1f1f1;">
                        <div class="link-model-wrapper all-icon-model" style="max-height: 560px;overflow-y: auto;height: auto;">
                            <div class="card-content collapse show">
                                <div class="c-model-header">
                                    <h5> Attachments</h5>
                                    <a href="javascript:;" id="add_attachments_model_close" class="close-model">
                                        <i class="fal fa-times"></i>
                                    </a>
                                </div>
                                <div class="card-body" style="max-height: 400px;overflow-y: auto;">
                                    @if(isset($Slip->vLocation) && ($Slip->vLocation =='In office ready to pickup' || $Slip->vLocation == 'On route to the lab' | $Slip->vLocation == 'In lab'))
                                    <form action="{{route('admin.labcase.dropzoneStore')}}" class="dropzone" id="dropzonewidget" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="iTampId" id="iTampId" value="{{$tampid}}">
                                        <input type="hidden" name="iCaseId" id="iCaseId" value="{{$Case->iCaseId}}">
                                        <input type="hidden" name="iSlipId" id="iSlipId" value="{{$Slip->iSlipId}}">
                                    </form>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="attachment_list" class="mt-3">
                                            </div>
                                        </div>
                                    </div>
                                 
                                </div>
                                <div class="c-model-footer py-3">
                                 
                                    @if(isset($Slip->vLocation) && ($Slip->vLocation =='In office ready to pickup' || $Slip->vLocation == 'On route to the lab' | $Slip->vLocation == 'In lab'))
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-11 mx-auto p-0">
                                            <div class="text-end">
                                                <a href="javascript:;" id="submit_attacement" class="model-btn">
                                                    Submit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

             {{-- For Open Driver Model history Start Jun 1 2022 --}}
             <div class="modal fade mt-10" id="DriverModel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="DriverModelLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl" style="max-width: 1000px;width: 1000px;">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="DriverModelLabel">Driver History</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="{{route('admin.labcase.store_driverHistory')}}" method="post" >
                        @csrf
                        <div class="modal-body" id="DriverRespose" style="max-height:380px ;height: auto;overflow-y: auto;">
                        </div>
                        <div class="modal-footer justify-content-between">
                        <button type="button" class="back-btn red-btn" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="add-btn HideForHistory">@if(isset($Slip->vLocation) && ($Slip->vLocation =='On route to the lab' || $Slip->vLocation=='On route to the office')) Drop Off @else Pick Up @endif</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>              
             {{-- For Open Driver Model history End Jun 1 2022 --}}

             {{-- Edit Sage Modal Start --}}
             <div class="modal fade" id="EditSatageModal">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="EditSatageModalLabel">Adding stage notes</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    
                    <div class="modal-body">
                        <table class="tslip driver-inner-model" style="border-bottom: 1px solid #eff2f5;">
                            <tbody>
                              <tr>
                                <th>Office:</th>
                                <td><span>{{$CustomerLabData->vOfficeName}}</span></td> 
                                <th>Patient:</th>
                                <td><span>{{$Case->vPatientName}}</span></td>
                                <th>Case #</th>
                                <td><span>{{$Case->vCaseNumber}}</span></td>
                                <th>Pan #</th>
                                <td><span>{{$Case->vCasePanNumber?$Case->vCasePanNumber:'N/A'}}</span></td>
                              </tr>
                              </tbody>
                          </table>
                          <div id="NotesRespose" style="max-height: 200px;height: auto;overflow-y: auto;">
                              
                          </div>
                    </div>
                   
                    @if((isset($Slip->vLocation) && ($Slip->vLocation =='In office ready to pickup' || $Slip->vLocation == 'On route to the office') ))
                        <div class="modal-body">
                            <textarea class="form-control" rows="6" id="tDescription_Ed" name="tDescription_Ed" placeholder="Description" ></textarea>
                            <span id="tDescription_Ed_error" style="display: none;color:red;">This field is required</span>
                        </div>
                        <div class="modal-footer justify-content-between">
                        <button type="button" class="back-btn red-btn" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="add-btn EditStageSubmit">Submit</button>
                        </div>
                        @else
                        <br>
                    @endif
                  </div>
                </div>
              </div>        
             {{-- Edit Sage Modal End --}}

            {{-- For Open Call Log history Start Jun 17 2022 --}}
            <div class="modal fade mt-10" id="CallLogModel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="CallLogModelLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="CallLogModelLabel">Call Log History</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                        <div class="modal-body" id="CallLogRespose">
                        </div>
                        <div class="modal-footer justify-content-between">
                        <button type="button" class="back-btn red-btn" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
                </div>              
                {{-- For Open Call Log history End Jun 17 2022 --}}

                 {{-- For Open Call Log  Start Jun 17 2022 --}}
                <div class="modal fade mt-10" id="CallLogStoreModel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="CallLogStoreModelLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="CallLogStoreModelLabel">Add call</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form action="{{route('admin.labcase.insert_call_log')}}" id="CallLogfrm" method="post" >
                            @csrf
                            <div class="modal-body" id="CallLogStoreRespose">
                            </div>
                            <div class="modal-footer justify-content-between">
                                <input type="hidden" name="vSlipNumber" value="{{$Slip->vSlipNumber}}">
                            <button type="button" class="back-btn red-btn" data-bs-dismiss="modal">Close</button>
                            <button type="button" class="add-btn SubmitCallLog">Submit</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>              
                 {{-- For Open Call Log  End Jun 17 2022 --}}
            </div>
        </div>
    </div>
</div>


<iframe id="printf" name="printf" style="display: none;"></iframe>
@endsection
@section('custom-js')

{{-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.0/dist/jquery.min.js"></script> --}}

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>
<script>
    // Edit Stage Start
    $('#edit_stage').click(function(){
        $('#tDescription_Ed_error').hide();
       $('#EditSatageModal').modal('show');
    });
    $('.EditStageSubmit').click(function(){
        tDescription_Ed = $.trim($('#tDescription_Ed').val());
        let iSlipId = "{{$Slip->iSlipId}}";
        let vOfficeName = "{{$CustomerLabData->vOfficeName}}";
        let vPatientName = "{{$Case->vPatientName}}";
        let vCaseNumber = "{{$Case->vCaseNumber}}";
        let vCasePanNumber = "{{$Case->vCasePanNumber}}";
        error = false;
        if(tDescription_Ed.length == 0)
        {
            $('#tDescription_Ed_error').show();
            error = true;
        }
        if(error == false)
        {
            $.ajax({
            url: "{{route('admin.labcase.edit_stage_notes')}}",
            type: "post",
            data: {
                tDescription_Ed:tDescription_Ed,
                iSlipId: iSlipId,
                vSlipNumber: "{{$Slip->vSlipNumber}}",
                iCaseId: "{{$Case->iCaseId}}",
                vPatientName:vPatientName,
                vOfficeName:vOfficeName,
                vCaseNumber:vCaseNumber,
                vCasePanNumber:vCasePanNumber,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {
                    $("#tDescription_Ed").val("");
                    // $('#EditSatageModal').modal('toggle');
                    notification_success("Stage notes updated successfully");
                    stage_notes_history();
                    $("#EditSatageModal #NotesRespose").animate({ scrollTop: $('#EditSatageModal #NotesRespose').prop("scrollHeight")}, 'slow');
                }
            });
        }
        else
        {
            return false;
        }
        
    });
    // Edit Stage End
    // For Open Driver Model history Start Jun 1 2022
    $('.driverModel').click(function(){
        let iSlipId = "{{$Slip->iSlipId}}";
        let vOfficeName = "{{$CustomerLabData->vOfficeName}}";
        let vPatientName = "{{$Case->vPatientName}}";
        let vCaseNumber = "{{$Case->vCaseNumber}}";
        let vCasePanNumber = "{{$Case->vCasePanNumber}}";
        let Status = $(this).attr("data-name");
        let HistoryType = $(this).attr("data-hname");
        $('#DriverModel').modal('show');
        if(HistoryType == 'DriverHistory')
        {
            $('.HideForHistory').hide();
        }
        else
        {
            $('.HideForHistory').show();
        }
        $.ajax({
            url: "{{route('admin.labcase.view_driverhistory')}}",
            type: "post",
            data: {
                iCaseId: "{{$Case->iCaseId}}",
                iSlipId: iSlipId,
                vPatientName:vPatientName,
                vOfficeName:vOfficeName,
                vCaseNumber:vCaseNumber,
                vCasePanNumber:vCasePanNumber,
                CurrentStatus:Status,
                HistoryType:HistoryType,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {
                $("#DriverRespose").html(response);
                setTimeout(function(){
                    $("#DriverModel .modal-body").animate({ scrollTop: $('#DriverModel .modal-body').prop("scrollHeight")}, 'slow');
                }, 500);
            }
        });
    });

    $('#edit_stage').click(function(){
        stage_notes_history();
    });
    $(document).ready(function(){
        stage_notes_history();
        next_preview_exist()
    })

    function next_preview_exist()
    {
        let iSlipId = "{{$Slip->iSlipId}}";
        let iCaseId = "{{$Case->iCaseId}}";
        $.ajax({
            url: "{{route('admin.labcase.next_previous_exist')}}",
            type: "post",
            data: {
                iCaseId: "{{$Case->iCaseId}}",
                iSlipId: iSlipId,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {  
                $('#nextStage').html(response);
            }
        });
    }
    function stage_notes_history(){
        let iSlipId = "{{$Slip->iSlipId}}";
        let vOfficeName = "{{$CustomerLabData->vOfficeName}}";
        let vPatientName = "{{$Case->vPatientName}}";
        let vCaseNumber = "{{$Case->vCaseNumber}}";
        let vCasePanNumber = "{{$Case->vCasePanNumber}}";
        $.ajax({
            url: "{{route('admin.labcase.view_notes')}}",
            type: "post",
            data: {
                iCaseId: "{{$Case->iCaseId}}",
                iSlipId: iSlipId,
                vPatientName:vPatientName,
                vOfficeName:vOfficeName,
                vCaseNumber:vCaseNumber,
                vCasePanNumber:vCasePanNumber,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {
                $("#NotesRespose").html(response);
                $("#tDescription").html(response);
                setTimeout(function(){
                    $("#EditSatageModal #NotesRespose").animate({ scrollTop: $('#EditSatageModal #NotesRespose').prop("scrollHeight")}, 'slow');
                    $("#virtual_slip_model #tDescription").animate({ scrollTop: $('#virtual_slip_model #tDescription').prop("scrollHeight")}, 'slow');
                }, 500);
                 
            }
        });
    }


    // For Open Call Log Model history Start
    function Call_log_model()
    {
        let iSlipId = "{{$Slip->iSlipId}}";
        let vOfficeName = "{{$CustomerLabData->vOfficeName}}";
        let vPatientName = "{{$Case->vPatientName}}";
        let vCaseNumber = "{{$Case->vCaseNumber}}";
        let vCasePanNumber = "{{$Case->vCasePanNumber}}";
        
        $('#CallLogModel').modal('show');
        $.ajax({
            url: "{{route('admin.labcase.view_call_log')}}",
            type: "post",
            data: {
                iSlipId: iSlipId,
                iCaseId: "{{$Case->iCaseId}}",
               
                vPatientName:vPatientName,
                vOfficeName:vOfficeName,
                vCaseNumber:vCaseNumber,
                vCasePanNumber:vCasePanNumber,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {
                $("#CallLogRespose").html(response);
            }
        });
    }

    function Call_log_store_model()
    {
        let iSlipId = "{{$Slip->iSlipId}}";
        let vOfficeName = "{{$CustomerLabData->vOfficeName}}";
        let vPatientName = "{{$Case->vPatientName}}";
        let vCaseNumber = "{{$Case->vCaseNumber}}";
        let vCasePanNumber = "{{$Case->vCasePanNumber}}";
        $('#CallLogStoreModel').modal('show');
        $.ajax({
            url: "{{route('admin.labcase.store_call_log')}}",
            type: "post",
            data: {
                iSlipId: iSlipId,
                iCaseId: "{{$Case->iCaseId}}",
             
                vPatientName:vPatientName,
                vOfficeName:vOfficeName,
                vCaseNumber:vCaseNumber,
                vCasePanNumber:vCasePanNumber,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {
                $("#CallLogStoreRespose").html(response);
            }
        });
    }

    $('.SubmitCallLog').click(function(){
    iCallId = $("#id").val();
    dDate           = $("#dDate").val();
    iQuantity       = $("#iQuantity").val();
    vName       = $("#vName").val();
    tDescription  = $("#tDescriptions").val();
        
    var error = false;

       
        if (tDescription.length == 0) 
        {
            $("#tDescription_error").show();
            error = true;
        } 
        else 
        {
            $("#tDescription_error").hide();
        }
        if(vName.length == 0)
        {
            $("#vName_error").show();
            error = true;
        } 
        else 
        {
            $("#vName_error").hide();
        }
        setTimeout(function(){
            if(error == true){
                return false;
            } 
            else {
                $("#CallLogfrm").submit();
                return true;
            }
        }, 1000);
        
    });
    // For Open Call Log Model history End

    $(document).on('click', '#ReadyToSend', function() {
        swal({
                title: "You are about to logout {{$Case->vPatientName}}",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((Ok) => {
                if (Ok) {
                    let iSlipId = "{{$Slip->iSlipId}}";
                    let vOfficeName = "{{$CustomerLabData->vOfficeName}}";
                    let vPatientName = "{{$Case->vPatientName}}";
                    let vCaseNumber = "{{$Case->vCaseNumber}}";
                    let vCasePanNumber = "{{$Case->vCasePanNumber}}";
                    setTimeout(function() {
                        $.ajax({
                            url: "{{route('admin.labcase.ready_to_send')}}",
                            type: "post",
                            data: {
                                iSlipId: iSlipId,
                                vPatientName:vPatientName,
                                vOfficeName:vOfficeName,
                                vCaseNumber:vCaseNumber,
                                vCasePanNumber:vCasePanNumber,
                                _token: '{{csrf_token()}}',
                            },
                            success: function(response) {
                                if(response)
                                {
                                    location.reload();
                                }
                            }
                        });
                    }, 1000);
                }
            })
    });
    // For Open Driver Model history End Jun 1 2022

    $(document).ready(function() {
       
        $('#virtual_slip_model').addClass('show d-block');
    });
    
    $(document).on('click', '#back', function() {
        window.location.href = " {{route('admin.labcase')}}";

    });

    $(document).on('click', '#image_popup', function() {
        view_attacement();
    });

    function view_attacement()
    {
        iSlipId = "{{$Slip->iSlipId}}";
        iCaseId= "{{$Case->iCaseId}}";
        $('#add_attachments_model').addClass('show d-block');

        $.ajax({
            url: "{{route('admin.labcase.view_attacement')}}",
            type: "post",
            data: {
                iSlipId: iSlipId,
                iCaseId:iCaseId,
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
                $("#cancel_attacement").trigger("click");
                $("#attachment_list").html(response);
                Dropzone.forElement('#dropzonewidget').removeAllFiles(true);

            }
        });
    }
    $(document).on('click', '#add_attachments_model_close', function() {
        $("#add_attachments_model").removeClass("show d-block");

    });


    $(document).on("click", ".print_paper", function() {
        iCaseId = $(this).data("case");
        iSlipId = $(this).data("slip");

        $.ajax({
            url: "{{route('admin.labcase.paper_slip')}}",
            type: "post",
            data: {
                iCaseId: iCaseId,
                iSlipId: iSlipId,
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
                var newWin = window.frames["printf"];
                setTimeout(function(){

                newWin.document.write(response);
                newWin.document.close();
                },500);
            }
        });
    });

    // Readmore Read less start
    function ReadMoreLess(change_txt,selected_id) {
       if(change_txt == 'less_read')
       {
           $('#less_read'+selected_id).hide();
           $('#full_read'+selected_id).show();
       }
       else
       {
           $('#less_read'+selected_id).show();
           $('#full_read'+selected_id).hide();
       }
    }
    // Readmore Read less end


    $(document).on('click', '#attachment_delete', function() {

        swal({
                title: "Are you sure delete this items.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");


                    setTimeout(function() {
                        $.ajax({
                            url: "{{route('admin.labcase.delete_gallery_slip')}}",
                            type: "get",
                            data: {
                                id: id
                            },
                            success: function(response) {
                        
                                setTimeout(function(){
                                    view_attacement()
                                },500);
                            }
                        });
                    }, 1000);
                }
            })
    });

    var myimg = "<div class=''><i class='fas fa-cloud-upload-alt upload'></i></div>";
    Dropzone.options.dropzonewidget = {
        paramName: "vImage",
        dictDefaultMessage: myimg + '<br>' + "<h4 class='darg'>Drag and drop your Image,Video And Audio here.</h4>" + '<br>' + "<span> Once uploaded, you can select and drag images to recorder below.</span>" + '<br>' + "<a href='javascript:;' class='album-addbtn pass btn mt-5' >Add Image </a>",
       // acceptedFiles: "image/*, video/* , audio/*",
        maxFilesize: 200 // MB,
    };

    $(document).on('click', '#submit_attacement', function() {

        $.ajax({
            url: "{{route('admin.labcase.attacement_newstage')}}",
            type: "post",
            data: {
                iCaseId: "{{$Slip->iSlipId}}",
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
                $("#add_attachments_model_close").trigger("click");
                $("#attachment_list").html(response);
                Dropzone.forElement('#dropzonewidget').removeAllFiles(true);
                setTimeout(function(){
                    view_attacement()
                },500);
            }
        });

    });

    // // Update Billing Data Start
    // $( window ).on("load", function() {
    //     @if((isset($upper_product->iSlipProductId) && isset($lower_product->iSlipProductId) && $upper_product->eStatus =='Finished' && $lower_product->eStatus =='Finished') || (isset($upper_product->iSlipProductId) && !isset($lower_product->iSlipProductId) && $upper_product->eStatus =='Finished') || (!isset($upper_product->iSlipProductId) && isset($lower_product->iSlipProductId) && $lower_product->eStatus =='Finished'))

    //         $.ajax({
    //             url: "{{route('admin.labcase.update_billing')}}",
    //             type: "post",
    //             data: {
    //                 iCaseId : "{{isset($Case->iCaseId)?$Case->iCaseId:''}}",
    //                 iSlipId : "{{isset($Slip->iSlipId)?$Slip->iSlipId:''}}",
    //                 _token: '{{csrf_token()}}',
    //             },
    //             success: function(response) {
                    
    //             }
    //         });
    //     @endif 
    // });
   
    // // Update Billing Data End
</script>

@endsection