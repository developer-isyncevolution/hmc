@if (!empty($Upper_category_addons))    
    @if (count($Upper_category_addons) > 1)
        <option value="">Category</option>
    @endif
    @foreach($Upper_category_addons as $key => $add_ons_cat)
        <option value="{{$add_ons_cat->iAddCategoryId}}"> {{$add_ons_cat->vCategoryName}}</option>
    @endforeach
@else
    <option value="">No Category</option>
@endif
