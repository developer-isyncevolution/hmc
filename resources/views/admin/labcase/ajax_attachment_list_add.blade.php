@if(count($data) > 0)
@foreach($data as $value)
@php
$infoPath = pathinfo(asset('/uploads/slip_gallery/'.$value->vName));
$ext = $infoPath['extension'];
@endphp

@if ($ext == 'mp4' || $ext == 'mov' || $ext == 'mkv')

        <div class="col-lg-4 galleryimg"  >
            <a target="_blank" href="{{asset('uploads/slip_gallery/'.$value->vName)}}" data-fancybox="group" data-width="800" data-height="800" class="fancybox-gallery" data-bs-toggle="tooltip" data-bs-placement="top" title="{{$value->vName}}">
                <p class="text-center mt-2">
                    <video width="120" height="150" controls autoplay="" id="video">
                        <source id="vid" src="{{asset('uploads/slip_gallery/'.$value->vName)}}" type="video/mp4">
                    </video>
                    <a class="delete" id="attachment_delete" href="javascript:;" data-id="{{$value->iSlipAttachmentId}}"><i class="la la-trash text-danger"></i>
                    </a>
                </p>
            </a>
        </div>
    @elseif ($ext == 'mp3' || $ext == 'ogg' || $ext == 'flac')

    <div class="col-lg-4 galleryimg">
        <a target="_blank" href="#audio" data-fancybox="group" data-width="800" data-height="800" data-bs-toggle="tooltip" data-bs-placement="top" title="{{$value->vName}}">
            <p class="text-center mt-2">
                <video width="120" height="100" controls autoplay="" id="audio">
                    <source id="" src="{{asset('uploads/slip_gallery/'.$value->vName)}}" type="audio/mp3">
                </video>
        </a> <a class="delete" id="attachment_delete" href="javascript:;" data-id="{{$value->iSlipAttachmentId}}"><i class="la la-trash text-danger"></i></a></p>
        </a>
    </div>
    @elseif($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg')  
        
    <div class="col-lg-4 galleryimg">
        <a target="_blank" href="{{asset('uploads/slip_gallery/'.$value->vName)}}" data-fancybox="group" data-width="800" data-height="800" data-bs-toggle="tooltip" data-bs-placement="top" title="{{$value->vName}}">
            <div class="mained">
                <img src="{{asset('uploads/slip_gallery/'.$value->vName)}}" class="imgese" />
            </div>
        </a>
        <a class="delete" id="attachment_delete" href="javascript:;" data-id="{{$value->iSlipAttachmentId}}"><i class="la la-trash text-danger"></i></a>
    </div>  
    @else
    
    <div class="col-lg-4 galleryimg">
    
      
      {{-- <a class="delete" download="{{$value->vName}}" href="{{asset('uploads/slip_gallery/'.$value->vName)}}" style="right: 5px;top: 113px;" ><i style="color: #eff2f5" class="fa fa-download"></i>
      </a>
    
      <i style="font-size: 149px;color: #b3b3c1;" class="fas fa-file-alt">
        <a href="#" download="{{asset('uploads/slip_gallery/'.$value->vName)}}"> <i style="color: #777785; font-size: 20px;" class="fas fa-download"></i></a>
      </i>
        <a class="delete" id="attachment_delete" href="javascript:;" data-id="{{$value->iSlipAttachmentId}}"><i class="la la-trash text-danger"></i>
        </a> --}}

           
          
            <i style="font-size: 149px;color: #b3b3c1;" class="fas fa-file-alt">
              <a href="#" download="{{asset('uploads/slip_gallery/'.$value->vName)}}"> </a>
            </i>
            <a download="{{$value->vName}}" href="{{asset('uploads/slip_gallery/'.$value->vName)}}" style="right: 5px;top: 113px;" > <i style="color: #777785; font-size: 20px;" class="fas fa-download"></i>
            </a>
            {{-- <a class="delete" download="{{$value->vName}}" href="{{asset('uploads/slip_gallery/'.$value->vName)}}" style="right: 5px;top: 113px;" ><i style="color: #777785; font-size: 20px;" class="fas fa-download"></i>
            </a> --}}
              <a class="delete" id="attachment_delete" href="javascript:;" data-id="{{$value->iSlipAttachmentId}}"><i class="la la-trash text-danger"></i>
              </a>
             
          
       
    </div>        
@endif
@endforeach
@else
<div class="col-md-12">
    <center><b>No record found</b></center>
</div><br>&nbsp;
@endif







