
@php
    $eVirualSlipAccess = \App\Libraries\General::check_permission_slip('eVirualSlip');
    $ePaperSlipAccess = \App\Libraries\General::check_permission_slip('ePaperSlip');
	$eReadyToSendAccess = \App\Libraries\General::check_permission_slip('eReadyToSend');
	$colspan = '8';
    $eEditStageAccess = \App\Libraries\General::check_permission_slip('eEditStage');
    $eNewStageAccess = \App\Libraries\General::check_permission_slip('eNewStage');
    $eReadyToSendAccess = \App\Libraries\General::check_permission_slip('eReadyToSend');
    $eViewDriverHistoryAccess = \App\Libraries\General::check_permission_slip('eViewDriverHistory');
    $ePickUpAccess = \App\Libraries\General::check_permission_slip('ePickUp');
    $eDropOffAccess = \App\Libraries\General::check_permission_slip('eDropOff');
    $eDirectionAccess = \App\Libraries\General::check_permission_slip('eDirection');
    $eAddCallLogAccess = \App\Libraries\General::check_permission_slip('eAddCallLog');
    $eHistoryCallAccess = \App\Libraries\General::check_permission_slip('eHistoryCall');
    $eLastModifiedAccess = \App\Libraries\General::check_permission_slip('eLastModified');
@endphp
@if(isset($ePaperSlipAccess) && $ePaperSlipAccess =='No' && isset($eVirualSlipAccess) && $eVirualSlipAccess =='No')
@php
$colspan = '6';
@endphp
@elseif(isset($ePaperSlipAccess) && $ePaperSlipAccess =='No')
@php
$colspan = '7';
@endphp
@elseif(isset($eVirualSlipAccess) && $eVirualSlipAccess =='No')
@php
$colspan = '7';
@endphp

@endif


@if(count($data) > 0)
@foreach($data as $key => $value)
<tr>
	@if(count($data) == 1)
		<input type="hidden" name="count_data" id="count_data" value="1">
	@endif
	<td>
		<span class="badge d-inline-block" style="background-color:{{ $value->vColor }}">@if(!empty($value->vCasePanNumber)){{ $value->vCasePanNumber }} @else {{"----"}}@endif</span>
	</td>
	<td>{{isset($value->vOffice__Code)&& !empty($value->vOffice__Code)?$value->vOffice__Code:$value->office_name__slip}}
	
		@if($key == 0)
			<input type="hidden" id="office_code_cop"  value="{{isset($office_codes)?$office_codes:''}}">
		@endif
		
	</td>
	<td>{{$value->vPatientName}}</td>
	@if(isset($eVirualSlipAccess) && $eVirualSlipAccess =='Yes')
	<td>
		<a class="slip_redirect" href="{{route('admin.labcase.virtual_slip',[$value->iCaseId,$value->iSlipId])}}" >
			<i class="fal fa-eye" style="color: #054b97;font-size: 18px;"></i>
		</a>

	</td>
	@endif
	@if(isset($ePaperSlipAccess) && $ePaperSlipAccess =='Yes')
	<td>
		<div class="d-flex justify-content-center">
			<a href="{{route('admin.labcase.addon_listing_modal',[$value->iCaseId,$value->iSlipId,$value->iLabId])}}" data-iLabId={{$value->iLabId}} class="btn add-btn mx-1 "><i class="fal fa-plus" style="color: #ffffff;font-size: 18px;"></i> Add ons</a>
			&nbsp;&nbsp;&nbsp;
			<a href="javascript:;" data-case="{{$value->iCaseId}}" data-slip="{{$value->iSlipId}}" class="me-5 print_paper mt-3">
				<i class="fal fa-print" style="color: #054b97;font-size: 18px;"></i>
			</a>
			<div class="form-check form-check-sm form-check-custom form-check-solid ms-2">
				<input id="Case_ID_{{$value->iCaseId}}" type="checkbox" name="Case_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iSlipId}}">
				<label for="Case_ID_{{$value->iCaseId}}">&nbsp;</label>
			</div>
			
		</div>
	</td>
	@endif
	<td >
		<div class="d-flex" style="justify-content: center;gap:35px;align-items: center;">
		@if(isset($eReadyToSendAccess) && $eReadyToSendAccess =='Yes' && isset($value->vLocation) && $value->vLocation =='In lab')
			{{-- <a href="{{route('admin.labcase.ready_to_send_qr',[$value->iSlipId])}}" title="Ready to send" id="ReadyToSend"> --}}
			
			<a href="#" title="Ready to send"  id="ReadyToSend" data-iCaseId="{{$value->iCaseId}}" data-iSlipId="{{$value->iSlipId}}" data-vPatientName="{{$value->vPatientName}}" data-vCasePanNumber="{{$value->vCasePanNumber}}" data-vPatientName="{{$value->vPatientName}}" data-vCaseNumber="{{$value->vCaseNumber}}" data-vOfficeName="{{$value->vOfficeName}}">
			   <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.85 32" style="
				  fill: #0E66B2;
				  height: 20px;
			   "><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M11.9,32a13.51,13.51,0,0,1-.69-1.18c-1.08-2.52-2.16-5-3.2-7.56A2,2,0,0,0,6.77,22c-1.74-.7-3.47-1.43-5.2-2.16A2.11,2.11,0,0,1,0,17.52a2.47,2.47,0,0,1,1.37-1.67Q4.85,13.92,8.26,12q10-5.72,20-11.4A3.87,3.87,0,0,1,29.91,0a2,2,0,0,1,1.91,2.45c-.4,2.7-.83,5.4-1.25,8.1q-1.21,8-2.44,15.89c-.07.49-.14,1-.23,1.47a2.11,2.11,0,0,1-3.2,1.7c-2.06-.87-4.13-1.71-6.18-2.63a1.27,1.27,0,0,0-1.67.29c-1.39,1.42-2.84,2.79-4.27,4.17C12.42,31.59,12.25,31.72,11.9,32ZM30.79,1.74l-.25-.09L13,23.47c.26.13.37.21.49.26L25.08,28.6c1.18.5,1.66.2,1.86-1.09q1-6.6,2-13.19.93-6,1.85-12A3.26,3.26,0,0,0,30.79,1.74Zm-1.94,0-.11-.16L27.9,2,8.82,12.89l-7.06,4a1.13,1.13,0,0,0-.71,1,1,1,0,0,0,.71.93c2,.83,4,1.65,6,2.49ZM24.69,7.24l-.12-.11L8.62,22l2.6,6.21.17,0c0-.21,0-.42,0-.63,0-.91.05-1.82,0-2.73a2.71,2.71,0,0,1,.73-2c2-2.46,4-4.95,6-7.43ZM12.54,29.83l.15.08,3.89-3.7-4-1.68Z"></path></g></g></svg></a> 
		@elseif($value->vLocation =='In lab ready to pickup' && \App\Libraries\General::admin_info()['iCustomerTypeId'] == 1)
			<a title="Ready to send undo" style="text-align: end" href="#" class="undoStatus" data-slip_id="{{$value->iSlipId}}" data-case_id="{{$value->iCaseId}}" class="btn-icon delete-icon  me-4">
				<i style="color:#0E66B2" class="fa fa-undo" aria-hidden="true"></i>
			</a>
		@endif
		

		{{-- Pick up drop off --}}
		
		<div style="">
		@if(isset($eDropOffAccess) && $eDropOffAccess =='Yes' && ($value->vLocation =='On route to the lab' || $value->vLocation == 'On route to the office'))
		@if($value->vLocation =='On route to the office')
			@php
				$title_drop = 'Office';
			@endphp   
		@elseif($value->vLocation =='On route to the lab')
			@php
				$title_drop = 'Lab';
			@endphp
		@else
			@php
				$title_drop = '';
			@endphp
		@endif
			<p class="mb-0" style="color:red;">{{$title_drop}}</p>
			<a href="{{route('admin.labcase.view_driverhistory_by_listing',[$value->iCaseId,$value->iSlipId,$title_drop])}}" title="Drop Off {{$title_drop}}" @if(isset($value->vLocation) && $value->vLocation =='On route to the lab') data-name="DropOff" @endif class="driverModel " @if(isset($value->vLocation) && $value->vLocation =='On route to the office') data-name="LabDropUp" @endif>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.38 34.13" style="&#10;    fill: red;&#10;    height: 30px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M22.55,28.77a1.1,1.1,0,0,0-1.13-.68c-.87,0-1.76,0-2.64,0l-.56,0a5.28,5.28,0,0,1,0-.66V17.85h-.59v.25c0,3.12,0,6.23,0,9.35v.63H14.47a2.54,2.54,0,0,0-.39,0,1,1,0,0,0-.84.65.85.85,0,0,0,.24.9l2.1,2.27c.53.57,1,1.16,1.6,1.71a1,1,0,0,0,1.58,0l3.47-3.72C22.51,29.57,22.77,29.25,22.55,28.77Zm-.64.48a1,1,0,0,1-.15.19l-3.28,3.49c-.45.48-.61.48-1.07,0-1.09-1.15-2.19-2.32-3.27-3.49a1.06,1.06,0,0,1-.19-.23.31.31,0,0,1,.21-.49,2.12,2.12,0,0,1,.44,0h6.69a2.55,2.55,0,0,1,.39,0C22,28.76,22.07,29,21.91,29.25Z" style="&#10;    stroke: red;&#10;    stroke-width: 0.25px;&#10;"/><path d="M12.36,17.24c0-.65-.09-1.12-.1-1.6V8.08A1.91,1.91,0,0,1,14.31,6c2.6,0,5.21,0,7.81,0a2,2,0,0,1,2.24,2.29q0,3.78,0,7.56c0,1.64-.66,2.26-2.27,2.26-4.17,0-8.33,0-12.5,0a2.19,2.19,0,0,1-2.22-1.35C7,16,6.6,15.33,6.21,14.62L6,14.68v4.73c.76.67,1.59,1.43,2.47,2.14a5,5,0,0,1,1.83,3.33c.23,1.71.49,3.42.73,5.13a2,2,0,0,1-1.76,2.43A1.93,1.93,0,0,1,7.2,30.59c-.27-1.62-.54-3.24-.78-4.86a2.29,2.29,0,0,0-.88-1.57C4.23,23.08,3,22,1.66,20.86A4.48,4.48,0,0,1,0,17.21c0-2,0-3.91,0-5.87A3,3,0,0,1,3.27,8c.51,0,1,0,1.53,0a3.93,3.93,0,0,1,3.49,2.05c.57,1,1.12,2,1.71,3,.24.4.59.73.86,1.12a3,3,0,0,1,.23.57.9.9,0,0,1-1.42-.38C9.05,13.29,8.39,12.17,7.8,11A3.42,3.42,0,0,0,4.32,8.93c-.48,0-1,0-1.44,0a2,2,0,0,0-2,2c-.05,2.32,0,4.65,0,7a3.09,3.09,0,0,0,1.25,2.32c1.45,1.23,2.9,2.45,4.31,3.72a2.25,2.25,0,0,1,.68,1.18c.32,1.67.54,3.36.81,5a2.89,2.89,0,0,0,.18.73,1,1,0,0,0,1.25.62,1,1,0,0,0,.79-1.15c-.3-2.1-.6-4.2-1-6.28a3.33,3.33,0,0,0-1-1.53,27.36,27.36,0,0,0-2.39-2A1.78,1.78,0,0,1,5.07,19c0-1.36,0-2.72,0-4.08,0-.58,0-1.18.73-1.37s1,.3,1.29.83c.37.72.77,1.44,1.15,2.17a1.22,1.22,0,0,0,1.22.73C10.43,17.21,11.39,17.24,12.36,17.24Zm5.92,0c1.33,0,2.66,0,4,0,.8,0,1.17-.32,1.17-1.12q0-4,0-8c0-.81-.37-1.12-1.17-1.12q-3.94,0-7.89,0c-.86,0-1.21.29-1.22,1.17q0,3.95,0,7.89c0,.91.36,1.17,1.3,1.17Z"/><path d="M1.81,3.22A3.16,3.16,0,0,1,5,0,3.25,3.25,0,0,1,8.32,3.21,3.3,3.3,0,0,1,5.05,6.45,3.22,3.22,0,0,1,1.81,3.22ZM5,5.51A2.42,2.42,0,0,0,7.42,3.14,2.42,2.42,0,0,0,5.06.83,2.34,2.34,0,0,0,5,5.51Z"/><path d="M0,22.61l.76-.22a8,8,0,0,1,.11,1q0,3.13,0,6.28c0,.22,0,.45,0,.68.07.79.5,1.23,1.15,1.19s1-.44,1-1.22c0-1.3,0-2.61,0-3.91a4.6,4.6,0,0,1,.23-.94l.38,0a3.75,3.75,0,0,1,.25.83c0,1.38,0,2.77,0,4.16A2,2,0,0,1,2,32.46a2,2,0,0,1-2-2C0,27.89,0,25.31,0,22.61Z"/></g></g></svg>
				<!-- Drop Off -->
			</a>
			@elseif(isset($ePickUpAccess) && $ePickUpAccess =='Yes' && ($value->vLocation =='In office ready to pickup' || $value->vLocation =='In lab ready to pickup'))
				@php
				  $title = '';
				 @endphp
				@if($value->vLocation =='In office ready to pickup')
				 @php
					 $title = 'Office';
				 @endphp   
				@elseif($value->vLocation =='In lab ready to pickup')
				 @php
				  $title = 'Lab';
				 @endphp
				@endif
				<p class="mb-0" style="color:green;">{{$title}}</p>
				<a href="{{route('admin.labcase.view_driverhistory_by_listing',[$value->iCaseId,$value->iSlipId,$title])}}" title="Pick Up {{$title}}" class="@if(isset($value->vLocation) && $value->vLocation =='In office ready to pickup' || $value->vLocation =='In lab ready to pickup') driverModel @endif "  @if(isset($value->vLocation) && $value->vLocation =='In lab ready to pickup') data-name="LabPickUp" @endif>
					<!-- Pick up -->
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.38 34.12" style="&#10;    fill: green;&#10;    height: 30px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M1.83,23.08A1.1,1.1,0,0,0,3,23.77H5.6l.56,0c0,.24,0,.45,0,.65V34h.6v-.25c0-3.12,0-6.23,0-9.35v-.62H9.9a2.67,2.67,0,0,0,.4,0,1,1,0,0,0,.83-.66.83.83,0,0,0-.24-.9c-.69-.76-1.38-1.51-2.09-2.27-.53-.57-1.05-1.16-1.61-1.71a1,1,0,0,0-1.57,0L2.15,22C1.87,22.29,1.61,22.6,1.83,23.08Zm.63-.48a1.46,1.46,0,0,1,.16-.19c1.09-1.17,2.19-2.32,3.28-3.49.45-.48.61-.48,1.07,0l3.27,3.49a1.3,1.3,0,0,1,.18.23.3.3,0,0,1-.2.49,2.17,2.17,0,0,1-.44,0H3.09a2.48,2.48,0,0,1-.39,0C2.4,23.09,2.31,22.86,2.46,22.6Z" style="&#10;    stroke: green;&#10;    stroke-width: 0.25px;&#10;"/><path d="M14.9,17.24a1.22,1.22,0,0,0,1.23-.73c.38-.73.77-1.45,1.15-2.17.28-.53.57-1,1.29-.83s.72.79.72,1.37c0,1.36,0,2.72,0,4.08a1.77,1.77,0,0,1-.73,1.53,27.36,27.36,0,0,0-2.39,2,3.35,3.35,0,0,0-1,1.53c-.41,2.08-.72,4.18-1,6.28A1,1,0,0,0,15,31.49a1,1,0,0,0,1.24-.62,2.9,2.9,0,0,0,.19-.73c.27-1.67.49-3.36.8-5a2.32,2.32,0,0,1,.68-1.18c1.41-1.27,2.87-2.49,4.32-3.72a3.09,3.09,0,0,0,1.25-2.32c0-2.32.06-4.65,0-7a2,2,0,0,0-2-2c-.48-.05-1,0-1.44,0A3.41,3.41,0,0,0,16.58,11c-.59,1.17-1.25,2.29-1.87,3.43a.9.9,0,0,1-1.42.38,3,3,0,0,1,.23-.57c.27-.39.62-.72.86-1.12.59-1,1.13-2,1.71-3A3.93,3.93,0,0,1,19.58,8c.51,0,1,0,1.53,0a3,3,0,0,1,3.26,3.3c0,2,0,3.91,0,5.87a4.51,4.51,0,0,1-1.66,3.65c-1.3,1.1-2.57,2.22-3.89,3.3A2.36,2.36,0,0,0,18,25.73c-.23,1.62-.5,3.24-.77,4.86a1.93,1.93,0,0,1-2.11,1.85A2,2,0,0,1,13.3,30c.24-1.71.51-3.42.74-5.13a5,5,0,0,1,1.83-3.33c.88-.71,1.7-1.47,2.47-2.14V14.68l-.17-.06c-.39.71-.81,1.41-1.16,2.14a2.21,2.21,0,0,1-2.23,1.35c-4.16,0-8.32,0-12.49,0C.68,18.09,0,17.47,0,15.83c0-2.52,0-5,0-7.56A2,2,0,0,1,2.25,6c2.61,0,5.22,0,7.82,0a1.9,1.9,0,0,1,2,2.1q0,3.78,0,7.56c0,.48,0,.95-.09,1.6C13,17.24,14,17.21,14.9,17.24Zm-5,0c.94,0,1.28-.26,1.29-1.17q.06-3.94,0-7.89C11.2,7.29,10.86,7,10,7Q6.06,7,2.11,7C1.31,7,.94,7.32.93,8.13q0,4,0,8c0,.8.38,1.11,1.18,1.12,1.33,0,2.66,0,4,0Z"/><path d="M19.32,6.45a3.3,3.3,0,0,1-3.26-3.24A3.25,3.25,0,0,1,19.33,0a3.17,3.17,0,0,1,3.24,3.22A3.23,3.23,0,0,1,19.32,6.45Zm2.34-3.33A2.34,2.34,0,0,0,19.32.83,2.41,2.41,0,0,0,17,3.14a2.42,2.42,0,0,0,2.38,2.37A2.33,2.33,0,0,0,21.66,3.12Z"/><path d="M24.37,22.61l-.77-.22c0,.38-.1.68-.11,1v6.28c0,.22,0,.45,0,.68-.07.79-.5,1.23-1.15,1.19s-1-.44-1-1.22c0-1.3,0-2.61,0-3.91a4,4,0,0,0-.22-.94l-.38,0a3.28,3.28,0,0,0-.25.83c0,1.38,0,2.77,0,4.16a2,2,0,0,0,1.92,2.05,2,2,0,0,0,2-2C24.39,27.89,24.37,25.31,24.37,22.61Z"/></g></g></svg>
					<!-- Pick Up -->
				</a>
				@endif
		</div>
	</div>
	</td>
	<td>
		<span class="">{{$value->vLocation}}</span>
		
		@if($value->vLocation == "Draft")
			@if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == 1)
			<a href="{{route('admin.labcase.edit_slip_lab',[$value->iCaseId,$value->iSlipId])}}" class="me-5 print_paper">
				<i class="fad fa-pencil"></i>
			</a>
			@else			
			<a href="{{route('admin.labcase.edit_slip',[$value->iCaseId,$value->iSlipId])}}" class="me-5 print_paper">
				<i class="fad fa-pencil"></i>
			</a>
			@endif
		@endif
		@if($value->eIsDeleted != "Yes")
		@if(\App\Libraries\General::admin_info()['customerType'] == 'Lab Admin')
		<a href="javascript:;" data-patient="{{$value->vPatientName}}" id="delete" data-id="{{$value->iCaseId}}" class="btn-icon delete-icon  me-4">
			<i class="fad fa-trash-alt"></i>
		</a>
		@endif
		@else
		@if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == 1)
		<a style="text-align: end" href="{{route('admin.labcase.undo_delete_slip',[$value->iCaseId,$value->iSlipId])}}" data-patient="{{$value->vPatientName}}" class="btn-icon delete-icon  me-4">
			<i style="color:#0E66B2" class="fa fa-undo" aria-hidden="true"></i>
		</a>
	
		@endif
		@endif

	@php 
		$delivery_date = $value->dDeliveryDate;
		$current_date = date("Y-m-d");

		$difference =  date_diff(date_create($delivery_date), date_create($current_date));

		$days = $difference->days;
		$days_text = "";
		$color_class = "";
	@endphp
	
	@if($delivery_date > $current_date)
		@php 
			$days_text = $days ." days to ".date('m/d', strtotime($value->dDeliveryDate)); 
			$color_class = "text-primary";
		@endphp
	@elseif($delivery_date < $current_date)
		@php 
			$days_text = $days ." days past " .date('m/d', strtotime($value->dDeliveryDate)); 
			$color_class = "text-danger";
		@endphp
	@elseif($delivery_date == $current_date)
		@php 
			$days_text = "Due today ".date('m/d', strtotime($value->dDeliveryDate)); 
			$color_class = "text-success";
		@endphp
	@endif

	@if($value->vLocation == "In office")
		@php 
			$days_text = "Delivered"; 
			$color_class = " ";
		@endphp
	@endif


	<td>
		<span class="{{$color_class}}">{{$days_text}}</span>
	</td>
	
	
		<td> {{date('m/d/Y',strtotime($value->dtAddedDate))}} @ {{date('h:i:a',strtotime($value->dtAddedDate))}}</td>
	</td>
</tr>

@endforeach
<tr>
	{{-- <td  class="text-center border-right-0">
		<a href="javascript:;" title="Multiple delete" id="delete_btn" class="btn-icon  delete_permission delete-icon">
			<i class="fad fa-trash-alt"></i>
		</a>
	</td> --}}
	<td class="border-0" colspan="{{$colspan}}">
		<div class="d-flex">
			{!! $paging !!}
		</div>
	</td>
	{{-- <td class="border-0"></td>
	<td colspan="5" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
	<td class="text-end border-left-0">
		{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}}  --}}
		<select class="w-100px show-drop" id ="page_limit">
			<option @if ((isset(Request()->limit_page) && Request()->limit_page == 10) || (isset($limit) && $limit ==10))
				selected
				@endif value="10">10</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 20) || (isset($limit) && $limit ==20))
				selected
				@endif value="20">20</option>
				<option  @if ((isset(Request()->limit_page) && Request()->limit_page == 50) || (isset($limit) && $limit ==50) )
					selected 
				@endif value="50">50</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 100) || (isset($limit) && $limit ==100))
					selected
				@endif value="100">100</option>
		</select>
	</td>
	
</tr>
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif
<script>
	   $(document).on('click', '#ReadyToSend', function() {
		var patientName = '"'+$(this).data("vpatientname")+'"';
		
        swal({
                title: "You are about to logout ",
				text: '"'+patientName+'"',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((Ok) => {
                if (Ok) {
                    let iSlipId = $(this).data("islipid");
                    let vOfficeName = $(this).data("vofficename");
                    let vPatientName = $(this).data("vpatientname");
                    let vCaseNumber = $(this).data("vcasenumber");
                    let vCasePanNumber = $(this).data("vcasepannumber");
                    setTimeout(function() {
                        $.ajax({
                            url: "{{route('admin.labcase.ready_to_send')}}",
                            type: "post",
                            data: {
                                iSlipId: iSlipId,
                                vPatientName:vPatientName,
                                vOfficeName:vOfficeName,
                                vCaseNumber:vCaseNumber,
                                vCasePanNumber:vCasePanNumber,
                                _token: '{{csrf_token()}}',
                            },
                            success: function(response) {
                                if(response)
                                {
									notification_success($(this).data("vpatientname")+" logged out successfully");
									AjaxListing();
                                }
                            }
                        });
                    }, 1000);
                }
            })
    });

	$('.undoStatus').click(function(){
		
		swal({
                title: "Are you want to undo status",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((Ok) => {
                if (Ok) {
					var iSlipId = $(this).attr('data-slip_id');
					var iCaseId = $(this).attr('data-case_id');
                    setTimeout(function() {
                        $.ajax({
                            url: "{{route('admin.labcase.undo_status')}}",
                            type: "post",
                            data: {
                                iSlipId: iSlipId,
								iCaseId:iCaseId,
                                _token: '{{csrf_token()}}',
                            },
                            success: function(response) {
                                if(response =='success')
                                {
									notification_success("Undo status successfully");
									AjaxListing();
                                }
								else
								{
									notification_error("Something went wrong");
								}
                            }
                        });
                    }, 1000);
                }
            })
	});
	var storedCheckboxArray = JSON.parse(sessionStorage.getItem("items"));
	// var checkedBox = new Array();
	if(storedCheckboxArray ==null)
	{
		var checkedBox = new Array();
	}
	else
	{
		var checkedBox = storedCheckboxArray;
	}
	$('.checkboxall').change(function() {
        if($(this).is(":checked")) {
           checkedBox.push($(this).val());
        }
        else
        {
            checkedBox.pop($(this).val());
        }
        sessionStorage.setItem("items", JSON.stringify(checkedBox));
        //$('#textbox1').val($(this).is(':checked'));        
    });

</script>