

<div class="row other_layots image-checkbox-main-div" id="show_layout{{$layout_num}}">
@for ($i = ($start_num+1); $i <= $end_num; $i++)

    <div class="col">
        <label class="image-checkbox imgCheck{{$layout_num}} CheckImg{{$i}}" data-id="{{$i}}">
            <img class="img-responsive" src="https://dummyimage.com/850x400/ffffff/000000&text={{$i}}" />
        </label>
        <input type="checkbox" id="layout_checkbox_{{$i}}" class="layout_box{{$i}} checkboxCloseModal" name="Layoutimage[]"  value="{{$i}}" />
    </div>
    @endfor
</div>