@if (!empty($Stage))

@if (count($Stage) > 0)
@foreach($Stage as $key => $Stage_val)


@if($eType == "Upper")

    <div class="d-flex justify-content-between align-items-center py-1">
        <div class="form-check form-check-sm form-check-custom form-check-solid">    
        <i class="fas fa-check-circle" style="font-size:20px;color: #0095e8;"></i>
            <label for="c-id" class="ps-1 @if($eType == 'Upper' && $Stage_val->vName =='Finish') UpperOldStage @elseif($eType == 'Lower' && $Stage_val->vName =='Finish') LowerOldStage @endif" > {{$Stage_val->vName}}</label>
        </div>    
        <span>{{date('m/d/Y',strtotime($Stage_val->dtAddedDate))}}</span>
    </div>
@else
    <div class="d-flex justify-content-between align-items-center py-1">            
        <span>{{date('m/d/Y',strtotime($Stage_val->dtAddedDate))}}</span>
        <div class="form-check form-check-sm form-check-custom form-check-solid">    
            <label for="c-id"> {{$Stage_val->vName}} </label>
            <i class="fas fa-check-circle ps-1" style="font-size:20px;color: #0095e8;"></i>
        </div>
    </div>
@endif
@endforeach
@endif

@endif