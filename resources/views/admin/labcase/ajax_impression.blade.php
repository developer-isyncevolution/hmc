@if($eType == "Upper")
<ul class="m-0 p-0">

	@foreach($impression as $key=> $impressions_val)

	<li>
		<div class="check-box-wrapper-left">
			<input type="checkbox" name="upper_impression_checkbox[]" data-id="{{$key}}" id="upper_impression_{{$impressions_val->iImpressionId}}" class="low_list inner-checkbox upper_impression_checked" value="{{$impressions_val->iImpressionId}}">
			<label id="upper_imps_name_{{$impressions_val->iImpressionId}}" for="upper_impression_{{$impressions_val->iImpressionId}}" class="low_list inner-checbox-label w-100">{{$impressions_val->vName}}</label>
			<input type="number" class="upper_impressions_qtys low_list low_quantity" name="upper_impression_qty_{{$impressions_val->iImpressionId}}" id="upper_impression_qty_{{$impressions_val->iImpressionId}}" placeholder="qty" min="0" style="display: none;" value="">
		</div>
		<div class="c-box-qty-right">

			{{-- <a href="#" class="edit-button">
				Add & <i class="fal fa-times close-icon"></i>
			</a> --}}

		</div>
	</li>

	<div class="text-danger" style="display: none;" id="upper_impression_qty_error_{{$impressions_val->iImpressionId}}">Enter {{$impressions_val->vName}} qty</div>

	@endforeach

	<div class="text-danger" style="display: none;" id="upper_impression_checkbox_error">Select any one impression </div>

	<li>
		<a href="javascript:;" id="upper_add_impressions_btn" class="btn add-btn w-100 my-2">Add Selected Impressions</a>
	</li>
</ul>
@elseif($eType == "Lower")

<ul class="m-0 p-0">
	@foreach($impression as $impressions_val)

	<li>
		<div class="check-box-wrapper-left">
			<input type="checkbox" name="lower_impression_checkbox[]" id="lower_impression_{{$impressions_val->iImpressionId}}" class="low_list inner-checkbox lower_impression_checked" value="{{$impressions_val->iImpressionId}}">
			<label id="lower_imps_name_{{$impressions_val->iImpressionId}}" for="lower_impression_{{$impressions_val->iImpressionId}}" class="low_list inner-checbox-label w-100">{{$impressions_val->vName}}</label>
			<input type="number" class=" lower_impressions_qtys low_list low_quantity" name="lower_impression_qty_{{$impressions_val->iImpressionId}}" id="lower_impression_qty_{{$impressions_val->iImpressionId}}" style="display: none;" placeholder="qty" min="0" value="">
		</div>
		<div class="c-box-qty-right">

			{{-- <a href="#" class="edit-button">
				Add & <i class="fal fa-times close-icon"></i>
			</a> --}}

		</div>
	</li>

	<div class="text-danger" style="display: none;" id="lower_impression_qty_error_{{$impressions_val->iImpressionId}}">Enter {{$impressions_val->vName}} qty</div>

	@endforeach

	<div class="text-danger" style="display: none;" id="lower_impression_checkbox_error">Select any one impression </div>

	<li>
		<a href="javascript:;" id="lower_add_impressions_btn" class="btn add-btn w-100 my-2">Add Selected Impressions</a>
	</li>
</ul>

@endif