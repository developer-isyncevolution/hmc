<table class="tslip driver-inner-model" style="border-bottom: 1px solid #eff2f5;">
    <tbody>
      <tr>
        <th>Office: <span>{{$data['vOfficeName']?$data['vOfficeName']:'N/A'}}</span></th>
        <th>Patient: <span>{{$data['vPatientName']?$data['vPatientName']:'N/A'}}</span></th>
        <th>Case # <span>{{$data['vCaseNumber']?$data['vCaseNumber']:'N/A'}}</span></th>
        <th>Pan # <span>{{$data['vCasePanNumber']?$data['vCasePanNumber']:'N/A'}}</span></th>
      </tr>
      </tbody>
</table>
@if(count($callHistory)>0)
@foreach ($callHistory as $callHistory_val)
<table class="tslip driver-inner-model mb-5 text-center">
    <tbody>
        <tr>
            <td colspan="8">
              <table class="w-50 mx-auto my-2 text-center">
                <tr>
                  <th>Slip ID: {{isset($callHistory_val->vSlipNumber)?$callHistory_val->vSlipNumber:'N/A'}}</th>
                  <th>Stage: {{isset($callHistory_val->vStageName)?$callHistory_val->vStageName:'N/A'}}</th>
                </tr>
              </table>
            </td>
        </tr>
		@php
		$call_log = $callHistory_val->call_log;
		@endphp
        @if(count($call_log)>0)
        @foreach ($call_log as $call_log_val)
        
        <tr>
          <td style="color: #0e66b2" class="text-start">
            The following call was taken by  {{$call_log_val->vCallTaken?$call_log_val->vCallTaken:'N/A'}} at {{$call_log_val->dDate?date("m/d/Y", strtotime($call_log_val->dDate)):'N/A'}} @ {{$call_log_val->tTime?$call_log_val->tTime:'N/A'}} Person attended
            {{$call_log_val->vName?$call_log_val->vName:'N/A'}}
          </td>
        </tr>
        <tr>
          <td class="font-bold text-start">
              @if(isset($call_log_val->tDescription) && $call_log_val->tDescription!='')
              {{$call_log_val->tDescription}}
              @endif
              <hr style="color:#6c6c6c73" class="m-0">
          </td>
        </tr>
        @endforeach
        @endif
    
      </tbody>
    </table>
@endforeach
@endif