@if (!empty($Product))    
    @if (count($Product) > 1)
        <option value="">Select Product</option>
    @endif
    @foreach($Product as $key => $Product_val)
        <option value="{{$Product_val->iCategoryProductId}}"> {{$Product_val->vName}}</option>
    @endforeach
@else
    <option value="">No Product Found</option>
@endif
