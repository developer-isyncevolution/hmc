<ul class="add-case-heder-ul borded-li" id="lower_rush_fees_div">

    @foreach($data as $value_we)
        @if($value_we['etype'] == "upper")

            <input type="hidden" id="Upper_rush_exist">
            <li>
                @php 
                    $date = explode('-', $value_we['dDate']);
                    $upper_date_formate =  $date[0]. "/" . $date[1]. "/" .$date[2];
                    $upper_date_formate_checked =  $date[0]. "_" . $date[1]. "_" .$date[2];
                @endphp
                
                <div class="form-check">
                    <input data-date="{{$upper_date_formate_checked}}" class="form-check-input upper_radio_click" name="upper_rush" type="radio" value="{{$value_we['iFeesId']}}" id="upper_flexCheckDefault_{{$value_we['iWorkingTime']}}" @checked(isset($UpperdDeliveryRushDate) && $UpperdDeliveryRushDate == $value_we['dDate'])>
                    <label class="form-check-label" for="upper_flexCheckDefault_{{$value_we['iWorkingTime']}}" id= 'upper_checked_{{$upper_date_formate_checked}}'> Days {{$value_we['iWorkingTime']}} = {{$value_we['vFee']}}%</label>
                </div>
                <label  id="upper_rush_dd_{{$value_we['iFeesId']}}">{{$upper_date_formate}}</label>
            </li>
        @else 
            <input type="hidden" id="Lower_rush_exist">
            <li>
                @php 

                    $date = explode('-', $value_we['dDate']);
                    $lower_date_formate =  $date[0]. "/" . $date[1]. "/" .$date[2];
                    $lower_date_formate_checked =  $date[0]. "_" . $date[1]. "_" .$date[2];
                @endphp
                <div class="form-check">
                    <input data-date="{{$lower_date_formate_checked}}" class="form-check-input lower_radio_click" name="lower_rush" type="radio" value="{{$value_we['iFeesId']}}" id="lower_flexCheckDefault_{{$value_we['iWorkingTime']}}" @checked(isset($LowerdDeliveryRushDate) && $LowerdDeliveryRushDate == $value_we['dDate'])>
                    <label class="form-check-label" id= 'lower_checked_{{$lower_date_formate_checked}}' for="lower_flexCheckDefault_{{$value_we['iWorkingTime']}}"> Days {{$value_we['iWorkingTime']}} = {{$value_we['vFee']}}%</label>
                </div>
                <label  id="lower_rush_dd_{{$value_we['iFeesId']}}">{{$lower_date_formate }}</label>
            </li>
        @endif
    @endforeach
</ul>