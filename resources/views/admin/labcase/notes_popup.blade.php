<style>
    @import url('https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap');
  </style>  
  <table class="tslip driver-inner-model">
  <tbody>
      @if(count($slip_notes)>0)
        @foreach ($slip_notes as $slip_notes_val)
            <tr>
                <td>
                    <table class="w-50 mx-auto my-2 text-center">
                    <th>Slip ID:</th>
                            <td>
                            {{$slip_notes_val->vSlipNumber?$slip_notes_val->vSlipNumber:'N/A'}}
                            </td>
                    <th>Stage:</th>
                            <td>
                                {{$slip_notes_val->vStageName?$slip_notes_val->vStageName:'N/A'}}
                            </td>
                    <th></th>
                            <td class="font-bold">
                            {{$slip_notes_val->dDeliveryDate?date("m/d/Y", strtotime($slip_notes_val->dDeliveryDate)):'N/A'}}
                            </td>
                    </table>
                </td>
            </tr>
            @foreach ($slip_notes_val->stage_note as $key=> $stage_note_val)
                @if($stage_note_val->eNewSlipAdded =='Yes')
                 @if($key != 0)
                    <tr>
                        <td style="color: #0e66b2">
                            {{'The following notes were  created by '.$stage_note_val->vCreatedByName. ' on ' .date('m/d/Y', strtotime($stage_note_val->dtAddedDate)).' @ ' .date('h:i a', strtotime($stage_note_val->tAddedTime))}}
                        </td>
                    </tr>
                  @endif
                  @else
                        <tr>
                            <td style="color: #0e66b2">
                                {{'The following notes were  created by '.$stage_note_val->vCreatedByName. ' on ' .date('m/d/Y', strtotime($stage_note_val->dtAddedDate)).' @ ' .date('h:i a', strtotime($stage_note_val->tAddedTime))}}
                            </td>
                        </tr>
                @endif     
                <tr>
                    <td class="font-bold">
                        {{$stage_note_val->tStagesNotes?$stage_note_val->tStagesNotes:'N/A'}}
                        <hr style="color:#6c6c6c73" class="m-0">
                    </td>
                </tr>
            @endforeach
        @endforeach
      @else
      @endif
  </tbody>
