@extends('layouts.admin.index')
@section('content')

   <!-- Add Ons -->
   {{-- Addons Modal start --}}
 
<div class="modal fade add-Case-model" id="AddonsModal" tabindex="-1" aria-labelledby="AddonsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-l" style="width: 40%;max-width: 40%;">
        <div class="modal-content">
           
            <form method="post" action="{{route('admin.labcase.store_addons')}}" id="frm">
                @csrf
            <div class="modal-header">
                <h3 class="modal-title" id="AddonsModalLabel">Seletct Addons </h3>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <h5> Slip # {{isset($data['vSlipNumber'])?$data['vSlipNumber']:'N/A'}}</h5>
                <a href="{{route('admin.labcase')}}" class="btn-close" aria-label="Close"></a>
            </div>
            <div class="modal-body " id="AddonsModalData">
                <table class="tslip driver-inner-model" style="border-bottom: 1px solid #eff2f5; padding-bottom:20px;">
                    <tbody>
                      <tr>
                        <th>Office: <span>{{$data['vOfficeName']?$data['vOfficeName']:'N/A'}}</span></th>
                        <th>Patient: <span>{{$data['vPatientName']?$data['vPatientName']:'N/A'}}</span></th>
                        <th>Case # <span>{{$data['vCaseNumber']?$data['vCaseNumber']:'N/A'}}</span></th>
                        <th>Pan # <span>{{$data['vCasePanNumber']?$data['vCasePanNumber']:'N/A'}}</span></th>
                        {{-- <th>Slip ID: <span> {{isset($data['vSlipNumber'])?$data['vSlipNumber']:'N/A'}}</span></th> --}}
                      </tr>
                      </tbody>
                </table>

                <div class="row mb-3 mt-6" id="category_addons_div" >
                    <div class="col-lg-5 text-center">
                        <div id="upper_category_addons_div" class="mb-1">
                            @if(isset($upper_product))
                            @foreach($upper_addons as $key => $value)
                                <div class="row" id="upper_addons_list_{{$key}}">
                                    <div class="col-lg-4 pe-0">
                                        <input type="text" name="upper_add_on_cat[]" data-name="{{$value->iAddCategoryId}}" id="upper_add_on_cat_{{$key}}" readonly class="w-100 h-100 qty-input" value="{{$value->vAddonCategoryName}}">
                                        <input type="hidden" name="upper_add_on_cat_id[]"  id="upper_add_on_cat"  class="w-100 h-100 qty-input" value="{{$value->iAddCategoryId}}">
                                    </div>
                                    <div class="col-lg-4 p-0">
                                        <input type="text" name="upper_add_on[]" data-name="{{$value->iSubAddCategoryId}}" id="upper_add_on_{{$key}}" readonly class="w-100 h-100 qty-input" value="{{$value->vAddonName}}">
                                        <input type="hidden" name="upper_add_on_id[]" id="upper_add_on"  class="w-100 h-100 qty-input" value="{{$value->iSubAddCategoryId}}">
                                    </div> 
                                    <div class="col-lg-2 p-0"> 
                                        <input id="upper_add_on_qty_{{$key}}" readonly type="number" placeholder="Qty" name="upper_add_on_qty[]" class="w-100 h-100 qty-input" value="{{$value->iQuantity}}">
                                    </div> 
                                    <div class="col-lg-1 align-self-center p-0"> 
                                        <a href="javascript:;" data-id="{{$key}}" class="btn-icon edit_permission edit-icon upper_add_on_edit" data-id="{{$key}}">  
                                            <i class="fad fa-pencil" style="font-size:12px;"></i> 
                                        </a> 
                                    </div>  
                                    <div class="col-lg-1 align-self-center p-0">  
                                        <a href="javascript:;" data-id="{{$key}}" class="btn-icon delete-icon  delete_upper_add_ons"> 
                                            <i class="fad fa-trash-alt" style="font-size:12px;"></i> 
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        </div>
                        @if(isset($upper_product))
                        <div class="add-one-upper-container" id="upper_addons_model_div" >
                            <a href="javascript:;" id="upper_addons" value="upper_addons" class="btn add-btn me-2 w-100">Select Upper Add Ons</a>
                            
                            <div class="select-add-one-upper-side d-none" id="upper_model">
                
                                <div class="c-modal-header">
                                    <h6 class="m-0">Upper category addons</h6>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5 p-0">
                                        <div class="mb-3">
                                            <select id="upper_category_addons" name="upper_category_addons">
                                                @if(isset($Upper_category_addons) && !empty($Upper_category_addons))
                                                <option value="">Select category addons</option>
                                                @foreach($Upper_category_addons as $key => $value)
                                                <option data-name="{{$value->vCategoryName}}" value="{{$value->iAddCategoryId}}"> {{$value->vCategoryName}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 p-0">
                                        <div class="mb-3">
                                            <select id="upper_subaddons" name="upper_subaddons">
                                                <option value="">Select Addons</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 p-0">
                                        <div class="mb-3">
                                            <input type="number" placeholder="qty" id="iUpperQty" value="" name="iUpperQty" class="w-100 qty-input">
                                        </div>
                                    </div>
                                    <input type="hidden" id="upperAddOnType" value="Add">
                                    <input type="hidden" id="upper_addons_edit" value="">
                
                                </div>
                                <div class="text-danger" style="display: none;" id="upper_category_addons_error_div">select catagory</div>
                                <div class="text-danger" style="display: none;" id="upper_subaddons_error_div">select sub addon</div>
                                <div class="text-danger" style="display: none;" id="upper_upperqty_error_div">enter qty</div>
                                <div class="d-flex justify-content-between">
                                    <a type="submit" id="upper_submit" class="submit-btn me-2 btn add-btn w-100">Add</a>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="col-lg-2 text-center align-self-center">
                        <label> <strong>Add ons</strong></label>
                    </div>
                    <div class="col-lg-5 text-center">
                        <div id="lower_category_addons_div" class="mb-1">
                            @if(isset($lower_product))
                            @foreach($lower_addons as $key => $value)
                                <div class="row" id="lower_addons_list_{{$key}}">
                                    <div class="col-lg-4 pe-0">
                                        <input type="text" name="lower_add_on_cat[]" data-name="{{$value->iAddCategoryId}}" id="lower_add_on_cat_{{$key}}" readonly class="w-100 h-100 qty-input" value="{{$value->vAddonCategoryName}}">
                                        <input type="hidden" name="lower_add_on_cat_id[]"  id="lower_add_on_cat"  class="w-100 h-100 qty-input" value="{{$value->iAddCategoryId}}">
                                    </div>
                                    <div class="col-lg-4 p-0">
                                        <input type="text" name="lower_add_on[]" data-name="{{$value->iSubAddCategoryId}}" id="lower_add_on_{{$key}}" readonly class="w-100 h-100 qty-input" value="{{$value->vAddonName}}">
                                        <input type="hidden" name="lower_add_on_id[]" id="lower_add_on"  class="w-100 h-100 qty-input" value="{{$value->iSubAddCategoryId}}">
                                    </div> 
                                    <div class="col-lg-2 p-0"> 
                                        <input id="lower_add_on_qty_{{$key}}" type="number" readonly placeholder="Qty" name="lower_add_on_qty[]" class="w-100 h-100 qty-input" value="{{$value->iQuantity}}">
                                    </div> 
                                    <div class="col-lg-1 align-self-center p-0"> 
                                        <a href="javascript:;" data-id="{{$key}}" class="btn-icon edit_permission edit-icon lower_add_on_edit" data-id="{{$key}}">  
                                            <i class="fad fa-pencil" style="font-size:12px;"></i> 
                                        </a> 
                                    </div>  
                                    <div class="col-lg-1 align-self-center p-0">  
                                        <a href="javascript:;" data-id="{{$key}}" class="btn-icon delete-icon  delete_lower_add_ons"> 
                                            <i class="fad fa-trash-alt" style="font-size:12px;"></i> 
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        </div>
                        @if(isset($lower_product))
                        <div class="add-one-upper-container" id="lower_category_addons_model_div" >
                            <a href="javascript:;" id="lower_addons" value="lower_addons" class="btn add-btn me-2 w-100">Select Lower Add Ons</a>
                
                            <div class="select-add-one-upper-side d-none" id="lower_model">
                
                                <div class="c-modal-header">
                                    <h6 class="m-0">Lower category addons</h6>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5 p-0">
                                        <div class="mb-3">
                                            <select name="lower_category_addons" id="lower_category_addons">
                                                @if(isset($Lower_category_addons) && !empty($Lower_category_addons))
                                                <option value="">Select category addons</option>
                                                @foreach($Lower_category_addons as $key => $value)
                                                <option value="{{$value->iAddCategoryId}}"> {{$value->vCategoryName}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 p-0">
                
                                        <div class="mb-3">
                                            <select name="lower_subaddons" id="lower_subaddons">
                                                <option value="">Select addons</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 p-0">
                                        <div class="mb-3">
                                            <input type="number" placeholder="qty" id="iLowerQty" value="" class="w-100 qty-input">
                                        </div>
                                    </div>
                                    <input type="hidden" id="lowerAddOnType" value="Add">
                                    <input type="hidden" id="lower_addons_edit" value="">
                
                                </div>
                                <div class="text-danger" style="display: none"  id="lower_category_addons_error_div">select catagory</div>
                                <div class="text-danger" style="display: none" id="lower_subaddons_error_div">select sub addon</div>
                                <div class="text-danger"  style="display: none" id="lower_lowerqty_error_div">enter qty</div>
                                <div class="d-flex justify-content-between">
                                    <a type="submit" id="lower_submit" class="btn submit-btn me-2 add-btn w-100">Add</a>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>  
            <input type="hidden" name="iSlipId" value="{{$iSlipId}}">
            <div class="modal-footer">
                <a href="{{route('admin.labcase')}}" class="btn btn-secondary" >Close</a>
                <a type="submit" id="SubmitAddons" class="btn submit-btn me-2">Submit</a>
            </div>
        </form>
        </div>
        </div>
    </div>
    {{-- Addons Modal start --}}
 
    @endsection
    
    @section('custom-js')
    <script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>
    <script>
    $('#lower_category_addons').selectize();
    $('#upper_category_addons').selectize();
    $('#upper_subaddons').selectize();
    $('#lower_subaddons').selectize();
    $('#upper_status').selectize();
    $('#lower_status').selectize();

    $('document').ready(function(){
       $('#AddonsModal').show();
       $('#AddonsModal').addClass('show');
    });
    $(document).on('click', '#upper_addons', function() {
		iLabId = {{$iLabId}};
        if ($("#upper_model").hasClass("d-none")) {
            $('#upper_model').removeClass("d-none");
            $.ajax({
                url: "{{route('admin.labcase.get_addons_category')}}",
                type: "post",
                data: {
                    iLabId: iLabId,
                    eType: "Upper",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {
                    // console.log(result);
                    $('#upper_category_addons').selectize()[0].selectize.destroy();
                    $('#upper_category_addons').html(result);
                    $('#upper_category_addons').selectize();
                }
            });
        } else {
            $('#upper_model').addClass("d-none");
        }
    });

    $(document).on('click', '#lower_addons', function() {
        if ($("#lower_model").hasClass("d-none")) {
            $('#lower_model').removeClass("d-none");
            iLabId = {{$iLabId}};
            $.ajax({
                url: "{{route('admin.labcase.get_addons_category')}}",
                type: "post",
                data: {
                    eType: "Lower",
                    iLabId: iLabId,
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {
                    $('#lower_category_addons').selectize()[0].selectize.destroy();
                    $('#lower_category_addons').html(result);
                    $('#lower_category_addons').selectize();
                }
            });
        } else {
            $('#lower_model').addClass("d-none");
        }
    });

    $(document).on('click', '.upper_add_on_edit', function() {
        id = $(this).data("id");
        cat = $("#upper_add_on_cat_" + id).data('name');
        add_ons = $("#upper_add_on_" + id).data('name');
        qty = $("#upper_add_on_qty_" + id).val();

        $("#upper_addons_edit").val(id);
        $('#upper_category_addons').data('selectize').setValue(cat,true);
        $("#iUpperQty").val(qty);
        $("#upperAddOnType").val("Edit");

        $.ajax({
            url: "{{route('admin.labcase.getcategoryaddons')}}",
            type: "post",
            data: {
                iAddCategoryId: cat,
                eType: "Upper",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#upper_subaddons').selectize()[0].selectize.destroy();
                $('#upper_subaddons').html(result);
                $('#upper_subaddons').selectize();
                $('#upper_subaddons').data('selectize').setValue(add_ons,true);
                $('#upper_model').removeClass("d-none");
            }
        });

    });

    $(document).on('click', '.lower_add_on_edit', function() {
        id = $(this).data("id");
        cat = $("#lower_add_on_cat_" + id).data('name');
        add_ons = $("#lower_add_on_" + id).data('name');
        qty = $("#lower_add_on_qty_" + id).val();

        $("#lower_addons_edit").val(id);
        $('#lower_category_addons').data('selectize').setValue(cat,true);
        $("#iLowerQty").val(qty);
        $("#lowerAddOnType").val("Edit");

        $.ajax({
            url: "{{route('admin.labcase.getcategoryaddons')}}",
            type: "post",
            data: {
                iAddCategoryId: cat,
                eType: "Lower",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#lower_subaddons').selectize()[0].selectize.destroy();
                $('#lower_subaddons').html(result);
                $('#lower_subaddons').selectize();
                $('#lower_subaddons').data('selectize').setValue(add_ons,true);
                $('#lower_model').removeClass("d-none");
            }
        });
    });

    @if(isset($upper_addons))
        var upperaddons = parseInt("{{count($upper_addons)}}");
    @else
        var upperaddons = 0;
    @endif
    
    $(document).on('click', '#upper_submit', function() {

        upper_category_addons = $("#upper_category_addons").val();
        upper_subaddons = $("#upper_subaddons").val();
        iUpperQty = $("#iUpperQty").val();

        if (upper_category_addons.length == 0) {
            $("#upper_category_addons_error_div").show();
            return false;
        } else {
            $("#upper_category_addons_error_div").hide();
        }

        if (upper_subaddons.length == 0) {
            $("#upper_subaddons_error_div").show();
            return false;
        } else {
            $("#upper_subaddons_error_div").hide();
        }

        if (iUpperQty <= 0) {
            $("#upper_upperqty_error_div").show();
            return false;
        } else {
            $("#upper_upperqty_error_div").hide();
        }

        // if (upper_category_addons.length == 0 || upper_subaddons.length == 0 || iUpperQty.length == 0) {
        //     $("#upper_category_addons_error_div").show();
        //     return false;
        // }else{
        //     $("#upper_category_addons_error_div").hide();
        // }

        upper_addon_category_name = $("#upper_category_addons").text();
        upper_subaddons_name = $("#upper_subaddons").text();


        // alert($("#upperAddOnType").val());
        if ($("#upperAddOnType").val() == "Add") {

            $("#upper_category_addons_div").append('<div class="row" id="upper_addons_list_' + upperaddons + '"><div class="col-lg-4 pe-0"><input type="text" name="upper_add_on_cat[]" data-name="' + upper_category_addons + '" id="upper_add_on_cat_' + upperaddons + '" readonly class="w-100 h-100 qty-input" value="' + upper_addon_category_name + '"><input type="hidden" name="upper_add_on_cat_id[]"  id="upper_add_on_cat"  class="w-100 h-100 qty-input" value="' + upper_category_addons + '"></div><div class="col-lg-4 p-0"><input type="text" name="upper_add_on[]" data-name="' + upper_subaddons + '" id="upper_add_on_' + upperaddons + '" readonly class="w-100 h-100 qty-input" value="' + upper_subaddons_name + '"><input type="hidden" name="upper_add_on_id[]" id="upper_add_on"  class="w-100 h-100 qty-input" value="' + upper_subaddons + '"></div> <div class="col-lg-2 p-0"> <input id="upper_add_on_qty_' + upperaddons + '" type="number" placeholder="Qty" name="upper_add_on_qty[]" class="w-100 h-100 qty-input" value="' + iUpperQty + '"> </div> <div class="col-lg-1 align-self-center p-0"> <a href="javascript:;" data-id="' + upperaddons + '" class="btn-icon edit_permission edit-icon upper_add_on_edit" data-id="' + upperaddons + '">  <i class="fad fa-pencil" style="font-size:12px;"></i> </a> </div>  <div class="col-lg-1 align-self-center p-0">  <a href="javascript:;" data-id="' + upperaddons + '" class="btn-icon delete-icon  delete_upper_add_ons"> <i class="fad fa-trash-alt" style="font-size:12px;"></i> </a>  </div> </div>');
            upperaddons++;
        } else {

            upper_addons_edit = $("#upper_addons_edit").val();

            $("#upper_addons_list_" + upper_addons_edit).html('<div class="col-lg-4 pe-0"><input type="text" name="upper_add_on_cat[]" data-name="' + upper_category_addons + '" id="upper_add_on_cat_' + upper_addons_edit + '" readonly class="w-100 h-100 qty-input" value="' + upper_addon_category_name + '"></div><div class="col-lg-4 p-0"><input type="text" name="upper_add_on[]" data-name="' + upper_subaddons + '" id="upper_add_on_' + upper_addons_edit + '" readonly class="w-100 h-100 qty-input" value="' + upper_subaddons_name + '"></div> <div class="col-lg-2 p-0"> <input id="upper_add_on_qty_' + upper_addons_edit + '" type="number" placeholder="Qty" name="upper_add_on_qty[]" class="w-100 h-100 qty-input" value="' + iUpperQty + '"> </div> <div class="col-lg-1 align-self-center p-0"> <a href="javascript:;" data-id="' + upper_addons_edit + '" class="btn-icon edit_permission edit-icon upper_add_on_edit" data-id="' + upper_addons_edit + '">  <i class="fad fa-pencil" style="font-size:12px;"></i> </a> </div>  <div class="col-lg-1 align-self-center p-0">  <a href="javascript:;" data-id="' + upper_addons_edit + '" class="btn-icon delete-icon  delete_upper_add_ons"> <i class="fad fa-trash-alt" style="font-size:12px;"></i> </a>  </div>');
        }

        $("#upper_category_addons").val("");
        $('#upper_subaddons').selectize()[0].selectize.destroy();
        $('#upper_subaddons').html("");
        $('#upper_subaddons').selectize();
        $("#iUpperQty").val("");
        $('#upper_model').addClass("d-none");
        $("#upperAddOnType").val("Add");
        $("#upper_addons_edit").val("");
    });


    $(document).on('click', '#lower_submit', function() {
    lower_category_addons = $("#lower_category_addons").val();
    lower_subaddons = $("#lower_subaddons").val();
    iLowerQty = $("#iLowerQty").val();

    if (lower_category_addons.length == 0) {
        $("#lower_category_addons_error_div").show();
        return false;
    } else {
        $("#lower_category_addons_error_div").hide();
    }

    if (lower_subaddons.length == 0) {
        $("#lower_subaddons_error_div").show();
        return false;
    } else {
        $("#lower_subaddons_error_div").hide();
    }
    if (iLowerQty <= 0) {
        $("#lower_lowerqty_error_div").show();
        return false;

    } else {
        $("#lower_lowerqty_error_div").hide();
    }


    $("#category_addons_div").show();

    lower_addon_category_name = $("#lower_category_addons").text();
    lower_subaddons_name = $("#lower_subaddons").text();

    @if(isset($lower_addons))
        var loweraddons = parseInt("{{count($lower_addons)}}");
    @else
        var loweraddons = 0;
    @endif

    if ($("#lowerAddOnType").val() == "Add") {

        $("#lower_category_addons_div").append('<div class="row" id="lower_addons_list_' + loweraddons + '"><div class="col-lg-4 pe-0"><input type="text" name="lower_add_on_cat[]" data-name="' + lower_category_addons + '" id="lower_add_on_cat_' + loweraddons + '" readonly class="w-100 h-100 qty-input" value="' + lower_addon_category_name + '">   <input type="hidden" name="lower_add_on_cat_id[]"  id="lower_add_on_cat"  class="w-100 h-100 qty-input" value="' + lower_category_addons + '"></div><div class="col-lg-4 p-0"><input type="text" name="lower_add_on[]" data-name="' + lower_subaddons + '" id="lower_add_on_' + loweraddons + '" readonly class="w-100 h-100 qty-input" value="' + lower_subaddons_name + '"> <input type="hidden" name="lower_add_on_id[]"  id="lower_add_on_cat"  class="w-100 h-100 qty-input" value="' + lower_subaddons + '"></div> <div class="col-lg-2 p-0"> <input id="lower_add_on_qty_' + loweraddons + '" type="number" placeholder="Qty" name="lower_add_on_qty[]" class="w-100 h-100 qty-input" value="' + iLowerQty + '"> </div> <div class="col-lg-1 align-self-center p-0"> <a href="javascript:;" data-id="' + loweraddons + '" class="btn-icon edit_permission edit-icon lower_add_on_edit" data-id="' + loweraddons + '">  <i class="fad fa-pencil" style="font-size:12px;"></i> </a> </div>  <div class="col-lg-1 align-self-center p-0">  <a href="javascript:;" data-id="' + loweraddons + '" class="btn-icon delete-icon  delete_lower_add_ons"> <i class="fad fa-trash-alt" style="font-size:12px;"></i> </a>  </div> </div>');
        loweraddons++;
    } else {

        lower_addons_edit = $("#lower_addons_edit").val();

        $("#lower_addons_list_" + lower_addons_edit).html('<div class="col-lg-4 pe-0"><input type="text" name="lower_add_on_cat[]" data-name="' + lower_category_addons + '" id="lower_add_on_cat_' + lower_addons_edit + '" readonly class="w-100 h-100 qty-input" value="' + lower_addon_category_name + '"></div><div class="col-lg-4 p-0"><input type="text" name="lower_add_on[]" data-name="' + lower_subaddons + '" id="lower_add_on_' + lower_addons_edit + '" readonly class="w-100 h-100 qty-input" value="' + lower_subaddons_name + '"></div> <div class="col-lg-2 p-0"> <input id="lower_add_on_qty_' + lower_addons_edit + '" type="number" placeholder="Qty" name="lower_add_on_qty[]" class="w-100 h-100 qty-input" value="' + iLowerQty + '"> </div> <div class="col-lg-1 align-self-center p-0"> <a href="javascript:;" data-id="' + lower_addons_edit + '" class="btn-icon edit_permission edit-icon lower_add_on_edit" data-id="' + lower_addons_edit + '">  <i class="fad fa-pencil" style="font-size:12px;"></i> </a> </div>  <div class="col-lg-1 align-self-center p-0">  <a href="javascript:;" data-id="' + lower_addons_edit + '" class="btn-icon delete-icon  delete_lower_add_ons"> <i class="fad fa-trash-alt" style="font-size:12px;"></i> </a>  </div>');

    }

    $('#lower_subaddons').selectize()[0].selectize.destroy();
    $('#lower_subaddons').html("");
    $('#lower_subaddons').selectize();
    $("#lowerAddOnType").val("Add");
    $("#lower_addons_edit").val("");
    $("#lower_category_addons").val("");
    $("#ilowerQty").val("");
    $('#lower_model').addClass("d-none");


    });

    $(document).on('change', '#upper_category_addons', function() {
        iAddCategoryId = $("#upper_category_addons").val();
        $.ajax({
            url: "{{route('admin.labcase.getcategoryaddons')}}",
            type: "post",
            data: {
                iAddCategoryId: iAddCategoryId,
                eType: "Upper",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#upper_subaddons').selectize()[0].selectize.destroy();
                $('#upper_subaddons').html(result);
                $('#upper_subaddons').selectize();
            }
        });
    });
    $(document).on('change', '#lower_category_addons', function() {
        var iAddCategoryId = $("#lower_category_addons").val();
        $.ajax({
            url: "{{route('admin.labcase.getcategoryaddons')}}",
            type: "post",
            data: {
                iAddCategoryId: iAddCategoryId,
                eType: "Lower",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#lower_subaddons').selectize()[0].selectize.destroy();
                $('#lower_subaddons').html(result);
                $('#lower_subaddons').selectize();
            }
        });
    });
    $(document).on('click', '.delete_upper_add_ons', function() {
        id = $(this).data("id");
        $("#upper_addons_list_" + id).remove();
    });
    $(document).on('click', '.delete_lower_add_ons', function() {
        id = $(this).data("id");
        $("#lower_addons_list_" + id).remove();
    });

    $(document).on('click', '#SubmitAddons', function() {
        error = false;   
        setTimeout(function() {
            if (error == true) {
                return false;
            } else {
                $("#frm").submit();
                return true;
            }
        }, 1000);
    });
</script>
<!-- Add Ons END -->
@endsection