<style>
  @import url('https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap');

</style>
@if(count($show_history)>0)
<table class="tslip driver-inner-model new-table" style="border-bottom: 1px solid #eff2f5;">
  <tbody>
    <tr>
      <th>Office: <span class="normal-font">{{$show_history[0]->vOfficeName?$show_history[0]->vOfficeName:'N/A'}}</span></th>      
      <th>Patient: <span class="normal-font">{{$show_history[0]->vPatientName?$show_history[0]->vPatientName:'N/A'}}</span></th>      
      <th>Case # <span class="normal-font">{{isset($data['vCaseNumber'])?$data['vCaseNumber']:'N/A'}}</span></th>      
      <th>Pan # <span class="normal-font">{{$show_history[0]->vCasePanNumber?$show_history[0]->vCasePanNumber:'N/A'}}</span></th>      
    </tr>
  </tbody>
</table>

@foreach($show_history as $key=> $show_history_val)
@if($key == 1)
<br>
@endif



<table class="tslip driver-inner-model mb-5 new-table" style="table-layout: fixed">
  <tbody>
    <tr>
      <th class="text-end pe-5" colspan="3">Slip ID: <span class="normal-font">{{$show_history_val->vSlipNumber?$show_history_val->vSlipNumber:'N/A'}}</span></th>      
      <td></td>
      <th class="text-start" colspan="3">Stage: <span class="normal-font">{{$show_history_val->vStageName?$show_history_val->vStageName:'N/A'}}</span></th>
    </tr>
    @php
    $driver_history = $show_history_val->driver_history;
    @endphp
    @foreach ($driver_history as $driver_history_val)
    <tr>
      <td class="font-bold">
        {{$driver_history_val->vLocation?$driver_history_val->vLocation:'N/A'}}
      </td>
      <td class="text-end">
        {{$driver_history_val->dtAddedDate?date("m/d/Y", strtotime($driver_history_val->dtAddedDate)):'N/A'}}
      </td>
      <td class="text-center">
        {{$driver_history_val->dtAddedDate?date("h:i:s A", strtotime($driver_history_val->dtAddedDate)):'N/A'}}
      </td>
      <td class="font-bold text-center"> {{$driver_history_val->eUserType?$driver_history_val->eUserType:'N/A'}}</td>
      <td class="text-center">{{$driver_history_val->vLoginUserName?$driver_history_val->vLoginUserName:'N/A'}} </td>
      <td class="font-bold signature-font text-center">Signature</td>
      <td class="text-center">{{$driver_history_val->vSignature?$driver_history_val->vSignature:$driver_history_val->vLoginUserName}}</td>

    </tr>
    @endforeach
  </tbody>
</table>
@endforeach
@else
<table class="tslip driver-inner-model" style="border-bottom: 1px solid #eff2f5;">
  <tbody>
    <tr>
      <th>Office: <span class="normal-font">{{$data['vOfficeName']?$data['vOfficeName']:'N/A'}}</span></th>      
      <th>Patient: <span class="normal-font">{{$data['vPatientName']?$data['vPatientName']:'N/A'}}</span></th>      
      <th>Case # <span class="normal-font">{{$data['vCaseNumber']?$data['vCaseNumber']:'N/A'}}</span></th>      
      <th>Pan # <span class="normal-font">{{$data['vCasePanNumber']?$data['vCasePanNumber']:'N/A'}}</span></th>      
    </tr>
  </tbody>
</table>
<table class="tslip driver-inner-model mb-5">
  <tbody>
    <tr>
      <td colspan="8">
        <table class="w-50 mx-auto my-2 text-center">
          <tr>
            <th>Slip ID: <span class="normal-font">{{$slip_number?$slip_number:'N/A'}}</span></th>            
            <th>Stage: <span class="normal-font">N/A</span></th>            
          </tr>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<h4 style="text-align: center">No Record Found</h4>
@endif

@if(isset($HistoryType) && $HistoryType =='ShowAll')
<div class="row" id="signatureFocus">
  <div class="col-lg-8 mx-auto text-center my-2">
    <input type="hidden" name="iSlipId" value="{{$data['iSlipId']?$data['iSlipId']:null}}">
    <input type="hidden" name="CurrentStatus" value="{{isset($CurrentStatus) && $CurrentStatus?$CurrentStatus:null}}">
    <input type="text" name="vSignature" placeholder="Signature" class="signature-input signature-font" required>
  </div>
</div>
@endif