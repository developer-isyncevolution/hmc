@if (!empty($Grade))    
    @if (count($Grade) > 1)
        <option value="">Select Grade</option>
    @endif
    @foreach($Grade as $key => $Grade_val)
        <option value="{{$Grade_val->iGradeId}}"> {{$Grade_val->vQualityName}}</option>
    @endforeach
@else
    <option value="">No Grade Found</option>
@endif
