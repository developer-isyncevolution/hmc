@extends('layouts.admin.index')
@section('content')
<div class="main-panel">
  <div class="card">
    <div class="col-lg-12 mx-auto">
        <div class="card-header">
          <h4 class="card-title" id="row-separator-basic-form">{{isset($emails->iEmailId) ? 'Edit' : 'Add'}} email</h4>
        </div>
        <form action="{{route('admin.email.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($emails)) {{$emails->iEmailId}} @endif">
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Email code</label>
                <input type="text" class="form-control" id="vEmailCode" name="vEmailCode" placeholder="Email code" value="@if(old('vEmailCode')!=''){{old('vEmailCode')}}@elseif(isset($emails->vEmailCode)){{$emails->vEmailCode}}@else{{old('vEmailCode')}}@endif">
                <div class="text-danger" style="display: none;" id="vEmailCode_error">Enter email code</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Email title</label>
                <input type="text" class="form-control" id="vTitle" name="vTitle" placeholder="Email title" value="@if(old('vTitle')!=''){{old('vTitle')}}@elseif(isset($emails->vTitle)){{$emails->vTitle}}@else{{old('vTitle')}}@endif">
                <div class="text-danger" style="display: none;" id="vTitle_error">Enter email title</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">From name</label>
                <input type="text" class="form-control" id="vFromName" name="vFromName" placeholder="From name" value="@if(old('vFromName')!=''){{old('vFromName')}}@elseif(isset($emails->vFromName)){{$emails->vFromName}}@else{{old('vFromName')}}@endif">
                <div class="text-danger" style="display: none;" id="vFromName_error">Enter email from name</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">From email</label>
                <input type="text" class="form-control" id="vFromEmail" name="vFromEmail" placeholder="From email" value="@if(old('vFromEmail')!=''){{old('vFromEmail')}}@elseif(isset($emails->vFromEmail)){{$emails->vFromEmail}}@else{{old('vFromEmail')}}@endif">
                <div class="text-danger" style="display: none;" id="vFromEmail_error">Enter from email</div>
            </div> 
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Cc email</label>
                <input type="text" class="form-control" id="vCcEmail" name="vCcEmail" placeholder="Cc email" value="@if(old('vCcEmail')!=''){{old('vCcEmail')}}@elseif(isset($emails->vCcEmail)){{$emails->vCcEmail}}@else{{old('vCcEmail')}}@endif">
                <div class="text-danger" style="display: none;" id="vCcEmail_error">Enter cc email</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Bcc email</label>
                <input type="text" class="form-control" id="vBccEmail" name="vBccEmail" placeholder="Bcc email" value="@if(old('vBccEmail')!=''){{old('vBccEmail')}}@elseif(isset($emails->vBccEmail)){{$emails->vBccEmail}}@else{{old('vBccEmail')}}@endif">
                <div class="text-danger" style="display: none;" id="vBccEmail_error">Enter bcc email</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Email subject</label>
                <input type="text" class="form-control" id="vSubject" name="vSubject" placeholder="Email subject" value="@if(old('vSubject')!=''){{old('vSubject')}}@elseif(isset($emails->vSubject)){{$emails->vSubject}}@else{{old('vSubject')}}@endif">
                <div class="text-danger" style="display: none;" id="vSubject_error">Enter subject</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Status</label>
                <select id="eStatus" name="eStatus" class="form-control">
                    <option value="Active" @if(isset($emails)) @if($emails->eStatus == 'Active') selected @endif @endif>Active</option>
                    <option value="Inactive" @if(isset($emails)) @if($emails->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
                </select>
                <div class="mt-1">
                    @error('eStatus')
                        <code>{{ $message }}</code>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <label class="required fs-5 fw-bold mb-2">Message</label>
                <div id="toolbar-container"></div>
                <textarea class="form-control" id="tMessage" name="tMessage" placeholder="Email Message">@if(old('tMessage')!=''){{old('tMessage')}}@elseif(isset($emails->tMessage)){{$emails->tMessage}}@else{{old('tMessage')}}@endif</textarea>
                <div class="text-danger" style="display: none;" id="tMessage_error">Enter message</div>
            </div>            
            <div class="col-12 align-self-end d-inline-block text-center">
              <a type="submit" id="submit" class="btn btn-primary btn-lg">Submit</a>
              <a href="{{route('admin.email')}}" class="btn btn-info btn-lg">Back</a>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')  
    <script>
        tinymce.init({
        selector: 'textarea',
        height: 500,
        menubar: true,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help wordcount'
        ],
        toolbar: 'insert | undo redo | formatselect fontselect fontsizeselect | bold italic underline backcolor forecolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat | blockquote | help | code',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        theme_advanced_fonts : "Andale Mono=andale mono,times;"+
            "Arial=arial,helvetica,sans-serif;"+
            "Arial Black=arial black,avant garde;"+
            "Book Antiqua=book antiqua,palatino;"+
            "Comic Sans MS=comic sans ms,sans-serif;"+
            "Courier New=courier new,courier;"+
            "Georgia=georgia,palatino;"+
            "Helvetica=helvetica;"+
            "Impact=impact,chicago;"+
            "Symbol=symbol;"+
            "Tahoma=tahoma,arial,helvetica,sans-serif;"+
            "Terminal=terminal,monaco;"+
            "Times New Roman=times new roman,times;"+
            "Trebuchet MS=trebuchet ms,geneva;"+
            "Verdana=verdana,geneva;"+
            "Webdings=webdings;"+
            "Wingdings=wingdings,zapf dingbats",
        fontsize_formats: '11px 12px 14px 16px 18px 24px 36px 48px',
        textcolor_map: [
            "000000", "Black",
            "993300", "Burnt orange",
            "333300", "Dark olive",
            "003300", "Dark green",
            "003366", "Dark azure",
            "000080", "Navy Blue",
            "333399", "Indigo",
            "333333", "Very dark gray",
            "800000", "Maroon",
            "FF6600", "Orange",
            "808000", "Olive",
            "008000", "Green",
            "008080", "Teal",
            "0000FF", "Blue",
            "666699", "Grayish blue",
            "808080", "Gray",
            "FF0000", "Red",
            "FF9900", "Amber",
            "99CC00", "Yellow green",
            "339966", "Sea green",
            "33CCCC", "Turquoise",
            "3366FF", "Royal blue",
            "800080", "Purple",
            "999999", "Medium gray",
            "FF00FF", "Magenta",
            "FFCC00", "Gold",
            "FFFF00", "Yellow",
            "00FF00", "Lime",
            "00FFFF", "Aqua",
            "00CCFF", "Sky blue",
            "993366", "Red violet",
            "FFFFFF", "White",
            "FF99CC", "Pink",
            "FFCC99", "Peach",
            "FFFF99", "Light yellow",
            "CCFFCC", "Pale green",
            "CCFFFF", "Pale cyan",
            "99CCFF", "Light sky blue",
            "CC99FF", "Plum"
          ],
          file_picker_callback: function(callback, value, meta) 
            {
            if (meta.filetype == 'image') {
            $('#upload').trigger('click');
            $('#upload').on('change', function() {
            var file = this.files[0];
            var reader = new FileReader();
            reader.onload = function(e) {
            callback(e.target.result, {
            alt: ''
            });
            };
            reader.readAsDataURL(file);
            });
            }
            },
    });
$(document).on('click','#submit',function()
  {
    iCategoryId         = $("#id").val();
    vEmailCode          = $("#vEmailCode").val();
    vTitle              = $("#vTitle").val();
    vFromName           = $("#vFromName").val();
    vFromEmail          = $("#vFromEmail").val();
    vSubject            = $("#vSubject").val();
    tMessage            = tinyMCE.get('tMessage').getContent();

    var error = false;

    if(vEmailCode.length == 0){
      $("#vEmailCode_error").show();
      error = true;
    } else {
      $("#vEmailCode_error").hide();
    }
    if(vTitle.length == 0){
      $("#vTitle_error").show();
      error = true;
    } else {
      $("#vTitle_error").hide();
    }
    if(vFromName.length == 0){
      $("#vFromName_error").show();
      error = true;
    } else {
      $("#vFromName_error").hide();
    }
    if(vSubject.length == 0){
      $("#vSubject_error").show();
      error = true;
    } else {
      $("#vSubject_error").hide();
    }

    if(tMessage.length == 0){
      $("#tMessage_error").show();
      error = true;
    } else {
      $("#tMessage_error").hide();
    }
    if(vFromEmail.length == 0){
        $("#vFromEmail_error").show();
        error = true;
    } else {
      $("#vFromEmail_error").hide();
    } 
    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
    </script>
@endsection