@extends('layouts.admin.index')
@section('content')

<div class="main-panel">

<div class="page-title text-center">
        <h3>
        Changes password
        </h3>
    </div>


  <div class="card">
    <div class="col-lg-12 mx-auto">
   
      <div class="card-header">
        <h3 class="card-title align-items-start flex-column">
         
        </h3>
        <ul class="d-flex justify-content-between my-2">
          <li class="d-flex">
            
          </li>
        </ul>




        <ul class="c-nav-tab">
                <li>
                <a class="btn add-btn active  mx-2" href="{{route('admin.office.ChangePassword',$offices->iOfficeId)}}" aria-controls="#tab-{{ $offices->iOfficeId }}"> Changes password</a>
                </li>
                <li>
                <a href="{{route('admin.office')}}" aria-controls="#tab-{{ $offices->iOfficeId }}" class="btn add-btn  mx-2"> Office</a>
                </li>
               
              
            </ul>
      </div>
    
      <form action="{{route('admin.office.changePassword')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($offices)) {{$offices->iOfficeId}} @endif">
        <input type="hidden" name="iCustomerId" value="@if(isset($offices)) {{$offices->iCustomerId}} @endif">
        <div class="col-xl-6 col-lg-12 col-md-6">
          <label>New password</label>
          <input type="Password" class="form-control" id="vPassword" name="vPassword" placeholder="Password" value="">
          <div class="text-danger" style="display: none;" id="vPassword_error">Please enter password</div>
        </div>
        <div class="col-xl-6 col-lg-12 col-md-6">
          <label>Confirm new password</label>
          <input type="Password" class="form-control" id="vPassword2" name="vPassword2" placeholder="Confirm password" value="">
          <div class="text-danger" style="display: none;" id="vPassword2_error">Please enter confirm password</div>
          <div class="text-danger" id="vPassword2_same_error" style="display: none;">Password should match</div>
        </div>
        <div class="col-4 align-self-end d-inline-block mx-auto text-center">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.office')}}" class="btn back-btn">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script>
  $(document).on('click', '#submit', function() {
    vPassword = $("#vPassword").val();
    vPassword2 = $("#vPassword2").val();

    var error = false;
    if (vPassword.length == 0) {
      $("#vPassword_error").show();
      error = true;
    } else {
      $("#vPassword_error").hide();
    }

    if (vPassword2.length == 0) {
      $("#vPassword2_error").show();
      error = true;
    } else {
      $("#vPassword2_error").hide();
    }

    if (vPassword.length != 0 && vPassword2.length != 0) {
      if (vPassword != vPassword2) {
        $("#vPassword2_same_error").show();
        return true;
      } else {
        $("#vPassword2_same_error").hide();
      }
    } else {
      $("#vPassword2_same_error").hide();
    }
    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
</script>
@endsection