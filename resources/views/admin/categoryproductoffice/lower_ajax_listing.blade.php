@if(count($data) > 0)
@foreach($data as $key => $value)
<tr>
	<td>
		@if(isset($value->vImage))
			<img alt="{{$value->vImage}}" style="height: 30px;" src="{{asset('uploads/categoryproduct/'.$value->vImage)}}">
		@else 
		   <img style="width: 30px;" id="img" value="" src="{{asset('images/no-image.gif')}}">
		@endif

	</td>
	<td>
		{{$value->vCode}}
	</td>
	<td>{{ $value->vName }}</td>
	<td>{{ isset($value->vOfficePrice)?$value->vOfficePrice:$value->vPrice }}</td>
	

</tr>
@endforeach
<tr>
	{{-- <td  class="text-center border-right-0">
		<a href="javascript:;" title="Multiple delete" id="delete_btn" class="btn-icon  delete_permission delete-icon">
		<i class="fad fa-trash-alt"></i>
		</a>
	</td> --}}
	{{-- <td class="border-0"></td>
	<td colspan="4" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
	<td class="border-0" colspan="2">
		<div class="d-flex">
			{!! $paging !!}
		</div>
	</td>
	<td colspan="2"  class="text-end border-left-0">
		{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}} --}}
		{{-- {{ dd($data) }} --}}
	<select class="w-100px show-drop" id="upper_page_limit">
		<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 10)
			selected
			@endif value="10">10</option>
		<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 20)
			selected
			@endif value="20">20</option>
		<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 50)
			selected
			@endif value="50">50</option>
		<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 100)
			selected
			@endif value="100">100</option>
	</select>
	</td>
</tr>
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif