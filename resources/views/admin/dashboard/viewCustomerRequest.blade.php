@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
    <div class="row">
        <div class="col-sm-4">
            <h3>Customer Registration Request</h3>
            <table class="table">
                <tr>
                    <td>
                        <strong style="display: inline-flex;width: 130px;">Customer Type :</strong> <span>{{$data->vCustomerType}}</span>
                    </td>
                    <td>
                        <strong style="display: inline-flex;width: 130px;">{{$data->vCustomerType}} Name : </strong> <span>{{$data->vOfficeName}}</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong style="display: inline-flex;width: 130px;">Street Address :</strong> <span>{{$data->vAddress}}</span>
                    </td>
                    <td>
                        <strong style="display: inline-flex;width: 130px;">City : </strong> <span>{{$data->vCity}}</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong style="display: inline-flex;width: 130px;">State :</strong> <span>{{$data->vState}}</span>
                    </td>
                    <td>
                        <strong style="display: inline-flex;width: 130px;">Zip Code : </strong> <span>{{$data->vZipCode}}</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong style="display: inline-flex;width: 130px;">Contact Name :</strong> 
                        <span>
                            @if(isset($data->vFirstName) && !empty($data->vFirstName) && isset($data->vLastName) && !empty($data->vLastName))
                                {{$data->vFirstName.' '.$data->vLastName}}
                            @else
                                {{$data->vName}}
                            @endif
                        </span>
                    </td>
                    <td>
                        <strong style="display: inline-flex;width: 130px;">Title : </strong> <span>{{$data->vTitle}}</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <strong style="display: inline-flex;width: 130px;">Email :</strong> <span>{{$data->vEmail}}</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong style="display: inline-flex;width: 130px;">Phone :</strong> <span>{{$data->vMobile}}</span>
                    </td>
                    <td>
                        <strong style="display: inline-flex;width: 130px;">Cellulor :</strong> <span>{{$data->vCellulor}}</span>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-12 align-self-end d-inline-block">
          <a href="{{route('admin.approveCustomer',$data->iCustomerId)}}" class="btn approve-btn me-2">Approve</a>
          <a href="{{route('admin.rejectCustomer',$data->iCustomerId)}}" id="submit" class="btn reject-btn me-2">Reject</a>
          <a href="{{route('admin.dashboard')}}" class="btn back-btn">Back</a>
        </div>
    </div>


</div>
@endsection