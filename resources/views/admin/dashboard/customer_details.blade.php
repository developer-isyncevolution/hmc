<div class="table-data table-responsive">
  <table class="table">
     <thead>
        <tr>
           <th class="min-w-100px">
              <span class="text-muted fw-bold text-muted d-block fs-7">Customer Name</span>
           </th>
           <th class="min-w-100px">
              <span class="text-muted fw-bold text-muted d-block fs-7">Website</span>
           </th>
           <th class="min-w-100px">
              <span class="text-muted fw-bold text-muted d-block fs-7">Email</span>
           </th>
           <th class="min-w-100px">
              <span class="text-muted fw-bold text-muted d-block fs-7">Mobile</span>
           </th>
           <th class="min-w-100px">
              <span class="text-muted fw-bold text-muted d-block fs-7">Cellulor</span>
           </th>
           <th class="min-w-100px">
              <span class="text-muted fw-bold text-muted d-block fs-7">Office Name</span>
           </th>
           <th class="min-w-100px">
              <span class="text-muted fw-bold text-muted d-block fs-7">Address</span>
           </th>
         
        </tr>
     </thead>
     <tbody >
        <tr>
          @if(isset($data) && !empty($data))
              <td>
                  @if(isset($data->vFirstName) && !empty($data->vFirstName) && isset($data->vLastName) && !empty($data->vLastName))
                     {{$data->vFirstName.' '.$data->vLastName}}
                  @else
                     {{$data->vName}}
                  @endif
              </td>
              <td>{{$data->vWebsiteName}}</td>
              <td>{{$data->vEmail}}</td>
              <td>{{$data->vMobile}}</td>
              <td>{{$data->vCellulor}}</td>
              <td>{{$data->vOfficeName}}</td>
              <td>{{$data->vAddress}}</td>
          @else
          <h4>No Record Found</h4>    
          @endif
        </tr>
       
     </tbody>
  </table>
</div>