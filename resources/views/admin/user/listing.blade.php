@extends('layouts.admin.index')
@section('content')
<!-- BEGIN: Content-->
<!-- DOM - jQuery events table -->
<div class="main-panel">
    <div class="page-title text-center">
        <h3>
            User
        </h3>
    </div>
    <div class="col-lg-12 mx-auto">
        <div class="listing-page">
            <div class="card mb-5 mb-xl-4">            
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-mid">
                                <ul class="d-flex list-unstyled">
                                    @if(\App\Libraries\General::check_permission('','eCreate') == true)
                                    <li>
                                        <a href="{{route('admin.user.create')}}" class="btn add-btn create_permission">
                                            <i class="fa fa-plus"></i> Add
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    @if(\App\Libraries\General::admin_info()['customerType']=='Super Admin')
                    <div class="col-lg-6">
                        <select name="iCustomerId" id="iCustomerId" >
                            <option value="">Select customer</option>
                            @foreach($customer as $key => $customers)
                                <option value="{{$customers->iCustomerId}}" @if(isset($iCustomerId)){{$customers->iCustomerId == $iCustomerId  ? 'selected' : ''}} @endif>{{$customers->vOfficeName}}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    {{-- <div class="col-lg-3">
                        <select name="iOfficeId" id="iOfficeId">
                            <option value="">Select office</option>
                            @foreach($office as $key => $offices)
                            <option value="{{$offices->iOfficeId}}" @if(isset($users)){{$offices->iOfficeId == $users->iOfficeId  ? 'selected' : ''}} @endif>{{$offices->vOfficeName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <select name="iDoctorId" id="iDoctorId" >
                            <option value="">Select doctor</option>
                            @foreach($doctor as $key => $doctors)
                            <option value="{{$doctors->iDoctorId}}" @if(isset($users)){{$doctors->iDoctorId == $users->iDoctorId  ? 'selected' : ''}} @endif>{{$doctors->vFirstName." ".$doctors->vLastName }}</option>
                            @endforeach
                        </select>
                    </div> --}}
                    <div class="col-lg-6">
                        <div class="custome-serce-box">
                            <input type="text" class="form-control" id="keyword" name="search" placeholder="Search">
                            <span class="search-btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($iDoctorId))
                <input type="hidden" name="doctor" id="doctor" value="{{$iDoctorId}}">
            @endisset
            @if(isset($iCustomerId))
            <input type="hidden" name="customer" id="customer" value="{{$iCustomerId}}">
            @endisset
            @if(isset($iOfficeId))
            <input type="hidden" name="office" id="office" value="{{$iOfficeId}}">
            @endisset
                <!-- <div class="col-xl-6 col-lg-12 col-md-6">
                    <label>doctor</label>
                    <select name="iDoctorId" id="iDoctorId" class="form-control">
                        @foreach($doctor as $key => $doctors)
                            <option value="{{$doctors->iDoctorId}}" @if(isset($users)){{$doctors->iDoctorId == $users->iDoctorId  ? 'selected' : ''}} @endif>{{$doctors->vFirstName }}</option>
                        @endforeach
                    </select>
                </div> -->
            <div class="table-data table-responsive">
                <table class="table">
                    <thead>
                        <tr class="fw-bolder text-muted">
                            <th class="w-25px" data-orderable="false">
                                <div class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" id="selectall" type="checkbox" name="selectall">
                                    <label for="selectall">&nbsp;</label>
                                </div>
                            </th>
                            {{-- <th class="min-w-140px">
                                <a id="vName" class="sort" data-column="vName" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Doctor Name</span>
                                </a>
                            </th> --}}
                            <th class="min-w-140px">
                                <a id="vFirstName" class="sort" data-column="vFirstName" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">First Name</span>
                                </a>
                            </th>
                            <th class="min-w-140px">
                                <a id="vLastName" class="sort" data-column="vLastName" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Last Name</span>
                                </a>
                            </th>
                            <th class="min-w-120px">
                                <a id="vUserName" class="sort" data-column="vUserName" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">User Name</span>
                                </a>
                            </th>
                            <th class="min-w-120px">
                                <a id="eStatus" class="sort" data-column="eStatus" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Status</span>
                                </a>
                            </th>
                            <th class="min-w-100px">
                                <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="table_record">
                    </tbody>
                </table>
                <div class="text-center loaderimg">
                    <div class="loaderinner">
                        <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- DOM - jQuery events table -->
<!-- END: Content-->
@endsection

@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
 $('#iCustomerId').selectize();
 $('#iDoctorId').selectize();
 $('#iOfficeId').selectize();
    $(document).ready(function() {
        var iDoctorId = $("#doctor").val();
        var iCustomerId = $("#customer").val();
        var iOfficeId   = $("#office").val();
            $.ajax({
                url: "{{route('admin.user.ajaxListing')}}",
                type: "get",
                data: {
                    iDoctorId:iDoctorId,iCustomerId:iCustomerId,iOfficeId:iOfficeId,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });

    $("#selectall").click(function() {
        if (this.checked) {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', true);
            });
        } else {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', false);
            });
        }
    });


    $("#keyword").keyup(function() {
        var keyword = $("#keyword").val();
        var iDoctorId = $("#iDoctorId").val();
        var iCustomerId = $("#iCustomerId").val();
        var iOfficeId = $("#iOfficeId").val();
        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.user.ajaxListing')}}",
                type: "get",
                data: {
                    keyword: keyword,iDoctorId:iDoctorId,iCustomerId:iCustomerId,iOfficeId:iOfficeId,
                    action: 'search'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });

    $(document).on('click', '#delete_btn', function() {
        swal({
                title: "Are you sure delete this user ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
        var id = [];

        $("input[name='User_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");
        var iDoctorId = $("#doctor").val();
        var iCustomerId = $("#iCustomerId").val();
        var iOfficeId = $("#iOfficeId").val();

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                title: "Are you sure delete all user ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "{{route('admin.user.ajaxListing')}}",
                        type: "get",
                        data: {
                            id: id,iDoctorId:iDoctorId,iCustomerId:iCustomerId,iOfficeId:iOfficeId,
                            action: 'multiple_delete'
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                            notification_error("User Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                        }
                    });
                }
            });
        }
    });
    $(document).on('click', '#delete', function() {
        swal({
                title: "Are you sure delete this user ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");
                    var iDoctorId = $("#doctor").val();
                    var iCustomerId = $("#iCustomerId").val();
                    $("#ajax-loader").show();

                        $.ajax({
                            url: "{{route('admin.user.ajaxListing')}}",
                            type: "get",
                            data: {
                                id: id,iDoctorId:iDoctorId,iCustomerId:iCustomerId,
                                action: 'delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("User Deleted Successfully");
                                setTimeout(function() {
                                   location.reload();
                                }, 1000);
                            }
                        });
                }
            })
    });
    $(document).on('click', '.sort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');
        var iDoctorId = $("#doctor").val();
        var iCustomerId = $("#iCustomerId").val();
        var iOfficeId = $("#iOfficeId").val();

        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.user.ajaxListing')}}",
                type: "get",
                data: {
                    column: column,
                    iDoctorId:iDoctorId,iCustomerId:iCustomerId,iOfficeId:iOfficeId,
                    order,
                    order,
                    action: 'sort'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });

    $(document).on('click', '.ajax_page', function() {
        pages = $(this).data("pages");
        var iDoctorId = $("#doctor").val();
        var iCustomerId = $("#iCustomerId").val();
        var iOfficeId = $("#iOfficeId").val();

        $("#table_record").html('');
        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.user.ajaxListing')}}",
                type: "get",
                data: {
                    pages: pages,iDoctorId:iDoctorId,iCustomerId:iCustomerId,iOfficeId:iOfficeId,
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });
    $(document).on('change', '#iDoctorId', function() {
        var iDoctorId = $("#iDoctorId").val();
        var iCustomerId = $("#iCustomerId").val();
        var iOfficeId = $("#iOfficeId").val();

        $("#table_record").html('');
        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.user.ajaxListing')}}",
                type: "get",
                data: {
                    iDoctorId:iDoctorId,iCustomerId:iCustomerId,iOfficeId:iOfficeId,
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });
    $(document).on('change', '#iCustomerId', function() {
        var iDoctorId = $("#iDoctorId").val();
        var iCustomerId = $("#iCustomerId").val();
        var iOfficeId = $("#iOfficeId").val();

        $("#table_record").html('');
        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.user.ajaxListing')}}",
                type: "get",
                data: {
                    iCustomerId:iCustomerId,iOfficeId:iOfficeId,iDoctorId:iDoctorId,
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });
    $(document).on('change', '#iOfficeId', function() {
        var iOfficeId = $("#iOfficeId").val();
        var iCustomerId = $("#iCustomerId").val();
        $("#table_record").html('');
        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.user.ajaxListing')}}",
                type: "get",
                data: {
                    iOfficeId:iOfficeId,iCustomerId:iCustomerId,
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });
    $(document).on('change','#page_limit',function()
    {
        limit_page = this.value;
        $("#table_record").html('');
        $("#ajax-loader").show();
        url = "{{route('admin.user.ajaxListing')}}";

        setTimeout(function(){
            $.ajax({
                url: url,
                type: "get",
                data:  {limit_page: limit_page}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            // hideLoader();
            }, 500);
    });
</script>
@endsection