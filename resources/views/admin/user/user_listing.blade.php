@extends('layouts.admin.index')
@section('content')
<!-- BEGIN: Content-->
<!-- DOM - jQuery events table -->
<div class="main-panel">
    
    <div class="col-lg-12 mx-auto">
        <div class="listing-page">
            <div class="card mb-5 mb-xl-4">
                <div class="card-header">
                    <ul class="c-nav-tab">
                      <li class="">
                        <a href="{{route('admin.customer.edit',$iCustomerId)}}" class=" add-btn btn mx-2">
                          Customer
                        </a>
                      </li>  
                      <li class="">
                        <a href="{{route('admin.office.officeedit',[$iCustomerId,$iOfficeId])}}" class="add-btn btn mx-2">
                          Office
                        </a>
                      </li>
                      <li class="">
                        <a href="{{route('admin.doctor.doctoredit',[$iCustomerId,$iOfficeId,$iDoctorId])}}" class="add-btn btn mx-2">
                          Doctor
                        </a>
                      </li>
                      <li class="">
                        <a href="" class="active add-btn btn mx-2">
                          User
                        </a>
                      </li>
                      
                    </ul>
                    <ul class="d-flex justify-content-center my-2 list-unstyled flex-wrap w-100">
                        @if(\App\Libraries\General::check_permission('','eCreate') == true)
                        <li class="d-flex">
                            <a href="{{route('admin.user.user-create',['iCustomerId' => $iCustomerId,'iOfficeId' =>$iOfficeId,'iDoctorId' =>$iDoctorId])}}" class="btn add-btn create_permission mx-2">
                                <i class="fa fa-plus"></i> Add
                            </a>
                        </li>
                        @endif
                    </ul>
                  </div>
            </div>

            @if(isset($iDoctorId))
                <input type="hidden" name="doctor" id="doctor" value="{{$iDoctorId}}">
            @endisset
                <!-- <div class="col-xl-6 col-lg-12 col-md-6">
                    <label>doctor</label>
                    <select name="iDoctorId" id="iDoctorId" class="form-control">
                        @foreach($doctor as $key => $doctors)
                            <option value="{{$doctors->iDoctorId}}" @if(isset($users)){{$doctors->iDoctorId == $users->iDoctorId  ? 'selected' : ''}} @endif>{{$doctors->vFirstName}}</option>
                        @endforeach
                    </select>
                </div> -->
            <div class="table-data table-responsive">
                <table class="table">
                    <thead>
                        <tr class="fw-bolder text-muted">
                            <th class="w-25px" data-orderable="false">
                                <div class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" id="selectall" type="checkbox" name="selectall">
                                    <label for="selectall">&nbsp;</label>
                                </div>
                            </th>
                            <th class="min-w-140px">
                                <a id="vFirstName" class="sort" data-column="vFirstName" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">First Name</span>
                                </a>
                            </th>
                            <th class="min-w-140px">
                                <a id="vLastName" class="sort" data-column="vLastName" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Last Name</span>
                                </a>
                            </th>
                            <th class="min-w-120px">
                                <a id="vUserName" class="sort" data-column="vUserName" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">User Name</span>
                                </a>
                            </th>
                            <th class="min-w-120px">
                                <a id="eStatus" class="sort" data-column="eStatus" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Status</span>
                                </a>
                            </th>
                            <th class="min-w-100px">
                                <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="table_record">
                    </tbody>
                </table>
                <div class="text-center loaderimg">
                    <div class="loaderinner">
                        <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- DOM - jQuery events table -->
<!-- END: Content-->
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script>
    $('#iDoctorId').select2({
        width: '100%',
        placeholder: "Select customer",
        allowClear: true
    });
    $(document).ready(function() 
    {
        var iDoctorId = $("#doctor").val();
        $.ajax({
            url: "{{route('admin.user.userajaxListing')}}",
            type: "get",
            data: {
                iDoctorId:iDoctorId,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $("#selectall").click(function() {
        if (this.checked) {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', true);
            });
        } else {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', false);
            });
        }
    });


    $("#keyword").keyup(function() 
    {
        var keyword = $("#keyword").val();
        var iDoctorId = $("#doctor").val();

        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.user.userajaxListing')}}",
            type: "get",
            data: {
                keyword: keyword,iDoctorId:iDoctorId,
                action: 'search'
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '#delete_btn', function() {
        swal({
                title: "Are you sure delete this user.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
        var id = [];

        $("input[name='User_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");
        var iDoctorId = $("#doctor").val();

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                title: "Are you sure delete all user.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "{{route('admin.user.userajaxListing')}}",
                        type: "get",
                        data: {
                            id: id,iDoctorId:iDoctorId,
                            action: 'multiple_delete'
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                        }
                    });
                }
            });
        }
    });
    $(document).on('click', '#delete', function() {
        swal({
                title: "Are you sure delete this user.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");
                    var iDoctorId = $("#doctor").val();
                    $("#ajax-loader").show();

                        $.ajax({
                            url: "{{route('admin.user.userajaxListing')}}",
                            type: "get",
                            data: {
                                id: id,iDoctorId:iDoctorId,
                                action: 'delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("User Deleted Successfully");
                                setTimeout(function() {
                                   location.reload();
                                }, 1000);
                            }
                        });
                }
            })
    });
    $(document).on('click', '.sort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');
        var iDoctorId = $("#doctor").val();

        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.user.userajaxListing')}}",
                type: "get",
                data: {
                    column: column,
                    iDoctorId:iDoctorId,
                    order,
                    order,
                    action: 'sort'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });

    $(document).on('click', '.ajax_page', function() {
        pages = $(this).data("pages");
        var iDoctorId = $("#doctor").val();
        $("#table_record").html('');
        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.user.userajaxListing')}}",
                type: "get",
                data: {
                    pages: pages,iDoctorId:iDoctorId,
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });
    $(document).on('change', '#iDoctorId', function() {
        var iDoctorId = $("#iDoctorId").val();

        $("#table_record").html('');
        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.user.userajaxListing')}}",
                type: "get",
                data: {
                    iDoctorId:iDoctorId
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });
    $(document).on('change','#page_limit',function()
    {
        limit_page = this.value;
        $("#table_record").html('');
        $("#ajax-loader").show();
        url = "{{route('admin.user.userajaxListing')}}";

        setTimeout(function(){
            $.ajax({
                url: url,
                type: "get",
                data:  {limit_page: limit_page}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            hideLoader();
            }, 500);
    });
</script>
@endsection