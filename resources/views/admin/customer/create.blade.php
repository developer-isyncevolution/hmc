@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
<div class="page-title text-center">
        <h3>
        Add customer 
        </h3> 
    </div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
      <div class="card-header">
        @if(isset($customers->iCustomerId))
        {{-- <ul class="c-nav-tab"> --}}
          {{-- <li class="">
            <a href="" class="active add-btn btn mx-2">
              Customer
            </a>
          </li>   --}}
          {{-- <li class="">
            <a href="{{route('admin.officelist',$customers->iCustomerId)}}" class="add-btn btn mx-2">
              Office
            </a>
          </li> --}}
          {{-- <li>
            <a href="{{route('admin.userlist',$customers->iCustomerId)}}" class="btn add-btn mx-2">
              User
            </a>
          </li> --}}
        {{-- </ul> --}}
        @else
          {{-- <h4 class="card-title" id="row-separator-basic-form">Add customer</h4> --}}
        @endif
      </div>
      <form action="{{route('admin.customer.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($customers)) {{$customers->iCustomerId}} @endif">
        <div class="col-xl-12 col-lg-12 col-md-12">
          <label>Office</label>
          <input type="text" class="form-control" id="OfficeName" name="OfficeName" value="{{$id}}" readonly>
        </div>
        {{-- <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Profile Picture</label>
          <input class="form-control" type="file" id="vImage" name="vImage" accept="image/*">
          @if(isset($customers))
             <img style="width: 100px;" id="img" value="@if(old('vImage') == 'vImage') selected @endif" src="{{asset('uploads/customer/'.$customers->vImage)}}">
          @else 
             <img style="width: 100px;" id="img" value="" src="{{asset('images/no-image.gif')}}">
          @endif
         <div class="text-danger" style="display: none;" id="vImage_error">select logo</div>
        </div> --}}
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Customer type</label>
            <select name="iCustomerTypeId" id="iCustomerTypeId">
              @foreach($customertype as $key => $customertypes)
              <option value="{{$customertypes->iCustomerTypeId}}" @if(isset($customers)){{$customertypes->iCustomerTypeId == $customers->iCustomerTypeId  ? 'selected' : ''}} @endif>{{$customertypes->vTypeName}}</option>
              @endforeach
            </select>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Status</label>
          <select id="eStatus" name="eStatus">
            <option value="Active" @if(isset($customers)) @if($customers->eStatus == 'Active') selected @endif @endif>Active</option>
            <option value="Inactive" @if(isset($customers)) @if($customers->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
            <option value="Stand By" @if(isset($customers)) @if($customers->eStatus == 'Stand By') selected @endif @endif>Stand By</option>
            <option value="Suspended" @if(isset($customers)) @if($customers->eStatus == 'Suspended') selected @endif @endif>Suspended</option>
          </select>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Lab/Office name</label>
          <input type="text" class="form-control" id="vOfficeName" name="vOfficeName" placeholder="Lab/Office name" value="@if(old('vOfficeName')!=''){{old('vOfficeName')}}@elseif(isset($customers->vOfficeName)){{$customers->vOfficeName}}@else{{old('vOfficeName')}}@endif">
          <div class="text-danger" style="display: none;" id="vOfficeName_error">Please enter lab/office name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Office code</label>
          <input type="text" class="form-control" id="vOfficeCode" name="vOfficeCode" placeholder="Office code" value="@if(old('vOfficeCode')!=''){{old('vOfficeCode')}}@elseif(isset($customers->vOfficeCode)){{$customers->vOfficeCode}}@else{{old('vOfficeCode')}}@endif">
          <div class="text-danger" style="display: none;" id="vOfficeCode_error">Please enter office code</div>
          <div class="text-danger" style="display: none;" id="vOfficeCodeMax_error">Please enter max 5 latter</div>
          
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Address</label>
          <input type="text" class="form-control" id="vAddress" name="vAddress" placeholder="Address" value="@if(old('vAddress')!=''){{old('vAddress')}}@elseif(isset($customers->vAddress)){{$customers->vAddress}}@else{{old('vAddress')}}@endif">
          <div class="text-danger" style="display: none;" id="vAddress_error">Please enter </div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>City</label>
          <input type="text" class="form-control" id="vCity" name="vCity" placeholder="City" value="@if(old('vCity')!=''){{old('vCity')}}@elseif(isset($customers->vCity)){{$customers->vCity}}@else{{old('vCity')}}@endif">
          <div class="text-danger" style="display: none;" id="vCity_error">Please enter city</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>State</label>
            <select name="iStateId" id="iStateId" >
              @foreach($states as $key => $states)
              <option value="{{$states->iStateId}}" @if(isset($customers)){{$states->iStateId == $customers->iStateId  ? 'selected' : ''}} @endif>{{$states->vState}}</option>
              @endforeach
            </select>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Zip code</label>
          <input type="number" class="form-control" id="vZipCode" name="vZipCode" placeholder="zipcode" value="@if(old('vZipCode')!=''){{old('vZipCode')}}@elseif(isset($customers->vZipCode)){{$customers->vZipCode}}@else{{old('vZipCode')}}@endif">
          <div class="text-danger" style="display: none;" id="vZipCode_error">Please enter zipcode</div>
        </div>
        {{-- <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Contact name</label>
          <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($customers->vName)){{$customers->vName}}@else{{old('vName')}}@endif">
          <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
        </div> --}}
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>First name</label>
          <input type="text" class="form-control" id="vFirstName" name="vFirstName" placeholder="First name" value="@if(old('vFirstName')!=''){{old('vFirstName')}}@elseif(isset($customers->vFirstName)){{$customers->vFirstName}}@else{{old('vFirstName')}}@endif">
          <div class="text-danger" style="display: none;" id="vFirstName_error">Please enter first name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Last name</label>
          <input type="text" class="form-control" id="vLastName" name="vLastName" placeholder="Last name" value="@if(old('vLastName')!=''){{old('vLastName')}}@elseif(isset($customers->vLastName)){{$customers->vLastName}}@else{{old('vLastName')}}@endif">
          <div class="text-danger" style="display: none;" id="vLastName_error">Please enter last name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Title</label>
          <input type="text" class="form-control" id="vTitle" name="vTitle" placeholder="Title" value="@if(old('vTitle')!=''){{old('vTitle')}}@elseif(isset($customers->vTitle)){{$customers->vTitle}}@else{{old('vTitle')}}@endif">
          <div class="text-danger" style="display: none;" id="vTitle_error">Please enter title</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Email</label>
          <input type="text" class="form-control" id="vEmail" name="vEmail" placeholder="Email" value="@if(old('vEmail')!=''){{old('vEmail')}}@elseif(isset($customers->vEmail)){{$customers->vEmail}}@else{{old('vEmail')}}@endif">
          <div class="text-danger" style="display: none;" id="vEmail_error">Please enter email</div>
          <div class="text-danger" style="display: none;" id="vEmail_valid_error">Please enter valid email</div>
        </div>
        
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Website name</label>
          <input type="text" class="form-control" id="vWebsiteName" name="vWebsiteName" placeholder="website name" value="@if(old('vWebsiteName')!=''){{old('vWebsiteName')}}@elseif(isset($customers->vWebsiteName)){{$customers->vWebsiteName}}@else{{old('vWebsiteName')}}@endif">
          <div class="text-danger" style="display: none;" id="vWebsiteName_error">Please enter website name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Mobile</label>
          <input type="text" class="form-control" id="vMobile" name="vMobile" placeholder="Mobile" value="@if(old('vMobile')!=''){{old('vMobile')}}@elseif(isset($customers->vMobile)){{$customers->vMobile}}@else{{old('vMobile')}}@endif">
          <div class="text-danger" style="display: none;" id="vMobile_error">Please enter mobile</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Cell</label>
          <input type="text" class="form-control" id="vCellulor" name="vCellulor" placeholder="Cell" value="@if(old('vCellulor')!=''){{old('vCellulor')}}@elseif(isset($customers->vCellulor)){{$customers->vCellulor}}@else{{old('vCellulor')}}@endif">
          <div class="text-danger" style="display: none;" id="vCellulor_error">Please enter cell</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Notification Email</label>
          <input type="text" class="form-control" id="vNotificationEmail" name="vNotificationEmail" placeholder="Notification Email" value="@if(old('vNotificationEmail')!=''){{old('vNotificationEmail')}}@elseif(isset($customers->vNotificationEmail)){{$customers->vNotificationEmail}}@else{{old('vNotificationEmail')}}@endif">
          <div class="text-danger" style="display: none;" id="vNotificationEmail_error">Please enter cell</div>
        </div>
        <div class="col-xl-6 col-lg-12 col-md-12">
          <label>Note</label>
          <div id="toolbar-container"></div>
            <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Note">@if(old('tDescription')!=''){{old('tDescription')}}@elseif(isset($customers->tDescription)){{$customers->tDescription}}@else{{old('tDescription')}}@endif</textarea>
          <div id="tDescription_error" class="text-danger" style="display: none;">Please enter note </div>
        </div>

        <div class="col-xl-12 col-lg-12 col-md-12">
          <h4>Email settings</h4>
        </div>

        <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Email Host</label>
            <input type="text" class="form-control" id="vHost" name="vHost" placeholder="Email Host" value="@if(isset($customers->vHost)){{$customers->vHost}}@endif">
            <div class="text-danger" style="display: none;" id="vHost_error">Please enter host</div>
        </div>

        <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Email Username</label>
            <input type="text" class="form-control" id="vEmailUserName" name="vEmailUserName" placeholder="Email Username" value="@if(isset($customers->vEmailUserName)){{$customers->vEmailUserName}}@endif">
            <div class="text-danger" style="display: none;" id="vEmailUserName_error">Please enter email username</div>
        </div>

        <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Email Password</label>
            <input type="text" class="form-control" id="vEmailPassword" name="vEmailPassword" placeholder="Email Password" value="@if(isset($customers->vEmailPassword)){{$customers->vEmailPassword}}@endif">
            <div class="text-danger" style="display: none;" id="vEmailPassword_error">Please enter email Password</div>
        </div>

        <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Email Port</label>
            <input type="text" class="form-control" id="vEmailPort" name="vEmailPort" placeholder="Email Port" value="@if(isset($customers->vEmailPort)){{$customers->vEmailPort}}@endif">
            <div class="text-danger" style="display: none;" id="vEmailPort_error">Please enter email port</div>
        </div>

        <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Email Encryption</label>
            <input type="text" class="form-control" id="vEmailEncryption" name="vEmailEncryption" placeholder="Email Encryption" value="@if(isset($customers->vEmailEncryption)){{$customers->vEmailEncryption}}@endif">
            <div class="text-danger" style="display: none;" id="vEmailEncryption_error">Please enter email encryption</div>
        </div>

        <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>From Email Address</label>
            <input type="text" class="form-control" id="vFromEmailAddress" name="vFromEmailAddress" placeholder="From Email Address" value="@if(isset($customers->vFromEmailAddress)){{$customers->vFromEmailAddress}}@endif">
            <div class="text-danger" style="display: none;" id="vFromEmailAddress_error">Please enter from email</div>
        </div>
        
        <div class="col-12 align-self-end d-inline-block">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.customer')}}" class="btn back-btn">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('custom-js')
{{-- {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}} --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
   <script>
 $('#eStatus').selectize();
 $('#iCustomerTypeId').selectize();
 $('#iStateId').selectize();
  // $('#eStatus').select2({
  //       width: '100%',
  //       placeholder: "Select customer",
  //       allowClear: true
  //   });
  // $('#iCustomerTypeId').select2({
  //       width: '100%',
  //       placeholder: "Select customer",
  //       allowClear: true
  //   });
  $(document).on('change', '#vImage', function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#img').attr('src', e.target.result);
      };
      reader.readAsDataURL(this.files[0]);
    }
  });
  $(document).on('click', '#submit', function() {
    id          = $("#id").val();
    vOfficeName = $("#vOfficeName").val();
    vOfficeCode = $("#vOfficeCode").val();
    vImage      = $("#vImage").val();
    vAddress    = $("#vAddress").val();
    vCity       = $("#vCity").val();
    vZipCode    = $("#vZipCode").val();
    vTitle      = $("#vTitle").val();
    vWebsiteName= $("#vWebsiteName").val();
    // vName       = $("#vName").val();
    vEmail      = $("#vEmail").val();
    vMobile     = $("#vMobile").val();
    vCellulor   = $("#vCellulor").val();
    tDescription= $("#tDescription").val();
    vFirstName    = $("#vFirstName").val();
    vLastName     = $("#vLastName").val();
    
    var error = false;

    if (vOfficeName.length == 0) {
      $("#vOfficeName_error").show();
      error = true;
    } else {
      $("#vOfficeName_error").hide();
    }
    
    if(vOfficeCode.length == 0){
        $("#vOfficeCode_error").show();
        $("#vOfficeCodeMax_error").hide();
        error = true;
      } else{
        if(vOfficeCode.length > 5){
          $("#vOfficeCodeMax_error").show();
          $("#vOfficeCode_error").hide();
          error = true;
        }else{
          $("#vOfficeCodeMax_error").hide();
          $("#vOfficeCode_error").hide();
        }
      }
    if (vAddress.length == 0) {
      $("#vAddress_error").show();
      error = true;
    } else {
      $("#vAddress_error").hide();
    }
    if (vCity.length == 0) {
      $("#vCity_error").show();
      error = true;
    } else {
      $("#vCity_error").hide();
    }
    if (vZipCode.length == 0) {
      $("#vZipCode_error").show();
      error = true;
    } else {
      $("#vZipCode_error").hide();
    }
    if (vTitle.length == 0) {
      $("#vTitle_error").show();
      error = true;
    } else {
      $("#vTitle_error").hide();
    }
    if (vFirstName.length == 0) {
      $("#vFirstName_error").show();
      error = true;
    } else {
      $("#vFirstName_error").hide();
    }
    if (vLastName.length == 0) {
      $("#vLastName_error").show();
      error = true;
    } else {
      $("#vLastName_error").hide();
    }
    // if (vName.length == 0) {
    //   $("#vName_error").show();
    //   error = true;
    // } else {
    //   $("#vName_error").hide();
    // }
         // Email validtion start
         if (vEmail.length == 0) {
      $("#vEmail_error").text("Please enter email");
      $("#vEmail_error").show();
      $("#vEmail_valid_error").hide();
        error = true;
    }else{
      if(validateEmail(vEmail))
      {
        $("#vEmail_valid_error").hide();
        @if(!isset($customers))
          $.ajax({
            url: "{{route('admin.staff.CheckExistEmail')}}",
            type: "post",
            data: {
                vEmail : $('#vEmail').val(),
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
              if(response == 1)
              {
                $('#vEmail_error').show();
                $("#vEmail_error").text("This email alredy exist");
                error = true;
                return false;
              }
              else
              {
                $('#vEmail_error').hide();
                $('#vEmail_valid_error').hide();
                return true;
              }
            }
          }); 
        @endif  
      }else{
        $("#vEmail_valid_error").show();
        $("#vEmail_error").hide();
        error = true;
      }
    }
// Email validtion end
 
    
    
    
    

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
function validateEmail(vEmail) 
{
  var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  if (filter.test(vEmail)) {
    return true;
  }
  else {
    return false;
  }
}
$(document).ready(function() {
    var mobile = [{ "mask": "### ### ####"}, { "mask": "### ### ####"}];
    $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
   })
  $(document).ready(function() {
  var cellulor = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-####"}];
  $('#vCellulor').inputmask({ 
      mask: cellulor, 
      greedy: false, 
      definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
  })
</script>
@endsection