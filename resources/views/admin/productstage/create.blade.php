
@extends('layouts.admin.index')

@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<div class="main-panel">
   <div class="page-title text-center">
      <h3>
         {{isset($productstage->iProductStageId) ? 'Edit' : 'Add'}} Stage
      </h3>
   </div>
   <div class="card">
      <div class="col-lg-12 mx-auto">
         <div class="card mb-5 mb-xl-4">
            <div class="card-header border-0 p-0">
               <ul class="c-nav-tab">
                  <li class="">
                     <a href="{{route('admin.category')}}" class=" add-btn btn mx-2">
                     Categories
                     </a>
                  </li>
                  <li class="">
                     <a href="{{route('admin.categoryproduct')}}" class=" btn add-btn  mx-2">
                     Products
                     </a>
                  </li>
                  <li>
                     <a href="{{route('admin.productstage')}}" class="active btn add-btn mx-2">
                     Stages
                     </a>
                  </li>
                  <li>
                     <a href="{{route('admin.addoncategory')}}" class="btn add-btn mx-2">
                     Categories Add ons
                     </a>
                  </li>
                  <li>
                     <a href="{{route('admin.subaddoncategory')}}" class="btn add-btn mx-2">
                     Add ons
                     </a>
                  </li>
               </ul>
            </div>
         </div>
         <form action="{{route('admin.productstage.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($productstage)) {{$productstage->iProductStageId}} @endif">
            <input type="hidden" id="iCategoryId_edit" value="@if(isset($productstage)){{$productstage->iCategoryId}}@endif">
            <input type="hidden" id="iCategoryProductId_edit" value="@if(isset($productstage)){{$productstage->iCategoryProductId}}@endif">
            {{-- 
            <div class="col-xxl-4 col-lg-6 col-md-12">
               <label>Product Type</label>
               <input type="radio"  class="color_radio" id="Upper" name="eType" value="Upper" @if(isset($productstage)) @if($productstage->eType == 'Upper') checked @endif @endif>
               <label for="Upper">Upper</label><br>
               <input type="radio"  class="color_radio" id="Lower" name="eType" value="Lower" @if(isset($productstage)) @if($productstage->eType == 'Lower') checked @endif @endif>
               <label for="Lower">Lower</label><br>
               <input type="radio"  class="color_radio" id="Both" name="eType" value="Both" @if(isset($productstage)) @if($productstage->eType == 'Both') checked @endif @endif>
               <label for="Both">Both</label><br>
            </div>
            --}}
            <div class="col-xxl-4 col-lg-6 col-md-12 align-self-center">
               <label class="d-block mb-2">Stage Type</label>
               <input type="radio" class="color_radio d-inline-block" id="Upper" name="eType" value="Upper" @if(isset($productstage)) @if($productstage->eType == 'Upper') checked @endif @endif>
               <label for="Upper" class="me-2">Upper</label>
               <input type="radio" class="color_radio d-inline-block" id="Lower" name="eType" value="Lower" @if(isset($productstage)) @if($productstage->eType == 'Lower') checked @endif @endif>
               <label for="Lower" class="me-2">Lower</label>
               <input type="radio" class="color_radio d-inline-block" id="Both" name="eType" value="Both" @if(isset($productstage)) @if($productstage->eType == 'Both') checked @endif @endif>
               <label for="Both" class="me-2">Both</label>
               <div class="text-danger" style="display: none;" id="eType_error">Please select product type</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
               <label>Category</label>
               <select name="iCategoryId" id="iCategoryId">
                  <option value="none">Select First Product Type</option>
                  @if(isset($productstage->iCategoryId))
                  @foreach($category as $key => $categorys)
                  <option value="{{$categorys->iCategoryId}}" @if(isset($productstage)){{$productstage->iCategoryId == $categorys->iCategoryId  ? 'selected' : ''}} @endif>{{$categorys->vName}}</option>
                  @endforeach
                  @endif
               </select>
               <div class="text-danger" style="display: none;" id="iCategoryId_error">Please select category</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
               <label>Select product </label>
               <select name="iCategoryProductId" id="iCategoryProductId">
                  <option value="none">Select Product </option>
               </select>
               <div class="text-danger" style="display: none;" id="iCategoryProductId_error">Please select product</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
               <label>Code</label>
               <input type="text" class="form-control" id="vCode" name="vCode" placeholder="Code" value="@if(old('vCode')!=''){{old('vCode')}}@elseif(isset($productstage->vCode)){{$productstage->vCode}}@else{{old('vCode')}}@endif">
               <div class="text-danger" style="display: none;" id="vCode_error">Please enter code</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
               <label>Name</label>
               <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($productstage->vName)){{$productstage->vName}}@else{{old('vName')}}@endif">
               <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
               <label>Day To Pick Up</label>
               <input type="number" class="form-control" id="iPickUpDay" name="iPickUpDay" placeholder="pick up day" value="@if(old('iPickUpDay')!=''){{old('iPickUpDay')}}@elseif(isset($productstage->iPickUpDay)){{$productstage->iPickUpDay}}@else{{old('iPickUpDay')}}@endif">
               <div class="text-danger" style="display: none;" id="iPickUpDay_error">Please enter pick up day</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
               <label>Days To Process</label>
               <input type="number" class="form-control" id="iProcessDay" name="iProcessDay" placeholder="Process day" value="@if(old('iProcessDay')!=''){{old('iProcessDay')}}@elseif(isset($productstage->iProcessDay)){{$productstage->iProcessDay}}@else{{old('iProcessDay')}}@endif">
               <div class="text-danger" style="display: none;" id="iProcessDay_error">Please enter process day</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
               <label>Days To Deliver</label>
               <input type="number" class="form-control" id="iDeliverDay" name="iDeliverDay" placeholder="Deliver day" value="@if(old('iDeliverDay')!=''){{old('iDeliverDay')}}@elseif(isset($productstage->iDeliverDay)){{$productstage->iDeliverDay}}@else{{old('iDeliverDay')}}@endif">
               <div class="text-danger" style="display: none;" id="iDeliverDay_error">Please enter deliver day</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12 ">
               <div id="Grade_price_response">
               </div>
               @if(isset($grade_data) && count($grade_data)>0)
               <div class=" GradePrices">
                  <input type="hidden" id="total_grades" value="{{count($grade_data);}}">
                  <label>Grades - Price</label>
                  <div id="toolbar-container"></div>
                  @foreach ($grade_data as $key=> $grade_data_val)
                  <div class="row g-3 align-items-center">
                     <div class="col-3">
                        <span>@if(isset($grade_data_val->vQualityName)){{$grade_data_val->vQualityName}}  @endif</span>
                     </div>
                     <div class="col-auto">
                        <input type="number" class="form-control StagePriceChange" data-id ="{{$key}}" id="vStagePrice{{$key}}" name="vStagePrice[]" placeholder="Price" value="@if(isset($grade_data_val->vPrice)){{$grade_data_val->vPrice}}@endif">
                        <input type="hidden" class="form-control"  name="iGradeId[]"  value="@if(isset($grade_data_val->iGradeId)){{$grade_data_val->iGradeId}}@endif">
                        <div class="text-danger" style="display: none;" id="vStagePrice_error{{$key}}">Select grade price</div>
                     </div>
                  </div>
                  @endforeach
               </div>
               @else
             
               @endif
               <div class="MainPrice" >
                <label>Price </label>
                <div id="toolbar-container"></div>
                <input type="number" class="form-control" id="vPrice" name="vPrice" placeholder="Price" value="@if(old('vPrice')!=''){{old('vPrice')}}@elseif(isset($productstage->vPrice)){{$productstage->vPrice}}@else{{old('vPrice')}}@endif">
                <div id="vPrice_error" class="text-danger" style="display: none;">Please enter price </div>
               </div>
           
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
               <label>Status</label>
               <select id="eStatus" name="eStatus">
               <option value="Active" @if(isset($productstage)) @if($productstage->eStatus == 'Active') selected @endif @endif>Active</option>
               <option value="Inactive" @if(isset($productstage)) @if($productstage->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
               </select>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
               <label>Sequence</label>
               <div id="toolbar-container"></div>
               <input type="number" class="form-control" id="iSequence" name="iSequence" placeholder="Sequence" value="@if(old('iSequence')!=''){{old('iSequence')}}@elseif(isset($productstage->iSequence)){{$productstage->iSequence}}@else{{old('iSequence')}}@endif">
               <div id="iSequence_error" class="text-danger" style="display: none;">Please enter Sequence </div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
               <div class="col-xxl-4 col-lg-6 col-md-12 align-self-center">
                  <label class="d-block mb-2">Releasing Stage</label>
                  <input type="radio" class=" d-inline-block" id="Yes" name="eReleasing" value="Yes" @if(isset($productstage)) @if($productstage->eReleasing == 'Yes') checked @endif @endif>
                  <label for="Yes" class="me-2">Yes</label>
                  <input type="radio" class=" d-inline-block" id="No" name="eReleasing" value="No" @if(isset($productstage)) @if($productstage->eReleasing == 'No') checked @endif @endif>
                  <label for="No" class="me-2">No</label>
               </div>
            </div>
            <div class="page-title text-center">
               <h5>
                  Main product field
               </h5>
            </div>
            <div class="row c-label-wrapper">
               <div class="col-lg-12 align-self-center">
                  <div class="left-side-check-wrapper my-4">
                     <div class="row g-3">
                        <div class="col-lg-3 col-xl-2 col-md-4 col-sm-6">
                           <label class="form-check">
                           <input class="form-check-input " type="checkbox" id="eExtractions" name="eExtractions" value="Yes" @if(isset($productstage)) @if($productstage->eExtractions == 'Yes') checked @endif @endif>
                           <span class="form-check-label">Extractions:</span>
                           </label>
                        </div>
                        <div class="col-lg-3 col-xl-2 col-md-4 col-sm-6">
                           <label class="form-check">
                           <input class="form-check-input" type="checkbox" id="eGumShades" name="eGumShades" value="Yes" @if(isset($productstage)) @if($productstage->eGumShades == 'Yes') checked @endif @endif>
                           <span class="form-check-label">Gum Shades:</span>
                           </label>
                        </div>
                        <div class="col-lg-3 col-xl-2 col-md-4 col-sm-6">
                           <label class="form-check">
                           <input class="form-check-input" type="checkbox" id="eTeethShades" name="eTeethShades" value="Yes" @if(isset($productstage)) @if($productstage->eTeethShades == 'Yes') checked @endif @endif>
                           <span class="form-check-label">Teeth Shades:</span>
                           </label>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6">
                           <label class="form-check">
                           <input class="form-check-input" type="checkbox" id="eImpressions" name="eImpressions" value="Yes" @if(isset($productstage)) @if($productstage->eImpressions == 'Yes') checked @endif @endif>
                           <span class="form-check-label">Impressions:</span>
                           </label>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-12">
                  <div id="extractions_option">
                     <ul class="teeth-ul">
                        <li class="teeth-li">
                           <a href="javascript:;">
                              <div class="input-group">
                                 <label class="form-control inner-label te-in-month" value="" id="upper_in_mouth">
                                 <input class="form-check-input default_unchecked_main" type="checkbox" id="eTeethInMouth" name="eTeethInMouth" value="Yes"@if(isset($productstage)) @if($productstage->eTeethInMouth == 'Yes') checked @endif @endif>teeth in mouth
                                 </label>
                                 <input type="hidden" id="upper_in_mouth_value" value="">
                              </div>
                           </a>
                           <div class="check-d-requird">
                              <ul class="p-0 m-0 mt-3">
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eTeethInDefault" name="eTeethInDefault" value="Yes"@if(isset($productstage)) @if($productstage->eTeethInDefault == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Default</span>
                                    </label>
                                 </li>
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eTeethInRequird" name="eTeethInRequird" value="Yes"@if(isset($productstage)) @if($productstage->eTeethInRequird == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Requird:</span>
                                    </label>
                                 </li>
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eTeethInOptional" name="eTeethInOptional" value="Yes"@if(isset($productstage)) @if($productstage->eTeethInOptional == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Optional:</span>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="teeth-li">
                           <div class="input-group">
                              <label class="form-control inner-label missing-te" value="" id="upper_missing_teeth">
                              <input class="form-check-input default_unchecked_main" type="checkbox" id="eMessingTeeth" name="eMessingTeeth" value="Yes"@if(isset($productstage)) @if($productstage->eMessingTeeth == 'Yes') checked @endif @endif> missing teeth
                              </label>
                              <input type="hidden" id="upper_missing_teeth_value" value="">
                           </div>
                           <div class="check-d-requird">
                              <ul class="p-0 m-0 mt-3">
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eMessingDefault" name="eMessingDefault" value="Yes"@if(isset($productstage)) @if($productstage->eMessingDefault == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Default</span>
                                    </label>
                                 </li>
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eMessingRequird" name="eMessingRequird" value="Yes"@if(isset($productstage)) @if($productstage->eMessingRequird == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Requird:</span>
                                    </label>
                                 </li>
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eMessingOptional" name="eMessingOptional" value="Yes"@if(isset($productstage)) @if($productstage->eMessingOptional == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Optional:</span>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="teeth-li">
                           <div class="input-group">
                              <label class="form-control inner-label ex-dekivery" value="" id="upper_ectract_delivery">
                              <input class="form-check-input default_unchecked_main" type="checkbox" id="eExtractDelivery" name="eExtractDelivery" value="Yes"@if(isset($productstage)) @if($productstage->eExtractDelivery == 'Yes') checked @endif @endif> will extract on delivery
                              </label>
                              <input type="hidden" id="upper_ectract_delivery_value" value="">
                           </div>
                           <div class="check-d-requird">
                              <ul class="p-0 m-0 mt-3">
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eExtractDefault" name="eExtractDefault" value="Yes"@if(isset($productstage)) @if($productstage->eExtractDefault == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Default</span>
                                    </label>
                                 </li>
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eExtractRequird" name="eExtractRequird" value="Yes"@if(isset($productstage)) @if($productstage->eExtractRequird == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Requird:</span>
                                    </label>
                                 </li>
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eExtractOptional" name="eExtractOptional" value="Yes"@if(isset($productstage)) @if($productstage->eExtractOptional == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Optional:</span>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="teeth-li">
                           <div class="input-group">
                              <label class="form-control inner-label ben-extracted" value="" id="upper_been_extracted">
                              <input class="form-check-input default_unchecked_main" type="checkbox" id="eExtracted" name="eExtracted" value="Yes"@if(isset($productstage)) @if($productstage->eExtracted == 'Yes') checked @endif @endif> has been extracted
                              </label>
                              <input type="hidden" id="upper_been_extracted_value" value="">
                           </div>
                           <div class="check-d-requird">
                              <ul class="p-0 m-0 mt-3">
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eExtractedDefault" name="eExtractedDefault" value="Yes"@if(isset($productstage)) @if($productstage->eExtractedDefault == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Default</span>
                                    </label>
                                 </li>
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eExtractedRequird" name="eExtractedRequird" value="Yes"@if(isset($productstage)) @if($productstage->eExtractedRequird == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Requird:</span>
                                    </label>
                                 </li>
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eExtractedOptional" name="eExtractedOptional" value="Yes"@if(isset($productstage)) @if($productstage->eExtractedOptional == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Optional:</span>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="teeth-li">
                           <div class="input-group">
                              <label class="form-control inner-label fix-add" value="" id="upper_fix">
                              <input class="form-check-input default_unchecked_main" type="checkbox" id="eFixorAdd" name="eFixorAdd" value="Yes"@if(isset($productstage)) @if($productstage->eFixorAdd == 'Yes') checked @endif @endif> fix or add
                              </label>
                              <input type="hidden" id="upper_fix_value" value="">
                           </div>
                           <div class="check-d-requird">
                              <ul class="p-0 m-0 mt-3">
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eFixorAddDefault" name="eFixorAddDefault" value="Yes"@if(isset($productstage)) @if($productstage->eFixorAddDefault == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Default</span>
                                    </label>
                                 </li>
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eFixorAddRequird" name="eFixorAddRequird" value="Yes"@if(isset($productstage)) @if($productstage->eFixorAddRequird == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Requird:</span>
                                    </label>
                                 </li>
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eFixorAddOptional" name="eFixorAddOptional" value="Yes"@if(isset($productstage)) @if($productstage->eFixorAddOptional == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Optional:</span>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="teeth-li">
                           <div class="input-group">
                              <label class="form-control inner-label clasps" value="" id="upper_clasps">
                              <input class="form-check-input default_unchecked_main" type="checkbox" id="eClasps" name="eClasps" value="Yes"@if(isset($productstage)) @if($productstage->eClasps == 'Yes') checked @endif @endif> clasps
                              </label>
                              <input type="hidden" id="upper_clasps_value" value="">
                           </div>
                           <div class="check-d-requird">
                              <ul class="p-0 m-0 mt-3">
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eClaspsDefault" name="eClaspsDefault" value="Yes"@if(isset($productstage)) @if($productstage->eClaspsDefault == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Default</span>
                                    </label>
                                 </li>
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eClaspsRequird" name="eClaspsRequird" value="Yes"@if(isset($productstage)) @if($productstage->eClaspsRequird == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Requird:</span>
                                    </label>
                                 </li>
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked" type="checkbox" id="eClaspsOptional" name="eClaspsOptional" value="Yes"@if(isset($productstage)) @if($productstage->eClaspsOptional == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Optional:</span>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="page-title text-center">
               <h5>
                  Opposing product field
               </h5>
            </div>
            <div class="row c-label-wrapper">
               <div class="col-lg-12 align-self-center">
                  <div class="left-side-check-wrapper my-4">
                     <div class="row g-3">
                        <div class="col-lg-3 col-xl-2 col-md-4 col-sm-6">
                           <label class="form-check">
                           <input class="form-check-input" type="checkbox" id="eOpposingExtractions" name="eOpposingExtractions" value="Yes" @if(isset($productstage)) @if($productstage->eOpposingExtractions == 'Yes') checked @endif @endif>
                           <span class="form-check-label">Extractions:</span>
                           </label>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6">
                           <label class="form-check">
                           <input class="form-check-input" type="checkbox" id="eOpposingImpressions" name="eOpposingImpressions" value="Yes" @if(isset($productstage)) @if($productstage->eOpposingImpressions == 'Yes') checked @endif @endif>
                           <span class="form-check-label">Impressions:</span>
                           </label>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-12">
                  <div id="extractions_option">
                     <ul class="teeth-ul">
                        <li class="teeth-li">
                           <a href="javascript:;">
                              <div class="input-group">
                                 <label class="form-control inner-label te-in-month" value="" id="upper_in_mouth">
                                 <input class="form-check-input default_unchecked_opposing_main" type="checkbox" id="eOpposingTeethInMouth" name="eOpposingTeethInMouth" value="Yes"@if(isset($productstage)) @if($productstage->eOpposingTeethInMouth == 'Yes') checked @endif @endif>teeth in mouth
                                 </label>
                                 <input type="hidden" id="upper_in_mouth_value" value="">
                              </div>
                           </a>
                           <div class="check-d-requird">
                              <ul class="p-0 m-0 mt-3">
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked_opposing" type="checkbox" id="eOpposingTeethInDefault" name="eOpposingTeethInDefault" value="Yes"@if(isset($productstage)) @if($productstage->eOpposingTeethInDefault == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Default</span>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="teeth-li">
                           <div class="input-group">
                              <label class="form-control inner-label missing-te" value="" id="upper_missing_teeth">
                              <input class="form-check-input default_unchecked_opposing_main" type="checkbox" id="eOpposingMessingTeeth" name="eOpposingMessingTeeth" value="Yes"@if(isset($productstage)) @if($productstage->eOpposingMessingTeeth == 'Yes') checked @endif @endif> missing teeth
                              </label>
                              <input type="hidden" id="upper_missing_teeth_value" value="">
                           </div>
                           <div class="check-d-requird">
                              <ul class="p-0 m-0 mt-3">
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked_opposing" type="checkbox" id="eOpposingMessingDefault" name="eOpposingMessingDefault" value="Yes"@if(isset($productstage)) @if($productstage->eOpposingMessingDefault == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Default</span>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="teeth-li">
                           <div class="input-group">
                              <label class="form-control inner-label ex-dekivery" value="" id="upper_ectract_delivery">
                              <input class="form-check-input default_unchecked_opposing_main" type="checkbox" id="eOpposingExtractDelivery" name="eOpposingExtractDelivery" value="Yes"@if(isset($productstage)) @if($productstage->eOpposingExtractDelivery == 'Yes') checked @endif @endif> will extract on delivery
                              </label>
                              <input type="hidden" id="upper_ectract_delivery_value" value="">
                           </div>
                           <div class="check-d-requird">
                              <ul class="p-0 m-0 mt-3">
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked_opposing" type="checkbox" id="eOpposingExtractDefault" name="eOpposingExtractDefault" value="Yes"@if(isset($productstage)) @if($productstage->eExtractDefault == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Default</span>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="teeth-li">
                           <div class="input-group">
                              <label class="form-control inner-label ben-extracted" value="" id="upper_been_extracted">
                              <input class="form-check-input default_unchecked_opposing_main" type="checkbox" id="eOpposingExtracted" name="eOpposingExtracted" value="Yes"@if(isset($productstage)) @if($productstage->eOpposingExtracted == 'Yes') checked @endif @endif> has been extracted
                              </label>
                              <input type="hidden" id="upper_been_extracted_value" value="">
                           </div>
                           <div class="check-d-requird">
                              <ul class="p-0 m-0 mt-3">
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked_opposing" type="checkbox" id="eOpposingExtractedDefault" name="eOpposingExtractedDefault" value="Yes"@if(isset($productstage)) @if($productstage->eOpposingExtractedDefault == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Default</span>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="teeth-li">
                           <div class="input-group">
                              <label class="form-control inner-label fix-add" value="" id="upper_fix">
                              <input class="form-check-input default_unchecked_opposing_main" type="checkbox" id="eOpposingFixorAdd" name="eOpposingFixorAdd" value="Yes"@if(isset($productstage)) @if($productstage->eOpposingFixorAdd == 'Yes') checked @endif @endif> fix or add
                              </label>
                              <input type="hidden" id="upper_fix_value" value="">
                           </div>
                           <div class="check-d-requird">
                              <ul class="p-0 m-0 mt-3">
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked_opposing" type="checkbox" id="eOpposingFixorAddDefault" name="eOpposingFixorAddDefault" value="Yes"@if(isset($productstage)) @if($productstage->eOpposingFixorAddDefault == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Default</span>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="teeth-li">
                           <div class="input-group">
                              <label class="form-control inner-label clasps" value="" id="upper_clasps">
                              <input class="form-check-input default_unchecked_opposing_main" type="checkbox" id="eOpposingClasps" name="eOpposingClasps" value="Yes"@if(isset($productstage)) @if($productstage->eOpposingClasps == 'Yes') checked @endif @endif> clasps
                              </label>
                              <input type="hidden" id="upper_clasps_value" value="">
                           </div>
                           <div class="check-d-requird">
                              <ul class="p-0 m-0 mt-3">
                                 <li>
                                    <label class="form-check">
                                    <input class="form-check-input default_unchecked_opposing" type="checkbox" id="eOpposingClaspsDefault" name="eOpposingClaspsDefault" value="Yes"@if(isset($productstage)) @if($productstage->eOpposingClaspsDefault == 'Yes') checked @endif @endif>
                                    <span class="form-check-label">Default</span>
                                    </label>
                                 </li>
                              </ul>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            {{-- Office Prices show start --}}
            <hr>
            <div class="col-lg-6 ">
               <h5 class="pt-3">Office Prices</h5>
               <div id="office_price_response">
               </div>
               <div class="listing-page MainOfficePrices">
                  <div class="table-data table-responsive">
                     <table class="table">
                        <thead>
                           <tr>
                              <th class="min-w-100px">
                                 <span class="text-muted fw-bold text-muted d-block fs-7">Office Name</span>
                              </th>
                              @if(isset($product_grade) && count($product_grade)>0)
                              @foreach ($product_grade as $key => $value)
                              <th  class="min-w-100px hideOffice vGradeOfficePrice_{{$value->iGradeId}}">
                                 <span class="text-muted fw-bold text-muted d-block fs-7">{{ $value->vQualityName }}</span>
                              </th>
                              @endforeach
                              @else
                              <th class="min-w-100px MainOfficePrice">
                                 <span class="text-muted fw-bold text-muted d-block fs-7">Office Price</span>
                              </th>
                              @endif
                              <th class="min-w-100px showOffice" style="display: none;">
                                <span class="text-muted fw-bold text-muted d-block fs-7">Office Price</span>
                             </th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($edit) && !empty($edit) && $edit =='Yes') 
                           @if(isset($office) && !empty($office) && count($office) >0)
                           @foreach($office as $key => $office_val)
                           <tr class="hideOffice">
                              <td>{{isset($office_val['vOfficeName'])?$office_val['vOfficeName']:'N/A'}}</td>
                              @if(isset($office_val['grades']) && !empty($office_val['grades']))
                              @foreach ($office_val['grades'] as $key_grade => $value_grade)
                              <td  class="vGradeOfficePrice_{{$value_grade['iGradeid']}}">
                                 <input type="number" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][vOfficePrice]"  class="form-control vGradePrice_{{$key_grade}}" id="" value="{{isset($value_grade['vPrice'])?$value_grade['vPrice']:''}}" required>
                              </td>
                              <input type="hidden" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][iGradeId]" value="{{isset($value_grade['iGradeid'])?$value_grade['iGradeid']:''}}">
                              @endforeach
                              @else
                              {{-- @foreach ($grades as $key_grade => $value_grade)
                              <td style="display: none;" class="vGradeOfficePrice_{{$value_grade->iGradeId}}">
                                 <input type="number" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][vOfficePrice]"  class="form-control " id="" value="" required>
                              </td>
                              <input type="hidden" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][iGradeId]" value="{{isset($value_grade->iGradeId)?$value_grade->iGradeId:''}}">
                              @endforeach --}}
                              <td class="MainOfficePrice" >
                                 <input type="number" name="Office[{{$key}}][vOfficePrice]"  class="form-control vOfficePrice " id="vOfficePrice" value="{{isset($office_val['vPrice'])?$office_val['vPrice']:''}}" required>
                              </td>
                              @endif
                              <input type="hidden" name="Office[{{$key}}][iOfficeId]" value="{{isset($office_val['iOfficeId'])?$office_val['iOfficeId']:''}}">
                              <input type="hidden" name="Office[{{$key}}][vOfficeName]" value="{{isset($office_val['vOfficeName'])?$office_val['vOfficeName']:''}}">
                              <input type="hidden" name="Office[{{$key}}][iProductId]" value="{{isset($office_val['iCategoryProductId'])?$office_val['iCategoryProductId']:''}}">
                           </tr>
                           @endforeach
                           @endif
                           @else
                           @if(isset($office) && !empty($office) && count($office) >0)
                           @foreach($office as $key => $office_val)
                           <tr>
                              <td>{{isset($office_val->vOfficeName)?$office_val->vOfficeName:'N/A'}}</td>
                              <td class="MainOfficePrice" >
                                 <input type="number" name="Office[{{$key}}][vOfficePrice]"  class="form-control vOfficePrice " id="vOfficePrice" value="{{isset($office_val->vOfficePrice)?$office_val->vOfficePrice:''}}" required>
                              </td>
                              @if(isset($grades) && !empty($grades))
                              @foreach ($grades as $key_grade => $value_grade)
                              <td style="display: none;" class="vGradeOfficePrice_{{$value_grade->iGradeId}}">
                                 <input type="number" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][vOfficePrice]"  class="form-control vGradePrice_{{$key_grade}}" id="" value="{{isset($office_val->vOfficePrice)?$office_val->vOfficePrice:''}}" required>
                              </td>
                              <input type="hidden" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][iGradeId]" value="{{isset($value_grade->iGradeId)?$value_grade->iGradeId:''}}">
                              @endforeach
                              @endif
                              <input type="hidden" name="Office[{{$key}}][iOfficeId]" value="{{isset($office_val->iCustomerId)?$office_val->iCustomerId:$office_val->iOfficeId}}">
                              <input type="hidden" name="Office[{{$key}}][vOfficeName]" value="{{$office_val->vOfficeName}}">
                              <input type="hidden" name="Office[{{$key}}][iOfficeProductPriceId]" value="{{isset($office_val->iOfficeProductPriceId)?$office_val->iOfficeProductPriceId:''}}">
                              <input type="hidden" name="Office[{{$key}}][iProductId]" value="{{isset($categoryproducts->iCategoryProductId)?$categoryproducts->iCategoryProductId:''}}">
                           </tr>
                           @endforeach
                           @endif
                           @endif


                           {{-- For fetch old office in edit --}}
                           @if(isset($office_old) && !empty($office_old) && count($office_old) >0)
                           @foreach($office_old as $key => $office_val)
                           <tr class="OldOfficePriceShow" style="display:none;">
                              <td>{{isset($office_val->vOfficeName)?$office_val->vOfficeName:'N/A'}}</td>
                              <td class="" >
                                 <input type="number" name="Office[{{$key}}][vOfficePrice]"  class="form-control vOfficePrice " id="vOfficePrice" value="{{isset($office_val->vOfficePrice)?$office_val->vOfficePrice:''}}" required>
                              </td>
                        
                              <input type="hidden" name="Office[{{$key}}][iOfficeId]" value="{{isset($office_val->iCustomerId)?$office_val->iCustomerId:$office_val->iOfficeId}}">
                              <input type="hidden" name="Office[{{$key}}][vOfficeName]" value="{{$office_val->vOfficeName}}">
                              <input type="hidden" name="Office[{{$key}}][iOfficeProductPriceId]" value="{{isset($office_val->iOfficeProductPriceId)?$office_val->iOfficeProductPriceId:''}}">
                              <input type="hidden" name="Office[{{$key}}][iProductId]" value="{{isset($categoryproducts->iCategoryProductId)?$categoryproducts->iCategoryProductId:''}}">
                           </tr>
                           @endforeach
                           @endif
                           {{-- For fetch old office in edit --}}
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            {{-- Office Prices show end --}}
            <div class="col-12 align-self-end d-inline-block ">
               <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
               <a href="{{route('admin.productstage')}}" class="btn back-btn me-2">Back</a>
            </div>
            @if(isset($grade_data) && count($grade_data)>0)
            <input type="hidden" id="is_grade" name="is_grade" value="yes">
            @else
            <input type="hidden" id="is_grade" name="is_grade" value="no">
            @endif
         </form>
      </div>
   </div>
</div>
</div>
@endsection
@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
   $('#iCategoryId').selectize();
   $('#iCategoryProductId').selectize();
   $('#eType').selectize();
   $('#eStatus').selectize();
   
   $(document).ready(function() {
    @if(isset($grade_data) && count($grade_data)>0)
     $('.MainPrice').hide();
    @endif
       var iCategoryId_edit   = $("#iCategoryId_edit").val();
       var iCategoryProductId_edit   = $("#iCategoryProductId_edit").val();
       if(iCategoryProductId_edit != ''){
             $("#iCategoryProductId").html('');
             $.ajax({
               url:"{{route('admin.productstage.getproduct_edit')}}",
               type: "post",
               data: {
                 iCategoryProductId: iCategoryProductId_edit,
                   _token: '{{csrf_token()}}' 
               },
               dataType : 'json',
               success: function(result){
                 $('#iCategoryProductId').selectize()[0].selectize.destroy();
                   $.each(result.categoryproduct,function(key,value)
                   {
                           if(iCategoryProductId_edit == value.iCategoryProductId) 
                           {
                             $("#iCategoryProductId").append('<option selected value="'+value.iCategoryProductId+'">'+value.vName+'</option>');
                           }
                       });
                       $('#iCategoryProductId').selectize();
                   }
               });
               
         }
         // Main product field
       ischecked= $('#eExtractions').is(':checked');
   
       if (ischecked == false) {
   
         $(".default_unchecked").attr("disabled", true);
         $(".default_unchecked_main").attr("disabled", true);
   
         }else {
           $(".default_unchecked_main").removeAttr("disabled", false);
           
         }
   
         eTeethInMouth= $('#eTeethInMouth').is(':checked');
   
         if (eTeethInMouth == false) {
           $("#eTeethInDefault").attr("disabled", true);
           $("#eTeethInDefault").prop("checked", false);
           $("#eTeethInRequird").attr("disabled", true);
           $("#eTeethInRequird").prop("checked", false);
           $("#eTeethInOptional").attr("disabled", true);
           $("#eTeethInOptional").prop("checked", false);
         }else{
           $("#eTeethInDefault").removeAttr("disabled", false);
           $("#eTeethInRequird").removeAttr("disabled", false);
         }
         eTeethInRequird= $('#eTeethInRequird').is(':checked');
   
           if (eTeethInRequird == false) {
             $("#eTeethInOptional").attr("disabled", true);
             $("#eTeethInOptional").prop("checked", false);
           }else{
             $("#eTeethInOptional").removeAttr("disabled", false);
         }
         eTeethInDefault= $('#eTeethInDefault').is(':checked');
   
           if (eTeethInDefault == true) {
             $("#eMessingDefault").prop("checked", false);
             $("#eExtractDefault").prop("checked", false);
             $("#eExtractedDefault").prop("checked", false);
             $("#eFixorAddDefault").prop("checked", false);
             $("#eClaspsDefault").prop("checked", false);
           }
   
           eMessingTeeth= $('#eMessingTeeth').is(':checked');
   
           if (eMessingTeeth == false) {
             $("#eMessingDefault").attr("disabled", true);
             $("#eMessingDefault").prop("checked", false);
             $("#eMessingRequird").attr("disabled", true);
             $("#eMessingRequird").prop("checked", false);
             $("#eMessingOptional").attr("disabled", true);
             $("#eMessingOptional").prop("checked", false);
           }else{
             $("#eMessingDefault").removeAttr("disabled", false);
             $("#eMessingRequird").removeAttr("disabled", false);
           }
           eMessingRequird= $('#eMessingRequird').is(':checked');
   
           if (eMessingRequird == false) {
             $("#eMessingOptional").attr("disabled", true);
             $("#eMessingOptional").prop("checked", false);
           }else{
             $("#eMessingOptional").removeAttr("disabled", false);
           }
           eMessingDefault= $('#eMessingDefault').is(':checked');
   
           if (eMessingDefault == true) {
             $("#eTeethInDefault").prop("checked", false);
             $("#eExtractDefault").prop("checked", false);
             $("#eExtractedDefault").prop("checked", false);
             $("#eFixorAddDefault").prop("checked", false);
             $("#eClaspsDefault").prop("checked", false);
           }
   
           eExtractDelivery= $('#eExtractDelivery').is(':checked');
   
           if (eExtractDelivery == false) {
             $("#eExtractDefault").attr("disabled", true);
             $("#eExtractDefault").prop("checked", false);
             $("#eExtractRequird").attr("disabled", true);
             $("#eExtractRequird").prop("checked", false);
             $("#eExtractOptional").attr("disabled", true);
             $("#eExtractOptional").prop("checked", false);
           }else{
             $("#eExtractDefault").removeAttr("disabled", false);
             $("#eExtractRequird").removeAttr("disabled", false);
           }
   
           eExtractRequird= $('#eExtractRequird').is(':checked');
   
             if (eExtractRequird == false) {
               $("#eExtractOptional").attr("disabled", true);
               $("#eExtractOptional").prop("checked", false);
             }else{
               $("#eExtractOptional").removeAttr("disabled", false);
             }    
             
             eExtractDefault= $('#eExtractDefault').is(':checked');
   
             if (eExtractDefault == true) {
               $("#eTeethInDefault").prop("checked", false);
               $("#eMessingDefault").prop("checked", false);
               $("#eExtractedDefault").prop("checked", false);
               $("#eFixorAddDefault").prop("checked", false);
               $("#eClaspsDefault").prop("checked", false);
             }
   
             eExtracted= $('#eExtracted').is(':checked');
   
               if (eExtracted == false) {
                 $("#eExtractedDefault").attr("disabled", true);
                 $("#eExtractedDefault").prop("checked", false);
                 $("#eExtractedRequird").attr("disabled", true);
                 $("#eExtractedRequird").prop("checked", false);
                 $("#eExtractedOptional").attr("disabled", true);
                 $("#eExtractedOptional").prop("checked", false);
               }else{
                 $("#eExtractedDefault").removeAttr("disabled", false);
                 $("#eExtractedRequird").removeAttr("disabled", false);
               }
   
               eExtractedRequird= $('#eExtractedRequird').is(':checked');
   
                 if (eExtractedRequird == false) {
                   $("#eExtractedOptional").attr("disabled", true);
                   $("#eExtractedOptional").prop("checked", false);
                 }else{
                   $("#eExtractedOptional").removeAttr("disabled", false);
               }
               eExtractedDefault= $('#eExtractedDefault').is(':checked');
   
               if (eExtractedDefault == true) {
                 $("#eTeethInDefault").prop("checked", false);
                 $("#eMessingDefault").prop("checked", false);
                 $("#eExtractDefault").prop("checked", false);
                 $("#eFixorAddDefault").prop("checked", false);
                 $("#eClaspsDefault").prop("checked", false);
               }
   
           eFixorAdd= $('#eFixorAdd').is(':checked');
   
             if (eFixorAdd == false) {
               $("#eFixorAddDefault").attr("disabled", true);
               $("#eFixorAddDefault").prop("checked", false);
               $("#eFixorAddRequird").attr("disabled", true);
               $("#eFixorAddRequird").prop("checked", false);
               $("#eFixorAddOptional").attr("disabled", true);
               $("#eFixorAddOptional").prop("checked", false);
             }else{
               $("#eFixorAddDefault").removeAttr("disabled", false);
               $("#eFixorAddRequird").removeAttr("disabled", false);
             }
   
             eFixorAddRequird= $('#eFixorAddRequird').is(':checked');
   
             if (eFixorAddRequird == false) {
               $("#eFixorAddOptional").attr("disabled", true);
               $("#eFixorAddOptional").prop("checked", false);
             }else{
               $("#eFixorAddOptional").removeAttr("disabled", false);
             }
   
             eFixorAddDefault= $('#eFixorAddDefault').is(':checked');
   
             if (eFixorAddDefault == true) {
                 $("#eTeethInDefault").prop("checked", false);
                 $("#eMessingDefault").prop("checked", false);
                 $("#eExtractDefault").prop("checked", false);
                 $("#eExtractedDefault").prop("checked", false);
                 $("#eClaspsDefault").prop("checked", false);
              }
   
              eClasps= $('#eClasps').is(':checked');
   
               if (eClasps == false) {
                 $("#eClaspsDefault").attr("disabled", true);
                 $("#eClaspsDefault").prop("checked", false);
                 $("#eClaspsRequird").attr("disabled", true);
                 $("#eClaspsRequird").prop("checked", false);
                 $("#eClaspsOptional").attr("disabled", true);
                 $("#eClaspsOptional").prop("checked", false);
               }else{
                 $("#eClaspsDefault").removeAttr("disabled", false);
                 $("#eClaspsRequird").removeAttr("disabled", false);
               }
               eClaspsRequird= $('#eClaspsRequird').is(':checked');
   
               if (eClaspsRequird == false) {
                 $("#eClaspsOptional").attr("disabled", true);
                 $("#eClaspsOptional").prop("checked", false);
               }else{
                 $("#eClaspsOptional").removeAttr("disabled", false);
               }
               eClaspsDefault= $('#eClaspsDefault').is(':checked');
   
               if (eClaspsDefault == true) {
                 $("#eTeethInDefault").prop("checked", false);
                 $("#eMessingDefault").prop("checked", false);
                 $("#eExtractDefault").prop("checked", false);
                 $("#eExtractedDefault").prop("checked", false);
                 $("#eFixorAddDefault").prop("checked", false);
               }
         // Main product field End
   
         // Opposing product field
   
         ischecked_opposing= $('#eOpposingExtractions').is(':checked');
   
       if (ischecked_opposing == false) {
   
         $(".default_unchecked_opposing").attr("disabled", true);
         $(".default_unchecked_opposing_main").attr("disabled", true);
   
         }else {
           $(".default_unchecked_opposing_main").removeAttr("disabled", false);
           
         }
         eOpposingTeethInMouth= $('#eOpposingTeethInMouth').is(':checked');
   
         if (eOpposingTeethInMouth == false) {
           $("#eOpposingTeethInDefault").attr("disabled", true);
           $("#eOpposingTeethInDefault").prop("checked", false);
         }else{
           $("#eOpposingTeethInDefault").removeAttr("disabled", false);
         }
         eOpposingTeethInDefault= $('#eOpposingTeethInDefault').is(':checked');
   
           if (eOpposingTeethInDefault == true) {
             $("#eOpposingMessingDefault").prop("checked", false);
             $("#eOpposingExtractDefault").prop("checked", false);
             $("#eOpposingExtractedDefault").prop("checked", false);
             $("#eOpposingFixorAddDefault").prop("checked", false);
             $("#eOpposingClaspsDefault").prop("checked", false);
           }
   
           eOpposingMessingTeeth= $('#eOpposingMessingTeeth').is(':checked');
   
           if (eOpposingMessingTeeth == false) {
             $("#eOpposingMessingDefault").attr("disabled", true);
             $("#eOpposingMessingDefault").prop("checked", false);
           }else{
             $("#eOpposingMessingDefault").removeAttr("disabled", false);
           }
           eOpposingMessingDefault= $('#eOpposingMessingDefault').is(':checked');
   
             if (eOpposingMessingDefault == true) {
               $("#eOpposingTeethInDefault").prop("checked", false);
               $("#eOpposingExtractDefault").prop("checked", false);
               $("#eOpposingExtractedDefault").prop("checked", false);
               $("#eOpposingFixorAddDefault").prop("checked", false);
               $("#eOpposingClaspsDefault").prop("checked", false);
             }
             eOpposingExtractDelivery= $('#eOpposingExtractDelivery').is(':checked');
   
               if (eOpposingExtractDelivery == false) {
                 $("#eOpposingExtractDefault").attr("disabled", true);
                 $("#eOpposingExtractDefault").prop("checked", false);
               }else{
                 $("#eOpposingExtractDefault").removeAttr("disabled", false);
               }
               eOpposingExtractDefault= $('#eOpposingExtractDefault').is(':checked');
   
               if (eOpposingExtractDefault == true) {
                 $("#eOpposingTeethInDefault").prop("checked", false);
                 $("#eOpposingMessingDefault").prop("checked", false);
                 $("#eOpposingExtractedDefault").prop("checked", false);
                 $("#eOpposingFixorAddDefault").prop("checked", false);
                 $("#eOpposingClaspsDefault").prop("checked", false);
               }
   
               eOpposingExtracted= $('#eOpposingExtracted').is(':checked');
   
               if (eOpposingExtracted == false) {
                 $("#eOpposingExtractedDefault").attr("disabled", true);
                 $("#eOpposingExtractedDefault").prop("checked", false);
               }else{
                 $("#eOpposingExtractedDefault").removeAttr("disabled", false);
               }
               eOpposingExtractedDefault= $('#eOpposingExtractedDefault').is(':checked');
   
                 if (eOpposingExtractedDefault == true) {
                   $("#eOpposingTeethInDefault").prop("checked", false);
                   $("#eOpposingMessingDefault").prop("checked", false);
                   $("#eOpposingExtractDefault").prop("checked", false);
                   $("#eOpposingFixorAddDefault").prop("checked", false);
                   $("#eOpposingClaspsDefault").prop("checked", false);
                 }
                 eOpposingFixorAdd= $('#eOpposingFixorAdd').is(':checked');
                 if (eOpposingFixorAdd == false) {
                   $("#eOpposingFixorAddDefault").attr("disabled", true);
                   $("#eOpposingFixorAddDefault").prop("checked", false);
                 }else{
                   $("#eOpposingFixorAddDefault").removeAttr("disabled", false);
                 }
                 eOpposingFixorAddDefault= $('#eOpposingFixorAddDefault').is(':checked');
   
                 if (eOpposingFixorAddDefault == true) {
                   $("#eOpposingTeethInDefault").prop("checked", false);
                   $("#eOpposingMessingDefault").prop("checked", false);
                   $("#eOpposingExtractDefault").prop("checked", false);
                   $("#eOpposingExtractedDefault").prop("checked", false);
                   $("#eOpposingClaspsDefault").prop("checked", false);
                 }
                 eOpposingClasps= $('#eOpposingClasps').is(':checked');
   
                 if (eOpposingClasps == false) {
                   $("#eOpposingClaspsDefault").attr("disabled", true);
                   $("#eOpposingClaspsDefault").prop("checked", false);
                 }else{
                   $("#eOpposingClaspsDefault").removeAttr("disabled", false);
                 }
                 eOpposingClaspsDefault= $('#eOpposingClaspsDefault').is(':checked');
   
                 if (eOpposingClaspsDefault == true) {
                   $("#eOpposingTeethInDefault").prop("checked", false);
                   $("#eOpposingMessingDefault").prop("checked", false);
                   $("#eOpposingExtractDefault").prop("checked", false);
                   $("#eOpposingExtractedDefault").prop("checked", false);
                   $("#eOpposingFixorAddDefault").prop("checked", false);
                 }
         // Opposing product field End
   
   }); 
   
   //Main product field
   $(document).on('change', '#eExtractions', function() {
     eExtractions= $('#eExtractions').is(':checked');
   
       if (eExtractions == true) {
   
         $(".default_unchecked_main").removeAttr("disabled", false);
   
       }else {
         
         $(".default_unchecked_main").attr("disabled", true);
         $(".default_unchecked_main").prop("checked", false);
         $(".default_unchecked").attr("disabled", true);
         $(".default_unchecked").prop("checked", false);
       }
     });
     
   
     //Teeth In Mouth Checkbox
       $(document).on('change', '#eTeethInMouth', function() {
         eTeethInMouth= $('#eTeethInMouth').is(':checked');
   
           if (eTeethInMouth == false) {
             $("#eTeethInDefault").attr("disabled", true);
             $("#eTeethInDefault").prop("checked", false);
             $("#eTeethInRequird").attr("disabled", true);
             $("#eTeethInRequird").prop("checked", false);
             $("#eTeethInOptional").attr("disabled", true);
             $("#eTeethInOptional").prop("checked", false);
           }else{
             $("#eTeethInDefault").removeAttr("disabled", false);
             $("#eTeethInRequird").removeAttr("disabled", false);
         }
       });
       $(document).on('change', '#eTeethInRequird', function() {
         eTeethInRequird= $('#eTeethInRequird').is(':checked');
   
           if (eTeethInRequird == false) {
             $("#eTeethInOptional").attr("disabled", true);
             $("#eTeethInOptional").prop("checked", false);
           }else{
             $("#eTeethInOptional").removeAttr("disabled", false);
         }
       });
   
       $(document).on('change', '#eTeethInDefault', function() {
         eTeethInDefault= $('#eTeethInDefault').is(':checked');
   
           if (eTeethInDefault == true) {
             $("#eMessingDefault").prop("checked", false);
             $("#eExtractDefault").prop("checked", false);
             $("#eExtractedDefault").prop("checked", false);
             $("#eFixorAddDefault").prop("checked", false);
             $("#eClaspsDefault").prop("checked", false);
           }
       });
     //Teeth In Mouth Checkbox End
   
     //Messing Teeth Checkbox
     $(document).on('change', '#eMessingTeeth', function() {
         eMessingTeeth= $('#eMessingTeeth').is(':checked');
   
           if (eMessingTeeth == false) {
             $("#eMessingDefault").attr("disabled", true);
             $("#eMessingDefault").prop("checked", false);
             $("#eMessingRequird").attr("disabled", true);
             $("#eMessingRequird").prop("checked", false);
             $("#eMessingOptional").attr("disabled", true);
             $("#eMessingOptional").prop("checked", false);
           }else{
             $("#eMessingDefault").removeAttr("disabled", false);
             $("#eMessingRequird").removeAttr("disabled", false);
           }
       });
       $(document).on('change', '#eMessingRequird', function() {
         eMessingRequird= $('#eMessingRequird').is(':checked');
   
           if (eMessingRequird == false) {
             $("#eMessingOptional").attr("disabled", true);
             $("#eMessingOptional").prop("checked", false);
           }else{
             $("#eMessingOptional").removeAttr("disabled", false);
         }
       });
       $(document).on('change', '#eMessingDefault', function() {
         eMessingDefault= $('#eMessingDefault').is(':checked');
   
           if (eMessingDefault == true) {
             $("#eTeethInDefault").prop("checked", false);
             $("#eExtractDefault").prop("checked", false);
             $("#eExtractedDefault").prop("checked", false);
             $("#eFixorAddDefault").prop("checked", false);
             $("#eClaspsDefault").prop("checked", false);
           }
       });
     //Messing Teeth Checkbox End
   
     //Extract Delivery Checkbox
     $(document).on('change', '#eExtractDelivery', function() {
         eExtractDelivery= $('#eExtractDelivery').is(':checked');
   
           if (eExtractDelivery == false) {
             $("#eExtractDefault").attr("disabled", true);
             $("#eExtractDefault").prop("checked", false);
             $("#eExtractRequird").attr("disabled", true);
             $("#eExtractRequird").prop("checked", false);
             $("#eExtractOptional").attr("disabled", true);
             $("#eExtractOptional").prop("checked", false);
           }else{
             $("#eExtractDefault").removeAttr("disabled", false);
             $("#eExtractRequird").removeAttr("disabled", false);
           }
       });
       $(document).on('change', '#eExtractRequird', function() {
         eExtractRequird= $('#eExtractRequird').is(':checked');
   
           if (eExtractRequird == false) {
             $("#eExtractOptional").attr("disabled", true);
             $("#eExtractOptional").prop("checked", false);
           }else{
             $("#eExtractOptional").removeAttr("disabled", false);
         }
       });
       $(document).on('change', '#eExtractDefault', function() {
         eExtractDefault= $('#eExtractDefault').is(':checked');
   
           if (eExtractDefault == true) {
             $("#eTeethInDefault").prop("checked", false);
             $("#eMessingDefault").prop("checked", false);
             $("#eExtractedDefault").prop("checked", false);
             $("#eFixorAddDefault").prop("checked", false);
             $("#eClaspsDefault").prop("checked", false);
           }
       });
     //Extract Delivery Checkbox End
   
     //Extract  Checkbox
   
     $(document).on('change', '#eExtracted', function() {
       eExtracted= $('#eExtracted').is(':checked');
   
         if (eExtracted == false) {
           $("#eExtractedDefault").attr("disabled", true);
           $("#eExtractedDefault").prop("checked", false);
           $("#eExtractedRequird").attr("disabled", true);
           $("#eExtractedRequird").prop("checked", false);
           $("#eExtractedOptional").attr("disabled", true);
           $("#eExtractedOptional").prop("checked", false);
         }else{
           $("#eExtractedDefault").removeAttr("disabled", false);
           $("#eExtractedRequird").removeAttr("disabled", false);
         }
     });
     $(document).on('change', '#eExtractedRequird', function() {
       eExtractedRequird= $('#eExtractedRequird').is(':checked');
   
         if (eExtractedRequird == false) {
           $("#eExtractedOptional").attr("disabled", true);
           $("#eExtractedOptional").prop("checked", false);
         }else{
           $("#eExtractedOptional").removeAttr("disabled", false);
       }
     });
     $(document).on('change', '#eExtractedDefault', function() {
         eExtractedDefault= $('#eExtractedDefault').is(':checked');
   
           if (eExtractedDefault == true) {
             $("#eTeethInDefault").prop("checked", false);
             $("#eMessingDefault").prop("checked", false);
             $("#eExtractDefault").prop("checked", false);
             $("#eFixorAddDefault").prop("checked", false);
             $("#eClaspsDefault").prop("checked", false);
           }
       });
   //Extract Checkbox End
   
   //Fixor Add  Checkbox
   $(document).on('change', '#eFixorAdd', function() {
       eFixorAdd= $('#eFixorAdd').is(':checked');
   
         if (eFixorAdd == false) {
           $("#eFixorAddDefault").attr("disabled", true);
           $("#eFixorAddDefault").prop("checked", false);
           $("#eFixorAddRequird").attr("disabled", true);
           $("#eFixorAddRequird").prop("checked", false);
           $("#eFixorAddOptional").attr("disabled", true);
           $("#eFixorAddOptional").prop("checked", false);
         }else{
           $("#eFixorAddDefault").removeAttr("disabled", false);
           $("#eFixorAddRequird").removeAttr("disabled", false);
         }
     });
     $(document).on('change', '#eFixorAddRequird', function() {
       eFixorAddRequird= $('#eFixorAddRequird').is(':checked');
   
         if (eFixorAddRequird == false) {
           $("#eFixorAddOptional").attr("disabled", true);
           $("#eFixorAddOptional").prop("checked", false);
         }else{
           $("#eFixorAddOptional").removeAttr("disabled", false);
       }
     });
     $(document).on('change', '#eFixorAddDefault', function() {
         eFixorAddDefault= $('#eFixorAddDefault').is(':checked');
   
           if (eFixorAddDefault == true) {
             $("#eTeethInDefault").prop("checked", false);
             $("#eMessingDefault").prop("checked", false);
             $("#eExtractDefault").prop("checked", false);
             $("#eExtractedDefault").prop("checked", false);
             $("#eClaspsDefault").prop("checked", false);
           }
       });
   //Fixor Add Checkbox End
   
   //Clasps  Checkbox
   $(document).on('change', '#eClasps', function() {
       eClasps= $('#eClasps').is(':checked');
   
         if (eClasps == false) {
           $("#eClaspsDefault").attr("disabled", true);
           $("#eClaspsDefault").prop("checked", false);
           $("#eClaspsRequird").attr("disabled", true);
           $("#eClaspsRequird").prop("checked", false);
           $("#eClaspsOptional").attr("disabled", true);
           $("#eClaspsOptional").prop("checked", false);
         }else{
           $("#eClaspsDefault").removeAttr("disabled", false);
           $("#eClaspsRequird").removeAttr("disabled", false);
         }
     });
     $(document).on('change', '#eClaspsRequird', function() {
       eClaspsRequird= $('#eClaspsRequird').is(':checked');
   
         if (eClaspsRequird == false) {
           $("#eClaspsOptional").attr("disabled", true);
           $("#eClaspsOptional").prop("checked", false);
         }else{
           $("#eClaspsOptional").removeAttr("disabled", false);
       }
     });
     $(document).on('change', '#eClaspsDefault', function() {
         eClaspsDefault= $('#eClaspsDefault').is(':checked');
   
           if (eClaspsDefault == true) {
             $("#eTeethInDefault").prop("checked", false);
             $("#eMessingDefault").prop("checked", false);
             $("#eExtractDefault").prop("checked", false);
             $("#eExtractedDefault").prop("checked", false);
             $("#eFixorAddDefault").prop("checked", false);
           }
       });
   //Clasps Checkbox End
   
   // Main product field End
   
   
   // Opposing product field 
   
   $(document).on('change', '#eOpposingExtractions', function() {
     eOpposingExtractions= $('#eOpposingExtractions').is(':checked');
   
       if (eOpposingExtractions == true) {
   
         $(".default_unchecked_opposing_main").removeAttr("disabled", false);
   
       }else {
         
         $(".default_unchecked_opposing_main").attr("disabled", true);
         $(".default_unchecked_opposing_main").prop("checked", false);
         $(".default_unchecked_opposing").attr("disabled", true);
         $(".default_unchecked_opposing").prop("checked", false);
       }
     });
   
     //Teeth In Mouth Checkbox
     $(document).on('change', '#eOpposingTeethInMouth', function() {
         eOpposingTeethInMouth= $('#eOpposingTeethInMouth').is(':checked');
   
           if (eOpposingTeethInMouth == false) {
             $("#eOpposingTeethInDefault").attr("disabled", true);
             $("#eOpposingTeethInDefault").prop("checked", false);
           }else{
             $("#eOpposingTeethInDefault").removeAttr("disabled", false);
         }
       });
       $(document).on('change', '#eOpposingTeethInDefault', function() {
         eOpposingTeethInDefault= $('#eOpposingTeethInDefault').is(':checked');
   
           if (eOpposingTeethInDefault == true) {
             $("#eOpposingMessingDefault").prop("checked", false);
             $("#eOpposingExtractDefault").prop("checked", false);
             $("#eOpposingExtractedDefault").prop("checked", false);
             $("#eOpposingFixorAddDefault").prop("checked", false);
             $("#eOpposingClaspsDefault").prop("checked", false);
           }
       });
     //Teeth In Mouth Checkbox End
   
     //Messing Teeth Checkbox
     $(document).on('change', '#eOpposingMessingTeeth', function() {
         eOpposingMessingTeeth= $('#eOpposingMessingTeeth').is(':checked');
   
           if (eOpposingMessingTeeth == false) {
             $("#eOpposingMessingDefault").attr("disabled", true);
             $("#eOpposingMessingDefault").prop("checked", false);
           }else{
             $("#eOpposingMessingDefault").removeAttr("disabled", false);
           }
       });
       $(document).on('change', '#eOpposingMessingDefault', function() {
         eOpposingMessingDefault= $('#eOpposingMessingDefault').is(':checked');
   
           if (eOpposingMessingDefault == true) {
             $("#eOpposingTeethInDefault").prop("checked", false);
             $("#eOpposingExtractDefault").prop("checked", false);
             $("#eOpposingExtractedDefault").prop("checked", false);
             $("#eOpposingFixorAddDefault").prop("checked", false);
             $("#eOpposingClaspsDefault").prop("checked", false);
           }
       });
     //Messing Teeth Checkbox End
   
     //Extract Delivery Checkbox
     $(document).on('change', '#eOpposingExtractDelivery', function() {
         eOpposingExtractDelivery= $('#eOpposingExtractDelivery').is(':checked');
   
           if (eOpposingExtractDelivery == false) {
             $("#eOpposingExtractDefault").attr("disabled", true);
             $("#eOpposingExtractDefault").prop("checked", false);
           }else{
             $("#eOpposingExtractDefault").removeAttr("disabled", false);
           }
       });
       $(document).on('change', '#eOpposingExtractDefault', function() {
         eOpposingExtractDefault= $('#eOpposingExtractDefault').is(':checked');
   
           if (eOpposingExtractDefault == true) {
             $("#eOpposingTeethInDefault").prop("checked", false);
             $("#eOpposingMessingDefault").prop("checked", false);
             $("#eOpposingExtractedDefault").prop("checked", false);
             $("#eOpposingFixorAddDefault").prop("checked", false);
             $("#eOpposingClaspsDefault").prop("checked", false);
           }
       });
     //Extract Delivery Checkbox End
   
     //Extract  Checkbox
   
     $(document).on('change', '#eOpposingExtracted', function() {
       eOpposingExtracted= $('#eOpposingExtracted').is(':checked');
   
         if (eOpposingExtracted == false) {
           $("#eOpposingExtractedDefault").attr("disabled", true);
           $("#eOpposingExtractedDefault").prop("checked", false);
         }else{
           $("#eOpposingExtractedDefault").removeAttr("disabled", false);
         }
     });
     $(document).on('change', '#eOpposingExtractedDefault', function() {
         eOpposingExtractedDefault= $('#eOpposingExtractedDefault').is(':checked');
   
           if (eOpposingExtractedDefault == true) {
             $("#eOpposingTeethInDefault").prop("checked", false);
             $("#eOpposingMessingDefault").prop("checked", false);
             $("#eOpposingExtractDefault").prop("checked", false);
             $("#eOpposingFixorAddDefault").prop("checked", false);
             $("#eOpposingClaspsDefault").prop("checked", false);
           }
       });
   //Extract Checkbox End
   
   //Fixor Add  Checkbox
   $(document).on('change', '#eOpposingFixorAdd', function() {
       eOpposingFixorAdd= $('#eOpposingFixorAdd').is(':checked');
         if (eOpposingFixorAdd == false) {
           $("#eOpposingFixorAddDefault").attr("disabled", true);
           $("#eOpposingFixorAddDefault").prop("checked", false);
         }else{
           $("#eOpposingFixorAddDefault").removeAttr("disabled", false);
         }
     });
     $(document).on('change', '#eOpposingFixorAddDefault', function() {
         eOpposingFixorAddDefault= $('#eOpposingFixorAddDefault').is(':checked');
   
           if (eOpposingFixorAddDefault == true) {
             $("#eOpposingTeethInDefault").prop("checked", false);
             $("#eOpposingMessingDefault").prop("checked", false);
             $("#eOpposingExtractDefault").prop("checked", false);
             $("#eOpposingExtractedDefault").prop("checked", false);
             $("#eOpposingClaspsDefault").prop("checked", false);
           }
       });
   //Fixor Add Checkbox End
   
   //Clasps  Checkbox
   $(document).on('change', '#eOpposingClasps', function() {
       eOpposingClasps= $('#eOpposingClasps').is(':checked');
   
         if (eOpposingClasps == false) {
           $("#eOpposingClaspsDefault").attr("disabled", true);
           $("#eOpposingClaspsDefault").prop("checked", false);
         }else{
           $("#eOpposingClaspsDefault").removeAttr("disabled", false);
         }
     });
     $(document).on('change', '#eOpposingClaspsDefault', function() {
         eOpposingClaspsDefault= $('#eOpposingClaspsDefault').is(':checked');
   
           if (eOpposingClaspsDefault == true) {
             $("#eOpposingTeethInDefault").prop("checked", false);
             $("#eOpposingMessingDefault").prop("checked", false);
             $("#eOpposingExtractDefault").prop("checked", false);
             $("#eOpposingExtractedDefault").prop("checked", false);
             $("#eOpposingFixorAddDefault").prop("checked", false);
           }
       });
   //Clasps Checkbox End
     
   // Opposing product field End
   $(document).on('change', '#iCategoryId', function() {
         var iCategoryId = $("#iCategoryId").val();
         $.ajax({
             url:"{{route('admin.productstage.getproduct')}}",
             type: "post",
             data: {
                 iCategoryId: iCategoryId,
                 _token: '{{csrf_token()}}' 
             },
             success: function(result){
   
               $('#iCategoryProductId').selectize()[0].selectize.destroy();
               $('#iCategoryProductId').html(result);
               $('#iCategoryProductId').selectize();
              }
         });
     });
   
   
   $(document).on('click', '.color_radio', function() {
     var eType = $('input[name="eType"]:checked').val();
     $.ajax({
       url: "{{route('admin.productstage.fetch_category') }}",
       type: "POST",
       data: {
         eType: eType,
         "_token": "{{ csrf_token() }}",
       },
       success: function(response) {
           $('#iCategoryId').selectize()[0].selectize.destroy();
           $("#iCategoryId").html(response);
           $('#iCategoryId').selectize();
         
       }
     });
   });
   
   $(document).on('click', '#submit', function() {
     iProductStageId = $("#id").val();
     iCategoryId = $("#iCategoryId").val();
     iCategoryProductId = $("#iCategoryProductId").val();
     vCode = $("#vCode").val();
     vName = $("#vName").val();
     vPrice = $("#vPrice").val();
     iPickUpDay = $("#iPickUpDay").val();
     iProcessDay = $("#iProcessDay").val();
     iDeliverDay = $("#iDeliverDay").val();
     iSequence = $("#iSequence").val();
     eType = $('input[name="eType"]:checked').val();
   
     var error = false;
     var total_grade = $('#total_grades').val();
     if(total_grade != undefined && total_grade !=0)
     {
       for (let i = 0; i < total_grade; i++) {
         var vStagePrice = $('#vStagePrice'+i).val();
       
         if(vStagePrice.length == 0)
         {
             $("#vStagePrice_error"+i).show();
             error = true;
         }  
         else
         {
             $("#vStagePrice_error"+i).hide();
         }
       }
     }
     else
     {
       @if(isset($grade_data) && count($grade_data) == 0)
       if (vPrice.length == 0) {
       $("#vPrice_error").show();
       error = true;
       } else {
         $("#vPrice_error").hide();
       }
       @endif
     }
   
     if ($('input[name="eType"]:checked').length == 0) {
       $("#eType_error").show();
       error=true;
      }else {
        $("#eType_error").hide();
     }
     
     if (vCode.length == 0) {
       $("#vCode_error").show();
       error = true;
     } else {
       $("#vCode_error").hide();
     }
     if (iCategoryId == "none") {
       $("#iCategoryId_error").show();
       error = true;
     } else {
       $("#iCategoryId_error").hide();
     }
     if (iCategoryProductId == "none") {
       $("#iCategoryProductId_error").show();
       error = true;
     } else {
       $("#iCategoryProductId_error").hide();
     }
     if (iSequence.length == 0) {
       $("#iSequence_error").show();
       error = true;
     } else {
       $("#iSequence_error").hide();
     }
   
     if (vName.length == 0) {
       $("#vName_error").show();
       error = true;
     } else {
       $("#vName_error").hide();
     }
     
     if (iPickUpDay.length == 0) {
       $("#iPickUpDay_error").show();
       error = true;
     } else {
       $("#iPickUpDay_error").hide();
     }
     if (iProcessDay.length == 0) {
       $("#iProcessDay_error").show();
       error = true;
     } else {
       $("#iProcessDay_error").hide();
     }
     if (iDeliverDay.length == 0) {
       $("#iDeliverDay_error").show();
       error = true;
     } else {
       $("#iDeliverDay_error").hide();
     }
     
     setTimeout(function() {
       if (error == true) {
         return false;
       } else {
         $("#frm").submit();
         return true;
       }
     }, 1000);
   
   });
   
    //For update office price start
         $("#vPrice").keyup(function(){
             $('.vOfficePrice').val(this.value);
         });
     //For update office price end
   
     // grade price by office start
     $(document).on('change', '#iCategoryProductId', function() {
     
         $('.GradePrices').hide();
         iCategoryProductId = $(this).val();
         $.ajax({
         url: "{{route('admin.productstage.fetch_grade_price') }}",
         type: "POST",
         data: {
           iCategoryProductId: iCategoryProductId,
           "_token": "{{ csrf_token() }}",
         },
         success: function(response) {
          
           if(response != 'no_grade')
           {
             $("#Grade_price_response").html(response);
             $('.MainPrice').hide();
             
             setTimeout(() => {
               ShowOfficeGradePrce(iCategoryProductId)
             }, 1000);
             $('#is_grade').val('yes');
           }
           else
           {
           
               $("#Grade_price_response").html('');
               $("#office_price_response").html('');
               $('.MainOfficePrice').show();
               $('.MainOfficePrices').show();
               $('.MainPrice').show();
               $('#vPrice').val('');
               $('#is_grade').val('no');
               @if(isset($edit) && !empty($edit) && $edit =='Yes')
               $('.hideOffice').hide()
               $('.MainOfficePrice').hide()
               $('.OldOfficePriceShow').show();
               $('.showOffice').show();
               @endif
           }
         }
       });
     });
   
     function ShowOfficeGradePrce(iCategoryProductId)
     {
       $.ajax({
         url: "{{route('admin.productstage.fetch_office_grade_price') }}",
         type: "POST",
         data: {
           iCategoryProductId: iCategoryProductId,
           "_token": "{{ csrf_token() }}",
         },
         success: function(response) {
           
             if(response !=0)
             {
               $('.MainOfficePrices').hide();
               $("#office_price_response").html(response);
               $('.MainOfficePrice').hide();
             }
             else
             {
               $("#office_price_response").html('');
              
             }
         }
       });
     }
     // grade price by office end

    //  Change office price defult
    
    $(".StagePriceChange").keyup(function(){
      var stage_prce_id = $(this).attr("data-id");
      var stage_prce_val = $(this).val();
      $(".vGradePrice_"+stage_prce_id).val(stage_prce_val);  
    });
</script>
@endsection