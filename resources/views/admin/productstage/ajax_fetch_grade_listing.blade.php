
@if(isset($grade_data) && count($grade_data)>0)
<input type="hidden" id="total_grades" value="{{count($grade_data);}}">
<div class=" GradePrices">
    <label>Grades - Price</label>
    <div id="toolbar-container"></div>
        @foreach ($grade_data as $key=> $grade_data_val)
        <div class="row g-3 align-items-center">
            <div class="col-3">
                <span>@if(isset($grade_data_val->vQualityName)){{$grade_data_val->vQualityName}}  @endif</span>
            </div>
            <div class="col-auto">
                <input type="number" class="form-control StagePriceChange" id="vStagePrice{{$key}}" name="vStagePrice[]" placeholder="Price" data-id ="{{$key}}" value="">
                <input type="hidden" class="form-control"  name="iGradeId[]"  value="@if(isset($grade_data_val->iGradeId)){{$grade_data_val->iGradeId}}@endif">
                <div class="text-danger" style="display: none;" id="vStagePrice_error{{$key}}">Select grade price</div>
            </div>            
        </div>
        @endforeach
    </div>
@endif

<script>
    $(".StagePriceChange").keyup(function(){
      var stage_prce_id = $(this).attr("data-id");
      var stage_prce_val = $(this).val();
      $(".vGradePrice_"+stage_prce_id).val(stage_prce_val);  
    });
    </script>