<div class="listing-page ">
    <div class="table-data table-responsive">
       <table class="table">
          <thead>
             <tr>
                <th class="min-w-100px">
                   <span class="text-muted fw-bold text-muted d-block fs-7">Office Name</span>
                </th>
                <th class="min-w-100px MainOfficePrice">
                   <span class="text-muted fw-bold text-muted d-block fs-7">Office Price</span>
                </th>
                @if(isset($product_grade) && !empty($product_grade))
                 @foreach ($product_grade as $key => $value)
                 <th  class="min-w-100px vGradeOfficePrice_{{$value->iGradeId}}">
                     <span class="text-muted fw-bold text-muted d-block fs-7">{{ $value->vQualityName }}</span>
                 </th>
                 @endforeach
                @endif
             </tr>
          </thead>
          <tbody>
                     @if(isset($office) && !empty($office) && count($office) >0)
                     @foreach($office as $key => $office_val)
                     <tr>
                         <td>{{isset($office_val['vOfficeName'])?$office_val['vOfficeName']:'N/A'}}</td>
                         <td class="MainOfficePrice" >
                             <input type="number" name="Office[{{$key}}][vOfficePrice]"  class="form-control vOfficePrice " id="vOfficePrice" value="{{isset($office_val['vPrice'])?$office_val['vPrice']:''}}" required>
                         </td>
                       
                         @if(isset($office_val['grades']) && !empty($office_val['grades']))
                             @foreach ($office_val['grades'] as $key_grade => $value_grade)
                             <td class="vGradeOfficePrice_{{$value_grade['iGradeid']}}">
                                 <input type="number" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][vOfficePrice]"  class="form-control vGradePrice_{{$key_grade}}" id="" value="" required>
                             </td>
                             <input type="hidden" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][iGradeId]" value="{{isset($value_grade['iGradeid'])?$value_grade['iGradeid']:''}}">
                             @endforeach
                             @else
                             @foreach ($grades as $key_grade => $value_grade)
                             <td style="display: none;" class="vGradeOfficePrice_{{$value_grade->iGradeId}}">
                                 <input type="number" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][vOfficePrice]"  class="form-control vGradePrice_{{$key_grade}}" id="" value="" required>
                             </td>
                             <input type="hidden" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][iGradeId]" value="{{isset($value_grade->iGradeId)?$value_grade->iGradeId:''}}">
                             @endforeach

                         @endif
                         <input type="hidden" name="Office[{{$key}}][iOfficeId]" value="{{isset($office_val['iOfficeId'])?$office_val['iOfficeId']:''}}">
                         <input type="hidden" name="Office[{{$key}}][vOfficeName]" value="{{isset($office_val['vOfficeName'])?$office_val['vOfficeName']:''}}">
                       
                         <input type="hidden" name="Office[{{$key}}][iProductId]" value="{{isset($office_val['iCategoryProductId'])?$office_val['iCategoryProductId']:''}}">
                     
                     </tr>
                     @endforeach
                 @endif
              
          </tbody>
       </table>
    </div>
 </div>