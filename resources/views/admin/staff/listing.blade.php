@extends('layouts.admin.index')
@section('content')
    <!-- BEGIN: Content-->
    <!-- DOM - jQuery events table -->
    <div class="main-panel">
        <div class="page-title text-center">
            <h3>
                Staff
            </h3>
        </div>
        <div class="col-lg-12 mx-auto">
            <div class="listing-page">
                <div class="card mb-5 mb-xl-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-mid">
                                    <ul class="d-flex list-unstyled">
                                        @if(\App\Libraries\General::check_permission('','eCreate') == true)
                                        <li>
                                            <a href="{{ route('admin.staff.create') }}" class="btn create_permission add-btn">
                                                <i class="fa fa-plus"></i> Add
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mx-auto">
                            <div class="custome-serce-box">
                                <input type="text" class="form-control" id="keyword" name="search" placeholder="Search">
                                <span class="search-btn"><i class="fas fa-search"></i></span>
                            </div>
                        </div>
                        @if(\App\Libraries\General::admin_info()['customerType']=='Super Admin')
                        <div class="col-lg-4 mx-auto">
                            <select name="iCustomerId" id="iCustomerId" class="w-100 filter_change">
                                <option value="">Select Cutomer</option>
                                @foreach ($customer as $key => $customers)
                                    <option value="{{ $customers->iCustomerId }}">
                                        {{ $customers->vOfficeName }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        <div class="col-lg-4 mx-auto">
                            <select name="iDepartmentId" id="iDepartmentId" class="w-100 filter_change">
                                <option value="">Select Department</option>
                                @foreach ($department as $key => $departments)
                                <option value="{{ $departments->iDepartmentId }}"
                                    @if (isset($iDepartmentId)) {{ $departments->iDepartmentId == $iDepartmentId ? 'selected' : '' }} @endif>
                                        {{ $departments->vName }}</option>
                                        @endforeach
                                    </select>
                        </div>
                    </div>
                </div>
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr class="fw-bolder text-muted">
                                <th class="w-25px" data-orderable="false">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input class="form-check-input" type="checkbox" value="1" data-kt-check="true"
                                            data-kt-check-target=".widget-13-check" id="selectall" type="checkbox"
                                            name="selectall">
                                        <label for="selectall">&nbsp;</label>
                                    </div>
                                </th>
                                {{-- <th class="min-w-140px">
                                <a id="vName" class="sort" data-column="vName" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Doctor Name</span>
                                </a>
                            </th> --}}
                                <th class="min-w-140px">
                                    <a id="vFirstName" class="sort" data-column="vFirstName" data-order="ASC"
                                        href="javascript:;">
                                        <span class="text-muted fw-bold text-muted d-block fs-7"> Name</span>
                                    </a>
                                </th>
                                <th class="min-w-140px">
                                    <a id="vLastName" class="sort" data-column="vLastName" data-order="ASC"
                                        href="javascript:;">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Customer</span>
                                    </a>
                                </th>
                                <th class="min-w-120px">
                                    <a id="vEmail" class="sort" data-column="vEmail" data-order="ASC"
                                        href="javascript:;">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Email</span>
                                    </a>
                                </th>
                                <th class="min-w-120px">
                                    <a id="vUserName"  data-order="ASC"
                                        href="javascript:;">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Department</span>
                                    </a>
                                </th>
                                {{-- <th class="min-w-120px">
                                    <a id="vUserName" class="sort" data-column="vUserName" data-order="ASC"
                                        href="javascript:;">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">User Name</span>
                                    </a>
                                </th> --}}
                                <th class="min-w-120px">
                                    <a id="eStatus" class="sort" data-column="eStatus" data-order="ASC"
                                        href="javascript:;">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Status</span>
                                    </a>
                                </th>
                                <th class="min-w-100px">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader" width="250px"
                                height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- DOM - jQuery events table -->
    <!-- END: Content-->
@endsection

@section('custom-js')
    <script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
    <script>
        $('#iDepartmentId').selectize();
        $('#iCustomerId').selectize();
        $(document).ready(function() {
            $.ajax({
                url: "{{ route('admin.staff.ajaxListing') }}",
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $("#selectall").click(function() {
            if (this.checked) {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', true);
                });
            } else {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', false);
                });
            }
        });


        $("#keyword").keyup(function() {
            var keyword = $("#keyword").val();
            var iDoctorId = $("#iDoctorId").val();
            var iDepartmentId = $("#iDepartmentId").val();
            var iOfficeId = $("#iOfficeId").val();
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.staff.ajaxListing') }}",
                type: "get",
                data: {
                    keyword: keyword,
                    iDoctorId: iDoctorId,
                    iDepartmentId: iDepartmentId,
                    iOfficeId: iOfficeId,
                    action: 'search'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        
        $(document).on('click', '#delete_btn', function() {
            var id = [];

            $("input[name='Staff_ID[]']:checked").each(function() {
                id.push($(this).val());
            });

            var id = id.join(",");

            if (id.length == 0) {
                alert('Please select records.')
            } else {
                swal({
                        title: "Are you sure delete all Staff ?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "{{ route('admin.staff.ajaxListing') }}",
                                type: "get",
                                data: {
                                    id: id,
                                    action: 'multiple_delete'
                                },
                                success: function(response) {
                                    $("#table_record").html(response);
                                    $("#ajax-loader").hide();
                                    notification_error("Staff Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                                }
                            });
                        }
                    });
            }
        });
                
                    $(document).on('click', '#delete', function() {
                    swal({
                            title: "Are you sure delete this staff ?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                id = $(this).data("id");
                                var iDoctorId = $("#doctor").val();
                                var iDepartmentId = $("#iDepartmentId").val();
                                $("#ajax-loader").show();

                                $.ajax({
                                    url: "{{ route('admin.staff.ajaxListing') }}",
                                    type: "get",
                                    data: {
                                        id: id,
                                        iDepartmentId: iDepartmentId,
                                        action: 'delete'
                                    },
                                    success: function(response) {
                                        $("#table_record").html(response);
                                        $("#ajax-loader").hide();
                                        notification_error("Staff Deleted Successfully");
                                        setTimeout(function() {
                                            location.reload();
                                        }, 1000);
                                    }
                                });
                            }
                        })
                }); $(document).on('click', '.sort', function() {
                    column = $(this).data("column");
                    order = $(this).attr('data-order');
                    var iDepartmentId = $("#iDepartmentId").val();

                    if (order == "ASC") {
                        $(this).attr('data-order', 'DESC');
                    } else {
                        $(this).attr('data-order', 'ASC');
                    }

                    $("#ajax-loader").show();

                    $.ajax({
                        url: "{{ route('admin.staff.ajaxListing') }}",
                        type: "get",
                        data: {
                            column: column,
                            iDepartmentId: iDepartmentId,
                            order,
                            order,
                            action: 'sort'
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                        }
                    });
                });

                $(document).on('click', '.ajax_page', function() {
                    pages = $(this).data("pages");
                    var iDepartmentId = $("#iDepartmentId").val();
                    $("#table_record").html('');
                    $("#ajax-loader").show();

                    $.ajax({
                        url: "{{ route('admin.staff.ajaxListing') }}",
                        type: "get",
                        data: {
                            pages: pages,
                            iDepartmentId: iDepartmentId,
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                        }
                    });
                }); 
        
                $(document).on('change', '.filter_change', function() {
                    // var iDoctorId = $("#iDoctorId").val();
                    var iCustomerId = $("#iCustomerId").val();
                  
                    var iDepartmentId = $("#iDepartmentId").val();
                    $("#table_record").html('');
                    $("#ajax-loader").show();

                    $.ajax({
                        url: "{{ route('admin.staff.ajaxListing') }}",
                        type: "get",
                        data: {
                            iCustomerId: iCustomerId,
                            iDepartmentId: iDepartmentId,
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                        }
                    });
                }); 
                $(document).on('change', '#page_limit', function() {
                    limit_page = this.value;
                    $("#table_record").html('');
                    $("#ajax-loader").show();
                    url = "{{ route('admin.staff.ajaxListing') }}";

                    setTimeout(function() {
                        $.ajax({
                            url: url,
                            type: "get",
                            data: {
                                limit_page: limit_page
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                            }
                        });
                        // hideLoader();
                    }, 500);
                });
    </script>
@endsection
