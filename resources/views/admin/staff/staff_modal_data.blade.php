
  <div class="card">
    <h5>Email : {{$staffs->vEmail}}</h5>
    <div class="col-lg-12 mx-auto">   
      <form action="{{route('admin.staff.store')}}" id="frm_staff" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($staffs)) {{$staffs->iStaffId}} @endif">
        <input type="hidden" name="iLoginAdminId" value="@if(isset($staffs->iLoginAdminId)) {{$staffs->iLoginAdminId}} @endif">
      
     
        <input type="hidden" name="iCustomerId" value="@if(isset($staffs->iCustomerId)) {{$staffs->iCustomerId}} @endif">
 

        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>First name</label>
          <input type="text" class="form-control" id="vFirstName_H" name="vFirstName" placeholder="First name" value="@if(isset($staffs->vFirstName)){{$staffs->vFirstName}}@endif">
          <div class="text-danger" style="display: none;" id="vFirstName_error">Please enter first name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Last name</label>
          <input type="text" class="form-control" id="vLastName_H" name="vLastName" placeholder="Last name" value="@if(old('vLastName')!=''){{old('vLastName')}}@elseif(isset($staffs->vLastName)){{$staffs->vLastName}}@else{{old('vLastName')}}@endif">
          <div class="text-danger" style="display: none;" id="vLastName_error">Please enter last name</div>
        </div>
    
   
   
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Mobile</label>
          <input type="text" class="form-control" id="vMobile" name="vMobile" placeholder="Mobile" value="@if(old('vMobile')!=''){{old('vMobile')}}@elseif(isset($staffs->vMobile)){{$staffs->vMobile}}@else{{old('vMobile')}}@endif">
          <div class="text-danger" style="display: none;" id="vMobile_error">Please enter mobile</div>
        </div>
    
       

        
        <div class="col-12 align-self-end d-inline-block">
          <a type="submit" id="submit_staff_modal" class="btn submit-btn me-4">Submit</a>
          <button type="button" class="btn btn-secondary close-model" data-bs-dismiss="modal">Close</button>
          
        </div>
        <input type="hidden" name="iDepartmentId" id="iDepartmentId_H" value="{{$staffs->iDepartmentId}}">
        <input type="hidden" name="vEmail" id="vEmail_H" value="{{$staffs->vEmail}}">
      </form>
    </div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
    var mobile = [{ "mask": "### ### ####"}, { "mask": "### ### ####"}];
    $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
 
  var cellulor = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-####"}];
  $('#vCellulor').inputmask({ 
      mask: cellulor, 
      greedy: false, 
      definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
  });
  $('.close-model').click(function(){
            $('#UserDetailModal').hide();
            $('#UserDetailModal').removeClass('show');
       });

    $('#eStatus').select2({
    width: '100%',
    placeholder: "Select status",
    allowClear: true
});
</script>

