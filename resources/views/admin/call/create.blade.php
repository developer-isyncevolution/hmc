@extends('layouts.admin.index')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<div class="main-panel">
  <div class="page-title text-center">
    <h3>
      {{isset($calls->iCallId) ? 'Edit' : 'Add'}} call
    </h3>
</div>
  <div class="card">
    <div class="col-lg-12 mx-auto">         
        <form action="{{route('admin.call.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($calls)) {{$calls->iCallId}} @endif">
            @php $date = date('Y-m-d'); @endphp
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label >Date</label>
                <input type="date" class="form-control" id="dDate" name="dDate" placeholder="Name" value="@if(old('dDate')!=''){{old('dDate')}}@elseif(isset($calls->dDate)){{$calls->dDate}}@else{{$date}}@endif">
                <div class="text-danger" style="display: none;" id="dDate_error">Please enter name</div>
            </div>
            @php $time = date('H:i');@endphp
            
            <div class="col-xxl-4 col-lg-6 col-md-12">
              <label >Time</label>
              <input type="time" class="form-control" id="tTime" name="tTime" placeholder="Name" value="@if(old('tTime')!=''){{old('tTime')}}@elseif(isset($calls->tTime)){{$calls->tTime}}@else{{$time}}@endif">
              <div class="text-danger" style="display: none;" id="tTime_error">Please enter name</div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Call Taken</label>
                <input type="text" class="form-control" value="{{$username}}" readonly>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Person Attended</label>
                <input type="text" class="form-control" id="vName" name="vName" placeholder="Person attended" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($calls->vName)){{$calls->vName}}@else{{old('vName')}}@endif">
                <div class="text-danger" style="display: none;" id="vName_error">Please enter person attended</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
              <label>Reason</label>
              <div id="toolbar-container"></div>
                <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Reason">@if(old('tDescription')!=''){{old('tDescription')}}@elseif(isset($calls->tDescription)){{$calls->tDescription}}@else{{old('tDescription')}}@endif</textarea>
              <div id="tDescription_error" class="text-danger" style="display: none;">Please enter reason </div>
           </div> 
            {{-- <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Status</label>
                <select id="eStatus" name="eStatus" class="form-control">
                    <option value="Active" @if(isset($calls)) @if($calls->eStatus == 'Active') selected @endif @endif>Active</option>
                    <option value="Inactive" @if(isset($calls)) @if($calls->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
                </select>
                <div class="mt-1">
                    @error('eStatus')
                        <code>{{ $message }}</code>
                    @enderror
                </div>
            </div>        --}}
            <div class="col-12 align-self-end d-inline-block ">
                <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
                <a href="{{route('admin.call')}}" class="btn back-btn me-2">Back</a>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
   <script>
    //  $('#eStatus').select2({
    //     width: '100%',
    //     placeholder: "Select customer",
    //     allowClear: true
    // });
  $(document).on('click','#submit',function()
  {
    iCallId = $("#id").val();
    dDate           = $("#dDate").val();
    iQuantity       = $("#iQuantity").val();
    vName       = $("#vName").val();
    tDescription  = $("#tDescription").val();

    var error = false;

    if(dDate.length == 0){
      $("#dDate_error").show();
      error = true;
    } else {
      $("#dDate_error").hide();
    }
    
    if(vName.length == 0){
      $("#vName_error").show();
      error = true;
    } else {
      $("#vName_error").hide();
    }
    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
    </script>
@endsection
