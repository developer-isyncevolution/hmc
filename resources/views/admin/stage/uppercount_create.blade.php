@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
  <div class="card">
    <div class="col-lg-12 mx-auto">
      <form action="{{route('admin.stage.uppercount_store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($uppercounts)) {{$uppercounts->iFeesId}} @endif">
        <input type="hidden" name="iProductStageId" value="@if(isset($productstage)){{$productstage->iProductStageId}}@endif">

        {{-- <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Stage price</label>
          <input type="text" class="form-control" id="iStagePrice" readonly name="iStagePrice" placeholder="Stage price" value="@if(isset($uppercounts->iStagePrice)){{$uppercounts->iStagePrice}}@elseif(isset($productstage->vPrice)){{$productstage->vPrice}}@endif">
        </div> --}}
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Fee Percentage</label>
          <input type="number" class="form-control" id="vFee" name="vFee" placeholder="Fee Percentage" value="@if(old('vFee')!=''){{old('vFee')}}@elseif(isset($uppercounts->vFee)){{$uppercounts->vFee}}@else{{old('vFee')}}@endif">
          <div class="text-danger" style="display: none;" id="vFee_error">Please enter Fee Percentage</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Working Time</label>
          <select id="iWorkingTime" name="iWorkingTime" class="form-control">
            
          @for($i=1; $i<=$productstage->iPickUpDay + $productstage->iProcessDay+$productstage->iDeliverDay; $i++)
            <option value="{{$i}}" @if(isset($uppercounts)) @if($uppercounts->iWorkingTime == $i) selected @endif @endif>{{$i}} Days</option>
            @endfor
          </select>
          <div class="text-danger" style="display: none;" id="iWorkingTime_error">Please enter Working Time</div>
        </div>
        {{-- <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Rush Fee</label>
          <input type="text" readonly class="form-control" id="vRushFee" name="vRushFee" placeholder="Rush Fee" value="@if(old('vRushFee')!=''){{old('vRushFee')}}@elseif(isset($uppercounts->vRushFee)){{$uppercounts->vRushFee}}@else{{old('vRushFee')}}@endif">
          <div class="text-danger" style="display: none;" id="vRushFee_error">Please enter Rush Fee</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Total Rush</label>
          <input type="text" readonly class="form-control" id="vTotalRush" name="vTotalRush" placeholder="Total Rush" value="@if(old('vTotalRush')!=''){{old('vTotalRush')}}@elseif(isset($uppercounts->vTotalRush)){{$uppercounts->vTotalRush}}@else{{old('vTotalRush')}}@endif">
          <div class="text-danger" style="display: none;" id="vTotalRush_error">Please enter Total Rush</div>
        </div> --}}
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Status</label>
          <select id="eStatus" name="eStatus" class="form-control">
            <option value="Active" @if(isset($uppercounts)) @if($uppercounts->eStatus == 'Active') selected @endif @endif>Active</option>
            <option value="Inactive" @if(isset($uppercounts)) @if($uppercounts->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
            
          </select>
        </div>
        <div class="col-12 align-self-end d-inline-block">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.rushdate')}}" class="btn back-btn">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
   $('#eStatus').selectize();
  $('#iWorkingTime').selectize();
  
  $(document).on('click', '#submit', function() {
    id          = $("#id").val();
    vFee        = $("#vFee").val();
    vRushFee        = $("#vRushFee").val();
    vTotalRush        = $("#vTotalRush").val();
    iWorkingTime        = $("#iWorkingTime").val();
  
    var error = false;

    if (vFee.length == 0) {
      $("#vFee_error").show();
      error = true;
    } else {
      $("#vFee_error").hide();
    }
    // if (vRushFee.length == 0) {
    //   $("#vRushFee_error").show();
    //   error = true;
    // } else {
    //   $("#vRushFee_error").hide();
    // }
    // if (vTotalRush.length == 0) {
    //   $("#vTotalRush_error").show();
    //   error = true;
    // } else {
    //   $("#vTotalRush_error").hide();
    // }
    if (iWorkingTime.length == 0) {
      $("#iWorkingTime_error").show();
      error = true;
    } else {
      $("#iWorkingTime_error").hide();
    }

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
function validateEmail(vEmail) 
{
  var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  if (filter.test(vEmail)) {
    return true;
  }
  else {
    return false;
  }
}
$(document).ready(function() {
  // $('#vFee').on("keyup", function(){
    // var fee  = $('#vFee').val();
    // var stageprice  = $('#iStagePrice').val();
    // var rushfee = ((stageprice*fee)/100);
    // var totalrush =parseInt(stageprice)+parseInt(rushfee);
    // $('#vRushFee').val(rushfee);
    // $('#vTotalRush').val(totalrush);

  // }); 
    var mobile = [{ "mask": "### ### ####"}, { "mask": "### ### ####"}];
    $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
   })
  $(document).ready(function() {
  var cellulor = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-####"}];
  $('#vCellulor').inputmask({ 
      mask: cellulor, 
      greedy: false, 
      definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
  })
</script>
@endsection