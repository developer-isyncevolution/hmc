
@if(count($data) > 0)
@foreach($data as $key => $value)
<tr>
	<td class="text-center">
		<div class="form-check form-check-sm form-check-custom form-check-solid">
			<input id="GumBrand_ID_{{$value->iGumBrandId}}" type="checkbox" name="GumBrand_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iGumBrandId}}">
			<label for="GumBrand_ID_{{$value->iGumBrandId}}">&nbsp;</label>
		</div>
	</td>
	<td>{{$value->vName}}</td>
	<td>{{$value->iSequence}}</td>

	<td>
		<span class="badge badge-light-success">{{$value->eStatus}}</span>
	</td>

	<td >
		<div class="d-inline-flex align-items-center">
			@if(\App\Libraries\General::check_permission('','eEdit','gum_shades_brand') == 'true')
			<a href="{{route('admin.stage.gumbrand_edit',$value->iGumBrandId)}}" class="btn-icon me-4">
				<i class="fad fa-pencil"></i>
			</a>
			@endif
			@if(\App\Libraries\General::check_permission('','eDelete','gum_shades_brand') == 'true')
			<a href="javascript:;" id="delete" data-id="{{$value->iGumBrandId}}" class="btn-icon delete-icon delete_permission me-4">
				<i class="fad fa-trash-alt"></i>
			</a>
			@endif
	</div>
	</td>

</tr>
@endforeach
<tr>
	<td colspan="1" class="text-center border-right-0">
		@if(\App\Libraries\General::check_permission('','eDelete','gum_shades_brand') == 'true')
		<a href="javascript:;" title="Multiple delete"  id="delete_btn" data-id="{{$value->iGumBrandId}}" class="btn-icon  delete_permission delete-icon">
			<i class="fad fa-trash-alt"></i>
		</a>
		@endif
	</td>
	{{-- <td colspan="3" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
	<td class="border-0" colspan="2">
		<div class="d-flex">
			{!! $paging !!}
		</div>
	</td>
	<td colspan="3"  class="text-end border-left-0">
		{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}}  --}}
		<select class="w-100px show-drop" id ="page_limit">
			<option @if ((isset(Request()->limit_page) && Request()->limit_page == 10) || (isset($limit) && $limit ==10))
				selected
				@endif value="10">10</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 20) || (isset($limit) && $limit ==20))
				selected
				@endif value="20">20</option>
				<option  @if ((isset(Request()->limit_page) && Request()->limit_page == 50) || (isset($limit) && $limit ==50) )
					selected 
				@endif value="50">50</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 100) || (isset($limit) && $limit ==100))
					selected
				@endif value="100">100</option>
		</select>
	</td>
</tr>
@else
<tr class="text-center">
	<td colspan="15">No Record Found</td>
</tr>
@endif