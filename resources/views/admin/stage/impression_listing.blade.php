@extends('layouts.admin.index')
@section('content')
<!-- BEGIN: Content-->
<!-- DOM - jQuery events table -->
<div class="main-panel">
    <div class="page-title text-center">
        <h3>
            Impressions
        </h3>
    </div>
    <div class="col-lg-12 mx-auto">
        <div class="listing-page">

            <div class="card mb-5 mb-xl-4">
                <div class="card-header border-0">
                    <ul class="c-nav-tab">
                        @if(\App\Libraries\General::check_permission('','eRead','extractions') == 'true')
                        <li class="">
                            <a href="{{route('admin.stage')}}" class=" add-btn btn mx-2">
                                Extractions
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','impressions') == 'true')
                        <li class="">
                            <a href="{{route('admin.impression')}}" class="active btn add-btn  mx-2">
                                Impressions
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','tooth_shades') == 'true')
                        <li>
                            <a href="{{route('admin.toothshadesbrand')}}" class="btn add-btn mx-2">
                                Tooth Shades
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','gum_shades') == 'true')
                        <li>
                            <a href="{{route('admin.gumbrand')}}" class="btn add-btn mx-2">
                                Gum Shades
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','stage_notes') == 'true')
                        <li>
                            <a href="{{route('admin.notes')}}" class="btn add-btn mx-2">
                                Stage Notes
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','rush_dates') == 'true')
                        <li>
                            <a href="{{route('admin.rushdate')}}" class="btn add-btn mx-2">
                                Rush Dates
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
  

                <div class="row">
                  
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-mid">
                                @if(\App\Libraries\General::check_permission('','eCreate','impressions') == 'true')
                                <ul class="d-flex list-unstyled">
                                    <li>
                                        <a href="{{route('admin.stage.impression_create')}}" class="btn add-btn create_permission">
                                            <i class="fa fa-plus"></i> Add
                                        </a>
                                    </li>
                                </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if(\App\Libraries\General::admin_info()['customerType']=='Super Admin')
                    
                    <div class="col-lg-4 mx-auto">
                        <select name="iCustomerId" id="iCustomerId" class="w-100 filter_change">
                            <option value="">Select Cutomer</option>
                            @foreach ($customer as $key => $customers)
                                <option value="{{ $customers->iCustomerId }}">
                                    {{ $customers->vOfficeName }}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    <div class="col-lg-4 mx-auto">
                    <div class="custome-serce-box">
                                <input type="text" class="form-control" id="keyword" name="search" class="search" placeholder="Search">
                                <span class="search-btn"><i class="fas fa-search"></i></span>
                            </div>
                    </div>
                    <div class="col-lg-4 mx-auto">
                    </div>
                </div>






            </div>
            <div class="table-data table-responsive">
                <table class="table">
                    <thead>
                        <tr class="fw-bolder text-muted">
                            <th class="w-25px text-center" data-orderable="false">
                                <div class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" id="selectall" type="checkbox" name="selectall">
                                    <label for="selectall">&nbsp;</label>
                                </div>
                            </th>
                            <th class="min-w-100px">
                                <a id="vName" class="sort" data-column="vName" data-order="ASC" href="javascript:;">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Name</span>
                                </a>
                            </th>
                            <th class="min-w-100px">
                                <a id="vCode" class="sort" data-column="vCode" data-order="ASC" href="javascript:;">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Code</span>
                                </a>
                            </th>
                            <th class="min-w-100px">
                                <a id="iSequence" class="sort" data-column="iSequence" data-order="ASC" href="javascript:;">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Sequence</span>
                                </a>
                            </th>
                            <th class="min-w-100px">
                                <a id="eStatus" class="sort" data-column="eStatus" data-order="ASC" href="javascript:;">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Status</span>
                                </a>
                            </th>
                            <th class="max-w-100px">
                                <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="table_record">
                    </tbody>
                </table>
                <div class="text-center loaderimg">
                    <div class="loaderinner">
                        <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- DOM - jQuery events table -->
<!-- END: Content-->
@endsection

@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>
<script>
    $('#iCustomerId').selectize();
    $(document).ready(function() {

        $.ajax({
            url: "{{route('admin.stage.ImpressionajaxListing')}}",
            type: "get",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('change', '.filter_change', function() {
        // var iDoctorId = $("#iDoctorId").val();
        var iCustomerId = $("#iCustomerId").val();
        $("#table_record").html('');
        $("#ajax-loader").show();

        $.ajax({
            url: "{{ route('admin.stage.ImpressionajaxListing') }}",
            type: "get",
            data: {
                iCustomerId: iCustomerId
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    }); 

    $("#selectall").click(function() {
        if (this.checked) {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', true);
            });
        } else {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', false);
            });
        }
    });


    $("#keyword").keyup(function() {
        var keyword = $("#keyword").val();

        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.stage.ImpressionajaxListing')}}",
            type: "get",
            data: {
                keyword: keyword,
                action: 'search'
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '#delete_btn', function() {
        var id = [];

        $("input[name='Impressiona_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                    title: "Are you sure delete all Impression ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{route('admin.stage.ImpressionajaxListing')}}",
                            type: "get",
                            data: {
                                id: id,
                                action: 'multiple_delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("Impression Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                            }
                        });
                    }
                });
        }
    });
    $(document).on('click', '#delete', function() {
        swal({
                title: "Are you sure delete this Impression ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");

                    $("#ajax-loader").show();

                    $.ajax({
                        url: "{{route('admin.stage.ImpressionajaxListing')}}",
                        type: "get",
                        data: {
                            id: id,
                            action: 'delete'
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                            notification_error("Impression Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                        }
                    });
                }
            })
    });
    $(document).on('click', '.sort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');
        iCustomerId = $('#iCustomerId').val();
        keyword = $('#keyword').val();
       
       
        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.stage.ImpressionajaxListing')}}",
            type: "get",
            data: {
                column: column,
                order,
                order,
                action: 'sort',
                keyword:keyword,
                iCustomerId:iCustomerId,
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '.ajax_page', function() {
        pages = $(this).data("pages");

        $("#table_record").html('');
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.stage.ImpressionajaxListing')}}",
            type: "get",
            data: {
                pages: pages
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#page_limit', function() {
        limit_page = this.value;
        $("#table_record").html('');
        $("#ajax-loader").show();
        url = "{{route('admin.stage.ImpressionajaxListing')}}";

        setTimeout(function() {
            $.ajax({
                url: url,
                type: "get",
                data: {
                    limit_page: limit_page
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            hideLoader();
        }, 500);
    });
</script>
@endsection