@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
    <div class="page-title text-center">
        <h3>
            Casepan Number
        </h3>
    </div>
    <div class="col-lg-12 mx-auto">
        <div class="card mb-5 mb-xl-4">

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-mid">
                            <ul class="d-flex list-unstyled">
                                @if(\App\Libraries\General::check_permission('','eCreate') == true)
                                @if(isset($iCasepanId))
                                <li class=" me-2">
                                    <a href="{{route('admin.casepannumber.create',$iCasepanId)}}" class="btn add-btn create_permission me-2">
                                        <i class="fa fa-plus"></i> Add
                                    </a>
                                </li>
                                @else
                                <li class=" me-2">
                                    <a href="{{route('admin.casepannumber.create')}}" class="btn add-btn create_permission me-2">
                                        <i class="fa fa-plus"></i> Add
                                    </a>
                                </li>
                                @endif
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
             

                <div class="col-lg-6 mx-auto">
                    <select name="iCasepanId" id="iCasepanId">
                        <option>Select Casepan</option>
                        @foreach($casepan as $key => $casepans)
                        <option value="{{$casepans->iCasepanId}}" @if(isset($iCasepanId)){{$casepans->iCasepanId == $iCasepanId  ? 'selected' : ''}} @endif>{{$casepans->vName}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-6 mx-auto">
                    <div class="custome-serce-box">
                        <input type="text" class="form-control" id="keyword" name="search" placeholder="Search">
                        <span class="search-btn"><i class="fas fa-search"></i></span>
                    </div>
                </div>
            </div>

        </div>



        @if(isset($iCasepanId))
        <input type="hidden" name="casepan" id="casepan" value="{{$iCasepanId}}">
        @endisset
        <div class="listing-page">
            <div class="table-data table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="w-25px" data-orderable="false">
                                <div class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input id="selectall" type="checkbox" name="selectall" class="form-check-input">
                                    <label for="selectall">&nbsp;</label>
                                </div>
                            </th>
                            <th scope="col" class="min-w-350px"> <span class="text-muted fw-bold text-muted d-block fs-7"> Casepan </span> </a></th>
                            <th scope="col" class="min-w-350px"> <span class="text-muted fw-bold text-muted d-block fs-7"> Available </span> </a></th>
                            <th scope="col" class="max-w-100px"><span class="text-muted fw-bold text-muted d-block fs-7"> Action </span></th>
                        </tr>
                    </thead>
                    <tbody id="table_record">
                    </tbody>
                </table>
                <div class="text-center loaderimg">
                    <div class="loaderinner">
                        <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom-css')
@endsection

@section('custom-js')

<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
   $('#eStatus').selectize();
   $('#iCasepanId').selectize();
    $(document).ready(function() {
        var iCasepanId = $("#casepan").val();
        $.ajax({
            url: "{{route('admin.casepannumber.ajaxListing')}}",
            type: "get",
            data: {
                iCasepanId: iCasepanId,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $("#selectall").click(function() {
        if (this.checked) {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', true);
            });
        } else {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', false);
            });
        }
    });

    $(document).on('click', '#delete_btn', function() {
        var id = [];

        $("input[name='CasepanNumber_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");
        var iCasepanId = $("#casepan").val();
        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                    title: "Are you sure delete all casepannumber ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{route('admin.casepannumber.ajaxListing')}}",
                            type: "get",
                            data: {
                                id: id,
                                iCasepanId: iCasepanId,
                                action: 'multiple_delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("Casepan Number Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                            }
                        });
                    }
                });
        }
    });

    $("#keyword").keyup(function() {
        var keyword = $("#keyword").val();
        var iCasepanId = $("#casepan").val();
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.casepannumber.ajaxListing')}}",
            type: "get",
            data: {
                keyword: keyword,
                iCasepanId: iCasepanId,
                action: 'search'
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '#delete', function() {
        swal({
                title: "Are you sure delete this casepannumber ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");
                    var iCasepanId = $("#casepan").val();

                    $("#ajax-loader").show();

                    $.ajax({
                        url: "{{route('admin.casepannumber.ajaxListing')}}",
                        type: "get",
                        data: {
                            id: id,
                            iCasepanId: iCasepanId,
                            action: 'delete'
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                            notification_error("Casepan Number Deleted Successfully");
                                setTimeout(function() {
                                   location.reload();
                                }, 1000);
                        }
                    });
                }
            })
    });
    $(document).on('click', '.sort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');
        var iCasepanId = $("#casepan").val();

        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.casepannumber.ajaxListing')}}",
            type: "get",
            data: {
                column: column,
                order,
                order,
                iCasepanId: iCasepanId,
                action: 'sort'
            },
            success: function(response) {
                console.log(response);
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '.ajax_page', function() {
        pages = $(this).data("pages");
        var iCasepanId = $("#casepan").val();
        $("#table_record").html('');
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.casepannumber.ajaxListing')}}",
            type: "get",
            data: {
                pages: pages,
                iCasepanId: iCasepanId,
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#iCasepanId', function() {
        var iCasepanId = $("#iCasepanId").val();

        $("#table_record").html('');
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.casepannumber.ajaxListing')}}",
            type: "get",
            data: {
                iCasepanId: iCasepanId
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#page_limit', function() {
        var iCasepanId = $("#casepan").val();
        limit_page = this.value;
        $("#table_record").html('');
        $("#ajax-loader").show();
        url = "{{route('admin.casepannumber.ajaxListing')}}";

        setTimeout(function() {
            $.ajax({
                url: url,
                type: "get",
                data: {
                    limit_page: limit_page,
                    iCasepanId: iCasepanId
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            hideLoader();
        }, 500);
    });
</script>
@endsection