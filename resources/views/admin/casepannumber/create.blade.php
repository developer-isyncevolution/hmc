@extends('layouts.admin.index')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<div class="main-panel">
<div class="page-title text-center">
        <h3>
        {{isset($casepannumbers->iCasepanNumberId) ? 'Edit' : 'Add'}} Casepan Number
        </h3>
    </div>
  <div class="card">
    <div class="col-lg-12 mx-auto">       
        <form action="{{route('admin.casepannumber.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($casepannumbers)) {{$casepannumbers->iCasepanNumberId}} @endif">
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label >Name</label>
                <select name="iCasepanId" id="iCasepanId" class="form-control">
                  <option value="none">Select casepan</option>
                  <option id="addnew" value="addnew">Add New</option>
                  @foreach($casepan as $key => $casepans)
                  <option value="{{$casepans->iCasepanId}}" style="background-color:{{$casepans->vColor}}" @if(isset($iCasepanId)){{$casepans->iCasepanId == $iCasepanId  ? 'selected' : ''}} @endif><td style="background-color:{{ $casepans->vColor }}">{{$casepans->vName}}</option>
                  @endforeach
                </select>
                <div class="text-danger" style="display: none;" id="iCasepanId_error">Please select casepan</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Number</label>
                <input type="text" class="form-control" id="vNumber" name="vNumber" placeholder="Number" value="@if(old('vNumber')!=''){{old('vNumber')}}@elseif(isset($casepannumbers->vNumber)){{$casepannumbers->vNumber}}@else{{old('vNumber')}}@endif">
                <div class="text-danger" style="display: none;" id="vNumber_error">Please enter number</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Status</label>
                <select id="eStatus" name="eStatus" class="form-control">
                    <option value="Active" @if(isset($casepannumbers)) @if($casepannumbers->eStatus == 'Active') selected @endif @endif>Active</option>
                    <option value="Inactive" @if(isset($casepannumbers)) @if($casepannumbers->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
                </select>
                <div class="mt-1">
                    @error('eStatus')
                        <code>{{ $message }}</code>
                    @enderror
                </div>
            </div>       
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Available</label>
                <select id="eUse" name="eUse" class="form-control">
                  <option value="No" @if(isset($casepannumbers)) @if($casepannumbers->eUse == 'No') selected @endif @endif>Yes</option>
                    <option value="Yes" @if(isset($casepannumbers)) @if($casepannumbers->eUse == 'Yes') selected @endif @endif>No</option>
                </select>
                
            </div>       
            <div class="col-12 align-self-end d-inline-block ">
                <a type="submit" id="submit" data-name="Action_Not_Back" class="btn submit submit-btn me-2">Submit</a>
                <a href="{{route('admin.casepannumber',$iCasepanId)}}" class="btn back-btn me-2">Back</a>
                &nbsp;&nbsp;&nbsp;
                <input type="hidden" id="Action_back" name="Action_back" value="No">
                <a type="submit" data-name="Action_back"  class="btn submit submit-btn me-2">Add new case pan #
                </a>
            </div>
        </form>

        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div  class="modal-dialog modal-xl">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Casepan</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <form action="{{route('admin.casepan.store_model_casepan')}}" id="frm_casepen" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="id" value="@if(isset($casepans)) {{$casepans->iCasepanId}} @endif">
                  <div class="col-xxl-4 col-lg-6 col-md-12">
                      <label >Name</label>
                      <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="">
                      <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
                  </div>
                  <div class="col-xxl-4 col-lg-6 col-md-12">
                    <input type="radio"  class="color_radio" id="withcolor" name="color" value="withcolor">
                      <label for="withcolor">With Color</label><br>
                    <input type="radio"  class="color_radio" id="withoutcolor" name="color" value="withoutcolor">
                      <label for="withoutcolor">Without Color</label><br>
                  </div>
                  @if(isset($casepans->vColor))
                  <div class="col-xxl-4 col-lg-6 col-md-12" id="color_select">
                    <label>Color</label>
                    <input type="text" class="form-control" id="vColor" name="vColor" placeholder="Color" value="">
                  </div>
                  @else 
                  <div class="col-xxl-4 col-lg-6 col-md-12" id="color_select"  style="display: none" >
                    <label>Color</label>
                    <input type="text" class="form-control" id="vColor" name="vColor" placeholder="Color" value="">
                  </div>
                  @endif
                  <div class="col-xxl-4 col-lg-6 col-md-12">
                      <label>Quantity</label>
                      <input type="text" class="form-control" id="iQuantity" name="iQuantity" placeholder="Quantity" value="">
                      <div class="text-danger" style="display: none;" id="iQuantity_error">Please enter quantity</div>
                  </div>
                  <div class="col-xxl-4 col-lg-6 col-md-12">
                      <label>Sequence</label>
                      <input type="text" class="form-control" id="iSequence" name="iSequence" placeholder="Sequence" value="">
                      <div class="text-danger" style="display: none;" id="iSequence_error">Please enter sequence</div>
                  </div>
                  
                  
                </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <a type="submit" id="submit_casepan" class="btn submit-btn me-2">Add</a>
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
  
   $('#eStatus').selectize();
   $('#iCasepanId').selectize();
   $('#eStatus').selectize();

  //  Casepan Add start
    $(document).ready(function() {

      $('input[name=color]').prop('checked', true);

    });
    $(document).on('click', '.color_radio', function() {
      var color = $('input[name="color"]:checked').val();
      if(color == "withcolor"){
        $("#color_select").show();
        error = true;
      } else {
        $("#color_select").hide();
      }
    });
    $(document).on('click','#submit_casepan',function()
    {
      iCasepanId = $("#id").val();
      vName           = $("#vName").val();
      iQuantity       = $("#iQuantity").val();
      iSequence       = $("#iSequence").val();

      var error = false;

      if(vName.length == 0){
        $("#vName_error").show();
        error = true;
      } else {
        $("#vName_error").hide();
      }
      if(iQuantity.length == 0){
        $("#iQuantity_error").show();
        error = true;
      } else {
        $("#iQuantity_error").hide();
      }
      if(iSequence.length == 0){
        $("#iSequence_error").show();
        error = true;
      } else {
        $("#iSequence_error").hide();
      }
      setTimeout(function(){
        if(error == true){
          return false;
        } else {
          $("#frm_casepen").submit();
          return true;
        }
      }, 1000);

    });
// Casepan Add end

  $('#vColor').colorpicker();

   $(document).on('change', '#iCasepanId', function() {
    iCasepanId          = $("#iCasepanId").val();
    if (iCasepanId== "addnew") {
      $("#exampleModal").modal("show");
    }
    });
  $(document).on('click','.submit',function()
  {
    if($(this).data("name") =='Action_back')
    {
        $('#Action_back').val('Yes');  
    }
    iCasepanId = $("#iCasepanId").val();
    vName           = $("#vName").val();
    vNumber       = $("#vNumber").val();

    var error = false;

    if(vNumber.length == 0){
      $("#vNumber_error").show();
      error = true;
    } else {
      $("#vNumber_error").hide();
    }
    if(iCasepanId == "none"){
      $("#iCasepanId_error").show();
      error = true;
    } else {
      $("#iCasepanId_error").hide();
    }
    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
    </script>
@endsection
