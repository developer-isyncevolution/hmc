@php
$i = 1;
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Billing Statement</title>


</head>
<style>

</style>

<body style=" font-size: 14px;color: rgb(158, 157, 157);font-family:
        sans-serif;">
    <table style="max-width: 1000px;width: 100%; margin: 0 auto;
            border-collapse: collapse;">

        <tr>
            <td style="width:35%;">
                <div style="width: 150px; height:60px">
                    @if (isset($office->vImage) && !empty($office->vImage))
                        <img src="{{ public_path('uploads/customer/' . $office->vImage) }}" alt="logo"
                            style="object-fit: contain;width:100%;height:100%;">
                    @endif
                </div>
            </td>
            <td style="width:30%;" rowspan="2">
                        <div style="font-weight:
                                bold;color:black;font-size:14px; text-align:center;">
                            {{isset($duration_month)&& !empty($duration_month) ?$duration_month:''}}
                        </div>
            </td>
            <td style="width:35%; ">
                <div style="width: 150px; height:60px">
                    @if (isset($labs->vImage) && !empty($labs->vImage))
                        <img src="{{ public_path('uploads/customer/' . $labs->vImage) }}" alt="logo"
                            style="object-fit: contain;width:100%;height:100%;">
                    @endif
                </div>
            </td>
        </tr>
        <tr>
            <td style="color:black;font-size:12px;padding: 10px;">
                <div><span style="font-weight: bold;">Bill From :
                    </span>{{ isset($office->vOfficeName) ? $office->vOfficeName : '--' }}</div>
                <div style="margin-top: 10px;"><span
                        style="font-weight:
                            bold;">Address:</span>
                    {{ isset($office->vAddress) ? $office->vAddress : '--' }}{{ isset($office->vZipCode) ? ', ' . $office->vZipCode : '' }}
                </div>
            </td>
            <td colspan="2" style="color:black;font-size:12px;padding:
                    10px;">
                <div><span style="font-weight: bold;">Bill To :
                    </span>{{ isset($labs->vOfficeName) ? $labs->vOfficeName : '--' }}</div>
                <div style="margin-top: 10px;"><span
                        style="font-weight:
                            bold;">Address:</span>
                    {{ isset($labs->vAddress) ? $labs->vAddress : '--' }}{{ isset($labs->vZipCode) ? ', ' . $labs->vZipCode : '' }}
                </div>
            </td>

        </tr>
    </table>
    <table style="max-width: 1000px;width: 100%; margin: 0 auto;
            border-collapse: collapse;">

    </table>
    <table
        style="max-width: 1000px;width: 100%; margin: 0 auto;font-size:
            10px; margin-top:
            20px; border-collapse: collapse;border: 1px solid #383838;">
        <tr>
            <td style="padding: 10px;border: 1px solid #383838;">
                <span style="font-weight: bold;color:black;">Slip# - Patient</span>
            </td>
            <td style="padding: 10px;border: 1px solid #383838;">
                <span style="font-weight: bold;color:black;">
                    Jaw - Grade
                </span>
            </td>
            <td style="padding: 10px;border: 1px solid #383838;">
                <span style="font-weight: bold;color:black;">
                    Product - Stage
                </span>
            </td>
            <td style="padding: 10px;border: 1px solid #383838;">
                <span style="font-weight: bold;color:black;">
                    Total
                </span>
            </td>
            <td style="padding: 10px;border: 1px solid #383838;">
                <span style="font-weight: bold;color:black;">
                    Addon * Qty
                </span>
            </td>
            <td style="padding: 10px;border: 1px solid #383838;">
                <span style="font-weight: bold;color:black;">
                    Total
                </span>
            </td>
            <td style="padding: 10px;border: 1px solid #383838;">
                <span style="font-weight: bold;color:black;">
                    Due Date
                </span>
            </td>
            <td style="padding: 10px;border: 1px solid #383838;">
                <span style="font-weight: bold;color:black;">
                    Total
                </span>
            </td>

        </tr>
        @foreach ($data as $key => $value)
        @php
            $billing_product = count($value->billing_product);
            $product_2_row = 0;
            $product_1_row = 0;
        @endphp
        
        @if($billing_product == 1)
            @php 
                $product_1_row = count($value->billing_product[0]->billing_addons);
                $total_row = $product_1_row;
            @endphp
        @elseif($billing_product == 2)
            @php 
                $product_1_row = count($value->billing_product[0]->billing_addons);
                $product_2_row = count($value->billing_product[1]->billing_addons);
                $total_row = $product_1_row+$product_2_row;
            @endphp
        @endif
            <tr style="margin: 0px;padding:0px;">
                <td style="padding: 10px;border: 1px solid #383838;">
                    <span style="font-weight: normal;color:black;">
                        {{ $value->vSlipNumber }}</span><br><span
                        style="font-weight: normal;color:black;">{{ $value->vPatientName }}
                    </span>
                </td>
               
                @if (isset($value->billing_product) && !empty($value->billing_product))
                    <td style="padding: 0px;border: 1px solid #383838;">

                        <table style="width: 100% !important;white-space: nowrap !important; border-spacing: 0px !important;">
                            @foreach ($value->billing_product as $key => $billing_product_val)
                            @if($billing_product_val->eType =='Upper' && $product_1_row != 0)
                                @for ($i = 0; $i < $product_1_row; $i++)
                                    <tr>
                                        <td @if ($i == $product_1_row-1 && $product_2_row != 0)
                                        style="border-bottom: 1px solid black; padding:0px;margin:0px;"@endif>
                                            @if($i == 0)
                                                <span style="color:black">
                                                    {{ $billing_product_val->eType}}
                                                    {{isset($billing_product_val->vGradeName)?' - '.$billing_product_val->vGradeName:''}}
                                                </span>
                                            @else
                                                <span style="color:black;visibility: hidden;">------</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endfor
                            
                            @endif
                            @if($billing_product_val->eType =='Lower' && $product_2_row !=0)
                                @for ($j = 0; $j < $product_2_row; $j++)
                                    <tr>
                                        <td >
                                            @if($j == 0)
                                            <span style="color:black">
                                                {{ $billing_product_val->eType}}
                                                {{isset($billing_product_val->vGradeName)?' - '.$billing_product_val->vGradeName:''}}
                                            </span>    
                                            @else
                                                <span style="color:black; visibility: hidden;">------</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endfor
                                @else
                            @endif

                            @if($product_1_row ==0 && $product_2_row ==0)
                                <span style="color:black">
                                    {{ $billing_product_val->eType}}
                                    {{isset($billing_product_val->vGradeName)?' - '.$billing_product_val->vGradeName:''}}
                                </span> 
                            @endif
                                
                            @endforeach
                        </table>
                    </td>
                @endif
                @if (isset($value->billing_product) && !empty($value->billing_product))
                    <td style="border: 1px solid #383838;padding: 0px;">
                        <table style="width: 100% !important;white-space: nowrap !important; border-spacing: 0px !important;">
                            @foreach ($value->billing_product as $key => $billing_product_val)
                            @if($billing_product_val->eType =='Upper' && $product_1_row != 0)
                                
                                @for ($i = 0; $i < $product_1_row; $i++)
                                    <tr>
                                        <td @if ($i == $product_1_row-1 && $product_2_row != 0)
                                        style="border-bottom: 1px solid black; padding:0px;margin:0px;"@endif>
                                            @if($i == 0)
                                                <span style="color:black">
                                                    {{ $billing_product_val->vProductName . ' - ' . $billing_product_val->vStageCode }}
                                                </span>
                                            @else
                                                <span style="color:black;visibility: hidden;">------</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endfor

                            @endif
                            @if($billing_product_val->eType =='Lower' && $product_2_row !=0)
                                @for ($j = 0; $j < $product_2_row; $j++)
                                    <tr>
                                        <td >
                                            @if($j == 0)
                                            <span style="color:black">
                                                {{ $billing_product_val->vProductName . ' - ' . $billing_product_val->vStageCode }}
                                            </span>    
                                            @else
                                                <span style="color:black; visibility: hidden;">------</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endfor
                            @endif

                            @if($product_1_row ==0 && $product_2_row ==0)
                                <span style="color:black">
                                    {{ $billing_product_val->vProductName . ' - ' . $billing_product_val->vStageCode }}
                                </span> 
                            @endif
                            @endforeach
                        </table>
                    </td>
                @endif
                @if (isset($value->billing_product) && !empty($value->billing_product))
                    <td style="border: 1px solid #383838;padding: 0px;">
                        <table style="width: 100% !important;white-space: nowrap !important; border-spacing: 0px !important;">
                            @foreach ($value->billing_product as $key => $billing_product_val)
                            @if($billing_product_val->eType =='Upper' && $product_1_row != 0)
                                
                                @for ($i = 0; $i < $product_1_row; $i++)
                                    <tr>
                                        <td @if ($i == $product_1_row-1 && $product_2_row != 0)
                                        style="border-bottom: 1px solid #383838; padding:0px;margin:0px;"@endif>
                                            @if($i == 0)
                                                <span style="color:black;padding: 10px;">
                                                    @if (isset($billing_product_val->iStageTotalWithRush) &&
                                                    !empty($billing_product_val->iStageTotalWithRush) &&
                                                    $billing_product_val->iStageTotalWithRush != null){{ $billing_product_val->iStageTotalWithRush }}@else{{ $billing_product_val->iStagePrice }}@endif
                                                </span>
                                            @else
                                                <span style="color:black;visibility: hidden;">------</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endfor

                            @endif
                            @if($billing_product_val->eType =='Lower' && $product_2_row !=0)
                                @for ($j = 0; $j < $product_2_row; $j++)
                                    <tr>
                                        <td >
                                            @if($j == 0)
                                            <span style="color:black;padding: 10px;">
                                                @if (isset($billing_product_val->iStageTotalWithRush) &&
                                                !empty($billing_product_val->iStageTotalWithRush) &&
                                                $billing_product_val->iStageTotalWithRush != null){{ $billing_product_val->iStageTotalWithRush }}@else{{ $billing_product_val->iStagePrice }}@endif
                                            </span>    
                                            @else
                                                <span style="color:black; visibility: hidden;">------</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endfor
                            @endif
                            @if($product_1_row ==0 && $product_2_row ==0)
                            <span style="color:black;padding: 10px;">
                                @if (isset($billing_product_val->iStageTotalWithRush) &&
                                !empty($billing_product_val->iStageTotalWithRush) &&
                                $billing_product_val->iStageTotalWithRush != null){{ $billing_product_val->iStageTotalWithRush }}@else{{ $billing_product_val->iStagePrice }}@endif
                            </span>    
                            @endif
                                
                            @endforeach
                        </table>


                       
                    </td>
                @endif
                @if (isset($value->billing_product) && !empty($value->billing_product))
                    <td style="padding: 0px;border: 1px solid #383838; margin:0px;">


                        <table style="width: 100% !important;white-space: nowrap !important; border-spacing: 0px !important;padding:0px; margin:0px;">
                            @foreach ($value->billing_product as $key => $billing_product_val)
                            @if($billing_product_val->eType =='Upper' && $product_1_row != 0)
                                
                                @for ($i = 0; $i < $product_1_row; $i++)
                                    <tr style="margin:0px;padding:0px;">
                                        <td @if ($i == $product_1_row-1 && $product_2_row != 0)
                                            style="border-bottom: 1px solid #383838; padding:0px;margin:0px;"@endif>
                                            <span style="color:black; padding-left:20px;">
                                                {{ isset($billing_product_val->billing_addons[$i]->vAddonCode) ? $billing_product_val->billing_addons[$i]->vAddonCode . '*' . $billing_product_val->billing_addons[$i]->iAddOnsQty : $billing_product_val->billing_addons[$i]->vSlipAddonsName . '*' . $billing_product_val->billing_addons[$i]->iAddOnsQty }}
                                            </span>
                                        </td>
                                    </tr>
                                @endfor
                                    
                            @endif
                            @if($billing_product_val->eType =='Lower' && $product_2_row !=0)
                                @for ($j = 0; $j < $product_2_row; $j++)
                                    <tr>
                                        <td>
                                            <span style="color:black; padding-left:20px;">
                                                {{ isset($billing_product_val->billing_addons[$j]->vAddonCode) ? $billing_product_val->billing_addons[$j]->vAddonCode . '*' . $billing_product_val->billing_addons[$j]->iAddOnsQty : $billing_product_val->billing_addons[$j]->vSlipAddonsName . '*' . $billing_product_val->billing_addons[$j]->iAddOnsQty }}
                                            </span>
                                        </td>
                                    </tr>
                                @endfor
                            @endif

                            @if(($product_1_row ==0 && $product_2_row ==0) )
                            <span style="color:black; padding-left:20px;">--</span>
                            @endif

                            @endforeach
                        </table>



                    </td>
                @endif
                @if (isset($value->billing_product) && !empty($value->billing_product))
                    <td style="padding: 0px;border: 1px solid #383838;">
                        <table style="width: 100% !important;white-space: nowrap !important; border-spacing: 0px !important;padding:0px; margin:0px;">
                            @foreach ($value->billing_product as $key => $billing_product_val)
                            @if($billing_product_val->eType =='Upper' && $product_1_row != 0)
                                
                                @for ($i = 0; $i < $product_1_row; $i++)
                                    <tr style="margin:0px;padding:0px;">
                                        <td @if ($i == $product_1_row-1 && $product_2_row != 0)
                                            style="border-bottom: 1px solid #383838; padding:0px;margin:0px;"
                                        @endif>
                                            <span style="color:black;padding-left:10px;">
                                                {{ isset($billing_product_val->billing_addons[$i]->iAddOnsTotal) ? $billing_product_val->billing_addons[$i]->iAddOnsTotal : '--'  }}
                                            </span>
                                        </td>
                                    </tr>
                                @endfor

                            @endif
                            @if($billing_product_val->eType =='Lower' && $product_2_row !=0)
                                @for ($j = 0; $j < $product_2_row; $j++)
                                    <tr>
                                        <td>
                                            <span style="color:black;padding-left:10px;">
                                                {{ isset($billing_product_val->billing_addons[$j]->iAddOnsTotal) ? $billing_product_val->billing_addons[$j]->iAddOnsTotal : '--'  }}
                                            </span>
                                        </td>
                                    </tr>
                                @endfor
                            @endif
                                
                            @if(($product_1_row ==0 && $product_2_row ==0) )
                            <span style="color:black; padding-left:20px;">--</span>
                            @endif

                            @endforeach
                        </table>
                    </td>
                @endif
                <td style="padding: 10px;border: 1px solid #383838;">
                    <span style="font-weight: normal;color:black;">
                        {{ date('m/d/Y', strtotime($value->dDeliveryDate)) }}
                    </span>
                </td>
                <td style="padding: 10px;border: 1px solid #383838;">
                    <span style="font-weight: normal;color:black;">
                        {{ isset($value->iMainTotal) && !empty($value->iMainTotal)?$value->iMainTotal:'--' }}
                    </span>
                </td>
                

            </tr>
        @endforeach
    </table>
    <div
        style="max-width: 1000px;width: 100%; margin: 0 auto;font-size:
            12px; margin-top: 20px; border-collapse: collapse">
        <div style="margin-left: auto; width: 300px;text-align: end;">
            <table style="width: 100%;color:black;">
                <tr>
                    <td style="padding: 5px;font-weight: bold;">Date Due By</td>
                    <td style="padding: 5px;">8/15/22</td>
                </tr>
                <tr>
                    <td style="padding: 5px;font-weight: bold;">Total</td>
                    <td style="padding: 5px;">
                        @if(isset($data[0]->MainTotalAll) && !empty($data[0]->MainTotalAll))
                            {{'$'.$data[0]->MainTotalAll}}
                            @else
                            --
                        @endif</td>
                </tr>
            </table>

        </div>
    </div>

</body>

</html>
