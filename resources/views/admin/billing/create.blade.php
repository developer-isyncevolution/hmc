@php
    $billings = $billings[0];
@endphp
@extends('layouts.admin.index')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<div class="main-panel">
  <div class="page-title text-center">
    <h3>
      Edit Billing
    </h3>
</div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
        <form action="{{route('admin.billing.store')}}" id="frm" class="row g-5 add-product mt-0" method="post">
            @csrf
            <input type="hidden" name="id" value="@if(isset($billings->iBillingId)) {{$billings->iBillingId}} @endif">
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label >Slip</label>
                <input type="text" class="form-control" id="vSlipNumber" name="vSlipNumber"  value="@isset($billings->vSlipNumber){{$billings->vSlipNumber}}@endisset" disabled>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label >Patient</label>
                <input type="text" class="form-control" id="vPatientName" name="vPatientName"  value="@isset($billings->vPatientName){{$billings->vPatientName}}@endisset" disabled>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label >Main Total</label>
                <input style="background: #ffe7e7" type="number" class="form-control" id="iMainTotal" name="iMainTotal"  value="@isset($billings->iMainTotal){{$billings->iMainTotal}}@endisset" required readonly>
                <div class="text-danger" style="display: none;" id="iMainTotal_error">Enter stage price</div>
            </div>
            @if(isset($billings->billing_product) && !empty($billings->billing_product))
              @foreach($billings->billing_product as $key=> $billing_product_val)
                @if($key !=0 )
                <hr style="margin-top:30px;">
                @endif
                <h3>{{$billing_product_val->eType}} Product</h3>
                <div class="col-xxl-4 col-lg-6 col-md-12">
                    <label >Jaw</label>
                    <input type="text" class="form-control" id="eType" name="eType"  value="@isset($billing_product_val->eType){{$billing_product_val->eType}}@endisset" disabled>
                </div>
                <div class="col-xxl-4 col-lg-6 col-md-12">
                    <label >Grade</label>
                    <input type="text" class="form-control" id="vGradeName" name="vGradeName"  value="@isset($billing_product_val->vGradeName){{$billing_product_val->vGradeName}}@endisset" disabled>
                </div>
                <div class="col-xxl-4 col-lg-6 col-md-12">
                    <label >Product</label>
                    <input type="text" class="form-control" id="vProductName" name="vProductName"  value="@isset($billing_product_val->vProductName){{$billing_product_val->vProductName}}@endisset" disabled>
                </div>
                <div class="col-xxl-4 col-lg-6 col-md-12">
                    <label >Stage</label>
                    <input type="text" class="form-control" id="vStageCode" name="vStageCode"  value="@isset($billing_product_val->vStageCode){{$billing_product_val->vStageCode}}@endisset" disabled>
                </div>
                <div class="col-xxl-4 col-lg-6 col-md-12">
                    <label >Stage Total</label>
                    @if(isset($billing_product_val->iStageTotalWithRush) && !empty($billing_product_val->iStageTotalWithRush) && $billing_product_val->iStageTotalWithRush != null)
                    <input type="text" class="form-control ChangePrice" id="i{{$billing_product_val->eType}}StageTotalWithRush" name="{{$billing_product_val->eType}}StageTotal[iStageTotalWithRush]" value="{{trim($billing_product_val->iStageTotalWithRush)}}" step="any">
                    @else
                    <input type="number"  class="form-control iStagePrice ChangePrice" id="i{{$billing_product_val->eType}}StagePrice" name="{{$billing_product_val->eType}}StageTotal[iStagePrice]" value="{{trim($billing_product_val->iStagePrice)}}" required step="any">
                    @endif
                    <div class="text-danger" style="display: none;" id="iStagePrice_error">Enter stage price</div>
                  
                </div>
                <div class="col-xxl-4 col-lg-6 col-md-12">
                  <label >Delivery Date</label>
                  <input type="date" class="form-control" id="dDeliveryDate" name="{{$billing_product_val->eType}}StageTotal[dDeliveryDate]"  value="@isset($billing_product_val->dDeliveryDate){{$billing_product_val->dDeliveryDate}}@endisset" >
                </div>
                @if(isset($billing_product_val->billing_addons) && count($billing_product_val->billing_addons)>0)
                <div class="row mt-6" id="Addons_exist">
                  <h5>Addons</h5>
                  <div class="col-xxl-4 col-lg-6 col-md-12">
                      <label >Addon * Qty</label>
                      @if(isset($billing_product_val->billing_addons) && !empty($billing_product_val->billing_addons))
                      @foreach($billing_product_val->billing_addons as $key=>$billing_addons_val)
                        @if(isset($billing_addons_val->vSlipAddonsName) && !empty($billing_addons_val->vSlipAddonsName) && $billing_addons_val->vSlipAddonsName != null)
                          <input type="text" class="form-control" id="iStageTotalWithRush" name="iStageTotalWithRush" value="{{$billing_addons_val->vSlipAddonsName.' * '.$billing_addons_val->iAddOnsQty}}" disabled>
                          @endif
                      @endforeach
                      @endif
                  </div>
                  <div class="col-xxl-4 col-lg-6 col-md-12" id="{{$billing_product_val->eType}}Addons">
                      <label >Addon Total</label>
                      @if(isset($billing_product_val->billing_addons) && !empty($billing_product_val->billing_addons))
                      @foreach($billing_product_val->billing_addons as $key=>$billing_addons_val)
                        @if(isset($billing_addons_val->iAddOnsTotal) && !empty($billing_addons_val->iAddOnsTotal) && $billing_addons_val->iAddOnsTotal != null)
                          <input type="number" class="form-control iAddOnsTotal ChangePrice" id="i{{$billing_product_val->eType}}AddOnsTotal{{$key}}" name="{{$billing_product_val->eType}}AddonTotal[{{$key}}][iAddOnsTotal]" value="{{$billing_addons_val->iAddOnsTotal}}" required step="any">
                          <input type="hidden" class="form-control "  name="{{$billing_product_val->eType}}AddonTotal[{{$key}}][iAddCategoryId]" value="{{$billing_addons_val->iAddCategoryId}}" required>
                          @endif
                      @endforeach
                      @endif
                  </div>
                </div>
                @endif
                
              @endforeach
            @endif  
            <div class="col-12 align-self-end d-inline-block mt-4">
              <button type="submit" id="submit" class="btn submit-btn me-2">Submit</button>
              <a href="{{route('admin.billing')}}" class="btn back-btn me-2">Back</a>
            </div>
          </form>
            
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script>
  $(".ChangePrice").keyup(function(){
    var MainTotal = 0;
    var StageTotal = 0;
    var UpperStageTotal = 0;
    var LowerStageTotal = 0;
    var UpperAddons = 0;
    var LowerAddons = 0;
    var AddonsTotal = 0;

    
    if($('#iUpperStageTotalWithRush').val() !=undefined && $('#iUpperStageTotalWithRush').val() !=null && $('#iUpperStageTotalWithRush').val() !='' )
    {
      var UpperStageTotal = Number($('#iUpperStageTotalWithRush').val());  
    }
    else if($('#iUpperStagePrice').val() !=undefined && $('#iUpperStagePrice').val() !=null && $('#iUpperStagePrice').val() !='')
    {
      var UpperStageTotal = Number($('#iUpperStagePrice').val());
    }

    if($('#iLowerStageTotalWithRush').val() !=undefined && $('#iLowerStageTotalWithRush').val() !=null && $('#iLowerStageTotalWithRush').val() !='')
    {
      var LowerStageTotal = Number($('#iLowerStageTotalWithRush').val());  
    }
    else if($('#iLowerStagePrice').val() !=undefined && $('#iLowerStagePrice').val() !=null && $('#iLowerStagePrice').val() !='')
    {
      var LowerStageTotal = Number($('#iLowerStagePrice').val());;
    }
    
    var StageTotal = UpperStageTotal+LowerStageTotal;

    if ($('#Addons_exist').length)
    {
      if($('#iUpperAddOnsTotal0').length)
      {
          $('#UpperAddons input[type=number]').each(function() {
            if($(this).val() != '')
            {
              var Addons = $(this).val();
            }
            else
            {
              var Addons = 0;
            }
            UpperAddons += parseInt(Addons, 10);
          });
      }
      if($('#iLowerAddOnsTotal0').length)
      {
        $('#UpperAddons input[type=number]').each(function() {
            if($(this).val() != '')
            {
              var Addons = $(this).val();
            }
            else
            {
              var Addons = 0;
            }
            LowerAddons += parseInt(Addons, 10);
          });
      }
      AddonsTotal = UpperAddons+LowerAddons;
      MainTotal = AddonsTotal+StageTotal;
    }
    else
    {
      var MainTotal = StageTotal;
    }
    $('#iMainTotal').val(MainTotal);
    
  });
  
    </script>
@endsection
