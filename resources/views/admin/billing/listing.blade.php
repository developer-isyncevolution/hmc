@extends('layouts.admin.index')

<link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <style>
          .swal-text
	{
		color: #0060a2 !important;
        font-size: 28px !important;
	}
    </style>
@section('content')
    <div class="main-panel">
        <div class="page-title text-center">
            <h3>
                Billing
            </h3>
        </div>
        <div class="col-lg-12 mx-auto">
            <div class="card mb-5 mb-xl-4">
                {{-- <div class="row">
                    <div class="col-lg-4 ms-auto">
                        <div class="custome-serce-box">
                            <input type="text" class="form-control" id="keyword" name="search" placeholder="Search">
                            <span class="search-btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div> --}}
                <div class="row g-3 mt-2">
                    <div class="col-lg-4 mx-auto">
                        @if (\App\Libraries\General::admin_info()['customerType'] == 'Lab Admin')
                            <div class="custome-serce-box">
                              
                                <select name="iOfficeId" id="iOfficeId" class="w-100 CustomerChange">
                                    <option  value="">Select Office</option>
                                   
                                    @if (isset($office) && !empty($office))
                                        <option @selected(!empty(Session::get('criteria')['iChangeId']) && Session::has('criteria') && Session::get('criteria')['iChangeId']== 'All') value="All">All</option>
                                        @foreach ($office as $key => $office_value)
                                            <option  @if ( (!empty(Session::get('criteria')['iChangeId']) &&  Session::has('criteria') && Session::get('criteria')['iChangeId'] == $office_value->iCustomerId) ) selected @endif
                                                value="{{ $office_value->iCustomerId }}"> {{ $office_value->vOfficeName }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        @else
                            <div class="custome-serce-box">

                                <select name="iLabId" id="iLabId" class="w-100 CustomerChange">
                                    <option value="">Select Lab</option>
                                    @if (isset($labs) && !empty($labs))
                                        @foreach ($labs as $key => $labs_value)
                                            <option @if ( (!empty(Session::get('criteria')['iChangeId']) && Session::has('criteria') && Session::get('criteria')['iChangeId'] == $labs_value->iCustomerId )) selected @endif
                                                value="{{ $labs_value->iCustomerId }}"> {{ $labs_value->vOfficeName }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-4 ms-auto">
                        <div class="custome-serce-box">
                            <input style="height: 37px;" type="text" class="form-control" id="keyword" name="search" placeholder="Search" value="@if(Session::has('criteria') && !empty(Session::get('criteria')['vKeyword'])) {{Session::get('criteria')['vKeyword']}}@endif">
                            <span class="search-btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
              
                    <div class="col-lg-4 mx-auto">
                        <div class="custome-serce-box">
                            <select style="height: 37px;" name="action_print" id="action_print" class="form-control w-100">
                                <option value="">Select Action</option>
                                <option value="print_statement">Print Statement</option>
                                <option value="print_invoice">Print Invoice</option>
                               
                            </select>
                        </div>
                    </div>
                   
                    <div class="col-lg-4 mx-auto">
                        {{-- {{dd(Session::get('criteria')['duration']);}} --}}
                        <select style="height: 37px;" name="duration" id="duration" class="form-control custome-serce-box w-100" >
                            <option @if ((!empty(Session::get('criteria')['duration']) && Session::has('criteria') && Session::get('criteria')['duration'] == 'Today' )) selected @endif value="Today">Today</option>
                            <option @if ((!empty(Session::get('criteria')['duration']) && Session::has('criteria') && Session::get('criteria')['duration'] == 'Yesterday' )) selected @endif  value="Yesterday">Yesterday</option>
                            <option @if ((!empty(Session::get('criteria')['duration']) && Session::has('criteria') && Session::get('criteria')['duration'] == 'ThisWeek' )) selected @endif value="ThisWeek">This Week</option>
                            <option @if ((!empty(Session::get('criteria')['duration']) && Session::has('criteria') && Session::get('criteria')['duration'] == 'LastWeek' )) selected @endif  value="LastWeek">Last Week</option>
                            <option @if ((!empty(Session::get('criteria')['duration']) && Session::has('criteria') && Session::get('criteria')['duration'] == 'ThisMonth' )) selected @endif value="ThisMonth">This Month</option>
                            <option @if ((!empty(Session::get('criteria')['duration']) && Session::has('criteria') && Session::get('criteria')['duration'] == 'LastMonth' )) selected @endif value="LastMonth">Last Month</option>
                            <option @if ((!empty(Session::get('criteria')['duration']) && Session::has('criteria') && Session::get('criteria')['duration'] == 'ThisYear' )) selected @endif value="ThisYear">This Year</option>
                            <option @if ((!empty(Session::get('criteria')['duration']) && Session::has('criteria') && Session::get('criteria')['duration'] == 'LastYear' )) selected @endif value="LastYear">Last Year</option>
                            <option @if ((!empty(Session::get('criteria')['duration']) && Session::has('criteria') && Session::get('criteria')['duration'] == 'Custom')) selected @endif value="Custom">Custom</option>
                        </select>
                    </div>
                    <div class="col-lg-6 mx-auto HideForDate">
                      
                    </div>
                    
                    <div class="col-lg-2  customeDateShow" style="display: none">
                        <div class="form-group">
                            <input style="height: 37px;" class="form-control changeDate" type="text" placeholder="Start Date" id="startdate" autocomplete="false"/>
                        </div>
                    </div>
                    <div class="col-lg-2  customeDateShow" style="display: none">
                        <div class="form-group">
                            <input style="height: 37px;" class="form-control changeDate" type="text" placeholder="End Date" id="enddate" autocomplete="false"/>
                        </div>
                    </div>
                    <div class="col-lg-2  customeDateShow" style="display: none">
                        <a style="height: 37px;" href="javascript:;" id="SerchSlip" class="btn add-btn mx-2 "> Search</a>
                    </div>
                    <div class="col-lg-2 text-center" >
                        <a href="#" onclick="print_invoice_statement('print_invoice')">
                            <i style="color: #054b97;font-size: 30px;" class="fal fa-print" ></i>
                        </a>
                        &nbsp;&nbsp;
                        <a href="#" onclick="sent_mail_invoice()">
                            <i class="fal fa-paper-plane" style="color: #054b97;font-size: 30px;" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="listing-page">
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                    <th class="w-25px" data-orderable="false">
                                        <div class="form-check form-check-sm form-check-custom form-check-solid">
                                            <input class="form-check-input all_checked" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" id="selectall" type="checkbox" name="selectall">
                                            <label for="selectall">&nbsp;</label>
                                        </div>
                                    </th>
                                    <th style="font-size: 13px;" scope="col" class="min-w-40px">
                                        <span >Patient</span>
                                    </th>
                                    <th style="font-size: 12px;" scope="col" class="min-w-10px">
                                        <span >
                                            U/L
                                        </span>
                                    </th>
                                    <th style="font-size: 12px;" scope="col" class="min-w-50px">
                                        <span >
                                            Product
                                        </span>
                                    </th>
                                    <th style="font-size: 12px;" scope="col" class="min-w-40px">
                                        <span >
                                            Grade
                                        </span>
                                    </th>
                                    <th style="font-size: 12px;" scope="col" class="min-w-50px">
                                        <span >
                                            Stage
                                        </span>
                                    </th>
                                    <th style="font-size: 12px;" scope="col" class="min-w-20px">
                                        <span >
                                            Total
                                        </span>
                                        
                                    </th>
                                    <th style="font-size: 12px;" scope="col" class="min-w-10px">
                                        <span >
                                            R%
                                        </span>
                                    </th>
                                    <th style="font-size: 12px;" scope="col" class="min-w-60px">
                                        <span >
                                            Add on
                                        </span>
                                    </th>
                                    <th style="font-size: 12px;" scope="col" class="min-w-10px">
                                        <span >
                                            Qty
                                        </span>
                                    </th>
                                    <th style="font-size: 12px;" scope="col" class="min-w-10px">
                                        <span >
                                            Total
                                        </span>
                                    </th>
                                    <th style="font-size: 12px;" scope="col" class="min-w-70px">
                                        <span >
                                            Due Date
                                        </span>
                                    </th>
                                    <th style="font-size: 12px;" scope="col" class="min-w-10px">
                                        <span >
                                            Total
                                        </span>
                                        : <span style="font-weight: 400" class="total_count">$0</span>
                                    </th>
                                    <th style="font-size: 12px;" scope="col" class="min-w-70px"><a href="#"> <span
                                        class="text-muted fw-bold text-muted d-block fs-7"> Actions </span> </a></th>
                                
                            </tr>
                        </thead>
                        <tbody id="table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader" width="250px"
                                height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="{{route('admin.billing.PrintBilling')}}" method="post" id="print_form" target="_blank">
        @csrf
        <input type="hidden" name="checked_id" id="checked_id" class="checked_id">
       
        <input type="hidden" name="duration_val" class="duration_val" id="duration_val">
    </form>

    <form action="{{route('admin.billing.PrintBillingInvoice')}}" method="post" id="print_invoice_form" target="_blank">
        @csrf
        <input type="hidden" name="checked_id" id="checked_id" class="checked_id">
        <input type="hidden" name="duration_val" class="duration_val" id="duration_val">
    </form>
 

    <div class="modal fade" id="toothShadeModal" tabindex="-1" aria-labelledby="toothShadeModalLabel" aria-modal="true" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <form action="{{route('admin.billing.SentMailBillingInvoice')}}" id="sent_mail_invoice_form" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="toothShadeModalLabel">Select Notification Email</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                @csrf     
                <label>Notfication Email</label>
                    <input type="text" class="form-control" id="vNotificationEmail" name="vNotificationEmail" placeholder="Notfication Email" value="@if(old('vNotificationEmail')!=''){{old('vNotificationEmail')}}@elseif(isset($toothBrands->vNotificationEmail)){{$toothBrands->vNotificationEmail}}@else{{old('vNotificationEmail')}}@endif">
                    <div class="text-danger" style="display: none;" id="vNotificationEmail_error">Please enter notfication email</div>
                </div>       
                <input type="hidden" name="checked_id" id="checked_id" class="checked_id">
                <input type="hidden" name="duration_val" class="duration_val" id="duration_val">
               
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <a type="submit" id="submit_email" class="btn submit-btn me-2">Send mail</a>
                </div>
            </form>
        </div>
          </div>
        </div>
    </div>
    
@endsection

@section('custom-css')
    <style>
    </style>
@endsection

@section('custom-js')
    <script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script>
        // $('#duration').selectize();
        $('#iOfficeId').selectize();
        $('#iLabId').selectize();

        $(document).ready(function() {
            AjaxListing();
           
            @if((!empty(Session::get('criteria')['duration']) && Session::has('criteria') && Session::get('criteria')['duration'] == 'Custom')) 
                $('.HideForDate').hide();
            @endif
        });
        function AjaxListing(action='')
        {
            $("#ajax-loader").show();
            var startdate = $('#startdate').val();
            var enddate = $('#enddate').val();
            var duration = $("#duration").val();
         
            var keyword = $("#keyword").val();
            var iCustomerId = $('.CustomerChange').val();
            $.ajax({
                url: "{{ route('admin.billing.ajaxListing') }}",
                type: "get",
                data: {
                    startdate: startdate,
                    enddate: enddate,
                    keyword: keyword,
                    action: action,
                    duration: duration,
                    iChangeId:iCustomerId,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                    if(duration =='Custom')
                    {
                        $('.customeDateShow').show();
                    }
                }
            });
        }
        $("#selectall").click(function() {
            if (this.checked) {
                
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', true);
                });
            } else {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', false);
                });
            }
        });
        
        $(document).on('change','.checkboxall',function(){
        // $('.checkboxall').change(function() {
				MainPriceCount();
            });
        // $('#selectall').change(function() {
            $(document).on('change','#selectall',function(){
            MainPriceCount();
        });
        function MainPriceCount()
        {
            var total = 0;
            $('input.checkboxall:checkbox:checked').each(function () {
                PriceId = $(this).attr('data-id');
                total_new = $('.FetchPrice'+PriceId).attr('data-price');
                total += Number(total_new);
            });
            // $('.checkboxall').each(function() {
               
            // });
            // $("input[name='Billing_Id[]']:checked").each( function () {
                
            //     total_new = $('#Billing_Main_Total'+$(this).val()).attr('data-price');
            //     total += Number(total_new);
            // });
            $('.total_count').text('$'+total)
        }
        
       

       

        $("#keyword").keyup(function() {
            AjaxListing('search');
        });

        // Print Bill start
            $(document).on('change','#action_print',function(){
                print_invoice_statement();
            });

            function print_invoice_statement(action_print='')
            {
                var id = [];
                $("input[name='Billing_Id[]']:checked").each( function () {
                    id.push($(this).val());
                });
              

                var id = id.join(",");
                if(action_print =='')
                {
                    var action_print = $('#action_print').val();
                }
                var duration = $('#duration').val();
                if(duration !='Custom')
                {
                    $('.duration_val').val(duration);
                }
                if(action_print =='print_statement')
                {
                    if (id.length == 0) {
                        alert('Please select records.')
                    }else{
                            $('.checked_id').val(id);
                            $("#print_form").submit();
                            return true;
                        }
                }
                if(action_print =='print_invoice')
                {
                    
                    if (id.length == 0) {
                        alert('Please select records.')
                    }else{
                            $('.checked_id').val(id);
                            $("#print_invoice_form").submit();
                            return true;
                        }
                }

            }
        // Print Bill end

        //Sent Bill mail start

        function sent_mail_invoice()
        {
            var id = [];
            $("input[name='Billing_Id[]']:checked").each( function () {
                id.push($(this).val());
            });
            var id = id.join(",");
            var duration = $('#duration').val();
            if(duration !='Custom')
            {
                $('.duration_val').val(duration);
            }
            if (id.length == 0) {
                alert('Please select records.')
            }
            else
            {
                // alert($('#iOfficeId').val())
                if($('#iOfficeId').val() =='')
                {
                    alert('Please select office.')
                }
                else
                {
                    $('#toothShadeModal').modal('show');
                    $('.checked_id').val(id);
                    var iOfficeId = $("#iOfficeId").val();
                    $.ajax({
                        url: "{{ route('admin.billing.fetchCustomerEmail') }}",
                        type: "post",
                        data: {
                            iOfficeId: iOfficeId,
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function(response) {
                            if(response !='' && response != null)
                            {
                                $('#vNotificationEmail').val(response);
                            }
                        }
                    });
                }
            }
        }

        $('#submit_email').click(function(){
            vNotificationEmail = $('#vNotificationEmail').val();
            var error = false;
            if (vNotificationEmail.length == 0) {
                $("#vNotificationEmail_error").show();
                error = true;
            } else {
                $("#vNotificationEmail_error").hide();
            }
            if (error == true) {
                return false;
            } else {
                $("#sent_mail_invoice_form").submit();
                return true;
            }
        });
        //Sent Bill mail end
        $(document).on('click', '.sort', function() {
            column = $(this).data("column");
            order = $(this).attr('data-order');


            if (order == "ASC") {
                $(this).attr('data-order', 'DESC');
            } else {
                $(this).attr('data-order', 'ASC');
            }

            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.billing.ajaxListing') }}",
                type: "get",
                data: {
                    column: column,
                    order,
                    order,
                    action: 'sort'
                },
                success: function(response) {
                    // console.log(response);
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });

          
        });

        $(document).on('click', '.ajax_page', function() {
            pages = $(this).data("pages");

            $("#table_record").html('');
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.billing.ajaxListing') }}",
                type: "get",
                data: {
                    pages: pages
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });
        $(document).on('change', '#page_limit', function() {
            limit_page = this.value;
            $("#table_record").html('');
            $("#ajax-loader").show();
            url = "{{ route('admin.billing.ajaxListing') }}";

            setTimeout(function() {
                $.ajax({
                    url: url,
                    type: "get",
                    data: {
                        limit_page: limit_page
                    },
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
                // hideLoader();
            }, 500);
        });
        // Search by onchange date
        $(document).on('change', '#duration', function() {
            var duration = $("#duration").val();
     
            if (duration == 'Custom') {
                $('.customeDateShow').show();
                $('.HideForDate').hide();
            } else {
                $('.customeDateShow').hide();
                $('.HideForDate').show();
            }
            AjaxListing('search');
        });
        // $('.changeDate').datepicker();
        $('#startdate').datepicker({
            orientation: "bottom auto",
            
        });
        $('#enddate').datepicker({
            orientation: "bottom auto",
        });
        // var start_date = $('#startdate').val();
        // $('#enddate').datepicker('destroy').datepicker({
        //     orientation: "bottom auto",
        //     dateFormat: "mm/dd/yy",
        //     startDate: new Date(start_date),
        // });
        $('#SerchSlip').click(function() {
            var startdate = $('#startdate').val();
            var enddate = $('#enddate').val();
            $('.duration_val').val(startdate+' - '+enddate);
            if (startdate != '' && enddate != '') {
              
                AjaxListing('search');
            } else if (startdate == '') {
                alert('please select start date')
            } else if (enddate == '') {
                alert('please select end date')
            }
        });

        $(document).on('change', '.CustomerChange', function() {
           
            var iCustomerId = $(this).val();
            setTimeout(() => {
                AjaxListing('search');
            }, 500);
        });

        $(document).on('click', '.reffreshBilling', function() {
            var patient = $(this).data("patient");
		swal({
                title: "Are you want to reffresh billing",
                text: '"'+patient+'"',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((Ok) => {
                if (Ok) {
					var iSlipId = $(this).attr('data-slip_id');
					var iCaseId = $(this).attr('data-case_id');
                    setTimeout(function() {
                        $.ajax({
                            url: "{{route('admin.labcase.update_billing_ref')}}",
                            type: "post",
                            data: {
                                iSlipId: iSlipId,
								iCaseId:iCaseId,
                                _token: '{{csrf_token()}}',
                            },
                            success: function(response) {
                                if(response =='success')
                                {
									notification_success("Reffresh billing successfully");
									AjaxListing();
                                }
								else
								{
									notification_error("Something went wrong");
								}
                            }
                        });
                    }, 1000);
                }
            })
	});

   
 
    </script>
@endsection
