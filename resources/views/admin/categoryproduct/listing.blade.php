@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
    <div class="page-title text-center">
        <h3>
            Lab Administrator
        </h3>
    </div>
    <div class="card mb-5 mb-xl-4">
        <div class="card-header border-0">
            <ul class="c-nav-tab">
                @if(\App\Libraries\General::check_permission('CategoryController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.category')}}" class=" add-btn btn mx-2">
                        Categories
                    </a>
                </li>
                @endif
                @if(\App\Libraries\General::check_permission('CategoryProductController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.categoryproduct')}}" class=" active btn add-btn  mx-2">
                        Products
                    </a>
                </li>
                @endif
                @if(\App\Libraries\General::check_permission('ProductStageController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.productstage')}}" class="btn add-btn mx-2">
                        Stages
                    </a>
                </li>
                @endif
                @if(\App\Libraries\General::check_permission('AddoncategoryController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.addoncategory')}}" class="btn add-btn mx-2">
                        Categories Add ons
                    </a>
                </li>
                @endif
                @if(\App\Libraries\General::check_permission('SubaddoncategoryController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.subaddoncategory')}}" class="btn add-btn mx-2">
                        Add ons
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-mid">
                    <ul class="d-flex list-unstyled">
                        @if(\App\Libraries\General::check_permission('','eCreate') == true)
                        <li>
                            <a href="{{route('admin.categoryproduct.create')}}" class="btn create_permission add-btn me-2">
                                <i class="fa fa-plus"></i> Add
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-6 mx-auto">
            <div class="card mb-5 mb-xl-4">

                <div class="row g-4">
                    <div class="col-lg-12">
                        <h3 class="card-title">
                            Upper Product
                        </h3>
                    </div>
                    <div class="col-lg-8 mx-auto">
                        <select name="iCategoryId" id="iCategoryId" >
                            <option value="">Select category</option>
                            @foreach($uppercategory as $key => $uppercategorys)
                            <option value="{{$uppercategorys->iCategoryId}}" @if(isset($categorys)){{$uppercategorys->iCategoryId == $categorys->iCategoryId  ? 'selected' : ''}} @endif>{{$uppercategorys->vName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-8 mx-auto">
                    <div class="custome-serce-box">
                        <input type="text" class="form-control" id="upperkeyword" name="search" class="search" placeholder="Search">
                        <span class="search-btn"><i class="fas fa-search"></i></span>
                    </div>
                    </div>
                </div>
            </div>
            <div class="listing-page">
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <!-- <th class="w-25px" data-orderable="false">
                                <div class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input id="Uselectall" type="checkbox" name="Uselectall" class="form-check-input">
                                    <label for="selectall">&nbsp;</label>
                                </div>
                            </th> -->
                                <th>Image</th>
                                <th scope="col" class=""><a id="vCode" class="upersort" data-column="vCode" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">Code </span> </a></th>
                                <th scope="col" class=""><a id="vName" class="upersort" data-column="vName" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">Name </span> </a></th>
                                <th scope="col" class=""><a id="iSequence" class="upersort" data-column="iSequence" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Sequence </span> </a></th>
                                <th scope="col" class=""><a id="eStatus" class="upersort" data-column="eStatus" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Status </span> </a></th>
                                <th scope="col" class=""><span class="text-muted fw-bold text-muted d-block fs-7"> Action </span></th>
                            </tr>
                        </thead>
                        <tbody id="upper_table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 mx-auto">
            <div class="card mb-5 mb-xl-4">

                <div class="row g-4">
                    <div class="col-lg-12">
                        <h3 class="card-title">
                            Lower Product
                        </h3>
                    </div>
                    <div class="col-lg-8 mx-auto">
                        <select name="iCategoryProductId" id="CategoryProduct">
                            <option value="">Select category</option>
                            @foreach($lowercategory as $key => $lowercategorys)
                            <option value="{{$lowercategorys->iCategoryId}}" @if(isset($categorys)){{$lowercategorys->iCategoryId == $categorys->iCategoryId  ? 'selected' : ''}} @endif>{{$lowercategorys->vName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-8 mx-auto">
                    <div class="custome-serce-box">
                        <input type="text" class="form-control" id="lower_keyword" name="search" class="search" placeholder="Search">
                        <span class="search-btn"><i class="fas fa-search"></i></span>
                    </div>
                    </div>

                </div>
            </div>
            <div class="listing-page">
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <!-- <th class="w-25px" data-orderable="false">
                                <div class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input id="Lselectall" type="checkbox" name="Lselectall" class="form-check-input">
                                    <label for="selectall">&nbsp;</label>
                                </div>
                            </th> -->
                                <th>Image</th>
                                <th scope="col" class=""><a id="vCode" class="lowersort" data-column="vCode" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">Code </span> </a></th>
                                <th scope="col" class=""><a id="vName" class="lowersort" data-column="vName" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">Name </span> </a></th>
                                <th scope="col" class=""><a id="iSequence" class="lowersort" data-column="iSequence" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Sequence </span> </a></th>
                                <th scope="col" class=""><a id="eStatus" class="lowersort" data-column="eStatus" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Status </span> </a></th>
                                <th scope="col" class=""><span class="text-muted fw-bold text-muted d-block fs-7"> Action </span></th>
                            </tr>
                        </thead>
                        <tbody id="lower_table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="lower-ajax-loader" width="250px" height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom-css')
<style></style>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;

<script>
    $('#iCategoryId').selectize();
    $('#CategoryProduct').selectize();

    // $("#iCategoryId").click(function(){
    //     // alert($(this).data("value"));
    //     alert();
    // })

    // $('#iCategoryId').select2({
    //     width: '100%',
    //     placeholder: "Select category",
    //     allowClear: true
    // });
    // $('#CategoryProduct').select2({
    //     width: '100%',
    //     placeholder: "Select category",
    //     allowClear: true
    // });
    $(document).ready(function() {
        var eType = 'Upper';
        var iCategoryId = $("#iCategoryId").val();
        $.ajax({
            url: "{{route('admin.categoryproduct.ajaxListing')}}",
            type: "get",
            data: {
                iCategoryId: iCategoryId,
                eType: eType,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $("#Uselectall").click(function() {
        if (this.checked) {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', true);
            });
        } else {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', false);
            });
        }
    });

    $(document).on('click', '#delete_btn', function() {
        var eType = 'Upper';
        var id = [];

        $("input[name='CategoryProduct_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                    title: "Are you sure delete all categoryproduct ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{route('admin.categoryproduct.ajaxListing')}}",
                            type: "get",
                            data: {
                                id: id,
                                eType: eType,
                                action: 'multiple_delete'
                            },
                            success: function(response) {
                                $("#upper_table_record").html(response);
                                $("#ajax-loader").hide();
                            }
                        });
                    }
                });
        }
    });

    $("#upperkeyword").keyup(function() {
        var upperkeyword = $("#upperkeyword").val();
        var eType = 'Upper';
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.categoryproduct.ajaxListing')}}",
            type: "get",
            data: {
                upperkeyword: upperkeyword,
                eType: eType,
                action: 'search'
            },
            success: function(response) {
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '#delete', function() {
        swal({
                title: "Are you sure delete this upper categoryproduct ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");
                    var eType = 'Upper';

                    $("#ajax-loader").show();

                    $.ajax({
                        url: "{{route('admin.categoryproduct.ajaxListing')}}",
                        type: "get",
                        data: {
                            id: id,
                            eType: eType,
                            action: 'delete'
                        },
                        success: function(response) {
                            $("#upper_table_record").html(response);
                            $("#ajax-loader").hide();
                            notification_error("Category Product Deleted Successfully");
                                setTimeout(function() {
                                   location.reload();
                                }, 1000);
                        }
                    });
                }
            })
    });
    $(document).on('click', '.upersort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');
        var eType = 'Upper';

        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.categoryproduct.ajaxListing')}}",
            type: "get",
            data: {
                column: column,
                order,
                order,
                eType: eType,
                action: 'sort'
            },
            success: function(response) {
                console.log(response);
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '.ajax_page_upper', function() {
        pages = $(this).data("pages");9
        var eType = 'Upper';
        $("#upper_table_record").html('');
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.categoryproduct.ajaxListing')}}",
            type: "get",
            data: {
                pages: pages,
                eType: eType,
            },
            success: function(response) {
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#iCategoryId', function() {
        var eType = 'Upper';
        var iCategoryId = $("#iCategoryId").val();
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.categoryproduct.ajaxListing')}}",
            type: "get",
            data: {
                iCategoryId: iCategoryId,
                eType: eType,
            },
            success: function(response) {
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#upper_page_limit', function() {
        upper_limit_page = this.value;
        var eType = 'Upper';
        $("#upper_table_record").html('');
        $("#ajax-loader").show();
        url = "{{route('admin.categoryproduct.ajaxListing')}}";

        setTimeout(function() {
            $.ajax({
                url: url,
                type: "get",
                data: {
                    upper_limit_page: upper_limit_page,
                    eType: eType
                },
                success: function(response) {
                    $("#upper_table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            // hideLoader();
        }, 500);
    });
</script>
<script>
    
    $(document).ready(function() {
        var eType = 'Lower';
        var CategoryProduct = $("#CategoryProduct").val();
        $.ajax({
            url: "{{route('admin.categoryproduct.LowerajaxListing')}}",
            type: "get",
            data: {
                CategoryProduct: CategoryProduct,
                eType: eType,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#lower_table_record").html(response);
                $("#lower-ajax-loader").hide();
            }
        });
    });

    $("#Lselectall").click(function() {
        if (this.checked) {
            $('.lcheckboxall').each(function() {
                $(".lcheckboxall").prop('checked', true);
            });
        } else {
            $('.lcheckboxall').each(function() {
                $(".lcheckboxall").prop('checked', false);
            });
        }
    });

    $(document).on('click', '#lower_delete_btn', function() {
        var id = [];
        var eType = 'Lower';
        $("input[name='Lower_CategoryProduct_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                    title: "Are you sure delete all lower categoryproduct.?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{route('admin.categoryproduct.LowerajaxListing')}}",
                            type: "get",
                            data: {
                                id: id,
                                eType: eType,
                                action: 'multiple_delete'
                            },
                            success: function(response) {
                                $("#lower_table_record").html(response);
                                $("#lower-ajax-loader").hide();
                            }
                        });
                    }
                });
        }
    });

    $("#lower_keyword").keyup(function() {
        var lower_keyword = $("#lower_keyword").val();
        var eType = 'Lower';
        $("#lower-ajax-loader").show();

        $.ajax({
            url: "{{route('admin.categoryproduct.LowerajaxListing')}}",
            type: "get",
            data: {
                lower_keyword: lower_keyword,
                eType: eType,
                action: 'search'
            },
            success: function(response) {
                $("#lower_table_record").html(response);
                $("#lower-ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '#lowerdelete', function() {
        swal({
                title: "Are you sure delete this lower categoryproduct.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");
                    var eType = 'Lower';
                    $("#lower-ajax-loader").show();

                    $.ajax({
                        url: "{{route('admin.categoryproduct.LowerajaxListing')}}",
                        type: "get",
                        data: {
                            id: id,
                            eType: eType,
                            action: 'delete'
                        },
                        success: function(response) {
                            $("#lower_table_record").html(response);
                            $("#lower-ajax-loader").hide();
                            notification_error("Category Product Deleted Successfully");
                                setTimeout(function() {
                                   location.reload();
                                }, 1000);
                        }
                    });
                }
            })
    });
    $(document).on('click', '.lowersort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');
        var eType = 'Lower';


        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#lower-ajax-loader").show();

        $.ajax({
            url: "{{route('admin.categoryproduct.LowerajaxListing')}}",
            type: "get",
            data: {
                column: column,
                order,
                order,
                eType: eType,
                action: 'sort'
            },
            success: function(response) {
                console.log(response);
                $("#lower_table_record").html(response);
                $("#lower-ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '.ajax_page_lower', function() {
        pages = $(this).data("pages");
        var eType = 'Lower';
        $("#lower_table_record").html('');
        $("#lower-ajax-loader").show();

        $.ajax({
            url: "{{route('admin.categoryproduct.LowerajaxListing')}}",
            type: "get",
            data: {
                pages: pages,
                eType: eType,
            },
            success: function(response) {
                $("#lower_table_record").html(response);
                $("#lower-ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#CategoryProduct', function() {
        var eType = 'Lower';
        var CategoryProduct = $("#CategoryProduct").val();
        $("#lower-ajax-loader").show();

        $.ajax({
            url: "{{route('admin.categoryproduct.LowerajaxListing')}}",
            type: "get",
            data: {
                CategoryProduct: CategoryProduct,
                eType: eType,
            },
            success: function(response) {
                $("#lower_table_record").html(response);
                $("#lower-ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#lower_page_limit', function() {
        lower_limit_page = this.value;
        $("#lower_table_record").html('');
        $("#lower-ajax-loader").show();
        url = "{{route('admin.categoryproduct.LowerajaxListing')}}";

        setTimeout(function() {
            $.ajax({
                url: url,
                type: "get",
                data: {
                    lower_limit_page: lower_limit_page
                },
                success: function(response) {
                    $("#lower_table_record").html(response);
                    $("#lower-ajax-loader").hide();
                }
            });
            // hideLoader();
        }, 500);
    });
</script>
@endsection