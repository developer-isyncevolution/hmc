@extends('layouts.admin.index')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css"
        rel="stylesheet">

    <div class="main-panel">
        <div class="page-title text-center">
            <h3>
                {{ isset($categoryproducts->iCategoryProductId) ? 'Edit' : 'Add' }} Product
            </h3>
        </div>
        <div class="card">
            <div class="col-lg-12 mx-auto">
                <div class="card mb-5 mb-xl-4">
                    <div class="card-header border-0">
                        <ul class="c-nav-tab">
                            <li class="">
                                <a href="{{ route('admin.category') }}" class=" add-btn btn mx-2">
                                    Categories
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ route('admin.categoryproduct') }}" class="active btn add-btn  mx-2">
                                    Products
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.productstage') }}" class="btn add-btn mx-2">
                                    Stages
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.addoncategory') }}" class="btn add-btn mx-2">
                                    Categories Add ons
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.subaddoncategory') }}" class="btn add-btn mx-2">
                                    Add ons
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <form action="{{ route('admin.categoryproduct.store') }}" id="frm" class="row g-5 add-product mt-0"
                    method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id"
                        value="@if (isset($categoryproducts)) {{ $categoryproducts->iCategoryProductId }} @endif">

                    <div class="col-xxl-4 col-lg-6 col-md-12 align-self-center">
                        <label class="d-block mb-2">Product Type</label>
                        <input type="radio" class="color_radio d-inline-block" id="Upper" name="eType" value="Upper"
                            @if (isset($categoryproducts)) @if ($categoryproducts->eType == 'Upper') checked @endif @endif>
                        <label for="Upper" class="me-2">Upper</label>
                        <input type="radio" class="color_radio d-inline-block" id="Lower" name="eType" value="Lower"
                            @if (isset($categoryproducts)) @if ($categoryproducts->eType == 'Lower') checked @endif @endif>
                        <label for="Lower" class="me-2">Lower</label>
                        <input type="radio" class="color_radio d-inline-block" id="Both" name="eType" value="Both"
                            @if (isset($categoryproducts)) @if ($categoryproducts->eType == 'Both') checked @endif @endif>
                        <label for="Both" class="me-2">Both</label>
                        <div class="text-danger" style="display: none;" id="eType_error">Please select product type</div>
                    </div>
                    <div class="col-xxl-4 col-lg-6 col-md-12">
                        <label>Category</label>
                        <select name="iCategoryId" id="iCategoryId">
                            <option value="none">Select First Product Type</option>
                            @if (isset($categoryproducts->iCategoryId))
                                @foreach ($category as $key => $categorys)
                                    <option value="{{ $categorys->iCategoryId }}"
                                        @if (isset($categoryproducts)) {{ $categoryproducts->iCategoryId == $categorys->iCategoryId ? 'selected' : '' }} @endif>
                                        {{ $categorys->vName }}</option>
                                @endforeach
                            @endif
                        </select>
                        <div class="text-danger" style="display: none;" id="iCategoryId_error">Please select category
                        </div>
                    </div>
                    <div class="col-xxl-4 col-lg-6 col-md-12">
                        <label>Code</label>
                        <input type="text" class="form-control" id="vCode" name="vCode" placeholder="Code"
                            value="@if (old('vCode') != '') {{ old('vCode') }}@elseif(isset($categoryproducts->vCode)){{ $categoryproducts->vCode }}@else{{ old('vCode') }} @endif">
                        <div class="text-danger" style="display: none;" id="vCode_error">Please enter code</div>
                    </div>
                    <div class="col-xxl-4 col-lg-6 col-md-12">
                        <label>Name</label>
                        <input type="text" class="form-control" id="vName" name="vName" placeholder="Name"
                            value="@if (old('vName') != '') {{ old('vName') }}@elseif(isset($categoryproducts->vName)){{ $categoryproducts->vName }}@else{{ old('vName') }} @endif">
                        <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
                    </div>
                    <div class="row" id="checked_box_grade">
                        <div class="col-md-12">
                            <label class="my-2"><strong>Select Grades</strong></label>
                        </div>
                        @if (isset($categoryproducts) && !empty($categoryproducts))
                            @if (isset($prograde) && !empty($prograde) && isset($grades) && !empty($grades))
                           
                                @foreach ($grades as $key => $value)
                                @if (in_array($value->iGradeId, $prograde))
                                        <div class="col-md-1">
                                            <label
                                                for="iGradeId_{{ $value->iGradeId }}">{{ $value->vQualityName }}</label>
                                            <input class="Grades_check" type="checkbox" id="iGradeId_{{ $value->iGradeId }}" name="iGradeId[]"
                                                value="{{ $value->iGradeId }}" checked>
                                       
                                        </div>
                                        <div class="col-md-3">
                                        @if(isset($product_grade[$key]->vPrice) && !empty($product_grade[$key]->vPrice))
                                        <input class="form-control vGradePriceChange" placeholder="price" type="number"  data-id ="{{$key}}"  name="vGradePrice[]" id="vGradePrice_{{ $value->iGradeId }}" value="{{ $product_grade[$key]->vPrice }}">  
                                        @else
                                        <input class="form-control vGradePriceChange" placeholder="price" type="number"  data-id ="{{$key}}"  name="vGradePrice[]" id="vGradePrice_{{ $value->iGradeId }}" value="">  
                                        @endif
                                        <div class="text-danger" style="display: none;" id="vGradePrice_error{{ $value->iGradeId }}">Select grade price</div>
                                        </div>
                                       
                                    @else
                                        <div class="col-md-1">
                                            <label for="iGradeId_{{ $value->iGradeId }}">{{ $value->vQualityName }}</label>
                                            <input  type="checkbox" id="iGradeId_{{ $value->iGradeId }}" name="iGradeId[]"
                                                value="{{ $value->iGradeId }}">
                                        </div>
                                        <div class="col-md-3">
                                            <input placeholder="price" class="form-control vGradePriceChange"  data-id ="{{$key}}"  style="display: none;" type="number" name="vGradePrice[]" id="vGradePrice_{{ $value->iGradeId }}" value="">  
                                            <div class="text-danger" style="display: none;" id="vGradePrice_error{{ $value->iGradeId }}">Select grade price</div>
                                        </div>
                                    @endif
                                @endforeach
                                @else
                                
                                @foreach ($grades as $key => $value)
                                    <div class="col-md-1">
                                        <label for="iGradeId_{{ $value->iGradeId }}">{{ $value->vQualityName }}</label>
                                        <input type="checkbox" id="iGradeId_{{ $value->iGradeId }}" name="iGradeId[]"
                                            value="{{ $value->iGradeId }}">
                                    </div>
                                        <div class="col-md-3">
                                            <input placeholder="price" class="form-control vGradePriceChange"   data-id ="{{$key}}"  style="display: none;" type="number" name="vGradePrice[]" id="vGradePrice_{{ $value->iGradeId }}" value="">  
                                            <div class="text-danger" style="display: none;" id="vGradePrice_error{{ $value->iGradeId }}">Select grade price</div>
                                        </div>
                                @endforeach
                            @endif
                        
                        
                        @else
                        @foreach ($grades as $key => $value)
                        <div class="col-md-1">
                            <label for="iGradeId_{{ $value->iGradeId }}">{{ $value->vQualityName }}</label>
                            <input type="checkbox" id="iGradeId_{{ $value->iGradeId }}" name="iGradeId[]"
                                value="{{ $value->iGradeId }}">
                               
                        </div>
                        <div class="col-md-3">
                            <input placeholder="price" class="form-control vGradePriceChange" style="display: none;" type="number" name="vGradePrice[]" data-id ="{{$key}}" id="vGradePrice_{{ $value->iGradeId }}" value="">  
                            <div class="text-danger" style="display: none;" id="vGradePrice_error{{ $value->iGradeId }}">Select grade price</div>
                        </div>
                    @endforeach
                        @endif
                        <div class="text-danger" style="display: none;" id="iGradeId_error">Select any one grade </div>
                    </div>

                    {{-- Shades section start --}}
                    <div class="row" id="checked_box_gumshades">
                        <div class="col-md-12">
                            <label class="my-2"><strong>Select Gum Shades</strong></label>
                        </div>

                        @foreach ($gumshades as $key => $gumshadesvalue)
                            <div class="col-md-3">
                                <strong>{{ $gumshadesvalue->vBrnadName }}</strong>
                                @foreach ($gumshadesvalue->gumshades as $key_shade => $value)
                                    @php
                                        $checked = "";
                                    @endphp
                                    @if(in_array($value->iGumShadesId, $proshades))
                                        @php
                                            $checked = "checked";
                                        @endphp
                                    @endif

                                    <div class="form-control">
                                        <label for="iGumShadesId_{{ $value->iGumShadesId }}">{{$value->vGumShade }}</label>
                                        <input class="" type="checkbox" id="iGumShadesId_{{ $value->iGumShadesId }}" name="iGumShadesId[]" value="{{ $value->iGumShadesId }} " {{$checked}}>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    
                    </div>

                    {{-- Shades section end --}}
                    <div class="col-xxl-4 col-lg-6 col-md-12">
                        <label>Clasp to repair or add</label>
                        <select id="eClaspAdd" name="eClaspAdd">
                            <option value="Yes"
                                @if (isset($categoryproducts)) @if ($categoryproducts->eClaspAdd == 'Yes') selected @endif
                                @endif>Yes</option>
                            <option value="No"
                                @if (isset($categoryproducts)) @if ($categoryproducts->eClaspAdd == 'No') selected @endif
                                @endif>No</option>
                        </select>
                    </div>
                    <div class="col-xxl-4 col-lg-6 col-md-12">
                        <label>Status</label>
                        <select id="eStatus" name="eStatus">
                            <option value="Active"
                                @if (isset($categoryproducts)) @if ($categoryproducts->eStatus == 'Active') selected @endif
                                @endif>Active</option>
                            <option value="Inactive"
                                @if (isset($categoryproducts)) @if ($categoryproducts->eStatus == 'Inactive') selected @endif
                                @endif>Inactive</option>
                        </select>
                    </div>

                    <div class="col-xxl-4 col-lg-6 col-md-12 vPrice">
                        <label>Price</label>
                        <div id="toolbar-container"></div>
                        <input type="number" class="form-control" id="vPrice" name="vPrice" placeholder="Price"
                            value="@if (old('vPrice') != '') {{ old('vPrice') }}@elseif(isset($categoryproducts->vPrice)){{ $categoryproducts->vPrice }}@else{{ old('vPrice') }} @endif">
                        <div id="vPrice_error" class="text-danger" style="display: none;">Please enter price </div>
                    </div>
                    <div class="col-xxl-4 col-lg-6 col-md-12">
                        <label>Sequence</label>
                        <div id="toolbar-container"></div>
                        <input type="number" class="form-control" id="iSequence" name="iSequence" placeholder="Sequence"
                            value="@if (old('iSequence') != '') {{ old('iSequence') }}@elseif(isset($categoryproducts->iSequence)){{ $categoryproducts->iSequence }}@else{{ old('iSequence') }} @endif">

                        <div id="iSequence_error" class="text-danger" style="display: none;">Please enter Sequence </div>
                    </div>
                    <div class="col-xxl-4 col-lg-6 col-md-12">
                        <label class="fs-5 fw-bold mb-2">Products image</label>
                        <input class="form-control" type="file" id="vImage" name="vImage" accept="image/*">
                        @if (isset($categoryproducts))
                            <img style="width: 100px;" id="img" value="@if (old('vImage') == 'vImage') selected @endif"
                                src="{{ asset('uploads/categoryproduct/' . $categoryproducts->vImage) }}">
                        @else
                            <img style="width: 100px;" id="img" value="" src="{{ asset('images/no-image.gif') }}">
                        @endif
                        <div id="vImage_error" class="text-danger" style="display: none;">Select products image</div>
                    </div>
                    <div class="col-xxl-4 col-lg-6 col-md-12" id="lower_image" style="display: none">
                        <label class="fs-5 fw-bold mb-2"> Products lower image</label>
                        <input class="form-control" type="file" id="vLowerImage" name="vLowerImage" accept="image/*">
                        @if (isset($categoryproducts))
                            <img style="width: 100px;" id="lowerimg"
                                value="@if (old('vLowerImage') == 'vLowerImage') selected @endif"
                                src="{{ asset('uploads/categoryproduct/' . $categoryproducts->vLowerImage) }}">
                        @else
                            <img style="width: 100px;" id="lowerimg" value="" src="{{ asset('images/no-image.gif') }}">
                        @endif
                    </div>

                    {{-- Office Prices show start --}}
                    <hr>
                    <div class="col-lg-6 ">
                        <h5 class="pt-3">Office Prices</h5>
                        <div class="listing-page">
                           <div class="table-data table-responsive">
                              <table class="table">
                                 <thead>
                                    <tr>
                                       <th class="min-w-100px">
                                          <span class="text-muted fw-bold text-muted d-block fs-7">Office Name</span>
                                       </th>
                                       <th class="min-w-100px MainOfficePrice">
                                          <span class="text-muted fw-bold text-muted d-block fs-7">Office Price</span>
                                       </th>
                                       @if(isset($grades) && !empty($grades))
                                        @foreach ($grades as $key => $value)
                                        <th style="display: none;" class="min-w-100px vGradeOfficePrice_{{$value->iGradeId}}">
                                            <span class="text-muted fw-bold text-muted d-block fs-7">{{ $value->vQualityName }}</span>
                                        </th>
                                        @endforeach
                                       @endif
                                    </tr>
                                 </thead>
                                 <tbody>
                                     @if(isset($edit) && !empty($edit) && $edit =='Yes') 
                                            @if(isset($office) && !empty($office) && count($office) >0)
                                            @foreach($office as $key => $office_val)
                                            <tr>
                                                <td>{{isset($office_val['vOfficeName'])?$office_val['vOfficeName']:'N/A'}}</td>
                                                <td class="MainOfficePrice" >
                                                    <input type="number" name="Office[{{$key}}][vOfficePrice]"  class="form-control vOfficePrice " id="vOfficePrice" value="{{isset($office_val['vPrice'])?$office_val['vPrice']:''}}" required>
                                                </td>
                                                @if(isset($office_val['grades']) && !empty($office_val['grades']))
                                                    @foreach ($office_val['grades'] as $key_grade => $value_grade)
                                                    <td style="display: none;" class="vGradeOfficePrice_{{$value_grade['iGradeid']}}">
                                                        <input type="number" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][vOfficePrice]"  class="form-control vGradePrice_{{$key_grade}}" id="" value="{{isset($value_grade['vPrice'])?$value_grade['vPrice']:''}}" required>
                                                    </td>
                                                    <input type="hidden" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][iGradeId]" value="{{isset($value_grade['iGradeid'])?$value_grade['iGradeid']:''}}">
                                                    @endforeach
                                                    @else
                                                    @foreach ($grades as $key_grade => $value_grade)
                                                    <td style="display: none;" class="vGradeOfficePrice_{{$value_grade->iGradeId}}">
                                                        <input type="number" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][vOfficePrice]"  class="form-control vGradePrice_{{$key_grade}}" id="" value="" required>
                                                    </td>
                                                    <input type="hidden" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][iGradeId]" value="{{isset($value_grade->iGradeId)?$value_grade->iGradeId:''}}">
                                                    @endforeach

                                                @endif
                                                <input type="hidden" name="Office[{{$key}}][iOfficeId]" value="{{isset($office_val['iOfficeId'])?$office_val['iOfficeId']:''}}">
                                                <input type="hidden" name="Office[{{$key}}][vOfficeName]" value="{{isset($office_val['vOfficeName'])?$office_val['vOfficeName']:''}}">
                                              
                                                <input type="hidden" name="Office[{{$key}}][iProductId]" value="{{isset($office_val['iCategoryProductId'])?$office_val['iCategoryProductId']:''}}">
                                            
                                            </tr>
                                            @endforeach
                                        @endif
                                     @else
                                        @if(isset($office) && !empty($office) && count($office) >0)
                                            @foreach($office as $key => $office_val)
                                            <tr>
                                                <td>{{isset($office_val->vOfficeName)?$office_val->vOfficeName:'N/A'}}</td>
                                                <td class="MainOfficePrice" >
                                                    <input type="number" name="Office[{{$key}}][vOfficePrice]"  class="form-control vOfficePrice " id="vOfficePrice" value="{{isset($office_val->vOfficePrice)?$office_val->vOfficePrice:''}}" required>
                                                </td>
                                                @if(isset($grades) && !empty($grades))
                                                    @foreach ($grades as $key_grade => $value_grade)
                                                    <td style="display: none;" class="vGradeOfficePrice_{{$value_grade->iGradeId}}">
                                                        <input type="number" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][vOfficePrice]"  class="form-control vGradePrice_{{$key_grade}}" id="" value="{{isset($office_val->vOfficePrice)?$office_val->vOfficePrice:''}}" required>
                                                    </td>
                                                    <input type="hidden" name="Office[{{$key}}][OfficeGrade][{{$key_grade}}][iGradeId]" value="{{isset($value_grade->iGradeId)?$value_grade->iGradeId:''}}">
                                                    @endforeach
                                                @endif
                                                <input type="hidden" name="Office[{{$key}}][iOfficeId]" value="{{isset($office_val->iCustomerId)?$office_val->iCustomerId:$office_val->iOfficeId}}">
                                                <input type="hidden" name="Office[{{$key}}][vOfficeName]" value="{{$office_val->vOfficeName}}">
                                                <input type="hidden" name="Office[{{$key}}][iOfficeProductPriceId]" value="{{isset($office_val->iOfficeProductPriceId)?$office_val->iOfficeProductPriceId:''}}">
                                                <input type="hidden" name="Office[{{$key}}][iProductId]" value="{{isset($categoryproducts->iCategoryProductId)?$categoryproducts->iCategoryProductId:''}}">
                                            
                                            </tr>
                                            @endforeach
                                        @endif
                                        @endif
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        
                     </div>
                    {{-- Office Prices show end --}}
                    <div class="col-12 align-self-end d-inline-block ">
                        <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
                        <a href="{{ route('admin.categoryproduct') }}" class="btn back-btn me-2">Back</a>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
@endsection

@section('custom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js">
    </script>
    <script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
    <script>
        $('#iCategoryId').selectize();
        $('#eStatus').selectize();
        $('#eType').selectize();
        $('#eClaspAdd').selectize();
       

        // Grades checkbox change start
        $(document).ready(function() {
            $("input[type=checkbox]").change(function() {
           
            var ischecked= $(this).is(':checked');
            if($(this).attr("name") !='iGumShadesId[]')
            {     
                if(ischecked == true){
                  
                        $('#vGradePrice_'+$(this).val()).show();
                        $('.vGradeOfficePrice_'+$(this).val()).show();
                        $('.vPrice').hide();
                        $('.MainOfficePrice').hide();
                    }
                    else{    
                        // if ($('.Grades_check:checked').length == $('.Grades_check').length) {
                        //     $('.vPrice').show();
                        //     $('.MainOfficePrice').show();
                        // }
                        $('input:checkbox.Grades_check').each(function () {
                           if(this.checked)
                           {
                              
                                $('.vPrice').hide();
                                $('.vOfficePrice').hide();
                                $('.MainOfficePrice').hide();
                           }
                           else
                           {
                                $('.vPrice').show();
                                $('.vOfficePrice').show();
                                $('.MainOfficePrice').show();
                           }
                        });

                      
                    
                        
                        
                        $('#vGradePrice_'+$(this).val()).hide(); 
                        $('.vGradeOfficePrice_'+$(this).val()).hide();
    
                        
                    }
            }

            }); 
        });
        
        // Grades checkbox change end

        $(document).ready(function() {
            // Grades checkbox change start
            if ($('input.Grades_check').is(':checked')) {
                // $('#vPrice').val('');
                $('.vPrice').hide();
            }
            // Grades checkbox change end
            
            
            $("input[name='iGradeId[]']:checked").each(function() {
               
                // var vGradePrice = $('#vGradePrice_'+$(this).val()).val();
                // console.log($(this).val());
                $('#vGradePrice_'+$(this).val()).show();
                $('.vGradeOfficePrice_'+$(this).val()).show();
                $('.vOfficePrice').hide();
                $('.MainOfficePrice').hide();
             
            });

            
            var eType = $('input[name="eType"]:checked').val();
            if (eType == "Both") {
                $("#lower_image").show();
            } else {
                $("#lower_image").hide();
            }
        });

        $(document).on('change', '#vImage', function() {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#img').attr('src', e.target.result);
                };
                reader.readAsDataURL(this.files[0]);
            }
        });
        $(document).on('change', '#vLowerImage', function() {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#lowerimg').attr('src', e.target.result);
                };
                reader.readAsDataURL(this.files[0]);
            }
        });
        $(document).on('click', '.color_radio', function() {
            var eType = $('input[name="eType"]:checked').val();

            if (eType == "Both") {
                $("#lower_image").show();
            } else {
                $("#lower_image").hide();
            }
            $.ajax({
                url: "{{ route('admin.categoryproduct.fetch_category') }}",
                type: "POST",
                data: {
                    eType: eType,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $('#iCategoryId').selectize()[0].selectize.destroy();
                    $("#iCategoryId").html(response);
                    $('#iCategoryId').selectize();
                }
            });
        });

        $(document).on('click', '#submit', function() {
            iCategoryProductId = $("#id").val();
            vOfficePrice = $(".vOfficePrice").val();
            vCode = $("#vCode").val();
            iCategoryId = $("#iCategoryId").val();
            iQuantity = $("#iQuantity").val();
            vName = $("#vName").val();
            iSequence = $("#iSequence").val();
            vPrice = $("#vPrice").val();

           

            var error = false;
            // Grades checkbox change start
            $("input[name='iGradeId[]']:checked").each(function() {
                var vGradePrice = $('#vGradePrice_'+$(this).val()).val();
                if(vGradePrice.length == 0)
                {
                    $("#vGradePrice_error"+$(this).val()).show();
                    error = true;
                }  
                else
                {
                    $("#vGradePrice_error"+$(this).val()).hide();
                }
            });
            // Grades checkbox change end
            // if(vOfficePrice.length == 0)
            // {
            //     alert('Please Select Office Price');
            //     error = true;
            // }    
           
            if ($('input[name="eType"]:checked').length == 0) {
                $("#eType_error").show();
                error = true;
            } else {
                $("#eType_error").hide();
            }

            if (vCode.length == 0) {
                $("#vCode_error").show();
                error = true;
            } else {
                $("#vCode_error").hide();
            }

            if (iCategoryId == 'none') {
                $("#iCategoryId_error").show();
                error = true;
            } else {
                $("#iCategoryId_error").hide();
            }

            if (vName.length == 0) {
                $("#vName_error").show();
                error = true;
            } else {
                $("#vName_error").hide();
            }

            // if ($('input[type="checkbox"]:checked').length == 0) {
            //   $("#iGradeId_error").show();
            //   error = true;
            // } else {
            //   $("#iGradeId_error").hide();
            // }
            if($('.vPrice').css('display') != 'none')
            {
                if (vPrice.length == 0) {
                    $("#vPrice_error").show();
                    error = true;
                } else {
                    $("#vPrice_error").hide();
                }
            }

            if (iSequence.length == 0) {
                $("#iSequence_error").show();
                error = true;
            } else {
                $("#iSequence_error").hide();
            }

            setTimeout(function() {
                if (error == true) {
                    return false;
                } else {
                    $("#frm").submit();
                    return true;
                }
            }, 1000);

        });

        //For update office price start
        $("#vPrice").keyup(function(){
            $('.vOfficePrice').val(this.value);
        });
        //For update office price end


        $(".vGradePriceChange").keyup(function(){
            var stage_prce_id = $(this).attr("data-id");
            var stage_prce_val = $(this).val();
            $(".vGradePrice_"+stage_prce_id).val(stage_prce_val);  
        });
    </script>
@endsection
