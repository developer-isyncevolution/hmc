<div class="row" id="checked_box_gumshades">
    <div class="col-md-12">
        <label class="my-2">Select Gum Shades</label>
    </div>
    @if (isset($categoryproducts) && !empty($categoryproducts))
    {{-- {{dd($gumshades);}} --}}
        @if (isset($proshades) && !empty($proshades) && isset($gumshades) && !empty($gumshades))
             @foreach ($gumshades as $key => $gumshadesvalue)
                <div class="col-md-3">
                    <h6 >{{ $key }}</h6>
                
                    {{-- @foreach ($gumshadesvalue as $key => $value) --}}
                      
                        @if (in_array($gumshadesvalue['iGumShadesId'], $proshades))
                        <div class="col-md-4">
                            <label
                                for="iGumShadesId_{{ $gumshadesvalue['iGumShadesId'] }}">{{ $gumshadesvalue['vGumShade'] }}</label>
                            <input class="Grades_check" type="checkbox" id="iGumShadesId_{{ $gumshadesvalue['iGumShadesId'] }}" name="iGumShadesId[]"
                                value="{{ $gumshadesvalue['iGumShadesId'] }}" checked>
                    
                        </div>
                        @else
                            <div class="col-md-4">
                                <label for="iGumShadesId_{{ $gumshadesvalue['iGumShadesId']}}">{{ $gumshadesvalue['vGumShade']}}</label>
                                <input  type="checkbox" id="iGumShadesId_{{ $gumshadesvalue['iGumShadesId'] }}" name="iGumShadesId[]"
                                    value="{{ $gumshadesvalue['iGumShadesId'] }}">
                            </div>
                        
                        @endif
                    {{-- @endforeach --}}
                </div>
             @endforeach
            @else
            @foreach ($gumshades as $key => $gumshadesvalue)
            <div class="col-md-6">
                    <h6 >{{ $key }}</h6>
                    <label for="iGumShadesId_{{ $gumshadesvalue['iGumShadesId'] }}">{{ $gumshadesvalue['vGumShade'] }}</label>
                    <input type="checkbox" id="iGumShadesId_{{ $gumshadesvalue['iGumShadesId'] }}" name="iGumShadesId[]"
                        value="{{ $gumshadesvalue['iGumShadesId'] }}">
                </div>
            @endforeach
        @endif
        @foreach ($gumshades as $key => $gumshadesvalue)
        <div class="col-md-6">
                <h6 >{{ $key }}</h6>
                <label for="iGumShadesId_{{ $gumshadesvalue['iGumShadesId'] }}">{{ $gumshadesvalue['vGumShade'] }}</label>
                <input type="checkbox" id="iGumShadesId_{{ $gumshadesvalue['iGumShadesId'] }}" name="iGumShadesId[]"
                    value="{{ $gumshadesvalue['iGumShadesId'] }}">
            </div>
        @endforeach
    @endif
    {{-- @if (isset($categoryproducts) && !empty($categoryproducts))
        @if (isset($proshades) && !empty($proshades) && isset($gumshades) && !empty($gumshades))
       
            @foreach ($gumshades as $key => $value)
            @if (in_array($value->iGumShadesId, $proshades))
                    <div class="col-md-4">
                        <label
                            for="iGumShadesId_{{ $value->iGumShadesId }}">{{ $value->vGumShade }}</label>
                        <input class="Grades_check" type="checkbox" id="iGumShadesId_{{ $value->iGumShadesId }}" name="iGumShadesId[]"
                            value="{{ $value->iGumShadesId }}" checked>
                   
                    </div>
                @else
                    <div class="col-md-4">
                        <label for="iGumShadesId_{{ $value->iGumShadesId }}">{{ $value->vGumShade }}</label>
                        <input  type="checkbox" id="iGumShadesId_{{ $value->iGumShadesId }}" name="iGumShadesId[]"
                            value="{{ $value->iGumShadesId }}">
                    </div>
                   
                @endif
            @endforeach
            @else
            @foreach ($gumshades as $key => $value)
                <div class="col-md-4">
                    <label for="iGumShadesId_{{ $value->iGumShadesId }}">{{ $value->vGumShade }}</label>
                    <input type="checkbox" id="iGumShadesId_{{ $value->iGumShadesId }}" name="iGumShadesId[]"
                        value="{{ $value->iGumShadesId }}">
                </div>
                    
            @endforeach
        @endif
    
    
    @else
    @foreach ($gumshades as $key => $value)
    <div class="col-md-4">
        <label for="iGumShadesId_{{ $value->iGumShadesId }}">{{ $value->vGumShade }}</label>
        <input type="checkbox" id="iGumShadesId_{{ $value->iGumShadesId }}" name="iGumShadesId[]"
            value="{{ $value->iGumShadesId }}">
           
    </div>
    @endforeach
    @endif --}}


  
</div>