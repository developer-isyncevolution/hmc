@if(count($data) > 0)
@foreach($data as $key => $value)
<tr>
	<!-- <td>
		<div class="form-check form-check-sm form-check-custom form-check-solid">
			<input id="CategoryProduct_ID_{{$value->iCategoryProductId}}" type="checkbox" name="CategoryProduct_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iCategoryProductId}}">
			<label for="CategoryProduct_ID_{{$value->iCategoryProductId}}">&nbsp;</label>
		</div>
	</td> -->
	<td>
		@if(isset($value->vImage))
			<img alt="{{$value->vImage}}" style="height: 30px;" src="{{asset('uploads/categoryproduct/'.$value->vImage)}}">
		@else 
		   <img style="width: 30px;" id="img" value="" src="{{asset('images/no-image.gif')}}">
		@endif
		{{-- @if($value->eType == 'Both')	
		<hr>
			@if(isset($value->vLowerImage))
				<img alt="{{$value->vLowerImage}}" style="height: 30px;" src="{{asset('uploads/categoryproduct/'.$value->vLowerImage)}}">
			@endif
		@endif	 --}}
	</td>
	<td>
		{{$value->vCode}}
	</td>
	<td>{{ $value->vName }}</td>
	<td>{{ $value->iSequence }}</td>
	<td>
		<span class="badge badge-light-success">
			{{$value->eStatus}}
		</span>
	</td>
	<td>
		<div class="d-inline-flex align-items-center">
			@if(\App\Libraries\General::check_permission('','eEdit') == true)
			<a href="{{route('admin.categoryproduct.edit',$value->iCategoryProductId)}}" class="btn-icon edit_permission edit-icon me-4">
			<i class="fad fa-pencil"></i>
			</a>
			@endif
			@if(\App\Libraries\General::check_permission('','eDelete') == true)
			<a href="javascript:;" id="delete" data-id="{{$value->iCategoryProductId}}" class="btn-icon delete-icon delete_permission me-4">
			<i class="fad fa-trash-alt"></i>
			</a>
			@endif
			@if(\App\Libraries\General::check_permission('ProductStageController','eRead') == 'true')
			<a href="{{route('admin.productstageselect',$value->iCategoryProductId)}}" class="list-label-btn2 edit_permission edit-icon me-4">Stage</a>
			@endif
		</div>
	</td>

</tr>
@endforeach
<tr>
	{{-- <td  class="text-center border-right-0">
		<a href="javascript:;" title="Multiple delete" id="delete_btn" class="btn-icon  delete_permission delete-icon">
		<i class="fad fa-trash-alt"></i>
		</a>
	</td> --}}
	{{-- <td class="border-0"></td>
	<td colspan="4" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
	<td class="border-0" colspan="4">
		<div class="d-flex">
			{!! $paging !!}
		</div>
	</td>
	<td colspan="2"  class="text-end border-left-0">
		{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}} --}}
		{{-- {{ dd($data) }} --}}
	<select class="w-100px show-drop" id="upper_page_limit">
		<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 10)
			selected
			@endif value="10">10</option>
		<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 20)
			selected
			@endif value="20">20</option>
		<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 50)
			selected
			@endif value="50">50</option>
		<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 100)
			selected
			@endif value="100">100</option>
	</select>
	</td>
</tr>
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif