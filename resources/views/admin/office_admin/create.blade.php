@extends('layouts.admin.index')
@section('content')
<div class="main-panel">
<div class="page-title text-center">
        <h3>
        Add Office Admin 
        </h3> 
    </div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
     
      <form action="{{route('admin.office_admin.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @if(isset($lab_data->iOfficeAdminId) && $lab_data->iOfficeAdminId!='')
        <input type="hidden" name="iOfficeAdminId" value="{{$lab_data->iOfficeAdminId}}">
        @endif
        @csrf
        @if(\App\Libraries\General::admin_info()['customerType']=='Super Admin')
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Customers</label>
            <select name="iCustomerId" id="iCustomerId">
             
              @if(isset($iCustomerId_New) && !empty($iCustomerId_New))
                @foreach($customer as $key => $customers)
                <option value="{{$customers->iCustomerId}}" @if(isset($iCustomerId_New)){{($customers->iCustomerId == $iCustomerId_New ) ? 'selected' : ''}} @endif>{{$customers->vOfficeName}}</option>
                @endforeach
              @else
                @foreach($customer as $key => $customers)
                <option value="{{$customers->iCustomerId}}" @if(isset($lab_data)){{($customers->iCustomerId == $lab_data->iCustomerId ) ? 'selected' : ''}} @endif>{{$customers->vOfficeName}}</option>
                @endforeach
              @endif
            
            </select>
        </div>
        @endif
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>First name</label>
          <input type="text" class="form-control" id="vFirstName" name="vFirstName" placeholder="First name" value="@if(isset($lab_data->vFirstName)){{$lab_data->vFirstName}}@endif">
          <div class="text-danger" style="display: none;" id="vFirstName_error">Please enter first name</div>
        </div>

        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Last name</label>
          <input type="text" class="form-control" id="vLastName" name="vLastName" placeholder="Last name" value="@if(isset($lab_data->vLastName)){{$lab_data->vLastName}}@endif">
          <div class="text-danger" style="display: none;" id="vLastName_error">Please enter last name</div>
        </div>
    
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Email</label>
          <input type="text" @if(isset($lab_data->vEmail)) @if (\App\Libraries\General::admin_info()['customerType'] != 'Super Admin') readonly @endif @endif class="form-control" id="vEmail" name="vEmail" placeholder="Email" value="@if(old('vEmail')!=''){{old('vEmail')}}@elseif(isset($lab_data->vEmail)){{$lab_data->vEmail}}@else{{old('vEmail')}}@endif">
          <div class="text-danger" style="display: none;" id="vEmail_error">Please enter email</div>
          <div class="text-danger" style="display: none;" id="vEmail_valid_error">Please enter valid email</div>
        </div>
        @if(!isset($lab_data->iOfficeAdminId) )
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Password</label>
            <input type="password" class="form-control" id="vPassword" name="vPassword" placeholder="Password" value="">
            <div class="text-danger" style="display: none;" id="vPassword_error">Please enter password</div>
          </div>
        @endif
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Mobile</label>
          <input type="text" class="form-control" id="vMobile" name="vMobile" placeholder="Mobile" value="@if(old('vMobile')!=''){{old('vMobile')}}@elseif(isset($lab_data->vMobile)){{$lab_data->vMobile}}@else{{old('vMobile')}}@endif">
          <div class="text-danger" style="display: none;" id="vMobile_error">Please enter mobile</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Cell</label>
          <input type="text" class="form-control" id="vCellulor" name="vCellulor" placeholder="Cell" value="@if(old('vCellulor')!=''){{old('vCellulor')}}@elseif(isset($lab_data->vCellulor)){{$lab_data->vCellulor}}@else{{old('vCellulor')}}@endif">
          <div class="text-danger" style="display: none;" id="vCellulor_error">Please enter cell</div>
        </div>
      
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Status</label>
          <select id="eStatus" name="eStatus">
            <option value="Active" @if(isset($lab_data)) @if($lab_data->eStatus == 'Active') selected @endif @endif>Active</option>
            <option value="Inactive" @if(isset($lab_data)) @if($lab_data->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
            <option value="Stand By" @if(isset($lab_data)) @if($lab_data->eStatus == 'Stand By') selected @endif @endif>Stand By</option>
            <option value="Suspended" @if(isset($lab_data)) @if($lab_data->eStatus == 'Suspended') selected @endif @endif>Suspended</option>
          </select>
        </div>

        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Profile Picture</label>
         
          <input class="form-control" type="file" id="vImage" name="vImage" accept="image/*">
          @if(isset($lab_data->vImage))
             <img style="width: 100px;" id="img" value="@if(old('vImage') == 'vImage') selected @endif" src="{{asset('uploads/lab_office/'.$lab_data->vImage)}}">
          @else 
             <img style="width: 100px;" id="img" value="" src="{{asset('images/no-image.gif')}}">
          @endif
         <div class="text-danger" style="display: none;" id="vImage_error">select logo</div>
        </div>
        
        <div class="col-12 align-self-end d-inline-block">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.office_admin')}}" class="btn back-btn">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
   <script>
 $('#eStatus').selectize();
 $('#iCustomerId').selectize();
 $('#iStateId').selectize();

  $(document).on('change', '#vImage', function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#img').attr('src', e.target.result);
      };
      reader.readAsDataURL(this.files[0]);
    }
  });
  $(document).on('click', '#submit', function() {
    vFirstName      = $("#vFirstName").val();
    vLastName      = $("#vLastName").val();
    vEmail    = $("#vEmail").val();
    vPassword       = $("#vPassword").val();
    vMobile    = $("#vMobile").val();
    vCellulor      = $("#vCellulor").val();
    vPassword      = $("#vPassword").val();

    var error = false;
    if (vFirstName.length == 0) {
      $("#vFirstName_error").show();
      error = true;
    } else {
      $("#vFirstName_error").hide();
    }

    if (vLastName.length == 0) {
      $("#vLastName_error").show();
      error = true;
    } else {
      $("#vLastName_error").hide();
    }
    @if(!isset($lab_data->iOfficeAdminId))
    {
      if (vPassword.length == 0) {
        $("#vPassword_error").show();
        error = true;
      } else {
        $("#vPassword_error").hide();
      }
    }
    @endif
    // Email validtion start
   if (vEmail.length == 0) {
      $("#vEmail_error").text("Please enter email");
      $("#vEmail_error").show();
      $("#vEmail_valid_error").hide();
        error = true;
    }else{
      if(validateEmail(vEmail))
      {
        $("#vEmail_valid_error").hide();
        @if(!isset($lab_data))
          $.ajax({
            url: "{{route('admin.staff.CheckExistEmail')}}",
            type: "post",
            data: {
                vEmail : $('#vEmail').val(),
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
              if(response == 1)
              {
                $('#vEmail_error').show();
                $("#vEmail_error").text("This email alredy exist");
                error = true;
                return false;
              }
              else
              {
                $('#vEmail_error').hide();
                $('#vEmail_valid_error').hide();
                return true;
              }
            }
          }); 
        @endif  
      }else{
        $("#vEmail_valid_error").show();
        $("#vEmail_error").hide();
        error = true;
      }
    }
// Email validtion end
 
    
    
    
   

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
function validateEmail(vEmail) 
{
  var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  if (filter.test(vEmail)) {
    return true;
  }
  else {
    return false;
  }
}
$(document).ready(function() {
    var mobile = [{ "mask": "### ### ####"}, { "mask": "### ### ####"}];
    $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
   })
  $(document).ready(function() {
  var cellulor = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-####"}];
  $('#vCellulor').inputmask({ 
      mask: cellulor, 
      greedy: false, 
      definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
  })
</script>
@endsection