@php 

$general_logo = \App\Libraries\General::setting_info('Company');
// dd($general_logo);
$logo_main         = $general_logo['COMPANY_LOGO']['vValue'];

$general_info    = \App\Libraries\General::setting_info('Appearance');
@endphp

<!DOCTYPE html>

<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <title>{{$general_info['CPANEL_TITLE']['vValue']}}</title>
    <meta charset="utf-8" />
    <link rel="icon" type="image/ico" href="{{ asset('admin/assets/images/logo/favicon.png') }}">
  
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
   
 
    {{-- <link rel="shortcut icon" href="assets/media/logos/favicon.ico" /> --}}
 
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
    @include('layouts.admin.css')

    <!-- inc css -->
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body">
    <main class="login-form">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <h3 class="card-header text-center my-2">
                        <img src="{{asset('uploads/logo/'.$logo_main)}}" class="img-fluid h-125px">
                    </h3>

                    <div class="card-body">
                        <form method="POST" id="frm" action="{{ route('login_action') }}">
                            @csrf
                            <div class="form-group mb-3">
                                <input type="text" placeholder="Email" id="email" class="form-control" name="email"
                                    required autofocus>
                                <div class="text-danger" style="display: none;" id="email_error">Enter Email</div>
                            </div>
                            <div class="form-group mb-3">
                                <input type="password" placeholder="Password" id="password" class="form-control"
                                    name="password" required>
                                <div class="text-danger" style="display: none;" id="password_error">Enter Password
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <div class="checkbox d-inline-block">
                                    <label class="align-self-center d-flex">
                                        <input type="checkbox" name="remember" class="me-1">
                                        <span>Remember Me</span>
                                    </label>
                                </div>
                            </div>
                            <div class="d-grid mx-auto">
                                <a type="submit" id="submit" class="btn submit-btn">Login</a>
                            </div>
                            <div class="link-link">
                                <div>
                                    <a href="{{route('admin.admin.forgotPassword')}}" type="submit" class="for">Forgot your
                                        Password?</a>
                                </div>
                                <div>
                                    <a href="{{ url('/admin/register/customer') }}" class="for">Register New
                                        Costumer</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    @if ($message = Session::get('success'))
        <script>
            $( window ).on( "load", function() {
                notification_success("{{$message}}"); 
            });
        </script>
    @endif
    @if ($message = Session::get('error'))
        <script>
            $( window ).on( "load", function() {
                notification_error("{{$message}}"); 
            });
        </script>
    @endif



    <!--end::Footer-->

    <!--end::Root-->
    <!--begin::Drawers-->
    <!--begin::Activities drawer-->

    <!--end::Activities drawer-->

    <!--end::Chat drawer-->
    <!--end::Drawers-->
    <!--end::Main-->
    <!--begin::Engage drawers-->
    <!--begin::Demos drawer-->

    <!--end::Demos drawer-->
    <!--end::Engage drawers-->
    <!--begin::Engage toolbar-->
    <!-- <div class="engage-toolbar d-flex position-fixed px-5 fw-bolder zindex-2 top-50 end-0 transform-90 mt-10 mt-lg-20 gap-2">
            begin::Demos drawer toggle
            <button id="kt_engage_demos_toggle" class="engage-demos-toggle btn btn-flex h-35px bg-body btn-color-gray-700 btn-active-color-gray-900 shadow-sm fs-6 px-4 rounded-top-0" title="Check out 20 more demos" data-bs-toggle="tooltip" data-bs-placement="left" data-bs-dismiss="click" data-bs-trigger="hover">
                <span id="kt_engage_demos_label">Demos</span>
            </button>
            end::Demos drawer toggle
            begin::Purchase link
            <a href="https://1.envato.market/EA4JP" target="_blank" class="engage-purchase-link btn btn-flex h-35px bg-body btn-color-gray-700 btn-active-color-gray-900 px-5 shadow-sm rounded-top-0">Buy Now</a>
            end::Purchase link
        </div> -->
    <!--end::Engage toolbar-->
    <!--begin::Scrolltop-->

    <!--end::Modal - Users Search-->
    <!--end::Modals-->
    <!--begin::Javascript-->
    <script>
        var hostUrl = "assets/";
    </script>
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <!-- js inc -->
    @include('layouts.admin.js')
    <!--end::Page Custom Javascript-->
    <!--end::Javascript-->
    @yield('custom-js')
</body>
<!--end::Body-->

</html>

<script>
    // $(document).ready(function() {
    // 	localStorage.setItem('check_show_flag', 'on');
    // });
    $(document).on('click', '#submit', function() {
        email = $("#email").val();
        password = $("#password").val();

        var error = false;

        if (email.length == 0) {
            $("#email_error").show();
            error = true;
        } else {
            $("#email_error").hide();
        }
        if (password.length == 0) {
            $("#password_error").show();
            error = true;
        } else {
            $("#password_error").hide();
        }

        setTimeout(function() {
            if (error == true) {
                return false;
            } else {
                $("#frm").submit();
                return true;
            }
        }, 1000);

    });
    $(document).on("keypress", "#password,#email", function(e) {
        if (e.which == 13) {
            $("#submit").trigger('click');
        }
    });
</script>
