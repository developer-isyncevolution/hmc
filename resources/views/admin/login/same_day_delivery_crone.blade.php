@if(count($SlipData)>0)
<div style="display: grid;">
  <table style="border: 1px solid rgb(121, 115, 115);" class="center">
    <thead>
      <tr>
        <th style="border: 1px solid rgb(121, 115, 115);" >Slip Number</th>
        <th style="border: 1px solid rgb(121, 115, 115);" >Patient Name</th>
        <th style="border: 1px solid rgb(121, 115, 115);" >Office Code</th>
        <th style="border: 1px solid rgb(121, 115, 115);" >Pickup Date</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($SlipData as $key => $SlipDataVal)
      <tr>
        <td style="border: 1px solid rgb(121, 115, 115);">{{isset($SlipDataVal->vSlipNumber) && !empty($SlipDataVal->vSlipNumber)?$SlipDataVal->vSlipNumber:'--' }}</td>
        <td style="border: 1px solid rgb(121, 115, 115);">{{isset($SlipDataVal->vPatientName) && !empty($SlipDataVal->vPatientName)?$SlipDataVal->vPatientName:'--' }}</td>
        <td style="border: 1px solid rgb(121, 115, 115);">{{isset($SlipDataVal->vOffice__Code) && !empty($SlipDataVal->vOffice__Code)?$SlipDataVal->vOffice__Code:'--' }}</td>
        <td style="border: 1px solid rgb(121, 115, 115);">{{isset($SlipDataVal->dPickUpDate) && !empty($SlipDataVal->dPickUpDate)?(date('m/d/Y',strtotime($SlipDataVal->dPickUpDate))):'--' }}</td>
      </tr>
      @endforeach
      
    </tbody>
  </table>
  @else
  <h2>No Slips Avilable</h2>
  @endif
</div>