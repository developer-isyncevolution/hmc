@php 

$general_logo = \App\Libraries\General::setting_info('Company');
// dd($general_logo);
$logo_main         = $general_logo['COMPANY_LOGO']['vValue'];

$general_info    = \App\Libraries\General::setting_info('Appearance');
@endphp

<!DOCTYPE html>

<html lang="en">
<!--begin::Head-->
<style>
    .error_show{
        color: red;
    }
</style>
<head>
    <base href="">
    <title>{{$general_info['CPANEL_TITLE']['vValue']}}</title>
    <meta charset="utf-8" />
    <link rel="icon" type="image/ico" href="{{ asset('admin/assets/images/logo/favicon.png') }}">
  
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
   
 
    {{-- <link rel="shortcut icon" href="assets/media/logos/favicon.ico" /> --}}
 
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
    @include('layouts.admin.css')

    <!-- inc css -->
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body">
    <main class="login-form">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <h3 class="card-header text-center my-2">
                        <img src="{{asset('uploads/logo/'.$logo_main)}}" class="img-fluid h-125px">
                    </h3>

                    <h3 style="text-align: center">Set Password</h3>
                    <div class="card-body">
                        <form id="frm" action="{{route('admin.reset_password_action')}}" method="Post" >
                            @csrf
                            <input type="hidden" id="code" name="code" value="{{$code}}">
                            <div class="row">
                            
                               <div class="col-lg-12">
                                   <input type="password" class="form-control c-input-type mb-2" id="vPassword" name="vPassword"  value="" placeholder="Enter New Password">
                                  <div class="error_show" style="display: none"  id="vPassword_error">Please Enter Password</div>
                                 
                               </div>
                               <div class="col-lg-12">
                                  <input type="password" class="form-control c-input-type mb-2" id="vPassword2" name="vPassword2" placeholder="Confirm Password">
                                  <div id="vPassword2_error" style="display: none" class="error_show">Please Enter Confirm Password</div>
                                  <div class="error_show" style="display: none" id="vPassword2_same_error">Password not match,</div>
                               </div>                           
                               <div class="d-grid mx-auto mt-3">
                                  <a type="submit" id="submit" class="btn submit-btn submit">Login</a>
                               </div>
                            </div>
                         </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    @if ($message = Session::get('success'))
        <script>
            $( window ).on( "load", function() {
                notification_success("{{$message}}"); 
            });
        </script>
    @endif
    @if ($message = Session::get('error'))
        <script>
            $( window ).on( "load", function() {
                notification_error("{{$message}}"); 
            });
        </script>
    @endif



  
    <script>
        var hostUrl = "assets/";
    </script>
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <!-- js inc -->
    @include('layouts.admin.js')
    <!--end::Page Custom Javascript-->
    <!--end::Javascript-->
    @yield('custom-js')
</body>
<!--end::Body-->

</html>

<script>
    // $(document).ready(function() {
    // 	localStorage.setItem('check_show_flag', 'on');
    // });
    $(document).on('click','.submit',function(){
        vPassword     = $("#vPassword").val();
        vPassword2    = $("#vPassword2").val();

        error = false;

        if(vPassword.length == 0){
            $("#vPassword_error").show();
            error = true;
        } else{
            $("#vPassword_error").hide();
        }

        if(vPassword2.length == 0){
            $("#vPassword2_error").show();
            $("#vPassword2_same_error").hide();
            error = true;
        } else{
            if(vPassword != vPassword2){
                $("#vPassword2_same_error").show();
                $("#vPassword2_error").hide();
                return false;
            } else{
                $("#vPassword2_same_error").hide();
                $("#vPassword2_error").hide();
            }
            $("#vPassword2_error").hide();
            $("#vPassword2_same_error").hide();
        }

        if(error == true){
            return false;
        } else {
            $("#frm").submit();
            return true;
        }
    });
</script>
