@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
  <div class="page-title text-center">
    <h3>
       Doctor
    </h3>
</div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
      <div class="card-header">
        <ul class="c-nav-tab">
          <li class="">
            <a href="{{route('admin.customer.edit',$iCustomerId)}}" class="add-btn btn mx-2">
              Customer
            </a>
          </li>  
          <li class="">
            <a href="{{route('admin.office.officeedit',[$iCustomerId,$iOfficeId])}}" class="add-btn btn mx-2">
              Office
            </a>
          </li>
          <li>
            <a href="" class="active add-btn btn mx-2">
              Doctor
            </a>
          </li>
          @if(isset($doctors))
          
          <li class="">
            <a href="{{route('admin.userlist',[$doctors->iCustomerId,$doctors->iOfficeId,$doctors->iDoctorId])}}" class="add-btn btn mx-2">
              User
            </a>
          </li>
          @endif
        </ul>
      </div>
      <form action="{{route('admin.doctor.doctorstore')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($doctors)) {{$doctors->iDoctorId}} @endif">
        <input type="hidden" name="iCustomerId" value="{{$iCustomerId}}">
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Office</label>
            <select name="iOfficeId" id="iOfficeId">
              @foreach($office as $key => $offices)
              <option value="{{$offices->iOfficeId}}" @if(isset($doctors)){{$offices->iOfficeId == $doctors->iOfficeId  ? 'selected' : ''}} @endif>{{$offices->vOfficeName}}</option>
              @endforeach
            </select>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Licence</label>
          <input type="text" class="form-control" id="vLicence" name="vLicence" placeholder="Licence" value="@if(old('vLicence')!=''){{old('vLicence')}}@elseif(isset($doctors->vLicence)){{$doctors->vLicence}}@else{{old('vLicence')}}@endif">
          <div class="text-danger" style="display: none;" id="vLicence_error">Please enter licence</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>First name</label>
          <input type="text" class="form-control" id="vFirstName" name="vFirstName" placeholder="First name" value="@if(old('vFirstName')!=''){{old('vFirstName')}}@elseif(isset($doctors->vFirstName)){{$doctors->vFirstName}}@else{{old('vFirstName')}}@endif">
          <div class="text-danger" style="display: none;" id="vFirstName_error">Please enter first name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Last name</label>
          <input type="text" class="form-control" id="vLastName" name="vLastName" placeholder="Last name" value="@if(old('vLastName')!=''){{old('vLastName')}}@elseif(isset($doctors->vLastName)){{$doctors->vLastName}}@else{{old('vLastName')}}@endif">
          <div class="text-danger" style="display: none;" id="vLastName_error">Please enter last name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Middle initial</label>
          <input type="text" class="form-control" id="vMiddleInitial" name="vMiddleInitial" placeholder="Middle initial" value="@if(old('vMiddleInitial')!=''){{old('vMiddleInitial')}}@elseif(isset($doctors->vMiddleInitial)){{$doctors->vMiddleInitial}}@else{{old('vMiddleInitial')}}@endif">
          <div class="text-danger" style="display: none;" id="vMiddleInitial_error">Please enter middle initial</div>
        </div>
        
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Email</label>
          <input type="text" class="form-control" id="vEmail" name="vEmail" placeholder="Email" value="@if(old('vEmail')!=''){{old('vEmail')}}@elseif(isset($doctors->vEmail)){{$doctors->vEmail}}@else{{old('vEmail')}}@endif" autocomplete="off">
          <div class="text-danger" style="display: none;" id="vEmail_error">Please enter email</div>
          <div class="text-danger" style="display: none;" id="vEmail_valid_error">Please enter valid email</div>
        </div>
        @if(!isset($doctors->iDoctorId))
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Temporary Password</label>
          <input type="password" class="form-control" id="vPassword" name="vPassword" placeholder="Password" value="@if(old('vPassword')!=''){{old('vPassword')}}@elseif(isset($doctors->vPassword)){{$doctors->vPassword}}@else{{old('vPassword')}}@endif" autocomplete="off">
          <div class="text-danger" style="display: none;" id="vPassword_error">Please enter password</div>
        </div>
        @endif
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Mobile</label>
          <input type="text" class="form-control" id="vMobile" name="vMobile" placeholder="Mobile" value="@if(old('vMobile')!=''){{old('vMobile')}}@elseif(isset($doctors->vMobile)){{$doctors->vMobile}}@else{{old('vMobile')}}@endif">
          <div class="text-danger" style="display: none;" id="vMobile_error">Please enter mobile</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Cell</label>
          <input type="text" class="form-control" id="vCellulor" name="vCellulor" placeholder="Cell" value="@if(old('vCellulor')!=''){{old('vCellulor')}}@elseif(isset($doctors->vCellulor)){{$doctors->vCellulor}}@else{{old('vCellulor')}}@endif">
          <div class="text-danger" style="display: none;" id="vCellulor_error">Please enter cell</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Status</label>
          <select id="eStatus" name="eStatus">
            <option value="Active" @if(isset($doctors)) @if($doctors->eStatus == 'Active') selected @endif @endif>Active</option>
            <option value="Inactive" @if(isset($doctors)) @if($doctors->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
          </select>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Note</label>
          <div id="toolbar-container"></div>
            <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Description">@if(old('tDescription')!=''){{old('tDescription')}}@elseif(isset($doctors->tDescription)){{$doctors->tDescription}}@else{{old('tDescription')}}@endif</textarea>
          <div id="tDescription_error" class="text-danger" style="display: none;">Please enter note </div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Please sign here</label>
          <div>
            <canvas id="signature"  style="border: 1px solid #e4e6ef;border-radius:5px;"></canvas>
          </div>
            <br>
            <a id="download_sign" class="btn submit-btn">Save</a>
            <a id="clear-signature" class="btn back-btn">Clear</a>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label class=" fs-5 fw-bold mb-2">Upload Signature</label>
          <input class="form-control" type="file" id="vImage" name="vImage" accept="image/*">
          <input class="form-control" type="hidden" id="image" name="image" accept="image/*">
          @if(isset($doctors))
             <img style="width: 100px;" id="img" value="@if(old('vImage') == 'vImage') selected @endif" src="{{asset('uploads/doctor/'.$doctors->vImage)}}">
          @else 
             <img style="width: 100px;" id="img" value="" src="{{asset('images/no-image.gif')}}">
          @endif
         <div class="text-danger" style="display: none;" id="vImage_error">select signature image</div>
        </div>
        <div class="col-12 align-self-end d-inline-block">
          <a type="submit" id="submit" class="btn submit-btn">Submit</a>
          <a href="{{route('admin.doctorlist',[$iCustomerId,$iOfficeId])}}" class="btn back-btn">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/signature_pad/1.3.4/signature_pad.min.js" integrity="sha512-Mtr2f9aMp/TVEdDWcRlcREy9NfgsvXvApdxrm3/gK8lAMWnXrFsYaoW01B5eJhrUpBT7hmIjLeaQe0hnL7Oh1w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
 $('#iCustomerId').selectize();
 $('#eStatus').selectize();
 $('#iOfficeId').selectize();
  jQuery(document).ready(function($){
    
    var canvas = document.getElementById("signature");
    var signaturePad = new SignaturePad(canvas);
    
    $('#clear-signature').on('click', function(){
        signaturePad.clear();
    });
    
});

$("#download_sign").click(function(){
  var canvas = document.getElementById("signature");
  // var dataURL = canvas.toDataURL("image/png");
  // var newTab = window.open('about:blank','image from canvas');
  // newTab.document.write("<img src='" + dataURL + "' alt='from canvas'/>");
  var imageData = canvas.toDataURL();
    document.getElementsByName("image")[0].setAttribute("value", imageData);
    $('#img').attr('src', imageData);
  // canvas.toBlob(function(blob) {
  //     saveAs(blob, "pretty image.png");
  // });
})
  $(document).on('click', '#submit', function() {
    id            = $("#id").val();
    vLicence      = $("#vLicence").val();
    vFirstName    = $("#vFirstName").val();
    vLastName     = $("#vLastName").val();
    vMiddleInitial= $("#vMiddleInitial").val();
    vEmail        = $("#vEmail").val();
    vPassword     = $("#vPassword").val();
    vMobile       = $("#vMobile").val();
    vCellulor     = $("#vCellulor").val();
    vImage        = $("#image").val();
    tDescription  = $("#tDescription").val();
    
    var error = false;

    if (vLicence.length == 0) {
      $("#vLicence_error").show();
      error = true;
    } else {
      $("#vLicence_error").hide();
    }
    if (vFirstName.length == 0) {
      $("#vFirstName_error").show();
      error = true;
    } else {
      $("#vFirstName_error").hide();
    }
    if (vLastName.length == 0) {
      $("#vLastName_error").show();
      error = true;
    } else {
      $("#vLastName_error").hide();
    }
    // if (vMiddleInitial.length == 0) {
    //   $("#vMiddleInitial_error").show();
    //   error = true;
    // } else {
    //   $("#vMiddleInitial_error").hide();
    // }
    <?php if(!isset($doctors->iDoctorId)){?>
      if (vPassword.length == 0) {
        $("#vPassword_error").show();
        error = true;
      } else {
        $("#vPassword_error").hide();
      }
    <?php } ?>
    if (vEmail.length == 0) {
      $("#vEmail_error").show();
      $("#vEmail_valid_error").hide();
        error = true;
    }else{
      if(validateEmail(vEmail))
      {
        $("#vEmail_valid_error").hide();

      }else{
        $("#vEmail_valid_error").show();
        $("#vEmail_error").hide();
        error = true;
      }
    }
     
    
    
    
    

    <?php if(!isset($doctors->iDoctorId)){?>
      if(vImage.length == 0){
        $("#vImage_error").show();
        error = true;
      } else {
        $("#vImage_error").hide();
      } 
    <?php } ?>

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
function validateEmail(vEmail) 
{
  var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  if (filter.test(vEmail)) {
    return true;
  }
  else {
    return false;
  }
}
$(document).ready(function() {
    var mobile = [{ "mask": "### ### ####"}, { "mask": "### ### ####"}];
    $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
   })
  $(document).ready(function() {
  var cellulor = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-####"}];
  $('#vCellulor').inputmask({ 
      mask: cellulor, 
      greedy: false, 
      definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
  })
</script>
@endsection