@if(count($data) > 0)
<option>Select doctor</option>
@foreach($data as $key => $value)
	<option value="{{$value->iDoctorId}}">{{$value->vFirstName}} {{$value->vLastName}}</option>
@endforeach
@else
	<option value="">No record found</option>
@endif