@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
  <div class="page-title text-center">
    <h3>
      {{isset($users->iUserId) ? 'Edit' : 'Add'}} user
    </h3>
</div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
      {{-- <div class="card-header"> --}}
        {{-- <h4 class="card-title" id="row-separator-basic-form">{{isset($users->iUserId) ? 'Edit' : 'Add'}} user</h4> --}}
      {{-- </div> --}}
      <form action="{{route('admin.user_lab.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($users)) {{$users->iUserId}} @endif">
        @if(\App\Libraries\General::admin_info()['customerType']=='Super Admin')
        <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>customer</label>
            <select name="iCustomerId" id="iCustomerId">
              <option value="none">Select customer</option>
                @foreach($customer as $key => $customers)
                <option value="{{$customers->iCustomerId}}" @if(isset($users)){{$customers->iCustomerId == $users->iCustomerId  ? 'selected' : ''}} @endif>{{$customers->vOfficeName}}</option>
                  @endforeach
            </select>
            <div class="text-danger" style="display: none;" id="iCustomerId_error">Select customer</div>
        </div>
        {{-- <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Office</label>
            <select name="iOfficeId" id="iOfficeId">
              <option value="">Select office</option>
              @foreach($office as $key => $offices)
                <option value="{{$offices->iOfficeId}}" @if(isset($users)){{$offices->iOfficeId == $users->iOfficeId  ? 'selected' : ''}} @endif>{{$offices->vOfficeName}}</option>
              @endforeach
            </select>
            <div class="text-danger" style="display: none;" id="iOfficeId_error">Select category</div>
          </div> 
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Doctor</label>
            <select name="iDoctorId" id="iDoctorId">
                <option value="">Select doctor</option>
                @foreach($doctor as $key => $doctors)
                <option value="{{$doctors->iDoctorId}}" @if(isset($users)){{$doctors->iDoctorId == $users->iDoctorId  ? 'selected' : ''}} @endif>{{$doctors->vFirstName}} {{$doctors->vLastName}}</option>
              @endforeach
              </select>
          </div> --}}
        @endif
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Title</label>
          <input type="text" class="form-control" id="vTitle" name="vTitle" placeholder="Title" value="@if(old('vTitle')!=''){{old('vTitle')}}@elseif(isset($users->vTitle)){{$users->vTitle}}@else{{old('vTitle')}}@endif">
          <div class="text-danger" style="display: none;" id="vTitle_error">Please enter title</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>First name</label>
          <input type="text" class="form-control" id="vFirstName" name="vFirstName" placeholder="First name" value="@if(old('vFirstName')!=''){{old('vFirstName')}}@elseif(isset($users->vFirstName)){{$users->vFirstName}}@else{{old('vFirstName')}}@endif">
          <div class="text-danger" style="display: none;" id="vFirstName_error">Please enter first name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Last name</label>
          <input type="text" class="form-control" id="vLastName" name="vLastName" placeholder="Last name" value="@if(old('vLastName')!=''){{old('vLastName')}}@elseif(isset($users->vLastName)){{$users->vLastName}}@else{{old('vLastName')}}@endif">
          <div class="text-danger" style="display: none;" id="vLastName_error">Please enter last name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12 position-relative">
          <label>User Name</label>
          <input type="text" class="form-control" id="vUserName" name="vUserName" placeholder="Username" value="@if(old('vUserName')!=''){{old('vUserName')}}@elseif(isset($users->vUserName)){{$users->vUserName}}@else{{old('vUserName')}}@endif">
          <a href="javascript:;" id="generate_username" class="icon-text">
            <span class="svg-icon"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo2/dist/../src/media/svg/icons/General/Update.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="30" height="30"/>
                    <path d="M8.43296491,7.17429118 L9.40782327,7.85689436 C9.49616631,7.91875282 9.56214077,8.00751728 9.5959027,8.10994332 C9.68235021,8.37220548 9.53982427,8.65489052 9.27756211,8.74133803 L5.89079566,9.85769242 C5.84469033,9.87288977 5.79661753,9.8812917 5.74809064,9.88263369 C5.4720538,9.8902674 5.24209339,9.67268366 5.23445968,9.39664682 L5.13610134,5.83998177 C5.13313425,5.73269078 5.16477113,5.62729274 5.22633424,5.53937151 C5.384723,5.31316892 5.69649589,5.25819495 5.92269848,5.4165837 L6.72910242,5.98123382 C8.16546398,4.72182424 10.0239806,4 12,4 C16.418278,4 20,7.581722 20,12 C20,16.418278 16.418278,20 12,20 C7.581722,20 4,16.418278 4,12 L6,12 C6,15.3137085 8.6862915,18 12,18 C15.3137085,18 18,15.3137085 18,12 C18,8.6862915 15.3137085,6 12,6 C10.6885336,6 9.44767246,6.42282109 8.43296491,7.17429118 Z" fill="#000000" fill-rule="nonzero"/>
                </g>
            </svg><!--end::Svg Icon--></span></a>
          <div class="text-danger" style="display: none;" id="vUserName_error">Please enter username</div>
        </div>
        @if(!isset($users->iUserId))
        <div class="col-xxl-4 col-lg-6 col-md-12  position-relative">
          <label>Password</label>
          <input type="text" class="form-control" id="vPassword" name="vPassword" placeholder="Password" value="@if(old('vPassword')!=''){{old('vPassword')}}@elseif(isset($users->vPassword)){{$users->vPassword}}@else{{old('vPassword')}}@endif">
          <a href="javascript:;" id="generate_password" class="icon-text">
            <span class="svg-icon"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo2/dist/../src/media/svg/icons/General/Update.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="30" height="30"/>
                    <path d="M8.43296491,7.17429118 L9.40782327,7.85689436 C9.49616631,7.91875282 9.56214077,8.00751728 9.5959027,8.10994332 C9.68235021,8.37220548 9.53982427,8.65489052 9.27756211,8.74133803 L5.89079566,9.85769242 C5.84469033,9.87288977 5.79661753,9.8812917 5.74809064,9.88263369 C5.4720538,9.8902674 5.24209339,9.67268366 5.23445968,9.39664682 L5.13610134,5.83998177 C5.13313425,5.73269078 5.16477113,5.62729274 5.22633424,5.53937151 C5.384723,5.31316892 5.69649589,5.25819495 5.92269848,5.4165837 L6.72910242,5.98123382 C8.16546398,4.72182424 10.0239806,4 12,4 C16.418278,4 20,7.581722 20,12 C20,16.418278 16.418278,20 12,20 C7.581722,20 4,16.418278 4,12 L6,12 C6,15.3137085 8.6862915,18 12,18 C15.3137085,18 18,15.3137085 18,12 C18,8.6862915 15.3137085,6 12,6 C10.6885336,6 9.44767246,6.42282109 8.43296491,7.17429118 Z" fill="#000000" fill-rule="nonzero"/>
                </g>
            </svg><!--end::Svg Icon--></span>
          </a>
          <div class="text-danger" style="display: none;" id="vPassword_error">Please enter password</div>
        </div>
        @endif
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Email</label>
          <input type="text" class="form-control" id="vEmail" name="vEmail" placeholder="Email" value="@if(old('vEmail')!=''){{old('vEmail')}}@elseif(isset($users->vEmail)){{$users->vEmail}}@else{{old('vEmail')}}@endif" @if(isset($users->vEmail)) @if (\App\Libraries\General::admin_info()['customerType'] != 'Super Admin') readonly @endif @endif>
          <div class="text-danger" style="display: none;" id="vEmail_error">Please enter email</div>
          <div class="text-danger" style="display: none;" id="vEmail_valid_error">Please enter valid email</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Mobile</label>
          <input type="text" class="form-control" id="vMobile" name="vMobile" placeholder="Mobile" value="@if(old('vMobile')!=''){{old('vMobile')}}@elseif(isset($users->vMobile)){{$users->vMobile}}@else{{old('vMobile')}}@endif">
          <div class="text-danger" style="display: none;" id="vMobile_error">Please enter mobile</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Status</label>
          <select id="eStatus" name="eStatus">
            <option value="Active" @if(isset($users)) @if($users->eStatus == 'Active') selected @endif @endif>Active</option>
            <option value="Inactive" @if(isset($users)) @if($users->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
            <option value="In Vacation" @if(isset($users)) @if($users->eStatus == 'In Vacation') selected @endif @endif>In Vacation</option>
            <option value="Fired" @if(isset($users)) @if($users->eStatus == 'Fired') selected @endif @endif>Fired</option>
            <option value="Resigned" @if(isset($users)) @if($users->eStatus == 'Resigned') selected @endif @endif>Resigned</option>
            <option value="Sick" @if(isset($users)) @if($users->eStatus == 'Sick') selected @endif @endif>Sick</option>
            <option value="Suspended" @if(isset($users)) @if($users->eStatus == 'Suspended') selected @endif @endif>Suspended</option>
          </select>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Note</label>
          <div id="toolbar-container"></div>
            <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Description">@if(old('tDescription')!=''){{old('tDescription')}}@elseif(isset($users->tDescription)){{$users->tDescription}}@else{{old('tDescription')}}@endif</textarea>
          <div id="tDescription_error" class="text-danger" style="display: none;">Please enter note </div>
      </div>
      <div class="col-xxl-4 col-lg-6 col-md-12">
        <label>Profile Picture</label>
       
        <input class="form-control" type="file" id="vImage" name="vImage" accept="image/*">
        @if(isset($users->vImage))
           <img style="width: 100px;" id="img" value="@if(old('vImage') == 'vImage') selected @endif" src="{{asset('uploads/lab_office/'.$users->vImage)}}">
        @else 
           <img style="width: 100px;" id="img" value="" src="{{asset('images/no-image.gif')}}">
        @endif
       <div class="text-danger" style="display: none;" id="vImage_error">select logo</div>
      </div>
        
        <div class="col-12 align-self-end d-inline-block">
          <a type="submit" id="submit" class="btn submit-btn me-4">Submit</a>
          <a href="{{route('admin.user_lab')}}" class="btn back-btn me-4">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
 $('#iCustomerId').selectize();
 // $('#iDoctorId').selectize();
 $('#iOfficeId').selectize();
 $('#eStatus').selectize();
 $(document).on('change', '#vImage', function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#img').attr('src', e.target.result);
      };
      reader.readAsDataURL(this.files[0]);
    }
  });
   $(document).ready(function() {
        $('#iCustomerId').on('change',function(e) {
            var iCustomerId = $("#iCustomerId").val();
            $.ajax({
                url:"{{route('admin.user_lab.fetch_office') }}",
                type:"POST",
                data: {
                iCustomerId: iCustomerId,
                 "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $("#iOfficeId").html(response);
                      
                }
            });
        });
    });
    
        // $('#iOfficeId').on('change',function(e) {
        //   var iOfficeId = $("#iOfficeId").val();
        //     $.ajax({
        //         url: "{{route('admin.user_lab.fetch_doctor') }}",
        //         type: "POST",
        //         data:  {iOfficeId:iOfficeId,
        //           "_token": "{{ csrf_token() }}",
        //           }, 
        //         success: function(response) {
        //           $("#iDoctorId").html(response);
        //         }
        //     });
        // });
  $(document).on('click', '#submit', function() {
    id          = $("#id").val();
    vUserName   = $("#vUserName").val();
    vFirstName  = $("#vFirstName").val();
    vLastName   = $("#vLastName").val();
    iCustomerId = $("#iCustomerId").val();
    iOfficeId   = $("#iOfficeId").val();
    vLastName   = $("#vLastName").val();
    vPassword   = $("#vPassword").val();
    vEmail      = $("#vEmail").val();
    vMobile     = $("#vMobile").val();
    vTitle      = $("#vTitle").val();
    tDescription= $("#tDescription").val();
    
    var error = false;

    if (vUserName.length == 0) {
      $("#vUserName_error").show();
      error = true;
    } else {
      $("#vUserName_error").hide();
    }
    if (vFirstName.length == 0) {
      $("#vFirstName_error").show();
      error = true;
    } else {
      $("#vFirstName_error").hide();
    }
    if (vLastName.length == 0) {
      $("#vLastName_error").show();
      error = true;
    } else {
      $("#vLastName_error").hide();
    }
    if (iCustomerId == "none") {
      $("#iCustomerId_error").show();
      error = true;
    } else {
      $("#iCustomerId_error").hide();
    }
    <?php if(!isset($users->iUserId)){?>
      if (vPassword.length == 0) {
        $("#vPassword_error").show();
        error = true;
      } else {
        $("#vPassword_error").hide();
      }
    <?php } ?>

   // Email validtion start
   if (vEmail.length == 0) {
      $("#vEmail_error").text("Please enter email");
      $("#vEmail_error").show();
      $("#vEmail_valid_error").hide();
        error = true;
    }else{
      if(validateEmail(vEmail))
      {
        $("#vEmail_valid_error").hide();
        @if(!isset($users))
          $.ajax({
            url: "{{route('admin.staff.CheckExistEmail')}}",
            type: "post",
            data: {
                vEmail : $('#vEmail').val(),
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
              if(response == 1)
              {
                $('#vEmail_error').show();
                $("#vEmail_error").text("This email alredy exist");
                error = true;
                return false;
              }
              else
              {
                $('#vEmail_error').hide();
                $('#vEmail_valid_error').hide();
                return true;
              }
            }
          }); 
        @endif  
      }else{
        $("#vEmail_valid_error").show();
        $("#vEmail_error").hide();
        error = true;
      }
    }
// Email validtion end
     
    
    
    if (vTitle.length == 0) {
      $("#vTitle_error").show();
      error = true;
    } else {
      $("#vTitle_error").hide();
    }
    

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });

  $(document).ready(function() {
        $("#generate_password").click(function() {
            $.ajax({
                url: "{{route('admin.user_lab.generatepassword')}}",
                type: "get",
                dataType: "JSON",
                success: function(data) {
                    $("#vPassword").val(data);
                }
            });
            return false;
        });

        $("#generate_username").click(function() {
            $.ajax({
                url: "{{route('admin.user_lab.generatepassword')}}",
                type: "get",
                dataType: "JSON",
                success: function(response) {
                    $("#vUserName").val(response);
                }
            });
            return false;
        });
    });
function validateEmail(vEmail) 
{
  var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  if (filter.test(vEmail)) {
    return true;
  }
  else {
    return false;
  }
}
$(document).ready(function() {
    var mobile = [{ "mask": "### ### ####"}, { "mask": "### ### ####"}];
    $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
   })
  $(document).ready(function() {
  var cellulor = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-####"}];
  $('#vCellulor').inputmask({ 
      mask: cellulor, 
      greedy: false, 
      definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
  })
</script>
@endsection