<!DOCTYPE html>

<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <title>Customer Registration</title>
    <meta charset="utf-8" />
    <meta name="description"
        content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
    <meta name="keywords"
        content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title"
        content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
    <meta property="og:url" content="https://keenthemes.com/metronic" />
    <meta property="og:site_name" content="Keenthemes | Metronic" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <script src="https://code.jquery.com/jquery-3.6.0.slim.js" integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY=" crossorigin="anonymous"></script>
    @include('layouts.admin.css')

    <!-- inc css -->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body">
    <main class="login-form">
        <div class="row">
            <div class="">
                <div class="card">
                    <h3 class="card-header text-center my-2">
                        <img src="{{ asset('admin/assets/images/logo/sliplogo.png') }}" class="img-fluid h-125px">
                    </h3>

                    <h3 class="text-center my-2">
                        Customer Registration
                    </h3>

                    <div class="card-body">
                        <form method="POST" id="frm" action="{{ route('registration_action') }}">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group mb-3 selectize-control single">
                                        <label for="iCustomerTypeId">Customer Type</label>
                                        <select id="iCustomerTypeId" name="iCustomerTypeId" class="form-control">
                                            <option value="">Select Customer type</option>
                                            @foreach($customertype as $key => $customertypes)
                                                <option value="{{$customertypes->iCustomerTypeId}}" >{{$customertypes->vTypeName}}</option>
                                            @endforeach
                                        </select>
                                        <div class="text-danger" style="display: none;" id="iCustomerTypeId_error">Select Customer type</div>
                                    </div>
                                </div>                            
                                <div class="col-lg-6">
                                    
                                    <div class="form-group mb-3">
                                        <label for="vOfficeName">Lab/Office Name</label>
                                        <input type="text" placeholder="Lab/Office name" id="vOfficeName" class="form-control" name="vOfficeName" required>
                                        <div class="text-danger" style="display: none;" id="vOfficeName_error">Enter Lab/Office name</div>
                                    </div>
                                </div>                            
                                <div class="col-lg-6">
                                    
                                    <div class="form-group mb-3">
                                        <label for="vAddress">Street Address</label>
                                        <input type="text" placeholder="Street Address" id="vAddress" class="form-control" name="vAddress" required>
                                        <div class="text-danger" style="display: none;" id="vAddress_error">Enter Address</div>
                                    </div>
                                </div>                            
                                <div class="col-lg-6">
                                    
                                    <div class="form-group mb-3">
                                        <label for="vCity">City</label>
                                        <input type="text" placeholder="City" id="vCity" class="form-control" name="vCity" required>
                                        <div class="text-danger" style="display: none;" id="vCity_error">Enter City</div>
                                    </div>
                                </div>                            
                                <div class="col-lg-6">
                                    
                                    <div class="form-group mb-3 selectize-control single">
                                        <label for="iStateId">State</label>
                                        <select id="iStateId" name="iStateId" class="form-control">
                                            <option value="">Select State</option>
                                            @foreach($states as $key => $states)
                                                <option value="{{$states->iStateId}}" @if(isset($customers)){{$states->iStateId == $customers->iStateId  ? 'selected' : ''}} @endif>{{$states->vState}}</option>
                                            @endforeach
                                        </select>
                                        <div class="text-danger mt-6" style="display: none;" id="iStateId_error">Select State</div>
                                    </div>
                                </div>                            
                                <div class="col-lg-6">
                                    
                                    <div class="form-group mb-3">
                                        <label for="vZipCode">Zip Code</label>
                                        <input type="text" placeholder="Zip Code" id="vZipCode" class="form-control" name="vZipCode" required>
                                        <div class="text-danger" style="display: none;" id="vZipCode_error">Enter Zip Code</div>
                                    </div>
                                </div>                            
                                {{-- <div class="col-lg-6">
                                    <div class="form-group mb-3">
                                        <label for="vName">Contact Name</label>
                                        <input type="text" placeholder="Contact Name" id="vName" class="form-control" name="vName" required>
                                        <div class="text-danger" style="display: none;" id="vName_error">Enter Contact Name</div>
                                    </div>
                                </div>                             --}}
                                <div class="col-lg-6">
                                    
                                    <div class="form-group mb-3">
                                    <label>First name</label>
                                    <input type="text" class="form-control" id="vFirstName" name="vFirstName" placeholder="First name" value="">
                                    <div class="text-danger" style="display: none;" id="vFirstName_error">Please enter first name</div>
                                  </div>
                                </div>
                                <div class="col-lg-6">
                                    
                                    <div class="form-group mb-3">
                                    <label>Last name</label>
                                    <input type="text" class="form-control" id="vLastName" name="vLastName" placeholder="Last name" value="">
                                    <div class="text-danger" style="display: none;" id="vLastName_error">Please enter last name</div>
                                  </div>
                                  </div>
                                <div class="col-lg-6">
                                    <div class="form-group mb-3">
                                        <label for="vTitle">Title</label>
                                        <input type="text" placeholder="Title" id="vTitle" class="form-control" name="vTitle" required>
                                        <div class="text-danger" style="display: none;" id="vTitle_error">Enter Title</div>
                                    </div>
                                </div>                            
                                <div class="col-lg-6">
                                    <div class="form-group mb-3">
                                        <label for="vEmail">Email</label>
                                        <input type="text" placeholder="Email" id="vEmail" class="form-control" name="vEmail" required>
                                        <div class="text-danger" style="display: none;" id="vEmail_error">Please enter email</div>
                                        <div class="text-danger" style="display: none;" id="vEmail_valid_error">Please enter valid email</div>
                                    </div>
                                </div>                            
                                <div class="col-lg-6">
                                    
                                    <div class="form-group mb-3">
                                        <label for="vMobile">Phone</label>
                                        <input type="text" placeholder="Mobile" id="vMobile" class="form-control" name="vMobile" required>
                                        <div class="text-danger" style="display: none;" id="vMobile_error">Enter Mobile</div>
                                    </div>
                                </div>                            
                                <div class="col-lg-6">
                                    
                                    <div class="form-group mb-3">
                                        <label for="vCellulor">Cell</label>
                                        <input type="text" placeholder="Cell" id="vCellulor" class="form-control" name="vCellulor" required>
                                        <div class="text-danger" style="display: none;" id="vCellulor_error">Enter Cell</div>
                                    </div>
                                </div>
                                    
                                    <div class="d-grid mx-auto">
                                        <a type="submit" id="submit" class="btn submit-btn">Sign Up</a>
                                    </div>
                                    <div class="link-link">
                                        <div>
                                            Already have Account?
                                            <u><a href="{{ url('/admin/login') }}" class="for">Login</a></u>
                                        </div>
                                    </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    @if ($message = Session::get('success'))
        <script>
            $( window ).on( "load", function() {
                notification_success("{{$message}}"); 
            });
        </script>
    @endif
    @if ($message = Session::get('error'))
        <script>
            $( window ).on( "load", function() {
                notification_error("{{$message}}"); 
            });
        </script>
    @endif
    <!--begin::Javascript-->
    <script>
        var hostUrl = "assets/";
    </script>
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <!-- js inc -->
    @include('layouts.admin.js')
    <!--end::Page Custom Javascript-->
    <!--end::Javascript-->
    @yield('custom-js')
</body>
<!--end::Body-->

</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>
<script>
    $('#iStateId').selectize();
    $(document).on('click', '#submit', function() {
        var vEmail = $("#vEmail").val();
        var vOfficeName = $("#vOfficeName").val();
        var vAddress = $("#vAddress").val();
        var vCity = $("#vCity").val();
        var vZipCode = $("#vZipCode").val();
        // var vName = $("#vName").val();
        var vFirstName    = $("#vFirstName").val();
        var vLastName     = $("#vLastName").val();
        var vTitle = $("#vTitle").val();
        var vMobile = $("#vMobile").val();
        var vCellulor = $("#vCellulor").val();
        var error = false;

        if (vEmail.length == 0) {
            $("#vEmail_error").show();
            $("#vEmail_valid_error").hide();
                error = true;
            }
        else{
           
            if(validateEmail(vEmail))
            {
                    $.ajax({
                    url: "{{route('admin.admin.CheckExistEmail')}}",
                    type: "post",
                    data: {
                        vEmail : $('#vEmail').val(),
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function(response) {
                        if(response == 1)
                        {
                            $('#vEmail_error').show();
                            $("#vEmail_error").text("This email alredy exist");
                            error = true;
                            return false;
                        }
                        else
                        {
                            $('#vEmail_error').hide();
                            $('#vEmail_valid_error').hide();
                        return true;
                        }
                    }
                    }); 
            }else{
                $("#vEmail_valid_error").show();
                $("#vEmail_error").hide();
                error = true;
            }
           
        }
        if ($('#iCustomerTypeId').val() == '') {
            $("#iCustomerTypeId_error").show();
            error = true;
        } else {
            $("#iCustomerTypeId_error").hide();
        }
        if (vOfficeName.length == 0) {
            $("#vOfficeName_error").show();
            error = true;
        } else {
            $("#vOfficeName_error").hide();
        }
        if (vAddress.length == 0) {
            $("#vAddress_error").show();
            error = true;
        } else {
            $("#vAddress_error").hide();
        }
        if (vCity.length == 0) {
            $("#vCity_error").show();
            error = true;
        } else {
            $("#vCity_error").hide();
        }
        if ($('#iStateId').val() == '') {
            $("#iStateId_error").show();
            error = true;
        } else {
            $("#iStateId_error").hide();
        }
        if ($('#vZipCode').val() == '') {
            $("#vZipCode_error").show();
            error = true;
        } else {
            $("#vZipCode_error").hide();
        }
        // if ($('#vName').val() == '') {
        //     $("#vName_error").show();
        //     error = true;
        // } else {
        //     $("#vName_error").hide();
        // }
        if (vFirstName.length == 0) {
            $("#vFirstName_error").show();
            error = true;
        } else {
            $("#vFirstName_error").hide();
        }
        if (vLastName.length == 0) {
            $("#vLastName_error").show();
            error = true;
        } else {
            $("#vLastName_error").hide();
        }
        if ($('#vTitle').val() == '') {
            $("#vTitle_error").show();
            error = true;
        } else {
            $("#vTitle_error").hide();
        }
        if ($('#vMobile').val() == '') {
            $("#vMobile_error").show();
            error = true;
        } else {
            $("#vMobile_error").hide();
        }
        // if ($('#vCellulor').val() == '') {
        //     $("#vCellulor_error").show();
        //     error = true;
        // } else {
        //     $("#vCellulor_error").hide();
        // }
        if ($('#eStatus').val() == '') {
            $("#eStatus_error").show();
            error = true;
        } else {
            $("#eStatus_error").hide();
        }

        setTimeout(function() {
            if (error == true) {
                return false;
            } else {
                $("#frm").submit();
                return true;
            }
        }, 1000);

    });
    function validateEmail(vEmail) 
    {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(vEmail)) {
            return true;
        }
        else {
            return false;
        }
    }
    $(document).on("keypress", "#password,#email", function(e) {
        if (e.which == 13) {
            $("#submit").trigger('click');
        }
    });
    $(document).ready(function() {
        var cellulor = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-####"}];
        $('#vCellulor').inputmask({ 
        mask: cellulor, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });


        var mobile = [{ "mask": "### ### ####"}, { "mask": "### ### ####"}];
        $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
    })
</script>
