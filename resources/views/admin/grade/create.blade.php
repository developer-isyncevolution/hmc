@extends('layouts.admin.index')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<div class="main-panel">
  <div class="page-title text-center">
    <h3>
      {{isset($grades->iGradeId) ? 'Edit' : 'Add'}} grade
    </h3>
</div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
          {{-- <div class="card-header">
            <h4 class="card-title" id="row-separator-basic-form">{{isset($grades->iGradeId) ? 'Edit' : 'Add'}} grade</h4>
            
          </div> --}}
        <form action="{{route('admin.grade.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($grades)) {{$grades->iGradeId}} @endif">
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label >Quality name</label>
                <input type="text" class="form-control" id="vQualityName" name="vQualityName" placeholder="Quality name" value="@if(old('vQualityName')!=''){{old('vQualityName')}}@elseif(isset($grades->vQualityName)){{$grades->vQualityName}}@else{{old('vQualityName')}}@endif">
                <div class="text-danger" style="display: none;" id="vQualityName_error">Please enter quality name</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Sequence</label>
                <input type="number" class="form-control" id="iSequence" name="iSequence" placeholder="Sequence" value="@if(old('iSequence')!=''){{old('iSequence')}}@elseif(isset($grades->iSequence)){{$grades->iSequence}}@else{{old('iSequence')}}@endif">
                <div class="text-danger" style="display: none;" id="iSequence_error">Please enter sequence</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Status</label>
                <select id="eStatus" name="eStatus">
                    <option value="Active" @if(isset($grades)) @if($grades->eStatus == 'Active') selected @endif @endif>Active</option>
                    <option value="Inactive" @if(isset($grades)) @if($grades->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
                </select>
                <div class="mt-1">
                    @error('eStatus')
                        <code>{{ $message }}</code>
                    @enderror
                </div>
            </div>       
            <div class="col-12 align-self-end d-inline-block ">
                <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
                <a href="{{route('admin.grade')}}" class="btn back-btn me-2">Back</a>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
   $('#eStatus').selectize();
  $(document).on('click','#submit',function()
  {
    iGradeId = $("#id").val();
    vQualityName           = $("#vQualityName").val();
    iSequence       = $("#iSequence").val();

    var error = false;

    if(vQualityName.length == 0){
      $("#vQualityName_error").show();
      error = true;
    } else {
      $("#vQualityName_error").hide();
    }
    if(iSequence.length == 0){
      $("#iSequence_error").show();
      error = true;
    } else {
      $("#iSequence_error").hide();
    }
    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
    </script>
@endsection
