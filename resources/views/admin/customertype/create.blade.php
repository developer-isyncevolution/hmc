@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
<div class="page-title text-center">
        <h3>
        {{isset($customertypes->iCustomerTypeId) ? 'Edit' : 'Add'}} Customer Type
        </h3>
    </div>
  <div class="card">
    <div class="col-lg-12 mx-auto">         
        <form action="{{route('admin.customertype.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($customertypes)) {{$customertypes->iCustomerTypeId}} @endif">
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required mb-2">Name</label>
                <input type="text" class="form-control" id="vTypeName" name="vTypeName" placeholder="Title" value="@if(old('vTypeName')!=''){{old('vTypeName')}}@elseif(isset($customertypes->vTypeName)){{$customertypes->vTypeName}}@else{{old('vTypeName')}}@endif">
                <div class="text-danger" style="display: none;" id="vTypeName_error">Please enter name</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required mb-2">Status</label>
                <select id="eStatus" name="eStatus" >
                    <option value="Active" @if(isset($customertypes)) @if($customertypes->eStatus == 'Active') selected @endif @endif>Active</option>
                    <option value="Inactive" @if(isset($customertypes)) @if($customertypes->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
                </select>
                <div class="mt-1">
                    @error('eStatus')
                        <code>{{ $message }}</code>
                    @enderror
                </div>
            </div>       
            <div class="col-12 align-self-end d-inline-block text-center">
                <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
                <a href="{{route('admin.customertype')}}" class="btn back-btn me-2">Back</a>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
   <script>
 $('#eStatus').selectize();
  $(document).on('click','#submit',function()
  {
    iCustomerTypeId = $("#id").val();
    vTypeName           = $("#vTypeName").val();

    var error = false;

    if(vTypeName.length == ''){
      $("#vTypeName_error").show();
      error = true;
    } else {
      $("#vTypeName_error").hide();
    }
    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
    </script>
@endsection
