@extends('layouts.admin.index')
@section('content')

@php
$url = Request::segment(4);
@endphp

<div class="main-panel">
    <div class="col-lg-11 mx-auto">
        <div class="listing-page">
        <form action="{{route('admin.modulePermission.store')}}" name="frm" id="frm" class="row g-4 add-banner mt-0" method="post" enctype="multipart/form-data">
            @csrf

            <input type="hidden" name="id" value="">

            <div class="col-xl-6 col-lg-12 col-md-6">
                <label>Role</label>
                <select class="form-control" id="department" name="iDepartmentId">
                    <option value="">Select Role</option>
                    @foreach($department as $key => $value)
                    <option value="{{ $value->iDepartmentId }}" @if($url == $value->iDepartmentId) selected @endif>{{ $value->vName }}</option>
                    @endforeach
                </select>
            </div>  

            @if(isset($iPermissionId))
            {{-- For Dlip Permission Start --}}
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Modules</th>
                                <th>Add Slip</th>
                                <th>Edit Stage</th>
                                <th>Read Slip</th>
                                <th>Virual Slip</th>
                                <th>Paper Slip</th>
                                <th>New Stage</th>
                                <th>Ready to Send</th>
                                <th>View Driver History</th>
                                <th>Pick Up</th>
                                <th>Drop Off</th>
                                <th>Direction</th>
                                <th>Add Call Log</th>
                                <th>History Call</th>
                                <th>Last Modified</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($modules as $key => $value)
                            @if((\App\Libraries\General::admin_info()['customerType'] == 'Lab Admin' ) && $value->iModuleId ==9)
                            
                            <tr>
                                <td>
                                     <input type="hidden" name="permission_slip[0][iModuleId]" value="{{$value->iModuleId}}"> 
                                    {{ $value->vTitle }} 
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eAddSlip checkboxalleAddSlip" name="permission_slip[0][eAddSlip]" value="@if(isset($modulesPermission[0]->eAddSlip) && !empty($modulesPermission))@if($modulesPermission[0]->eAddSlip == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->eAddSlip) && !empty($modulesPermission)) @if($modulesPermission[0]->eAddSlip == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eEditStage checkboxalleEditStage" name="permission_slip[{{0}}][eEditStage]" value="@if(isset($modulesPermission[0]->eEditStage) && !empty($modulesPermission))@if($modulesPermission[0]->eEditStage == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->eEditStage) && !empty($modulesPermission)) @if($modulesPermission[0]->eEditStage == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eReadSlip checkboxalleReadSlip" name="permission_slip[{{0}}][eReadSlip]" value="@if(isset($modulesPermission[0]->eReadSlip) && !empty($modulesPermission))@if($modulesPermission[0]->eReadSlip == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->eReadSlip) && !empty($modulesPermission)) @if($modulesPermission[0]->eReadSlip == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eVirualSlip checkboxalleVirualSlip" name="permission_slip[{{0}}][eVirualSlip]" value="@if(isset($modulesPermission[0]->eVirualSlip) && !empty($modulesPermission))@if($modulesPermission[0]->eVirualSlip == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->eVirualSlip) && !empty($modulesPermission)) @if($modulesPermission[0]->eVirualSlip == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_ePaperSlip checkboxallPaperSlip" name="permission_slip[{{0}}][ePaperSlip]" value="@if(isset($modulesPermission[0]->ePaperSlip) && !empty($modulesPermission))@if($modulesPermission[0]->ePaperSlip == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->ePaperSlip) && !empty($modulesPermission)) @if($modulesPermission[0]->ePaperSlip == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eNewStage checkboxallNewStage" name="permission_slip[{{0}}][eNewStage]" value="@if(isset($modulesPermission[0]->eNewStage) && !empty($modulesPermission))@if($modulesPermission[0]->eNewStage == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->eNewStage) && !empty($modulesPermission)) @if($modulesPermission[0]->eNewStage == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eReadyToSend checkboxallReadyToSend" name="permission_slip[{{0}}][eReadyToSend]" value="@if(isset($modulesPermission[0]->eReadyToSend) && !empty($modulesPermission))@if($modulesPermission[0]->eReadyToSend == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->eReadyToSend) && !empty($modulesPermission)) @if($modulesPermission[0]->eReadyToSend == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eViewDriverHistory checkboxallViewDriverHistory" name="permission_slip[{{0}}][eViewDriverHistory]" value="@if(isset($modulesPermission[0]->eViewDriverHistory) && !empty($modulesPermission))@if($modulesPermission[0]->eViewDriverHistory == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->eViewDriverHistory) && !empty($modulesPermission)) @if($modulesPermission[0]->eViewDriverHistory == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_ePickUp checkboxallPickUp" name="permission_slip[{{0}}][ePickUp]" value="@if(isset($modulesPermission[0]->ePickUp) && !empty($modulesPermission))@if($modulesPermission[0]->ePickUp == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->ePickUp) && !empty($modulesPermission)) @if($modulesPermission[0]->ePickUp == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eDropOff checkboxallDropOff" name="permission_slip[{{0}}][eDropOff]" value="@if(isset($modulesPermission[0]->eDropOff) && !empty($modulesPermission))@if($modulesPermission[0]->eDropOff == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->eDropOff) && !empty($modulesPermission)) @if($modulesPermission[0]->eDropOff == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eDirection checkboxallDirection" name="permission_slip[{{0}}][eDirection]" value="@if(isset($modulesPermission[0]->eDirection) && !empty($modulesPermission))@if($modulesPermission[0]->eDirection == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->eDirection) && !empty($modulesPermission)) @if($modulesPermission[0]->eDirection == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eAddCallLog checkboxallAddCallLog" name="permission_slip[{{0}}][eAddCallLog]" value="@if(isset($modulesPermission[0]->eAddCallLog) && !empty($modulesPermission))@if($modulesPermission[0]->eAddCallLog == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->eAddCallLog) && !empty($modulesPermission)) @if($modulesPermission[0]->eAddCallLog == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eHistoryCall checkboxallHistoryCall" name="permission_slip[{{0}}][eHistoryCall]" value="@if(isset($modulesPermission[0]->eHistoryCall) && !empty($modulesPermission))@if($modulesPermission[0]->eHistoryCall == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->eHistoryCall) && !empty($modulesPermission)) @if($modulesPermission[0]->eHistoryCall == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eLastModified checkboxallHistoryCall" name="permission_slip[{{0}}][eLastModified]" value="@if(isset($modulesPermission[0]->eLastModified) && !empty($modulesPermission))@if($modulesPermission[0]->eLastModified == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[0]->eLastModified) && !empty($modulesPermission)) @if($modulesPermission[0]->eLastModified == 'Yes') checked @endif @endif>
                                </td>
                            </tr>
                            @elseif($value->iModuleId ==9 )
                            <tr>
                              
                                <td>
                                     <input type="hidden" name="permission_slip[{{$key}}][iModuleId]" value="{{$value->iModuleId}}"> 
                                    {{ $value->vTitle }} 
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eAddSlip checkboxalleAddSlip" name="permission_slip[{{$key}}][eAddSlip]" value="@if(isset($modulesPermission[$key]->eAddSlip) && !empty($modulesPermission))@if($modulesPermission[$key]->eAddSlip == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eAddSlip) && !empty($modulesPermission)) @if($modulesPermission[$key]->eAddSlip == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eEditStage checkboxalleEditStage" name="permission_slip[{{$key}}][eEditStage]" value="@if(isset($modulesPermission[$key]->eEditStage) && !empty($modulesPermission))@if($modulesPermission[$key]->eEditStage == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eEditStage) && !empty($modulesPermission)) @if($modulesPermission[$key]->eEditStage == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eReadSlip checkboxalleReadSlip" name="permission_slip[{{$key}}][eReadSlip]" value="@if(isset($modulesPermission[$key]->eReadSlip) && !empty($modulesPermission))@if($modulesPermission[$key]->eReadSlip == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eReadSlip) && !empty($modulesPermission)) @if($modulesPermission[$key]->eReadSlip == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eVirualSlip checkboxalleVirualSlip" name="permission_slip[{{$key}}][eVirualSlip]" value="@if(isset($modulesPermission[$key]->eVirualSlip) && !empty($modulesPermission))@if($modulesPermission[$key]->eVirualSlip == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eVirualSlip) && !empty($modulesPermission)) @if($modulesPermission[$key]->eVirualSlip == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_ePaperSlip checkboxallPaperSlip" name="permission_slip[{{$key}}][ePaperSlip]" value="@if(isset($modulesPermission[$key]->ePaperSlip) && !empty($modulesPermission))@if($modulesPermission[$key]->ePaperSlip == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->ePaperSlip) && !empty($modulesPermission)) @if($modulesPermission[$key]->ePaperSlip == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eNewStage checkboxallNewStage" name="permission_slip[{{$key}}][eNewStage]" value="@if(isset($modulesPermission[$key]->eNewStage) && !empty($modulesPermission))@if($modulesPermission[$key]->eNewStage == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eNewStage) && !empty($modulesPermission)) @if($modulesPermission[$key]->eNewStage == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eReadyToSend checkboxallReadyToSend" name="permission_slip[{{$key}}][eReadyToSend]" value="@if(isset($modulesPermission[$key]->eReadyToSend) && !empty($modulesPermission))@if($modulesPermission[$key]->eReadyToSend == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eReadyToSend) && !empty($modulesPermission)) @if($modulesPermission[$key]->eReadyToSend == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eViewDriverHistory checkboxallViewDriverHistory" name="permission_slip[{{$key}}][eViewDriverHistory]" value="@if(isset($modulesPermission[$key]->eViewDriverHistory) && !empty($modulesPermission))@if($modulesPermission[$key]->eViewDriverHistory == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eViewDriverHistory) && !empty($modulesPermission)) @if($modulesPermission[$key]->eViewDriverHistory == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_ePickUp checkboxallPickUp" name="permission_slip[{{$key}}][ePickUp]" value="@if(isset($modulesPermission[$key]->ePickUp) && !empty($modulesPermission))@if($modulesPermission[$key]->ePickUp == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->ePickUp) && !empty($modulesPermission)) @if($modulesPermission[$key]->ePickUp == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eDropOff checkboxallDropOff" name="permission_slip[{{$key}}][eDropOff]" value="@if(isset($modulesPermission[$key]->eDropOff) && !empty($modulesPermission))@if($modulesPermission[$key]->eDropOff == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eDropOff) && !empty($modulesPermission)) @if($modulesPermission[$key]->eDropOff == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eDirection checkboxallDirection" name="permission_slip[{{$key}}][eDirection]" value="@if(isset($modulesPermission[$key]->eDirection) && !empty($modulesPermission))@if($modulesPermission[$key]->eDirection == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eDirection) && !empty($modulesPermission)) @if($modulesPermission[$key]->eDirection == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eAddCallLog checkboxallAddCallLog" name="permission_slip[{{$key}}][eAddCallLog]" value="@if(isset($modulesPermission[$key]->eAddCallLog) && !empty($modulesPermission))@if($modulesPermission[$key]->eAddCallLog == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eAddCallLog) && !empty($modulesPermission)) @if($modulesPermission[$key]->eAddCallLog == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eHistoryCall checkboxallHistoryCall" name="permission_slip[{{$key}}][eHistoryCall]" value="@if(isset($modulesPermission[$key]->eHistoryCall) && !empty($modulesPermission))@if($modulesPermission[$key]->eHistoryCall == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eHistoryCall) && !empty($modulesPermission)) @if($modulesPermission[$key]->eHistoryCall == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_eLastModified checkboxallHistoryCall" name="permission_slip[{{$key}}][eLastModified]" value="@if(isset($modulesPermission[$key]->eLastModified) && !empty($modulesPermission))@if($modulesPermission[$key]->eLastModified == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eLastModified) && !empty($modulesPermission)) @if($modulesPermission[$key]->eLastModified == 'Yes') checked @endif @endif>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- For Dlip Permission End --}}
            @if(\App\Libraries\General::admin_info()['customerType'] !='Lab Admin')
            <div class="col-xl-12 col-lg-12 col-md-12 mt-12">
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Modules</th>
                                <th>Read
                                    <br><input id="selectallRead" type="checkbox" class="css-checkbox">
                                </th>
                                <th>Create
                                    <br><input id="selectallCreate" type="checkbox" class="css-checkbox">
                                </th>
                                <th>Edit
                                    <br><input id="selectallEdit" type="checkbox" class="css-checkbox">
                                </th>
                                <th>Delete
                                    <br><input id="selectallDelete" type="checkbox" class="css-checkbox">
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($modules as $key => $value)
                            @if($value->iModuleId !=9)
                            <tr>
                                <td>
                                    <input type="hidden" name="permission[{{$key}}][iModuleId]" value="{{$value->iModuleId}}">
                                    {{ $value->vTitle }}
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_read checkboxallRead" name="permission[{{$key}}][eRead]" value="@if(isset($modulesPermission[$key]->eRead) && !empty($modulesPermission))@if($modulesPermission[$key]->eRead == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eRead) && !empty($modulesPermission)) @if($modulesPermission[$key]->eRead == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_create checkboxallCreate" name="permission[{{$key}}][eCreate]" value="@if(isset($modulesPermission[$key]->eCreate) && !empty($modulesPermission))@if($modulesPermission[$key]->eCreate == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eCreate) && !empty($modulesPermission)) @if($modulesPermission[$key]->eCreate == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_edit checkboxallEdit" name="permission[{{$key}}][eEdit]" value="@if(isset($modulesPermission[$key]->eEdit) && !empty($modulesPermission))@if($modulesPermission[$key]->eEdit == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eEdit) && !empty($modulesPermission)) @if($modulesPermission[$key]->eEdit == 'Yes') checked @endif @endif>
                                </td>
                                <td>
                                    <input type="checkbox" class="permission_delete checkboxallDelete" name="permission[{{$key}}][eDelete]" value="@if(isset($modulesPermission[$key]->eDelete) && !empty($modulesPermission))@if($modulesPermission[$key]->eDelete == 'Yes') Yes @else No @endif @endif" data-id="{{$value->iModuleId}}" @if(isset($modulesPermission[$key]->eDelete) && !empty($modulesPermission)) @if($modulesPermission[$key]->eDelete == 'Yes') checked @endif @endif>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @endif
            @endif

            <hr class="mx-0">
            
            <div class="col-12 align-self-end d-inline-block mt-0">
                @if(isset($iPermissionId))
                <a href="javascript:;" class="btn submit-btn submit">Submit</a>
                <a href="{{route('admin.modulePermission.create')}}" class="back-btn btn">Back</a>
                @endif
            </div>
        </form>
</div>
    </div>
</div>
@endsection
@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script>

 $('#department').select2({
        width: '100%',
        placeholder: "Select status",
        allowClear: true
    });

$(document).on('click','.submit',function(){
    var error = false;
    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

});


$("#department").change(function(){
    var department = $("#department").val();

    var url = '{{ url("admin/module-permission/edit/") }}'+'/'+department;
    
    if (department != '') {
        window.location.href = url;
    }

});
$("#selectallRead").click(function()
{
    if(this.checked){
        $('.checkboxallRead').each(function(){
            $(".checkboxallRead").prop('checked', true);
            $(".permission_read").val('Yes');
        });
    }else{
        $('.checkboxallRead').each(function(){
            $(".checkboxallRead").prop('checked', false);
            $(".permission_read").val('No');
        });
    }
});
$("#selectallCreate").click(function()
{
    if(this.checked){
        $('.checkboxallCreate').each(function(){
            $(".checkboxallCreate").prop('checked', true);
            $(".permission_create").val('Yes');
        });
    }else{
        $('.checkboxallCreate').each(function(){
            $(".checkboxallCreate").prop('checked', false);
            $(".permission_create").val('No');
        });
    }
});

$("#selectallEdit").click(function()
{
    if(this.checked){
        $('.checkboxallEdit').each(function(){
            $(".checkboxallEdit").prop('checked', true);
            $(".permission_edit").val('Yes');
        });
    }else{
        $('.checkboxallEdit').each(function(){
            $(".checkboxallEdit").prop('checked', false);
            $(".permission_edit").val('No');
        });
    }
});

$("#selectallDelete").click(function()
{
    if(this.checked){
        $('.checkboxallDelete').each(function(){
            $(".checkboxallDelete").prop('checked', true);
            $(".permission_delete").val('Yes');
        });
    }else{
        $('.checkboxallDelete').each(function(){
            $(".checkboxallDelete").prop('checked', false);
            $(".permission_delete").val('No');
        });
    }
});

$(".permission_read").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});

$(".permission_create").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});

$(".permission_edit").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});

$(".permission_delete").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_eAddSlip").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_eEditStage").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_eReadSlip").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_eVirualSlip").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_ePaperSlip").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_eNewStage").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_eReadyToSend").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_eViewDriverHistory").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_ePickUp").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_eDropOff").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_eDirection").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_eAddCallLog").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_eHistoryCall").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});
$(".permission_eLastModified").click(function()
{
    if(this.checked){
        $(this).prop('checked', true);
        $(this).val('Yes');
    }else{
        $(this).prop('checked', false);
        $(this).val('No');
    }
});

</script>
@endsection