@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
  <div class="page-title text-center">
    <h3>
      {{isset($departments->iDepartmentId) ? 'Edit' : 'Add'}} Department
    </h3>
</div>
  <div class="card">
    <div class="col-lg-12 mx-auto">        
        <form action="{{route('admin.department.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($departments)) {{$departments->iDepartmentId}} @endif">
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label >Name</label>
                <input type="text" class="form-control" id="vName" name="vName" placeholder="Title" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($departments->vName)){{$departments->vName}}@else{{old('vName')}}@endif">
                <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label >Status</label>
                <select id="eStatus" name="eStatus">
                    <option value="Active" @if(isset($departments)) @if($departments->eStatus == 'Active') selected @endif @endif>Active</option>
                    <option value="Inactive" @if(isset($departments)) @if($departments->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
                </select>
                <div class="mt-1">
                    @error('eStatus')
                        <code>{{ $message }}</code>
                    @enderror
                </div>
            </div>       
            <div class="col-12 align-self-end d-inline-block ">
                <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
                <a href="{{route('admin.department')}}" class="btn back-btn me-2">Back</a>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
   $('#eStatus').selectize();
  $(document).on('click','#submit',function()
  {
    iDepartmentId = $("#id").val();
    vName           = $("#vName").val();

    var error = false;

    if(vName.length == 0){
      $("#vName_error").show();
      error = true;
    } else {
      $("#vName_error").hide();
    }
    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
    </script>
@endsection
