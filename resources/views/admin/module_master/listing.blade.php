@extends('layouts.admin.index')
@section('content')
    <div class="main-panel">
        <div class="page-title text-center">
            <h3>
                Module Master
            </h3>
        </div>
        <div class="col-lg-12 mx-auto">
            <div class="listing-page">
                <div class="card mb-5 mb-xl-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-mid">
                                    <ul class="d-flex list-unstyled">
                                        @if(\App\Libraries\General::check_permission('','eCreate') == true)
                                        <li>
                                            <a href="{{ route('admin.moduleMaster.create') }}" class="btn add-btn create_permission mx-2">
                                                <i class="fa fa-plus"></i> Add
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mx-auto">
                            <div class="custome-serce-box">
                                <input type="text" class="form-control" id="keyword" name="search" placeholder="Search">
                                <span class="search-btn"><i class="fas fa-search"></i></span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr class="fw-bolder text-muted">
                                <th class="w-25px text-center" data-orderable="false">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input class="form-check-input" type="checkbox" value="1" data-kt-check="true"
                                            data-kt-check-target=".widget-13-check" id="selectall" type="checkbox"
                                            name="selectall">
                                        <label for="selectall">&nbsp;</label>
                                    </div>
                                </th>
                                <th class="min-w-100px">
                                    <a id="vTitle" class="sort" data-column="vTitle" data-order="ASC" href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Title</span>
                                    </a>
                                </th>
                                <th class="max-w-100px">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader" width="250px"
                                height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function() {
            setTimeout(function() {
                $.ajax({
                    url: "{{ route('admin.moduleMaster.ajax_listing') }}",
                    type: "get",
                    data: {},
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
            }, 1000);
        });

        $("#selectall").click(function() {
            if (this.checked) {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', true);
                });
            } else {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', false);
                });
            }
        });

        $(document).on('click', '#delete_btn', function() {
            var iModuleId = [];

            $("input[name='iModuleId[]']:checked").each(function() {
                iModuleId.push($(this).val());
            });

            var iModuleId = iModuleId.join(",");

            if (iModuleId.length == 0) {
                alert('Please select records.')
            } else {
                swal({
                        title: "Are you sure delete all Module Master ?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "{{ route('admin.moduleMaster.ajax_listing') }}",
                                type: "GET",
                                data: {
                                    iModuleId: iModuleId,
                                    action: 'multiple_delete'
                                },
                                success: function(response) {
                                    $("#table_record").html(response);
                                    $("#ajax-loader").hide();
                                    notification_error("Module Master Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                                }
                            });
                        }
                    });
            }
        });

        $("#keyword").keyup(function() {
            var keyword = $("#keyword").val();

            setTimeout(function() {
                $.ajax({
                    url: "{{ route('admin.moduleMaster.ajax_listing') }}",
                    type: "get",
                    data: {
                        keyword: keyword,
                        action: 'search'
                    },
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                        $("#table_record").show();
                    }
                });
            }, 1000);
        });

        $(document).on('click', '.sort', function() {
            column = $(this).data("column");
            order = $(this).attr('data-order');


            if (order == "ASC") {
                $(this).attr('data-order', 'DESC');
            } else {
                $(this).attr('data-order', 'ASC');
            }

            // $("#ajax-loader").show();

            setTimeout(function() {
                $.ajax({
                    url: "{{ route('admin.moduleMaster.ajax_listing') }}",
                    type: "get",
                    data: {
                        column: column,
                        order,
                        order,
                        action: 'sort'
                    },
                    success: function(response) {
                        console.log(response);
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
            }, 1000);
        });

        // $(document).on('click', '#delete', function() {
        //     if (confirm('Are you sure delete this data ?')) {
        //         id = $(this).data("id");

        //         $("#ajax-loader").show();

        //         setTimeout(function() {
        //             $.ajax({
        //                 url: "{{ route('admin.moduleMaster.ajax_listing') }}",
        //                 type: "GET",
        //                 data: {
        //                     iModuleId: id,
        //                     action: 'delete'
        //                 },
        //                 success: function(response) {
        //                     $("#table_record").html(response);
        //                     $("#ajax-loader").hide();
        //                 }
        //             });
        //         }, 1000);
        //     }
        // });

        $(document).on('click', '#delete', function() {
            swal({
                    title: "Are you sure delete this Module Master ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        id = $(this).data("id");

                        $("#ajax-loader").show();

                        $.ajax({
                            url: "{{ route('admin.moduleMaster.ajax_listing') }}",
                            type: "get",
                            data: {
                                id: id,
                                action: 'delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("Module Master Deleted Successfully");
                            }
                        });
                    }
                })
        });

        $(document).on('click', '.ajax_page', function() {
            pages = $(this).data("pages");
            // keyword = $("#keyword").val();
            var keyword = $("#on_change").val();

            $("#table_record").html('');
            $("#ajax-loader").show();

            url = "{{ route('admin.moduleMaster.ajax_listing') }}";

            setTimeout(function() {
                $.ajax({
                    url: url,
                    type: "GET",
                    data: {
                        pages: pages,
                        keyword: keyword,
                        action: 'search'
                    },
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
            }, 500);
        });

        $("#keyword").on('keydown', function(e) {
            if (e.keyCode == 27) {
                $("#keyword").val('');
                $(".searchRemove").hide();
            } else {
                $(".searchRemove").show();
            }
        });
        $(document).on('change', '#page_limit', function() {
                    limit_page = this.value;
                    $("#table_record").html('');
                    $("#ajax-loader").show();
                    url = "{{ route('admin.moduleMaster.ajax_listing') }}";

                    setTimeout(function() {
                        $.ajax({
                            url: url,
                            type: "get",
                            data: {
                                limit_page: limit_page
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                            }
                        });
                        // hideLoader();
                    }, 500);
                });
    </script>
@endsection
