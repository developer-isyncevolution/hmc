@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
    <div class="page-title text-center">
        <h3>
            Module Master
        </h3>
    </div>
    <div class="col-lg-11 mx-auto">
        <div class="listing-page">
        <form action="{{route('admin.moduleMaster.store')}}" name="frm" id="frm" class="row g-4 add-banner mt-0" method="post" enctype="multipart/form-data">
            @csrf

            <input type="hidden" name="id" value="@if(isset($data)){{ $data->iModuleId }}@endif">
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Title</label>
                <input type="text" class="form-control" id="vTitle" name="vTitle" placeholder="Title" value="@if(old('vTitle')!=''){{old('vTitle')}}@elseif(isset($data->vTitle)){{$data->vTitle}}@else{{old('vTitle')}}@endif">
                <div class="text-danger" style="display: none;" id="vTitle_error">Please enter title</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Module Name</label>
                <input type="text" class="form-control" id="vModuleName" name="vModuleName" placeholder=" Module Name" value="@if(old('vModuleName')!=''){{old('vModuleName')}}@elseif(isset($data->vModuleName)){{$data->vModuleName}}@else{{old('vModuleName')}}@endif">
                <div class="text-danger" style="display: none;" id="vModuleName_error">Please enter module name</div>
            </div>

            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Status</label>
                <select id="eStatus" name="eStatus" class="form-control">
                  <option value="Active" @if(isset($data)) @if($data->eStatus == 'Active') selected @endif @endif>Active</option>
                  <option value="Inactive" @if(isset($data)) @if($data->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
                </select>
              </div>       

            <hr class="mx-0">
            
            <div class="col-12 align-self-end d-inline-block mt-0">
                <a href="javascript:;" class="btn submit-btn submit">Submit</a>
                <a href="{{route('admin.moduleMaster')}}" class="back-btn btn">Back</a>
            </div>
        </form>
</div>
    </div>
</div>
@endsection
@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script type="text/javascript">
 $('#eStatus').select2({
        width: '100%',
        placeholder: "Select customer",
        allowClear: true
    });
$(document).on('click','.submit',function(){
    vTitle              = $("#vTitle").val();
    eStatus             = $("#eStatus").val();
    vModuleName         = $("#vModuleName").val();

    var error = false;

    if(vTitle.length == ""){
        error = true;
        $("#vTitle_error").show();
    }
    else{
        $("#vTitle_error").hide();
    }

    if(vModuleName.length == ""){
        error = true;
        $("#vModuleName_error").show();
    }
    else{
        $("#vModuleName_error").hide();
    }

    if(eStatus.length == ""){
        error = true;
        $("#eStatus_error").show();
    }
    else{
        $("#eStatus_error").hide();
    }

    console.log({vTitle, vModuleName, eStatus});

    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

});

</script>
@endsection