@if(count($data) > 0)
@foreach($data as $key => $value)
<tr>
	<td>
		<div class="form-check form-check-sm form-check-custom form-check-solid">
			<input id="Admin_ID_{{$value->iAdminId}}" type="checkbox" name="Admin_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iAdminId}}">
			<label for="Admin_ID_{{$value->iAdminId}}">&nbsp;</label>
		</div>
	</td>
	<!-- <td>
		<img alt="{{$value->vImage}}" style="height:50px; width:50px;" src="{{asset('uploads/admin/'.$value->vImage)}}">
	</td> -->

	<td>
		{{$value->vName}}
	</td>

	<td>
		{{$value->email}}
	</td>

	<td>
		{{$value->vMobile}}
	</td>
	<td>
		<span class="badge badge-light-success">{{$value->eStatus}}</span>
	</td>
	<td class="d-flex">
		@if(\App\Libraries\General::check_permission('','eEdit') == true)
		<a href="{{route('admin.admin.edit',$value->iAdminId)}}" title="Edit" class="btn-icon edit_permission edit-icon me-4">
			<!--begin::Svg Icon | path: icons/duotune/art/art005.svg-->
			<i class="fad fa-pencil"></i>
			<!--end::Svg Icon-->
		</a>
		@endif
		@if(\App\Libraries\General::check_permission('','eDelete') == true)
		<a href="javascript:;" class="btn-icon me-3  delete_permission delete-icon" title="Delete" id="delete" data-id="{{$value->iAdminId}}">
			<!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
		<i class="fad fa-trash-alt"></i>
			<!--end::Svg Icon-->
		</a>
		@endif
	</td>
</tr>
@endforeach
<tr>
	<td colspan="15" class="text-start">
	@if(\App\Libraries\General::check_permission('','eDelete') == true)
	<a href="javascript:;" class="btn-icon me-3  delete_permission delete-icon" title="Multiple Delete" id="delete_btn" data-id="{{$value->iAdminId}}">
			<!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
		<i class="fad fa-trash-alt"></i>
			<!--end::Svg Icon-->
		</a>
	</td>
	@endif
</tr>
@else
<tr class="text-center">
	<td colspan="9">No Record Found</td>
</tr>
@endif