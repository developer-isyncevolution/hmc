@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
  <div class="page-title text-center">
    <h3>
      Changes password
    </h3>
  </div>
  <div class="card">
  <div class="card-header border-0 p-0">
      <ul class="c-nav-tab">
        <li>
        <a href="{{route('admin.admin.edit',$admins->iAdminId)}}" aria-controls="#tab-{{ $admins->iAdminId }}" class="add-btn btn mx-2">Admin</a>
        </li>
        <li> 
          <a href="{{route('admin.admin.ChangePassword',$admins->iAdminId)}}" aria-controls="#tab-{{ $admins->iAdminId }}" class="add-btn btn mx-2 active">Changes password</a>
        </li>
      </ul>
    </div>
    <div class="col-lg-12 mx-auto">
      @if(isset($admins))
      <!-- <div class="card-header py-5">
        <h3 class="card-title align-items-start flex-column">
          <a href="{{route('admin.admin.ChangePassword',$admins->iAdminId)}}" aria-controls="#tab-{{ $admins->iAdminId }}"> <span class="card-label fw-bolder fs-3 mb-1"> </span> </a>
        </h3>
        <ul class="d-flex justify-content-between my-2">
          <li class="d-flex">
            <a href="{{route('admin.admin.edit',$admins->iAdminId)}}" aria-controls="#tab-{{ $admins->iAdminId }}" class="btn btn-primary btn-lg">Admin</a>
          </li>
        </ul>
      </div> -->
      @endif
      <form action="{{route('admin.admin.changePassword')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($admins)) {{$admins->iAdminId}} @endif">
        <div class="col-xl-6 col-lg-12 col-md-6">
          <label>New password</label>
          <input type="Password" class="form-control" id="vPassword" name="vPassword" placeholder="Password" value="">
          <div class="text-danger" style="display: none;" id="vPassword_error">Please enter password</div>
        </div>
        <div class="col-xl-6 col-lg-12 col-md-6">
          <label>Confirm new password</label>
          <input type="Password" class="form-control" id="vPassword2" name="vPassword2" placeholder="Confirm password" value="">
          <div class="text-danger" style="display: none;" id="vPassword2_error">Please enter confirm password</div>
          <div class="text-danger" id="vPassword2_same_error" style="display: none;">Password should match</div>
        </div>

        <div class="col-4 align-self-end d-inline-block mx-auto text-center">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.admin')}}" class="btn back-btn me-2">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script>
  $(document).on('click', '#submit', function() {
    vPassword = $("#vPassword").val();
    vPassword2 = $("#vPassword2").val();

    var error = false;
    if (vPassword.length == 0) {
      $("#vPassword_error").show();
      error = true;
    } else {
      $("#vPassword_error").hide();
    }

    if (vPassword2.length == 0) {
      $("#vPassword2_error").show();
      error = true;
    } else {
      $("#vPassword2_error").hide();
    }

    if (vPassword.length != 0 && vPassword2.length != 0) {
      if (vPassword != vPassword2) {
        $("#vPassword2_same_error").show();
        return true;
      } else {
        $("#vPassword2_same_error").hide();
      }
    } else {
      $("#vPassword2_same_error").hide();
    }
    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
</script>
@endsection