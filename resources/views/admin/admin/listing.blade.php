@extends('layouts.admin.index')
@section('content')
    <!-- BEGIN: Content-->
    <!-- DOM - jQuery events table -->
    <div class="main-panel">
        <div class="page-title text-center">
            <h3>
                Super Admin
            </h3>
        </div>

        <div class="col-lg-12 mx-auto">
            <div class="card mb-5 mb-xl-4">
                <div class="row">
                    @if(\App\Libraries\General::check_permission('AdminController','eCreate') == 'true')
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-mid">
                                <ul class="d-flex list-unstyled">
                                    @if(\App\Libraries\General::check_permission('','eCreate') == true)
                                    <li>
                                        <a href="{{ route('admin.admin.create') }}" class="btn add-btn create_permission mx-2">
                                            <i class="fa fa-plus"></i> Add
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="col-lg-6 mx-auto">
                        <div class="custome-serce-box">
                            <input type="text" class="form-control" id="keyword" name="search" class="search"
                                placeholder="Search">
                            <span class="search-btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>



            <div class="listing-page">
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr class="fw-bolder text-muted">
                                <th class="w-25px" data-orderable="false">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input class="form-check-input" type="checkbox" value="1" data-kt-check="true"
                                            data-kt-check-target=".widget-13-check" id="selectall" type="checkbox"
                                            name="selectall">
                                        <label for="selectall">&nbsp;</label>
                                    </div>
                                </th>
                                <!-- <th class="min-w-150px">Image</th> -->
                                <th class="min-w-140px"><a id="vName" class="sort" data-column="vName"
                                        data-order="ASC" href="#"><span
                                            class="text-muted fw-bold text-muted d-block fs-7">Name</span></a></th>
                                <th class="min-w-120px"><a id="email" class="sort" data-column="email"
                                        data-order="ASC" href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Email</span></a>
                                </th>
                                <th class="min-w-120px"><a id="vMobile" class="sort" data-column="vMobile"
                                        data-order="ASC" href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Mobile</span> </a>
                                </th>
                                <th class="min-w-120px"><a id="eStatus" class="sort" data-column="eStatus"
                                        data-order="ASC" href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Status</span></a>
                                </th>
                                <th class="min-w-100px">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader" width="250px"
                                height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection


@section('custom-js')
<script>
       
        $(document).ready(function() {
           
            $.ajax({
                url: "{{ route('admin.admin.ajaxListing') }}",
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $("#selectall").click(function() {
            if (this.checked) {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', true);
                });
            } else {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', false);
                });
            }
        });


        $("#keyword").keyup(function() {
            var keyword = $("#keyword").val();

            $("#ajax-loader").show();
            $.ajax({
                url: "{{ route('admin.admin.ajaxListing') }}",
                type: "get",
                data: {
                    keyword: keyword,
                    action: 'search'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '#delete_btn', function() {
            var id = [];

            $("input[name='Admin_ID[]']:checked").each(function() {
                id.push($(this).val());
            });

            var id = id.join(",");

            if (id.length == 0) {
                alert('Please select records.')
            } else {
                swal({
                        title: "Are you sure delete all Admin ?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "{{ route('admin.admin.ajaxListing') }}",
                                type: "get",
                                data: {
                                    id: id,
                                    action: 'multiple_delete'
                                },
                                success: function(response) {
                                    $("#table_record").html(response);
                                    $("#ajax-loader").hide();
                                    notification_error("Admin Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                                }
                            });
                        }
                    });
            }
        });
        $(document).on('click', '#delete', function() {
            
            setTimeout(function() {
                swal({
                        title: "Are you sure delete this Admin ?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            id = $(this).data("id");
    
                            $("#ajax-loader").show();
                            $.ajax({
                                url: "{{ route('admin.admin.ajaxListing') }}",
                                type: "get",
                                data: {
                                    id: id,
                                    action: 'delete'
                                },
                                success: function(response) {
                                    $("#table_record").html(response);
                                    $("#ajax-loader").hide();
                                    // notification_error("Access Denied");
                                    notification_error("Admin Deleted Successfully");
                                    // setTimeout(function() {
                                    //     location.reload();
                                    // }, 1000);
                                }
                            });
                        }
                    })
            }, 1000);
        });
        $(document).on('click', '.sort', function() {
            column = $(this).data("column");
            order = $(this).attr('data-order');


            if (order == "ASC") {
                $(this).attr('data-order', 'DESC');
            } else {
                $(this).attr('data-order', 'ASC');
            }

            $("#ajax-loader").show();
            $.ajax({
                url: "{{ route('admin.admin.ajaxListing') }}",
                type: "get",
                data: {
                    column: column,
                    order,
                    order,
                    action: 'sort'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '.ajax_page', function() {
            pages = $(this).data("pages");

            $("#table_record").html('');
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.admin.ajaxListing') }}",
                type: "POST",
                data: {
                    pages: pages
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

       
    </script>
@endsection
