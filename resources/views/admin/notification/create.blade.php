@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
  <div class="card">
    <div class="col-lg-12 mx-auto">
        <div class="card-header">
          <h4 class="card-title" id="row-separator-basic-form">{{isset($notifications->iNotificationId) ? 'Edit' : 'Add'}} notification</h4>
        </div>
        <form action="{{route('admin.notification.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($notifications)) {{$notifications->iNotificationId}} @endif">
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Notification code</label>
                <input type="text" class="form-control" id="vNotificationCode" name="vNotificationCode" placeholder="Notification code" value="@if(old('vNotificationCode')!=''){{old('vNotificationCode')}}@elseif(isset($notifications->vNotificationCode)){{$notifications->vNotificationCode}}@else{{old('vNotificationCode')}}@endif">
                <div class="text-danger" style="display: none;" id="vNotificationCode_error">Enter notification code</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Email</label>
                <select id="eEmail" name="eEmail" class="form-control">
                    <option value="Yes" @if(isset($notifications)) @if($notifications->eEmail == 'Yes') selected @endif @endif>Yes</option>
                    <option value="No" @if(isset($notifications)) @if($notifications->eEmail == 'No') selected @endif @endif>No</option>
                </select>
                <div class="mt-1">
                    @error('eEmail')
                        <code>{{ $message }}</code>
                    @enderror
                </div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label class="required fs-5 fw-bold mb-2">Sms</label>
                <select id="eSms" name="eSms" class="form-control">
                    <option value="Yes" @if(isset($notifications)) @if($notifications->eSms == 'Yes') selected @endif @endif>Yes</option>
                    <option value="No" @if(isset($notifications)) @if($notifications->eSms == 'No') selected @endif @endif>No</option>
                </select>
                <div class="mt-1">
                    @error('eSms')
                        <code>{{ $message }}</code>
                    @enderror
                </div>
            </div>
            <hr>
            <div class="col-12 align-self-end d-inline-block text-center">
              <a type="submit" id="submit" class="btn btn-primary btn-lg">Submit</a>
              <a href="{{route('admin.notification')}}" class="btn btn-info btn-lg">Back</a>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script>
$(document).on('click','#submit',function()
  {
    iNotificationId         = $("#id").val();
    vNotificationCode   = $("#vNotificationCode").val();

    var error = false;

    if(vNotificationCode.length == 0){
      $("#vNotificationCode_error").show();
      error = true;
    } else {
      $("#vNotificationCode_error").hide();
    }

    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
</script>
@endsection
