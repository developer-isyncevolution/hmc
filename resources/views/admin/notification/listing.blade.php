@extends('layouts.admin.index')
@section('content')
<div class="main-panel">
    <div class="col-lg-11 mx-auto">
        <div class="listing-page">
            <div class="card mb-5 mb-xl-8">
                <div class="card-header border-0 py-5">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label fw-bolder fs-3 mb-1">Notification</span>
                    </h3>

                    <ul class="d-flex justify-content-between my-2">
                        <li class="d-flex">
                            <div class="d-flex mx-2">
                                <input type="text" class="form-control" id="keyword" name="search" class="search" placeholder="Search">
                            </div>

                            <a href="javascript:;" id="delete_btn" class="btn btn-danger mx-2"> Multiple Delete</a>

                            <a href="{{route('admin.notification.create')}}" class="btn btn-primary mx-2">
                                Create Notification
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="table-data table-responsive">
                <table class="table">
                    <thead>
                        <tr class="fw-bolder text-muted">
                            <th class="w-25px" data-orderable="false">
                                <div class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" id="selectall" type="checkbox" name="selectall">
                                    <label for="selectall">&nbsp;</label>
                                </div>
                            </th>
                            <th class="min-w-140px">
                                <a id="vNotificationCode" class="sort" data-column="vNotificationCode" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Notification Code</span>
                                </a>
                            </th>
                            <th class="min-w-120px"><a id="eEmail" class="sort" data-column="eEmail" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Email</span>
                                </a>
                            </th>
                            <th class="min-w-120px"><a id="eSms" class="sort" data-column="eSms" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Sms</span>
                                </a>
                            </th>
                            <th class="min-w-100px">
                                <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="table_record">
                    </tbody>
                </table>
                <div class="text-center loaderimg">
                    <div class="loaderinner">
                        <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom-js')
<script type="text/javascript">
    $(document).ready(function()
    {
      $.ajax({
            url: "{{route('admin.notification.ajaxListing')}}",
            type: "get",
            data:  {_token: '{{ csrf_token() }}'}, 
            success: function(response) {
               $("#table_record").html(response);
               $("#ajax-loader").hide();
            }
        });
    });

    $("#keyword").keyup(function(){
        var keyword = $("#keyword").val();
        $("#ajax-loader").show();

        setTimeout(function(){
            $.ajax({
                url: "{{route('admin.notification.ajaxListing')}}",
                type: "get",
                data:  {keyword:keyword,action:'search'}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        }, 1000);
    });
    $(document).on('click','.sort',function(){
        column = $(this).data("column");
        order = $(this).attr('data-order');


        if(order == "ASC"){
            $(this).attr('data-order','DESC');
        } else{
            $(this).attr('data-order','ASC');
        }

        $("#ajax-loader").show();

        setTimeout(function(){
            $.ajax({
                url: "{{route('admin.notification.ajaxListing')}}",
                type: "get",
                data:  {column:column,order,order,action:'sort'}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        }, 1000);
    });

    $("#selectall").click(function()
    {
        if(this.checked){
            $('.checkboxall').each(function(){
                $(".checkboxall").prop('checked', true);
            });
        }else{
            $('.checkboxall').each(function(){
                $(".checkboxall").prop('checked', false);
            });
        }
    });

    $(document).on('click','#delete_btn',function(){
        var id = [];

        $("input[name='Notification_ID[]']:checked").each( function () {
            id.push($(this).val());
        });

        var id = id.join(",");

        if (id.length == 0) {
            alert('Please select records.')
        }else{
                setTimeout(function(){
                $.ajax({
                    url: "{{route('admin.notification.ajaxListing')}}",
                    type: "get",
                    data:  {id:id,action:'multiple_delete'}, 
                    success: function(response) {
                       $("#table_record").html(response);
                       $("#ajax-loader").hide();
                     }
                    });
                }, 1000);
            }
        });
    $(document).on('click','#delete',function(){
        swal({
          title: "Are you sure delete this Notification.?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
            if(willDelete)
            {
                id = $(this).data("id");

                $("#ajax-loader").show();

                setTimeout(function(){
                    $.ajax({
                        url: "{{route('admin.notification.ajaxListing')}}",
                        type: "get",
                        data:  {id:id,action:'delete'}, 
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                            notification_error("Notification Deleted Successfully");
                                setTimeout(function() {
                                   location.reload();
                                }, 1000);
                        }
                    });
                }, 1000);
            }
        })
    });
    $(document).on('click','.ajax_page',function()
    {
        pages = $(this).data("pages");

        $("#table_record").html('');
        $("#ajax-loader").show();

        setTimeout(function(){
            $.ajax({
                url: "{{route('admin.notification.ajaxListing')}}",
                type: "get",
                data:  {pages: pages}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        }, 500);
    });
</script>
@endsection