@if(count($data) > 0)
@foreach($data as $key => $value)
<tr>
	<td class="text-center">
		<div class="form-check form-check-sm form-check-custom form-check-solid">
			<input id="Customer_ID_{{$value->iCustomerId}}" type="checkbox" name="Customer_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iCustomerId}}">
			<label for="Customer_ID_{{$value->iCustomerId}}">&nbsp;</label>
		</div>
	</td>
	<td>{{$value->vTypeName}}</td>
	<td>{{$value->vOfficeName}}</td>
	<td>
		@if(isset($value->vFirstName) && !empty($value->vFirstName) && isset($value->vLastName) && !empty($value->vLastName))
			{{$value->vFirstName.' '.$value->vLastName}}
		@else
			{{$value->vName}}
		@endif
	</td>
	<td>{{$value->vTitle}}</td>

	<td>
		<span class="badge badge-light-success">{{$value->eStatus}}</span>
	</td>

	<td>
		<div class="d-inline-flex align-items-center">
			@if(\App\Libraries\General::check_permission('','eEdit') == true)
			<a href="{{route('admin.customer.edit',$value->iCustomerId)}}" class="btn-icon edit_permission edit-icon me-4">
				<i class="fad fa-pencil"></i>
			</a>
			<a href="{{route('admin.customer.ChangePassword',$value->iCustomerId)}}" class="btn-icon me-4">
				<i class="fad fa-key"></i>
			</a>
			@endif
			@if(\App\Libraries\General::check_permission('','eDelete') == true)
			<a href="javascript:;" id="delete" data-id="{{$value->iCustomerId}}" class="btn-icon delete-icon delete_permission me-4">
				<i class="fad fa-trash-alt"></i>
			</a>
			@endif
			@if(\App\Libraries\General::check_permission('LabAdminController','eRead') == 'true')
			@if($value->vTypeName =='LAB')
			<a href="{{route('admin.lab_admin.listing',$value->iCustomerId)}}" class="list-label-btn2 list_permission me-2">Lab Admin
			</a>
			@else
			<a href="{{route('admin.office_admin.listing',$value->iCustomerId)}}" class="list-label-btn2 list_permission me-2">Office Admin
			</a>
			@endif
			@endif
			{{-- @if(\App\Libraries\General::check_permission('OfficeController','eRead') == 'true')
			<a href="{{route('admin.office',$value->iCustomerId)}}" class="list-label-btn2 list_permission me-2">
				Office
			</a>
			@endif --}}
			@if(\App\Libraries\General::check_permission('DoctorController','eRead') == 'true')
			@if($value->vTypeName =='Office')
			<a href="{{route('admin.doctor',$value->iCustomerId)}}" class="list-label-btn2 list_permission me-2">
				Doctor
			</a>
			@endif
			@endif
			@if(\App\Libraries\General::check_permission('UserController','eRead') == 'true')
			<a href="{{route('admin.user',$value->iCustomerId)}}" class="list-label-btn2 list_permission me-2">
				User
			</a>
			@endif

		</div>
	</td>

</tr>
@endforeach
<tr>
	<td colspan="1" class="text-center border-right-0">
		@if(\App\Libraries\General::check_permission('','eDelete') == true)
		<a href="javascript:;" title="Multiple delete" id="delete_btn" data-id="{{$value->iCustomerId}}" class="btn-icon  delete_permission delete-icon">
			<i class="fad fa-trash-alt"></i>
		</a>
		@endif
	</td>
	<td class="border-0" colspan="5">
		<div class="d-flex">
			{!! $paging !!}
		</div>
	</td>
	{{-- <td colspan="5" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
	<td colspan="2" class="text-end border-left-0">
		{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}} --}}
		<select class="w-100px show-drop" id="page_limit">
			<option @if ((isset(Request()->limit_page) && Request()->limit_page == 10) || (isset($limit) && $limit ==10))
				selected
				@endif value="10">10</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 20) || (isset($limit) && $limit ==20))
				selected
				@endif value="20">20</option>
				<option  @if ((isset(Request()->limit_page) && Request()->limit_page == 50) || (isset($limit) && $limit ==50) )
					selected 
				@endif value="50">50</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 100) || (isset($limit) && $limit ==100))
					selected
				@endif value="100">100</option>
		</select>
	</td>
</tr>
@else
<tr class="text-center">
	<td colspan="15">No Record Found</td>
</tr>
@endif