@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
<div class="page-title text-center">
        <h3>
        Manage Office Profile
        </h3> 
    </div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
      <form action="{{route('admin.OfficeProfileAction')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($data)) {{$data->iCustomerId}} @endif">
        <div class="col-xl-12 col-lg-12 col-md-12">
          <label>Office</label>
          <input type="text" class="form-control" id="OfficeName" name="OfficeName" value="{{$data->iCustomerId}}" readonly>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Profile Picture</label>
          <input class="form-control" type="file" id="vImage" name="vImage" accept="image/*">
          @if(isset($data))
             <img style="width: 100px;" id="img" value="@if(old('vImage') == 'vImage') selected @endif" src="{{asset('uploads/customer/'.$data->vImage)}}">
          @else 
             <img style="width: 100px;" id="img" value="" src="{{asset('images/no-image.gif')}}">
          @endif
         <div class="text-danger" style="display: none;" id="vImage_error">select logo</div>
        </div>
        
       
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Office name</label>
          <input type="text" class="form-control" id="vOfficeName" name="vOfficeName" placeholder="Office name" value="@if(old('vOfficeName')!=''){{old('vOfficeName')}}@elseif(isset($data->vOfficeName)){{$data->vOfficeName}}@else{{old('vOfficeName')}}@endif">
          <div class="text-danger" style="display: none;" id="vOfficeName_error">Please enter lab/office name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Office code</label>
          <input type="text" class="form-control" id="vOfficeCode" name="vOfficeCode" placeholder="Office code" value="@if(old('vOfficeCode')!=''){{old('vOfficeCode')}}@elseif(isset($data->vOfficeCode)){{$data->vOfficeCode}}@else{{old('vOfficeCode')}}@endif">
          <div class="text-danger" style="display: none;" id="vOfficeCode_error">Please enter office code</div>
          <div class="text-danger" style="display: none;" id="vOfficeCodeMax_error">Please enter valid length of code</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Address</label>
          <input type="text" class="form-control" id="vAddress" name="vAddress" placeholder="Address" value="@if(old('vAddress')!=''){{old('vAddress')}}@elseif(isset($data->vAddress)){{$data->vAddress}}@else{{old('vAddress')}}@endif">
          <div class="text-danger" style="display: none;" id="vAddress_error">Please enter </div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>City</label>
          <input type="text" class="form-control" id="vCity" name="vCity" placeholder="City" value="@if(old('vCity')!=''){{old('vCity')}}@elseif(isset($data->vCity)){{$data->vCity}}@else{{old('vCity')}}@endif">
          <div class="text-danger" style="display: none;" id="vCity_error">Please enter city</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>State</label>
            <select name="iStateId" id="iStateId" >
              @foreach($states as $key => $states)
              <option value="{{$states->iStateId}}" @if(isset($data)){{$states->iStateId == $data->iStateId  ? 'selected' : ''}} @endif>{{$states->vState}}</option>
              @endforeach
            </select>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Zip code</label>
          <input type="number" class="form-control" id="vZipCode" name="vZipCode" placeholder="zipcode" value="@if(old('vZipCode')!=''){{old('vZipCode')}}@elseif(isset($data->vZipCode)){{$data->vZipCode}}@else{{old('vZipCode')}}@endif">
          <div class="text-danger" style="display: none;" id="vZipCode_error">Please enter zipcode</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Contact name</label>
          <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($data->vName)){{$data->vName}}@else{{old('vName')}}@endif">
          <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Title</label>
          <input type="text" class="form-control" id="vTitle" name="vTitle" placeholder="Title" value="@if(old('vTitle')!=''){{old('vTitle')}}@elseif(isset($data->vTitle)){{$data->vTitle}}@else{{old('vTitle')}}@endif">
          <div class="text-danger" style="display: none;" id="vTitle_error">Please enter title</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Email</label>
          <input type="text" class="form-control" id="vEmail" name="vEmail" placeholder="Email" value="@if(old('vEmail')!=''){{old('vEmail')}}@elseif(isset($data->vEmail)){{$data->vEmail}}@else{{old('vEmail')}}@endif">
          <div class="text-danger" style="display: none;" id="vEmail_error">Please enter email</div>
          <div class="text-danger" style="display: none;" id="vEmail_valid_error">Please enter valid email</div>
        </div>
        
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Website name</label>
          <input type="text" class="form-control" id="vWebsiteName" name="vWebsiteName" placeholder="website name" value="@if(old('vWebsiteName')!=''){{old('vWebsiteName')}}@elseif(isset($data->vWebsiteName)){{$data->vWebsiteName}}@else{{old('vWebsiteName')}}@endif">
          <div class="text-danger" style="display: none;" id="vWebsiteName_error">Please enter website name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Mobile</label>
          <input type="text" class="form-control" id="vMobile" name="vMobile" placeholder="Mobile" value="@if(old('vMobile')!=''){{old('vMobile')}}@elseif(isset($data->vMobile)){{$data->vMobile}}@else{{old('vMobile')}}@endif">
          <div class="text-danger" style="display: none;" id="vMobile_error">Please enter mobile</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Cell</label>
          <input type="text" class="form-control" id="vCellulor" name="vCellulor" placeholder="Cell" value="@if(old('vCellulor')!=''){{old('vCellulor')}}@elseif(isset($data->vCellulor)){{$data->vCellulor}}@else{{old('vCellulor')}}@endif">
          <div class="text-danger" style="display: none;" id="vCellulor_error">Please enter cell</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Notification Email</label>
          <input type="text" class="form-control" id="vNotificationEmail" name="vNotificationEmail" placeholder="Notification Email" value="@if(old('vNotificationEmail')!=''){{old('vNotificationEmail')}}@elseif(isset($data->vNotificationEmail)){{$data->vNotificationEmail}}@else{{old('vNotificationEmail')}}@endif">
          <div class="text-danger" style="display: none;" id="vNotificationEmail_error">Please enter cell</div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6">
          <label>Note</label>
          <div id="toolbar-container"></div>
            <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Note">@if(old('tDescription')!=''){{old('tDescription')}}@elseif(isset($data->tDescription)){{$data->tDescription}}@else{{old('tDescription')}}@endif</textarea>
          <div id="tDescription_error" class="text-danger" style="display: none;">Please enter note </div>
        </div>

        {{-- <div class="col-lg-6">
          <h5 class="pt-3">Labs</h5>
          <div class="listing-page">
            <div class="table-data table-responsive">
              <table class="table">
                <thead>
                  <tr>
                      <th>
                          <strong>Lab Name</strong>
                      </th>
                      <th>
                          <strong> Connected </strong>
                      </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($Labs as $key => $lab_value)
                    <tr>
                        <td>{{$lab_value->vOfficeName}}</td>
                        <td>
                          @if(in_array($lab_value->iCustomerId,$LabOffice_labids))
                            @foreach($LabOffice as $key => $LabOffice_value)
                              @if($LabOffice_value->iLabId == $lab_value->iCustomerId && $LabOffice_value->eStatus == "Active")
                                Connection Accepted
                              @elseif($LabOffice_value->iLabId == $lab_value->iCustomerId && $LabOffice_value->eStatus == "Pending")
                                Connection Pending
                              @elseif($LabOffice_value->iLabId == $lab_value->iCustomerId && $LabOffice_value->eStatus == "Inactive")
                                Connection Rejected
                              @endif
                            @endforeach
                          @else
                            <input id="new_lab_{{$lab_value->iCustomerId}}" type="checkbox" name="NewLab[]" value="{{$lab_value->iCustomerId}}">
                            <label for="new_lab_{{$lab_value->iCustomerId}}">Send Connection Request</label>
                          @endif
                        </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div> --}}
        
        <div class="col-12 align-self-end d-inline-block">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.customer')}}" class="btn back-btn">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
   <script>
 $('#eStatus').selectize();
 $('#iCustomerTypeId').selectize();
 $('#iStateId').selectize();
  // $('#eStatus').select2({
  //       width: '100%',
  //       placeholder: "Select customer",
  //       allowClear: true
  //   });
  // $('#iCustomerTypeId').select2({
  //       width: '100%',
  //       placeholder: "Select customer",
  //       allowClear: true
  //   });
  $(document).on('change', '#vImage', function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#img').attr('src', e.target.result);
      };
      reader.readAsDataURL(this.files[0]);
    }
  });
  $(document).on('click', '#submit', function() {
    id          = $("#id").val();
    vOfficeName = $("#vOfficeName").val();
    vOfficeCode = $("#vOfficeCode").val();
    vImage      = $("#vImage").val();
    vAddress    = $("#vAddress").val();
    vCity       = $("#vCity").val();
    vZipCode    = $("#vZipCode").val();
    vTitle      = $("#vTitle").val();
    vWebsiteName= $("#vWebsiteName").val();
    vName       = $("#vName").val();
    vEmail      = $("#vEmail").val();
    vMobile     = $("#vMobile").val();
    vCellulor   = $("#vCellulor").val();
    tDescription= $("#tDescription").val();
    
    var error = false;

    if (vOfficeName.length == 0) {
      $("#vOfficeName_error").show();
      error = true;
    } else {
      $("#vOfficeName_error").hide();
    }
    if(vOfficeCode.length == 0){
        $("#vOfficeCode_error").show();
        $("#vOfficeCodeMax_error").hide();
        error = true;
      } else{
        if(vOfficeCode.length > 5){
          $("#vOfficeCodeMax_error").show();
          $("#vOfficeCode_error").hide();
          error = true;
        }else{
          $("#vOfficeCodeMax_error").hide();
          $("#vOfficeCode_error").hide();
        }
      }
    if (vAddress.length == 0) {
      $("#vAddress_error").show();
      error = true;
    } else {
      $("#vAddress_error").hide();
    }
    if (vCity.length == 0) {
      $("#vCity_error").show();
      error = true;
    } else {
      $("#vCity_error").hide();
    }
    if (vZipCode.length == 0) {
      $("#vZipCode_error").show();
      error = true;
    } else {
      $("#vZipCode_error").hide();
    }
    if (vTitle.length == 0) {
      $("#vTitle_error").show();
      error = true;
    } else {
      $("#vTitle_error").hide();
    }
    
    if (vName.length == 0) {
      $("#vName_error").show();
      error = true;
    } else {
      $("#vName_error").hide();
    }
    if (vEmail.length == 0) {
      $("#vEmail_error").show();
      $("#vEmail_valid_error").hide();
        error = true;
    }else{
      if(validateEmail(vEmail))
      {
        $("#vEmail_valid_error").hide();

      }else{
        $("#vEmail_valid_error").show();
        $("#vEmail_error").hide();
        error = true;
      }
    }
 
    
    
    
    

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
function validateEmail(vEmail) 
{
  var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  if (filter.test(vEmail)) {
    return true;
  }
  else {
    return false;
  }
}
$(document).ready(function() {
    var mobile = [{ "mask": "### ### ####"}, { "mask": "### ### ####"}];
    $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
   })
  $(document).ready(function() {
  var cellulor = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-####"}];
  $('#vCellulor').inputmask({ 
      mask: cellulor, 
      greedy: false, 
      definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
  })
</script>
@endsection