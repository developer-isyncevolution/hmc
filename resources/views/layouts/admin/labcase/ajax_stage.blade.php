@if (!empty($Stage))
    @if (count($Stage) > 1)    
        <option style="color:rgb(23, 21, 116)" value="">Select new stage</option>
    @endif
    @foreach($Stage as $key => $Stage_val)
        <option value="{{$Stage_val->iProductStageId}}"> {{$Stage_val->vName}}</option>
    @endforeach
@else
    <option value="">No Stage Found</option>
@endif
