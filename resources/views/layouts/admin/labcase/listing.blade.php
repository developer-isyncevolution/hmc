@extends('layouts.admin.index')
@section('content')
<!-- BEGIN: Content-->
<!-- DOM - jQuery events table -->
@php
    $eAddSlipAccess = \App\Libraries\General::check_permission_slip('eAddSlip');
    $eVirualSlipAccess = \App\Libraries\General::check_permission_slip('eVirualSlip');
    $ePaperSlipAccess = \App\Libraries\General::check_permission_slip('ePaperSlip');
   
@endphp
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">
<style>
    .rush-a::before {
        background:url("{{asset('admin/assets/images/rush-bg.png')}}");
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center;
    }

    .error-icon i {
        font-size: 18px;
        color: orange;
    }


    .tooltip-inner {
        background-color: orange;
        color: #fff;
    }


    .tooltip-arrow::before {
        border-right-color: orange !important;
        border-left-color: orange !important;
    }


    .swal-button--danger {
        color: #fff;
        font-size: 14px;
        min-width: 140px;
        border: 1px solid #2a78cf !important;
        background-image: linear-gradient(to right, black, #0e66b2 400px, black 800px);
        background-size: 800px 100%;
        background-position: 50% 100%;
        background-repeat: no-repeat;
        border-radius: 5px;
        padding: 5px !important;
    }

    .swal-button--cancel {
        border-radius: 5px;
        padding: 5px !important;
        font-size: 14px;
        min-width: 140px;
    }

    .swal-footer {
        text-align: center;
    }
</style>

<div class="main-panel" style="padding: 20px; margin: 0px;">
    <div class="page-title text-center">
        <h3>
            Slips
        </h3>
    </div>

    <div class="col-lg-12 mx-auto">
        <div class="card mb-5 mb-xl-4">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">

                        <!-- Modal -->
                        <div class="modal fade" id="add_to_slip">
                            <div class="modal-dialog modal-xl" style="width: 95%;max-width: 95%;">
                                <div class="modal-content add-Case-model">
                                    <div class="modal-header p-2">
                                        <h5 class="modal-title" id="add-to-slipLabel"> Add Slip</h5>
                                        <button type="button" class="btn-close" id="main_slip_close"></button>
                                    </div>
                                    <div class="modal-body p-0 m-0">
                                        <form action="{{route('admin.labcase.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="vCaseNumber" id="vCaseNumber" value="{{$case_id}}">
                                            <input type="hidden" name="iCreatedById" id="iCreatedById" value="{{$AdminId}}">
                                            <input type="hidden" name="eCreatedByType" id="eCreatedByType" value="Admin">
                                            <input type="hidden" name="vSlipNumber" id="vSlipNumber" value="{{$slip_id}}">

                                            <input type="hidden" name="validation_error" id="validation_error" value="">

                                            <div class="main-panel add-case-listing" style="padding: 10px; margin: 0px;">
                                                <div class="header-flex">
                                                    <div class="left-header" style="gap: 30px;">
                                                        <div class="com">
                                                            <ul class="add-case-heder-ul">
                                                                <li>
                                                                    <select name="iLabId" id="iLabId" class="w-100">
                                                                        <option value="">Select Lab</option>
                                                                        @foreach($labs as $key => $lab_value)
                                                                        <option value="{{$lab_value->iCustomerId}}"> {{$lab_value->vOfficeName}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <div style="display: none;" id="iLabId_error" class="text-danger">Please select Lab</div>
                                                                </li>
                                                                <li>
                                                                    <div id="doctor_div" class="w-100" style="display: none;">
                                                                        <select name="iDoctorId" id="iDoctorId" class="w-100">
                                                                            <option value="">Select Doctor</option>
                                                                        </select>
                                                                        <div id="iDoctorId_error" style="display: none;" class="text-danger">Please select Doctor</div>
                                                                    </div>
                                                                </li>
                                                                <li>

                                                                    <div style="display: none;" id="vPatientName_div" class="w-100 h-100">
                                                                        <input type="text" class="form-control h-100" id="vPatientName" name="vPatientName" placeholder="Type patient name">
                                                                        <div id="vPatientName_error" style="display: none;" class="text-danger">Please enter patient name</div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        <div class="com">
                                                            <ul class="add-case-heder-ul w-auto">
                                                                <li>
                                                                    <span class="li-title"> <strong>Pan #</strong> </span>
                                                                    <span class="pan-value" id="case_pan_number">----</span>
                                                                </li>
                                                                <li>
                                                                    <span class="li-title"> <strong>Case #</strong> </span>
                                                                    {{$case_id}}
                                                                </li>
                                                                <li>
                                                                    <span class="li-title"><strong>Slip #</strong></span>{{$slip_id}}
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        <div class="com">
                                                            <ul class="add-case-heder-ul borded-li">
                                                                <li>
                                                                    <span class="li-title"> <strong> Created by</strong></span>
                                                                    <input type="text" class="form-control h-100 border-0 w-50" placeholder="{{$user_name}}" name="vCreatedByName" id="vCreatedByName" value="{{$user_name}}" readonly>
                                                                </li>
                                                                <div id="vCreatedByName_error" style="display: none;" class="text-danger">Enter name</div>

                                                                <li>
                                                                    <span>Location </span>
                                                                    <select name="vLocation" id="vLocation" readonly>
                                                                        <option value="Draft">Draft</option>
                                                                        <option value="In office ready to pickup">In office ready to pickup</option>
                                                                        <option value="On route to the lab">On route to the lab</option>
                                                                        <option value="In lab">In Lab</option>
                                                                        <option value="In lab ready to pickup">In lab ready to pickup</option>
                                                                        <option value="On route to the office">On route to the office</option>
                                                                        <option value="In office">In office</option>
                                                                       
                                                                    </select>
                                                                </li>
                                                                <li>
                                                                    <span>Case Status</span>

                                                                    <select name="eStatus" id="eStatus" readonly>
                                                                        <option value="Draft">Draft</option>
                                                                        <option value="On Process">On Process</option>
                                                                        <option value="Canceled">Canceled</option>
                                                                        <option value="Finished">Finished</option>
                                                                        <option value="On Hold">On Hold</option>
                                                                    </select>
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        <!-- <div class="com">
                                                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="mb-3">
                                                                            <label>Brand Name</label>
                                                                            <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="">
                                                                        </div>
                                                                        <div class="text-danger" id="vName_error">Enter Name</div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                                        <a type="submit" id="brand_submit" class="btn submit-btn me-2">Add</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> -->

                                                        <div class="com">
                                                            <ul class="add-case-heder-ul">
                                                                <li>
                                                                    <span class="li-title"> <strong>Pick up Date</strong> </span> <span id="final_pic_date" name="final_pic_date">----</span> <a href="javascript:;" id="pickup_date" name="pickup_date" class="form-control datepicker border-0" autocomplete="on"> <i class="fad fa-calendar-alt"></i></a>

                                                                </li>
                                                                <li>
                                                                    <span class="li-title"> <strong>Delivery Date </strong> </span> <span id="final_del_days" name="final_del_days"> ----</span> <a href="#" class="rush-a" id="rush_model_open"> Rush </a>
                                                                </li>
                                                                <li>
                                                                    <span class="li-title"> <strong>Delivery Time </strong> </span> <span id="final_del_time" name="final_del_time">----</span><a href="#"> <i class="fad fa-alarm-clock"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        <div class="com">
                                                            <ul class="add-case-heder-ul" style="visibility: hidden">
                                                                <li>
                                                                    <a href="#" class="p-icon">
                                                                        <i class="fad fa-print"></i>
                                                                    </a>
                                                                    <a href="#" class="bill-icon">
                                                                        <i class="fad fa-ballot-check"></i>
                                                                    </a>
                                                                </li>
                                                                <li></li>
                                                                <li>
                                                                    <a href="#" class="msg-icon">
                                                                        <i class="fad fa-comment-alt-edit"></i>
                                                                    </a>
                                                                    <a href="#" class="pic-icon">
                                                                        <i class="fad fa-image"></i>
                                                                    </a>
                                                                </li>

                                                            </ul>
                                                        </div>

                                                    </div>




                                                </div>


                                                <hr>

                                                <input type="hidden" name="iTampId" id="iTampId" value="{{$tampid}}">
                                                <input type="hidden" name="information_checkbox" id="information_checkbox" value="">

                                                <input type="hidden" name="upper_casepan_category" id="upper_casepan_category" value="No">
                                                <input type="hidden" name="lower_casepan_category" id="lower_casepan_category" value="No">



                                                <!-- ========================UPPER HIDDEN=========================================== -->
                                                <input type="hidden" name="upper_delivery_date" id="upper_delivery_date" value="">
                                                <input type="hidden" name="upper_pickup_date" id="upper_pickup_date" value="">
                                                <input type="hidden" name="upper_delivery_time" id="upper_delivery_time" value="">

                                                <!-- Stage Main Upper-->
                                                <input type="hidden" name="upper_extraction_stage" id="upper_extraction_stage" value="No">
                                                <input type="hidden" name="upper_Impression_stage" id="upper_Impression_stage" value="No">
                                                <input type="hidden" name="upper_teethshade_stage" id="upper_teethshade_stage" value="No">
                                                <input type="hidden" name="upper_gumshade_stage" id="upper_gumshade_stage" value="No">
                                                <!-- Stage Main Upper-->

                                                <!-- Extraction Internal Upper -->
                                                <input type="hidden" name="upper_eTeethInMouth" id="upper_eTeethInMouth" value="">
                                                <input type="hidden" name="upper_eTeethInDefault" id="upper_eTeethInDefault" value="">
                                                <input type="hidden" name="upper_eTeethInRequird" id="upper_eTeethInRequird" value="">
                                                <input type="hidden" name="upper_eMessingDefault" id="upper_eMessingDefault" value="">
                                                <input type="hidden" name="upper_eMessingRequird" id="upper_eMessingRequird" value="">
                                                <input type="hidden" name="upper_eMessingTeeth" id="upper_eMessingTeeth" value="">
                                                <input type="hidden" name="upper_eExtractDefault" id="upper_eExtractDefault" value="">
                                                <input type="hidden" name="upper_eExtractDelivery" id="upper_eExtractDelivery" value="">
                                                <input type="hidden" name="upper_eExtractRequird" id="upper_eExtractRequird" value="">
                                                <input type="hidden" name="upper_eExtracted" id="upper_eExtracted" value="">
                                                <input type="hidden" name="upper_eExtractedDefault" id="upper_eExtractedDefault" value="">
                                                <input type="hidden" name="upper_eExtractedRequird" id="upper_eExtractedRequird" value="">
                                                <input type="hidden" name="upper_eFixorAdd" id="upper_eFixorAdd" value="">
                                                <input type="hidden" name="upper_eFixorAddDefault" id="upper_eFixorAddDefault" value="">
                                                <input type="hidden" name="upper_eFixorAddRequird" id="upper_eFixorAddRequird" value="">
                                                <input type="hidden" name="upper_eClasps" id="upper_eClasps" value="">
                                                <input type="hidden" name="upper_eClaspsDefault" id="upper_eClaspsDefault" value="">
                                                <input type="hidden" name="upper_eClaspsRequird" id="upper_eClaspsRequird" value="">

                                                <input type="hidden" name="upper_eTeethInOptional" id="upper_eTeethInOptional" value="No">
                                                <input type="hidden" name="upper_eMessingOptional" id="upper_eMessingOptional" value="No">
                                                <input type="hidden" name="upper_eExtractOptional" id="upper_eExtractOptional" value="No">
                                                <input type="hidden" name="upper_eExtractedOptional" id="upper_eExtractedOptional" value="No">
                                                <input type="hidden" name="upper_eFixorAddOptional" id="upper_eFixorAddOptional" value="No">
                                                <input type="hidden" name="upper_eClaspsOptional" id="upper_eClaspsOptional" value="No">


                                                <input type="hidden" name="upper_eOpposingTeethInMouth" id="upper_eOpposingTeethInMouth" value="No">
                                                <input type="hidden" name="upper_eOpposingTeethInDefault" id="upper_eOpposingTeethInDefault" value="No">
                                                <input type="hidden" name="upper_eOpposingMessingTeeth" id="upper_eOpposingMessingTeeth" value="No">
                                                <input type="hidden" name="upper_eOpposingMessingDefault" id="upper_eOpposingMessingDefault" value="No">
                                                <input type="hidden" name="upper_eOpposingExtractDelivery" id="upper_eOpposingExtractDelivery" value="No">
                                                <input type="hidden" name="upper_eOpposingExtractDefault" id="upper_eOpposingExtractDefault" value="No">
                                                <input type="hidden" name="upper_eOpposingExtracted" id="upper_eOpposingExtracted" value="No">
                                                <input type="hidden" name="upper_eOpposingExtractedDefault" id="upper_eOpposingExtractedDefault" value="No">
                                                <input type="hidden" name="upper_eOpposingFixorAdd" id="upper_eOpposingFixorAdd" value="No">
                                                <input type="hidden" name="upper_eOpposingFixorAddDefault" id="upper_eOpposingFixorAddDefault" value="No">
                                                <input type="hidden" name="upper_eOpposingClasps" id="upper_eOpposingClasps" value="No">
                                                <input type="hidden" name="upper_eOpposingClaspsDefault" id="upper_eOpposingClaspsDefault" value="No">

                                                <input type="hidden" name="upper_eOpposingExtractions" id="upper_eOpposingExtractions" value="No">
                                                <input type="hidden" name="upper_eOpposingImpressions" id="upper_eOpposingImpressions" value="No">

                                                <input type="hidden" name="upper_rush_date_stage" id="upper_rush_date_stage" value="No">
                                                <input type="hidden" name="upper_rush_feesid" id="upper_rush_feesid" value="">


                                                <input type="hidden" name="upper_extraction_error" id="upper_extraction_error" value="true">
                                                <!-- Extraction Internal Upper -->

                                                <!-- ========================UPPER HIDDEN=========================================== -->


                                                <!-- ========================LOWER HIDDEN=========================================== -->

                                                <input type="hidden" name="lower_delivery_date" id="lower_delivery_date" value="">
                                                <input type="hidden" name="lower_pickup_date" id="lower_pickup_date" value="">
                                                <input type="hidden" name="lower_delivery_time" id="lower_delivery_time" value="">

                                                <!-- Stage Main Lower-->
                                                <input type="hidden" name="lower_extraction_stage" id="lower_extraction_stage" value="No">
                                                <input type="hidden" name="lower_Impression_stage" id="lower_Impression_stage" value="No">
                                                <input type="hidden" name="lower_teethshade_stage" id="lower_teethshade_stage" value="No">
                                                <input type="hidden" name="lower_gumshade_stage" id="lower_gumshade_stage" value="No">
                                                <!-- Stage Main Lower-->

                                                <!-- Extraction Internal Lower -->
                                                <input type="hidden" name="lower_eTeethInMouth" id="lower_eTeethInMouth" value="">
                                                <input type="hidden" name="lower_eTeethInDefault" id="lower_eTeethInDefault" value="">
                                                <input type="hidden" name="lower_eTeethInRequird" id="lower_eTeethInRequird" value="">
                                                <input type="hidden" name="lower_eMessingDefault" id="lower_eMessingDefault" value="">
                                                <input type="hidden" name="lower_eMessingRequird" id="lower_eMessingRequird" value="">
                                                <input type="hidden" name="lower_eMessingTeeth" id="lower_eMessingTeeth" value="">
                                                <input type="hidden" name="lower_eExtractDefault" id="lower_eExtractDefault" value="">
                                                <input type="hidden" name="lower_eExtractDelivery" id="lower_eExtractDelivery" value="">
                                                <input type="hidden" name="lower_eExtractRequird" id="lower_eExtractRequird" value="">
                                                <input type="hidden" name="lower_eExtracted" id="lower_eExtracted" value="">
                                                <input type="hidden" name="lower_eExtractedDefault" id="lower_eExtractedDefault" value="">
                                                <input type="hidden" name="lower_eExtractedRequird" id="lower_eExtractedRequird" value="">
                                                <input type="hidden" name="lower_eFixorAdd" id="lower_eFixorAdd" value="">
                                                <input type="hidden" name="lower_eFixorAddDefault" id="lower_eFixorAddDefault" value="">
                                                <input type="hidden" name="lower_eFixorAddRequird" id="lower_eFixorAddRequird" value="">
                                                <input type="hidden" name="lower_eClasps" id="lower_eClasps" value="">
                                                <input type="hidden" name="lower_eClaspsDefault" id="lower_eClaspsDefault" value="">
                                                <input type="hidden" name="lower_eClaspsRequird" id="lower_eClaspsRequird" value="">

                                                <input type="hidden" name="lower_eTeethInOptional" id="lower_eTeethInOptional" value="No">
                                                <input type="hidden" name="lower_eMessingOptional" id="lower_eMessingOptional" value="No">
                                                <input type="hidden" name="lower_eExtractOptional" id="lower_eExtractOptional" value="No">
                                                <input type="hidden" name="lower_eExtractedOptional" id="lower_eExtractedOptional" value="No">
                                                <input type="hidden" name="lower_eFixorAddOptional" id="lower_eFixorAddOptional" value="No">
                                                <input type="hidden" name="lower_eClaspsOptional" id="lower_eClaspsOptional" value="No">

                                                <!-- Opposite -->
                                                <input type="hidden" name="lower_eOpposingTeethInMouth" id="lower_eOpposingTeethInMouth" value="No">
                                                <input type="hidden" name="lower_eOpposingTeethInDefault" id="lower_eOpposingTeethInDefault" value="No">
                                                <input type="hidden" name="lower_eOpposingMessingTeeth" id="lower_eOpposingMessingTeeth" value="No">
                                                <input type="hidden" name="lower_eOpposingMessingDefault" id="lower_eOpposingMessingDefault" value="No">
                                                <input type="hidden" name="lower_eOpposingExtractDelivery" id="lower_eOpposingExtractDelivery" value="No">
                                                <input type="hidden" name="lower_eOpposingExtractDefault" id="lower_eOpposingExtractDefault" value="No">
                                                <input type="hidden" name="lower_eOpposingExtracted" id="lower_eOpposingExtracted" value="No">
                                                <input type="hidden" name="lower_eOpposingExtractedDefault" id="lower_eOpposingExtractedDefault" value="No">
                                                <input type="hidden" name="lower_eOpposingFixorAdd" id="lower_eOpposingFixorAdd" value="No">
                                                <input type="hidden" name="lower_eOpposingFixorAddDefault" id="lower_eOpposingFixorAddDefault" value="No">
                                                <input type="hidden" name="lower_eOpposingClasps" id="lower_eOpposingClasps" value="No">
                                                <input type="hidden" name="lower_eOpposingClaspsDefault" id="lower_eOpposingClaspsDefault" value="No">

                                                <input type="hidden" name="lower_eOpposingExtractions" id="lower_eOpposingExtractions" value="No">
                                                <input type="hidden" name="lower_eOpposingImpressions" id="lower_eOpposingImpressions" value="No">

                                                <input type="hidden" name="lower_rush_date_stage" id="lower_rush_date_stage" value="No">
                                                <input type="hidden" name="lower_rush_feesid" id="lower_rush_feesid" value="">




                                                <!-- Opposite -->

                                                <input type="hidden" name="lower_extraction_error" id="lower_extraction_error" value="true">

                                                <!-- Extraction Internal Lower -->

                                                <!-- ========================LOWER HIDDEN=========================================== -->
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="teeth-wrapper upper" id="upper_extraction" style="display: none;">

                                                            <h3 class="card-title mb-0">
                                                                MAXILLARY
                                                            </h3>

                                                            <svg width="100%" height="100%" viewBox="0 0 1500 822" class="teeth-svg">

                                                                <g>

                                                                    <g>
                                                                        <!-- upper-teeth 6 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-6-teeth-white" data-id="teeths2" data-name="6" x="403" y="123" width="136" height="178" xlink:href="{{asset('admin/assets/images/teeth/up-6.png')}}" class="img-for-teeth teeth_upper upper-6-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-6-teeth-yellow" data-id="teeths2" data-name="6" x="403" y="123" width="136" height="178" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-6.png')}}" class="img-for-teeth teeth_upper upper-6-class-teeth-yellow" ></image>
                                                                            <image id="upper-6-teeth-red" data-id="teeths2" data-name="6" x="403" y="123" width="136" height="178" xlink:href="{{asset('admin/assets/images/red-teeth/up-6.png')}}" class="img-for-teeth teeth_upper upper-6-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-6-teeth-grey" data-id="teeths2" data-name="6" x="403" y="123" width="136" height="178" xlink:href="{{asset('admin/assets/images/grey-teeth/up-6.png')}}" class="img-for-teeth teeth_upper upper-6-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-6-teeth-green" data-id="teeths2" data-name="6" x="403" y="123" width="136" height="178" xlink:href="{{asset('admin/assets/images/green-teeth/up-6.png')}}" class="img-for-teeth teeth_upper upper-6-class-teeth-green" style="display: none"></image>
                                                                            <image id="upper-6-claps" data-id="teeths2" data-name="6" x="400" y="105" xlink:href="{{asset('admin/assets/images/teeth/updc-6.png')}}" class="clapping clap-up-6 teeth_upper upper-claps" style="display: none"></image>

                                                                            <text id="upper-6-num" data-name="6" class="cls-1 teeth_upper" transform="translate(476.76 240.092) scale(1.305)">
                                                                                <tspan x="0">6</tspan>
                                                                            </text>
                                                                        </a>
                                                                    </g>

                                                                    <g>
                                                                        <!-- upper-teeth 5 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-5-teeth-white" data-id="teeths3" data-name="5" x="325" y="202" width="149" height="152" xlink:href="{{asset('admin/assets/images/teeth/up-5.png')}}" class="img-for-teeth teeth_upper upper-5-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-5-teeth-yellow" data-id="teeths3" data-name="5" x="325" y="202" width="149" height="152" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-5.png')}}" class="img-for-teeth teeth_upper upper-5-class-teeth-yellow"></image>
                                                                            <image id="upper-5-teeth-red" data-id="teeths3" data-name="5" x="325" y="202" width="149" height="152" xlink:href="{{asset('admin/assets/images/red-teeth/up-5.png')}}" class="img-for-teeth teeth_upper upper-5-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-5-teeth-grey" data-id="teeths3" data-name="5" x="325" y="202" width="149" height="152" xlink:href="{{asset('admin/assets/images/grey-teeth/up-5.png')}}" class="img-for-teeth teeth_upper upper-5-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-5-teeth-green" data-id="teeths3" data-name="5" x="325" y="202" width="149" height="152" xlink:href="{{asset('admin/assets/images/green-teeth/up-5.png')}}" class="img-for-teeth teeth_upper upper-5-class-teeth-green" style="display: none"></image>


                                                                            <text id="upper-5-num" data-name="5" class="cls-1 teeth_upper" transform="translate(405.696 287.468) scale(1.305)">
                                                                                <tspan x="0">5</tspan>
                                                                            </text>
                                                                            <image id="upper-5-claps" data-id="teeths3" data-name="5" x="315" y="190" xlink:href="{{asset('admin/assets/images/teeth/updc-5.png')}}" class="clapping clap-up-5 teeth_upper upper-claps" style="display: none"></image>
                                                                        </a>
                                                                    </g>
                                                                    <g>
                                                                        <!-- upper-teeth 4 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-4-teeth-white" data-id="teeths4" data-name="4" x="266" y="285" width="153" height="149" xlink:href="{{asset('admin/assets/images/teeth/up-4.png')}}" class="img-for-teeth teeth_upper upper-4-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-4-teeth-yellow" data-id="teeths4" data-name="4" x="266" y="285" width="153" height="149" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-4.png')}}" class="img-for-teeth teeth_upper upper-4-class-teeth-yellow"></image>
                                                                            <image id="upper-4-teeth-red" data-id="teeths4" data-name="4" x="266" y="285" width="153" height="149" xlink:href="{{asset('admin/assets/images/red-teeth/up-4.png')}}" class="img-for-teeth teeth_upper upper-4-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-4-teeth-grey" data-id="teeths4" data-name="4" x="266" y="285" width="153" height="149" xlink:href="{{asset('admin/assets/images/grey-teeth/up-4.png')}}" class="img-for-teeth teeth_upper upper-4-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-4-teeth-green" data-id="teeths4" data-name="4" x="266" y="285" width="153" height="149" xlink:href="{{asset('admin/assets/images/green-teeth/up-4.png')}}" class="img-for-teeth teeth_upper upper-4-class-teeth-green" style="display: none"></image>

                                                                            <text id="upper-4-num" data-name="4" class="cls-1 teeth_upper" transform="translate(346.65 357.771) scale(1.305)">
                                                                                <tspan x="0">4</tspan>
                                                                            </text>
                                                                            <image id="upper-4-claps" data-id="teeths4" data-name="4" x="250" y="280" xlink:href="{{asset('admin/assets/images/teeth/updc-4.png')}}" class="clapping clap-up-4 teeth_upper upper-claps" style="display: none"></image>
                                                                        </a>
                                                                    </g>
                                                                    <g>
                                                                        <!-- upper-teeth 3 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-3-teeth-white" data-id="teeths7" data-name="3" x="176" y="367" width="220" height="199" xlink:href="{{asset('admin/assets/images/teeth/up-3.png')}}" class="img-for-teeth teeth_upper upper-3-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-3-teeth-yellow" data-id="teeths7" data-name="3" x="176" y="367" width="220" height="199" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-3.png')}}" class="img-for-teeth teeth_upper upper-3-class-teeth-yellow"></image>
                                                                            <image id="upper-3-teeth-red" data-id="teeths7" data-name="3" x="176" y="367" width="220" height="199" xlink:href="{{asset('admin/assets/images/red-teeth/up-3.png')}}" class="img-for-teeth teeth_upper upper-3-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-3-teeth-grey" data-id="teeths7" data-name="3" x="176" y="367" width="220" height="199" xlink:href="{{asset('admin/assets/images/grey-teeth/up-3.png')}}" class="img-for-teeth teeth_upper upper-3-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-3-teeth-green" data-id="teeths7" data-name="3" x="176" y="367" width="220" height="199" xlink:href="{{asset('admin/assets/images/green-teeth/up-3.png')}}" class="img-for-teeth teeth_upper upper-3-class-teeth-green" style="display: none"></image>

                                                                            <text id="upper-3-num" data-name="3" class="cls-1 teeth_upper" transform="translate(289.371 467.923) scale(1.305)">
                                                                                <tspan x="0">3</tspan>
                                                                            </text>

                                                                            <image id="upper-3-claps" data-id="teeths7" data-name="3" x="160" y="367" xlink:href="{{asset('admin/assets/images/teeth/updc-3.png')}}" class="clapping clap-up-3 teeth_upper upper-claps" style="display: none"></image>
                                                                        </a>
                                                                    </g>
                                                                    <g>
                                                                        <!-- upper-teeth 2 -->
                                                                        <a href="javascript:;" class="test">
                                                                            <image id="upper-2-teeth-white" data-id="teeths6" data-name="2" x="115" y="500" width="203" height="188" xlink:href="{{asset('admin/assets/images/teeth/up-2.png')}}" class="img-for-teeth teeth_upper  upper-2-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-2-teeth-yellow" data-id="teeths6" data-name="2" x="115" y="500" width="203" height="188" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-2.png')}}" class="img-for-teeth teeth_upper upper-2-class-teeth-yellow"></image>
                                                                            <image id="upper-2-teeth-red" data-id="teeths6" data-name="2" x="115" y="500" width="203" height="188" xlink:href="{{asset('admin/assets/images/red-teeth/up-2.png')}}" class="img-for-teeth teeth_upper upper-2-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-2-teeth-grey" data-id="teeths6" data-name="2" x="115" y="500" width="203" height="188" xlink:href="{{asset('admin/assets/images/grey-teeth/up-2.png')}}" class="img-for-teeth teeth_upper upper-2-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-2-teeth-green" data-id="teeths6" data-name="2" x="115" y="500" width="203" height="188" xlink:href="{{asset('admin/assets/images/green-teeth/up-2.png')}}" class="img-for-teeth teeth_upper upper-2-class-teeth-green" style="display: none"></image>

                                                                            <image id="upper-2-claps" data-id="teeths6" data-name="2" x="100" y="500" xlink:href="{{asset('admin/assets/images/teeth/updc-2.png')}}" class="clapping clap-up-2 teeth_upper upper-claps" style="display: none"></image>

                                                                            <text id="upper-2-num" data-name="2" class="cls-1 teeth_upper" transform="translate(224.87 609.191) scale(1.305)">
                                                                                <tspan x="0">2</tspan>
                                                                            </text>
                                                                        </a>
                                                                    </g>

                                                                    <g>
                                                                        <!-- upper-teeth 1 -->
                                                                        <a href="javascript:;" class="test">

                                                                            <image id="upper-1-teeth-white" data-id="teeths5" data-name="1" x="57" y="640" width="192" height="156" xlink:href="{{asset('admin/assets/images/teeth/up-1.png')}}" class="img-for-teeth teeth_upper upper-1-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-1-teeth-yellow" data-id="teeths5" data-name="1" x="57" y="640" width="192" height="156" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-1.png')}}" class="img-for-teeth teeth_upper upper-1-class-teeth-yellow"></image>
                                                                            <image id="upper-1-teeth-red" data-id="teeths5" data-name="1" x="57" y="640" width="192" height="156" xlink:href="{{asset('admin/assets/images/red-teeth/up-1.png')}}" class="img-for-teeth teeth_upper  upper-1-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-1-teeth-grey" data-id="teeths5" data-name="1" x="57" y="640" width="192" height="156" xlink:href="{{asset('admin/assets/images/grey-teeth/up-1.png')}}" class="img-for-teeth teeth_upper   upper-1-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-1-teeth-green" data-id="teeths5" data-name="1" x="57" y="640" width="192" height="156" xlink:href="{{asset('admin/assets/images/green-teeth/up-1.png')}}" class="img-for-teeth teeth_upper  upper-1-class-teeth-green" style="display: none"></image>


                                                                            <text id="upper-1-num" data-name="1" data-id="teeths5" class="cls-1 teeth_upper" transform="translate(163.9 742.356) scale(1.305)">
                                                                                <tspan x="0">1</tspan>
                                                                            </text>

                                                                            <image id="upper-1-claps" data-id="teeths5" data-name="1" x="40" y="640" xlink:href="{{asset('admin/assets/images/teeth/up-dc-1.png')}}" class="clapping clap-up-1 teeth_upper upper-claps" style="display: none"></image>

                                                                        </a>
                                                                        <!-- upper-teeth 1 end-->
                                                                    </g>



                                                                    <g>
                                                                        <!-- upper-teeth 7 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-7-teeth-white" data-id="teeths1" data-name="7" x="498" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/teeth/up-7.png')}}" class="img-for-teeth teeth_upper upper-7-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-7-teeth-yellow" data-id="teeths1" data-name="7" x="498" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-7.png')}}" class="img-for-teeth teeth_upper upper-7-class-teeth-yellow"></image>
                                                                            <image id="upper-7-teeth-red" data-id="teeths1" data-name="7" x="498" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/red-teeth/up-7.png')}}" class="img-for-teeth teeth_upper upper-7-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-7-teeth-grey" data-id="teeths1" data-name="7" x="498" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/grey-teeth/up-7.png')}}" class="img-for-teeth teeth_upper upper-7-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-7-teeth-green" data-id="teeths1" data-name="7" x="498" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/green-teeth/up-7.png')}}" class="img-for-teeth teeth_upper upper-7-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <image id="upper-7-claps" data-id="teeths1" data-name="7" x="498" y="50" xlink:href="{{asset('admin/assets/images/teeth/updc-7.png')}}" class="clapping clap-up-7 teeth_upper upper-claps" style="display: none"></image>

                                                                        <text id="upper-7-num" data-name="7" class="cls-1 teeth_upper" transform="translate(563.111 198.624) scale(1.305)">
                                                                            <tspan x="0">7</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- upper-teeth 8 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-8-teeth-white" data-id="teeths8" data-name="8" x="607" y="35" xlink:href="{{asset('admin/assets/images/teeth/up-8.png')}}" class="img-for-teeth teeth_upper upper-8-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-8-teeth-yellow" data-id="teeths8" data-name="8" x="607" y="35" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-8.png')}}" class="img-for-teeth teeth_upper upper-8-class-teeth-yellow"></image>
                                                                            <image id="upper-8-teeth-red" data-id="teeths8" data-name="8" x="607" y="35" xlink:href="{{asset('admin/assets/images/red-teeth/up-8.png')}}" class="img-for-teeth teeth_upper upper-8-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-8-teeth-grey" data-id="teeths8" data-name="8" x="607" y="35" xlink:href="{{asset('admin/assets/images/grey-teeth/up-8.png')}}" class="img-for-teeth teeth_upper upper-8-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-8-teeth-green" data-id="teeths8" data-name="8" x="607" y="35" xlink:href="{{asset('admin/assets/images/green-teeth/up-8.png')}}" class="img-for-teeth teeth_upper upper-8-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <image id="upper-8-claps" data-id="teeths8" data-name="8" x="607" y="10" xlink:href="{{asset('admin/assets/images/teeth/updc-8.png')}}" class="clapping clap-up-7 teeth_upper upper-claps" style="display: none"></image>

                                                                        <text id="upper-8-num" data-name="8" class="cls-1 teeth_upper" transform="translate(697.646 179.813) scale(1.305)">
                                                                            <tspan x="0">8</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- upper-teeth 9 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-9-teeth-white" data-id="teeths16" data-name="9" x="755" y="35" width="150" height="210" xlink:href="{{asset('admin/assets/images/teeth/up-9.png')}}" class="img-for-teeth teeth_upper upper-9-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-9-teeth-yellow" data-id="teeths16" data-name="9" x="755" y="35" width="150" height="210" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-9.png')}}" class="img-for-teeth teeth_upper upper-9-class-teeth-yellow"></image>
                                                                            <image id="upper-9-teeth-red" data-id="teeths16" data-name="9" x="755" y="35" width="150" height="210" xlink:href="{{asset('admin/assets/images/red-teeth/up-9.png')}}" class="img-for-teeth teeth_upper upper-9-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-9-teeth-grey" data-id="teeths16" data-name="9" x="755" y="35" width="150" height="210" xlink:href="{{asset('admin/assets/images/grey-teeth/up-9.png')}}" class="img-for-teeth teeth_upper upper-9-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-9-teeth-green" data-id="teeths16" data-name="9" x="755" y="35" width="150" height="210" xlink:href="{{asset('admin/assets/images/green-teeth/up-9.png')}}" class="img-for-teeth teeth_upper upper-9-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <image id="upper-9-claps" data-id="teeths16" data-name="9" x="765" y="10" xlink:href="{{asset('admin/assets/images/teeth/updc-9.png')}}" class="clapping clap-up-9 teeth_upper upper-claps" style="display: none"></image>

                                                                        <text id="upper-9-num" data-name="9" class="cls-1 teeth_upper" transform="translate(821.604 176.548) scale(1.305)">
                                                                            <tspan x="0">9</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- upper-teeth 10 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-10-teeth-white" data-id="teeths9" data-name="10" x="889" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/teeth/up-10.png')}}" class="img-for-teeth teeth_upper upper-10-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-10-teeth-yellow" data-id="teeths9" data-name="10" x="889" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-10.png')}}" class="img-for-teeth teeth_upper upper-10-class-teeth-yellow" ></image>
                                                                            <image id="upper-10-teeth-red" data-id="teeths9" data-name="10" x="889" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/red-teeth/up-10.png')}}" class="img-for-teeth teeth_upper upper-10-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-10-teeth-grey" data-id="teeths9" data-name="10" x="889" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/grey-teeth/up-10.png')}}" class="img-for-teeth teeth_upper upper-10-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-10-teeth-green" data-id="teeths9" data-name="10" x="889" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/green-teeth/up-10.png')}}" class="img-for-teeth teeth_upper upper-10-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <image id="upper-10-claps" data-id="teeths9" data-name="10" x="910" y="50" xlink:href="{{asset('admin/assets/images/teeth/updc-10.png')}}" class="clapping clap-up-10 teeth_upper upper-claps" style="display: none"></image>

                                                                        <text id="upper-10-num" data-name="10" class="cls-1 teeth_upper" transform="translate(934.785 195.164) scale(1.305)">
                                                                            <tspan x="0">10</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- upper-teeth 11 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-11-teeth-white" data-id="teeths10" data-name="11" x="972" y="123" width="137" height="178" xlink:href="{{asset('admin/assets/images/teeth/up-11.png')}}" class="img-for-teeth teeth_upper upper-11-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-11-teeth-yellow" data-id="teeths10" data-name="11" x="972" y="123" width="137" height="178" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-11.png')}}" class="img-for-teeth teeth_upper upper-11-class-teeth-yellow"></image>
                                                                            <image id="upper-11-teeth-red" data-id="teeths10" data-name="11" x="972" y="123" width="137" height="178" xlink:href="{{asset('admin/assets/images/red-teeth/up-11.png')}}" class="img-for-teeth teeth_upper upper-11-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-11-teeth-grey" data-id="teeths10" data-name="11" x="972" y="123" width="137" height="178" xlink:href="{{asset('admin/assets/images/grey-teeth/up-11.png')}}" class="img-for-teeth teeth_upper upper-11-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-11-teeth-green" data-id="teeths10" data-name="11" x="972" y="123" width="137" height="178" xlink:href="{{asset('admin/assets/images/green-teeth/up-11.png')}}" class="img-for-teeth teeth_upper upper-11-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <image id="upper-11-claps" data-id="teeths10" data-name="11" x="1030" y="100" xlink:href="{{asset('admin/assets/images/teeth/updc-11.png')}}" class="clapping clap-up-11 teeth_upper upper-claps" style="display: none"></image>

                                                                        <text id="upper-11-num" data-name="11" class="cls-1 teeth_upper" transform="translate(1031.677 240.099) scale(1.305)">
                                                                            <tspan x="0">11</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- upper-teeth 12 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-12-teeth-white" data-id="teeths11" data-name="12" x="1040" y="198" width="149" height="152" xlink:href="{{asset('admin/assets/images/teeth/up-12.png')}}" class="img-for-teeth teeth_upper upper-12-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-12-teeth-yellow" data-id="teeths11" data-name="12" x="1040" y="198" width="149" height="152" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-12.png')}}" class="img-for-teeth teeth_upper upper-12-class-teeth-yellow"></image>
                                                                            <image id="upper-12-teeth-red" data-id="teeths11" data-name="12" x="1040" y="198" width="149" height="152" xlink:href="{{asset('admin/assets/images/red-teeth/up-12.png')}}" class="img-for-teeth teeth_upper upper-12-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-12-teeth-grey" data-id="teeths11" data-name="12" x="1040" y="198" width="149" height="152" xlink:href="{{asset('admin/assets/images/grey-teeth/up-12.png')}}" class="img-for-teeth teeth_upper upper-12-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-12-teeth-green" data-id="teeths11" data-name="12" x="1040" y="198" width="149" height="152" xlink:href="{{asset('admin/assets/images/green-teeth/up-12.png')}}" class="img-for-teeth teeth_upper upper-12-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <image id="upper-12-claps" data-id="teeths11" data-name="12" x="1115" y="175" xlink:href="{{asset('admin/assets/images/teeth/updc-12.png')}}" class="clapping clap-up-12 teeth_upper upper-claps upper-claps" style="display: none"></image>

                                                                        <text id="upper-12-num" data-name="12" class="cls-1 teeth_upper" transform="translate(1103.168 282.619) scale(1.305)">
                                                                            <tspan x="0">12</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- upper-teeth 13 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-13-teeth-white" data-id="teeths12" data-name="13" x="1095" y="284" width="154" height="149" xlink:href="{{asset('admin/assets/images/teeth/up-13.png')}}" class="img-for-teeth teeth_upper upper-13-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-13-teeth-yellow" data-id="teeths12" data-name="13" x="1095" y="284" width="154" height="149" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-13.png')}}" class="img-for-teeth teeth_upper upper-13-class-teeth-yellow"></image>
                                                                            <image id="upper-13-teeth-red" data-id="teeths12" data-name="13" x="1095" y="284" width="154" height="149" xlink:href="{{asset('admin/assets/images/red-teeth/up-13.png')}}" class="img-for-teeth teeth_upper upper-13-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-13-teeth-grey" data-id="teeths12" data-name="13" x="1095" y="284" width="154" height="149" xlink:href="{{asset('admin/assets/images/grey-teeth/up-13.png')}}" class="img-for-teeth teeth_upper upper-13-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-13-teeth-green" data-id="teeths12" data-name="13" x="1095" y="284" width="154" height="149" xlink:href="{{asset('admin/assets/images/green-teeth/up-13.png')}}" class="img-for-teeth teeth_upper upper-13-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <image id="upper-13-claps" data-id="teeths12" data-name="13" x="1215" y="270" xlink:href="{{asset('admin/assets/images/teeth/updc-13.png')}}" class="clapping clap-up-13 teeth_upper upper-claps" style="display: none"></image>

                                                                        <text id="upper-13-num" data-name="13" class="cls-1 teeth_upper" transform="translate(1169.916 345.118) scale(1.305)">
                                                                            <tspan x="0">13</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- upper-teeth 14 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-14-teeth-white" data-id="teeths15" data-name="14" x="1115" y="361" width="221" height="199" xlink:href="{{asset('admin/assets/images/teeth/up-14.png')}}" class="img-for-teeth teeth_upper upper-14-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-14-teeth-yellow" data-id="teeths15" data-name="14" x="1115" y="361" width="221" height="199" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-14.png')}}" class="img-for-teeth teeth_upper upper-14-class-teeth-yellow"></image>
                                                                            <image id="upper-14-teeth-red" data-id="teeths15" data-name="14" x="1115" y="361" width="221" height="199" xlink:href="{{asset('admin/assets/images/red-teeth/up-14.png')}}" class="img-for-teeth teeth_upper upper-14-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-14-teeth-grey" data-id="teeths15" data-name="14" x="1115" y="361" width="221" height="199" xlink:href="{{asset('admin/assets/images/grey-teeth/up-14.png')}}" class="img-for-teeth teeth_upper upper-14-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-14-teeth-green" data-id="teeths15" data-name="14" x="1115" y="361" width="221" height="199" xlink:href="{{asset('admin/assets/images/green-teeth/up-14.png')}}" class="img-for-teeth teeth_upper upper-14-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <image id="upper-14-claps" data-id="teeths15" data-name="14" x="1280" y="361" xlink:href="{{asset('admin/assets/images/teeth/updc-14.png')}}" class="clapping clap-up-14 teeth_upper upper-claps" style="display: none"></image>

                                                                        <text id="upper-14-num" data-name="14" class="cls-1 teeth_upper" transform="translate(1210.87 477.036) scale(1.305)">
                                                                            <tspan x="0">14</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- upper-teeth 15 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-15-teeth-white" data-id="teeths14" data-name="15" x="1194" y="503" width="203" height="188" xlink:href="{{asset('admin/assets/images/teeth/up-15.png')}}" class="img-for-teeth teeth_upper upper-15-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-15-teeth-yellow" data-id="teeths14" data-name="15" x="1194" y="503" width="203" height="188" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-15.png')}}" class="img-for-teeth teeth_upper upper-15-class-teeth-yellow"></image>
                                                                            <image id="upper-15-teeth-red" data-id="teeths14" data-name="15" x="1194" y="503" width="203" height="188" xlink:href="{{asset('admin/assets/images/red-teeth/up-15.png')}}" class="img-for-teeth teeth_upper upper-15-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-15-teeth-grey" data-id="teeths14" data-name="15" x="1194" y="503" width="203" height="188" xlink:href="{{asset('admin/assets/images/grey-teeth/up-15.png')}}" class="img-for-teeth teeth_upper upper-15-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-15-teeth-green" data-id="teeths14" data-name="15" x="1194" y="503" width="203" height="188" xlink:href="{{asset('admin/assets/images/green-teeth/up-15.png')}}" class="img-for-teeth teeth_upper upper-15-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <image id="upper-15-claps" data-id="teeths14" data-name="15" x="1350" y="503" xlink:href="{{asset('admin/assets/images/teeth/updc-15.png')}}" class="clapping clap-up-15 teeth_upper upper-claps" style="display: none"></image>

                                                                        <text id="upper-15-nuum" data-name="15" class="cls-1 teeth_upper" transform="translate(1303.838 603.41) scale(1.305)">
                                                                            <tspan x="0">15</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- upper-teeth 16 -->
                                                                        <a href="javascript:;">
                                                                            <image id="upper-16-teeth-white" data-id="teeths13" data-name="16" x="1262" y="643" width="192" height="156" xlink:href="{{asset('admin/assets/images/teeth/up-16.png')}}" class="img-for-teeth teeth_upper upper-16-class-teeth-white" style="display: none"></image>
                                                                            <image id="upper-16-teeth-yellow" data-id="teeths13" data-name="16" x="1262" y="643" width="192" height="156" xlink:href="{{asset('admin/assets/images/yellow-teeth/up-16.png')}}" class="img-for-teeth teeth_upper upper-16-class-teeth-yellow"></image>
                                                                            <image id="upper-16-teeth-red" data-id="teeths13" data-name="16" x="1262" y="643" width="192" height="156" xlink:href="{{asset('admin/assets/images/red-teeth/up-16.png')}}" class="img-for-teeth teeth_upper upper-16-class-teeth-red" style="display: none"></image>
                                                                            <image id="upper-16-teeth-grey" data-id="teeths13" data-name="16" x="1262" y="643" width="192" height="156" xlink:href="{{asset('admin/assets/images/grey-teeth/up-16.png')}}" class="img-for-teeth teeth_upper upper-16-class-teeth-grey" style="display: none"></image>
                                                                            <image id="upper-16-teeth-green" data-id="teeths13" data-name="16" x="1262" y="643" width="192" height="156" xlink:href="{{asset('admin/assets/images/green-teeth/up-16.png')}}" class="img-for-teeth teeth_upper upper-16-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <image id="upper-16-claps" data-id="teeths13" data-name="16" x="1422" y="643" xlink:href="{{asset('admin/assets/images/teeth/updc-16.png')}}" class="clapping clap-up-16 teeth_upper upper-claps" style="display: none"></image>

                                                                        <text id="upper-16-num" data-name="16" class="cls-1 teeth_upper" transform="translate(1370.808 743.76) scale(1.305)">
                                                                            <tspan x="0">16</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <!--upper teeth numbers  -->
                                                                </g>
                                                            </svg>

                                                            <div class="d-block text-center teeth-text">
                                                                <a href="javascript:;" class="add-btn p-2 mx-auto upper_mising_all" id="upper_mising_all">
                                                                    Missing all teeth
                                                                </a>
                                                            </div>
                                                            <div class="teeths-wrapper second-row p-0">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-xl-12 col-lg-12">
                                                                        <div class="row gy-3">
                                                                            <div class="col-md-12" id="upper_in_mouth_div" style="display: none;">
                                                                                <div class="input-group">
                                                                                    <label class="form-control d-flex align-items-center justify-content-center inner-label te-in-month lbl_btn_all upper_lable_click" value="" id="upper_in_mouth">

                                                                                        <div class="lable-left">
                                                                                            Teeth in mouth <br> <span id="upper_in_mouth_lbl"></span>
                                                                                        </div>

                                                                                    </label>
                                                                                    <div class="error-tooltip" id="upper_teeth_in_mouth_error" style="display:none;">
                                                                                        <span class="error-icon">
                                                                                            Please select one or more teeth in mouth
                                                                                            <img alt="img" src="{{asset('admin/assets/images/toast-img.png')}}" />
                                                                                        </span>
                                                                                    </div>


                                                                                    <input type="hidden" id="upper_in_mouth_value" name="upper_in_mouth_value" value="">

                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-12" id="upper_missing_teeth_div" style="display: none;">
                                                                                <div class="input-group">
                                                                                    <label class="form-control d-flex align-items-center justify-content-center inner-label missing-te lbl_btn_all upper_lable_click" value="" id="upper_missing_teeth">
                                                                                        <div class="lable-left">
                                                                                            missing teeth <br> <span id="upper_missing_teeth_lbl"></span>
                                                                                        </div>

                                                                                    </label>
                                                                                    <div class="error-tooltip" id="upper_missing_teeth_error" style="display:none;">
                                                                                        <span class="error-icon">
                                                                                            Please select one or more missing teeth
                                                                                            <img alt="toat img" src="{{asset('admin/assets/images/toast-img.png')}}" />
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="hidden" id="upper_missing_teeth_value" name="upper_missing_teeth_value" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12" id="upper_ectract_delivery_div" style="display: none;">
                                                                                <div class="input-group">
                                                                                    <label class="form-control d-flex align-items-center justify-content-center inner-label ex-dekivery lbl_btn_all upper_lable_click" value="" id="upper_ectract_delivery">
                                                                                        <div class="lable-left">
                                                                                            will extract on delivery <br><span id="upper_ectract_delivery_lbl"></span>
                                                                                        </div>

                                                                                    </label>
                                                                                    <div class="error-tooltip" id="upper_will_extract_on_delivery_error" style="display:none;">
                                                                                        <span class="error-icon">
                                                                                            Please select one or more will extract on delivery
                                                                                            <img alt="toat img" src="{{asset('admin/assets/images/toast-img.png')}}" />
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="hidden" id="upper_ectract_delivery_value" name="upper_ectract_delivery_value" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12" id="upper_been_extracted_div" style="display: none;">
                                                                                <div class="input-group">
                                                                                    <label class="form-control d-flex align-items-center justify-content-center inner-label ben-extracted lbl_btn_all upper_lable_click" value="" id="upper_been_extracted">
                                                                                        <div class="lable-left">

                                                                                            has been extracted <br><span id="upper_been_extracted_lbl"></span>
                                                                                        </div>

                                                                                    </label>
                                                                                    <div class="error-tooltip" id="upper_has_been_extracted_error" style="display:none;">
                                                                                        <span class="error-icon">
                                                                                            Please select one or more has been extracted
                                                                                            <img alt="toat img" src="{{asset('admin/assets/images/toast-img.png')}}" />
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="hidden" id="upper_been_extracted_value" name="upper_been_extracted_value" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6" id="upper_fix_div" style="display: none;">
                                                                                <div class="input-group">
                                                                                    <label class="form-control d-flex align-items-center justify-content-center inner-label fix-add lbl_btn_all upper_lable_click" value="" id="upper_fix">
                                                                                        <div class="">

                                                                                            fix or add <br> <span id="upper_fix_lbl"></span>
                                                                                        </div>

                                                                                    </label>
                                                                                    <div class="error-tooltip" id="upper_fix_or_add_error" style="display:none; height:45px;">
                                                                                        <span class="error-icon">
                                                                                            Please select one or more fix or add
                                                                                            <img alt="toat img" src="{{asset('admin/assets/images/toast-img.png')}}" />
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="hidden" id="upper_fix_value" name="upper_fix_value" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6" id="upper_clasps_div" style="display: none;">
                                                                                <div class="input-group">
                                                                                    <label class="form-control d-flex align-items-center justify-content-center inner-label clasps lbl_btn_all upper_lable_click" value="" id="upper_clasps">
                                                                                        <div class="">
                                                                                            clasps<br> <span id="upper_clasps_lbl"></span>
                                                                                        </div>
                                                                                    </label>

                                                                                    <div class="error-tooltip" id="upper_clasps_error" style="display:none;height:45px;">
                                                                                        <span class="error-icon">
                                                                                            Please select one or more clasps
                                                                                            <img alt="toat img" src="{{asset('admin/assets/images/toast-img.png')}}" />
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="hidden" id="upper_clasps_value" name="upper_clasps_value" value="">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <h3 class="card-title mb-0">
                                                                    CASE DESIGN
                                                                </h3>
                                                            </div>

                                                            <div class="col-lg-12 mb-3" id="product_button_div" style="display: none;">
                                                                <div class="row">
                                                                    <div class="col-lg-5 text-center">
                                                                        <div class="row">
                                                                            <div class="col-lg-3">
                                                                                <a href="javascript:;" id="upper_delete" class="remove-btn-slip me-auto">
                                                                                    <i class="fal fa-trash-alt"></i>
                                                                                </a>
                                                                            </div>
                                                                            <div class="col-lg-9 text-start">
                                                                                <a href="javascript:;" class="btn add-btn me-2" id="product_upper_button_div"> Add Upper Product </a>
                                                                            </div>
                                                                        </div>



                                                                    </div>
                                                                    <div class="col-lg-2"></div>
                                                                    <div class="col-lg-5 text-center">
                                                                        <div class="row">
                                                                            <div class="col-lg-9 text-end">
                                                                                <a href="javascript:;" class="btn add-btn me-2" id="product_lower_button_div"> Add Lower Product </a>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <a href="javascript:;" id="lower_delete" class="remove-btn-slip ms-auto me-5">
                                                                                    <i class="fal fa-trash-alt"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row" id="category_div" style="display: none;">
                                                                <div class="col-lg-5">
                                                                    <div id="category_upper_div" style="display: none;">
                                                                        <select name="iUpperCategoryId" id="iUpperCategoryId">
                                                                            @if (!empty($Upper_category))
                                                                            @if (count($Upper_category) > 1)
                                                                            <option value="">Select Category</option>
                                                                            @endif
                                                                            @foreach($Upper_category as $key => $value)
                                                                            <option value="{{$value->iCategoryId}}"> {{$value->vName}}</option>
                                                                            @endforeach
                                                                            @else
                                                                            <option value="">No Category Found</option>
                                                                            @endif
                                                                        </select>
                                                                        <div id="iUpperCategoryId_error" style="display: none;" class="text-danger">Please select Category</div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 align-self-center text-center">
                                                                    <label id="cat_lbl" style="display: none;"> <strong> Category</strong> </label>
                                                                </div>

                                                                <div class="col-lg-5">
                                                                    <div id="category_lower_div" style="display: none;">
                                                                        <select name="iLowerCategoryId" id="iLowerCategoryId">
                                                                            @if (!empty($Lower_category))
                                                                            @if (count($Lower_category) > 1)
                                                                            <option value="">Select Category</option>
                                                                            @endif
                                                                            @foreach($Lower_category as $key => $value)
                                                                            <option value="{{$value->iCategoryId}}"> {{$value->vName}}</option>
                                                                            @endforeach
                                                                            @else
                                                                            <option value="">No Category Found</option>
                                                                            @endif
                                                                        </select>
                                                                        <div id="iUpperCategoryId_error" style="display: none;" class="text-danger">Please select Category</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="product_div" style="display: none;">
                                                                <div class="col-lg-5">
                                                                    <div class="w-100">
                                                                        <div id="product_upper_div" style="display: none;">
                                                                            <select name="iUpperProductId" id="iUpperProductId" class="w-100">
                                                                                <option value="">Select Product</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 align-self-center text-center">
                                                                    <label id="product_lbl" style="display: none;"> <strong>Product</strong> </label>
                                                                </div>
                                                                <div class="col-lg-5">
                                                                    <div id="product_lower_div" style="display: none;">
                                                                        <select name="iLowerProductId" id="iLowerProductId" class="w-100">
                                                                            <option value="">Select Product</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- Grade START -->
                                                            <div class="row" id="grade_div">
                                                                <div class="col-lg-5">
                                                                    <div class="w-100">
                                                                        <div id="grade_upper_div" style="display: none;">
                                                                            <select name="iUpperGradeId" id="iUpperGradeId" class="w-100">
                                                                                <option value="">Select Grade</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 align-self-center text-center">
                                                                    <label id="grade_lbl" style="display: none;"> <strong> Grade</strong> </label>
                                                                </div>
                                                                <div class="col-lg-5">
                                                                    <div id="grade_lower_div" style="display: none;">
                                                                        <select name="iLowerGradeId" id="iLowerGradeId" class="w-100">
                                                                            <option value="">Select Grade</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Grade END -->

                                                            <div class="row" id="stage_div" style="display: none;">
                                                                <div class="col-lg-5">
                                                                    <div class="w-100">
                                                                        <div id="stage_upper_div" class="w-100" style="display: none;">
                                                                            <select name="iUpperStageId" id="iUpperStageId" class="w-100">
                                                                                <option value="">Select Stage</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 align-self-center text-center">
                                                                    <label id="stage_lbl" style="display: none;"> <strong> Stage</strong> </label>
                                                                </div>
                                                                <div class="col-lg-5">
                                                                    <div id="stage_lower_div" style="display: none;">
                                                                        <select name="iLowerStageId" id="iLowerStageId" class="w-100">
                                                                            <option value="">Select Stage</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <div class="row" id="teeth_shades_div" style="display: none;">
                                                                <div class="col-lg-5">

                                                                    <div class="w-100">
                                                                        <div id="upper_tsbrand_div" style="display: none;" class="w-100">
                                                                            <div class="row">
                                                                                <div class="col-lg-6 pe-0">
                                                                                    <select name="upper_tsbrand" id="upper_tsbrand">
                                                                                        <option value="">Select brand</option>
                                                                                        @foreach($toothbrand as $key => $value)
                                                                                        <option value="{{$value->iToothBrandId}}"> {{$value->vName}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-lg-6 ps-0" style="display:none;" id="upper_ts_select_div">
                                                                                    <select name="upper_tshade" id="upper_tshade">
                                                                                        <option value="">Select shade</option>
                                                                                    </select>
                                                                                </div>
                                                                                <!-- <div class="col-lg-1 align-self-center">
                                                                                    <a href="#" class="btn-icon edit_permission edit-icon ">
                                                                                        <i class="fad fa-pencil"></i>
                                                                                    </a>
                                                                                </div> -->
                                                                                <!-- <div class="col-lg-1 align-self-center">
                                                                                    <a href="#" class="btn-icon  delete_permission delete-icon">
                                                                                        <i class="fad fa-trash-alt"></i>
                                                                                    </a>
                                                                                </div> -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 align-self-center text-center">
                                                                    <label id="teethshade_lbl" style="display: none;"> <strong>Teeth shade </strong></label>
                                                                </div>
                                                                <div class="col-lg-5">
                                                                    <div id="lower_tsbrand_div" style="display: none;" class="w-100">
                                                                        <div class="row">
                                                                            <!-- <div class="col-lg-1 align-self-center">
                                                                                <a href="#" class="btn-icon edit_permission edit-icon ">
                                                                                    <i class="fad fa-pencil"></i>
                                                                                </a>
                                                                            </div> -->
                                                                            <!-- <div class="col-lg-1 align-self-center">
                                                                                <a href="#" class="btn-icon  delete_permission delete-icon">
                                                                                    <i class="fad fa-trash-alt"></i>
                                                                                </a>
                                                                            </div> -->
                                                                            <div class="col-lg-6 pe-0">
                                                                                <select name="lower_tsbrand" id="lower_tsbrand">
                                                                                    <option value="">Select brand</option>
                                                                                    @foreach($toothbrand as $key => $value)
                                                                                    <option value="{{$value->iToothBrandId}}"> {{$value->vName}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-lg-6 ps-0" style="display:none;" id="lower_ts_select_div">
                                                                                <select name="lower_tshade" id="lower_tshade">
                                                                                    <option value="">Select shade</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="gum_shades_div" style="display: none;">
                                                                <div class="col-lg-5">
                                                                    <div class="w-100">
                                                                        <div id="upper_gsbrand_div" style="display: none;">
                                                                            <div class="row">
                                                                                <div class="col-lg-6 pe-0">
                                                                                    <select name="upper_gsbrand" id="upper_gsbrand">
                                                                                        <option value="">Select brand</option>
                                                                                        @foreach($gumbrand as $key => $value)
                                                                                        <option value="{{$value->iGumBrandId}}"> {{$value->vName}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-lg-6 ps-0" style="display:none;" id="upper_gs_select_div">
                                                                                    <select name="upper_gshade" id="upper_gshade">
                                                                                        <option value="">Select shade</option>
                                                                                    </select>
                                                                                </div>
                                                                                <!-- <div class="col-lg-1 align-self-center">
                                                                                    <a href="#" class="btn-icon edit_permission edit-icon ">
                                                                                        <i class="fad fa-pencil"></i>
                                                                                    </a>
                                                                                </div> -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 align-self-center text-center">
                                                                    <label id="gumshade_lbl" style="display: none;"> <strong> Gum shade</strong> </label>
                                                                </div>
                                                                <div class="col-lg-5 align-self-center">
                                                                    <div id="lower_gsbrand_div" style="display: none;">
                                                                        <div class="row">
                                                                            <!-- <div class="col-lg-1 align-self-center">
                                                                                <a href="#" class="btn-icon edit_permission edit-icon ">
                                                                                    <i class="fad fa-pencil"></i>
                                                                                </a>
                                                                            </div> -->
                                                                            <div class="col-lg-6 pe-0">
                                                                                <select name="lower_gsbrand" id="lower_gsbrand">
                                                                                    <option value="">Select brand</option>
                                                                                    @foreach($gumbrand as $key => $value)
                                                                                    <option value="{{$value->iGumBrandId}}"> {{$value->vName}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-lg-6 ps-0" style="display:none;" id="lower_gs_select_div">
                                                                                <select name="lower_gshade" id="lower_gshade">
                                                                                    <option value="">Select shade</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row" id="impression_div" style="display:none;">
                                                                <div class="col-lg-5">
                                                                    <div class="impressions-dropdown" id="upper_impression_div" style="display:none;">
                                                                        <input type="text" placeholder="Select Impressions" id="upper_impressions" class="list-input" readonly="">
                                                                        <a class="close-btn" href="#"> <i class="fas fa-times"></i></a>
                                                                        <div id="upper_impression_dropdown" class="impressions-dropdown">
                                                                            <div class="low_list dropdown-list" id="upper_impressions_ajax_div">
                                                                                

                                                                            </div>
                                                                        </div>
                                                                        <span id="upper_impressions_warning" style="display:none;" class="impression-error"> <strong> No impression will be sent on this appointment</strong></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 align-self-center text-center">
                                                                    <label id="impression_lbl" style="display: none;"> <strong> Impressions</strong> </label>
                                                                </div>
                                                                <div class="col-lg-5">
                                                                    <div class="impressions-dropdown" id="lower_impression_div" style="display: none;">
                                                                        <input type="text" placeholder="Select Impressions" id="lower_impressions" class="list-input" readonly="" value="">
                                                                        <a class="close-btn" href="#"> <i class="fas fa-times"></i></a>
                                                                        <div id="lower_impression_dropdown" class="impressions-dropdown">
                                                                            <div class="low_list dropdown-list" id="lower_impressions_ajax_div">
                                                                                

                                                                            </div>
                                                                        </div>
                                                                        <span id="lower_impressions_warning" style="display:none;" class="impression-error"> <strong> No impression will be sent on this appointment</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- Add Ons -->
                                                            <div class="row mb-3 mt-2" id="category_addons_div" style="display: none;">
                                                                <div class="col-lg-5 text-center">
                                                                    <div id="upper_category_addons_div" class="mb-1">

                                                                    </div>
                                                                    <div class="add-one-upper-container" id="upper_addons_model_div" style="display:none">
                                                                        <a href="javascript:;" id="upper_addons" value="upper_addons" class="btn add-btn me-2 w-100">Select Add Ons</a>

                                                                        <div class="select-add-one-upper-side d-none" id="upper_model">

                                                                            <div class="c-modal-header">
                                                                                <h6 class="m-0">Upper category addons</h6>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-lg-5 p-0">
                                                                                    <div class="mb-3">
                                                                                        <select id="upper_category_addons" name="upper_category_addons">
                                                                                            @if(isset($Upper_category_addons) && !empty($Upper_category_addons))
                                                                                            <option value="">Select category addons</option>
                                                                                            @foreach($Upper_category_addons as $key => $value)
                                                                                            <option data-name="{{$value->vCategoryName}}" value="{{$value->iAddCategoryId}}"> {{$value->vCategoryName}}</option>
                                                                                            @endforeach
                                                                                            @endif
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-5 p-0">
                                                                                    <div class="mb-3">
                                                                                        <select id="upper_subaddons" name="upper_subaddons">
                                                                                            <option value="">Select Addons</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-2 p-0">
                                                                                    <div class="mb-3">
                                                                                        <input type="number" placeholder="qty" id="iUpperQty" value="" name="iUpperQty" class="w-100 qty-input">
                                                                                    </div>
                                                                                </div>
                                                                                <input type="hidden" id="upperAddOnType" value="Add">
                                                                                <input type="hidden" id="upper_addons_edit" value="">

                                                                            </div>
                                                                            <div class="text-danger" style="display: none;" id="upper_category_addons_error_div">select catagory</div>
                                                                            <div class="text-danger" style="display: none;" id="upper_subaddons_error_div">select sub addon</div>
                                                                            <div class="text-danger" style="display: none;" id="upper_upperqty_error_div">enter qty</div>
                                                                            <div class="d-flex justify-content-between">
                                                                                <a type="submit" id="upper_submit" class="submit-btn me-2 btn add-btn w-100">Add</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 text-center align-self-center">
                                                                    <label> <strong>Add ons</strong></label>
                                                                </div>
                                                                <div class="col-lg-5 text-center">
                                                                    <div id="lower_category_addons_div" class="mb-1"></div>
                                                                    <div class="add-one-upper-container" id="lower_category_addons_model_div" style="display:none">
                                                                        <a href="javascript:;" id="lower_addons" value="lower_addons" class="btn add-btn me-2 w-100">Select Add Ons</a>

                                                                        <div class="select-add-one-upper-side d-none" id="lower_model">

                                                                            <div class="c-modal-header">
                                                                                <h6 class="m-0">Lower category addons</h6>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-lg-5 p-0">
                                                                                    <div class="mb-3">
                                                                                        <select name="lower_category_addons" id="lower_category_addons">
                                                                                            @if(isset($Lower_category_addons) && !empty($Lower_category_addons ))
                                                                                            <option value="">Select category addons</option>
                                                                                            @foreach($Lower_category_addons as $key => $value)
                                                                                            <option value="{{$value->iAddCategoryId}}"> {{$value->vCategoryName}}</option>
                                                                                            @endforeach
                                                                                            @endif
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-5 p-0">

                                                                                    <div class="mb-3">
                                                                                        <select name="lower_subaddons" id="lower_subaddons">
                                                                                            <option value="">Select addons</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-2 p-0">
                                                                                    <div class="mb-3">
                                                                                        <input type="number" placeholder="qty" id="iLowerQty" value="" class="w-100 qty-input">
                                                                                    </div>
                                                                                </div>
                                                                                <input type="hidden" id="lowerAddOnType" value="Add">
                                                                                <input type="hidden" id="lower_addons_edit" value="">

                                                                            </div>
                                                                            <div class="text-danger" style="display: none;" id="lower_category_addons_error_div">select catagory</div>
                                                                            <div class="text-danger" style="display: none;" id="lower_subaddons_error_div">select sub addon</div>
                                                                            <div class="text-danger" style="display: none;" id="lower_lowerqty_error_div">enter qty</div>
                                                                            <div class="d-flex justify-content-between">
                                                                                <a type="submit" id="lower_submit" class="btn submit-btn me-2 add-btn w-100">Add</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Add Ons END -->
                                                            <div class="row" id="stage_status_div" style="display: none;">
                                                                <div class="col-lg-5">
                                                                    <div class="w-100" id="upper_status_div" style="display: none">
                                                                        <select name="upper_status" id="upper_status" class="w-100">
                                                                            <option value="Draft">Draft</option>
                                                                            <option value="On Process">On Process</option>
                                                                            <option value="Canceled">Canceled</option>
                                                                            <option value="Finished">Finished</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 align-self-center text-center">
                                                                    <label id="status_lbl" style="display: none;"> <strong> stage status </strong> </label>
                                                                </div>
                                                                <div class="col-lg-5">
                                                                    <div id="lower_status_div" style="display: none;" class="w-100">
                                                                        <select name="lower_status" id="lower_status">
                                                                            <option value="Draft">Draft</option>
                                                                            <option value="On Process">On Process</option>
                                                                            <option value="Canceled">Canceled</option>
                                                                            <option value="Finished">Finished</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row mb-1 mt-2" id="delivery_div" style="display: none;">
                                                                <div class="col-lg-5 text-center">
                                                                    <div class="row" id="upper_delivery_div" style="display: none;">
                                                                        <div class="col-lg-6" id="upper_delivery_lbl" style="display: none;">
                                                                            <span>
                                                                                {{-- <strong >Delivery Date</strong> --}}
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <span id="upper_div_date_lable" name="upper_div_date_lable"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 text-center">
                                                                    <label style="display: none;" id="delivery_lbl"> <strong>Delivery Date</strong></label>
                                                                </div>
                                                                <div class="col-lg-5 text-center">
                                                                    <div class="row" id="lower_delivery_div" style="display: none;">
                                                                        <div class="col-lg-6">
                                                                            <span id="lower_div_date_lable" name="lower_div_date_lable"></span>
                                                                        </div>
                                                                        <div class="col-lg-6" id="lower_delivery_lbl" style="display: none;">
                                                                            <span>
                                                                                {{-- <strong >Delivery Date</strong> --}}
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal fade" id="rush_model">
                                                                <div class="modal-dialog modal-xl modal-dialog-centered" style="max-width: 1000px;align-items: flex-start;">
                                                                    <div class="modal-content" style="background: #f1f1f1">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Please select date desired to rush case.</h5>
                                                                            <button type="button" class="btn-close btn_close_rush" id="rush_btn_close"></button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="col-lg-6">
                                                                                        <div class="row">
                                                                                            <div class="col-lg-2">
                                                                                                <a href="javascript:;" id="upper_rush_delete" class="remove-btn-slip mx-auto me-5">
                                                                                                    <i class="fal fa-trash-alt"></i>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="col-lg-10 text-start">
                                                                                                <h3 class="card-title mb-0">
                                                                                                    Upper
                                                                                                </h3>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-lg-6">
                                                                                        <div class="row">
                                                                                            <div class="col-lg-10 text-end">
                                                                                                <h3 class="card-title mb-0">
                                                                                                    Lower
                                                                                                </h3>

                                                                                            </div>
                                                                                            <div class="col-lg-2">
                                                                                                <a href="javascript:;" id="lower_rush_delete" class="remove-btn-slip mx-auto ms-3">
                                                                                                    <i class="fal fa-trash-alt"></i>
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="col-lg-5" id="upper_rush_selection_html">

                                                                                    </div>
                                                                                    <div class="col-lg-5 offset-2" id="lower_rush_selection_html">

                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <div class="row w-100">
                                                                                <div class="col-lg-2 col-md-12 align-self-end d-inline-block order-xl-1 order-2">
                                                                                    <a href="javascript:;" class="btn back-btn mx-2 cancel_rush w-100 cancel-btn">Close</a>
                                                                                </div>
                                                                                <div class="col-lg-10 col-md-12 order-xl-2 order-1">
                                                                                    <div class="w-100 justify-content-end">
                                                                                        <div class="d-flex justify-content-lg-end align-items-center flex-wrap">
                                                                                            <div class="add-case-bottom flex-nowrap" id="rush_warning">
                                                                                                <img alt="toat img" class="toast-img" src="{{asset('admin/assets/images/toast-img.png')}}" />
                                                                                                <label>
                                                                                                    By clicking this box you acknowledge case is being rushed and accept rush fees
                                                                                                </label>
                                                                                                <input class="ms-5 c-checkbox" type="checkbox" value="" name="rush_alert" id="rush_alert">
                                                                                                <a href="javascript:;" id="save_rush" class="btn add-btn ms-2"> Save Rush </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="teeth-wrapper lower" id="lower_extraction" style="display: none;">
                                                            <h3 class="card-title mb-0">
                                                                MANDIBULAR
                                                            </h3>
                                                            <svg width="100%" height="100%" viewBox="0 0 1550 862" class="teeth-svg">

                                                                <g>
                                                                    <g>
                                                                        <!-- lower-teeth 7 -->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-23-teeth-white" data-id="teeth2" data-name="23" x="540" y="60" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/d-23.png')}}" class="img-for-teeth teeth_lower lower-23-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-23-teeth-yellow" data-id="teeth2" data-name="23" x="540" y="60" width="115" height="186" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-23.png')}}" class="img-for-teeth teeth_lower lower-23-class-teeth-yellow"></image>
                                                                            <image id="lower-23-teeth-red" data-id="teeth2" data-name="23" x="540" y="60" width="115" height="186" xlink:href="{{asset('admin/assets/images/red-teeth/d-23.png')}}" class="img-for-teeth teeth_lower lower-23-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-23-teeth-grey" data-id="teeth2" data-name="23" x="540" y="60" width="115" height="186" xlink:href="{{asset('admin/assets/images/grey-teeth/d-23.png')}}" class="img-for-teeth teeth_lower lower-23-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-23-teeth-green" data-id="teeth2" data-name="23" x="540" y="60" width="115" height="186" xlink:href="{{asset('admin/assets/images/green-teeth/d-23.png')}}" class="img-for-teeth teeth_lower lower-23-class-teeth-green" style="display: none"></image>
                                                                        </a>
                                                                        <!-- image with cliap-23 -->
                                                                        <image id="lower-23-claps" data-name="23" x="525" y="-33" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/dc-23.png')}}" class="clapping clap-23 teeth_lower lower-claps" style="display: none"></image>
                                                                        <text id="lower-23-num" data-name="23" class="cls-1 teeth_lower" transform="translate(602.467 207.332) scale(1.199)">
                                                                            <tspan x="0" y="-50">23</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- lower-teeth 6 -->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-22-teeth-white" data-id="teeth4" data-name="22" x="435" y="93" width="135" height="200" xlink:href="{{asset('admin/assets/images/teeth/d-22.png')}}" class="img-for-teeth teeth_lower lower-22-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-22-teeth-yellow" data-id="teeth4" data-name="22" x="435" y="93" width="135" height="200" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-22.png')}}" class="img-for-teeth teeth_lower lower-22-class-teeth-yellow"></image>
                                                                            <image id="lower-22-teeth-red" data-id="teeth4" data-name="22" x="435" y="93" width="135" height="200" xlink:href="{{asset('admin/assets/images/red-teeth/d-22.png')}}" class="img-for-teeth teeth_lower lower-22-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-22-teeth-grey" data-id="teeth4" data-name="22" x="435" y="93" width="135" height="200" xlink:href="{{asset('admin/assets/images/grey-teeth/d-22.png')}}" class="img-for-teeth teeth_lower lower-22-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-22-teeth-green" data-id="teeth4" data-name="22" x="435" y="93" width="135" height="200" xlink:href="{{asset('admin/assets/images/green-teeth/d-22.png')}}" class="img-for-teeth teeth_lower lower-22-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <!--  image with cliap-22 -->
                                                                        <image id="lower-22-claps" data-name="22" x="430" y="15" width="100" height="200" xlink:href="{{asset('admin/assets/images/teeth/dc-22.png')}}" class="clapping clap-22 teeth_lower lower-claps" style="display: none"></image>
                                                                        <text id="lower-22-num" data-name="22" class="cls-1 teeth_lower" transform="translate(517.12 257.438) scale(1.199)">
                                                                            <tspan x="0" y="-50">22</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- lower-teeth 5 -->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-21-teeth-white" data-id="teeth3" data-name="21" x="342" y="153" width="150" height="177" xlink:href="{{asset('admin/assets/images/teeth/d-21.png')}}" class="img-for-teeth teeth_lower lower-21-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-21-teeth-yellow" data-id="teeth3" data-name="21" x="342" y="153" width="150" height="177" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-21.png')}}" class="img-for-teeth teeth_lower lower-21-class-teeth-yellow"></image>
                                                                            <image id="lower-21-teeth-red" data-id="teeth3" data-name="21" x="342" y="153" width="150" height="177" xlink:href="{{asset('admin/assets/images/red-teeth/d-21.png')}}" class="img-for-teeth teeth_lower lower-21-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-21-teeth-grey" data-id="teeth3" data-name="21" x="342" y="153" width="150" height="177" xlink:href="{{asset('admin/assets/images/grey-teeth/d-21.png')}}" class="img-for-teeth teeth_lower lower-21-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-21-teeth-green" data-id="teeth3" data-name="21" x="342" y="153" width="150" height="177" xlink:href="{{asset('admin/assets/images/green-teeth/d-21.png')}}" class="img-for-teeth teeth_lower lower-21-class-teeth-green" style="display: none"></image>
                                                                        </a>
                                                                        <!--  image with cliap-5 -->
                                                                        <image id="lower-21-claps" data-name="21" x="300" y="135" width="140" height="90" xlink:href="{{asset('admin/assets/images/teeth/dc-21.png')}}" class="clapping clap-21 teeth_lower lower-claps" style="display: none"></image>
                                                                        <text id="lower-21-num" data-name="21" class="cls-1 teeth_lower" transform="translate(455.319 283.973) scale(1.199)">
                                                                            <tspan x="-30" y="-50">21</tspan>
                                                                        </text>
                                                                    </g>
                                                                    <g>
                                                                        <!-- lower-teeth 4 start-->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-20-teeth-white" data-id="teeth5" data-name="20" x="265" y="225" width="167" height="159" xlink:href="{{asset('admin/assets/images/teeth/d-20.png')}}" class="img-for-teeth teeth_lower lower-20-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-20-teeth-yellow" data-id="teeth5" data-name="20" x="265" y="225" width="167" height="159" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-20.png')}}" class="img-for-teeth teeth_lower lower-20-class-teeth-yellow"></image>
                                                                            <image id="lower-20-teeth-red" data-id="teeth5" data-name="20" x="265" y="225" width="167" height="159" xlink:href="{{asset('admin/assets/images/red-teeth/d-20.png')}}" class="img-for-teeth teeth_lower lower-20-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-20-teeth-grey" data-id="teeth5" data-name="20" x="265" y="225" width="167" height="159" xlink:href="{{asset('admin/assets/images/grey-teeth/d-20.png')}}" class="img-for-teeth teeth_lower lower-20-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-20-teeth-green" data-id="teeth5" data-name="20" x="265" y="225" width="167" height="159" xlink:href="{{asset('admin/assets/images/green-teeth/d-20.png')}}" class="img-for-teeth teeth_lower lower-20-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <text id="lower-20-num" data-name="20" class="cls-1 teeth_lower" transform="translate(401.061 332.076) scale(1.199)">
                                                                            <tspan x="-30" y="-35">20</tspan>
                                                                        </text>

                                                                        <!--  image with cliap-4 -->
                                                                        <image id="lower-20-claps" data-name="20" x="240" y="175" width="100" height="159" xlink:href="{{asset('admin/assets/images/teeth/dc-20.png')}}" class="clapping clap-20 teeth_lower lower-claps" style="display: none"></image>
                                                                        <!-- lower-teeth 4 end-->
                                                                    </g>
                                                                    <g>
                                                                        <!-- lower-teeth 3 -->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-19-teeth-white" data-id="teeth6" data-name="19" x="165" y="290" width="235" height="191" xlink:href="{{asset('admin/assets/images/teeth/d-19.png')}}" class="img-for-teeth teeth_lower lower-19-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-19-teeth-yellow" data-id="teeth6" data-name="19" x="165" y="290" width="235" height="191" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-19.png')}}" class="img-for-teeth teeth_lower lower-19-class-teeth-yellow"></image>
                                                                            <image id="lower-19-teeth-red" data-id="teeth6" data-name="19" x="165" y="290" width="235" height="191" xlink:href="{{asset('admin/assets/images/red-teeth/d-19.png')}}" class="img-for-teeth teeth_lower lower-19-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-19-teeth-grey" data-id="teeth6" data-name="19" x="165" y="290" width="235" height="191" xlink:href="{{asset('admin/assets/images/grey-teeth/d-19.png')}}" class="img-for-teeth teeth_lower lower-19-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-19-teeth-green" data-id="teeth6" data-name="19" x="165" y="290" width="235" height="191" xlink:href="{{asset('admin/assets/images/green-teeth/d-19.png')}}" class="img-for-teeth teeth_lower lower-19-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <text id="lower-19-num" data-name="19" class="cls-1 teeth_lower" transform="translate(329.927 426.297) scale(1.199)">
                                                                            <tspan x="-35" y="-35">19</tspan>
                                                                        </text>

                                                                        <!--  image with cliap-3 -->
                                                                        <image id="lower-19-claps" data-name="19" x="155" y="255" width="100" height="191" xlink:href="{{asset('admin/assets/images/teeth/dc-19.png')}}" class="clapping clap-19 teeth_lower lower-claps" style="display: none"></image>

                                                                        <!-- lower-teeth 3 end-->
                                                                    </g>
                                                                    <!-- lower-teeth 1 end-->

                                                                    <g>
                                                                        <!-- lower-teeth 2 -->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-18-teeth-white" data-id="teeth7" data-name="18" x="95" y="400" width="232" height="194" xlink:href="{{asset('admin/assets/images/teeth/d-18.png')}}" class="img-for-teeth teeth_lower lower-18-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-18-teeth-yellow" data-id="teeth7" data-name="18" x="95" y="400" width="232" height="194" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-18.png')}}" class="img-for-teeth teeth_lower lower-18-class-teeth-yellow"></image>
                                                                            <image id="lower-18-teeth-red" data-id="teeth7" data-name="18" x="95" y="400" width="232" height="194" xlink:href="{{asset('admin/assets/images/red-teeth/d-18.png')}}" class="img-for-teeth teeth_lower lower-18-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-18-teeth-grey" data-id="teeth7" data-name="18" x="95" y="400" width="232" height="194" xlink:href="{{asset('admin/assets/images/grey-teeth/d-18.png')}}" class="img-for-teeth teeth_lower lower-18-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-18-teeth-green" data-id="teeth7" data-name="18" x="95" y="400" width="232" height="194" xlink:href="{{asset('admin/assets/images/green-teeth/d-18.png')}}" class="img-for-teeth teeth_lower lower-18-class-teeth-green" style="display: none"></image>
                                                                        </a>
                                                                        <!--  image with cliap-2 -->
                                                                        <image id="lower-18-claps" data-name="18" x="80" y="382" width="85" height="194" xlink:href="{{asset('admin/assets/images/teeth/dc-18.png')}}" class="clapping clap-18 teeth_lower lower-claps" style="display: none"></image>

                                                                        <text id="lower-18-num" data-name="18" class="cls-1 teeth_lower" transform="translate(263.156 536.61) scale(1.199)">
                                                                            <tspan x="-35" y="-30">18</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <!-- lower-teeth 2 end-->
                                                                    <g>
                                                                        <!-- lower-teeth 1 -->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-17-teeth-white" data-id="teeth15" data-name="17" x="30" y="555" width="252" height="200" xlink:href="{{asset('admin/assets/images/teeth/d-17.png')}}" class="img-for-teeth  teeth_lower lower-17-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-17-teeth-yellow" data-id="teeth15" data-name="17" x="30" y="555" width="252" height="200" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-17.png')}}" class="img-for-teeth  teeth_lower lower-17-class-teeth-yellow"></image>
                                                                            <image id="lower-17-teeth-red" data-id="teeth15" data-name="17" x="30" y="555" width="252" height="200" xlink:href="{{asset('admin/assets/images/red-teeth/d-17.png')}}" class="img-for-teeth  teeth_lower lower-17-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-17-teeth-grey" data-id="teeth15" data-name="17" x="30" y="555" width="252" height="200" xlink:href="{{asset('admin/assets/images/grey-teeth/d-17.png')}}" class="img-for-teeth  teeth_lower lower-17-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-17-teeth-green" data-id="teeth15" data-name="17" x="30" y="555" width="252" height="200" xlink:href="{{asset('admin/assets/images/green-teeth/d-17.png')}}" class="img-for-teeth  teeth_lower lower-17-class-teeth-green" style="display: none"></image>
                                                                        </a>

                                                                        <!--  image with cliap-1 -->
                                                                        <image id="lower-17-claps" data-name="17" x="15" y="540" width="80" height="190" xlink:href="{{asset('admin/assets/images/teeth/dc-17.png')}}" class="clapping clap-17 teeth_lower lower-claps" style="display: none"></image>

                                                                        <text id="lower-17-num" data-name="17" class="cls-1 teeth_lower" transform="translate(196.632 625.98) scale(1.199)">
                                                                            <tspan x="-30">17</tspan>
                                                                        </text>
                                                                    </g>

                                                                    


                                                                    <g>
                                                                        <!-- lower-teeth 8 -->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-24-teeth-white" data-id="teeth1" data-name="24" x="647" y="40" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/d-24.png')}}" class="img-for-teeth teeth_lower lower-24-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-24-teeth-yellow" data-id="teeth1" data-name="24" x="647" y="40" width="111" height="182" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-24.png')}}" class="img-for-teeth teeth_lower lower-24-class-teeth-yellow"></image>
                                                                            <image id="lower-24-teeth-red" data-id="teeth1" data-name="24" x="647" y="40" width="111" height="182" xlink:href="{{asset('admin/assets/images/red-teeth/d-24.png')}}" class="img-for-teeth teeth_lower lower-24-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-24-teeth-grey" data-id="teeth1" data-name="24" x="647" y="40" width="111" height="182" xlink:href="{{asset('admin/assets/images/grey-teeth/d-24.png')}}" class="img-for-teeth teeth_lower lower-24-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-24-teeth-green" data-id="teeth1" data-name="24" x="647" y="40" width="111" height="182" xlink:href="{{asset('admin/assets/images/green-teeth/d-24.png')}}" class="img-for-teeth teeth_lower lower-24-class-teeth-green" style="display: none"></image>
                                                                        </a>
                                                                        <!-- image with cliap-8 -->
                                                                        <image id="lower-24-claps" data-name="24" x="647" y="-60" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/dc-24.png')}}" class="clapping clap-24 teeth_lower lower-claps" style="display: none"></image>
                                                                        <text id="lower-24-num" data-name="24" class="cls-1 teeth_lower" transform="translate(703.596 190.046) scale(1.199)">
                                                                            <tspan x="0" y="-40">24</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- lower-teeth 9 -->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-25-teeth-white" data-id="teeth8" data-name="25" x="760" y="42" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/d-25.png')}}" class="img-for-teeth teeth_lower lower-25-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-25-teeth-yellow" data-id="teeth8" data-name="25" x="760" y="42" width="111" height="182" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-25.png')}}" class="img-for-teeth teeth_lower lower-25-class-teeth-yellow"></image>
                                                                            <image id="lower-25-teeth-red" data-id="teeth8" data-name="25" x="760" y="42" width="111" height="182" xlink:href="{{asset('admin/assets/images/red-teeth/d-25.png')}}" class="img-for-teeth teeth_lower lower-25-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-25-teeth-grey" data-id="teeth8" data-name="25" x="760" y="42" width="111" height="182" xlink:href="{{asset('admin/assets/images/grey-teeth/d-25.png')}}" class="img-for-teeth teeth_lower lower-25-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-25-teeth-greeen" data-id="teeth8" data-name="25" x="760" y="42" width="111" height="182" xlink:href="{{asset('admin/assets/images/green-teeth/d-25.png')}}" class="img-for-teeth teeth_lower lower-25-class-teeth-green" style="display: none"></image>
                                                                        </a>
                                                                        <!--  image with cliap-9 -->
                                                                        <image id="lower-25-claps" data-name="25" x="762" y="-58" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/dc-25.png')}}" class="clapping clap-25 teeth_lower lower-claps" style="display: none"></image>
                                                                        <text id="lower-25-num" data-name="25" class="cls-1 teeth_lower" transform="translate(789.005 190.046) scale(1.199)">
                                                                            <tspan x="10" y="-50">25</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- lower-teeth 10 -->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-26-teeth-white" data-id="teeth9" data-name="26" x="865" y="54" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/d-26.png')}}" class="img-for-teeth teeth_lower lower-26-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-26-teeth-yellow" data-id="teeth9" data-name="26" x="865" y="54" width="115" height="186" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-26.png')}}" class="img-for-teeth teeth_lower lower-26-class-teeth-yellow"></image>
                                                                            <image id="lower-26-teeth-red" data-id="teeth9" data-name="26" x="865" y="54" width="115" height="186" xlink:href="{{asset('admin/assets/images/red-teeth/d-26.png')}}" class="img-for-teeth teeth_lower lower-26-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-26-teeth-grey" data-id="teeth9" data-name="26" x="865" y="54" width="115" height="186" xlink:href="{{asset('admin/assets/images/grey-teeth/d-26.png')}}" class="img-for-teeth teeth_lower lower-26-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-26-teeth-green" data-id="teeth9" data-name="26" x="865" y="54" width="115" height="186" xlink:href="{{asset('admin/assets/images/green-teeth/d-26.png')}}" class="img-for-teeth teeth_lower lower-26-class-teeth-green" style="display: none"></image>
                                                                        </a>
                                                                        <!--  image with cliap-11 -->
                                                                        <image id="lower-26-claps" data-name="26" x="880" y="-35" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/dc-26.png')}}" class="clapping clap-26 teeth_lower lower-claps" style="display: none"></image>
                                                                        <text id="lower-26-num" data-name="26" class="cls-1 teeth_lower" transform="translate(881.008 207.152) scale(1.199)">
                                                                            <tspan x="25" y="-50">26</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- lower-teeth 11-->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-27-teeth-white" data-id="teeth11" data-name="27" x="955" y="80" width="135" height="200" xlink:href="{{asset('admin/assets/images/teeth/d-27.png')}}" class="img-for-teeth teeth_lower lower-27-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-27-teeth-yellow" data-id="teeth11" data-name="27" x="955" y="80" width="135" height="200" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-27.png')}}" class="img-for-teeth teeth_lower lower-27-class-teeth-yellow" ></image>
                                                                            <image id="lower-27-teeth-red" data-id="teeth11" data-name="27" x="955" y="80" width="135" height="200" xlink:href="{{asset('admin/assets/images/red-teeth/d-27.png')}}" class="img-for-teeth teeth_lower lower-27-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-27-teeth-grey" data-id="teeth11" data-name="27" x="955" y="80" width="135" height="200" xlink:href="{{asset('admin/assets/images/grey-teeth/d-27.png')}}" class="img-for-teeth teeth_lower lower-27-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-27-teeth-green" data-id="teeth11" data-name="27" x="955" y="80" width="135" height="200" xlink:href="{{asset('admin/assets/images/green-teeth/d-27.png')}}" class="img-for-teeth teeth_lower lower-27-class-teeth-green" style="display: none"></image>
                                                                        </a>
                                                                        <!--  image with cliap-11 -->
                                                                        <image id="lower-27-claps" data-name="27" x="990" y="-5" width="110" height="200" xlink:href="{{asset('admin/assets/images/teeth/dc-27.png')}}" class="clapping clap-27 teeth_lower lower-claps" style="display: none"></image>
                                                                        <text id="lower-27-num" data-name="27" class="cls-1 teeth_lower" transform="translate(982.792 240.944) scale(1.199)">
                                                                            <tspan x="25" y="-50">27</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- lower-teeth 12 -->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-28-teeth-white" data-id="teeth10" data-name="28" x="1035" y="134" width="150" height="177" xlink:href="{{asset('admin/assets/images/teeth/d-28.png')}}" class="img-for-teeth teeth_lower lower-28-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-28-teeth-yellow" data-id="teeth10" data-name="28" x="1035" y="134" width="150" height="177" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-28.png')}}" class="img-for-teeth teeth_lower lower-28-class-teeth-yellow" ></image>
                                                                            <image id="lower-28-teeth-red" data-id="teeth10" data-name="28" x="1035" y="134" width="150" height="177" xlink:href="{{asset('admin/assets/images/red-teeth/d-28.png')}}" class="img-for-teeth teeth_lower lower-28-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-28-teeth-grey" data-id="teeth10" data-name="28" x="1035" y="134" width="150" height="177" xlink:href="{{asset('admin/assets/images/grey-teeth/d-28.png')}}" class="img-for-teeth teeth_lower lower-28-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-28-teeth-green" data-id="teeth10" data-name="28" x="1035" y="134" width="150" height="177" xlink:href="{{asset('admin/assets/images/green-teeth/d-28.png')}}" class="img-for-teeth teeth_lower lower-28-class-teeth-green" style="display: none"></image>
                                                                        </a>
                                                                        <!--  image with cliap-12 -->
                                                                        <image id="lower-28-claps" data-name="28" x="1100" y="60" width="100" height="177" xlink:href="{{asset('admin/assets/images/teeth/dc-28.png')}}" class="clapping clap-28 teeth_lower lower-claps" style="display: none"></image>
                                                                        <text id="lower-28-num" data-name="28" class="cls-1 teeth_lower" transform="translate(1037.236 283.518) scale(1.199)">
                                                                            <tspan x="40" y="-50">28</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- lower-teeth 13 -->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-29-teeth-white" data-id="teeth12" data-name="29" x="1098" y="205" width="167" height="159" xlink:href="{{asset('admin/assets/images/teeth/d-29.png')}}" class="img-for-teeth teeth_lower lower-29-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-29-teeth-yellow" data-id="teeth12" data-name="29" x="1098" y="205" width="167" height="159" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-29.png')}}" class="img-for-teeth teeth_lower lower-29-class-teeth-yellow"></image>
                                                                            <image id="lower-29-teeth-red" data-id="teeth12" data-name="29" x="1098" y="205" width="167" height="159" xlink:href="{{asset('admin/assets/images/red-teeth/d-29.png')}}" class="img-for-teeth teeth_lower lower-29-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-29-teeth-grey" data-id="teeth12" data-name="29" x="1098" y="205" width="167" height="159" xlink:href="{{asset('admin/assets/images/grey-teeth/d-29.png')}}" class="img-for-teeth teeth_lower lower-29-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-29-teeth-green" data-id="teeth12" data-name="29" x="1098" y="205" width="167" height="159" xlink:href="{{asset('admin/assets/images/green-teeth/d-29.png')}}" class="img-for-teeth teeth_lower lower-29-class-teeth-green" style="display: none"></image>
                                                                        </a>
                                                                        <!--  image with cliap-13 -->
                                                                        <image id="lower-29-claps" data-name="29" x="1190" y="155" width="100" height="159" xlink:href="{{asset('admin/assets/images/teeth/dc-29.png')}}" class="clapping clap-29 teeth_lower lower-claps" style="display: none"></image>
                                                                        <text id="lower-29-num" data-name="29" class="cls-1 teeth_lower" transform="translate(1097.072 329.95) scale(1.199)">
                                                                            <tspan x="45" y="-50">29</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <g>
                                                                        <!-- lower-teeth 14 -->
                                                                        <a href="javascript:;">
                                                                            <image id="lower-30-teeth-white" data-id="teeth13" data-name="30" x="1130" y="278" width="234" height="191" xlink:href="{{asset('admin/assets/images/teeth/d-30.png')}}" class="img-for-teeth teeth_lower lower-30-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-30-teeth-yellow" data-id="teeth13" data-name="30" x="1130" y="278" width="234" height="191" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-30.png')}}" class="img-for-teeth teeth_lower lower-30-class-teeth-yellow"></image>
                                                                            <image id="lower-30-teeth-red" data-id="teeth13" data-name="30" x="1130" y="278" width="234" height="191" xlink:href="{{asset('admin/assets/images/red-teeth/d-30.png')}}" class="img-for-teeth teeth_lower lower-30-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-30-teeth-grey" data-id="teeth13" data-name="30" x="1130" y="278" width="234" height="191" xlink:href="{{asset('admin/assets/images/grey-teeth/d-30.png')}}" class="img-for-teeth teeth_lower lower-30-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-30-teeth-green" data-id="teeth13" data-name="30" x="1130" y="278" width="234" height="191" xlink:href="{{asset('admin/assets/images/green-teeth/d-30.png')}}" class="img-for-teeth teeth_lower lower-30-class-teeth-green" style="display: none"></image>
                                                                        </a>
                                                                        <!--  image with cliap-14 -->
                                                                        <image id="lower-30-claps" data-name="30" x="1280" y="258" width="100" height="191" xlink:href="{{asset('admin/assets/images/teeth/dc-30.png')}}" class="clapping clap-30 teeth_lower lower-claps" style="display: none"></image>
                                                                        <text id="lower-30-num" data-name="30" class="cls-1 teeth_lower" transform="translate(1164.707 409.172) scale(1.199)">
                                                                            <tspan x="45" y="-45">30</tspan>
                                                                        </text>
                                                                    </g>
                                                                    <!-- lower-teeth 15 -->

                                                                    <g>
                                                                        <a href="javascript:;">
                                                                            <image id="lower-31-teeth-white" data-id="teeth14" data-name="31" x="1210" y="395" width="232" height="194" xlink:href="{{asset('admin/assets/images/teeth/d-31.png')}}" class="img-for-teeth teeth_lower lower-31-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-31-teeth-yellow" data-id="teeth14" data-name="31" x="1210" y="395" width="232" height="194" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-31.png')}}" class="img-for-teeth teeth_lower lower-31-class-teeth-yellow"></image>
                                                                            <image id="lower-31-teeth-red" data-id="teeth14" data-name="31" x="1210" y="395" width="232" height="194" xlink:href="{{asset('admin/assets/images/red-teeth/d-31.png')}}" class="img-for-teeth teeth_lower lower-31-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-31-teeth-grey" data-id="teeth14" data-name="31" x="1210" y="395" width="232" height="194" xlink:href="{{asset('admin/assets/images/grey-teeth/d-31.png')}}" class="img-for-teeth teeth_lower lower-31-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-31-teeth-green" data-id="teeth14" data-name="31" x="1210" y="395" width="232" height="194" xlink:href="{{asset('admin/assets/images/green-teeth/d-31.png')}}" class="img-for-teeth teeth_lower lower-31-class-teeth-green" style="display: none"></image>
                                                                        </a>
                                                                        <!--  image with cliap-15 -->
                                                                        <image id="lower-31-claps" data-name="31" x="1390" y="420" width="80" height="130" xlink:href="{{asset('admin/assets/images/teeth/dc-30.png')}}" class="clapping clap-31 teeth_lower lower-claps" style="display: none"></image>
                                                                        <text id="lower-31-num" data-name="31" class="cls-1 teeth_lower" transform="translate(1238.134 502.8) scale(1.199)">
                                                                            <tspan x="50">31</tspan>
                                                                        </text>
                                                                        <!-- lower-teeth 16 -->
                                                                    </g>

                                                                    <g>
                                                                        <a href="javascript:;">
                                                                            <image id="lower-32-teeth-white" data-id="teeth16" data-name="32" x="1273" y="560" width="232" height="193" xlink:href="{{asset('admin/assets/images/teeth/d-32.png')}}" class="img-for-teeth teeth_lower lower-32-class-teeth-white" style="display: none"></image>
                                                                            <image id="lower-32-teeth-yellow" data-id="teeth16" data-name="32" x="1273" y="560" width="232" height="193" xlink:href="{{asset('admin/assets/images/yellow-teeth/d-32.png')}}" class="img-for-teeth teeth_lower lower-32-class-teeth-yellow"></image>
                                                                            <image id="lower-32-teeth-red" data-id="teeth16" data-name="32" x="1273" y="560" width="232" height="193" xlink:href="{{asset('admin/assets/images/red-teeth/d-32.png')}}" class="img-for-teeth teeth_lower lower-32-class-teeth-red" style="display: none"></image>
                                                                            <image id="lower-32-teeth-grey" data-id="teeth16" data-name="32" x="1273" y="560" width="232" height="193" xlink:href="{{asset('admin/assets/images/grey-teeth/d-32.png')}}" class="img-for-teeth teeth_lower lower-32-class-teeth-grey" style="display: none"></image>
                                                                            <image id="lower-32-teeth-green" data-id="teeth16" data-name="32" x="1273" y="560" width="232" height="193" xlink:href="{{asset('admin/assets/images/green-teeth/d-32.png')}}" class="img-for-teeth teeth_lower lower-32-class-teeth-green" style="display: none"></image>
                                                                        </a>
                                                                        <!--  image with cliap-32 -->
                                                                        <image id="lower-32-claps" data-name="32" x="1440" y="580" width="114" height="92" xlink:href="{{asset('admin/assets/images/teeth/dc-32.png')}}" class="clapping clap-32 teeth_lower lower-claps" style="display: none"></image>
                                                                        <text id="lower-32-num" data-name="32" class="cls-1 teeth_lower" transform="translate(1268.177 618.27) scale(1.199)">
                                                                            <tspan x="80" y="20">32</tspan>
                                                                        </text>
                                                                    </g>

                                                                    <!--lower teeth numbers ------------------------------ -->

                                                                </g>

                                                            </svg>

                                                            <div class="d-block text-center teeth-text">
                                                                <a href="javascript:;" class="add-btn p-2 mx-auto lower_mising_all">
                                                                    Missing all teeth
                                                                </a>
                                                            </div>
                                                            <div class="teeths-wrapper second-row p-0">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-xl-12 col-lg-12">
                                                                        <div class="row gy-3">

                                                                            <div class="col-md-12" id="lower_in_mouth_div" style="display: none;">
                                                                                <div class="input-group">
                                                                                    <label class="form-control d-flex align-items-center justify-content-center inner-label te-in-month lower_lbl_btn_all lower_lable_click" value="" id="lower_in_mouth">

                                                                                        <div class="lable-left">
                                                                                            Teeth in mouth <br> <span id="lower_in_mouth_lbl"></span>
                                                                                        </div>
                                                                                    </label>
                                                                                    <div class="error-tooltip" id="lower_teeth_in_mouth_error" style="display:none;">
                                                                                        <span class="error-icon d-flex justify-content-between">
                                                                                            <span>
                                                                                                Please select one or more teeth in mouth</span>
                                                                                            <img alt="toat img" src="{{asset('admin/assets/images/toast-img.png')}}" />
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="hidden" id="lower_in_mouth_value" name="lower_in_mouth_value" value="">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-12" id="lower_missing_teeth_div" style="display: none;">
                                                                                <div class="input-group">
                                                                                    <label class="form-control d-flex align-items-center justify-content-center inner-label missing-te lower_lbl_btn_all lower_lable_click" value="" id="lower_missing_teeth">

                                                                                        <div class="lable-left">
                                                                                            missing teeth <br> <span id="lower_missing_teeth_lbl"></span>

                                                                                        </div>

                                                                                    </label>
                                                                                    <div class="error-tooltip" id="lower_missing_teeth_error" style="display:none;">
                                                                                        <span class="error-icon d-flex w-100 justify-content-center">
                                                                                            <span>Please select one or more missing teeth</span>
                                                                                            <img alt="toat img" src="{{asset('admin/assets/images/toast-img.png')}}" class="" />
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="hidden" name="lower_missing_teeth_value" id="lower_missing_teeth_value" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12" id="lower_ectract_delivery_div" style="display: none;">
                                                                                <div class="input-group">
                                                                                    <label class="form-control d-flex align-items-center justify-content-center inner-label ex-dekivery lower_lbl_btn_all lower_lable_click" value="" id="lower_ectract_delivery">

                                                                                        <div class="lable-left">
                                                                                            will extract on delivery <br><span id="lower_ectract_delivery_lbl"></span>
                                                                                        </div>


                                                                                    </label>
                                                                                    <div class="error-tooltip" id="lower_will_extract_on_delivery_error" style="display:none;">
                                                                                        <span class="error-icon d-flex w-100">
                                                                                            <span>
                                                                                                Please select one or more will extract on delivery
                                                                                            </span>
                                                                                            <img alt="toat img" src="{{asset('admin/assets/images/toast-img.png')}}" class="ms-auto" />
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="hidden" id="lower_ectract_delivery_value" name="lower_ectract_delivery_value" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12" id="lower_been_extracted_div" style="display: none;">
                                                                                <div class="input-group">
                                                                                    <label class="form-control d-flex align-items-center justify-content-center inner-label ben-extracted lower_lbl_btn_all lower_lable_click" value="" id="lower_been_extracted">

                                                                                        <div class="lable-left">
                                                                                            has been extracted <br><span id="lower_been_extracted_lbl"></span>
                                                                                        </div>
                                                                                    </label>

                                                                                    <div class="error-tooltip" id="lower_has_been_extracted_error" style="display:none;">
                                                                                        <span class="error-icon d-flex w-100">
                                                                                            <span>Please select one or more has been extracted</span>
                                                                                            <img alt="toat img" src="{{asset('admin/assets/images/toast-img.png')}}" class="ms-auto" />
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="hidden" id="lower_been_extracted_value" name="lower_been_extracted_value" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6" id="lower_fix_div" style="display: none;">
                                                                                <div class="input-group">
                                                                                    <label class="form-control d-flex align-items-center justify-content-center inner-label fix-add lower_lbl_btn_all lower_lable_click" value="" id="lower_fix">



                                                                                        <div class="lable-left">
                                                                                            fix or add <br> <span id="lower_fix_lbl"></span>
                                                                                        </div>
                                                                                    </label>
                                                                                    <div class="error-tooltip" id="lower_fix_or_add_error" style="display:none;height:45px;">
                                                                                        <span class="error-icon d-flex w-100 ">
                                                                                            <span> Please select one or more fix or add</span>
                                                                                            <img alt="toat img" src="{{asset('admin/assets/images/toast-img.png')}}" class="ms-auto" />
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="hidden" id="lower_fix_value" name="lower_fix_value" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6" id="lower_clasps_div" style="display: none;">
                                                                                <div class="input-group">
                                                                                    <label class="form-control d-flex align-items-center justify-content-center inner-label clasps lower_lbl_btn_all lower_lable_click" value="" id="lower_clasps">

                                                                                        <div class="lable-left">
                                                                                            clasps<br> <span id="lower_clasps_lbl"></span>
                                                                                        </div>


                                                                                    </label>
                                                                                    <div class="error-tooltip" id="lower_clasps_error" style="display:none;height:45px;">
                                                                                        <span class="error-icon d-flex w-100">
                                                                                            <span>Please select one or more clasps</span>
                                                                                            <img alt="toat img" src="{{asset('admin/assets/images/toast-img.png')}}" class="ms-auto" />
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="hidden" id="lower_clasps_value" name="lower_clasps_value" value="">
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12 notes_div stagenote-text-area" style="display: none;overflow: hidden;">
                                                        <div class="stage-title-bold">
                                                            <div class="d-flex justify-content-center align-items-center">
                                                                <h3 class="card-title text-start">
                                                                    Stage Notes
                                                                </h3>
                                                                <ul class="note-btn-group ps-3">
                                                                    <li>
                                                                        <a href="javascript:;" id="add_attachments_model_open">
                                                                            <i class="fad fa-link"></i>
                                                                        </a>
                                                                    </li>

                                                                    {{-- <li>
                                                                            <a href="javascript:;" id="free_drawing_open">
                                                                                <i class="fad fa-pencil"></i>
                                                                            </a>
                                                                        </li>

                                                                        <li>
                                                                            <a href="javascript:;" id="add_video_open">
                                                                                <i class="fad fa-video"></i>
                                                                            </a>
                                                                        </li>

                                                                        <li>
                                                                            <a href="javascript:;" id="add_recording_open">
                                                                                <i class="fad fa-microphone"></i>
                                                                            </a>
                                                                        </li>

                                                                        <li>
                                                                            <a href="javascript:;" id="link_model_open">
                                                                                <i class="fad fa-external-link-alt"></i>
                                                                            </a>
                                                                        </li> --}}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Description"></textarea>

                                                    </div>




                                                    {{-- <div class="modal fade" id="link_model" tabindex="-1">
                                                        <div class="modal-dialog modal-lg modal-dialog-centered">
                                                            <div class="modal-content" style="background: #f1f1f1">
                                                                <div class="link-model-wrapper all-icon-model">
                                                                    <div class="c-model-header">
                                                                        <h5>Inset video</h5>
                                                                        <a href="#" class="close-model">
                                                                            <i class="fal fa-times"></i>
                                                                        </a>
                                                                    </div>

                                                                    <div class="c-model-body p-3">
                                                                        <div class="row">
                                                                            <div class="col-lg-11 mx-auto text-start p-0">
                                                                                <label for="exampleInputEmail1" class="form-label model-label"> <strong> Video URl</strong> <span>(Youtube, vimeo ,Instagram,Dailymotion or youku)</span></label>
                                                                                <input type="text" class="form-control p-0" id="exampleInputEmail1" aria-describedby="emailHelp">
                                                                            </div>
                                                                            <div class="col-lg-11 mx-auto mt-1 p-0">
                                                                                <ul class="list-group">
                                                                                    <li class="list-group-item">A second item</li>
                                                                                    <li class="list-group-item">A third item</li>
                                                                                    <li class="list-group-item">A fourth item</li>
                                                                                    <li class="list-group-item">And a fifth one</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="c-model-footer py-3">
                                                                        <hr>
                                                                        <div class="row">
                                                                            <div class="col-lg-11 mx-auto p-0">
                                                                                <div class="text-end">
                                                                                    <a href="#" class="model-btn">
                                                                                        Insert Video
                                                                                    </a>
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> --}}
                                                    <!-- link-model end-->

                                                    <!-- add-attachments-model start-->

                                                    <!-- add-attachments-model End-->

                                                    <!-- Free Drawing-model start-->
                                                    <div class="modal fade" id="free_drawing_model" tabindex="-1">
                                                        <div class="modal-dialog modal-lg modal-dialog-centered">
                                                            <div class="modal-content" style="background: #f1f1f1">
                                                                <div class="link-model-wrapper all-icon-model">
                                                                    <div class="c-model-header">
                                                                        <h5>Free Drawing</h5>
                                                                        <a href="#" class="close-model">
                                                                            <i class="fal fa-times"></i>
                                                                        </a>
                                                                    </div>



                                                                    <div class="c-model-footer py-3">
                                                                        <hr>
                                                                        <div class="row">
                                                                            <div class="col-lg-11 mx-auto ">
                                                                                <div class="text-end">

                                                                                    <a href="#" class="model-btn">
                                                                                        Submit Signature
                                                                                    </a>
                                                                                    <a href="#" class="model-btn clear-sign" id="sig-clearBtn">
                                                                                        Clear Signature
                                                                                    </a>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Free Drawing End-->

                                                    <!-- Add video model start-->
                                                    <div class="modal fade" id="add_video_model" tabindex="-1">
                                                        <div class="modal-dialog modal-lg modal-dialog-centered">
                                                            <div class="modal-content" style="background: #f1f1f1">
                                                                <div class="link-model-wrapper all-icon-model">
                                                                    <div class="c-model-header">
                                                                        <h5>Add video</h5>
                                                                        <a href="#" class="close-model">
                                                                            <i class="fal fa-times"></i>
                                                                        </a>
                                                                    </div>

                                                                    <div class="c-model-footer py-3">
                                                                        <hr>
                                                                        <div class="row">
                                                                            <div class="col-lg-11 mx-auto ">
                                                                                <div class="text-end">
                                                                                    <a href="#" class="model-btn">
                                                                                        Submit video
                                                                                    </a>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Add video-model End-->

                                                    <!-- Add recording model start-->
                                                    <div class="modal fade" id="add_recording_model" tabindex="-1">
                                                        <div class="modal-dialog modal-lg modal-dialog-centered">
                                                            <div class="modal-content" style="background: #f1f1f1">
                                                                <div class="link-model-wrapper all-icon-model">
                                                                    <div class="c-model-header">
                                                                        <h5>chosen Recording</h5>
                                                                        <a href="#" class="close-model">
                                                                            <i class="fal fa-times"></i>
                                                                        </a>
                                                                    </div>

                                                                    <div class="c-model-footer py-3">
                                                                        <hr>
                                                                        <div class="row">
                                                                            <div class="col-lg-11 mx-auto ">
                                                                                <div class="text-end">
                                                                                    <a href="#" class="model-btn">
                                                                                        Submit Recording
                                                                                    </a>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-lg-12 col-xl-4 col-xxl-4 align-self-end d-inline-block mt-3 order-xxl-1">
                                                            <a href="{{route('admin.labcase.office_admin_listing')}}" class="btn back-btn me-2 cancel-btn p-1" style="height:28px;">Cancel Slip</a>
                                                            <a type="submit" id="save_finish_later" class="btn submit-btn me-2 p-1" style="height:28px">Save slip and finish later</a>
                                                        </div>

                                                        <div class="col-lg-12 col-xl-8 col-xxl-6 align-self-end  order-xxl-2 order-1 ms-auto" id="submit_div" style="display:none;">
                                                            <div class="d-flex justify-content-lg-end align-items-center flex-wrap">
                                                                <div class="add-case-bottom ms-auto" id="information_class">
                                                                    <div class="d-flex justify-content-center align-items-center">
                                                                        <img alt="toat img" class="toast-img" src="{{asset('admin/assets/images/toast-img.png')}}" />
                                                                        <label>
                                                                            By clicking this box you acknowledge all information is correct
                                                                        </label>
                                                                        <input class="ms-5 c-checkbox" id="informations_checkbox" type="checkbox" value="">
                                                                    </div>
                                                                    <a href="javascript:;" id="submit" class="btn add-btn ms-2"> Submit slip </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </form>
                                        <div class="modal fade" id="add_attachments_model">
                                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                                <div class="modal-content" style="background: #f1f1f1">
                                                    <div class="link-model-wrapper all-icon-model">
                                                        <div class="c-model-header">
                                                            <h5>Add attachments</h5>
                                                            <a href="javascript:;" id="add_attachments_model_close" class="close-model">
                                                                <i class="fal fa-times"></i>
                                                            </a>
                                                        </div>
                                                        <div class="card-content collapse show">
                                                            <div class="card-body" style="max-height: 400px;overflow-y: auto;">
                                                                <form action="{{route('admin.labcase.dropzoneStore')}}" class="dropzone" id="dropzonewidget" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <input type="hidden" name="iTampId" id="iTampId" value="{{$tampid}}">
                                                                </form>

                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="mt-3" id="attachment_list">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>



                                                        <div class="c-model-footer py-3">
                                                            <hr>
                                                            <div class="row">
                                                                <div class="col-lg-11 mx-auto p-0">
                                                                    <div class="text-end">

                                                                        <a href="javascript:;" id="submit_attacement" class="model-btn">
                                                                            Submit
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gy-3">
                <div class="col-lg-4 col-sm-6 align-self-center">
                    @if($eAddSlipAccess == 'Yes')
                    <a href="javascript:;" id="open_add_slip" class="btn add-btn mx-2 create_permission">
                        <i class="fa fa-plus"></i> new slip
                    </a>
                    @endif
                </div>

                @if(\App\Libraries\General::admin_info()['customerType'] == 'Super Admin')
                <div class="col-lg-4 col-sm-6 align-self-center">
                    <select name="iOfficeId" id="iOfficeId_list" class="OnChangeSearch">
                        <option value="">Select office</option>
                        <option value="null">ALL</option>
                        @foreach($customer_data as $key => $customer_datas)
                        <option value="{{$customer_datas->iCustomerId}}" >{{$customer_datas->vOfficeName}}</option>
                        @endforeach
                    </select>
                </div>
                @elseif(\App\Libraries\General::admin_info()['customerType'] == 'Lab Admin')
                <div class="col-lg-4 col-sm-6 align-self-center">
                    <select name="iOfficeId" id="iOfficeId_list" class="OnChangeSearch">
                        <option value="">Select office</option>
                        <option value="null">ALL</option>
                        @foreach($office as $key => $offices)
                        <option value="{{$offices->iCustomerId}}" @if(isset($categoryproductselect)){{$offices->iCustomerId == $categoryproductselect->iCustomerId  ? 'selected' : ''}} @endif>{{$offices->vOfficeName}}</option>
                        @endforeach
                    </select>
                </div>
                @else
                <div class="col-lg-4 col-sm-6 align-self-center">
                </div>
                @endif
            
                <div class="col-lg-4 col-sm-6 align-self-center">
                    <select name="iDoctorId" id="iDoctorId_list" class="OnChangeSearch">
                        <option value="">Select Doctor </option>
                    </select>
                </div>
                
                <div class="col-lg-4 col-sm-6 align-self-center text-end ms-auto">
                    <div class="custome-serce-box">
                            <input type="text" class="form-control show_slip" id="keyword" name="search" class="search" placeholder="Search">
                            <span class="search-btn"><i class="fas fa-search"></i></span>
                    </div>
                </div>


                <div class="col-lg-4 col-sm-6 align-self-center">
                    <select name="vListLocation" id="vLocation_list" class="OnChangeSearch" tabindex="-1"> 
                        <option value="">Select location</option>
                        <option value="null">ALL</option>
                        <option value="1">In office ready to pickup</option>
                        <option value="2">On route to the lab</option>
                        <option value="3">In Lab</option>
                        <option value="4">In lab ready to pickup</option>
                        <option value="5">On route to the office</option>
                        <option value="6">In office</option>
                    </select>
                </div>
               
                <div class="col-lg-4 col-sm-6 align-self-center">
                    <select name="eListStatus" id="eStatus_list" class="OnChangeSearch" tabindex="-1">
                        <option value="">Select status</option>
                        <option value="null">ALL</option>
                        <option value="Draft">Draft</option>
                        <option selected value="On Process">On Process</option>
                        <option value="Canceled">Canceled</option>
                        <option value="Finished">Finished</option>
                        <option value="On Hold">On Hold</option>
                        {{-- <option value="IsDeleted">Trash</option> --}}
                    </select>
                </div>

                <form method="post" action="{{route('admin.billing.driverSlipPrint')}}" >
                    <div class="row" id="ShowCustomDate" style="display: none">
                        @csrf
                        <div class="col-lg-3 " >
                            <div class="form-group">
                                <input style="height: 37px;" class="form-control changeDate" type="text" placeholder="Start Date" id="startdate" name="startdate"/>
                            </div>
                        </div>
                        <div class="col-lg-3 " >
                            <div class="form-group">
                                <input style="height: 37px;" class="form-control changeDate" type="text" placeholder="End Date" id="enddate" name="enddate"/>
                            </div>
                        </div>
                        {{-- <div class="col-lg-3 " >
                            <input type="button"  style="height: 37px;visibility:hidden;" href="javascript:;" id="PrintDriverSlip" class="btn add-btn mx-2 ">
                        </div> --}}
                    </div>
                </form>
                <form method="post" action="{{route('admin.billing.driverSlipPrint')}}" id="submitCheckedForm">
                    @csrf
                    <input type="hidden" name="islipIdPrint" id="islipIdPrint" >
                    <input type="hidden" name="deliveryDateArray" id="deliveryDateArray" >
                    <input type="hidden" name="start_date" id="start_date" >
                    <input type="hidden" name="end_date" id="end_date" >
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">

            </div>

        </div>

    </div>
    <div class="listing-page">
        <div class="table-data table-responsive">
            <table class="table">
                <thead>
                    <tr class="fw-bolder text-muted">
                        <!-- <th class="min-w-150px">Image</th> -->
                        <th class="min-w-140px"><a id="vPatientName" class="sort" data-column="vPatientName" data-order="ASC" href="#"><span class="text-muted fw-bold text-muted d-block fs-7">Pan</span></a></th>

                        <th class="min-w-120px"><a id="vPatientName" class="sort" data-column="vPatientName" data-order="ASC" href="#">
                                <span class="text-muted fw-bold text-muted d-block fs-7">Lab Name</span> </a>
                        </th>
                        <th class="min-w-120px"><a id="vPatientName" class="sort" data-column="vPatientName" data-order="ASC" href="#">
                                <span class="text-muted fw-bold text-muted d-block fs-7">Patient</span></a>
                        </th>
                        @if(isset($eVirualSlipAccess) && $eVirualSlipAccess =='Yes')
                        <th class="min-w-120px"><a id="vPatientName" class="sort" data-column="vPatientName" data-order="ASC" href="#">
                                <span class="text-muted fw-bold text-muted d-block fs-7">Slip</span></a>
                        </th>
                        @endif
                       
                        @if(isset($ePaperSlipAccess) && $ePaperSlipAccess =='Yes')
                        <th class="min-w-120px">
                            {{-- <a id="vPatientName" class="sort" data-column="vPatientName" data-order="ASC" href="#">
                                <div class="d-flex justify-content-center">
                                    <select name="eDriverPrintSlip" id="eDriverPrintSlip"  tabindex="-1">
                                        
                                        <option value="">Select date</option>
                                        @if(isset($slip_data) && !empty($slip_data))
                                            @foreach ($slip_data as $key=> $value)
                                            <option value=" {{strtotime($value->dDeliveryDate)}}">
                                                @php 
                                                $delivery_date = $value->dDeliveryDate;
                                                $current_date = date("Y-m-d");
                                        
                                                $difference =  date_diff(date_create($delivery_date), date_create($current_date));
                                        
                                                $days = $difference->days;
                                                // $days_text = "";
                                            @endphp
                                            
                                            @if($delivery_date > $current_date)
                                                @php 
                                                    $days_text = $days ." days"; 
                                                @endphp
                                            @elseif($delivery_date == $current_date)
                                                @php 
                                                    $days_text = " today "; 
                                                @endphp
                                            @endif
                                             @if(!empty($days_text))   
                                                {{$days_text}}
                                              @endif  
                                            </option>
                                            @endforeach
                                            <option value="CustomDate">Custom Date</option>
                                        @endif
                                    </select>
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <div class="form-check form-check-sm form-check-custom form-check-solid">
                                <input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" id="selectall" type="checkbox" name="selectall">
                                <label for="selectall">&nbsp;</label>
                            </div>
                            &nbsp;&nbsp;
                            <a href="javascript:;" data-case="1" data-slip="1" class="mt-3 print_driver_checked_slip">
                                <i class="fal fa-print" style="color: #fff;font-size: 18px;"></i>
                            </a> --}}
                            <a href="javascript:;" data-case="1" data-slip="1" class="mt-3 ">
                                <i class="fal fa-print" style="color: #fff;font-size: 18px;"></i>
                            </a>
                           
                        </th>
                        @endif
                        <th class="min-w-120px"><a id="vPatientName" class="sort" data-column="vPatientName" data-order="ASC" href="#">
                                <span class="text-muted fw-bold text-muted d-block fs-7">Due Date </span></a>
                        </th>
                        <th scope="col" class="min-w-150px"><a id="eStatus" class="sort" data-column="eStatus" data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7"> Location </span> </a></th>
                        <th class="min-w-120px"><a  href="#">
                                <span class="text-muted fw-bold text-muted d-block fs-7">Created At</span></a>
                        </th>
                       
                    </tr>
                </thead>
                <tbody id="table_record">
                </tbody>
            </table>

            <div class="text-center loaderimg">
                <div class="loaderinner">
                    <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<iframe id="printf" name="printf" style="display: none;"></iframe>
@endsection

@section('custom-js')

<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>

<script async type="text/javascript">
    $('#iLabId').selectize();
    $('#iDoctorId').selectize();
    $('#vLocation').selectize();
    $('#eStatus').selectize();
 $('#eDriverPrintSlip').selectize({
        maxItems: null,
        delimiter: ',',
        persist: true,
        plugins: ['remove_button'],
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
    });
    $('#iLabId_list').selectize();
    $('#iOfficeId_list').selectize();
    $('#iDoctorId_list').selectize();
    $('#vLocation_list').selectize();
    $('#eStatus_list').selectize();
    $('#iUpperCategoryId').selectize();
    $('#iLowerCategoryId').selectize();
    $('#iUpperProductId').selectize();
    $('#iLowerProductId').selectize();
    $('#iUpperGradeId').selectize();
    $('#iLowerGradeId').selectize();
    $('#iUpperStageId').selectize();
    $('#iLowerStageId').selectize();
    // $('#upper_impression').selectize();
    // $('#lower_impression').selectize();
    $('#upper_tsbrand').selectize();
    $('#lower_tsbrand').selectize();
    $('#upper_tshade').selectize();
    $('#lower_tshade').selectize();
    $('#upper_gsbrand').selectize();
    $('#lower_gsbrand').selectize();
    $('#upper_gshade').selectize();
    $('#lower_gshade').selectize();
    $('#lower_status').selectize();
    $('#lower_status').selectize();
    $('#upper_status').selectize();
    $('#lower_category_addons').selectize();
    $('#upper_category_addons').selectize();
    $('#upper_subaddons').selectize();
    $('#lower_subaddons').selectize();
    $('#upper_status').selectize();
    $('#lower_status').selectize();
    $(document).ready(function() {
        // For Print Slip Data Start
         PrintSlip();
        // For Print Slip Data End

        $.ajax({
            url: "{{route('admin.labcase.ajax_listing_office')}}",
            type: "get",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
        $('#eStatus')[0].selectize.lock();
        $('#vLocation')[0].selectize.lock();
        $('#upper_status')[0].selectize.lock();
        $('#lower_status')[0].selectize.lock();
        $("#eStatus").attr("readonly", true);
        $("#vLocation").attr("readonly", true);
    });

    function PrintSlip()
    {
        var Sess_iSlipId = "{{Session::get('Sess_iSlipId')}}";
        var Sess_iCaseId = "{{Session::get('Sess_iCaseId')}}";
        if(Sess_iSlipId != '' && Sess_iSlipId !=undefined && Sess_iCaseId != '' && Sess_iCaseId !=undefined)
        {
            $.ajax({
                url: "{{route('admin.labcase.paper_slip')}}",
                type: "post",
                data: {
                    iCaseId: Sess_iCaseId,
                    iSlipId: Sess_iSlipId,
                    _token: '{{csrf_token()}}'
                },
                success: function(response) {
                    // alert("{{$_SERVER['HTTP_USER_AGENT']}}");
                    @php

                        $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
                        $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
                        $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
                        $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
                        $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");
                        $user_agent = $_SERVER['HTTP_USER_AGENT']; 

                        if($iPod || $iPhone || $iPad){
                            

                                if (stripos($_SERVER['HTTP_USER_AGENT'],"safari") && !stripos($_SERVER['HTTP_USER_AGENT'],"CriOS")) 
                                {
                                    @endphp
                                        var newWin = window.frames["printf"];

                                        setTimeout(function(){
                                            newWin.document.write(response);
                                            newWin.document.close();
                                            
                                            @php
                                                    Session::forget('Sess_iSlipId');
                                                    Session::forget('Sess_iCaseId');
                                            @endphp
                                        },500)
                                    @php

                                }else{

                                    @endphp
                                        const wnd = window.open('about:blank', '', '_blank, alwaysRaised=yes');
                                        wnd.document.write(response);
                                        wnd.focus();

                                        setTimeout(function(){
                                            wnd.print();
                                            setTimeout(function(){
                                                wnd.close();
                                            },500)

                                            @php
                                                    Session::forget('Sess_iSlipId');
                                                    Session::forget('Sess_iCaseId');
                                            @endphp
                                               
                                        },500)
                                    @php
                                }

                        }else{ @endphp
                            var newWin = window.frames["printf"];

                            setTimeout(function(){
                                newWin.document.write(response);
                                newWin.document.close();

                                 @php
                                        Session::forget('Sess_iSlipId');
                                        Session::forget('Sess_iCaseId');
                                @endphp
                                
                            },500)
                            @php
                        }


                    @endphp
                }
            });
        }
    }
    // $('#iOfficeId').on('change', function(e) {
    //     var iOfficeId = $("#iOfficeId").val();
    //     $.ajax({
    //         url: "{{route('admin.labcase.getofficedoc') }}",
    //         type: "POST",
    //         data: {
    //             iOfficeId: iOfficeId,
    //             "_token": "{{ csrf_token() }}",
    //         },
    //         success: function(response) {
    //             $("#iDoctorId").html(response);
    //             $("#iDoctorId")[0].selectize.open();
    //         }
    //     });
    // });
    $(document).on('change', '#iOfficeId', function() {
        var iOfficeId = $("#iOfficeId").val();
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.labcase.ajax_listing_office')}}",
            type: "get",
            data: {
                iOfficeId: iOfficeId,
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#iDoctorId', function() {
        var iDoctorId = $("#iDoctorId").val();
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.labcase.ajax_listing_office')}}",
            type: "get",
            data: {
                iDoctorId: iDoctorId,
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $('#eStatus').on('change', function(e) {
        var eStatus = $("#eStatus").val();
        $("#ajax-loader").show();
        $.ajax({
            url: "{{route('admin.labcase.ajax_listing_office')}}",
            type: "get",
            data: {
                eStatus: eStatus,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $("#selectall").click(function() {
        if (this.checked) {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', true);
            });
        } else {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', false);
            });
        }
    });


    $("#keyword").keyup(function() {
        SearchAjaxCall();
    });

    $(".OnChangeSearch").change(function(){
        SearchAjaxCall();
    });

    function SearchAjaxCall()
    {
        var keyword = $("#keyword").val();
        var iLabId_list = $("#iLabId_list").val();
        var iDoctorId_list = $("#iDoctorId_list").val();
        var vLocation_list = $( "#vLocation_list option:selected" ).text();
        var eStatus_list = $( "#eStatus_list option:selected" ).text();
       
        $("#ajax-loader").show();
        $.ajax({
            url: "{{route('admin.labcase.ajax_listing_office')}}",
            type: "get",
            data: {
                keyword: keyword,
                iLabId_list: iLabId_list,
                iDoctorId_list: iDoctorId_list,
                vLocation_list: vLocation_list,
                eStatus_list: eStatus_list,
                action: 'search'
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
                
                var iOfficeId = $("#iLabId_list").val();
                $.ajax({
                    url: "{{route('admin.labcase.getdoc_2')}}",
                    type: "post",
                    data: {
                        isFilter:'Yes',
                        _token: '{{csrf_token()}}'
                    },
                    success: function(result) {
                        $('#iDoctorId_list').selectize()[0].selectize.destroy();
                        $('#iDoctorId_list').html(result);
                        $('#iDoctorId_list').selectize();
                    }
                });
            }
        });
    }
    $(document).on('click', '#delete_btn', function() {
        var id = [];

        $("input[name='Case_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            $.ajax({
                url: "{{route('admin.labcase.ajax_listing_office')}}",
                type: "get",
                data: {
                    id: id,
                    action: 'multiple_delete'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        }
    });
    $(document).on('click', '#delete', function() {
        var patient = $(this).data("patient");
        swal({
                title: "Are you sure delete "+patient+"?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");

                    $("#ajax-loader").show();
                    $.ajax({
                        url: "{{route('admin.labcase.ajax_listing_office')}}",
                        type: "get",
                        data: {
                            id: id,
                            action: 'delete'
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                            notification_error("Labcase Deleted Successfully");
                                setTimeout(function() {
                                   location.reload();
                                }, 1000);
                        }
                    });
                }
            })
    });
    /*
    $(document).on('click', '.sort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');


        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#ajax-loader").show();
        $.ajax({
            url: "{{route('admin.labcase.ajax_listing_office')}}",
            type: "get",
            data: {
                column: column,
                order,
                order,
                action: 'sort'
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    */

    $(document).on('click', '.ajax_page', function() {
        pages = $(this).data("pages");

        $("#table_record").html('');
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.labcase.ajax_listing_office')}}",
            type: "get",
            data: {
                pages: pages
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    // For Page pr limit start

    $(document).on('change','#page_limit',function()
    {
        limit_page = this.value;
        $("#table_record").html('');

        setTimeout(function(){
            $.ajax({
                url: "{{route('admin.labcase.ajax_listing_office')}}",
                type: "get",
                data:  {limit_page: limit_page}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            }, 500);
    });
    // For Page pr limit end
</script>

<script>
    $(document).on('change', '#iLabId', function() {
        var iLabId = $("#iLabId").val();
        $.ajax({
            url: "{{route('admin.labcase.getdoc_2')}}",
            type: "post",
            data: {
                iLabId: iLabId,
                _token: '{{csrf_token()}}'
            },
            success: function(result) {

                get_impressions("Upper");
                get_impressions("Lower");

                $('#iDoctorId').selectize()[0].selectize.destroy();
                $('#iDoctorId').html(result);
                $('#iDoctorId').selectize();
                $('#doctor_div').show();

                iDoctorId = $('#iDoctorId').val();
                if (iDoctorId != "") {
                    $('#vPatientName_div').show();
                }else{
                    $("#iDoctorId")[0].selectize.open();
                }

            }
        });
    });


    $(document).on('change', '#iDoctorId', function() {
        var iDoctorId = $("#iDoctorId").val();
        if (iDoctorId == "") {
            $('#vPatientName_div').hide();
        } else {
            $('#vPatientName_div').show();
            $('#vPatientName').focus();
        }
    });

    function category_select_data(eType) {
        $.ajax({
            url: "{{route('admin.labcase.getcategory')}}",
            type: "get",
            data: {
                eType: eType,
                iLabId: $("#iLabId").val(),
                _token: '{{csrf_token()}}'
            },
            success: function(result) {

                if (eType == "Upper") {
                    $('#iUpperCategoryId').selectize()[0].selectize.destroy();
                    $('#iUpperCategoryId').html(result);
                    $('#iUpperCategoryId').selectize();
                    
                    iUpperCategoryId = $("#iUpperCategoryId").val();
                    if (iUpperCategoryId.length != 0) {
                         upper_category_function();
                    }else{
                         $("#iUpperCategoryId")[0].selectize.open();
                    }


                } else {
                    $('#iLowerCategoryId').selectize()[0].selectize.destroy();
                    $('#iLowerCategoryId').html(result);
                    $('#iLowerCategoryId').selectize();
                    iLowerCategoryId = $("#iLowerCategoryId").val();
                    if (iLowerCategoryId.length != 0) {
                         lower_category_function();
                    }else{
                         $("#iLowerCategoryId")[0].selectize.open();
                    }

                }

            }
        });
    }

    function toothbrand_select_data(eType) {
        $.ajax({
            url: "{{route('admin.labcase.gettoothbrand')}}",
            type: "get",
            data: {
                iLabId: $("#iLabId").val(),
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                if (eType == "Upper") {
                    $('#upper_tsbrand').selectize()[0].selectize.destroy();
                    $('#upper_tsbrand').html(result);
                    $('#upper_tsbrand').selectize();

                    // alert();
                    if ($("#lower_teethshade_stage").val() == "Yes") {
                        if ($("#lower_tsbrand").val() != "") {
                            $('#upper_tsbrand').data('selectize').setValue($("#lower_tsbrand").val());
                        }
                    } else if ($("#upper_tsbrand").val() != "") {
                        upper_tsbrand_change();
                    }else{
                        $("#upper_tsbrand")[0].selectize.open();
                    }

                } else {
                    $('#lower_tsbrand').selectize()[0].selectize.destroy();
                    $('#lower_tsbrand').html(result);
                    $('#lower_tsbrand').selectize();

                    if ($("#upper_teethshade_stage").val() == "Yes") {
                        if ($("#upper_tsbrand").val() != "") {
                            $('#lower_tsbrand').data('selectize').setValue($("#upper_tsbrand").val());
                        }
                    } else if ($("#lower_tsbrand").val() != "") {
                        lower_tsbrand_change();
                    }else{
                        $("#lower_tsbrand")[0].selectize.open();
                    }

                }
            }
        });
    }

    function gumbrand_select_data(eType) {

        if (eType == "Upper") {
            iProductId = $("#iUpperProductId").val();
        } else {
            iProductId = $("#iLowerProductId").val();
        }

        $.ajax({
            url: "{{route('admin.labcase.getgumbrand')}}",
            type: "get",
            data: {
                iProductId: iProductId,
                iLabId: $("#iLabId").val(),
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                if (eType == "Upper") {
                    $('#upper_gsbrand').selectize()[0].selectize.destroy();
                    $('#upper_gsbrand').html(result);
                    $('#upper_gsbrand').selectize();

                    if ($("#lower_gumshade_stage").val() == "Yes") {
                        if ($("#lower_gsbrand").val() != "") {
                            $('#upper_gsbrand').data('selectize').setValue($("#lower_gsbrand").val());
                        } else {
                            upper_gsbrand_changes();
                        }
                    } else if ($("#upper_gsbrand").val() != "") {
                        upper_gsbrand_changes();
                    }else{
                        $("#upper_gsbrand")[0].selectize.open();
                    }

                } else {
                    $('#lower_gsbrand').selectize()[0].selectize.destroy();
                    $('#lower_gsbrand').html(result);
                    $('#lower_gsbrand').selectize();

                    if ($("#upper_gumshade_stage").val() == "Yes") {
                        if ($("#upper_gsbrand").val() != "") {
                            $('#lower_gsbrand').data('selectize').setValue($("#upper_gsbrand").val());
                        } else {
                            lower_gsbrand_changes();
                        }
                    } else if ($("#lower_gsbrand").val() != "") {
                        lower_gsbrand_changes();
                    }else{
                        $("#lower_gsbrand")[0].selectize.open();
                    }

                }
            }
        });
    }


    $(document).on('change', '#iUpperCategoryId', function() {
        upper_category_function();
    });

    function upper_category_function() {
        var iUpperCategoryId = $("#iUpperCategoryId").val();
        if (iUpperCategoryId.length > 0) {
            $.ajax({
                url: "{{route('admin.labcase.getproduct')}}",
                type: "post",
                data: {
                    iCategoryId: iUpperCategoryId,
                    eType: "Upper",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {
                    $('#iUpperProductId').selectize()[0].selectize.destroy();
                    $('#iUpperProductId').html(result);
                    $('#iUpperProductId').selectize();
                    $('#product_div').show();
                    $('#product_lbl').show();
                    $('#product_upper_div').show();

                    iUpperProductId = $("#iUpperProductId").val();
                    if (iUpperProductId.length != 0) {
                        upper_product_change();
                    }else{
                        $("#iUpperProductId")[0].selectize.open();
                    }

                }
            });
        }
    }

    // function casepan_number(etype) {
    //     if (etype == "Upper" &&  $("#upper_casepan_category").val() == "Yes") {
    //         iCategoryId = $("#iUpperCategoryId").val();
    //     } else {
    //         iCategoryId = $("#iLowerCategoryId").val();
    //     }
    //     $.ajax({
    //         url: "{{route('admin.labcase.getcasepan')}}",
    //         type: "GET",
    //         dataType: "json",
    //         data: {
    //             iCategoryId: iCategoryId,
    //             _token: '{{csrf_token()}}'
    //         },
    //         success: function(response) {
    //             color = response.color;
    //             data = response.data;
    //             vNumber = data.vNumber;

    //             $('#case_pan_number').text(vNumber);
    //             // $('#case_pan_number').css('background-color': "' + color + '" );

    //         }
    //     });
    // }

    $(document).on('change', '#iLowerCategoryId', function() {
        lower_category_function();
    });

    function lower_category_function() {
        var iLowerCategoryId = $("#iLowerCategoryId").val();
        if (iLowerCategoryId.length > 0) {
            $.ajax({
                url: "{{route('admin.labcase.getproduct')}}",
                type: "post",
                data: {
                    iCategoryId: iLowerCategoryId,
                    eType: "Lower",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {
                    $('#iLowerProductId').selectize()[0].selectize.destroy();
                    $('#iLowerProductId').html(result);
                    $('#iLowerProductId').selectize();
                    $('#product_div').show();
                    $('#product_lbl').show();
                    $('#product_lower_div').show();

                    iLowerProductId = $("#iLowerProductId").val();
                    if (iLowerProductId.length != 0) {
                        lower_product_change();
                    }else{
                        $("#iLowerProductId")[0].selectize.open();
                    }
                }
            });
        }
    }

    function hide_show_upper_stages() {

        var upper_Impression_stage = $("#upper_Impression_stage").val();
        var upper_extraction_stage = $("#upper_extraction_stage").val();
        var upper_teethshade_stage = $("#upper_teethshade_stage").val();
        var upper_gumshade_stage = $("#upper_gumshade_stage").val();

        if (upper_teethshade_stage == 'Yes') {

            /* Teeth Shade */
            $('#teeth_shades_div').show();
            $('#teethshade_lbl').show();
            $('#upper_tsbrand_div').show();
            $("#upper_ts_select_div").hide();

            toothbrand_select_data("Upper");
            // upper_tsbrand_change();

            /* Teeth Shade */

            /* Gum Shade */
            // $('#gum_shades_div').hide();
            if ($("#lower_gumshade_stage").val() != "Yes") {
                $('#gum_shades_div').hide();
            }
            $('#upper_gsbrand_div').hide();
            /* Gum Shade */


            /* Impression */
            $('#upper_impression_div').hide();
            if ($("#lower_Impression_stage").val() != "Yes") {
                $('#impression_div').hide();
                $('#lower_impression_div').hide();
            } else if ($("#lower_Impression_stage").val() == "Yes") {
                $('#upper_impression_div').show();
            }
            /* Impression */

            /* Extraction */

            $('#upper_extraction').hide();
            if ($("#lower_extraction_stage").val() != "Yes") {
                $('#lower_extraction').hide();
            }
            //  else if ($("#lower_extraction_stage").val() == "Yes") {
            //     $('#upper_extraction').show();
            // }
            /* Extraction */


        } else if (upper_gumshade_stage == 'Yes') {
            /* Teeth Shade */
            if ($("#lower_teethshade_stage").val() != "Yes") {
                $('#teeth_shades_div').hide();
            }
            $('#upper_tsbrand_div').hide();
            /* Teeth Shade */

            /* Gum Shade */
            $('#gum_shades_div').show();
            $('#gumshade_lbl').show();
            $('#upper_gsbrand_div').show();
            $("#upper_gs_select_div").hide();

            gumbrand_select_data("Upper");
            // upper_gsbrand_changes();

            /* Gum Shade */

            /* Impression */
            $('#upper_impression_div').hide();
            if ($("#lower_Impression_stage").val() != "Yes") {
                $('#impression_div').hide();
                $('#lower_impression_div').hide();
            } else if ($("#lower_Impression_stage").val() == "Yes") {
                $('#upper_impression_div').show();
            }
            /* Impression */

            /* Extraction */
            $('#upper_extraction').hide();
            if ($("#lower_extraction_stage").val() != "Yes") {
                $('#lower_extraction').hide();
            }
            //  else if ($("#lower_extraction_stage").val() == "Yes") {
            //     $('#upper_extraction').show();
            // }
            /* Extraction */

        } else if (upper_Impression_stage == 'Yes') {


            /* Teeth Shade */
            if ($("#lower_teethshade_stage").val() != "Yes") {
                $('#teeth_shades_div').hide();
            }
            $('#upper_tsbrand_div').hide();
            /* Teeth Shade */

            /* Gum Shade */
            if ($("#lower_gumshade_stage").val() != "Yes") {
                $('#gum_shades_div').hide();
            }
            $('#upper_gsbrand_div').hide();
            /* Gum Shade */

            /* Impression */
            $('#impression_div').show();
            $('#impression_lbl').show();
            $('#upper_impression_div').show();
            $("#upper_impression_dropdown").addClass("open");
            // $('#lower_impression_div').show();
            /* Impression */

            /* Extraction */

            $('#upper_extraction').hide();
            if ($("#lower_extraction_stage").val() != "Yes") {
                $('#lower_extraction').hide();
            }
            //  else if ($("#lower_extraction_stage").val() == "Yes") {
            //     $('#upper_extraction').show();
            // }
            /* Extraction */

        } else if (upper_extraction_stage == 'Yes') {

            /* Teeth Shade */
            // $('#teeth_shades_div').hide();
            if ($("#lower_teethshade_stage").val() != "Yes") {
                $('#teeth_shades_div').hide();
            }

            $('#upper_tsbrand_div').hide();
            /* Teeth Shade */

            /* Gum Shade */
            // $('#gum_shades_div').hide();
            if ($("#lower_gumshade_stage").val() != "Yes") {
                $('#gum_shades_div').hide();
            }
            $('#upper_gsbrand_div').hide();
            /* Gum Shade */

            /* Impression */
            $('#upper_impression_div').hide();
            if ($("#lower_Impression_stage").val() != "Yes") {
                $('#impression_div').hide();
                $('#lower_impression_div').hide();
            } else if ($("#lower_Impression_stage").val() == "Yes") {
                $('#upper_impression_div').show();
            }
            /* Impression */

            /* Extraction */

            upper_extraction_inner_hideshow();
            upper_extraction_validation();
            $('#upper_extraction').show();
            /* Extraction */
        }
    }

    $(document).on('click', '#upper_addons', function() {
        if ($("#upper_model").hasClass("d-none")) {
            $('#upper_model').removeClass("d-none");
            $.ajax({
                url: "{{route('admin.labcase.get_addons_category')}}",
                type: "post",
                data: {
                    iLabId: $("#iLabId").val(),
                    eType: "Upper",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {
                    $('#upper_category_addons').selectize()[0].selectize.destroy();
                    $('#upper_category_addons').html(result);
                    $('#upper_category_addons').selectize();
                }
            });
        } else {
            $('#upper_model').addClass("d-none");
        }
    });

    $(document).on('click', '#lower_addons', function() {
        if ($("#lower_model").hasClass("d-none")) {
            $('#lower_model').removeClass("d-none");
            $.ajax({
                url: "{{route('admin.labcase.get_addons_category')}}",
                type: "post",
                data: {
                    iLabId: $("#iLabId").val(),
                    eType: "Lower",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {
                    $('#lower_category_addons').selectize()[0].selectize.destroy();
                    $('#lower_category_addons').html(result);
                    $('#lower_category_addons').selectize();
                }
            });
        } else {
            $('#lower_model').addClass("d-none");
        }
    });


    $(document).on('change', '#iUpperStageId', function() {
        upper_stage_changes();
    });


    function delivery_date(etype) {
        if (etype == "upper") {
            iProductStageId = $("#iUpperStageId").val();
            iCategoryId = $("#iUpperCategoryId").val();
        } else {
            iProductStageId = $("#iLowerStageId").val();
            iCategoryId = $("#iLowerCategoryId").val();
        }
        $.ajax({
            url: "{{route('admin.labcase.pickupdays')}}",
            type: "GET",
            dataType: "json",
            data: {
                iProductStageId: iProductStageId,
                iCategoryId: iCategoryId,
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
                if (etype == "upper") {
                    $("#upper_delivery_date").val(response.del_date);
                    $("#upper_pickup_date").val(response.pic_date);
                    $("#upper_delivery_time").val(response.delivery_time);
                } else {
                    $("#lower_delivery_date").val(response.del_date);
                    $("#lower_pickup_date").val(response.pic_date);
                    $("#lower_delivery_time").val(response.delivery_time);
                }

                upper_delivery_date = $("#upper_delivery_date").val();
                upper_pickup_date = $("#upper_pickup_date").val();
                upper_delivery_time = $("#upper_delivery_time").val();

                lower_delivery_date = $("#lower_delivery_date").val();
                lower_pickup_date = $("#lower_pickup_date").val();
                lower_delivery_time = $("#lower_delivery_time").val();

                if (upper_delivery_date.length != 0 && lower_delivery_date.length == 0) {
                    $("#final_del_days").text($("#upper_delivery_date").val());
                    $("#final_pic_date").text($("#upper_pickup_date").val());
                    $("#final_del_time").text(tConvert($("#upper_delivery_time").val()));

                    $("#upper_casepan_category").val("Yes");
                } else if (upper_delivery_date.length == 0 && lower_delivery_date.length != 0) {
                    $("#final_del_days").text($("#lower_delivery_date").val());
                    $("#final_pic_date").text($("#lower_pickup_date").val());
                    $("#final_del_time").text(tConvert($("#lower_delivery_time").val()));

                    $("#lower_casepan_category").val("Yes");
                } else if (upper_delivery_date.length != 0 && lower_delivery_date.length != 0) {
                    if (upper_delivery_date > lower_delivery_date) {
                        $("#final_del_days").text($("#upper_delivery_date").val());
                        // $("#final_del_days").text("---");
                        $("#final_del_time").text(tConvert($("#upper_delivery_time").val()));
                        $("#final_pic_date").text($("#upper_pickup_date").val());

                        $("#upper_casepan_category").val("Yes");

                    } else if (upper_delivery_date < lower_delivery_date) {
                        $("#final_del_days").text($("#lower_delivery_date").val());
                        // $("#final_del_days").text("---");
                        $("#final_pic_date").text($("#lower_pickup_date").val());
                        $("#final_del_time").text(tConvert($("#lower_delivery_time").val()));

                        $("#lower_casepan_category").val("Yes");

                    } else if (upper_delivery_date == lower_delivery_date) {
                        $("#final_del_days").text($("#upper_delivery_date").val());
                        $("#final_del_time").text(tConvert($("#upper_delivery_time").val()));
                        $("#final_pic_date").text($("#upper_pickup_date").val());

                        $("#upper_casepan_category").val("Yes");

                    }
                } else if (upper_delivery_date.length == 0 && lower_delivery_date.length == 0) {
                    $("#final_del_days").text("---");
                    $("#final_pic_date").text("---");
                    $("#final_del_time").text("---");
                }


                if ($("#final_del_days").text() != "---" && $("#final_del_days").text() != "") {
                    formatted_delivery_date_array = $("#final_del_days").text().split("-");
                    formatted_delivery_date = formatted_delivery_date_array[1] + "/" + formatted_delivery_date_array[2] + "/" + formatted_delivery_date_array[0];
                    $("#final_del_days").text(formatted_delivery_date);
                }

                formatted_pickup_date_array = $("#final_pic_date").text().split("-");
                formatted_pickup_date = formatted_pickup_date_array[1] + "/" + formatted_pickup_date_array[2] + "/" + formatted_pickup_date_array[0];
                $("#final_pic_date").text(formatted_pickup_date);



                upper_formatted_delivery_date_array = upper_delivery_date.split("-");
                lower_formatted_delivery_date_array = lower_delivery_date.split("-");

                upper_formatted_pickup_date_array = upper_pickup_date.split("-");
                lower_formatted_pickup_date_array = lower_pickup_date.split("-");

                upper_formatted_delivery_date = upper_formatted_delivery_date_array[1] + "/" + upper_formatted_delivery_date_array[2] + "/" + upper_formatted_delivery_date_array[0];
                lower_formatted_delivery_date = lower_formatted_delivery_date_array[1] + "/" + lower_formatted_delivery_date_array[2] + "/" + lower_formatted_delivery_date_array[0];

                // upper_formatted_pickup_date = upper_formatted_pickup_date_array[1]+"/"+upper_formatted_pickup_date_array[2]+"/"+upper_formatted_pickup_date_array[0];
                // lower_formatted_pickup_date = lower_formatted_pickup_date_array[1]+"/"+lower_formatted_pickup_date_array[2]+"/"+lower_formatted_pickup_date_array[0];

                $("#delivery_div").show();
                $("#upper_div_date_lable").text(upper_formatted_delivery_date);
                $("#lower_div_date_lable").text(lower_formatted_delivery_date);

            }
        });
    }

    function rush_date(etype) {
        if (etype == "upper") {
            iProductStageId = $("#iUpperStageId").val();
            iCategoryId = $("#iUpperCategoryId").val();
        } else {
            iProductStageId = $("#iLowerStageId").val();
            iCategoryId = $("#iLowerCategoryId").val();
        }
        $.ajax({
            url: "{{route('admin.labcase.rushdate')}}",
            type: "GET",
            data: {
                iProductStageId: iProductStageId,
                iCategoryId: iCategoryId,
                etype: etype,
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
                console.log(response);
                if (etype == "upper") {
                    $("#upper_rush_selection_html").html(response)
                    if ($('#Upper_rush_exist').length)
                    {
                        $("#upper_rush_date_stage").val("Yes");
                    }
                    else
                    {
                        $("#upper_rush_date_stage").val("No");
                    }

                } else {
                    $("#lower_rush_selection_html").html(response);
                    if ($('#Lower_rush_exist').length)
                    {
                        $("#lower_rush_date_stage").val("Yes");
                    }
                    else
                    {
                        $("#lower_rush_date_stage").val("No");
                    }
                    
                }
            }
        });
    }
    function get_rush_date(etype,get_only_date='no') {
        if (etype == "upper") {
            iProductStageId = $("#iUpperStageId").val();
            iCategoryId = $("#iUpperCategoryId").val();
        } else {
            iProductStageId = $("#iLowerStageId").val();
            iCategoryId = $("#iLowerCategoryId").val();
        }
       
        $.ajax({
            url: "{{route('admin.labcase.rushdate')}}",
            type: "GET",
            data: {
                iProductStageId: iProductStageId,
                iCategoryId: iCategoryId,
                get_only_date:get_only_date,
                etype: etype,
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
                if (etype == "upper") {
                    $("#upper_rush_selection_html").html(response)
                    if ($('#Upper_rush_exist').length)
                    {
                        $("#upper_rush_date_stage").val("Yes");
                    }
                    else
                    {
                        $("#upper_rush_date_stage").val("No");
                    }

                } else {
                    $("#lower_rush_selection_html").html(response);
                    if ($('#Lower_rush_exist').length)
                    {
                        $("#lower_rush_date_stage").val("Yes");
                    }
                    else
                    {
                        $("#lower_rush_date_stage").val("No");
                    }
                    
                }
            }
        });
    }


    $(document).on('click', '#save_rush', function() {


        if (!$("input:radio[name='upper_rush']").is(":checked") && !$("input:radio[name='lower_rush']").is(":checked")) {

            alert('Please Select Rush!');
        } else {
            if ($('#rush_alert').is(":checked")) {

                upper_rush_id = $('input[name="upper_rush"]:checked').val();
                $("#final_del_days").text("---");
                $("#upper_delivery_div").show();
                $("#upper_delivery_lbl").show();
                $("#delivery_lbl").show();

                
                if ($('input[name="upper_rush"]:checked').length != 0) {
                    $("#upper_div_date_lable").text($("#upper_rush_dd_" + upper_rush_id).text());
                    $("#upper_rush_date_stage").val("Yes");
                    $("#upper_rush_feesid").val(upper_rush_id);
                } else if(upper_formatted_delivery_date != "undefined/undefined/"){

                    $("#upper_div_date_lable").text(upper_formatted_delivery_date);
                }else{
                    $("#upper_div_date_lable").text("");
                }

                lower_rush_id = $('input[name="lower_rush"]:checked').val();
                $("#final_del_days").text("---");
                $("#lower_delivery_div").show();
                $("#lower_delivery_lbl").show();
                $("#delivery_lbl").show();

                if ($('input[name="lower_rush"]:checked').length != 0) {
                    $("#lower_div_date_lable").text($("#lower_rush_dd_" + lower_rush_id).text());
                    $("#lower_rush_date_stage").val("Yes");
                    $("#lower_rush_feesid").val(lower_rush_id);
                    $("#lower_rush_date_stage").val("Yes");
                } else if(lower_formatted_delivery_date != "undefined/undefined/" ){
                    $("#lower_div_date_lable").text(lower_formatted_delivery_date);
                    
                }else{
                    $("#lower_div_date_lable").text("");
                }

                lower_rush_date_stage = $("#lower_rush_date_stage").val();
                upper_rush_date_stage = $("#upper_rush_date_stage").val();

                if ($('input[name="lower_rush"]:checked').length != 0 && $('input[name="upper_rush"]:checked').length == 0 && $("#upper_rush_date_stage").val() != "No") {

                    swal({
                        title: "Select Upper Rush?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $('input[name="lower_rush"]:checked').each(function() {
                                var lower_rush = $(this).attr("data-date");
                                $("#upper_rush_selection_html").find("#upper_checked_"+lower_rush).trigger('click');
                            });
                            return false;
                        } else {
                            $(".btn_close_rush").trigger("click");
                        }
                    })

                }else if (upper_rush_date_stage == "No" && lower_rush_date_stage == "Yes"){
                    $(".btn_close_rush").trigger("click");
                }

               
                if ($('input[name="lower_rush"]:checked').length == 0 && $('input[name="upper_rush"]:checked').length != 0 && $("#lower_rush_date_stage").val() != "No") {
                    swal({
                        title: "Select Lower Rush?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $('input[name="upper_rush"]:checked').each(function() {
                                var upper_rush = $(this).attr("data-date");
                                $("#lower_rush_selection_html").find("#lower_checked_"+upper_rush).trigger('click');
                            });
                            return false;
                        } else {
                            $(".btn_close_rush").trigger("click");
                        }
                    })
                }else if (lower_rush_date_stage == "No" && upper_rush_date_stage == "Yes"){
                    $(".btn_close_rush").trigger("click");
                }

                // if(lower_rush_date_stage == "No" && upper_rush_date_stage == "Yes"){
                //     $(".btn_close_rush").trigger("click");

                // }
                // if(lower_rush_date_stage == "Yes" && upper_rush_date_stage == "No"){
                //     $(".btn_close_rush").trigger("click");

                // }
                if ($('input[name="lower_rush"]:checked').length != 0 && $('input[name="upper_rush"]:checked').length != 0) {
                    $(".btn_close_rush").trigger("click");
                }
                return false;
            } else {
                return false;
            }
        }
    });

    $(document).on("click", "#rush_alert", function() {

        if ($('#rush_alert').is(":checked")) {
            $("#rush_warning").addClass("active");
            $("#rush_alert").val("Yes");

        } else {
            $("#rush_warning").removeClass("active");
            $("#rush_alert").val("No");
        }

    });

    $(document).on("click", "#upper_rush_delete", function() {
        $(".upper_radio_click").prop("checked", false);
        $("#upper_rush_feesid").val("");
        delivery_date("upper");
        // $(".btn_close_rush").trigger("click");
    });

    $(document).on("click", "#lower_rush_delete", function() {
        $(".lower_radio_click").prop("checked", false);
        $("#lower_rush_feesid").val("");
        delivery_date("lower");
        // $(".btn_close_rush").trigger("click");

    });

    $(document).on("click", ".cancel_rush", function() {
        $("#rush_model").removeClass("show d-block");
    });

    function upper_stage_changes() {

        var iUpperStageId = $("#iUpperStageId").val();


        if (iUpperStageId.length > 0) {
            delivery_date("upper");
            rush_date("upper");

            $.ajax({
                url: "{{route('admin.labcase.getshadetype')}}",
                type: "post",
                dataType: "json",
                data: {
                    iProductStageId: iUpperStageId,
                    eType: "Upper",
                    _token: '{{csrf_token()}}'
                },
                success: function(response) {

                    if (response[0].eImpressions == 'Yes') {
                        upper_reset_Impression();
                        $("#upper_Impression_stage").val("Yes");
                    } else {
                        $("#upper_Impression_stage").val("No");
                    }

                    if (response[0].eExtractions == 'Yes') {
                        $("#upper_extraction_stage").val("Yes");

                        $("#upper_eTeethInMouth").val(response[0].eTeethInMouth);
                        $("#upper_eTeethInDefault").val(response[0].eTeethInDefault);
                        $("#upper_eTeethInRequird").val(response[0].eTeethInRequird);
                        $("#upper_eTeethInOptional").val(response[0].eTeethInOptional);

                        $("#upper_eMessingTeeth").val(response[0].eMessingTeeth);
                        $("#upper_eMessingDefault").val(response[0].eMessingDefault);
                        $("#upper_eMessingRequird").val(response[0].eMessingRequird);
                        $("#upper_eMessingOptional").val(response[0].eMessingOptional);

                        $("#upper_eExtractDelivery").val(response[0].eExtractDelivery);
                        $("#upper_eExtractDefault").val(response[0].eExtractDefault);
                        $("#upper_eExtractRequird").val(response[0].eExtractRequird);
                        $("#upper_eExtractOptional").val(response[0].eExtractOptional);

                        $("#upper_eExtracted").val(response[0].eExtracted);
                        $("#upper_eExtractedDefault").val(response[0].eExtractedDefault);
                        $("#upper_eExtractedRequird").val(response[0].eExtractedRequird);
                        $("#upper_eExtractedOptional").val(response[0].eExtractedOptional);

                        $("#upper_eFixorAdd").val(response[0].eFixorAdd);
                        $("#upper_eFixorAddDefault").val(response[0].eFixorAddDefault);
                        $("#upper_eFixorAddRequird").val(response[0].eFixorAddRequird);
                        $("#upper_eFixorAddOptional").val(response[0].eFixorAddOptional);

                        $("#upper_eClasps").val(response[0].eClasps);
                        $("#upper_eClaspsDefault").val(response[0].eClaspsDefault);
                        $("#upper_eClaspsRequird").val(response[0].eClaspsRequird);
                        $("#upper_eClaspsOptional").val(response[0].eClaspsOptional);

                        if (response[0].eTeethInDefault == "Yes") {
                            upper_default_extraction_var = "upper_eTeethInDefault";
                        } else if (response[0].eMessingDefault == "Yes") {
                            upper_default_extraction_var = "upper_eMessingDefault";
                        } else if (response[0].eExtractDefault == "Yes") {
                            upper_default_extraction_var = "upper_eExtractDefault";
                        } else if (response[0].eExtractedDefault == "Yes") {
                            upper_default_extraction_var = "upper_eExtractedDefault";
                        } else if (response[0].eFixorAddDefault == "Yes") {
                            upper_default_extraction_var = "upper_eFixorAddDefault";
                        } else if (response[0].eClaspsDefault == "Yes") {
                            upper_default_extraction_var = "upper_eClaspsDefault";
                        } else {
                            upper_default_extraction_var = "none";
                        }

                        upper_default_extraction(upper_default_extraction_var);

                        // upper_extraction_inner_hideshow();
                        // upper_extraction_validation();

                    } else {
                        $("#upper_extraction_stage").val("No");

                        $("#upper_eTeethInMouth").val("No");
                        $("#upper_eTeethInDefault").val("No");
                        $("#upper_eTeethInRequird").val("No");
                        $("#upper_eTeethInOptional").val("No");

                        $("#upper_eMessingTeeth").val("No");
                        $("#upper_eMessingDefault").val("No");
                        $("#upper_eMessingRequird").val("No");
                        $("#upper_eExtractOptional").val("No");

                        $("#upper_eExtractDelivery").val("No");
                        $("#upper_eExtractDefault").val("No");
                        $("#upper_eExtractRequird").val("No");
                        $("#upper_eMessingOptional").val("No");

                        $("#upper_eExtracted").val("No");
                        $("#upper_eExtractedDefault").val("No");
                        $("#upper_eExtractedRequird").val("No");
                        $("#upper_eExtractedOptional").val("No");

                        $("#upper_eFixorAdd").val("No");
                        $("#upper_eFixorAddDefault").val("No");
                        $("#upper_eFixorAddRequird").val("No");
                        $("#upper_eFixorAddOptional").val("No");

                        $("#upper_eClasps").val("No");
                        $("#upper_eClaspsDefault").val("No");
                        $("#upper_eClaspsRequird").val("No");
                        $("#upper_eClaspsOptional").val("No");

                        upper_default_extraction_var = "none";

                    }
                    if (response[0].eTeethShades == 'Yes') {
                        $("#upper_teethshade_stage").val("Yes");
                    } else {
                        $("#upper_teethshade_stage").val("No");
                    }
                    if (response[0].eGumShades == 'Yes') {
                        $("#upper_gumshade_stage").val("Yes");
                    } else {
                        $("#upper_gumshade_stage").val("No");
                        $("upper_addons_model_div").hide();
                    }
                    $("#upper_category_addons_div").html("");
                    $("#upper_addons_model_div").hide();
                    $('#upper_status_div').hide();

                    if ($("#iLowerStageId").val() == "") {
                        $("#category_addons_div").hide();
                        $("#stage_status_div").hide();
                    }

                    /*Opposite Side*/
                    if (response[0].eOpposingImpressions == "Yes") {
                        $("#lower_eOpposingImpressions").val("Yes");
                    } else {
                        $("#lower_eOpposingImpressions").val("No");
                    }

                    if (response[0].eOpposingExtractions == "Yes") {

                        $("#lower_eOpposingExtractions").val("Yes");

                        $("#lower_eOpposingTeethInMouth").val(response[0].eOpposingTeethInMouth);
                        $("#lower_eOpposingTeethInDefault").val(response[0].eOpposingTeethInDefault);

                        $("#lower_eOpposingMessingTeeth").val(response[0].eOpposingMessingTeeth);
                        $("#lower_eOpposingMessingDefault").val(response[0].eOpposingMessingDefault);

                        $("#lower_eOpposingExtractDelivery").val(response[0].eOpposingExtractDelivery);
                        $("#lower_eOpposingExtractDefault").val(response[0].eOpposingExtractDefault);

                        $("#lower_eOpposingExtracted").val(response[0].eOpposingExtracted);
                        $("#lower_eOpposingExtractedDefault").val(response[0].eOpposingExtractedDefault);

                        $("#lower_eOpposingFixorAdd").val(response[0].eOpposingFixorAdd);
                        $("#lower_eOpposingFixorAddDefault").val(response[0].eOpposingFixorAddDefault);

                        $("#lower_eOpposingClasps").val(response[0].eOpposingClasps);
                        $("#lower_eOpposingClaspsDefault").val(response[0].eOpposingClaspsDefault);


                        if (response[0].eOpposingTeethInDefault == "Yes") {
                            lower_opposite_default_extraction_var = "lower_opposite_eTeethInDefault";
                        } else if (response[0].eOpposingMessingDefault == "Yes") {
                            lower_opposite_default_extraction_var = "lower_opposite_eMessingDefault";
                        } else if (response[0].eOpposingExtractDefault == "Yes") {
                            lower_opposite_default_extraction_var = "lower_opposite_eExtractDefault";
                        } else if (response[0].eOpposingExtractedDefault == "Yes") {
                            lower_opposite_default_extraction_var = "lower_opposite_eExtractedDefault";
                        } else if (response[0].eOpposingFixorAddDefault == "Yes") {
                            lower_opposite_default_extraction_var = "lower_opposite_eFixorAddDefault";
                        } else if (response[0].eOpposingClaspsDefault == "Yes") {
                            lower_opposite_default_extraction_var = "lower_opposite_eClaspsDefault";
                        } else {
                            lower_opposite_default_extraction_var = "none";
                        }

                        if ($("#lower_extraction_stage").val() != "Yes") {
                            lower_opposite_default_extraction(lower_opposite_default_extraction_var);
                        }


                    } else {
                        $("#lower_eOpposingExtractions").val("No");

                        $("#lower_eOpposingTeethInMouth").val("No");
                        $("#lower_eOpposingTeethInDefault").val("No");

                        $("#lower_eOpposingMessingTeeth").val("No");
                        $("#lower_eOpposingMessingDefault").val("No");

                        $("#lower_eOpposingExtractDelivery").val("No");
                        $("#lower_eOpposingExtractDefault").val("No");

                        $("#lower_eOpposingExtracted").val("No");
                        $("#lower_eOpposingExtractedDefault").val("No");

                        $("#lower_eOpposingFixorAdd").val("No");
                        $("#lower_eOpposingFixorAddDefault").val("No");

                        $("#lower_eOpposingClasps").val("No");
                        $("#lower_eOpposingClaspsDefault").val("No");

                        lower_opposite_default_extraction_var = "none";
                    }
                    /*Opposite Side*/

                    // upper_reset_Impression();

                    hide_show_upper_stages();
                    // casepan_number("Upper");

                }
            });
        }

    }


    function hide_show_lower_stages() {
        // return false;
        var lower_Impression_stage = $("#lower_Impression_stage").val();
        var lower_extraction_stage = $("#lower_extraction_stage").val();
        var lower_teethshade_stage = $("#lower_teethshade_stage").val();
        var lower_gumshade_stage = $("#lower_gumshade_stage").val();


        if (lower_teethshade_stage == 'Yes') {

            /* Teeth Shade */
            $('#teeth_shades_div').show();
            $('#teethshade_lbl').show();
            $('#lower_tsbrand_div').show();
            $("#lower_ts_select_div").hide();

            toothbrand_select_data("Lower");
            // lower_tsbrand_change()
            /* Teeth Shade */

            /* Gum Shade */
            if ($("#upper_gumshade_stage").val() != "Yes") {
                $('#gum_shades_div').hide();
            }
            $('#lower_gsbrand_div').hide();
            /* Gum Shade */

            /* Impression */
            $('#lower_impression_div').hide();
            if ($("#upper_Impression_stage").val() != "Yes") {
                $('#impression_div').hide();
                $('#upper_impression_div').hide();
            } else if ($("#upper_Impression_stage").val() == "Yes") {
                $('#lower_impression_div').show();
            }
            /* Impression */

            /* Extraction */
            $('#lower_extraction').hide();
            if ($("#upper_extraction_stage").val() != "Yes") {
                $('#upper_extraction').hide();
            }
            //  else if ($("#upper_extraction_stage").val() == "Yes") {
            //     $('#lower_extraction').show();
            // }
            /* Extraction */


        } else if (lower_gumshade_stage == 'Yes') {
            /* Teeth Shade */
            // $('#teeth_shades_div').hide();
            if ($("#upper_teethshade_stage").val() != "Yes") {
                $('#teeth_shades_div').hide();
            }
            $('#lower_tsbrand_div').hide();
            /* Teeth Shade */

            /* Gum Shade */
            $('#gum_shades_div').show();
            $('#gumshade_lbl').show();
            $('#lower_gsbrand_div').show();
            $("#lower_gs_select_div").hide();
            gumbrand_select_data("Lower");
            // lower_gsbrand_changes();
            /* Gum Shade */

            /* Impression */
            $('#lower_impression_div').hide();
            if ($("#upper_Impression_stage").val() != "Yes") {
                $('#impression_div').hide();
                $('#upper_impression_div').hide();
            } else if ($("#upper_Impression_stage").val() == "Yes") {
                $('#lower_impression_div').show();
            }
            /* Impression */

            /* Extraction */
            $('#lower_extraction').hide();
            if ($("#upper_extraction_stage").val() != "Yes") {
                $('#upper_extraction').hide();
            }
            //  else if ($("#upper_extraction_stage").val() == "Yes") {
            //     $('#lower_extraction').show();
            // }
            /* Extraction */

        } else if (lower_Impression_stage == 'Yes') {

            /* Teeth Shade */
            // $('#teeth_shades_div').hide();
            if ($("#upper_gumshade_stage").val() != "Yes") {
                $('#gum_shades_div').hide();
            }
            $('#lower_tsbrand_div').hide();
            /* Teeth Shade */

            /* Gum Shade */
            // $('#gum_shades_div').hide();
            if ($("#upper_gumshade_stage").val() != "Yes") {
                $('#gum_shades_div').hide();
            }
            $('#lower_gsbrand_div').hide();
            $('#lower_gsbrand_div').hide();
            /* Gum Shade */

            /* Impression */
            $('#impression_div').show();
            $('#impression_lbl').show();
            $('#lower_impression_div').show();
            $("#lower_impression_dropdown").addClass("open");
            // $('#upper_impression_div').show();
            /* Impression */

            /* Extraction */
            $('#lower_extraction').hide();
            if ($("#upper_extraction_stage").val() != "Yes") {
                $('#upper_extraction').hide();
            }
            //  else if ($("#upper_extraction_stage").val() == "Yes") {
            //     $('#lower_extraction').show();
            // }
            /* Extraction */


        } else if (lower_extraction_stage == 'Yes') {

            /* Teeth Shade */
            // $('#teeth_shades_div').hide();
            if ($("#upper_gumshade_stage").val() != "Yes") {
                $('#gum_shades_div').hide();
            }
            $('#lower_tsbrand_div').hide();
            /* Teeth Shade */

            /* Gum Shade */
            // $('#gum_shades_div').hide();
            if ($("#upper_gumshade_stage").val() != "Yes") {
                $('#gum_shades_div').hide();
            }
            $('#lower_gsbrand_div').hide();
            $('#lower_gsbrand_div').hide();
            /* Gum Shade */

            /* Impression */
            $('#lower_impression_div').hide();
            if ($("#upper_Impression_stage").val() != "Yes") {
                $('#impression_div').hide();
                $('#upper_impression_div').hide();
            } else if ($("#upper_Impression_stage").val() == "Yes") {
                $('#lower_impression_div').show();
            }
            /* Impression */

            /* Extraction */
            lower_extraction_inner_hideshow();
            lower_extraction_validation();
            $('#lower_extraction').show();
            /* Extraction */


        }

    }
    $(document).on('change', '#iLowerStageId', function() {
        lower_stage_change();
    });

    function lower_stage_change() {


        var iLowerStageId = $("#iLowerStageId").val();
        if (iLowerStageId.length > 0) {
            delivery_date("lower");
            rush_date("lower");
            $.ajax({
                url: "{{route('admin.labcase.getshadetype')}}",
                type: "post",
                dataType: "json",
                data: {
                    iProductStageId: iLowerStageId,
                    eType: "Lower",
                    _token: '{{csrf_token()}}'
                },
                success: function(response) {


                    if (response[0].eImpressions == 'Yes') {
                        lower_default_Impression_var = "lower_Impression_stage";
                    } else {
                        lower_default_Impression_var = "none";
                    }

                    if (response[0].eImpressions == 'Yes') {
                        $("#lower_Impression_stage").val("Yes");
                        lower_reset_Impression();
                    } else {
                        $("#lower_Impression_stage").val("No");

                    }
                    if (response[0].eExtractions == 'Yes') {
                        $("#lower_extraction_stage").val("Yes");

                        $("#lower_eTeethInMouth").val(response[0].eTeethInMouth);
                        $("#lower_eTeethInDefault").val(response[0].eTeethInDefault);
                        $("#lower_eTeethInRequird").val(response[0].eTeethInRequird);
                        $("#lower_eTeethInOptional").val(response[0].eTeethInOptional);

                        $("#lower_eMessingDefault").val(response[0].eMessingDefault);
                        $("#lower_eMessingRequird").val(response[0].eMessingRequird);
                        $("#lower_eMessingTeeth").val(response[0].eMessingTeeth);
                        $("#lower_eMessingOptional").val(response[0].eMessingOptional);

                        $("#lower_eExtractDefault").val(response[0].eExtractDefault);
                        $("#lower_eExtractDelivery").val(response[0].eExtractDelivery);
                        $("#lower_eExtractRequird").val(response[0].eExtractRequird);
                        $("#lower_eExtractOptional").val(response[0].eExtractOptional);

                        $("#lower_eExtracted").val(response[0].eExtracted);
                        $("#lower_eExtractedDefault").val(response[0].eExtractedDefault);
                        $("#lower_eExtractedRequird").val(response[0].eExtractedRequird);
                        $("#lower_eExtractedOptional").val(response[0].eExtractedOptional);

                        $("#lower_eFixorAdd").val(response[0].eFixorAdd);
                        $("#lower_eFixorAddDefault").val(response[0].eFixorAddDefault);
                        $("#lower_eFixorAddRequird").val(response[0].eFixorAddRequird);
                        $("#lower_eFixorAddOptional").val(response[0].eFixorAddOptional);

                        $("#lower_eClasps").val(response[0].eClasps);
                        $("#lower_eClaspsDefault").val(response[0].eClaspsDefault);
                        $("#lower_eClaspsRequird").val(response[0].eClaspsRequird);
                        $("#lower_eClaspsOptional").val(response[0].eClaspsOptional);


                        if (response[0].eTeethInDefault == "Yes") {
                            lower_default_extraction_var = "lower_eTeethInDefault";
                        } else if (response[0].eMessingDefault == "Yes") {
                            lower_default_extraction_var = "lower_eMessingDefault";
                        } else if (response[0].eExtractDefault == "Yes") {
                            lower_default_extraction_var = "lower_eExtractDefault";
                        } else if (response[0].eExtractedDefault == "Yes") {
                            lower_default_extraction_var = "lower_eExtractedDefault";
                        } else if (response[0].eFixorAddDefault == "Yes") {
                            lower_default_extraction_var = "lower_eFixorAddDefault";
                        } else if (response[0].eClaspsDefault == "Yes") {
                            lower_default_extraction_var = "lower_eClaspsDefault";
                        } else {
                            lower_default_extraction_var = "none";
                        }

                        lower_default_extraction(lower_default_extraction_var);

                    } else {
                        $("#lower_extraction_stage").val("No");

                        $("#lower_eTeethInMouth").val("No");
                        $("#lower_eTeethInDefault").val("No");
                        $("#lower_eTeethInRequird").val("No");
                        $("#lower_eTeethInOptional").val("No");

                        $("#lower_eMessingDefault").val("No");
                        $("#lower_eMessingRequird").val("No");
                        $("#lower_eMessingTeeth").val("No");
                        $("#lower_eMessingOptional").val("No");

                        $("#lower_eExtractDefault").val("No");
                        $("#lower_eExtractDelivery").val("No");
                        $("#lower_eExtractRequird").val("No");
                        $("#lower_eExtractOptional").val("No");

                        $("#lower_eExtracted").val("No");
                        $("#lower_eExtractedDefault").val("No");
                        $("#lower_eExtractedRequird").val("No");
                        $("#lower_eExtractedOptional").val("No");

                        $("#lower_eFixorAdd").val("No");
                        $("#lower_eFixorAddDefault").val("No");
                        $("#lower_eFixorAddRequird").val("No");
                        $("#lower_eFixorAddOptional").val("No");

                        $("#lower_eClasps").val("No");
                        $("#lower_eClaspsDefault").val("No");
                        $("#lower_eClaspsRequird").val("No");
                        $("#lower_eClaspsOptional").val("No");

                        lower_default_extraction_var = "none";

                    }
                    if (response[0].eTeethShades == 'Yes') {
                        $("#lower_teethshade_stage").val("Yes");

                    } else {
                        $("#lower_teethshade_stage").val("No");

                    }
                    if (response[0].eGumShades == 'Yes') {
                        $("#lower_gumshade_stage").val("Yes");

                    } else {
                        $("#lower_gumshade_stage").val("No");
                    }

                    $("#lower_category_addons_div").html("");
                    $("#lower_addons_model_div").hide();
                    $('#lower_status_div').hide();

                    if ($("#iUpperStageId").val() == "") {
                        $("#category_addons_div").hide();
                        $("#stage_status_div").hide();
                    }


                    /*Opposite Side*/
                    if (response[0].eOpposingImpressions == "Yes") {
                        $("#upper_eOpposingImpressions").val("Yes");
                    } else {
                        $("#upper_eOpposingImpressions").val("No");
                    }

                    if (response[0].eOpposingExtractions == "Yes") {

                        $("#upper_eOpposingExtractions").val("Yes");

                        $("#upper_eOpposingTeethInMouth").val(response[0].eOpposingTeethInMouth);
                        $("#upper_eOpposingTeethInDefault").val(response[0].eOpposingTeethInDefault);

                        $("#upper_eOpposingMessingTeeth").val(response[0].eOpposingMessingTeeth);
                        $("#upper_eOpposingMessingDefault").val(response[0].eOpposingMessingDefault);

                        $("#upper_eOpposingExtractDelivery").val(response[0].eOpposingExtractDelivery);
                        $("#upper_eOpposingExtractDefault").val(response[0].eOpposingExtractDefault);

                        $("#upper_eOpposingExtracted").val(response[0].eOpposingExtracted);
                        $("#upper_eOpposingExtractedDefault").val(response[0].eOpposingExtractedDefault);

                        $("#upper_eOpposingFixorAdd").val(response[0].eOpposingFixorAdd);
                        $("#upper_eOpposingFixorAddDefault").val(response[0].eOpposingFixorAddDefault);

                        $("#upper_eOpposingClasps").val(response[0].eOpposingClasps);
                        $("#upper_eOpposingClaspsDefault").val(response[0].eOpposingClaspsDefault);


                        if (response[0].eOpposingTeethInDefault == "Yes") {
                            upper_opposite_default_extraction_var = "lower_opposite_eTeethInDefault";
                        } else if (response[0].eOpposingMessingDefault == "Yes") {
                            upper_opposite_default_extraction_var = "upper_opposite_eMessingDefault";
                        } else if (response[0].eOpposingExtractDefault == "Yes") {
                            upper_opposite_default_extraction_var = "upper_opposite_eExtractDefault";
                        } else if (response[0].eOpposingExtractedDefault == "Yes") {
                            upper_opposite_default_extraction_var = "upper_opposite_eExtractedDefault";
                        } else if (response[0].eOpposingFixorAddDefault == "Yes") {
                            upper_opposite_default_extraction_var = "upper_opposite_eFixorAddDefault";
                        } else if (response[0].eOpposingClaspsDefault == "Yes") {
                            upper_opposite_default_extraction_var = "upper_opposite_eClaspsDefault";
                        } else {
                            upper_opposite_default_extraction_var = "none";
                        }

                        if ($("#upper_extraction_stage").val() != "Yes") {
                            upper_opposite_default_extraction(upper_opposite_default_extraction_var);
                        }



                    } else {
                        $("#upper_eOpposingExtractions").val("No");

                        $("#upper_eOpposingTeethInMouth").val("No");
                        $("#upper_eOpposingTeethInDefault").val("No");

                        $("#upper_eOpposingMessingTeeth").val("No");
                        $("#upper_eOpposingMessingDefault").val("No");

                        $("#upper_eOpposingExtractDelivery").val("No");
                        $("#upper_eOpposingExtractDefault").val("No");

                        $("#upper_eOpposingExtracted").val("No");
                        $("#upper_eOpposingExtractedDefault").val("No");

                        $("#upper_eOpposingFixorAdd").val("No");
                        $("#upper_eOpposingFixorAddDefault").val("No");

                        $("#upper_eOpposingClasps").val("No");
                        $("#upper_eOpposingClaspsDefault").val("No");
                    }
                    /*Opposite Side*/


                    // lower_reset_Impression();

                    hide_show_lower_stages();

                    // casepan_number("Lower");

                }
            });

        }
    }

    $(document).on('click', '#upper_impressions', function() {
   
       
        if ($("#upper_impression_dropdown").hasClass("open")) {
            // $("#upper_impression_dropdown").removeClass("open");
            $( "#upper_add_impressions_btn" ).trigger( "click" );
        } else {
            $("#upper_impression_dropdown").addClass("open");
            $("#upper_model").addClass("d-none");
        }
    });
    $(document).on('click', '#lower_impressions', function() {
        
        if ($("#lower_impression_dropdown").hasClass("open")) {
            $( "#lower_add_impressions_btn" ).trigger( "click" );
            // $("#lower_impression_dropdown").removeClass("open");
        } else {

            $("#lower_impression_dropdown").addClass("open");
            $("#lower_model").addClass("d-none");
        }
    });



    $(document).on('click', '#upper_add_impressions_btn', function() {

        upper_imps_error = "false";
        upper_impression_lbl_text = [];

        if ($("#upper_Impression_stage").val() == "Yes") {
            ImpressionId = [];
            $("input[name='upper_impression_checkbox[]']:checked").each(function() {

                ImpressionId.push($(this).val());

            });
            if (ImpressionId.length == 0) {
                $("#upper_impression_checkbox_error").show();
                $("#upper_impressions_warning").hide();
                upper_imps_error = "true";
                return false;
            } else {
                $("#upper_impressions_warning").hide();
                $("#upper_impression_checkbox_error").hide();
                $.each(ImpressionId, function(key, value) {
                    upper_imprss_val = $("#upper_impression_qty_" + value).val();
                    upper_imprss_text = $("#upper_imps_name_" + value).text();
                    if (upper_imprss_val == "" || upper_imprss_val == 0) {
                        $("#upper_impression_qty_error_" + value).show();
                        upper_imps_error = "true";
                        return false;
                    } else {
                        $("#upper_impression_qty_error_" + value).hide();
                        upper_impression_lbl_text.push(upper_imprss_val + "x" + upper_imprss_text);
                    }
                });
            }
        } else {
            ImpressionId = [];
            $("input[name='upper_impression_checkbox[]']:checked").each(function() {
                ImpressionId.push($(this).val());
            });
            if (ImpressionId.length == 0) {
                $("#upper_impressions_warning").show();
                upper_imps_error = "false";
            } else {
                $("#upper_impressions_warning").hide();
                $("#upper_impression_checkbox_error").hide();
                $.each(ImpressionId, function(key, value) {
                    upper_imprss_val = $("#upper_impression_qty_" + value).val();
                    upper_imprss_text = $("#upper_imps_name_" + value).text();
                    if (upper_imprss_val == "") {
                        $("#upper_impression_qty_error_" + value).show();
                        upper_imps_error = "true";
                        return false;
                    } else {
                        $("#upper_impression_qty_error_" + value).hide();
                        upper_impression_lbl_text.push(upper_imprss_val + "x" + upper_imprss_text);
                    }
                });
            }
        }

        if (upper_imps_error == "false") {
            // alert("ij");

            // $("#upper_impression_dropdown").removeClass("open");
            // $('#lower_extraction').show();

            upper_extraction_stage = $("#upper_extraction_stage").val();
            upper_teethshade_stage = $("#upper_teethshade_stage").val();
            upper_gumshade_stage = $("#upper_gumshade_stage").val();

            $("#upper_impressions").val(upper_impression_lbl_text.join(", "));
            $("#upper_impression_dropdown").removeClass("open");

            if ($("#lower_eOpposingImpressions").val() == "Yes" && $("#lower_Impression_stage").val() != "Yes") {
                $('#lower_impression_div').show();
                $("#lower_impression_dropdown").addClass("open");
            } else {
                if ($("#lower_Impression_stage").val() != "Yes") {
                    $('#lower_impression_div').hide();
                    $("#lower_impression_dropdown").removeClass("open");
                }

                if ($("#lower_extraction_stage").val() == "Yes") {
                    lower_extraction_inner_hideshow();
                    lower_extraction_validation();
                    $('#lower_extraction').show();

                    lower_extraction_error = false;

                    if ($("#lower_eTeethInMouth").val() == "Yes") {

                        if ($("#lower_eTeethInRequird").val() == "Yes") {
                            if ($("#lower_eTeethInDefault").val() != "Yes") {
                                lower_extraction_error = true;
                            }
                        }
                    }

                    if ($("#lower_eMessingTeeth").val() == "Yes") {

                        if ($("#lower_eMessingRequird").val() == "Yes") {
                            if ($("#lower_eMessingDefault").val() != "Yes") {
                                lower_extraction_error = true;
                            }
                        }
                    }
                    if ($("#lower_eExtractDelivery").val() == "Yes") {

                        if ($("#lower_eExtractRequird").val() == "Yes") {
                            if ($("#lower_eExtractDefault").val() != "Yes") {
                                lower_extraction_error = true;
                            }
                        }
                    }
                    if ($("#lower_eExtracted").val() == "Yes") {

                        if ($("#lower_eExtractedRequird").val() == "Yes") {
                            if ($("#lower_eExtractedDefault").val() != "Yes") {
                                lower_extraction_error = true;
                            }
                        }
                    }
                    if ($("#lower_eFixorAdd").val() == "Yes") {

                        if ($("#lower_eFixorAddRequird").val() == "Yes") {
                            if ($("#lower_eFixorAddDefault").val() != "Yes") {
                                lower_extraction_error = true;
                            }
                        }
                    }
                    if ($("#lower_eClasps").val() == "Yes") {

                        if ($("#lower_eClaspsRequird").val() == "Yes") {
                            if ($("#lower_eClaspsDefault").val() != "Yes") {
                                lower_extraction_error = true;
                            }
                        }
                    }


                    if ($("#lower_extraction_error").val() == "false") {
                        lower_extraction_error = false;
                    }

                    if (lower_extraction_error == false) {

                        $("#lower_extraction_error").val("false");

                        $('#category_addons_div').show();
                        $('#lower_category_addons_model_div').show();
                        $('#lower_category_addons_div').show();
                        $('#lower_addons_div').show();
                        $('#category_addons_model_div').show();
                        $('#lower_addons').show();
                        $('#stage_status_div').show();
                        $('#status_lbl').show();
                        $('#lower_status_div').show();
                        $('#submit_div').show();
                        $('.notes_div').show();
                        $('#notes_lable_div').show();

                        $('#upper_extraction').show();
                    } else {
                        $("#lower_extraction_error").val("true");
                        $('#upper_extraction').hide();
                    }

                }

                if (upper_extraction_stage == "Yes") {

                    $("#upper_in_mouth_div").show();
                    $("#upper_missing_teeth_div").show();
                    $("#upper_mising_all").show();
                    $("#upper_ectract_delivery_div").show();
                    $("#upper_been_extracted_div").show();
                    $("#upper_fix_div").show();
                    $("#upper_clasps_div").show();

                    upper_extraction_inner_hideshow();
                    upper_extraction_validation();
                    $('#upper_extraction').show();
                }


                if ($("#upper_extraction_stage").val() != "Yes" && $("#upper_Impression_stage").val() == "Yes") {
                    $('#category_addons_div').show();
                    $('#category_addons_model_div').show();
                    $('#upper_category_addons_div').show();
                    $('#upper_addons_model_div').show();
                    $('#upper_addons_div').show();
                    $('#stage_status_div').show();
                    $('#upper_status_div').show();
                    $('#submit_div').show();
                    $('#status_lbl').show();
                    $('.notes_div').show();
                    $('#notes_lable_div').show();
                }


            }



            if ($("#lower_extraction_stage").val() != "Yes" && $("#lower_Impression_stage").val() == "Yes") {
                $('#category_addons_div').show();
                $('#lower_category_addons_model_div').show();
                $('#lower_category_addons_div').show();
                $('#lower_addons_div').show();
                $('#category_addons_model_div').show();
                $('#lower_addons').show();
                $('#stage_status_div').show();
                $('#status_lbl').show();
                $('#lower_status_div').show();
                $('#submit_div').show();
                $('.notes_div').show();
                $('#notes_lable_div').show();
            }




            /*else if (upper_teethshade_stage == "Yes") {

                           $('#teeth_shades_div').show();
                           $('#teethshade_lbl').show();
                           $('#upper_tsbrand_div').show();

                       } else if (upper_gumshade_stage == "Yes") {

                           $('#gum_shades_div').show();
                           $('#gumshade_lbl').show();
                           $('#upper_gsbrand_div').show();

                       }*/
        }


    });

    $(document).on('click', '#lower_add_impressions_btn', function() {

        lower_imps_error = "false";
        lower_impression_lbl_text = [];

        if ($("#lower_Impression_stage").val() == "Yes") {
            LowerImpressionId = [];
            $("input[name='lower_impression_checkbox[]']:checked").each(function() {
                LowerImpressionId.push($(this).val());
            });

            if (LowerImpressionId.length == 0) {
                $("#lower_impression_checkbox_error").show();
                $("#lower_impressions_warning").hide();
                lower_imps_error = true;
                return false;
            } else {
                $("#lower_impression_checkbox_error").hide();
                $("#lower_impressions_warning").hide();
                $.each(LowerImpressionId, function(key, value) {
                    lower_imprss_val = $("#lower_impression_qty_" + value).val();
                    lower_imprss_text = $("#lower_imps_name_" + value).text();
                    if (lower_imprss_val == "" || lower_imprss_val == 0) {
                        $("#lower_impression_qty_error_" + value).show();
                        lower_imps_error = true;
                        return false;
                    } else {
                        $("#lower_impression_qty_error_" + value).hide();
                        lower_impression_lbl_text.push(lower_imprss_val + "x" + lower_imprss_text);
                    }


                });
            }
        } else {

            LowerImpressionId = [];
            $("input[name='lower_impression_checkbox[]']:checked").each(function() {
                LowerImpressionId.push($(this).val());
            });

            if (LowerImpressionId.length == 0) {
                // $("#lower_impression_checkbox_error").show();
                $("#lower_impressions_warning").show();
                lower_imps_error = "false";
            } else {
                $("#lower_impression_checkbox_error").hide();
                $("#lower_impressions_warning").hide();
                $.each(LowerImpressionId, function(key, value) {
                    lower_imprss_val = $("#lower_impression_qty_" + value).val();
                    lower_imprss_text = $("#lower_imps_name_" + value).text();
                    if (lower_imprss_val == "") {
                        $("#lower_impression_qty_error_" + value).show();
                        lower_imps_error = true;
                        return false;
                    } else {
                        $("#lower_impression_qty_error_" + value).hide();
                        lower_impression_lbl_text.push(lower_imprss_val + "x" + lower_imprss_text);
                    }

                });
            }

        }


        if (lower_imps_error == "false") {
            // $('#lower_extraction').show();

            // $("#lower_impression_dropdown").removeClass("open");
            // $('#upper_extraction').show();
            lower_extraction_stage = $("#lower_extraction_stage").val();
            lower_teethshade_stage = $("#lower_teethshade_stage").val();
            lower_gumshade_stage = $("#lower_gumshade_stage").val();

            $("#lower_impressions").val(lower_impression_lbl_text.join(", "));
            $("#lower_impression_dropdown").removeClass("open");

            if ($("#upper_eOpposingImpressions").val() == "Yes" && $("#upper_Impression_stage").val() != "Yes") {
                $('#upper_impression_div').show();
                $("#upper_impression_dropdown").addClass("open");
            } else {
                if ($("#upper_Impression_stage").val() != "Yes") {
                    $('#upper_impression_div').hide();
                    $("#upper_impression_dropdown").removeClass("open");
                } else if ($("#upper_Impression_stage").val() == "Yes") {

                }

                if ($("#upper_extraction_stage").val() == "Yes") {

                    upper_extraction_inner_hideshow();
                    upper_extraction_validation();
                    $('#upper_extraction').show();


                    upper_extraction_error = false;

                    if ($("#upper_eTeethInMouth").val() == "Yes") {

                        if ($("#upper_eTeethInRequird").val() == "Yes") {
                            if ($("#upper_eTeethInDefault").val() != "Yes") {
                                upper_extraction_error = true;
                            }
                        }
                    }

                    if ($("#upper_eMessingTeeth").val() == "Yes") {

                        if ($("#upper_eMessingRequird").val() == "Yes") {
                            if ($("#upper_eMessingDefault").val() != "Yes") {
                                upper_extraction_error = true;
                            }
                        }
                    }
                    if ($("#upper_eExtractDelivery").val() == "Yes") {

                        if ($("#upper_eExtractRequird").val() == "Yes") {
                            if ($("#upper_eExtractDefault").val() != "Yes") {
                                upper_extraction_error = true;
                            }
                        }
                    }
                    if ($("#upper_eExtracted").val() == "Yes") {

                        if ($("#upper_eExtractedRequird").val() == "Yes") {
                            if ($("#upper_eExtractedDefault").val() != "Yes") {
                                upper_extraction_error = true;
                            }
                        }
                    }
                    if ($("#upper_eFixorAdd").val() == "Yes") {

                        if ($("#upper_eFixorAddRequird").val() == "Yes") {
                            if ($("#upper_eFixorAddDefault").val() != "Yes") {
                                upper_extraction_error = true;
                            }
                        }
                    }
                    if ($("#upper_eClasps").val() == "Yes") {
                        if ($("#upper_eClaspsRequird").val() == "Yes") {
                            if ($("#upper_eClaspsDefault").val() != "Yes") {
                                upper_extraction_error = true;
                            }
                        }
                    }

                    if ($("#upper_extraction_error").val() == "false") {
                        upper_extraction_error = false;
                    }

                    if (upper_extraction_error == false) {

                        $("#upper_extraction_error").val("false");

                        $('#category_addons_div').show();
                        $('#category_addons_model_div').show();
                        $('#upper_category_addons_div').show();
                        $('#upper_addons_model_div').show();
                        $('#upper_addons_div').show();
                        $('#stage_status_div').show();
                        $('#upper_status_div').show();
                        $('#submit_div').show();
                        $('#status_lbl').show();
                        $('.notes_div').show();
                        $('#notes_lable_div').show();

                        $('#lower_extraction').show();
                    } else {
                        $("#upper_extraction_error").val("true");
                        $('#lower_extraction').hide();
                    }

                }

                if (lower_extraction_stage == "Yes") {

                    $("#lower_in_mouth_div").show();
                    $("#lower_missing_teeth_div").show();
                    $("#lower_mising_all").show();
                    $("#lower_ectract_delivery_div").show();
                    $("#lower_been_extracted_div").show();
                    $("#lower_fix_div").show();
                    $("#lower_clasps_div").show();

                    lower_extraction_inner_hideshow();
                    lower_extraction_validation();
                    $('#lower_extraction').show();

                }


                if ($("#lower_extraction_stage").val() != "Yes" && $("#lower_Impression_stage").val() == "Yes") {
                    $('#category_addons_div').show();
                    $('#lower_category_addons_model_div').show();
                    $('#lower_category_addons_div').show();
                    $('#lower_addons_div').show();
                    $('#category_addons_model_div').show();
                    $('#lower_addons').show();
                    $('#stage_status_div').show();
                    $('#status_lbl').show();
                    $('#lower_status_div').show();
                    $('#submit_div').show();
                    $('.notes_div').show();
                    $('#notes_lable_div').show();
                }


            }


            if ($("#upper_extraction_stage").val() != "Yes" && $("#upper_Impression_stage").val() == "Yes") {
                $('#category_addons_div').show();
                $('#category_addons_model_div').show();
                $('#upper_category_addons_div').show();
                $('#upper_addons_model_div').show();
                $('#upper_addons_div').show();
                $('#stage_status_div').show();
                $('#upper_status_div').show();
                $('#submit_div').show();
                $('#status_lbl').show();
                $('.notes_div').show();
                $('#notes_lable_div').show();
            }





        }

    });

    function upper_extraction_inner_hideshow() {

        $("#upper_in_mouth_div").show();
        $("#upper_missing_teeth_div").show();
        $("#upper_mising_all").show();
        $("#upper_ectract_delivery_div").show();
        $("#upper_been_extracted_div").show();
        $("#upper_fix_div").show();
        $("#upper_clasps_div").show();

        upper_eTeethInMouth = $("#upper_eTeethInMouth").val();
        upper_eMessingTeeth = $("#upper_eMessingTeeth").val();
        upper_eExtractDelivery = $("#upper_eExtractDelivery").val();
        upper_eExtracted = $("#upper_eExtracted").val();
        upper_eFixorAdd = $("#upper_eFixorAdd").val();
        upper_eClasps = $("#upper_eClasps").val();

        if (upper_eTeethInMouth == "Yes") {
            $("#upper_in_mouth_div").show();

            if ($("#upper_eTeethInDefault").val() == "Yes") {
                $("#upper_teeth_in_mouth_error").hide();
            } else {
                if ($("#upper_eTeethInRequird").val() == "Yes") {
                    $("#upper_teeth_in_mouth_error").show();
                } else {
                    $("#upper_teeth_in_mouth_error").hide();
                }
            }

        } else {
            $("#upper_in_mouth_div").hide();
            $("#upper_teeth_in_mouth_error").hide();
        }

        if (upper_eMessingTeeth == "Yes") {
            $("#upper_missing_teeth_div").show();
            $("#upper_mising_all").show();

            if ($("#upper_eMessingDefault").val() == "Yes") {
                $("#upper_missing_teeth_error").hide();
            } else {
                if ($("#upper_eMessingRequird").val() == "Yes") {
                    $("#upper_missing_teeth_error").show();
                } else {
                    $("#upper_missing_teeth_error").hide();
                }
            }


        } else {
            $("#upper_missing_teeth_div").hide();
            $("#upper_mising_all").hide();
            $("#upper_missing_teeth_error").hide();
        }

        if (upper_eExtractDelivery == "Yes") {
            $("#upper_ectract_delivery_div").show();

            if ($("#upper_eExtractDefault").val() == "Yes") {
                $("#upper_will_extract_on_delivery_error").hide();
            } else {
                if ($("#upper_eExtractRequird").val() == "Yes") {
                    $("#upper_will_extract_on_delivery_error").show();
                } else {
                    $("#upper_will_extract_on_delivery_error").hide();
                }
            }


        } else {
            $("#upper_ectract_delivery_div").hide();
            $("#upper_will_extract_on_delivery_error").hide();
        }

        if (upper_eExtracted == "Yes") {
            $("#upper_been_extracted_div").show();

            if ($("#upper_eExtractedDefault").val() == "Yes") {
                $("#upper_has_been_extracted_error").hide();
            } else {
                if ($("#upper_eExtractedRequird").val() == "Yes") {
                    $("#upper_has_been_extracted_error").show();
                } else {
                    $("#upper_has_been_extracted_error").hide();
                }
            }

        } else {
            $("#upper_been_extracted_div").hide();
            $("#upper_has_been_extracted_error").hide();
        }

        if (upper_eFixorAdd == "Yes") {
            $("#upper_fix_div").show();

            if ($("#upper_eFixorAddDefault").val() == "Yes") {
                $("#upper_fix_or_add_error").hide();
            } else {
                if ($("#upper_eFixorAddRequird").val() == "Yes") {
                    $("#upper_fix_or_add_error").show();
                } else {
                    $("#upper_fix_or_add_error").hide();
                }
            }

        } else {
            $("#upper_fix_div").hide();
            $("#upper_fix_or_add_error").hide();
        }

        if (upper_eClasps == "Yes") {
            $("#upper_clasps_div").show();

            if ($("#upper_eClaspsDefault").val() == "Yes") {
                $("#upper_clasps_error").hide();
            } else {
                if ($("#upper_eClaspsRequird").val() == "Yes") {
                    $("#upper_clasps_error").show();
                } else {
                    $("#upper_clasps_error").hide();
                }
            }

        } else {
            $("#upper_clasps_div").hide();
            $("#upper_clasps_error").hide();
        }
    }

    function lower_opposite_extraction_inner_hideshow() {


        $("#lower_in_mouth_div").show();
        $("#lower_missing_teeth_div").show();
        $("#lower_mising_all").show();
        $("#lower_ectract_delivery_div").show();
        $("#lower_been_extracted_div").show();
        $("#lower_fix_div").show();
        $("#lower_clasps_div").show();

        lower_eOpposingTeethInMouth = $("#lower_eOpposingTeethInMouth").val();
        lower_eOpposingMessingTeeth = $("#lower_eOpposingMessingTeeth").val();
        lower_eOpposingExtractDelivery = $("#lower_eOpposingExtractDelivery").val();
        lower_eOpposingExtracted = $("#lower_eOpposingExtracted").val();
        lower_eOpposingFixorAdd = $("#lower_eOpposingFixorAdd").val();
        lower_eOpposingClasps = $("#lower_eOpposingClasps").val();

        if (lower_eOpposingTeethInMouth == "Yes") {
            $("#lower_in_mouth_div").show();
        } else {
            $("#lower_in_mouth_div").hide();
        }

        if (lower_eOpposingMessingTeeth == "Yes") {
            $("#lower_missing_teeth_div").show();
            $("#lower_mising_all").show();
        } else {
            $("#lower_missing_teeth_div").hide();
            $("#lower_mising_all").hide();
        }

        if (lower_eOpposingExtractDelivery == "Yes") {
            $("#lower_ectract_delivery_div").show();
        } else {
            $("#lower_ectract_delivery_div").hide();
        }

        if (lower_eOpposingExtracted == "Yes") {
            $("#lower_been_extracted_div").show();
        } else {
            $("#lower_been_extracted_div").hide();
        }

        if (lower_eOpposingFixorAdd == "Yes") {
            $("#lower_fix_div").show();
        } else {
            $("#lower_fix_div").hide();
        }

        if (lower_eOpposingClasps == "Yes") {
            $("#lower_clasps_div").show();
        } else {
            $("#lower_clasps_div").hide();
        }
    }

    function upper_opposite_extraction_inner_hideshow() {

        $("#upper_in_mouth_div").show();
        $("#upper_missing_teeth_div").show();
        $("#upper_mising_all").show();
        $("#upper_ectract_delivery_div").show();
        $("#upper_been_extracted_div").show();
        $("#upper_fix_div").show();
        $("#upper_clasps_div").show();

        upper_eOpposingTeethInMouth = $("#upper_eOpposingTeethInMouth").val();
        upper_eOpposingMessingTeeth = $("#upper_eOpposingMessingTeeth").val();
        upper_eOpposingExtractDelivery = $("#upper_eOpposingExtractDelivery").val();
        upper_eOpposingExtracted = $("#upper_eOpposingExtracted").val();
        upper_eOpposingFixorAdd = $("#upper_eOpposingFixorAdd").val();
        upper_eOpposingClasps = $("#upper_eOpposingClasps").val();

        if (upper_eOpposingTeethInMouth == "Yes") {
            $("#upper_in_mouth_div").show();
        } else {
            $("#upper_in_mouth_div").hide();
        }

        if (upper_eOpposingMessingTeeth == "Yes") {
            $("#upper_missing_teeth_div").show();
            $("#upper_mising_all").show();
        } else {
            $("#upper_missing_teeth_div").hide();
            $("#upper_mising_all").hide();
        }

        if (upper_eOpposingExtractDelivery == "Yes") {
            $("#upper_ectract_delivery_div").show();
        } else {
            $("#upper_ectract_delivery_div").hide();
        }

        if (upper_eOpposingExtracted == "Yes") {
            $("#upper_been_extracted_div").show();
        } else {
            $("#upper_been_extracted_div").hide();
        }

        if (upper_eOpposingFixorAdd == "Yes") {
            $("#upper_fix_div").show();
        } else {
            $("#upper_fix_div").hide();
        }

        if (upper_eOpposingClasps == "Yes") {
            $("#upper_clasps_div").show();
        } else {
            $("#upper_clasps_div").hide();
        }
    }

    function upper_based_lower_extraction() {
        upper_eTeethInMouth = $("#upper_eTeethInMouth").val();
        upper_eMessingTeeth = $("#upper_eMessingTeeth").val();
        upper_eExtractDelivery = $("#upper_eExtractDelivery").val();
        upper_eExtracted = $("#upper_eExtracted").val();
        upper_eFixorAdd = $("#upper_eFixorAdd").val();
        upper_eClasps = $("#upper_eClasps").val();

        if (upper_eTeethInMouth == "Yes") {
            $("#lower_in_mouth_div").show();
        } else {
            $("#lower_in_mouth_div").hide();
        }

        if (upper_eMessingTeeth == "Yes") {
            $("#lower_missing_teeth_div").show();
            $("#lower_mising_all").show();
        } else {
            $("#lower_missing_teeth_div").hide();
            $("#lower_mising_all").hide();
        }

        if (upper_eExtractDelivery == "Yes") {
            $("#lower_ectract_delivery_div").show();
        } else {
            $("#lower_ectract_delivery_div").hide();
        }

        if (upper_eExtracted == "Yes") {
            $("#lower_been_extracted_div").show();
        } else {
            $("#lower_been_extracted_div").hide();
        }

        if (upper_eFixorAdd == "Yes") {
            $("#lower_fix_div").show();
        } else {
            $("#lower_fix_div").hide();
        }

        if (upper_eClasps == "Yes") {
            $("#lower_clasps_div").show();
        } else {
            $("#lower_clasps_div").hide();
        }
    }

    function lower_extraction_inner_hideshow() {

        $("#lower_in_mouth_div").show();
        $("#lower_missing_teeth_div").show();
        $("#lower_mising_all").show();
        $("#lower_ectract_delivery_div").show();
        $("#lower_been_extracted_div").show();
        $("#lower_fix_div").show();
        $("#lower_clasps_div").show();

        lower_eTeethInMouth = $("#lower_eTeethInMouth").val();
        lower_eMessingTeeth = $("#lower_eMessingTeeth").val();
        lower_eExtractDelivery = $("#lower_eExtractDelivery").val();
        lower_eExtracted = $("#lower_eExtracted").val();
        lower_eFixorAdd = $("#lower_eFixorAdd").val();
        lower_eClasps = $("#lower_eClasps").val();

        if (lower_eTeethInMouth == "Yes") {
            $("#lower_in_mouth_div").show();

            if ($("#lower_eTeethInDefault").val() == "Yes") {
                $("#lower_teeth_in_mouth_error").hide();
            } else {
                if ($("#lower_eTeethInRequird").val() == "Yes") {
                    $("#lower_teeth_in_mouth_error").show();
                } else {
                    $("#lower_teeth_in_mouth_error").hide();
                }
            }

        } else {
            $("#lower_in_mouth_div").hide();
            $("#lower_teeth_in_mouth_error").hide();
        }

        if (lower_eMessingTeeth == "Yes") {
            $("#lower_missing_teeth_div").show();
            $("#lower_mising_all").show();

            if ($("#lower_eMessingDefault").val() == "Yes") {
                $("#lower_missing_teeth_error").hide();
            } else {
                if ($("#lower_eMessingRequird").val() == "Yes") {
                    $("#lower_missing_teeth_error").show();
                } else {
                    $("#lower_missing_teeth_error").hide();
                }
            }

        } else {
            $("#lower_missing_teeth_div").hide();
            $("#lower_mising_all").hide();
            $("#lower_missing_teeth_error").hide();
        }

        if (lower_eExtractDelivery == "Yes") {
            $("#lower_ectract_delivery_div").show();

            if ($("#lower_eExtractDefault").val() == "Yes") {
                $("#lower_will_extract_on_delivery_error").hide();
            } else {
                if ($("#lower_eExtractRequird").val() == "Yes") {
                    $("#lower_will_extract_on_delivery_error").show();
                } else {
                    $("#lower_will_extract_on_delivery_error").hide();
                }
            }

        } else {
            $("#lower_ectract_delivery_div").hide();
            $("#lower_will_extract_on_delivery_error").hide();
        }

        if (lower_eExtracted == "Yes") {
            $("#lower_been_extracted_div").show();


            if ($("#lower_eExtractedDefault").val() == "Yes") {
                $("#lower_has_been_extracted_error").hide();
            } else {
                if ($("#lower_eExtractedRequird").val() == "Yes") {
                    $("#lower_has_been_extracted_error").show();
                } else {
                    $("#lower_has_been_extracted_error").hide();
                }
            }


        } else {
            $("#lower_been_extracted_div").hide();
            $("#lower_has_been_extracted_error").hide();
        }

        if (lower_eFixorAdd == "Yes") {
            $("#lower_fix_div").show();

            if ($("#lower_eFixorAddDefault").val() == "Yes") {
                $("#lower_fix_or_add_error").hide();
            } else {
                if ($("#lower_eFixorAddRequird").val() == "Yes") {
                    $("#lower_fix_or_add_error").show();
                } else {
                    $("#lower_fix_or_add_error").hide();
                }
            }

        } else {
            $("#lower_fix_div").hide();
            $("#lower_fix_or_add_error").hide();
        }

        if (lower_eClasps == "Yes") {
            $("#lower_clasps_div").show();
            if ($("#lower_eClaspsDefault").val() == "Yes") {
                $("#lower_clasps_error").hide();
            } else {
                if ($("#lower_eClaspsRequird").val() == "Yes") {
                    $("#lower_clasps_error").show();
                } else {
                    $("#lower_clasps_error").hide();
                }
            }
        } else {
            $("#lower_clasps_div").hide();
            $("#lower_clasps_error").hide();
        }
    }

    function lower_based_upper_extraction() {
        lower_eTeethInMouth = $("#lower_eTeethInMouth").val();
        lower_eMessingTeeth = $("#lower_eMessingTeeth").val();
        lower_eExtractDelivery = $("#lower_eExtractDelivery").val();
        lower_eExtracted = $("#lower_eExtracted").val();
        lower_eFixorAdd = $("#lower_eFixorAdd").val();
        lower_eClasps = $("#lower_eClasps").val();

        if (lower_eTeethInMouth == "Yes") {
            $("#upper_in_mouth_div").show();
        } else {
            $("#upper_in_mouth_div").hide();
        }

        if (lower_eMessingTeeth == "Yes") {
            $("#upper_missing_teeth_div").show();
            $("#upper_mising_all").show();
        } else {
            $("#upper_missing_teeth_div").hide();
            $("#upper_mising_all").hide();
        }

        if (lower_eExtractDelivery == "Yes") {
            $("#upper_ectract_delivery_div").show();
        } else {
            $("#upper_ectract_delivery_div").hide();
        }

        if (lower_eExtracted == "Yes") {
            $("#upper_been_extracted_div").show();
        } else {
            $("#upper_been_extracted_div").hide();
        }

        if (lower_eFixorAdd == "Yes") {
            $("#upper_fix_div").show();
        } else {
            $("#upper_fix_div").hide();
        }

        if (lower_eClasps == "Yes") {
            $("#upper_clasps_div").show();
        } else {
            $("#upper_clasps_div").hide();
        }
    }

    $(document).on('change', '#iUpperProductId', function() {
        upper_product_change();
    });

    function upper_product_change() {
        var iUpperProductId = $("#iUpperProductId").val();
        if (iUpperProductId.length > 0) {
            $.ajax({
                url: "{{route('admin.labcase.getgrade')}}",
                type: "post",
                data: {
                    iCategoryProductId: iUpperProductId,
                    eType: "Upper",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {


                    if (result == "1") {
                        upper_grade_change();
                    } else {
                        /*Show upper Grade*/
                        $('#iUpperGradeId').selectize()[0].selectize.destroy();
                        $('#iUpperGradeId').html(result);
                        $('#iUpperGradeId').selectize();
                        $('#grade_div').show();
                        $('#grade_lbl').show();
                        $('#grade_upper_div').show();
                        /*Show upper Grade*/

                        iUpperGradeId = $('#iUpperGradeId').val();
                        if (iUpperGradeId.length != 0) {
                            upper_grade_change();
                        }else{
                            $("#iUpperGradeId")[0].selectize.open();
                        }
                    }
                }
            });
        }
    }


    $(document).on('change', '#iUpperGradeId', function() {
        upper_grade_change();
    });

    function upper_grade_change() {
        var iUpperProductId = $("#iUpperProductId").val();
        var iUpperCategoryId = $("#iUpperCategoryId").val();
        if (iUpperProductId.length > 0 && iUpperCategoryId.length > 0) {
            $.ajax({
                url: "{{route('admin.labcase.getstage')}}",
                type: "post",
                data: {
                    iCategoryProductId: iUpperProductId,
                    iCategoryId: iUpperCategoryId,
                    eType: "Upper",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {

                    /*Upper Stage Show*/
                    $('#iUpperStageId').selectize()[0].selectize.destroy();
                    $('#iUpperStageId').html(result);
                    $('#iUpperStageId').selectize();
                    $('#stage_div').show();
                    $('#stage_lbl').show();
                    $('#stage_upper_div').show();
                    /*Upper Stage Show*/
                    iUpperStageId = $('#iUpperStageId').val();
                    if (iUpperStageId.length != 0) {
                        upper_stage_changes();
                    }else{
                        $("#iUpperStageId")[0].selectize.open();
                    }
                }
            });
        }
    }

    $(document).on('change', '#iLowerGradeId', function() {
        lower_grade_change();
    });

    function lower_grade_change() {
        var iLowerProductId = $("#iLowerProductId").val();
        var iLowerCategoryId = $("#iLowerCategoryId").val();
        if (iLowerProductId.length > 0 && iLowerCategoryId.length > 0) {
            $.ajax({
                url: "{{route('admin.labcase.getstage')}}",
                type: "post",
                data: {
                    iCategoryProductId: iLowerProductId,
                    iCategoryId: iLowerCategoryId,
                    eType: "Upper",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {

                    /*Lower Stage Show*/
                    $('#iLowerStageId').selectize()[0].selectize.destroy();
                    $('#iLowerStageId').html(result);
                    $('#iLowerStageId').selectize();
                    $('#stage_div').show();
                    $('#stage_lbl').show();
                    $('#stage_lower_div').show();
                    /*Lower Stage Show*/
                    iLowerStageId = $('#iLowerStageId').val();
                    if (iLowerStageId.length != 0) {
                        lower_stage_change();
                    }else{
                        $("#iLowerStageId")[0].selectize.open();
                    }

                }
            });
        }
    }

    $(document).on('change', '#iLowerProductId', function() {
        lower_product_change();
    });

    function lower_product_change() {
        var iLowerProductId = $("#iLowerProductId").val();
        if (iLowerProductId.length > 0) {
            $.ajax({
                url: "{{route('admin.labcase.getgrade')}}",
                type: "post",
                data: {
                    iCategoryProductId: iLowerProductId,
                    eType: "Upper",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {

                    if (result == "1") {
                        lower_grade_change();
                    } else {
                        /*Show upper Grade*/
                        $('#iLowerGradeId').selectize()[0].selectize.destroy();
                        $('#iLowerGradeId').html(result);
                        $('#iLowerGradeId').selectize();
                        $('#grade_div').show();
                        $('#grade_lbl').show();
                        $('#grade_lower_div').show();
                        /*Show upper Grade*/

                        iLowerGradeId = $('#iLowerGradeId').val();
                        if (iLowerGradeId.length != 0) {
                            lower_grade_change();
                        }else{
                            $("#iLowerGradeId")[0].selectize.open();
                        }
                    }
                }
            });
        }
    }

    $(document).on('change', '#upper_tsbrand', function() {
        upper_tsbrand_change();
    });

    function upper_tsbrand_change() {
        var iToothBrandId = $("#upper_tsbrand").val();
        if (iToothBrandId.length > 0) {
            $.ajax({
                url: "{{route('admin.labcase.gettoothshades')}}",
                type: "post",
                data: {
                    iToothBrandId: iToothBrandId,
                    eType: "Upper",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {
                    $('#upper_tshade').selectize()[0].selectize.destroy();
                    $('#upper_tshade').html(result);
                    $('#upper_tshade').selectize();
                    $('#upper_ts_select_div').show();

                    if ($("#lower_tshade").val() != "") {
                        $('#upper_tshade').data('selectize').setValue($("#lower_tshade").val());
                        upper_ts_change();
                    } else if ($("#upper_tshade").val() != "") {
                        upper_ts_change();
                    }else{
                        $("#upper_tshade")[0].selectize.open();
                    }

                }
            });
        }
    }

    $(document).on('change', '#upper_tshade', function() {
        if ($("#upper_tshade").val() != "") {
            upper_ts_change();
        }
    });

    function upper_ts_change() {
        upper_gumshade_stage = $("#upper_gumshade_stage").val();
        upper_Impression_stage = $("#upper_Impression_stage").val();
        upper_extraction_stage = $("#upper_extraction_stage").val();

        if (upper_gumshade_stage == "Yes") {
            /*upper gs*/
            $('#gum_shades_div').show();
            $('#gumshade_lbl').show();
            $('#upper_gsbrand_div').show();
            $('#upper_gs_select_div').hide();

            gumbrand_select_data("Upper");

            /*upper gs*/
        } else if (upper_Impression_stage == "Yes") {

            /*upper gs*/
            $('#gum_shades_div').hide();
            $('#gumshade_lbl').hide();
            $('#upper_gsbrand_div').hide();
            $('#upper_gs_select_div').hide();
            /*upper gs*/


            /*Upper Impressions*/
            $('#impression_div').show();
            $('#impression_lbl').show();
            $('#upper_impression_div').show();
            $("#upper_impression_dropdown").addClass("open");
            // if ($("#lower_Impression_stage").val() != "Yes" && $("#lower_eOpposingImpressions").val() == "Yes") {
            //     $('#lower_impression_div').show();
            // }
            /*Upper Impressions*/

            $('#upper_extraction').hide();
            if ($("#lower_extraction_stage").val() != "Yes") {
                $('#lower_extraction').hide();
            }


            // My Comment
            // $('#category_addons_div').show();
            // $('#category_addons_model_div').show();
            // $('#upper_addons_model_div').show();
            // $('#upper_addons_div').show();
            // $('#stage_status_div').show();
            // $('#upper_status_div').show();
            // $('#submit_div').show();
            // $('#status_lbl').show();

        } else if (upper_extraction_stage == "Yes") {

            /*upper gs*/
            $('#gum_shades_div').hide();
            $('#gumshade_lbl').hide();
            $('#upper_gsbrand_div').hide();
            $('#upper_gs_select_div').hide();
            /*upper gs*/


            /*Upper Impressions*/
            $('#upper_impression_div').hide();
            if ($("#lower_Impression_stage").val() != "Yes") {
                $('#impression_lbl').hide();
                $('#impression_div').hide();
                $('#lower_impression_div').hide();
            }
            /*Upper Impressions*/

            upper_extraction_inner_hideshow();
            upper_extraction_validation();
            $('#upper_extraction').show();
            // if ($("#lower_extraction_stage").val() != "Yes") {
            //     $('#lower_extraction').show();
            // }

        } else {
            $('#category_addons_div').show();
            $('#category_addons_model_div').show();
            $('#upper_category_addons_div').show();
            $('#stage_status_div').show();
            $('#status_lbl').show();
            $('#submit_div').show();
            $('.notes_div').show();
            $('#upper_category_addons_model_div').show();
            $('#upper_addons_model_div').show();
            $('#upper_addons_div').show();
            $('#upper_addons').show();
            $('#upper_status_div').show();
        }
    }

    $(document).on('change', '#lower_tsbrand', function() {
        lower_tsbrand_change();
    });

    function lower_tsbrand_change() {
        var iToothBrandId = $("#lower_tsbrand").val();
        if (iToothBrandId.length > 0) {
            $.ajax({
                url: "{{route('admin.labcase.gettoothshades')}}",
                type: "post",
                data: {
                    iToothBrandId: iToothBrandId,
                    eType: "Lower",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {
                    $('#lower_tshade').selectize()[0].selectize.destroy();
                    $('#lower_tshade').html(result);
                    $('#lower_tshade').selectize();
                    $('#lower_ts_select_div').show();

                    if ($("#upper_tshade").val() != "") {
                        $('#lower_tshade').data('selectize').setValue($("#upper_tshade").val());
                        lower_ts_change();
                    } else if ($("#lower_tshade").val() != "") {
                        lower_ts_change();
                    }else{
                        $("#lower_tshade")[0].selectize.open();
                    }
                }
            });
        }
    }

    $(document).on('change', '#lower_tshade', function() {
        if ($("#lower_tshade").val() != "") {
            lower_ts_change();
        }
    });

    function lower_ts_change() {
        lower_gumshade_stage = $("#lower_gumshade_stage").val();
        lower_Impression_stage = $("#lower_Impression_stage").val();
        lower_extraction_stage = $("#lower_extraction_stage").val();

        if (lower_gumshade_stage == "Yes") {
            /*upper gs*/
            $('#gum_shades_div').show();
            $('#gumshade_lbl').show();
            $('#lower_gsbrand_div').show();
            $('#lower_gs_select_div').hide();
            gumbrand_select_data("Lower");
            /*upper gs*/
        } else if (lower_Impression_stage == "Yes") {

            /*upper gs*/
            $('#gum_shades_div').hide();
            $('#gumshade_lbl').hide();
            $('#lower_gsbrand_div').hide();
            $('#lower_gs_select_div').hide();
            /*upper gs*/


            /*Upper Impressions*/
            $('#impression_div').show();
            $('#impression_lbl').show();
            $('#lower_impression_div').show();
            $("#lower_impression_dropdown").addClass("open");
            // if ($("#upper_Impression_stage").val() != "Yes" && $("#upper_eOpposingImpressions").val() == "Yes") {
            //     $('#upper_impression_div').show();
            // }
            /*Upper Impressions*/

            $('#lower_extraction').hide();
            if ($("#upper_extraction_stage").val() != "Yes") {
                $('#upper_extraction').hide();
            }

        } else if (lower_extraction_stage == "Yes") {

            /*upper gs*/
            $('#gum_shades_div').hide();
            $('#gumshade_lbl').hide();
            $('#lower_gsbrand_div').hide();
            $('#lower_gs_select_div').hide();
            /*upper gs*/


            /*Upper Impressions*/
            $('#lower_impression_div').hide();
            if ($("#upper_Impression_stage").val() != "Yes") {
                $('#impression_lbl').hide();
                $('#impression_div').hide();
                $('#upper_impression_div').hide();
            }
            /*Upper Impressions*/

            lower_extraction_inner_hideshow();
            lower_extraction_validation();
            $('#lower_extraction').show();

        } else {
            $('#category_addons_div').show();
            $('#category_addons_model_div').show();
            $('#lower_category_addons_div').show();
            $('#stage_status_div').show();
            $('#status_lbl').show();
            $('#submit_div').show();
            $('.notes_div').show();
            $('#lower_category_addons_model_div').show();
            $('#upper_addons_model_div').show();
            $('#lower_addons_div').show();
            $('#lower_addons').show();
            $('#lower_status_div').show();
        }
    }

    $(document).on('change', '#upper_gsbrand', function() {
        upper_gsbrand_changes();
    });


    function upper_gsbrand_changes() {
        var iGumBrandId = $("#upper_gsbrand").val();
        if (iGumBrandId.length > 0) {
            $.ajax({
                url: "{{route('admin.labcase.getgumshades')}}",
                type: "post",
                data: {
                    iGumBrandId: iGumBrandId,
                    iProductId: $("#iUpperProductId").val(),
                    iLabId: $("#iLabId").val(),
                    eType: "Upper",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {
                    $('#upper_gshade').selectize()[0].selectize.destroy();
                    $('#upper_gshade').html(result);
                    $('#upper_gshade').selectize();
                    $('#upper_gs_select_div').show();

                    if ($("#lower_gshade").val() != "") {
                        $('#upper_gshade').data('selectize').setValue($("#lower_gshade").val());
                    }else if ($('#upper_gshade').val() != "") {
                        upper_gs_change();
                    }else{
                        $("#upper_gshade")[0].selectize.open();
                    }
                }
            });
        }
    }

    $(document).on('change', '#lower_gsbrand', function() {
        lower_gsbrand_changes();
    });


    function lower_gsbrand_changes() {
        var iGumBrandId = $("#lower_gsbrand").val();
        if (iGumBrandId.length > 0) {
            $.ajax({
                url: "{{route('admin.labcase.getgumshades')}}",
                type: "post",
                data: {
                    iGumBrandId: iGumBrandId,
                    iProductId: $("#iLowerProductId").val(),
                    iLabId: $("#iLabId").val(),
                    eType: "Lower",
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {
                    $('#lower_gshade').selectize()[0].selectize.destroy();
                    $('#lower_gshade').html(result);
                    $('#lower_gshade').selectize();
                    $('#lower_gs_select_div').show();

                    if ($("#upper_gshade").val() != "") {
                        $('#lower_gshade').data('selectize').setValue($("#upper_gshade").val());
                        lower_gs_change();
                    } else if ($('#lower_gshade').val() != "") {
                        lower_gs_change();
                    }else{
                        $("#lower_gshade")[0].selectize.open();
                    }
                }
            });
        }
    }

    $(document).on('change', '#upper_gshade', function() {
        if ($("#upper_gshade").val() != "") {
            upper_gs_change();
        }
    });

    function upper_gs_change() {

        upper_Impression_stage = $("#upper_Impression_stage").val();
        upper_extraction_stage = $("#upper_extraction_stage").val();

        if (upper_Impression_stage == "Yes") {

            /*Upper Impressions*/
            $('#impression_div').show();
            $('#impression_lbl').show();
            $('#upper_impression_div').show();
            $("#upper_impression_dropdown").addClass("open");
            /*Upper Impressions*/

            $('#upper_extraction').hide();
            if ($("#lower_extraction_stage").val() != "Yes") {
                $('#lower_extraction').hide();
            }

        } else if (upper_extraction_stage == "Yes") {
            $('#upper_extraction').show();
            if ($("#lower_extraction_stage").val() != "Yes") {
                $('#lower_extraction').show();
            }
        } else {
            $('#category_addons_div').show();
            $('#category_addons_model_div').show();
            $('#upper_category_addons_div').show();
            $('#stage_status_div').show();
            $('#status_lbl').show();
            $('#submit_div').show();
            $('.notes_div').show();
            $('#upper_category_addons_model_div').show();
            $('#upper_addons_model_div').show();
            $('#upper_addons_div').show();
            $('#upper_addons').show();
            $('#upper_status_div').show();
        }
    }

    $(document).on('change', '#lower_gshade', function() {
        if ($("#lower_gshade").val() != "") {
            lower_gs_change();
        }
    });

    function lower_gs_change() {
        lower_Impression_stage = $("#lower_Impression_stage").val();
        lower_extraction_stage = $("#lower_extraction_stage").val();

        if (lower_Impression_stage == "Yes") {

            /*Upper Impressions*/
            $('#impression_div').show();
            $('#impression_lbl').show();
            $('#lower_impression_div').show();
            $("#lower_impression_dropdown").addClass("open");
            /*Upper Impressions*/

            $('#lower_extraction').hide();
            if ($("#upper_extraction_stage").val() != "Yes") {
                $('#upper_extraction').hide();
            }

        } else if (lower_extraction_stage == "Yes") {
            $('#lower_extraction').show();
            if ($("#upper_extraction_stage").val() != "Yes") {
                $('#upper_extraction').show();
            }
        } else {
            $('#category_addons_div').show();
            $('#category_addons_model_div').show();
            $('#lower_category_addons_div').show();
            $('#stage_status_div').show();
            $('#status_lbl').show();
            $('#submit_div').show();
            $('.notes_div').show();
            $('#lower_category_addons_model_div').show();
            $('#upper_addons_model_div').show();
            $('#lower_addons_div').show();
            $('#lower_addons').show();
            $('#lower_status_div').show();
        }
    }

    $(document).on('change', '#upper_category_addons', function() {
        iAddCategoryId = $("#upper_category_addons").val();
        $.ajax({
            url: "{{route('admin.labcase.getcategoryaddons')}}",
            type: "post",
            data: {
                iAddCategoryId: iAddCategoryId,
                eType: "Upper",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#upper_subaddons').selectize()[0].selectize.destroy();
                $('#upper_subaddons').html(result);
                $('#upper_subaddons').selectize();
            }
        });
    });
    $(document).on('change', '#lower_category_addons', function() {
        var iAddCategoryId = $("#lower_category_addons").val();
        $.ajax({
            url: "{{route('admin.labcase.getcategoryaddons')}}",
            type: "post",
            data: {
                iAddCategoryId: iAddCategoryId,
                eType: "Lower",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#lower_subaddons').selectize()[0].selectize.destroy();
                $('#lower_subaddons').html(result);
                $('#lower_subaddons').selectize();
            }
        });
    });
    $("#vPatientName").keyup(function() {

        var vPatientName = $("#vPatientName").val();
        $("#product_button_div").show();
    });

    function get_impressions(eType){
        $.ajax({
            url: "{{route('admin.labcase.get_impressions')}}",
            type: "post",
            data: {
                iLabId: $("#iLabId").val(),
                eType: eType,
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                if (eType == "Upper") {
                    $("#upper_impressions_ajax_div").html(result);
                }else{
                    $("#lower_impressions_ajax_div").html(result);
                }
            }
        });
    }

    $(document).on('click', '#product_upper_button_div', async function() {

        await category_select_data("Upper");

        $("#category_div").show();
        $("#category_upper_div").show();
        $("#cat_lbl").show();
        


    });

    $(document).on('click', '#product_lower_button_div', async function() {
        await category_select_data("Lower");

        $("#category_div").show();
        $("#cat_lbl").show();
        $("#category_lower_div").show();


    });


    var upperaddons = 0;
    $(document).on('click', '#upper_submit', function() {

        upper_category_addons = $("#upper_category_addons").val();
        upper_subaddons = $("#upper_subaddons").val();
        iUpperQty = $("#iUpperQty").val();

        if (upper_category_addons.length == 0) {
            $("#upper_category_addons_error_div").show();
            return false;
        } else {
            $("#upper_category_addons_error_div").hide();
        }

        if (upper_subaddons.length == 0) {
            $("#upper_subaddons_error_div").show();
            return false;
        } else {
            $("#upper_subaddons_error_div").hide();
        }

        if (iUpperQty <= 0) {
            $("#upper_upperqty_error_div").show();
            return false;
        } else {
            $("#upper_upperqty_error_div").hide();
        }

        // if (upper_category_addons.length == 0 || upper_subaddons.length == 0 || iUpperQty.length == 0) {
        //     $("#upper_category_addons_error_div").show();
        //     return false;
        // }else{
        //     $("#upper_category_addons_error_div").hide();
        // }

        upper_addon_category_name = $("#upper_category_addons").text();
        upper_subaddons_name = $("#upper_subaddons").text();


        // alert($("#upperAddOnType").val());
        if ($("#upperAddOnType").val() == "Add") {

            $("#upper_category_addons_div").append('<div class="row" id="upper_addons_list_' + upperaddons + '"><div class="col-lg-4 pe-0"><input type="text" name="upper_add_on_cat[]" data-name="' + upper_category_addons + '" id="upper_add_on_cat_' + upperaddons + '" readonly class="w-100 h-100 qty-input" value="' + upper_addon_category_name + '"><input type="hidden" name="upper_add_on_cat_id[]"  id="upper_add_on_cat"  class="w-100 h-100 qty-input" value="' + upper_category_addons + '"></div><div class="col-lg-4 p-0"><input type="text" name="upper_add_on[]" data-name="' + upper_subaddons + '" id="upper_add_on_' + upperaddons + '" readonly class="w-100 h-100 qty-input" value="' + upper_subaddons_name + '"><input type="hidden" name="upper_add_on_id[]" id="upper_add_on"  class="w-100 h-100 qty-input" value="' + upper_subaddons + '"></div> <div class="col-lg-2 p-0"> <input id="upper_add_on_qty_' + upperaddons + '" type="number" placeholder="Qty" name="upper_add_on_qty[]" class="w-100 h-100 qty-input" value="' + iUpperQty + '"> </div> <div class="col-lg-1 align-self-center p-0"> <a href="javascript:;" data-id="' + upperaddons + '" class="btn-icon edit_permission edit-icon upper_add_on_edit" data-id="' + upperaddons + '">  <i class="fad fa-pencil" style="font-size:12px;"></i> </a> </div>  <div class="col-lg-1 align-self-center p-0">  <a href="javascript:;" data-id="' + upperaddons + '" class="btn-icon delete-icon delete_permission delete_add_ons"> <i class="fad fa-trash-alt" style="font-size:12px;"></i> </a>  </div> </div>');
            upperaddons++;
        } else {

            upper_addons_edit = $("#upper_addons_edit").val();

            $("#upper_addons_list_" + upper_addons_edit).html('<div class="col-lg-4 pe-0"><input type="text" name="upper_add_on_cat[]" data-name="' + upper_category_addons + '" id="upper_add_on_cat_' + upper_addons_edit + '" readonly class="w-100 h-100 qty-input" value="' + upper_addon_category_name + '"></div><div class="col-lg-4 p-0"><input type="text" name="upper_add_on[]" data-name="' + upper_subaddons + '" id="upper_add_on_' + upper_addons_edit + '" readonly class="w-100 h-100 qty-input" value="' + upper_subaddons_name + '"></div> <div class="col-lg-2 p-0"> <input id="upper_add_on_qty_' + upper_addons_edit + '" type="number" placeholder="Qty" name="upper_add_on_qty[]" class="w-100 h-100 qty-input" value="' + iUpperQty + '"> </div> <div class="col-lg-1 align-self-center p-0"> <a href="javascript:;" data-id="' + upper_addons_edit + '" class="btn-icon edit_permission edit-icon upper_add_on_edit" data-id="' + upper_addons_edit + '">  <i class="fad fa-pencil" style="font-size:12px;"></i> </a> </div>  <div class="col-lg-1 align-self-center p-0">  <a href="javascript:;" data-id="' + upper_addons_edit + '" class="btn-icon delete-icon delete_permission delete_add_ons"> <i class="fad fa-trash-alt" style="font-size:12px;"></i> </a>  </div>');
        }

        $("#upper_category_addons").val("");
        $('#upper_subaddons').selectize()[0].selectize.destroy();
        $('#upper_subaddons').html("");
        $('#upper_subaddons').selectize();
        $("#iUpperQty").val("");
        $('#upper_model').addClass("d-none");
        $("#upperAddOnType").val("Add");
        $("#upper_addons_edit").val("");
    });


    $(document).on('click', '.upper_add_on_edit', function() {
        id = $(this).data("id");
        cat = $("#upper_add_on_cat_" + id).data('name');
        add_ons = $("#upper_add_on_" + id).data('name');
        qty = $("#upper_add_on_qty_" + id).val();

        $("#upper_addons_edit").val(id);
        $('#upper_category_addons').data('selectize').setValue(cat);
        $("#iUpperQty").val(qty);
        $("#upperAddOnType").val("Edit");

        $.ajax({
            url: "{{route('admin.labcase.getcategoryaddons')}}",
            type: "post",
            data: {
                iAddCategoryId: cat,
                eType: "Upper",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#upper_subaddons').selectize()[0].selectize.destroy();
                $('#upper_subaddons').html(result);
                $('#upper_subaddons').selectize();
                $('#upper_subaddons').data('selectize').setValue(add_ons);
                $('#upper_model').removeClass("d-none");
            }
        });

    });
    $(document).on('click', '.delete_add_ons', function() {
        id = $(this).data("id");
        $("#upper_addons_list_" + id).remove();
    });


    var loweraddons = 0;
    $(document).on('click', '#lower_submit', function() {

        lower_category_addons = $("#lower_category_addons").val();
        lower_subaddons = $("#lower_subaddons").val();
        iLowerQty = $("#iLowerQty").val();

        if (lower_category_addons.length == 0) {
            $("#lower_category_addons_error_div").show();
            return false;
        } else {
            $("#lower_category_addons_error_div").hide();
        }

        if (lower_subaddons.length == 0) {
            $("#lower_subaddons_error_div").show();
            return false;
        } else {
            $("#lower_subaddons_error_div").hide();
        }
        if (iLowerQty <= 0) {
            $("#lower_lowerqty_error_div").show();
            return false;

        } else {
            $("#lower_lowerqty_error_div").hide();
        }


        $("#category_addons_div").show();

        lower_addon_category_name = $("#lower_category_addons").text();
        lower_subaddons_name = $("#lower_subaddons").text();


        if ($("#lowerAddOnType").val() == "Add") {

            $("#lower_category_addons_div").append('<div class="row" id="lower_addons_list_' + loweraddons + '"><div class="col-lg-4 pe-0"><input type="text" name="lower_add_on_cat[]" data-name="' + lower_category_addons + '" id="lower_add_on_cat_' + loweraddons + '" readonly class="w-100 h-100 qty-input" value="' + lower_addon_category_name + '">   <input type="hidden" name="lower_add_on_cat_id[]"  id="lower_add_on_cat"  class="w-100 h-100 qty-input" value="' + lower_category_addons + '"></div><div class="col-lg-4 p-0"><input type="text" name="lower_add_on[]" data-name="' + lower_subaddons + '" id="lower_add_on_' + loweraddons + '" readonly class="w-100 h-100 qty-input" value="' + lower_subaddons_name + '"> <input type="hidden" name="lower_add_on_id[]"  id="lower_add_on_cat"  class="w-100 h-100 qty-input" value="' + lower_subaddons + '"></div> <div class="col-lg-2 p-0"> <input id="lower_add_on_qty_' + loweraddons + '" type="number" placeholder="Qty" name="lower_add_on_qty[]" class="w-100 h-100 qty-input" value="' + iLowerQty + '"> </div> <div class="col-lg-1 align-self-center p-0"> <a href="javascript:;" data-id="' + loweraddons + '" class="btn-icon edit_permission edit-icon lower_add_on_edit" data-id="' + loweraddons + '">  <i class="fad fa-pencil" style="font-size:12px;"></i> </a> </div>  <div class="col-lg-1 align-self-center p-0">  <a href="javascript:;" data-id="' + loweraddons + '" class="btn-icon delete-icon delete_permission delete_lower_add_ons"> <i class="fad fa-trash-alt" style="font-size:12px;"></i> </a>  </div> </div>');
            loweraddons++;
        } else {

            lower_addons_edit = $("#lower_addons_edit").val();

            $("#lower_addons_list_" + lower_addons_edit).html('<div class="col-lg-4 pe-0"><input type="text" name="lower_add_on_cat[]" data-name="' + lower_category_addons + '" id="lower_add_on_cat_' + lower_addons_edit + '" readonly class="w-100 h-100 qty-input" value="' + lower_addon_category_name + '"></div><div class="col-lg-4 p-0"><input type="text" name="lower_add_on[]" data-name="' + lower_subaddons + '" id="lower_add_on_' + lower_addons_edit + '" readonly class="w-100 h-100 qty-input" value="' + lower_subaddons_name + '"></div> <div class="col-lg-2 p-0"> <input id="lower_add_on_qty_' + lower_addons_edit + '" type="number" placeholder="Qty" name="lower_add_on_qty[]" class="w-100 h-100 qty-input" value="' + iLowerQty + '"> </div> <div class="col-lg-1 align-self-center p-0"> <a href="javascript:;" data-id="' + lower_addons_edit + '" class="btn-icon edit_permission edit-icon lower_add_on_edit" data-id="' + lower_addons_edit + '">  <i class="fad fa-pencil" style="font-size:12px;"></i> </a> </div>  <div class="col-lg-1 align-self-center p-0">  <a href="javascript:;" data-id="' + lower_addons_edit + '" class="btn-icon delete-icon delete_permission delete_lower_add_ons"> <i class="fad fa-trash-alt" style="font-size:12px;"></i> </a>  </div>');

        }

        $('#lower_subaddons').selectize()[0].selectize.destroy();
        $('#lower_subaddons').html("");
        $('#lower_subaddons').selectize();
        $("#lowerAddOnType").val("Add");
        $("#lower_addons_edit").val("");
        $("#lower_category_addons").val("");
        $("#ilowerQty").val("");
        $('#lower_model').addClass("d-none");


    });


    $(document).on('click', '.lower_add_on_edit', function() {
        id = $(this).data("id");
        cat = $("#lower_add_on_cat_" + id).data('name');
        add_ons = $("#lower_add_on_" + id).data('name');
        qty = $("#lower_add_on_qty_" + id).val();

        $("#lower_addons_edit").val(id);
        $('#lower_category_addons').data('selectize').setValue(cat);
        $("#iLowerQty").val(qty);
        $("#lowerAddOnType").val("Edit");

        $.ajax({
            url: "{{route('admin.labcase.getcategoryaddons')}}",
            type: "post",
            data: {
                iAddCategoryId: cat,
                eType: "Lower",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#lower_subaddons').selectize()[0].selectize.destroy();
                $('#lower_subaddons').html(result);
                $('#lower_subaddons').selectize();
                $('#lower_subaddons').data('selectize').setValue(add_ons);
                $('#lower_model').removeClass("d-none");
            }
        });
    });
    $(document).on('click', '.delete_lower_add_ons', function() {
        id = $(this).data("id");
        $("#lower_addons_list_" + id).remove();
    });
</script>

<script>
    function upper_reset_extraction() {
        upper_in_mouth_array = [];
        upper_missing_teeth_array = [];
        upper_ectract_delivery_array = [];
        upper_been_extracted_array = [];
        upper_fix_array = [];
        upper_clasps_array = [];

        for (i = 1; i <= 16; i++) {

            $(".upper-" + i + "-class-teeth-yellow").show();

            $(".upper-" + i + "-class-teeth-grey").hide();
            $(".upper-" + i + "-class-teeth-red").hide();
            $(".upper-" + i + "-class-teeth-white").hide();
            $(".upper-" + i + "-class-teeth-green").hide();

        }

        $(".upper-claps").hide();


        $("#upper_teeth_in_mouth_error").hide();
        $("#upper_missing_teeth_error").hide();
        $("#upper_will_extract_on_delivery_error").hide();
        $("#upper_has_been_extracted_error").hide();
        $("#upper_fix_or_add_error").hide();
        $("#upper_clasps_error").hide();

        $("#upper_in_mouth_lbl").text('');
        $("#upper_ectract_delivery_lbl").text('');
        $("#upper_been_extracted_lbl").text('');
        $("#upper_fix_lbl").text('');
        $("#upper_clasps_lbl").text('');
        $("#upper_missing_teeth_lbl").text('');

        $("#upper_in_mouth_value").val("");
        $("#upper_missing_teeth_value").val("");
        $("#upper_ectract_delivery_value").val("");
        $("#upper_been_extracted_value").val("");
        $("#upper_fix_value").val("");
        $("#upper_clasps_value").val("");

    }

    function lower_reset_extraction() {
        lower_in_mouth_array = [];
        lower_missing_teeth_array = [];
        lower_ectract_delivery_array = [];
        lower_been_extracted_array = [];
        lower_fix_array = [];
        lower_clasps_array = [];

        for (i = 17; i <= 32; i++) {

            $(".lower-" + i + "-class-teeth-yellow").show();

            $(".lower-" + i + "-class-teeth-grey").hide();
            $(".lower-" + i + "-class-teeth-red").hide();
            $(".lower-" + i + "-class-teeth-white").hide();
            $(".lower-" + i + "-class-teeth-green").hide();

        }

        $(".lower-claps").hide();

        $("#lower_teeth_in_mouth_error").hide();
        $("#lower_missing_teeth_error").hide();
        $("#lower_will_extract_on_delivery_error").hide();
        $("#lower_has_been_extracted_error").hide();
        $("#lower_fix_or_add_error").hide();
        $("#lower_clasps_error").hide();

        $("#lower_in_mouth_lbl").text('');
        $("#lower_missing_teeth_lbl").text('');
        $("#lower_ectract_delivery_lbl").text('');
        $("#lower_been_extracted_lbl").text('');
        $("#lower_fix_lbl").text('');
        $("#lower_clasps_lbl").text('');

        $("#lower_in_mouth_value").val("");
        $("#lower_missing_teeth_value").val("");
        $("#lower_ectract_delivery_value").val("");
        $("#lower_been_extracted_value").val("");
        $("#lower_fix_value").val("");
        $("#lower_clasps_value").val("");
    }

    function upper_reset_Impression() {
        ImpressionId = [];
        upper_impsn = [];

        $(".upper_impression_checked").prop("checked", false);
        $(".upper_impressions_qtys").val("");
        $(".upper_impressions_qtys").hide();

        $("#upper_impressions").val("");
        $("#upper_impression_dropdown").removeClass("open");
        $('#category_addons_model_div').hide();
        $('#upper_addons_model_div').hide();
        $('#upper_addons_div').hide();
        $('#upper_status_div').hide();
        $('#submit_div').hide();
        $('.notes_div').hide();
        $('#notes_lable_div').hide();
        $('#upper_category_addons_div').html("");
    }

    function lower_reset_Impression() {
        LowerImpressionId = [];
        lower_impsn = [];


        $(".lower_impression_checked").prop("checked", false);
        $(".lower_impressions_qtys").val("");
        $(".lower_impressions_qtys").hide();

        $("#lower_impressions").val("");
        $("#lower_impression_dropdown").removeClass("open");
        $('#lower_addons_div').hide();
        $('#lower_category_addons_model_div').hide();
        $('#category_addons_model_div').hide();
        $('#lower_addons').hide();
        $('#lower_status_div').hide();
        $('#submit_div').hide();
        $('#lower_category_addons_div').html("");

    }

    function upper_default_Impression(default_for, lower) {

        upper_reset_Impression();

        if (lower == "Yes") {
            lower_reset_Impression();
        }

    }

    function lower_default_Impression(default_for, upper) {

        lower_reset_Impression();

        if (upper == "Yes") {
            upper_reset_Impression();
        }
    }


    function upper_default_extraction(default_for) {

        upper_reset_extraction();

        if (default_for == "upper_eTeethInDefault") {

            upper_in_mouth_array = [];
            $("#upper_in_mouth_lbl").addClass("active");

            for (i = 1; i <= 16; i++) {
                upper_in_mouth_array.push(parseInt(i));
                // $("#upper-" + i + "-teeth").addClass("in-mouth");

                $(".upper-" + i + "-class-teeth-yellow").show();

                $(".upper-" + i + "-class-teeth-grey").hide();
                $(".upper-" + i + "-class-teeth-red").hide();
                $(".upper-" + i + "-class-teeth-white").hide();
                $(".upper-" + i + "-class-teeth-green").hide();

            }
            upper_in_mouth_array = $.unique(upper_in_mouth_array.sort());
            upper_in_mouth_array.sort(function(a, b) {
                return a - b;
            });
            $("#upper_in_mouth_lbl").text(upper_in_mouth_array.join(","));

            $("#upper_in_mouth_value").val(upper_in_mouth_array.join(","));

        } else if (default_for == "upper_eMessingDefault") {

            upper_missing_teeth_array = [];
            $("#upper_missing_teeth_lbl").addClass("active");

            for (i = 1; i <= 16; i++) {
                upper_missing_teeth_array.push(parseInt(i));
                $(".upper-" + i + "-class-teeth-white").show();

                $(".upper-" + i + "-class-teeth-yellow").hide();
                $(".upper-" + i + "-class-teeth-red").hide();
                $(".upper-" + i + "-class-teeth-grey").hide();
                $(".upper-" + i + "-class-teeth-green").hide();

            }
            upper_missing_teeth_array = $.unique(upper_missing_teeth_array.sort());
            upper_missing_teeth_array.sort(function(a, b) {
                return a - b;
            });
            $("#upper_missing_teeth_lbl").text(upper_missing_teeth_array.join(","));
            $("#upper_missing_teeth_value").val(upper_missing_teeth_array.join(","));

        } else if (default_for == "upper_eExtractDefault") {

            upper_ectract_delivery_array = [];
            $("#upper_ectract_delivery_lbl").addClass("active");

            for (i = 1; i <= 16; i++) {
                upper_ectract_delivery_array.push(parseInt(i));

                $(".upper-" + i + "-class-teeth-red").show();

                $(".upper-" + i + "-class-teeth-yellow").hide();
                $(".upper-" + i + "-class-teeth-grey").hide();
                $(".upper-" + i + "-class-teeth-white").hide();
                $(".upper-" + i + "-class-teeth-green").hide();
            }
            upper_ectract_delivery_array = $.unique(upper_ectract_delivery_array.sort());
            upper_ectract_delivery_array.sort(function(a, b) {
                return a - b;
            });
            $("#upper_ectract_delivery_lbl").text(upper_ectract_delivery_array.join(","));
            $("#upper_ectract_delivery_value").val(upper_ectract_delivery_array.join(","));

        } else if (default_for == "upper_eExtractedDefault") {

            upper_been_extracted_array = [];
            $("#upper_been_extracted_lbl").addClass("active");

            for (i = 1; i <= 16; i++) {
                upper_been_extracted_array.push(parseInt(i));

                $(".upper-" + i + "-class-teeth-grey").show();

                $(".upper-" + i + "-class-teeth-yellow").hide();
                $(".upper-" + i + "-class-teeth-red").hide();
                $(".upper-" + i + "-class-teeth-white").hide();
                $(".upper-" + i + "-class-teeth-green").hide();

            }
            upper_been_extracted_array = $.unique(upper_been_extracted_array.sort());
            upper_been_extracted_array.sort(function(a, b) {
                return a - b;
            });
            $("#upper_been_extracted_lbl").text(upper_been_extracted_array.join(","));
            $("#upper_been_extracted_value").val(upper_been_extracted_array.join(","));


        } else if (default_for == "upper_eFixorAddDefault") {

            upper_fix_array = [];
            $("#upper_fix_lbl").addClass("active");

            for (i = 1; i <= 16; i++) {
                upper_fix_array.push(parseInt(i));

                $(".upper-" + i + "-class-teeth-green").show();

                $(".upper-" + i + "-class-teeth-yellow").hide();
                $(".upper-" + i + "-class-teeth-red").hide();
                $(".upper-" + i + "-class-teeth-white").hide();
                $(".upper-" + i + "-class-teeth-grey").hide();
            }
            upper_fix_array = $.unique(upper_fix_array.sort());
            upper_fix_array.sort(function(a, b) {
                return a - b;
            });
            $("#upper_fix_lbl").text(upper_fix_array.join(","));
            $("#upper_fix_value").val(upper_fix_array.join(","));


        } else if (default_for == "upper_eClaspsDefault") {

            upper_clasps_array = [];
            $("#upper_clasps_lbl").addClass("active");

            for (i = 1; i <= 16; i++) {
                upper_clasps_array.push(parseInt(i));
                $("#upper-" + i + "-teeth").addClass("calps");
            }
            upper_clasps_array = $.unique(upper_clasps_array.sort());
            upper_clasps_array.sort(function(a, b) {
                return a - b;
            });
            $("#upper_clasps_lbl").text(upper_clasps_array.join(","));
            $("#upper_clasps_value").val(upper_clasps_array.join(","));
        }

        upper_extraction_validation();

    }

    function lower_opposite_default_extraction(default_for) {
        lower_reset_extraction();

        if (default_for == "lower_opposite_eTeethInDefault") {

            lower_in_mouth_array = [];
            $("#lower_in_mouth_lbl").addClass("active");

            for (i = 17; i <= 32; i++) {
                lower_in_mouth_array.push(parseInt(i));
                // $("#lower-" + i + "-teeth").addClass("in-mouth");

                $(".lower-" + i + "-class-teeth-yellow").show();

                $(".lower-" + i + "-class-teeth-green").hide();
                $(".lower-" + i + "-class-teeth-red").hide();
                $(".lower-" + i + "-class-teeth-white").hide();
                $(".lower-" + i + "-class-teeth-grey").hide();

                // $("#lower-" + i + "-teeth").attr("src","{{asset('admin/assets/images/yellow-teeth')}}/d-"+i+".png");
                // $("#lower-" + i + "-teeth").attr("xlink:href","{{asset('admin/assets/images/yellow-teeth')}}/d-"+i+".png");
            }

            lower_in_mouth_array = $.unique(lower_in_mouth_array.sort());
            lower_in_mouth_array.sort(function(a, b) {
                return a - b;
            });
            $("#lower_in_mouth_lbl").text(lower_in_mouth_array.join(","));
            $("#lower_in_mouth_value").val(lower_in_mouth_array.join(","));

        } else if (default_for == "lower_opposite_eMessingDefault") {

            lower_missing_teeth_array = [];
            $("#lower_missing_teeth_lbl").addClass("active");

            for (i = 17; i <= 32; i++) {
                lower_missing_teeth_array.push(parseInt(i));
                $(".lower-" + i + "-class-teeth-white").show();

                $(".lower-" + i + "-class-teeth-yellow").hide();
                $(".lower-" + i + "-class-teeth-red").hide();
                $(".lower-" + i + "-class-teeth-green").hide();
                $(".lower-" + i + "-class-teeth-grey").hide();
                // $("#lower-" + i + "-teeth").addClass("missing");
            }
            lower_missing_teeth_array = $.unique(lower_missing_teeth_array.sort());
            lower_missing_teeth_array.sort(function(a, b) {
                return a - b;
            });
            $("#lower_missing_teeth_lbl").text(lower_missing_teeth_array.join(","));
            $("#lower_missing_teeth_value").val(lower_missing_teeth_array.join(","));

        } else if (default_for == "lower_opposite_eExtractDefault") {

            lower_ectract_delivery_array = [];
            $("#lower_ectract_delivery_lbl").addClass("active");

            for (i = 17; i <= 32; i++) {
                lower_ectract_delivery_array.push(parseInt(i));

                $(".lower-" + i + "-class-teeth-red").show();

                $(".lower-" + i + "-class-teeth-yellow").hide();
                $(".lower-" + i + "-class-teeth-green").hide();
                $(".lower-" + i + "-class-teeth-white").hide();
                $(".lower-" + i + "-class-teeth-grey").hide();
                // $("#lower-" + i + "-teeth").addClass("will-extract-on-deliver");
                // $("#lower-" + i + "-teeth").attr("src","{{asset('admin/assets/images/red-teeth')}}/d-"+i+".png");
                // $("#lower-" + i + "-teeth").attr("xlink:href","{{asset('admin/assets/images/red-teeth')}}/d-"+i+".png");
            }
            lower_ectract_delivery_array = $.unique(lower_ectract_delivery_array.sort());
            lower_ectract_delivery_array.sort(function(a, b) {
                return a - b;
            });
            $("#lower_ectract_delivery_lbl").text(lower_ectract_delivery_array.join(","));
            $("#lower_ectract_delivery_value").val(lower_ectract_delivery_array.join(","));

        } else if (default_for == "lower_opposite_eExtractedDefault") {

            lower_been_extracted_array = [];
            $("#lower_been_extracted_lbl").addClass("active");

            for (i = 17; i <= 32; i++) {
                lower_been_extracted_array.push(parseInt(i));

                $(".lower-" + i + "-class-teeth-grey").show();

                $(".lower-" + i + "-class-teeth-yellow").hide();
                $(".lower-" + i + "-class-teeth-red").hide();
                $(".lower-" + i + "-class-teeth-white").hide();
                $(".lower-" + i + "-class-teeth-green").hide();
                // $("#lower-" + i + "-teeth").addClass("ben-extracted");
                // $("#lower-" + i + "-teeth").attr("src","{{asset('admin/assets/images/grey-teeth')}}/d-"+i+".png");
                // $("#lower-" + i + "-teeth").attr("xlink:href","{{asset('admin/assets/images/grey-teeth')}}/d-"+i+".png");
            }
            lower_been_extracted_array = $.unique(lower_been_extracted_array.sort());
            lower_been_extracted_array.sort(function(a, b) {
                return a - b;
            });
            $("#lower_been_extracted_lbl").text(lower_been_extracted_array.join(","));
            $("#lower_been_extracted_value").val(lower_been_extracted_array.join(","));

        } else if (default_for == "lower_opposite_eFixorAddDefault") {

            lower_fix_array = [];
            $("#lower_fix_lbl").addClass("active");

            for (i = 17; i <= 32; i++) {
                lower_fix_array.push(parseInt(i));
                $(".lower-" + i + "-class-teeth-green").show();

                $(".lower-" + i + "-class-teeth-yellow").hide();
                $(".lower-" + i + "-class-teeth-red").hide();
                $(".lower-" + i + "-class-teeth-white").hide();
                $(".lower-" + i + "-class-teeth-grey").hide();
                // $("#lower-" + i + "-teeth").addClass("add-or-fix");
                // $("#lower-" + i + "-teeth").attr("src","{{asset('admin/assets/images/green-teeth')}}/d-"+i+".png");
                // $("#lower-" + i + "-teeth").attr("xlink:href","{{asset('admin/assets/images/green-teeth')}}/d-"+i+".png");
            }
            lower_fix_array = $.unique(lower_fix_array.sort());
            lower_fix_array.sort(function(a, b) {
                return a - b;
            });
            $("#lower_fix_lbl").text(lower_fix_array.join(","));
            $("#lower_fix_value").val(lower_fix_array.join(","));

        } else if (default_for == "lower_opposite_eClaspsDefault") {

            lower_clasps_array = [];
            $("#lower_clasps_lbl").addClass("active");

            for (i = 17; i <= 32; i++) {
                lower_clasps_array.push(parseInt(i));
                $("#lower-" + i + "-teeth").addClass("calps");
            }
            lower_clasps_array = $.unique(lower_clasps_array.sort());
            lower_clasps_array.sort(function(a, b) {
                return a - b;
            });
            $("#lower_clasps_lbl").text(lower_clasps_array.join(","));
            $("#lower_clasps_value").val(lower_clasps_array.join(","));
        }

    }



    function lower_default_extraction(default_for) {

        lower_reset_extraction();

        if (default_for == "lower_eTeethInDefault") {

            lower_in_mouth_array = [];
            $("#lower_in_mouth_lbl").addClass("active");

            for (i = 17; i <= 32; i++) {
                lower_in_mouth_array.push(parseInt(i));
                // $("#lower-" + i + "-teeth").addClass("in-mouth");

                $(".lower-" + i + "-class-teeth-yellow").show();

                $(".lower-" + i + "-class-teeth-green").hide();
                $(".lower-" + i + "-class-teeth-red").hide();
                $(".lower-" + i + "-class-teeth-white").hide();
                $(".lower-" + i + "-class-teeth-grey").hide();

                // $("#lower-" + i + "-teeth").attr("src","{{asset('admin/assets/images/yellow-teeth')}}/d-"+i+".png");
                // $("#lower-" + i + "-teeth").attr("xlink:href","{{asset('admin/assets/images/yellow-teeth')}}/d-"+i+".png");
            }
            lower_in_mouth_array = $.unique(lower_in_mouth_array.sort());
            lower_in_mouth_array.sort(function(a, b) {
                return a - b;
            });
            $("#lower_in_mouth_lbl").text(lower_in_mouth_array.join(","));
            $("#lower_in_mouth_value").val(lower_in_mouth_array.join(","));

        } else if (default_for == "lower_eMessingDefault") {

            lower_missing_teeth_array = [];
            $("#lower_missing_teeth_lbl").addClass("active");

            for (i = 17; i <= 32; i++) {
                lower_missing_teeth_array.push(parseInt(i));

                $(".lower-" + i + "-class-teeth-white").show();

                $(".lower-" + i + "-class-teeth-yellow").hide();
                $(".lower-" + i + "-class-teeth-red").hide();
                $(".lower-" + i + "-class-teeth-green").hide();
                $(".lower-" + i + "-class-teeth-grey").hide();
                // $("#lower-" + i + "-teeth").addClass("missing");
            }
            lower_missing_teeth_array = $.unique(lower_missing_teeth_array.sort());
            lower_missing_teeth_array.sort(function(a, b) {
                return a - b;
            });
            $("#lower_missing_teeth_lbl").text(lower_missing_teeth_array.join(","));
            $("#lower_missing_teeth_value").val(lower_missing_teeth_array.join(","));


        } else if (default_for == "lower_eExtractDefault") {

            lower_ectract_delivery_array = [];
            $("#lower_ectract_delivery_lbl").addClass("active");

            for (i = 17; i <= 32; i++) {
                lower_ectract_delivery_array.push(parseInt(i));

                $(".lower-" + i + "-class-teeth-red").show();

                $(".lower-" + i + "-class-teeth-yellow").hide();
                $(".lower-" + i + "-class-teeth-green").hide();
                $(".lower-" + i + "-class-teeth-white").hide();
                $(".lower-" + i + "-class-teeth-grey").hide();
                // $("#lower-" + i + "-teeth").addClass("will-extract-on-deliver");
            }
            lower_ectract_delivery_array = $.unique(lower_ectract_delivery_array.sort());
            lower_ectract_delivery_array.sort(function(a, b) {
                return a - b;
            });
            $("#lower_ectract_delivery_lbl").text(lower_ectract_delivery_array.join(","));
            $("#lower_ectract_delivery_value").val(lower_ectract_delivery_array.join(","));


        } else if (default_for == "lower_eExtractedDefault") {

            lower_been_extracted_array = [];
            $("#lower_been_extracted_lbl").addClass("active");

            for (i = 17; i <= 32; i++) {
                lower_been_extracted_array.push(parseInt(i));
                $(".lower-" + i + "-class-teeth-grey").show();

                $(".lower-" + i + "-class-teeth-yellow").hide();
                $(".lower-" + i + "-class-teeth-red").hide();
                $(".lower-" + i + "-class-teeth-white").hide();
                $(".lower-" + i + "-class-teeth-green").hide();
                // $("#lower-" + i + "-teeth").addClass("ben-extracted");
            }
            lower_been_extracted_array = $.unique(lower_been_extracted_array.sort());
            lower_been_extracted_array.sort(function(a, b) {
                return a - b;
            });
            $("#lower_been_extracted_lbl").text(lower_been_extracted_array.join(","));
            $("#lower_been_extracted_value").val(lower_been_extracted_array.join(","));


        } else if (default_for == "lower_eFixorAddDefault") {

            lower_fix_array = [];
            $("#lower_fix_lbl").addClass("active");

            for (i = 17; i <= 32; i++) {
                lower_fix_array.push(parseInt(i));
                $(".lower-" + i + "-class-teeth-green").show();

                $(".lower-" + i + "-class-teeth-yellow").hide();
                $(".lower-" + i + "-class-teeth-red").hide();
                $(".lower-" + i + "-class-teeth-white").hide();
                $(".lower-" + i + "-class-teeth-grey").hide();
                // $("#lower-" + i + "-teeth").addClass("add-or-fix");
            }
            lower_fix_array = $.unique(lower_fix_array.sort());
            lower_fix_array.sort(function(a, b) {
                return a - b;
            });
            $("#lower_fix_lbl").text(lower_fix_array.join(","));
            $("#lower_fix_value").val(lower_fix_array.join(","));


        } else if (default_for == "lower_eClaspsDefault") {

            lower_clasps_array = [];
            $("#lower_clasps_lbl").addClass("active");

            for (i = 17; i <= 32; i++) {
                lower_clasps_array.push(parseInt(i));
                $("#lower-" + i + "-teeth").addClass("calps");
            }
            lower_clasps_array = $.unique(lower_clasps_array.sort());
            lower_clasps_array.sort(function(a, b) {
                return a - b;
            });
            $("#lower_clasps_lbl").text(lower_clasps_array.join(","));
            $("#lower_clasps_value").val(lower_clasps_array.join(","));

        }

        lower_extraction_validation();

    }

    function upper_opposite_default_extraction(default_for) {

        upper_reset_extraction();

        if (default_for == "upper_opposite_eTeethInDefault") {
            upper_in_mouth_array = [];
            $("#upper_in_mouth_lbl").addClass("active");

            for (i = 1; i <= 16; i++) {
                upper_in_mouth_array.push(parseInt(i));
                // $("#upper-" + i + "-teeth").addClass("in-mouth");

                $(".upper-" + i + "-class-teeth-yellow").show();
                // $("#upper-" + i + "-teeth").attr("src","{{asset('admin/assets/images/yellow-teeth')}}/up-"+i+".png");
                // $("#upper-" + i + "-teeth").attr("xlink:href","{{asset('admin/assets/images/yellow-teeth')}}/up-"+i+".png");

            }
            upper_in_mouth_array = $.unique(upper_in_mouth_array.sort());
            upper_in_mouth_array.sort(function(a, b) {
                return a - b;
            });
            $("#upper_in_mouth_lbl").text(upper_in_mouth_array.join(","));
            $("#upper_in_mouth_value").val(upper_in_mouth_array.join(","));

        } else if (default_for == "upper_opposite_eMessingDefault") {

            upper_missing_teeth_array = [];
            $("#upper_missing_teeth_lbl").addClass("active");

            for (i = 1; i <= 16; i++) {
                upper_missing_teeth_array.push(parseInt(i));
                $(".upper-" + i + "-class-teeth-white").show();
                $(".upper-" + i + "-class-teeth-yellow").hide();
                $(".upper-" + i + "-class-teeth-red").hide();
                $(".upper-" + i + "-class-teeth-grey").hide();
                $(".upper-" + i + "-class-teeth-green").hide();
                // $("#upper-" + i + "-teeth").addClass("missing");
            }
            upper_missing_teeth_array = $.unique(upper_missing_teeth_array.sort());
            upper_missing_teeth_array.sort(function(a, b) {
                return a - b;
            });
            $("#upper_missing_teeth_lbl").text(upper_missing_teeth_array.join(","));
            $("#upper_missing_teeth_value").val(upper_missing_teeth_array.join(","));

        } else if (default_for == "upper_opposite_eExtractDefault") {

            upper_ectract_delivery_array = [];
            $("#upper_ectract_delivery_lbl").addClass("active");

            for (i = 1; i <= 16; i++) {
                upper_ectract_delivery_array.push(parseInt(i));

                $(".upper-" + i + "-class-teeth-red").show();

                $(".upper-" + i + "-class-teeth-yellow").hide();
                $(".upper-" + i + "-class-teeth-white").hide();
                $(".upper-" + i + "-class-teeth-grey").hide();
                $(".upper-" + i + "-class-teeth-green").hide();
                // $("#upper-" + i + "-teeth").addClass("will-extract-on-deliver");
                // $("#upper-" + i + "-teeth").attr("src","{{asset('admin/assets/images/red-teeth')}}/up-"+i+".png");
                // $("#upper-" + i + "-teeth").attr("xlink:href","{{asset('admin/assets/images/red-teeth')}}/up-"+i+".png");
            }
            upper_ectract_delivery_array = $.unique(upper_ectract_delivery_array.sort());
            upper_ectract_delivery_array.sort(function(a, b) {
                return a - b;
            });
            $("#upper_ectract_delivery_lbl").text(upper_ectract_delivery_array.join(","));
            $("#upper_ectract_delivery_value").val(upper_ectract_delivery_array.join(","));


        } else if (default_for == "upper_opposite_eExtractedDefault") {

            upper_been_extracted_array = [];
            $("#upper_been_extracted_lbl").addClass("active");

            for (i = 1; i <= 16; i++) {
                upper_been_extracted_array.push(parseInt(i));

                $(".upper-" + i + "-class-teeth-grey").show();

                $(".upper-" + i + "-class-teeth-yellow").hide();
                $(".upper-" + i + "-class-teeth-red").hide();
                $(".upper-" + i + "-class-teeth-white").hide();
                $(".upper-" + i + "-class-teeth-green").hide();
                // $("#upper-" + i + "-teeth").addClass("ben-extracted");
                // $("#upper-" + i + "-teeth").attr("src","{{asset('admin/assets/images/grey-teeth')}}/up-"+i+".png");
                // $("#upper-" + i + "-teeth").attr("xlink:href","{{asset('admin/assets/images/grey-teeth')}}/up-"+i+".png");

            }
            upper_been_extracted_array = $.unique(upper_been_extracted_array.sort());
            upper_been_extracted_array.sort(function(a, b) {
                return a - b;
            });
            $("#upper_been_extracted_lbl").text(upper_been_extracted_array.join(","));
            $("#upper_been_extracted_value").val(upper_been_extracted_array.join(","));


        } else if (default_for == "upper_opposite_eFixorAddDefault") {

            upper_fix_array = [];
            $("#upper_fix_lbl").addClass("active");

            for (i = 1; i <= 16; i++) {
                upper_fix_array.push(parseInt(i));
                $(".upper-" + i + "-class-teeth-green").show();

                $(".upper-" + i + "-class-teeth-yellow").hide();
                $(".upper-" + i + "-class-teeth-red").hide();
                $(".upper-" + i + "-class-teeth-white").hide();
                $(".upper-" + i + "-class-teeth-grey").hide();
                // $("#upper-" + i + "-teeth").addClass("add-or-fix");
                // $("#upper-" + i + "-teeth").attr("src","{{asset('admin/assets/images/green-teeth')}}/up-"+i+".png");
                // $("#upper-" + i + "-teeth").attr("xlink:href","{{asset('admin/assets/images/green-teeth')}}/up-"+i+".png");
            }
            upper_fix_array = $.unique(upper_fix_array.sort());
            upper_fix_array.sort(function(a, b) {
                return a - b;
            });
            $("#upper_fix_lbl").text(upper_fix_array.join(","));
            $("#upper_fix_value").val(upper_fix_array.join(","));


        } else if (default_for == "upper_opposite_eClaspsDefault") {


            upper_clasps_array = [];
            $("#upper_clasps_lbl").addClass("active");

            for (i = 1; i <= 16; i++) {
                upper_clasps_array.push(parseInt(i));
                $("#upper-" + i + "-teeth").addClass("calps");
            }
            upper_clasps_array = $.unique(upper_clasps_array.sort());
            upper_clasps_array.sort(function(a, b) {
                return a - b;
            });
            $("#upper_clasps_lbl").text(upper_clasps_array.join(","));
            $("#upper_clasps_value").val(upper_clasps_array.join(","));

        }
    }

    // Upper Teeth
    upper_in_mouth_array = [];
    upper_missing_teeth_array = [];
    upper_ectract_delivery_array = [];
    upper_been_extracted_array = [];
    upper_fix_array = [];
    upper_clasps_array = [];

    $(document).on('click', '.lbl_btn_all', function() {


        upper_teethshade_stage = $("#upper_teethshade_stage").val();
        upper_gumshade_stage = $("#upper_gumshade_stage").val();

        if (upper_teethshade_stage == "Yes") {
            $('#teeth_shades_div').show();
            $('#teethshade_lbl').show();
            $('#upper_tsbrand_div').show();
        } else if (upper_gumshade_stage == "Yes") {
            $('#gum_shades_div').show();
            $('#gumshade_lbl').show();
            $('#upper_gsbrand_div').show();
        }


        $(".lbl_btn_all").removeClass("selected-button-upper");
        $(this).addClass("selected-button-upper");
        $(".lbl_btn_all").removeClass("active");
        $(this).addClass('active');
    });
    $(document).on('click', '.upper_mising_all', function() {


        upper_in_mouth_array = [];
        upper_missing_teeth_array = [];
        upper_ectract_delivery_array = [];
        upper_been_extracted_array = [];
        upper_fix_array = [];
        upper_clasps_array = [];

        $('.teeth_upper').addClass("missing");
        $(".upper-claps").hide();
        $(".lbl_btn_all").removeClass("active");

        for (i = 1; i <= 16; i++) {
            upper_missing_teeth_array.push(parseInt(i));
            $(".upper-" + i + "-class-teeth-white").show();
            $(".upper-" + i + "-class-teeth-yellow").hide();
            $(".upper-" + i + "-class-teeth-red").hide();
            $(".upper-" + i + "-class-teeth-grey").hide();
            $(".upper-" + i + "-class-teeth-green").hide();
        }

        $('.teeth_upper').removeClass("in-mouth");
        $('.teeth_upper').removeClass("will-extract-on-deliver");
        $('.teeth_upper').removeClass("ben-extracted");
        $('.teeth_upper').removeClass("add-or-fix");
        $('.teeth_upper').removeClass("calps");


        $("#upper_in_mouth_lbl").text('');
        $("#upper_ectract_delivery_lbl").text('');
        $("#upper_been_extracted_lbl").text('');
        $("#upper_fix_lbl").text('');
        $("#upper_clasps_lbl").text('');

        $("#upper_in_mouth_value").val('');
        $("#upper_ectract_delivery_value").val('');
        $("#upper_been_extracted_value").val('');
        $("#upper_fix_value").val('');
        $("#upper_clasps_value").val('');

        upper_missing_teeth_array = $.unique(upper_missing_teeth_array.sort());
        upper_missing_teeth_array.sort(function(a, b) {
            return a - b;
        });


        $("#upper_missing_teeth_lbl").text(upper_missing_teeth_array.join(","));
        $("#upper_missing_teeth_value").val(upper_missing_teeth_array.join(","));

        upper_extraction_validation();

    });

    function upper_extraction_validation() {
        upper_extraction_error = false;

        $("#upper_teeth_in_mouth_error").hide();
        $("#upper_missing_teeth_error").hide();
        $("#upper_will_extract_on_delivery_error").hide();
        $("#upper_has_been_extracted_error").hide();
        $("#upper_fix_or_add_error").hide();
        $("#upper_clasps_error").hide();

        upper_eTeethInOptional = $("#upper_eTeethInOptional").val();
        upper_eMessingOptional = $("#upper_eMessingOptional").val();
        upper_eExtractOptional = $("#upper_eExtractOptional").val();
        upper_eExtractedOptional = $("#upper_eExtractedOptional").val();
        upper_eFixorAddOptional = $("#upper_eFixorAddOptional").val();
        upper_eClaspsOptional = $("#upper_eClaspsOptional").val();

        upper_eTeethInMouth_error = false;
        upper_eMessingTeeth_error = false;
        upper_eExtractDelivery_error = false;
        upper_eExtracted_error = false;
        upper_eFixorAdd_error = false;
        upper_eClasps_error = false;

        upper_optional = [];
        upper_required = [];
        upper_required_2 = [];


        if ($("#upper_eTeethInMouth").val() == "Yes") {
            if ($("#upper_eTeethInRequird").val() == "Yes") {
                upper_required.push(1);
                if (upper_eTeethInOptional == "Yes") {
                    upper_optional.push(1);
                }
                if ($("#upper_in_mouth_value").val().length == 0) {
                    upper_required_2.push(1);
                    upper_extraction_error = true;
                    $("#upper_teeth_in_mouth_error").show();
                }
            }
        }
        if ($("#upper_eMessingTeeth").val() == "Yes") {
            if ($("#upper_eMessingRequird").val() == "Yes") {
                upper_required.push(2);
                if (upper_eMessingOptional == "Yes") {
                    upper_optional.push(2);
                }
                if ($("#upper_missing_teeth_value").val().length == 0) {
                    upper_required_2.push(2);
                    upper_extraction_error = true;
                    $("#upper_missing_teeth_error").show();
                }
            }
        }
        if ($("#upper_eExtractDelivery").val() == "Yes") {
            if ($("#upper_eExtractRequird").val() == "Yes") {
                upper_required.push(3);
                if (upper_eExtractOptional == "Yes") {
                    upper_optional.push(3);
                }
                if ($("#upper_ectract_delivery_value").val().length == 0) {
                    upper_required_2.push(3);
                    upper_extraction_error = true;
                    $("#upper_will_extract_on_delivery_error").show();
                }
            }
        }
        if ($("#upper_eExtracted").val() == "Yes") {
            if ($("#upper_eExtractedRequird").val() == "Yes") {
                upper_required.push(4);
                if (upper_eExtractedOptional == "Yes") {
                    upper_optional.push(4);
                }
                if ($("#upper_been_extracted_value").val().length == 0) {
                    upper_required_2.push(4);
                    upper_extraction_error = true;
                    $("#upper_has_been_extracted_error").show();
                }
            }
        }
        if ($("#upper_eFixorAdd").val() == "Yes") {
            if ($("#upper_eFixorAddRequird").val() == "Yes") {
                upper_required.push(5);
                if (upper_eFixorAddOptional == "Yes") {
                    upper_optional.push(5);
                }
                if ($("#upper_fix_value").val().length == 0) {
                    upper_required_2.push(5);
                    upper_extraction_error = true;
                    $("#upper_fix_or_add_error").show();
                }
            }
        }
        if ($("#upper_eClasps").val() == "Yes") {
            if ($("#upper_eClaspsRequird").val() == "Yes") {
                upper_required.push(6);
                if (upper_eClaspsOptional == "Yes") {
                    upper_optional.push(6);
                }
                if ($("#upper_clasps_value").val().length == 0) {
                    upper_required_2.push(6);
                    upper_extraction_error = true;
                    $("#upper_clasps_error").show();
                }
            }
        }

        upper_optional_error = false;
        if (upper_optional.length > 0) {
            upper_optional_error = true;
        }

        $.each(upper_optional, function(key, value) {
            if ($.inArray(value, upper_required) != -1) {
                if (value == 1) {
                    if ($("#upper_in_mouth_value").val().length != 0) {
                        if (upper_extraction_optional_validation()) {
                            upper_optional_error = false;
                        }
                    }
                }
                if (value == 2) {
                    if ($("#upper_missing_teeth_value").val().length != 0) {
                        if (upper_extraction_optional_validation()) {
                            upper_optional_error = false;
                        }
                    }
                }
                if (value == 3) {
                    if ($("#upper_ectract_delivery_value").val().length != 0) {
                        if (upper_extraction_optional_validation()) {
                            upper_optional_error = false;
                        }
                    }
                }
                if (value == 4) {
                    if ($("#upper_been_extracted_value").val().length != 0) {
                        if (upper_extraction_optional_validation()) {
                            upper_optional_error = false;
                        }
                    }
                }
                if (value == 5) {
                    if ($("#upper_fix_value").val().length != 0) {
                        if (upper_extraction_optional_validation()) {
                            upper_optional_error = false;
                        }
                    }
                }
                if (value == 6) {
                    if ($("#upper_clasps_value").val().length != 0) {
                        if (upper_extraction_optional_validation()) {
                            upper_optional_error = false;
                        }
                    }
                }
            }
        });

        if (upper_optional_error == false) {
            upper_final_validation = [];
            upper_final_validation = upper_required_2.filter(function(val) {
                return upper_optional.indexOf(val) == -1;
            });

            if (upper_final_validation.length > 0) {
                upper_extraction_error = true;
            } else {
                upper_extraction_error = false;
            }

            if (upper_extraction_error == true) {
                $("#upper_extraction_error").val("true");

                if ($('#lower_extraction_stage').val() != "Yes") {
                    $("#lower_extraction").hide();
                }
            } else {
                $("#upper_extraction_error").val("false");

                if ($('#lower_eOpposingExtractions').val() == "Yes" && $("#lower_extraction_stage").val() != "Yes") {
                    lower_opposite_extraction_inner_hideshow();
                    $("#lower_extraction").show();
                    // upper_based_lower_extraction();
                }

                if ($("#upper_extraction_stage").val() == "Yes") {
                    $('#category_addons_div').show();
                    $('#category_addons_model_div').show();
                    $('#upper_category_addons_div').show();
                    $('#stage_status_div').show();
                    $('#submit_div').show();
                    $('#status_lbl').show();
                    $('.notes_div').show();
                    $('#upper_addons_model_div').show();
                    $('#upper_addons_div').show();
                    $('#upper_status_div').show();
                }
            }
        }

    }

    function upper_extraction_optional_validation() {

        $.each(upper_optional, function(key_internal, value_internal) {
            if (value_internal == 1) {
                $("#upper_teeth_in_mouth_error").hide();
            }
            if (value_internal == 2) {
                $("#upper_missing_teeth_error").hide();
            }
            if (value_internal == 3) {
                $("#upper_will_extract_on_delivery_error").hide();
            }
            if (value_internal == 4) {
                $("#upper_has_been_extracted_error").hide();
            }
            if (value_internal == 5) {
                $("#upper_fix_or_add_error").hide();
            }
            if (value_internal == 6) {
                $("#upper_clasps_error").hide();
            }
        });
        return true;
    }

    $(document).on('click', '.teeth_upper', function() {
        selected_tab = $(".selected-button-upper").attr("id");
        teeth_num = $(this).data("name");
        teeth_image = $(this).attr('id');
        if (selected_tab == "upper_in_mouth") {
            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
                // $("#upper-" + teeth_num + "-teeth-yellow").hide();

                $("#upper-" + teeth_num + "-claps").hide();

                if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                    var index = upper_clasps_array.indexOf(teeth_num);
                    if (index !== -1) {
                        upper_clasps_array.splice(index, 1);
                    }
                }

            } else {
                upper_in_mouth_array.push(teeth_num);

                $(".upper-" + teeth_num + "-class-teeth-yellow").show();

                $(".upper-" + teeth_num + "-class-teeth-green").hide();
                $(".upper-" + teeth_num + "-class-teeth-white").hide();
                $(".upper-" + teeth_num + "-class-teeth-red").hide();
                $(".upper-" + teeth_num + "-class-teeth-grey").hide();


            }

            // $(this).addClass("in-mouth");


            // $("#upper-" + teeth_num + "-teeth").removeClass("missing");
            // $("#upper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            // $("#upper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            // $("#upper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            // $("#upper-" + teeth_num + "-teeth").removeClass("calps");
        } else if (selected_tab == "upper_missing_teeth") {

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#upper-" + teeth_num + "-claps").hide();
            }


            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
                $("#upper-" + teeth_num + "-teeth-yellow").show();
                $("#upper-" + teeth_num + "-teeth-white").hide();
            } else {
                upper_missing_teeth_array.push(teeth_num);
                // $("#upper-" + teeth_num + "-teeth").addClass("missing");
                $(".upper-" + teeth_num + "-class-teeth-white").show();

                $(".upper-" + teeth_num + "-class-teeth-red").hide();
                $(".upper-" + teeth_num + "-class-teeth-green").hide();
                $(".upper-" + teeth_num + "-class-teeth-yellow").hide();
                $(".upper-" + teeth_num + "-class-teeth-grey").hide();
            }



            // $("#upper-"+teeth_num+"-teeth").addClass("active");

            // $("#upper-" + teeth_num + "-teeth").removeClass("will- -on-deliver");
            // $("#upper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            // $("#upper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            // $("#upper-" + teeth_num + "-teeth").removeClass("calps");
            // $("#upper-" + teeth_num + "-teeth").removeClass("in-mouth");
        } else if (selected_tab == "upper_ectract_delivery") {

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#upper-" + teeth_num + "-claps").hide();
            }

            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }

                $("#upper-" + teeth_num + "-teeth-red").hide();
                $("#upper-" + teeth_num + "-teeth-yellow").show();
            } else {
                upper_ectract_delivery_array.push(teeth_num);

                // alert($("#upper-" + teeth_num + "-teeth").prop('src'));
                $(".upper-" + teeth_num + "-class-teeth-red").show();

                $(".upper-" + teeth_num + "-class-teeth-green").hide();
                $(".upper-" + teeth_num + "-class-teeth-white").hide();
                $(".upper-" + teeth_num + "-class-teeth-yellow").hide();
                $(".upper-" + teeth_num + "-class-teeth-white").hide();
            }

            // $("#upper-"+teeth_num+"-teeth").addClass("active");

            // $("#upper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            // $("#upper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            // $("#upper-" + teeth_num + "-teeth").removeClass("calps");
            // $("#upper-" + teeth_num + "-teeth").removeClass("missing");
            // $("#upper-" + teeth_num + "-teeth").removeClass("in-mouth");
        } else if (selected_tab == "upper_been_extracted") {

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#upper-" + teeth_num + "-claps").hide();
            }
            
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
                $("#upper-" + teeth_num + "-teeth-grey").hide();
                $("#upper-" + teeth_num + "-teeth-yellow").show();
            } else {
                upper_been_extracted_array.push(teeth_num)

                $(".upper-" + teeth_num + "-class-teeth-grey").show();

                $(".upper-" + teeth_num + "-class-teeth-green").hide();
                $(".upper-" + teeth_num + "-class-teeth-white").hide();
                $(".upper-" + teeth_num + "-class-teeth-yellow").hide();
                $(".upper-" + teeth_num + "-class-teeth-red").hide();

            }


            $("#upper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            $("#upper-" + teeth_num + "-teeth").removeClass("calps");
            $("#upper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#upper-" + teeth_num + "-teeth").removeClass("missing");
            $("#upper-" + teeth_num + "-teeth").removeClass("in-mouth");

        } else if (selected_tab == "upper_fix") {

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#upper-" + teeth_num + "-claps").hide();
            }


            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }

                $("#upper-" + teeth_num + "-teeth-green").hide();
                $("#upper-" + teeth_num + "-teeth-yellow").show();
               
            } else {
                upper_fix_array.push(teeth_num)

                $(".upper-" + teeth_num + "-class-teeth-green").show();
                $(".upper-" + teeth_num + "-class-teeth-white").hide();
                $(".upper-" + teeth_num + "-class-teeth-yellow").hide();
                $(".upper-" + teeth_num + "-class-teeth-red").hide();
                $(".upper-" + teeth_num + "-class-teeth-grey").hide();
            }


            $("#upper-" + teeth_num + "-teeth").removeClass("calps");
            $("#upper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            $("#upper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#upper-" + teeth_num + "-teeth").removeClass("missing");
            $("#upper-" + teeth_num + "-teeth").removeClass("in-mouth");
        } else if (selected_tab == "upper_clasps") {

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#upper-" + teeth_num + "-claps").hide();
            } else if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                upper_clasps_array.push(teeth_num);
                $("#upper-" + teeth_num + "-claps").show();
            }
        }


        upper_in_mouth_array = $.unique(upper_in_mouth_array.sort());
        upper_missing_teeth_array = $.unique(upper_missing_teeth_array.sort());
        upper_ectract_delivery_array = $.unique(upper_ectract_delivery_array.sort());
        upper_been_extracted_array = $.unique(upper_been_extracted_array.sort());
        upper_fix_array = $.unique(upper_fix_array.sort());
        upper_clasps_array = $.unique(upper_clasps_array.sort());

        upper_in_mouth_array.sort(function(a, b) {
            return a - b;
        });
        upper_missing_teeth_array.sort(function(a, b) {
            return a - b;
        });
        upper_ectract_delivery_array.sort(function(a, b) {
            return a - b;
        });
        upper_been_extracted_array.sort(function(a, b) {
            return a - b;
        });
        upper_fix_array.sort(function(a, b) {
            return a - b;
        });
        upper_clasps_array.sort(function(a, b) {
            return a - b;
        });


        $("#upper_in_mouth_lbl").text(upper_in_mouth_array.join(","));
        $("#upper_missing_teeth_lbl").text(upper_missing_teeth_array.join(","));
        $("#upper_ectract_delivery_lbl").text(upper_ectract_delivery_array.join(","));
        $("#upper_been_extracted_lbl").text(upper_been_extracted_array.join(","));
        $("#upper_fix_lbl").text(upper_fix_array.join(","));
        $("#upper_clasps_lbl").text(upper_clasps_array.join(","));


        $("#upper_in_mouth_value").val(upper_in_mouth_array.join(","));
        $("#upper_missing_teeth_value").val(upper_missing_teeth_array.join(","));
        $("#upper_ectract_delivery_value").val(upper_ectract_delivery_array.join(","));
        $("#upper_been_extracted_value").val(upper_been_extracted_array.join(","));
        $("#upper_fix_value").val(upper_fix_array.join(","));
        $("#upper_clasps_value").val(upper_clasps_array.join(","));

        upper_extraction_validation();


    });

    // Lower Teeth ========================================================================================

    lower_in_mouth_array = [];
    lower_missing_teeth_array = [];
    lower_ectract_delivery_array = [];
    lower_been_extracted_array = [];
    lower_fix_array = [];
    lower_clasps_array = [];

    $(document).on('click', '.lower_lbl_btn_all', function() {


        lower_teethshade_stage = $("#lower_teethshade_stage").val();
        lower_gumshade_stage = $("#lower_gumshade_stage").val();

        if (lower_teethshade_stage == "Yes") {
            $('#teeth_shades_div').show();
            $('#teethshade_lbl').show();
            $('#lower_tsbrand_div').show();
        } else if (lower_gumshade_stage == "Yes") {
            $('#gum_shades_div').show();
            $('#gumshade_lbl').show();
            $('#lower_gsbrand_div').show();
        }


        $(".lower_lbl_btn_all").removeClass("selected-button-lower");
        $(this).addClass("selected-button-lower");
        $(".lower_lbl_btn_all").removeClass("active");
        $(this).addClass('active');
    });
    $(document).on('click', '.lower_mising_all', function() {

        lower_in_mouth_array = [];
        lower_missing_teeth_array = [];
        lower_ectract_delivery_array = [];
        lower_been_extracted_array = [];
        lower_fix_array = [];
        lower_clasps_array = [];

        $('.teeth_lower').addClass("missing");

        $(".lower-claps").hide();
        $(".lower_lbl_btn_all").removeClass("active");

        for (i = 17; i <= 32; i++) {
            lower_missing_teeth_array.push(parseInt(i));
            $(".lower-" + i + "-class-teeth-white").show();
            $(".lower-" + i + "-class-teeth-yellow").hide();
            $(".lower-" + i + "-class-teeth-red").hide();
            $(".lower-" + i + "-class-teeth-grey").hide();
            $(".lower-" + i + "-class-teeth-green").hide();
        }

        $("#lower_missing_teeth_lbl").text('17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32');

        $('.teeth_lower').removeClass("in-mouth");
        $('.teeth_lower').removeClass("will-extract-on-deliver");
        $('.teeth_lower').removeClass("ben-extracted");
        $('.teeth_lower').removeClass("add-or-fix");
        $('.teeth_lower').removeClass("calps");

        $("#lower_in_mouth_lbl").text('');
        $("#lower_ectract_delivery_lbl").text('');
        $("#lower_been_extracted_lbl").text('');
        $("#lower_fix_lbl").text('');
        $("#lower_clasps_lbl").text('');

        $("#lower_in_mouth_value").val('');
        $("#lower_ectract_delivery_value").val('');
        $("#lower_been_extracted_value").val('');
        $("#lower_fix_value").val('');
        $("#lower_clasps_value").val('');

        lower_missing_teeth_array = $.unique(lower_missing_teeth_array.sort());
        lower_missing_teeth_array.sort(function(a, b) {
            return a - b;
        });
        $("#lower_missing_teeth_lbl").text(lower_missing_teeth_array.join(","));
        $("#lower_missing_teeth_value").val(lower_missing_teeth_array.join(","));


        lower_extraction_validation();

    });

    function lower_extraction_validation() {
        lower_extraction_error = false;


        $("#lower_teeth_in_mouth_error").hide();
        $("#lower_missing_teeth_error").hide();
        $("#lower_will_extract_on_delivery_error").hide();
        $("#lower_has_been_extracted_error").hide();
        $("#lower_fix_or_add_error").hide();
        $("#lower_clasps_error").hide();

        lower_eTeethInOptional = $("#lower_eTeethInOptional").val();
        lower_eMessingOptional = $("#lower_eMessingOptional").val();
        lower_eExtractOptional = $("#lower_eExtractOptional").val();
        lower_eExtractedOptional = $("#lower_eExtractedOptional").val();
        lower_eFixorAddOptional = $("#lower_eFixorAddOptional").val();
        lower_eClaspsOptional = $("#lower_eClaspsOptional").val();

        lower_eTeethInMouth_error = false;
        lower_eMessingTeeth_error = false;
        lower_eExtractDelivery_error = false;
        lower_eExtracted_error = false;
        lower_eFixorAdd_error = false;
        lower_eClasps_error = false;

        lower_optional = [];
        lower_required = [];
        lower_required_2 = [];

        if ($("#lower_eTeethInMouth").val() == "Yes") {
            if ($("#lower_eTeethInRequird").val() == "Yes") {
                lower_required.push(1);
                if (lower_eTeethInOptional == "Yes") {
                    lower_optional.push(1);
                }
                if ($("#lower_in_mouth_value").val().length == 0) {
                    lower_required_2.push(1);
                    lower_extraction_error = true;
                    $("#lower_teeth_in_mouth_error").show();
                }
            }
        }

        if ($("#lower_eMessingTeeth").val() == "Yes") {
            if ($("#lower_eMessingRequird").val() == "Yes") {
                lower_required.push(2);
                if (lower_eMessingOptional == "Yes") {
                    lower_optional.push(2);
                }
                if ($("#lower_missing_teeth_value").val().length == 0) {
                    lower_required_2.push(2);
                    lower_extraction_error = true;
                    $("#lower_missing_teeth_error").show();
                }
            }
        }
        if ($("#lower_eExtractDelivery").val() == "Yes") {
            if ($("#lower_eExtractRequird").val() == "Yes") {
                lower_required.push(3);
                if (lower_eExtractOptional == "Yes") {
                    lower_optional.push(3);
                }
                if ($("#lower_ectract_delivery_value").val().length == 0) {
                    lower_required_2.push(3);
                    lower_extraction_error = true;
                    $("#lower_will_extract_on_delivery_error").show();
                }
            }
        }
        if ($("#lower_eExtracted").val() == "Yes") {
            if ($("#lower_eExtractedRequird").val() == "Yes") {
                lower_required.push(4);
                if (lower_eExtractedOptional == "Yes") {
                    lower_optional.push(4);
                }
                if ($("#lower_been_extracted_value").val().length == 0) {
                    lower_required_2.push(4);
                    lower_extraction_error = true;
                    $("#lower_has_been_extracted_error").show();
                }
            }
        }
        if ($("#lower_eFixorAdd").val() == "Yes") {
            if ($("#lower_eFixorAddRequird").val() == "Yes") {
                lower_required.push(5);
                if (lower_eFixorAddOptional == "Yes") {
                    lower_optional.push(5);
                }
                if ($("#lower_fix_value").val().length == 0) {
                    lower_required_2.push(5);
                    lower_extraction_error = true;
                    $("#lower_fix_or_add_error").show();
                }
            }
        }
        if ($("#lower_eClasps").val() == "Yes") {
            if ($("#lower_eClaspsRequird").val() == "Yes") {
                lower_required.push(6);
                if (lower_eClaspsOptional == "Yes") {
                    lower_optional.push(6);
                }
                if ($("#lower_clasps_value").val().length == 0) {
                    upper_required_2.push(6);
                    lower_extraction_error = true;
                    $("#lower_clasps_error").show();
                }
            }
        }

        lower_optional_error = false;
        if (lower_optional.length > 0) {
            lower_optional_error = true;
        }

        $.each(lower_optional, function(key, value) {
            if ($.inArray(value, lower_required) != -1) {
                if (value == 1) {
                    if ($("#lower_in_mouth_value").val().length != 0) {
                        if (lower_extraction_optional_validation()) {
                            lower_optional_error = false;
                        }
                    }
                }
                if (value == 2) {
                    if ($("#lower_missing_teeth_value").val().length != 0) {
                        if (lower_extraction_optional_validation()) {
                            lower_optional_error = false;
                        }
                    }
                }
                if (value == 3) {
                    if ($("#lower_ectract_delivery_value").val().length != 0) {
                        if (lower_extraction_optional_validation()) {
                            lower_optional_error = false;
                        }
                    }
                }
                if (value == 4) {
                    if ($("#lower_been_extracted_value").val().length != 0) {
                        if (lower_extraction_optional_validation()) {
                            lower_optional_error = false;
                        }
                    }
                }
                if (value == 5) {
                    if ($("#lower_fix_value").val().length != 0) {
                        if (lower_extraction_optional_validation()) {
                            lower_optional_error = false;
                        }
                    }
                }
                if (value == 6) {
                    if ($("#lower_clasps_value").val().length != 0) {
                        if (lower_extraction_optional_validation()) {
                            lower_optional_error = false;
                        }
                    }
                }
            }
        });

        if (lower_optional_error == false) {

            lower_final_validation = [];
            lower_final_validation = lower_required_2.filter(function(val) {
                return lower_optional.indexOf(val) == -1;
            });

            if (lower_final_validation.length > 0) {
                lower_extraction_error = true;
            } else {
                lower_extraction_error = false;
            }

            if (lower_extraction_error == false) {

                $("#lower_extraction_error").val("false");

                if ($("#upper_eOpposingExtractions").val() == "Yes" && $("#upper_extraction_stage").val() != "Yes") {
                    upper_opposite_extraction_inner_hideshow();
                    $('#upper_extraction').show();
                }

                if ($("#lower_extraction_stage").val() == "Yes") {
                    $('#category_addons_div').show();
                    $('#category_addons_model_div').show();
                    $('#lower_category_addons_div').show();
                    $('#stage_status_div').show();
                    $('#status_lbl').show();
                    $('#submit_div').show();
                    $('.notes_div').show();
                    $('#lower_category_addons_model_div').show();
                    $('#lower_addons_model_div').show();
                    $('#lower_addons_div').show();
                    $('#lower_addons').show();
                    $('#lower_status_div').show();
                }

            } else {
                $("#lower_extraction_error").val("true");

                if ($('#upper_extraction_stage').val() != "Yes") {
                    $('#upper_extraction').hide();
                }

            }
        }
    }

    function lower_extraction_optional_validation() {

        $.each(lower_optional, function(key_internal, value_internal) {
            if (value_internal == 1) {
                $("#lower_teeth_in_mouth_error").hide();
            }
            if (value_internal == 2) {
                $("#lower_missing_teeth_error").hide();
            }
            if (value_internal == 3) {
                $("#lower_will_extract_on_delivery_error").hide();
            }
            if (value_internal == 4) {
                $("#lower_has_been_extracted_error").hide();
            }
            if (value_internal == 5) {
                $("#lower_fix_or_add_error").hide();
            }
            if (value_internal == 6) {
                $("#lower_clasps_error").hide();
            }
        });
        return true;
    }

    $(document).on('click', '.teeth_lower', function() {
        lower_selected_tab = $(".selected-button-lower").attr("id");
        lower_teeth_num = $(this).data("name");
        teeth_image = $(this).attr('id')

        if (lower_selected_tab == "lower_in_mouth") {


            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
            }


            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {

                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }

                if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                    var index = lower_clasps_array.indexOf(lower_teeth_num);
                    if (index !== -1) {
                        lower_clasps_array.splice(index, 1);
                    }
                }

                $(".lower-" + lower_teeth_num + "-class-teeth-yellow").show();

                $(".lower-" + lower_teeth_num + "-class-teeth-green").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-white").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-red").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-grey").hide();

                // $("#lower-" + lower_teeth_num + "-teeth").attr("src", "{{asset('admin/assets/images/yellow-teeth')}}/d-" + lower_teeth_num + ".png");
                // $("#lower-" + lower_teeth_num + "-teeth").attr("xlink:href", "{{asset('admin/assets/images/yellow-teeth')}}/d-" + lower_teeth_num + ".png");
                $("#lower-" + lower_teeth_num + "-claps").hide();

            } else {
                lower_in_mouth_array.push(lower_teeth_num)
                $(".lower-" + lower_teeth_num + "-class-teeth-yellow").show();

                $(".lower-" + lower_teeth_num + "-class-teeth-green").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-white").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-red").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-grey").hide();
                // $("#lower-" + lower_teeth_num + "-teeth").attr("src", "{{asset('admin/assets/images/yellow-teeth')}}/d-" + lower_teeth_num + ".png");
                // $("#lower-" + lower_teeth_num + "-teeth").attr("xlink:href", "{{asset('admin/assets/images/yellow-teeth')}}/d-" + lower_teeth_num + ".png");
            }

            $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");

        } else if (lower_selected_tab == "lower_missing_teeth") {

            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            }


            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }

                $(".lower-" + lower_teeth_num + "-class-teeth-yellow").show();

                $(".lower-" + lower_teeth_num + "-class-teeth-white").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-red").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-grey").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-green").hide();
                // $("#lower-" + lower_teeth_num + "-teeth").attr("src", "{{asset('admin/assets/images/yellow-teeth')}}/d-" + lower_teeth_num + ".png");
                // $("#lower-" + lower_teeth_num + "-teeth").attr("xlink:href", "{{asset('admin/assets/images/yellow-teeth')}}/d-" + lower_teeth_num + ".png");
            } else {
                lower_missing_teeth_array.push(lower_teeth_num)

                $(".lower-" + lower_teeth_num + "-class-teeth-white").show();

                $(".lower-" + lower_teeth_num + "-class-teeth-yellow").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-red").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-grey").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-green").hide();
                // $("#lower-" + lower_teeth_num + "-teeth").attr("src", "{{asset('admin/assets/images/teeth')}}/d-" + lower_teeth_num + ".png");
                // $("#lower-" + lower_teeth_num + "-teeth").attr("xlink:href", "{{asset('admin/assets/images/teeth')}}/d-" + lower_teeth_num + ".png");
            }


            // $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
            // $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            // $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            // $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            // $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");
        } else if (lower_selected_tab == "lower_ectract_delivery") {

            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            }


            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }

                $(".lower-" + lower_teeth_num + "-class-teeth-yellow").show();

                $(".lower-" + lower_teeth_num + "-class-teeth-white").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-green").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-red").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-grey").hide();
                // $("#lower-" + lower_teeth_num + "-teeth").attr("src", "{{asset('admin/assets/images/yellow-teeth')}}/d-" + lower_teeth_num + ".png");
                // $("#lower-" + lower_teeth_num + "-teeth").attr("xlink:href", "{{asset('admin/assets/images/yellow-teeth')}}/d-" + lower_teeth_num + ".png");
            } else {
                lower_ectract_delivery_array.push(lower_teeth_num);

                $(".lower-" + lower_teeth_num + "-class-teeth-red").show();

                $(".lower-" + lower_teeth_num + "-class-teeth-yellow").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-white").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-green").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-grey").hide();
                // $("#lower-" + lower_teeth_num + "-teeth").attr("src", "{{asset('admin/assets/images/red-teeth')}}/d-" + lower_teeth_num + ".png");
                // $("#lower-" + lower_teeth_num + "-teeth").attr("xlink:href", "{{asset('admin/assets/images/red-teeth')}}/d-" + lower_teeth_num + ".png");
            }


            $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");
        } else if (lower_selected_tab == "lower_been_extracted") {


            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            }


            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);

                    $(".lower-" + lower_teeth_num + "-class-teeth-yellow").show();

                    $(".lower-" + lower_teeth_num + "-class-teeth-white").hide();
                    $(".lower-" + lower_teeth_num + "-class-teeth-red").hide();
                    $(".lower-" + lower_teeth_num + "-class-teeth-grey").hide();
                    $(".lower-" + lower_teeth_num + "-class-teeth-green").hide();
                }
                // $("#lower-" + lower_teeth_num + "-teeth").attr("src", "{{asset('admin/assets/images/yellow-teeth')}}/d-" + lower_teeth_num + ".png");
                // $("#lower-" + lower_teeth_num + "-teeth").attr("xlink:href", "{{asset('admin/assets/images/yellow-teeth')}}/d-" + lower_teeth_num + ".png");
            } else {
                lower_been_extracted_array.push(lower_teeth_num)

                $(".lower-" + lower_teeth_num + "-class-teeth-grey").show();

                $(".lower-" + lower_teeth_num + "-class-teeth-yellow").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-white").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-red").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-green").hide();
                // $("#lower-" + lower_teeth_num + "-teeth").attr("src", "{{asset('admin/assets/images/grey-teeth')}}/d-" + lower_teeth_num + ".png");
                // $("#lower-" + lower_teeth_num + "-teeth").attr("xlink:href", "{{asset('admin/assets/images/grey-teeth')}}/d-" + lower_teeth_num + ".png");
            }


            $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");
        } else if (lower_selected_tab == "lower_fix") {


            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            }


            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
                $(".lower-" + lower_teeth_num + "-class-teeth-yellow").show();

                $(".lower-" + lower_teeth_num + "-class-teeth-red").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-white").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-green").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-grey").hide();
                // $("#lower-" + lower_teeth_num + "-teeth").attr("src", "{{asset('admin/assets/images/green-teeth')}}/d-" + lower_teeth_num + ".png");
                // $("#lower-" + lower_teeth_num + "-teeth").attr("xlink:href", "{{asset('admin/assets/images/green-teeth')}}/d-" + lower_teeth_num + ".png");

            } else {
                lower_fix_array.push(lower_teeth_num)

                $(".lower-" + lower_teeth_num + "-class-teeth-green").show();

                $(".lower-" + lower_teeth_num + "-class-teeth-yellow").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-red").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-white").hide();
                $(".lower-" + lower_teeth_num + "-class-teeth-grey").hide();
                // $("#lower-" + lower_teeth_num + "-teeth").attr("src", "{{asset('admin/assets/images/green-teeth')}}/d-" + lower_teeth_num + ".png");
                // $("#lower-" + lower_teeth_num + "-teeth").attr("xlink:href", "{{asset('admin/assets/images/green-teeth')}}/d-" + lower_teeth_num + ".png");
            }


            $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");
        } else if (lower_selected_tab == "lower_clasps") {

            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            } else if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                lower_clasps_array.push(lower_teeth_num);
                $("#lower-" + lower_teeth_num + "-claps").show();
            }
        }


        lower_in_mouth_array = $.unique(lower_in_mouth_array.sort());
        lower_missing_teeth_array = $.unique(lower_missing_teeth_array.sort());
        lower_ectract_delivery_array = $.unique(lower_ectract_delivery_array.sort());
        lower_been_extracted_array = $.unique(lower_been_extracted_array.sort());
        lower_fix_array = $.unique(lower_fix_array.sort());
        lower_clasps_array = $.unique(lower_clasps_array.sort());


        lower_in_mouth_array.sort(function(a, b) {
            return a - b;
        });
        lower_missing_teeth_array.sort(function(a, b) {
            return a - b;
        });
        lower_ectract_delivery_array.sort(function(a, b) {
            return a - b;
        });
        lower_been_extracted_array.sort(function(a, b) {
            return a - b;
        });
        lower_fix_array.sort(function(a, b) {
            return a - b;
        });
        lower_clasps_array.sort(function(a, b) {
            return a - b;
        });


        $("#lower_in_mouth_lbl").text(lower_in_mouth_array.join(","));
        $("#lower_missing_teeth_lbl").text(lower_missing_teeth_array.join(","));
        $("#lower_ectract_delivery_lbl").text(lower_ectract_delivery_array.join(","));
        $("#lower_been_extracted_lbl").text(lower_been_extracted_array.join(","));
        $("#lower_fix_lbl").text(lower_fix_array.join(","));
        $("#lower_clasps_lbl").text(lower_clasps_array.join(","));


        $("#lower_in_mouth_value").val(lower_in_mouth_array.join(","));
        $("#lower_missing_teeth_value").val(lower_missing_teeth_array.join(","));
        $("#lower_ectract_delivery_value").val(lower_ectract_delivery_array.join(","));
        $("#lower_been_extracted_value").val(lower_been_extracted_array.join(","));
        $("#lower_fix_value").val(lower_fix_array.join(","));
        $("#lower_clasps_value").val(lower_clasps_array.join(","));

        lower_extraction_validation();


    });

    disableDates1 = "{{$holidays}}";
    disableDates2 = disableDates1.split(",");

    var disableDates = disableDates2;
    $('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
        daysOfWeekDisabled: [0, 5, 6],
        datesDisabled: disableDates,
    });

    $(document).on('click', '#submit_slip', function() {
        vPatientName = $("#vPatientName").val();
        vCreatedByName = $("#vCreatedByName").val();


        if (vPatientName.length == 0) {
            $("#vPatientName").focus();
        }
        if (vCreatedByName.length == 0) {
            $("#vCreatedByName").focus();
        }

    });
</script>
<script>
    $(document).on('click', '#informations_checkbox', function() {

        if ($("#informations_checkbox").is(':checked')) {
            $('#information_class').addClass("active");
            $('#information_class').removeClass("danger");
            $('#information_checkbox').val("Yes");
            $('#save_finish_later').css('display', 'none');
        } else {

            $('#save_finish_later').show();
            $('#information_checkbox').val("No");
            $('#information_class').removeClass("active");
            $('#information_class').removeClass("danger");
        }

    });
    $(document).on('click', '#save_finish_later', function() {

        iOfficeId = $("#iOfficeId").val();
        var error = false;

        // if (iOfficeId.length == 0) {
        //     $("#iOfficeId_error").show();
        //     return false;
        // } else {
        //     $("#iOfficeId_error").hide();

        // }


        setTimeout(function() {
            if (error == true) {
                return false;
            } else {
                $("#frm").submit();
                return true;
            }
        }, 1000);

    });

    function tConvert(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1); // Remove full string match value
            time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }

    $(document).on("click", "#upper_delete", function() {

        $("#upper_extraction_stage").val("No");
        $("#upper_gumshade_stage").val("No");
        $("#upper_teethshade_stage").val("No");
        $("#upper_Impression_stage").val("No");

        $("#upper_eTeethInMouth").val("No");
        $("#upper_eTeethInDefault").val("No");
        $("#upper_eTeethInRequird").val("No");

        $("#upper_eMessingDefault").val("No");
        $("#upper_eMessingRequird").val("No");
        $("#upper_eMessingTeeth").val("No");

        $("#upper_eExtractDefault").val("No");
        $("#upper_eExtractDelivery").val("No");
        $("#upper_eExtractRequird").val("No");

        $("#upper_eExtracted").val("No");
        $("#upper_eExtractedDefault").val("No");
        $("#upper_eExtractedRequird").val("No");

        $("#upper_eFixorAdd").val("No");
        $("#upper_eFixorAddDefault").val("No");
        $("#upper_eFixorAddRequird").val("No");

        $("#upper_eClasps").val("No");
        $("#upper_eClaspsDefault").val("No");
        $("#upper_eClaspsRequird").val("No");


        $("#lower_eOpposingExtractions").val("No");

        $("#lower_eOpposingTeethInMouth").val("No");
        $("#lower_eOpposingTeethInDefault").val("No");

        $("#lower_eOpposingMessingTeeth").val("No");
        $("#lower_eOpposingMessingDefault").val("No");

        $("#lower_eOpposingExtractDelivery").val("No");
        $("#lower_eOpposingExtractDefault").val("No");

        $("#lower_eOpposingExtracted").val("No");
        $("#lower_eOpposingExtractedDefault").val("No");

        $("#lower_eOpposingFixorAdd").val("No");
        $("#lower_eOpposingFixorAddDefault").val("No");

        $("#lower_eOpposingClasps").val("No");
        $("#lower_eOpposingClaspsDefault").val("No");

        upper_reset_extraction();
        upper_reset_Impression();

        if ($("#upper_eOpposingExtractions").val() == "Yes") {


            if ($("#upper_eOpposingTeethInDefault").val() == "Yes") {
                upper_opposite_default_extraction_var = "upper_opposite_eTeethInDefault";
            } else if ($("#upper_eOpposingMessingDefault").val() == "Yes") {
                upper_opposite_default_extraction_var = "upper_opposite_eMessingDefault";
            } else if ($("#upper_eOpposingExtractDefault").val() == "Yes") {
                upper_opposite_default_extraction_var = "upper_opposite_eExtractDefault";
            } else if ($("#upper_eOpposingExtractedDefault").val() == "Yes") {
                upper_opposite_default_extraction_var = "upper_opposite_eExtractedDefault";
            } else if ($("#upper_eOpposingFixorAddDefault").val() == "Yes") {
                upper_opposite_default_extraction_var = "upper_opposite_eFixorAddDefault";
            } else if ($("#upper_eOpposingClaspsDefault").val() == "Yes") {
                upper_opposite_default_extraction_var = "upper_opposite_eClaspsDefault";
            } else {
                upper_opposite_default_extraction_var = "none";
            }

            if ($("#upper_extraction_stage").val() != "") {
                upper_opposite_default_extraction(upper_opposite_default_extraction_var);
                upper_opposite_extraction_inner_hideshow();
            }

            // $('#lower_extraction').show();
        }


        $('#iUpperCategoryId').selectize()[0].selectize.destroy();
        $("#category_upper_div").hide();
        category_select_data("Upper");
        if ($("#iLowerCategoryId").val() == "" || $("#iLowerCategoryId").val().length == 0) {
            $("#category_div").hide();
            $("#cat_lbl").hide();
            category_select_data("Lower");
        }

        $('#iUpperProductId').selectize()[0].selectize.destroy();
        $('#iUpperProductId').val("");
        $("#product_upper_div").hide();
        if ($("#iLowerProductId").val() == "" || $("#iLowerProductId").val().length == 0) {
            $("#product_div").hide();
            $("#product_lbl").hide();
        }

        $('#iUpperGradeId').selectize()[0].selectize.destroy();
        $('#iUpperGradeId').val("");
        $("#grade_upper_div").hide();
        if ($("#iLowerGradeId").val() == "" || $("#iLowerGradeId").val().length == 0) {
            $("#grade_div").hide();
            $("#grade_lbl").hide();
        }


        $('#iUpperStageId').selectize()[0].selectize.destroy();
        $('#iUpperStageId').selectize();
        $('#iUpperStageId').data('selectize').setValue("");

        $("#stage_upper_div").hide();
        if ($("#iLowerStageId").val() == "" || $("#iLowerStageId").val().length == 0) {
            $("#stage_div").hide();
            $("#stage_lbl").hide();
        }

        $('#upper_tsbrand').selectize()[0].selectize.destroy();
        $('#upper_tsbrand').selectize();
        $('#upper_tsbrand').data('selectize').setValue("");

        $('#upper_tshade').selectize()[0].selectize.destroy();
        $('#upper_tshade').selectize();
        $('#upper_tshade').data('selectize').setValue("");

        $("#upper_ts_select_div").hide();
        $("#upper_tsbrand_div").hide();
        // return false;
        if ($("#lower_teethshade_stage").val() != "Yes") {
            $("#teeth_shades_div").hide();
            $("#teethshade_lbl").hide();
        }

        $('#upper_gsbrand').selectize()[0].selectize.destroy();
        $('#upper_gsbrand').selectize();
        $('#upper_gsbrand').data('selectize').setValue("");

        $('#upper_gshade').selectize()[0].selectize.destroy();
        $('#upper_gshade').selectize();
        $('#upper_gshade').data('selectize').setValue("");

        $("#upper_gs_select_div").hide();
        $("#upper_gsbrand_div").hide();
        if ($("#lower_gumshade_stage").val() != "Yes") {
            $("#gum_shades_div").hide();
            $("#gumshade_lbl").hide();
        }

        // return false;

        if ($("#lower_Impression_stage").val() != "Yes") {
            $("#upper_impression_div").hide();
            $("#upper_impressions_warning").hide();
            $("#impression_div").hide();
            $("#lower_impression_div").hide();
        }

        if ($("#lower_extraction_stage").val() != "Yes") {
            $("#upper_extraction").hide();
            $("#lower_extraction").hide();
        }

        $("#upper_category_addons_div").hide();
        $("#upper_category_addons_model_div").hide();
        $("#upper_status_div").hide();
        $("#upper_delivery_div").hide();

        $("#upper_addons_model_div").hide();
        $("#upper_category_addons_div").html("");
        $("#upper_model").addClass("d-none");

        $("#upper_delivery_date").val("");
        $("#upper_pickup_date").val("");
        $("#upper_delivery_time").val("");

        if ($("#lower_extraction_stage").val() != "Yes" && $("#lower_teethshade_stage").val() != "Yes" && $("#lower_gumshade_stage").val() != "Yes" && $("#lower_Impression_stage").val() != "Yes") {
            $("#category_addons_div").hide();
            $("#stage_status_div").hide();
            $("#delivery_div").hide();
        }

        $("#upper_rush_selection_html").html("");
        $("#upper_rush_date_stage").val("No");
        
        if ($("#iLowerStageId").val().length > 0) {
            delivery_date("lower");
            rush_date("lower");
        }

    });

    $(document).on("click", "#lower_delete", function() {

        $("#lower_extraction_stage").val("No");
        $("#lower_gumshade_stage").val("No");
        $("#lower_teethshade_stage").val("No");
        $("#lower_Impression_stage").val("No");

        $("#lower_eTeethInMouth").val("No");
        $("#lower_eTeethInDefault").val("No");
        $("#lower_eTeethInRequird").val("No");

        $("#lower_eMessingDefault").val("No");
        $("#lower_eMessingRequird").val("No");
        $("#lower_eMessingTeeth").val("No");

        $("#lower_eExtractDefault").val("No");
        $("#lower_eExtractDelivery").val("No");
        $("#lower_eExtractRequird").val("No");

        $("#lower_eExtracted").val("No");
        $("#lower_eExtractedDefault").val("No");
        $("#lower_eExtractedRequird").val("No");

        $("#lower_eFixorAdd").val("No");
        $("#lower_eFixorAddDefault").val("No");
        $("#lower_eFixorAddRequird").val("No");

        $("#lower_eClasps").val("No");
        $("#lower_eClaspsDefault").val("No");
        $("#lower_eClaspsRequird").val("No");


        $("#upper_eOpposingExtractions").val("No");

        $("#upper_eOpposingTeethInMouth").val("No");
        $("#upper_eOpposingTeethInDefault").val("No");

        $("#upper_eOpposingMessingTeeth").val("No");
        $("#upper_eOpposingMessingDefault").val("No");

        $("#upper_eOpposingExtractDelivery").val("No");
        $("#upper_eOpposingExtractDefault").val("No");

        $("#upper_eOpposingExtracted").val("No");
        $("#upper_eOpposingExtractedDefault").val("No");

        $("#upper_eOpposingFixorAdd").val("No");
        $("#upper_eOpposingFixorAddDefault").val("No");

        $("#upper_eOpposingClasps").val("No");
        $("#upper_eOpposingClaspsDefault").val("No");

        lower_reset_extraction();
        lower_reset_Impression();

        if ($("#lower_eOpposingExtractions").val() == "Yes") {

            if ($("#lower_eOpposingTeethInDefault").val() == "Yes") {
                lower_opposite_default_extraction_var = "lower_opposite_eTeethInDefault";
            } else if ($("#lower_eOpposingMessingDefault").val() == "Yes") {
                lower_opposite_default_extraction_var = "lower_opposite_eMessingDefault";
            } else if ($("#lower_eOpposingExtractDefault").val() == "Yes") {
                lower_opposite_default_extraction_var = "lower_opposite_eExtractDefault";
            } else if ($("#lower_eOpposingExtractedDefault").val() == "Yes") {
                lower_opposite_default_extraction_var = "lower_opposite_eExtractedDefault";
            } else if ($("#lower_eOpposingFixorAddDefault").val() == "Yes") {
                lower_opposite_default_extraction_var = "lower_opposite_eFixorAddDefault";
            } else if ($("#lower_eOpposingClaspsDefault").val() == "Yes") {
                lower_opposite_default_extraction_var = "lower_opposite_eClaspsDefault";
            } else {
                lower_opposite_default_extraction_var = "none";
            }

            if ($("#lower_extraction_stage").val() != "Yes") {
                lower_opposite_default_extraction(lower_opposite_default_extraction_var);
                lower_opposite_extraction_inner_hideshow();
            }
            // $('#lower_extraction').show();
        }


        $('#iLowerCategoryId').selectize()[0].selectize.destroy();
        $('#iLowerCategoryId').selectize();
        $('#iLowerCategoryId').data('selectize').setValue("");
        $("#category_lower_div").hide();
        category_select_data("Lower");
        if ($("#iUpperCategoryId").val() == "" || $("#iUpperCategoryId").val().length == 0) {
            $("#category_div").hide();
            $("#cat_lbl").hide();
            category_select_data("Upper");
        }

        $('#iLowerProductId').selectize()[0].selectize.destroy();
        $('#iLowerProductId').selectize();
        $('#iLowerProductId').data('selectize').setValue("");
        $("#product_lower_div").hide();
        if ($("#iUpperProductId").val() == "" || $("#iUpperProductId").val().length == 0) {
            $("#product_div").hide();
            $("#product_lbl").hide();
        }

        $('#iLowerGradeId').selectize()[0].selectize.destroy();
        $('#iLowerGradeId').selectize();
        $('#iLowerGradeId').data('selectize').setValue("");
        $("#grade_lower_div").hide();

        if ($("#iUpperGradeId").val() == "" || $("#iUpperGradeId").val().length == 0) {
            $("#grade_div").hide();
            $("#grade_lbl").hide();
        }


        $('#iLowerStageId').selectize()[0].selectize.destroy();
        $('#iLowerStageId').selectize();
        $('#iLowerStageId').data('selectize').setValue("");
        $("#stage_lower_div").hide();
        if ($("#iUpperStageId").val() == "" || $("#iUpperStageId").val().length == 0) {
            $("#stage_div").hide();
            $("#stage_lbl").hide();
        }


        $('#lower_tsbrand').selectize()[0].selectize.destroy();
        $('#lower_tsbrand').selectize();
        $('#lower_tsbrand').data('selectize').setValue("");

        $('#lower_tshade').selectize()[0].selectize.destroy();
        $('#lower_tshade').selectize();
        $('#lower_tshade').data('selectize').setValue("");

        $("#lower_ts_select_div").hide();
        $("#lower_tsbrand_div").hide();
        if ($("#upper_teethshade_stage").val() != "Yes") {
            $("#teeth_shades_div").hide();
            $("#teethshade_lbl").hide();
        }

        $('#lower_gsbrand').selectize()[0].selectize.destroy();
        $('#lower_gsbrand').selectize();
        $('#lower_gsbrand').data('selectize').setValue("");

        $('#lower_gshade').selectize()[0].selectize.destroy();
        $('#lower_gshade').selectize();
        $('#lower_gshade').data('selectize').setValue("");

        $("#lower_gs_select_div").hide();
        $("#lower_gsbrand_div").hide();
        if ($("#upper_gumshade_stage").val() != "Yes") {
            $("#gum_shades_div").hide();
            $("#gumshade_lbl").hide();
        }


        if ($("#upper_Impression_stage").val() != "Yes") {
            $("#lower_impression_div").hide();
            $("#lower_impressions_warning").hide();
            $("#impression_div").hide();
            $("#upper_impression_div").hide();
        }

        if ($("#upper_extraction_stage").val() != "Yes") {
            $("#lower_extraction").hide();
            $("#upper_extraction").hide();
        }

        $("#lower_category_addons_div").hide();
        $("#lower_category_addons_model_div").hide();
        $("#lower_status_div").hide();
        $("#lower_delivery_div").hide();

        $("#lower_addons_model_div").hide();
        $("#lower_category_addons_div").html("");
        $("#lower_model").addClass("d-none");

        $("#lower_delivery_date").val("");
        $("#lower_pickup_date").val("");
        $("#lower_delivery_time").val("");

        if ($("#upper_extraction_stage").val() != "Yes" && $("#upper_teethshade_stage").val() != "Yes" && $("#upper_gumshade_stage").val() != "Yes" && $("#upper_Impression_stage").val() != "Yes") {
            $("#category_addons_div").hide();
            $("#stage_status_div").hide();
            $("#delivery_div").hide();
        }

        $("#lower_rush_selection_html").html("");
        $("#lower_rush_date_stage").val("No");

        if ($("#iUpperStageId").val().length > 0) {
            delivery_date("upper");
            rush_date("upper");
        }

    });

    $(document).on('click', '#submit', async function() {

        vPatientName = $("#vPatientName").val();
        vCreatedByName = $("#vCreatedByName").val();
        iDoctorId = $("#iDoctorId").val();
        iOfficeId = $("#iOfficeId").val();
        var error = true;
        // if (iOfficeId.length == 0) {
        //     $("#iOfficeId_error").show();
        //     return false;
        // } else {
        //     $("#iOfficeId_error").hide();
        // }

        if (iDoctorId.length == 0) {
            $("#iDoctorId_error").show();
            return false;
        } else {
            $("#iDoctorId_error").hide();
        }

        if (vPatientName.length == 0) {
            $("#vPatientName_error").show();
            return false;
        } else {
            $("#vPatientName_error").hide();
        }
        if (vCreatedByName.length == 0) {
            $("#vCreatedByName_error").show();
            return false;
        } else {
            $("#vCreatedByName_error").hide();
        }

        if ($("#iUpperCategoryId").val() == "" && $("#iLowerCategoryId").val() == "") {
            alert("Please select category");
            return false;
        } else {

          

            let upper_final_validation = await upper_validation();
            let lower_final_validation = await lower_validation();


            if (upper_final_validation == "true" && lower_final_validation == "true") {
                if ($("#informations_checkbox").is(':checked')) {
                    $("#frm").submit();
                    return true;
                } else {
                    $('#information_class').removeClass("active");
                    $('#information_class').addClass("danger");
                }
            }else{
                return false;
            }
            
        }


    });


    async function upper_validation(){
        if ($("#iUpperProductId").val() != "") {
            grade = await check_if_grade_available("Upper");

            if (grade == "not_empty") {
                if ($("#iUpperGradeId").val() != "") {
                    let upper_check_if_stage_selected_var = await upper_check_if_stage_selected();
                    if (upper_check_if_stage_selected_var == "true") {
                        return "true";
                    }else{
                        return "false";
                    }
                } else {
                    alert("Please select upper grade");
                    return "false";
                }
            } else {
                let upper_check_if_stage_selected_var = await upper_check_if_stage_selected();
                if (upper_check_if_stage_selected_var == "true") {
                    return "true";
                }else{
                    return "false";
                }
            }

        } else {
            return "true";
        }
    }
    async function lower_validation(){
        if ($("#iLowerCategoryId").val() != "") {
            if ($("#iLowerProductId").val() != "") {
                grade = await check_if_grade_available("Lower");

                if (grade == "not_empty") {
                    if ($("#iLowerGradeId").val() != "") {
                        let lower_check_if_stage_selected_var = await lower_check_if_stage_selected();
                        if (lower_check_if_stage_selected_var == "true") {
                            return "true";
                        }else{
                            return "false";
                        }
                    } else {
                        alert("Please select lower grade");
                        return "false";
                    }
                } else {
                    let lower_check_if_stage_selected_var = await lower_check_if_stage_selected();
                    if (lower_check_if_stage_selected_var == "true") {
                        return "true";
                    }else{
                        return "false";
                    }
                }

            } else {
                return "true";
            }
        }
        else {
                return "true";
            }

    }


    async function upper_check_if_stage_selected() {
        if ($("#iUpperStageId").val() != "") {
            let upper_check_for_stage_inner_selection_var = await upper_check_for_stage_inner_selection();
            if (upper_check_for_stage_inner_selection_var == "true") {
                return "true";
            }else{
                return "false";
            }
        } else {
            alert("Please select upper stage");
            return "false";
        }
    }

    async function lower_check_if_stage_selected() {
        if ($("#iLowerStageId").val() != "") {
            let lower_check_for_stage_inner_selection_var = await lower_check_for_stage_inner_selection();
            if (lower_check_for_stage_inner_selection_var == "true") {
                return "true";
            }else{
                return "false";
            }
        } else {
            alert("Please select lower stage");
            return "false";
        }
    }

    async function upper_ts_validation(){
        if ($("#upper_teethshade_stage").val() == "Yes") {
            if ($("#upper_tsbrand").val() != "") {
                if ($("#upper_tshade").val() != "") {
                    return "true";
                } else {
                    alert("Please select upper teeth shade");
                    return "false";
                }
            } else {
                alert("Please select ts brand");
                return "false";
            }
        }else{
            return "true";
        }
    }

    async function upper_gs_validation(){
        if ($("#upper_gumshade_stage").val() == "Yes") {
            if ($("#upper_gsbrand").val() != "") {
                if ($("#upper_gshade").val() != "") {
                    return "true";
                } else {
                    alert("Please select upper gum shade");
                    return "false";
                }
            } else {
                alert("Please select gs brand");
                return "false";
            }
        }else{
            return "true";
        }
    }


    async function upper_imp_final_val(){
        if ($("#upper_Impression_stage").val() == "Yes") {
            ImpressionId = [];
            await $("input[name='upper_impression_checkbox[]']:checked").each(function() {
                ImpressionId.push($(this).val());
            });
            if (ImpressionId.length == 0) {
                alert("Please select upper impresions");
                return "false";
            }else{
                return "true";
            }
        }else{
            return "true";
        }
    }

    async function upper_ext_final_val(){

        await upper_extraction_validation();

        if ($("#upper_extraction_stage").val() == "Yes") {
            if ($("#upper_extraction_error").val() == "true") {
                alert("Please select upper extraction");
                return "false";
            }else{
                return "true";
            }
        }else{
            return "true";
        }
    }

    async function upper_check_for_stage_inner_selection() {
        // alert("upperjsklf");
        let upper_teeth = await upper_ts_validation();
        let upper_gum = await upper_gs_validation();
        let upper_impression = await upper_imp_final_val();
        let upper_extraction = await upper_ext_final_val();
       

        if (upper_teeth == "true" && upper_gum == "true" && upper_impression == "true" && upper_extraction == "true") {
            return "true"
        }else{
            return "false";
        }

    }


    async function lower_ts_validation(){
        if ($("#lower_teethshade_stage").val() == "Yes") {
            if ($("#lower_tsbrand").val() != "") {
                if ($("#lower_tshade").val() != "") {
                    return "true";
                } else {
                    alert("Please select lower teeth shade");
                    return "false";
                }
            } else {
                alert("Please select ts brand");
                return "false";
            }
        }else{
            return "true";
        }
    }

    async function lower_gs_validation(){
        if ($("#lower_gumshade_stage").val() == "Yes") {
            if ($("#lower_gsbrand").val() != "") {
                if ($("#lower_gshade").val() != "") {
                    return "true";
                } else {
                    alert("Please select lower gum shade");
                    return "false";
                }
            } else {
                alert("Please select gs brand");
                return "false";
            }
        }else{
            return "true";
        }
    }


    async function lower_imp_final_val(){
        if ($("#lower_Impression_stage").val() == "Yes") {
            ImpressionId = [];
            await $("input[name='lower_impression_checkbox[]']:checked").each(function() {
                ImpressionId.push($(this).val());
            });
            if (ImpressionId.length == 0) {
                alert("Please select lower impresions");
                return "false";
            }else{
                return "true";
            }
        }else{
            return "true";
        }
    }

    async function lower_ext_final_val(){

        await lower_extraction_validation();

        if ($("#lower_extraction_stage").val() == "Yes") {
            if ($("#lower_extraction_error").val() == "true") {
                er = "true"
                alert("Please select lower extraction");
                return "false";
            }else{
                return "true";
            }
        }else{
            return "true";
        }
    }


    async function lower_check_for_stage_inner_selection() {

        let lower_teeth = await lower_ts_validation();
        let lower_gum = await lower_gs_validation();
        let lower_impression = await lower_imp_final_val();
        let lower_extraction = await lower_ext_final_val();

        if (lower_teeth == "true" && lower_gum == "true" && lower_impression == "true" && lower_extraction == "true") {
            return "true"
        }else{
            return "false";
        }

    }

    function check_if_grade_available(eType) {
        var iUpperProductId = $("#iUpperProductId").val();
        $.ajax({
            url: "{{route('admin.labcase.getgrade')}}",
            type: "post",
            data: {
                iCategoryProductId: iUpperProductId,
                eType: eType,
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                if (result == "1") {
                    return "empty";
                } else {
                    return "not_empty";
                }
            }
        });
    }

    $('.notes_div').click(function() {
        $('#tDescription').focus();
    });
    $('textarea').click(function() {
        $('#tDescription').focus();
    });


    $(document).on("click", ".upper_impression_checked", function() {

        Id = $(this).val();

        if ($("#upper_impression_" + Id).is(':checked')) {
            $("#upper_impression_qty_" + Id).show();
            $("#upper_impression_qty_" + Id).val("");
        } else {
            $("#upper_impression_qty_" + Id).hide();
            $("#upper_impression_qty_" + Id).val("");
        }
    });

    $(document).on("click", ".lower_impression_checked", function() {

        LowerId = $(this).val();

        if ($("#lower_impression_" + LowerId).is(':checked')) {
            $("#lower_impression_qty_" + LowerId).show();
            $("#lower_impression_qty_" + LowerId).val("");
        } else {
            $("#lower_impression_qty_" + LowerId).hide();
            $("#lower_impression_qty_" + LowerId).val("");
        }

    });

    $(document).on('click', '#open_add_slip', function() {
        $("#add_to_slip").addClass("show d-block");
        $("#iLabId")[0].selectize.open();
    });
    $(document).on('click', '#main_slip_close', function() {
        $("#add_to_slip").removeClass("show d-block");

    });
    $(document).on('click', '#rush_model_open', function() {
        $("#rush_model").addClass("show d-block");

    });
    $(document).on('click', '#rush_btn_close', function() {
        $("#rush_model").removeClass("show d-block");

    });

    $(document).on('click', '#add_attachments_model_open', function() {
        $("#add_attachments_model").addClass("show d-block");

    });
    $(document).on('click', '#add_attachments_model_close', function() {
        $("#add_attachments_model").removeClass("show d-block");

    });
    $(document).on('click', '#cancel_attacement', function() {
        $("#add_attachments_model").removeClass("show d-block");

    });

    $(document).on('change', '#vImage', function() {
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img').attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });
</script>
<script type="text/javascript">
    var myimg = "<div class=''><i class='fas fa-cloud-upload-alt upload'></i></div>";
    Dropzone.options.dropzonewidget = {
        paramName: "vImage",
        dictDefaultMessage: myimg + '<br>' + "<h4 class='darg'>Drag and drop your Image,Video And Audio here.</h4>" + '<br>' + "<span> Once uploaded, you can select and drag images to recorder below.</span>" + '<br>' + "<a href='javascript:;' class='album-addbtn pass btn mt-5' >Add Image </a>",
        // acceptedFiles: "image/*, video/* , audio/*",
        maxFilesize: 200 // MB,
    };


    $(document).on('click', '#attachment_delete', function() {
        iTampId = "{{$tampid}}";
        swal({
                title: "Are you sure delete this items.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");


                    setTimeout(function() {
                        $.ajax({
                            url: "{{route('admin.labcase.delete_gallery')}}",
                            type: "get",
                            data: {
                                id: id,
                                iTampId: iTampId,
                            },
                            success: function(response) {
                                $("#attachment_list").html(response);
                            }
                        });
                    }, 1000);
                }
            })
    });
    $(document).on('click', '#submit_attacement', function() {

        iTampId = "{{$tampid}}";

        $.ajax({
            url: "{{route('admin.labcase.attacement')}}",
            type: "post",
            data: {
                iTampId: iTampId,
                
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
                $("#add_attachments_model_close").trigger("click");
                $("#attachment_list").html(response);
                Dropzone.forElement('#dropzonewidget').removeAllFiles(true);

            }
        });

    });
    // add-attachments-model


    $(document).on("click", ".print_paper", function() {
        iCaseId = $(this).data("case");
        iSlipId = $(this).data("slip");

        $.ajax({
            url: "{{route('admin.labcase.paper_slip')}}",
            type: "post",
            data: {
                iCaseId: iCaseId,
                iSlipId: iSlipId,
                _token: '{{csrf_token()}}'
            },
            success: function(data) {

                // alert("{{$_SERVER['HTTP_USER_AGENT']}}");
                @php

                    $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
                    $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
                    $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
                    $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
                    $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");
                    $user_agent = $_SERVER['HTTP_USER_AGENT']; 

                    if($iPod || $iPhone || $iPad){
                        

                            if (stripos($_SERVER['HTTP_USER_AGENT'],"safari") && !stripos($_SERVER['HTTP_USER_AGENT'],"CriOS")) 
                            {
                                @endphp
                                    var newWin = window.frames["printf"];

                                    setTimeout(function(){
                                        newWin.document.write(data);
                                        newWin.document.close();
                                        
                                    },500)
                                    @php
                            }else{

                                @endphp
                                const wnd = window.open('about:blank', '', '_blank, alwaysRaised=yes');
                                wnd.document.write(data);
                                wnd.focus();

                                setTimeout(function(){

                                    wnd.print();

                                    setTimeout(function(){
                                        wnd.close();
                                    },500)
                                       
                                },500)
                                @php
                            }

                            

                        // }

                    }else{ @endphp
                        var newWin = window.frames["printf"];

                        setTimeout(function(){
                            newWin.document.write(data);
                            newWin.document.close();
                            
                        },500)
                        @php
                    }


                @endphp


            }
        });
    });


    
        $('.show_slip').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13' || keycode == '75'){
                count_data = $('#count_data').val();
                if(count_data != undefined && count_data ==1)
                {
                    var url = $('.slip_redirect').attr('href');
                    window.location.href =  url;
                }
            }
        });
    
        // Search by onchange date
        $('#startdate').datepicker({
            orientation: "top auto",
            
        });
        $('#enddate').datepicker({
            orientation: "top auto",
        });
        $(document).on("click", "#PrintDriverSlip", function() {
            var startdate = $('#startdate').val();
            var enddate = $('#enddate').val();
            if (startdate != '' || enddate != '') {
                $("#PrintDriverSlip").submit();
            }
            else
            {
                alert("Please select any date");
                return false;
            }
        });
        $(document).on('change', '#eDriverPrintSlip', function() {
            var delivery_date_array = [];
            $("#eDriverPrintSlip :selected").each(function() {
                delivery_date_array.push(this.value);
            });
            // var delivery_date_array = delivery_date_array.join(",");
   
            if(jQuery.inArray("CustomDate", delivery_date_array) !== -1)
            {
                var $eDriverPrintSlip = $('#eDriverPrintSlip').selectize();
                var control = $eDriverPrintSlip[0].selectize;
                $("#eDriverPrintSlip")[0].selectize.clear();
                setTimeout(function(){
                    $('#eDriverPrintSlip').data('selectize').setValue("CustomDate",true);
                    $("#eDriverPrintSlip")[0].selectize.close();
                },500)
                
                $('#ShowCustomDate').show();
            }
            else
            {
                $('#ShowCustomDate').hide();
            }
        });

        $('.print_driver_checked_slip').click(function(){
           
            var id = [];
    
            $("input[name='Case_ID[]']:checked").each(function() {
                id.push($(this).val());
            });
            var id = id.join(",");
            var delivery_date_array = [];
            $("#eDriverPrintSlip :selected").each(function() {
                delivery_date_array.push(this.value);
            });
            var delivery_date_array_old = delivery_date_array.join(",");
            var delivery_date_array = delivery_date_array_old.toString().replace(/\s*\,\s*/g, ",").trim().split(",");
            
            if (id.length == 0 && delivery_date_array.length == 0) {
                alert('Please select records.')
                return false;
            }
            else
            {
                if(id.length !=0)
                {
                    $('#islipIdPrint').val(id);
                }
                if(delivery_date_array.length)
                {
                    $('#deliveryDateArray').val(delivery_date_array);
                }
                var startdate = $('#startdate').val();
                var enddate = $('#enddate').val();
          
                if (startdate != '') 
                {
                    $('#start_date').val(startdate)
                }
                if(enddate !='')
                {
                    $('#end_date').val(enddate)
                }
                $('#submitCheckedForm').submit();
            }
        });

</script>

@endsection
