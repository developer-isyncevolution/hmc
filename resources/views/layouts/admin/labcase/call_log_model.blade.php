{{-- view call log start --}}

<table class="tslip driver-inner-model" style="border-bottom: 1px solid #eff2f5;">
   <tbody>
     <tr>
       <th>Office: <span>{{$data['vOfficeName']?$data['vOfficeName']:'N/A'}}</span></th>
       <th>Patient: <span>{{$data['vPatientName']?$data['vPatientName']:'N/A'}}</span></th>
       <th>Case # <span>{{$data['vCaseNumber']?$data['vCaseNumber']:'N/A'}}</span></th>
       <th>Pan # <span>{{$data['vCasePanNumber']?$data['vCasePanNumber']:'N/A'}}</span></th>
     </tr>
     </tbody>
</table>
@if(count($callHistory)>0)
@foreach ($callHistory as $callHistory_val)
<table class="tslip driver-inner-model mb-5 text-center">
   <tbody>
       <tr>
           <td colspan="8">
             <table class="w-50 mx-auto my-2 text-center">
               <tr>
                 <th>Slip ID: {{isset($callHistory_val->vSlipNumber)?$callHistory_val->vSlipNumber:'N/A'}}</th>
                 <th>Stage: {{isset($callHistory_val->vStageName)?$callHistory_val->vStageName:'N/A'}}</th>
               </tr>
             </table>
           </td>
       </tr>
     @php
     $call_log = $callHistory_val->call_log;
     @endphp
       @if(count($call_log)>0)
       @foreach ($call_log as $call_log_val)
       
       <tr>
         <td style="color: #0e66b2" class="text-start">
           The following call was taken by  {{$call_log_val->vCallTaken?$call_log_val->vCallTaken:'N/A'}} at {{$call_log_val->dDate?date("m/d/Y", strtotime($call_log_val->dDate)):'N/A'}} @ {{$call_log_val->tTime?$call_log_val->tTime:'N/A'}} Person attended
           {{$call_log_val->vName?$call_log_val->vName:'N/A'}}
         </td>
       </tr>
       <tr>
         <td class="font-bold text-start">
             @if(isset($call_log_val->tDescription) && $call_log_val->tDescription!='')
             {{$call_log_val->tDescription}}
             @endif
             <hr style="color:#6c6c6c73" class="m-0">
         </td>
       </tr>
       @endforeach
       @endif
   
     </tbody>
   </table>
@endforeach
@endif
{{-- view call log end --}}
<div class="text-center">
   <a href="#"  id="new_call_log"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37.02 32.4" style="&#10;    fill: black;&#10;    height: 33px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M18.46,32.4H4.91A4.54,4.54,0,0,1,0,27.53V4.89A4.48,4.48,0,0,1,4.63,0C7.46,0,10.31,0,13.14,0A4.52,4.52,0,0,1,16.5,1.51c.82.86,1.71,1.66,2.51,2.54a2.08,2.08,0,0,0,1.71.69c3.81,0,7.61,0,11.42,0C35.05,4.73,37,6.46,37,9.35q.09,9.2,0,18.38A4.59,4.59,0,0,1,32.1,32.4Zm.14-2.21H32.15c1.86,0,2.73-.85,2.73-2.71q0-9,0-17.9c0-1.87-.86-2.69-2.75-2.69-4.12,0-8.25,0-12.38,0a2.14,2.14,0,0,1-1.7-.72c-1-1-2.06-2-3.06-3.1a2.62,2.62,0,0,0-2.06-.85c-2.65,0-5.29,0-7.94,0-2,0-2.82.81-2.82,2.83q0,11.17,0,22.35c0,1.92.88,2.77,2.79,2.77Z"/><path d="M22.75,26A14.52,14.52,0,0,1,9.13,14.56a18.5,18.5,0,0,1-.34-3A1.48,1.48,0,0,1,10.08,10l2.84-.65a1.42,1.42,0,0,1,1.72.83c.49,1.08,1,2.18,1.43,3.28a1.45,1.45,0,0,1-.44,1.76c-.33.29-.66.58-1,.85a.39.39,0,0,0-.1.59,9.78,9.78,0,0,0,3.71,3.65.44.44,0,0,0,.42-.09c.32-.33.59-.71.9-1.06a1.41,1.41,0,0,1,1.73-.42c1.12.45,2.23.93,3.32,1.42a1.41,1.41,0,0,1,.82,1.66c-.22,1-.46,2-.7,3.06A1.42,1.42,0,0,1,23.27,26Zm-.22-1c1.16,0,1.16,0,1.37-.92.18-.75.35-1.5.52-2.25.13-.55.08-.66-.42-.88-1-.43-2-.85-2.95-1.29a.59.59,0,0,0-.8.2c-.39.47-.79.93-1.17,1.41a.6.6,0,0,1-.87.18,11.62,11.62,0,0,1-4.86-4.86.63.63,0,0,1,.19-.91c.48-.38.94-.77,1.42-1.16a.54.54,0,0,0,.2-.69c-.46-1.07-.93-2.13-1.39-3.2a.5.5,0,0,0-.64-.31c-1,.22-1.89.46-2.84.67a.57.57,0,0,0-.53.67c.08.82.12,1.65.27,2.46a13,13,0,0,0,4.19,7.37A12.94,12.94,0,0,0,22.53,25Z"/><path d="M22.87,11.89h1.68a1.25,1.25,0,0,1,.55.06c.15.08.34.28.34.42a.64.64,0,0,1-.33.47,5.55,5.55,0,0,1-1.08.07H22.87v1.54c0,.2,0,.4,0,.61a.45.45,0,0,1-.49.45.46.46,0,0,1-.46-.48c0-.56,0-1.11,0-1.66,0-.36-.09-.49-.46-.47s-1,0-1.48,0-.66-.09-.66-.52.32-.48.64-.49c.51,0,1,0,1.53,0,.36,0,.45-.12.43-.45,0-.52,0-1,0-1.57,0-.32.13-.56.47-.58s.51.24.52.57C22.87,10.52,22.87,11.17,22.87,11.89Z"/></g></g></svg></a>
</div>
  <div class="row container mt-6 show_hide_call_log" style="display: none;">

   <div class="col-xxl-4 col-lg-6 col-md-12">
         <label>Call Taken</label>
         <input type="text" class="form-control" name="vCallTaken" value="{{$username}}" readonly>
      </div>
      <div class="col-xxl-4 col-lg-6 col-md-12">
         <label>Person Attended</label>
         <input type="text" class="form-control" id="vName" name="vName" placeholder="Person attended" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($calls->vName)){{$calls->vName}}@else{{old('vName')}}@endif">
         <div class="text-danger" style="display: none;" id="vName_error">Please enter person attended</div>
      </div>
      <div class="col-xxl-4 col-lg-6 col-md-12">
      <label>Reason</label>
      <div id="toolbar-container"></div>
         <textarea class="form-control" id="tDescriptions" name="tDescription" placeholder="Reason"></textarea>
      <div id="tDescription_error" class="text-danger" style="display: none;">Please enter reason </div>
      <br>
   </div> 
  
        <input type="hidden" name="iSlipId" value="{{$iSlipId?$iSlipId:null}}">
        <input type="hidden" name="iCaseId" value="{{$iCaseId?$iCaseId:null}}">
        <input type="hidden" name="id" value="@if(isset($calls)) {{$calls->iCallId}} @endif">
         @php $date = date('Y-m-d'); @endphp
        <input type="hidden" name="dDate" value="@if(isset($calls->dDate)){{$calls->dDate}}@else{{$date}}@endif">
        @php $time = date('H:i');@endphp
        <input type="hidden" name="tTime" value="@if(isset($calls->tTime)){{$calls->tTime}}@else{{$time}}@endif">
  </div>
  <script>
     $('#new_call_log').click(function(){
         $('.show_hide_call_log').show();
     });
  </script>
