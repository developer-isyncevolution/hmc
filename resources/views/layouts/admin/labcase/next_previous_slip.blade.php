{{-- @if(isset($previous_stage) && $previous_stage !='')
    <a href="{{route('admin.labcase.virtual_slip',[$previous_stage->iCaseId,$previous_stage->iSlipId])}}" class="btn add-btn"> Previous </a>
@else
    <a href="#" class="btn add-btn invisible" >Next</a>    
@endif --}}

@if(isset($middel_stage) && count($middel_stage)>0)
    @foreach($middel_stage as $key => $middel_stage_val)
    <a href="{{route('admin.labcase.virtual_slip',[$middel_stage_val['iCaseId'],$middel_stage_val['iSlipId']])}}" @if($middel_stage_val['iSlipId'] == $CuurentiSlipId) class=" btn back-btn gray-btn mb-1 disabled" @else class= "btn add-btn" @endif> {{$middel_stage_val['vProductStageName']}} </a>
    @endforeach
@endif


{{-- @if(isset($next_stage) && $next_stage !='')
    <a href="{{route('admin.labcase.virtual_slip',[$next_stage->iCaseId,$next_stage->iSlipId])}}" class="btn add-btn"> Next </a>
@else
    <a href="#" class="btn add-btn invisible" >Next</a>    
@endif --}}

@if(isset($next_stage) && $next_stage !='')
<input type="hidden" name="next_stage" id="next_stage" value="Yes">
@endif
