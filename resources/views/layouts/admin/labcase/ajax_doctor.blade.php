@if (!empty($Doctor))    
    @if (count($Doctor) > 1)
    <option value="">Select Doctor</option>
    @endif
    @if(isset($is_filter) && $is_filter =='Yes')
    <option value="null">ALL</option>
    @endif
    @foreach($Doctor as $key => $Doctor_val)
        <option value="{{$Doctor_val->iDoctorId}}"> {{$Doctor_val->vFirstName}} {{$Doctor_val->vLastName}}</option>
    @endforeach
@else
    <option value="">No Doctor Found</option>
@endif
