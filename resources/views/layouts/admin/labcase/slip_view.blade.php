@extends('layouts.admin.index')
@section('content')

@php
$labcases = $labcases;
$slip = $slip;
$slip = $slip;
@endphp





<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css" />


<!-- Button trigger modal -->
<button type="button" class="btn btn-primary open_model" data-bs-toggle="modal" data-bs-target="#slip_model" style="display: none">
    Launch static backdrop modal
</button>
<div class="slip-view-wrapper">
    <!-- Modal -->
    <div class="modal fade" id="slip_model" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="slip_modelLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl c-slip-model">
            <div class="modal-content">

                <div class="modal-body p-0 m-0">
                    <div class="main-panel add-case-listing m-0" style="padding: 0px 40px 0px 40px;">
                        <div class="modal-header m-0">
                            <h5 class="modal-title" id="slip_modelLabel">HMC INNOVS LLC</h5>
                            <a href="javascript:;" class="btn-close" id="back" data-bs-dismiss="modal" aria-label="Close"></a>
                        </div>
                        <div class="header-flex d-flex justify-content-between">
                            <div class="left-header">
                                <div class="com">
                                    <ul class="list-unstyled text-start">
                                        <li>
                                            <span class="li-title w-auto pe-5"> <strong>Office</strong> </span> <span>{{$labcases->vOfficeName}}</span>
                                        </li>
                                        <li>
                                            <span class="li-title w-auto pe-5"> <strong>Dr. </strong> </span> <span>{{$labcases->vFirstName." ".$labcases->vLastName}}</span>
                                        </li>
                                        <li>
                                            <span class="li-title w-auto pe-5"> <strong>Patient </strong> </span> <span>{{$labcases->vPatientName}}</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="com">
                                    <ul class="list-unstyled text-start">
                                        <li>
                                            <span class="li-title"> <strong>Pan #</strong> </span> 0001
                                        </li>
                                        <li>
                                            <span class="li-title"> <strong>Case #</strong> </span>
                                            0001
                                        </li>
                                        <li>
                                            <span class="li-title"><strong>Slip #</strong></span> 0001
                                        </li>
                                    </ul>
                                </div>
                                <div class="com">
                                    <ul class="list-unstyled text-start">
                                        <li>
                                            <span class="li-title"> <strong>Created by </strong> </span> <span>{{$labcases->vCreatedByName}}</span>
                                        </li>

                                        <li>
                                            <span class="li-title"> <strong>Location</strong> </span>
                                            <span>{{$slip->vLocation}}</span>

                                        </li>
                                        <li>
                                            <span class="li-title"><strong>Case status</strong></span> <span>{{$labcases->eStatus}}</span>
                                        </li>
                                        <li>
                                            <span class="li-title"><strong>Slip status</strong></span> <span>{{$slip->eStatus}}</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="com">
                                    <ul class="list-unstyled text-start">
                                        <li>
                                            <span class="li-title px-5"> <strong>Pick up Date</strong> </span> <span> @if(isset($final_pickup_date)){{ date('m/d/Y', strtotime($final_pickup_date)) }}@endif</span>
                                        </li>
                                        <li>
                                            <span class="li-title px-5"> <strong>Delivery Date </strong> </span> <span> @if(isset($final_dilevery_date)){{ date('m/d/Y', strtotime($final_dilevery_date)) }}@endif</span>
                                        </li>
                                        <li>
                                            <span class="li-title px-5"> <strong>Delivery Time </strong> </span> <span> @if(isset($final_dilevery_time)){{$final_dilevery_time}}@endif</span>
                                        </li>
                                    </ul>
                                </div>


                            </div>

                            <div class="right-header">
                                <div class="com">
                                    <ul class="add-case-heder-ul">
                                        <li>
                                            <a href="#" class="p-icon">
                                                <i class="fad fa-print"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fad fa-share"></i>
                                            </a>
                                            <a href="#" class="bill-icon">
                                                <i class="fad fa-ballot-check"></i>
                                            </a>
                                        </li>
                                        <li></li>
                                        <li>
                                            <a href="#" class="msg-icon">
                                                <i class="fad fa-comment-alt-edit"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fad fa-share"></i>
                                            </a>
                                            <a href="#" class="pic-icon">
                                                <i class="fad fa-image"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>



                        </div>


                        <hr>
                        <form action="{{route('admin.labcase.store')}}" id="frm" class="row add-product mt-0" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row gy-2">

                                <!-- view upper teeth start -->
                                <div class="col-xxl-3 col-lg-3 col-md-4">
                                    <div class="teeth-wrapper upper" id="upper_extraction">

                                        <h3 class="card-title mb-0">
                                            MAXILLARY
                                        </h3>

                                        <svg width="100%" height="100%" viewBox="0 0 1500 822" class="teeth-svg">

                                            <g>

                                                <g>
                                                    <!-- upper-teeth 5 -->
                                                    <a href="javascript:;">
                                                        <image id="uppper-5-teeth" data-id="teeths3" data-name="5" x="325" y="202" width="149" height="152" xlink:href="{{asset('admin/assets/images/teeth/up-5.png')}}" class="img-for-teeth teeth_uppper
                                                        
                                                        @if (in_array('5', $upper_vTeethInMouth))
                                                            {{'in-mouth '}}
                                                        @endif
                                                        @if (in_array('5', $upper_vMissingTeeth))
                                                            {{'missing '}}
                                                        @endif
                                                        @if (in_array('5', $upper_vWillExtractOnDelivery))
                                                            {{'will-extract-on-deliver '}}
                                                        @endif
                                                        @if (in_array('5', $upper_vHasBeenExtracted))
                                                            {{'ben-extracted '}}
                                                        @endif
                                                        @if (in_array('5', $upper_vFixOrAdd))
                                                            {{'add-or-fix '}}
                                                        @endif
                                                        
                                                        "></image>


                                                        <text id="uppper-5-num" data-name="5" class="cls-1 teeth_uppper" transform="translate(405.696 287.468) scale(1.305)">
                                                            <tspan x="0">5</tspan>
                                                        </text>
                                                        <image id="uppper-5-claps" data-id="teeths3" data-name="5" x="315" y="190" xlink:href="{{asset('admin/assets/images/teeth/updc-5.png')}}" class=" clap-up-5 teeth_uppper upper-claps" style="
                                                        
                                                        @if (in_array('5', $upper_vClasps))
                                                            {{' '}}
                                                        @else
                                                            {{'display: none; '}}
                                                            
                                                        @endif
                                                        
                                                        "></image>
                                                    </a>
                                                </g>
                                                <g>
                                                    <!-- upper-teeth 4 -->
                                                    <a href="javascript:;">
                                                        <image id="uppper-4-teeth" data-id="teeths4" data-name="4" x="266" y="285" width="153" height="149" xlink:href="{{asset('admin/assets/images/teeth/up-4.png')}}" class="img-for-teeth teeth_uppper
                                                        
                                                        @if (in_array('4', $upper_vTeethInMouth))
                                                            {{'in-mouth '}}
                                                        @endif
                                                        @if (in_array('4', $upper_vMissingTeeth))
                                                            {{'missing '}}
                                                        @endif
                                                        @if (in_array('4', $upper_vWillExtractOnDelivery))
                                                            {{'will-extract-on-deliver '}}
                                                        @endif
                                                        @if (in_array('4', $upper_vHasBeenExtracted))
                                                            {{'ben-extracted '}}
                                                        @endif
                                                        @if (in_array('4', $upper_vFixOrAdd))
                                                            {{'add-or-fix '}}
                                                        @endif

                                                        "></image>

                                                        <text id="uppper-4-num" data-name="4" class="cls-1 teeth_uppper" transform="translate(346.65 357.771) scale(1.305)">
                                                            <tspan x="0">4</tspan>
                                                        </text>
                                                        <image id="uppper-4-claps" data-id="teeths4" data-name="4" x="250" y="280" xlink:href="{{asset('admin/assets/images/teeth/updc-4.png')}}" class=" clap-up-4 teeth_uppper upper-claps" style="

                                                        @if (in_array('4', $upper_vClasps))
                                                            {{' '}}
                                                        @else
                                                            {{'display: none; '}}
                                                            
                                                        @endif
                                                        "></image>
                                                    </a>
                                                </g>
                                                <g>
                                                    <!-- upper-teeth 3 -->
                                                    <a href="javascript:;">
                                                        <image id="uppper-3-teeth" data-id="teeths7" data-name="3" x="176" y="367" width="220" height="199" xlink:href="{{asset('admin/assets/images/teeth/up-3.png')}}" class="img-for-teeth teeth_uppper
                                                        
                                                        @if (in_array('3', $upper_vTeethInMouth))
                                                            {{'in-mouth '}}
                                                        @endif
                                                        @if (in_array('3', $upper_vMissingTeeth))
                                                            {{'missing '}}
                                                        @endif
                                                        @if (in_array('3', $upper_vWillExtractOnDelivery))
                                                            {{'will-extract-on-deliver '}}
                                                        @endif
                                                        @if (in_array('3', $upper_vHasBeenExtracted))
                                                            {{'ben-extracted '}}
                                                        @endif
                                                        @if (in_array('3', $upper_vFixOrAdd))
                                                            {{'add-or-fix '}}
                                                        @endif
                                                        
                                                        "></image>

                                                        <text id="uppper-3-num" data-name="3" class="cls-1 teeth_uppper" transform="translate(289.371 467.923) scale(1.305)">
                                                            <tspan x="0">3</tspan>
                                                        </text>

                                                        <image id="uppper-3-claps" data-id="teeths7" data-name="3" x="160" y="367" xlink:href="{{asset('admin/assets/images/teeth/updc-3.png')}}" class=" clap-up-3 teeth_uppper upper-claps" style="
                                                        
                                                        @if (in_array('3', $upper_vClasps))
                                                            {{' '}}
                                                        @else
                                                            {{'display: none; '}}
                                                            
                                                        @endif
                                                        "></image>
                                                    </a>
                                                </g>
                                                <g>
                                                    <!-- upper-teeth 2 -->
                                                    <a href="javascript:;" class="test">
                                                        <image id="uppper-2-teeth" data-id="teeths6" data-name="2" x="115" y="500" width="203" height="188" xlink:href="{{asset('admin/assets/images/teeth/up-2.png')}}" class="img-for-teeth teeth_uppper
                                                        
                                                        @if (in_array('2', $upper_vTeethInMouth))
                                                            {{'in-mouth '}}
                                                        @endif
                                                        @if (in_array('2', $upper_vMissingTeeth))
                                                            {{'missing '}}
                                                        @endif
                                                        @if (in_array('2', $upper_vWillExtractOnDelivery))
                                                            {{'will-extract-on-deliver '}}
                                                        @endif
                                                        @if (in_array('2', $upper_vHasBeenExtracted))
                                                            {{'ben-extracted '}}
                                                        @endif
                                                        @if (in_array('2', $upper_vFixOrAdd))
                                                            {{'add-or-fix '}}
                                                        @endif
                                                        
                                                        "></image>

                                                        <image id="uppper-2-claps" data-id="teeths6" data-name="2" x="100" y="500" xlink:href="{{asset('admin/assets/images/teeth/updc-2.png')}}" class=" clap-up-2 teeth_uppper upper-claps" style="
                                                        
                                                        @if (in_array('2', $upper_vClasps))
                                                            {{' '}}
                                                        @else
                                                            {{'display: none; '}}
                                                            
                                                        @endif
                                                        "></image>

                                                        <text id="uppper-2-num" data-name="2" class="cls-1 teeth_uppper" transform="translate(224.87 609.191) scale(1.305)">
                                                            <tspan x="0">2</tspan>
                                                        </text>
                                                    </a>
                                                </g>

                                                <g>
                                                    <!-- upper-teeth 1 -->
                                                    <a href="javascript:;" class="test">

                                                        <image id="uppper-1-teeth" data-id="teeths5" data-name="1" x="57" y="640" width="192" height="156" xlink:href="{{asset('admin/assets/images/teeth/up-1.png')}}" class="img-for-teeth teeth_uppper
                                                        
                                                        @if (in_array('1', $upper_vTeethInMouth))
                                                            {{'in-mouth '}}
                                                        @endif
                                                        @if (in_array('1', $upper_vMissingTeeth))
                                                            {{'missing '}}
                                                        @endif
                                                        @if (in_array('1', $upper_vWillExtractOnDelivery))
                                                            {{'will-extract-on-deliver '}}
                                                        @endif
                                                        @if (in_array('1', $upper_vHasBeenExtracted))
                                                            {{'ben-extracted '}}
                                                        @endif
                                                        @if (in_array('1', $upper_vFixOrAdd))
                                                            {{'add-or-fix '}}
                                                        @endif
                                                        
                                                        "></image>


                                                        <text id="uppper-1-num" data-name="1" data-id="teeths5" class="cls-1 teeth_uppper" transform="translate(163.9 742.356) scale(1.305)">
                                                            <tspan x="0">1</tspan>
                                                        </text>

                                                        <image id="uppper-1-claps" data-id="teeths5" data-name="1" x="40" y="640" xlink:href="{{asset('admin/assets/images/teeth/up-dc-1.png')}}" class=" clap-up-1 teeth_uppper upper-claps" style="
                                                        @if (in_array('1', $upper_vClasps))
                                                            {{' '}}
                                                        @else
                                                            {{'display: none; '}}
                                                            
                                                        @endif
                                                        "></image>

                                                    </a>
                                                    <!-- upper-teeth 1 end-->
                                                </g>


                                                <g>
                                                    <!-- upper-teeth 6 -->
                                                    <a href="javascript:;">
                                                        <image id="uppper-6-teeth" data-id="teeths2" data-name="6" x="403" y="123" width="136" height="178" xlink:href="{{asset('admin/assets/images/teeth/up-6.png')}}" class="img-for-teeth teeth_uppper
                                                        
                                                        @if (in_array('6', $upper_vTeethInMouth))
                                                            {{'in-mouth '}}
                                                        @endif
                                                        @if (in_array('6', $upper_vMissingTeeth))
                                                            {{'missing '}}
                                                        @endif
                                                        @if (in_array('6', $upper_vWillExtractOnDelivery))
                                                            {{'will-extract-on-deliver '}}
                                                        @endif
                                                        @if (in_array('6', $upper_vHasBeenExtracted))
                                                            {{'ben-extracted '}}
                                                        @endif
                                                        @if (in_array('6', $upper_vFixOrAdd))
                                                            {{'add-or-fix '}}
                                                        @endif
                                                        
                                                        "></image>
                                                        <image id="uppper-6-claps" data-id="teeths2" data-name="6" x="400" y="105" xlink:href="{{asset('admin/assets/images/teeth/updc-6.png')}}" class=" clap-up-6 teeth_uppper upper-claps" style="
                                                        @if (in_array('6', $upper_vClasps))
                                                            {{' '}}
                                                        @else
                                                            {{'display: none; '}}
                                                            
                                                        @endif
                                                        "></image>

                                                        <text id="uppper-6-num" data-name="6" class="cls-1 teeth_uppper" transform="translate(476.76 240.092) scale(1.305)">
                                                            <tspan x="0">6</tspan>
                                                        </text>
                                                    </a>
                                                </g>



                                                <!-- upper-teeth 7 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-7-teeth" data-id="teeths1" data-name="7" x="498" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/teeth/up-7.png')}}" class="img-for-teeth teeth_uppper
                                                    
                                                    @if (in_array('7', $upper_vTeethInMouth))
                                                            {{'in-mouth '}}
                                                        @endif
                                                        @if (in_array('7', $upper_vMissingTeeth))
                                                            {{'missing '}}
                                                        @endif
                                                        @if (in_array('7', $upper_vWillExtractOnDelivery))
                                                            {{'will-extract-on-deliver '}}
                                                        @endif
                                                        @if (in_array('7', $upper_vHasBeenExtracted))
                                                            {{'ben-extracted '}}
                                                        @endif
                                                        @if (in_array('7', $upper_vFixOrAdd))
                                                            {{'add-or-fix '}}
                                                        @endif
                                                    
                                                    "></image>
                                                </a>

                                                <image id="uppper-7-claps" data-id="teeths1" data-name="7" x="498" y="50" xlink:href="{{asset('admin/assets/images/teeth/updc-7.png')}}" class=" clap-up-7 teeth_uppper upper-claps" style="
                                                @if (in_array('7', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif

                                                "></image>

                                                <text id="uppper-7-num" data-name="7" class="cls-1 teeth_uppper" transform="translate(563.111 198.624) scale(1.305)">
                                                    <tspan x="0">7</tspan>
                                                </text>

                                                <!-- upper-teeth 8 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-8-teeth" data-id="teeths8" data-name="8" x="607" y="35" xlink:href="{{asset('admin/assets/images/teeth/up-8.png')}}" class="img-for-teeth teeth_uppper
                                                    
                                                    @if (in_array('8', $upper_vTeethInMouth))
                                                            {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('8', $upper_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('8', $upper_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('8', $upper_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('8', $upper_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    "></image>
                                                </a>

                                                <image id="uppper-8-claps" data-id="teeths8" data-name="8" x="607" y="10" xlink:href="{{asset('admin/assets/images/teeth/updc-8.png')}}" class=" clap-up-7 teeth_uppper upper-claps" style="
                                                @if (in_array('8', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>

                                                <text id="uppper-8-num" data-name="8" class="cls-1 teeth_uppper" transform="translate(697.646 179.813) scale(1.305)">
                                                    <tspan x="0">8</tspan>
                                                </text>

                                                <!-- upper-teeth 9 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-9-teeth" data-id="teeths16" data-name="9" x="755" y="35" width="150" height="210" xlink:href="{{asset('admin/assets/images/teeth/up-9.png')}}" class="img-for-teeth teeth_uppper
                                                    
                                                    @if (in_array('9', $upper_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('9', $upper_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('9', $upper_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('9', $upper_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('9', $upper_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>

                                                <image id="uppper-9-claps" data-id="teeths16" data-name="9" x="765" y="10" xlink:href="{{asset('admin/assets/images/teeth/updc-9.png')}}" class=" clap-up-9 teeth_uppper upper-claps" style="

                                                @if (in_array('9', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>

                                                <text id="uppper-9-num" data-name="9" class="cls-1 teeth_uppper" transform="translate(821.604 176.548) scale(1.305)">
                                                    <tspan x="0">9</tspan>
                                                </text>

                                                <!-- upper-teeth 10 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-10-teeth" data-id="teeths9" data-name="10" x="889" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/teeth/up-10.png')}}" class="img-for-teeth teeth_uppper
                                                    
                                                    @if (in_array('10', $upper_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('10', $upper_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('10', $upper_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('10', $upper_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('10', $upper_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>

                                                <image id="uppper-10-claps" data-id="teeths9" data-name="10" x="910" y="50" xlink:href="{{asset('admin/assets/images/teeth/updc-10.png')}}" class=" clap-up-10 teeth_uppper upper-claps" style="
                                                @if (in_array('10', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif

                                                "></image>

                                                <text id="uppper-10-num" data-name="10" class="cls-1 teeth_uppper" transform="translate(934.785 195.164) scale(1.305)">
                                                    <tspan x="0">10</tspan>
                                                </text>

                                                <!-- upper-teeth 11 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-11-teeth" data-id="teeths10" data-name="11" x="972" y="123" width="137" height="178" xlink:href="{{asset('admin/assets/images/teeth/up-11.png')}}" class="img-for-teeth teeth_uppper
                                                    
                                                    @if (in_array('11', $upper_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('11', $upper_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('11', $upper_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('11', $upper_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('11', $upper_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    "></image>
                                                </a>

                                                <image id="uppper-11-claps" data-id="teeths10" data-name="11" x="1030" y="100" xlink:href="{{asset('admin/assets/images/teeth/updc-11.png')}}" class=" clap-up-11 teeth_uppper upper-claps" style="
                                                
                                                @if (in_array('11', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>

                                                <text id="uppper-11-num" data-name="11" class="cls-1 teeth_uppper" transform="translate(1031.677 240.099) scale(1.305)">
                                                    <tspan x="0">11</tspan>
                                                </text>

                                                <!-- upper-teeth 12 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-12-teeth" data-id="teeths11" data-name="12" x="1040" y="198" width="149" height="152" xlink:href="{{asset('admin/assets/images/teeth/up-12.png')}}" class="img-for-teeth teeth_uppper
                                                    
                                                    @if (in_array('12', $upper_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('12', $upper_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('12', $upper_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('12', $upper_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('12', $upper_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>

                                                <image id="uppper-12-claps" data-id="teeths11" data-name="12" x="1115" y="175" xlink:href="{{asset('admin/assets/images/teeth/updc-12.png')}}" class=" clap-up-12 teeth_uppper upper-claps upper-claps" style="
                                                
                                                @if (in_array('12', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>

                                                <text id="uppper-12-num" data-name="12" class="cls-1 teeth_uppper" transform="translate(1103.168 282.619) scale(1.305)">
                                                    <tspan x="0">12</tspan>
                                                </text>

                                                <!-- upper-teeth 13 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-13-teeth" data-id="teeths12" data-name="13" x="1095" y="284" width="154" height="149" xlink:href="{{asset('admin/assets/images/teeth/up-13.png')}}" class="img-for-teeth teeth_uppper
                                                    
                                                    @if (in_array('13', $upper_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('13', $upper_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('13', $upper_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('13', $upper_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('13', $upper_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif

                                                    "></image>
                                                </a>

                                                <image id="uppper-13-claps" data-id="teeths12" data-name="13" x="1215" y="270" xlink:href="{{asset('admin/assets/images/teeth/updc-13.png')}}" class=" clap-up-13 teeth_uppper upper-claps" style="
                                                @if (in_array('13', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>

                                                <text id="uppper-13-num" data-name="13" class="cls-1 teeth_uppper" transform="translate(1169.916 345.118) scale(1.305)">
                                                    <tspan x="0">13</tspan>
                                                </text>
                                                <!-- upper-teeth 14 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-14-teeth" data-id="teeths15" data-name="14" x="1115" y="361" width="221" height="199" xlink:href="{{asset('admin/assets/images/teeth/up-14.png')}}" class="img-for-teeth teeth_uppper
                                                    
                                                    @if (in_array('14', $upper_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('14', $upper_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('14', $upper_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('14', $upper_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('14', $upper_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>

                                                <image id="uppper-14-claps" data-id="teeths15" data-name="14" x="1280" y="361" xlink:href="{{asset('admin/assets/images/teeth/updc-14.png')}}" class=" clap-up-14 teeth_uppper upper-claps" style="

                                                @if (in_array('14', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>

                                                <text id="uppper-14-num" data-name="14" class="cls-1 teeth_uppper" transform="translate(1210.87 477.036) scale(1.305)">
                                                    <tspan x="0">14</tspan>
                                                </text>
                                                <!-- upper-teeth 15 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-15-teeth" data-id="teeths14" data-name="15" x="1194" y="503" width="203" height="188" xlink:href="{{asset('admin/assets/images/teeth/up-15.png')}}" class="img-for-teeth teeth_uppper
                                                    

                                                    @if (in_array('15', $upper_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('15', $upper_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('15', $upper_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('15', $upper_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('15', $upper_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>

                                                <image id="uppper-15-claps" data-id="teeths14" data-name="15" x="1350" y="503" xlink:href="{{asset('admin/assets/images/teeth/updc-15.png')}}" class=" clap-up-15 teeth_uppper upper-claps" style="
                                                @if (in_array('15', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>

                                                <text id="uppper-15-nuum" data-name="15" class="cls-1 teeth_uppper" transform="translate(1303.838 603.41) scale(1.305)">
                                                    <tspan x="0">15</tspan>
                                                </text>

                                                <!-- upper-teeth 16 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-16-teeth" data-id="teeths13" data-name="16" x="1262" y="643" width="192" height="156" xlink:href="{{asset('admin/assets/images/teeth/up-16.png')}}" class="img-for-teeth teeth_uppper
                                                    
                                                    @if (in_array('16', $upper_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('16', $upper_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('16', $upper_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('16', $upper_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('16', $upper_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>

                                                <image id="uppper-16-claps" data-id="teeths13" data-name="16" x="1422" y="643" xlink:href="{{asset('admin/assets/images/teeth/updc-16.png')}}" class=" clap-up-16 teeth_uppper upper-claps" style="
                                                @if (in_array('16', $upper_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>

                                                <text id="uppper-16-num" data-name="16" class="cls-1 teeth_uppper" transform="translate(1370.808 743.76) scale(1.305)">
                                                    <tspan x="0">16</tspan>
                                                </text>

                                                <!--upper teeth numbers  -->
                                            </g>
                                        </svg>

                                        <div class="d-block text-center teeth-text">
                                            <a href="javascript:;" class="add-btn p-2 mx-auto uppper_mising_all d-none" id="uppper_mising_all">
                                                Missing all teeth
                                            </a>
                                        </div>
                                        <div class="teeths-wrapper second-row p-0">
                                            <div class="row">
                                                <div class="col-md-12 col-xl-12 col-lg-12">
                                                    @if(!empty($upperstage))
                                                    <div class="row gy-2">
                                                        @if(!empty($upper_vTeethInMouth))
                                                        @php
                                                        $parts=array_filter($upper_vTeethInMouth);
                                                        $upper_vTeethInMouth = (implode(",",$parts));
                                                        @endphp
                                                        @endif

                                                        @if(!empty($upper_vTeethInMouth) && $upperstage->eTeethInMouth == "Yes")
                                                        <a href="javascript:;">
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <label class="form-control inner-label te-in-month lbl_btn_all" value="" id="upper_in_mouth" disabled> Teeth in mouth <br> <span id="upper_in_mouth_lbl">{{$upper_vTeethInMouth}}</span></label>
                                                                    <input type="hidden" id="upper_in_mouth_value" value="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                        @elseif($upperstage->eTeethInMouth == "No" && !empty($upper_product->vTeethInMouth) && $upperstage->eOpposingTeethInMouth == "Yes" && $upperstage->eOpposingExtractions == "Yes")
                                                        <a href="javascript:;">
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <label class="form-control inner-label te-in-month lbl_btn_all" value="" id="upper_in_mouth" disabled> Teeth in mouth <br> <span id="upper_in_mouth_lbl">{{$upper_product->vTeethInMouth}}</span></label>
                                                                    <input type="hidden" id="upper_in_mouth_value" value="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                        @endif


                                                        @if($upperstage->eMessingTeeth == "Yes" && !empty($upper_product->vMissingTeeth))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lbl_btn_all" value="" id="upper_missing_teeth" disabled>missing teeth <br> <span id="upper_missing_teeth_lbl">{{$upper_product->vMissingTeeth}}</span></label>
                                                                <input type="hidden" id="upper_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif($upperstage->eMessingTeeth == "No" && !empty($upper_product->vMissingTeeth) && $upperstage->eOpposingMessingTeeth == "Yes" && $upperstage->eOpposingExtractions == "Yes")
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lbl_btn_all" value="" id="upper_missing_teeth" disabled>missing teeth <br> <span id="upper_missing_teeth_lbl"></span></label>
                                                                <input type="hidden" id="upper_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif

                                                        @if($upperstage->eExtractDelivery == "Yes" && !empty($upper_product->vWillExtractOnDelivery))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lbl_btn_all" value="" id="upper_ectract_delivery" disabled>will extract on delivery <br><span id="upper_ectract_delivery_lbl">{{$upper_product->vWillExtractOnDelivery}}</span></label>
                                                                <input type="hidden" id="upper_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif($upperstage->eExtractDelivery == "No" && !empty($upper_product->vWillExtractOnDelivery) && $upperstage->eOpposingExtractDelivery == "Yes" && $upperstage->eOpposingExtractions == "Yes")
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lbl_btn_all" value="" id="upper_ectract_delivery" disabled>will extract on delivery <br><span id="upper_ectract_delivery_lbl"></span></label>
                                                                <input type="hidden" id="upper_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($upperstage->eExtracted == "Yes" && !empty($upper_product->vHasBeenExtracted))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lbl_btn_all" value="" id="upper_been_extracted" disabled> has been extracted <br><span id="upper_been_extracted_lbl">{{$upper_product->vHasBeenExtracted}}</span></label>
                                                                <input type="hidden" id="upper_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif($upperstage->eExtracted == "No" && !empty($upper_product->vHasBeenExtracted) && $upperstage->eOpposingExtracted == "Yes" && $upperstage->eOpposingExtractions == "Yes")
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lbl_btn_all" value="" id="upper_been_extracted" disabled> has been extracted <br><span id="upper_been_extracted_lbl"></span></label>
                                                                <input type="hidden" id="upper_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($upperstage->eFixorAdd == "Yes" && !empty($upper_product->vFixOrAdd))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lbl_btn_all" value="" id="upper_fix" disabled> fix or add <br> <span id="upper_fix_lbl">{{$upper_product->vFixOrAdd}}</span></label>
                                                                <input type="hidden" id="upper_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif($upperstage->eFixorAdd == "No" && !empty($upper_product->vFixOrAdd) && $upperstage->eOpposingFixorAdd == "Yes" && $upperstage->eOpposingExtractions == "Yes")
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lbl_btn_all" value="" id="upper_fix" disabled> fix or add <br> <span id="upper_fix_lbl"></span></label>
                                                                <input type="hidden" id="upper_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($upperstage->eClasps == "Yes" && !empty($upper_product->vClasps))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lbl_btn_all" value="" id="upper_clasps" disabled>clasps<br> <span id="upper_clasps_lbl">{{$upper_product->vClasps}}</span></label>
                                                                <input type="hidden" id="upper_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif($upperstage->eClasps == "No" && !empty($upper_product->vClasps) && $upperstage->eOpposingClasps == "Yes" && $upperstage->eOpposingExtractions == "Yes")
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lbl_btn_all" value="" id="upper_clasps" disabled>clasps<br> <span id="upper_clasps_lbl"></span></label>
                                                                <input type="hidden" id="upper_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                    @endif
                                                    <div class="row gy-2">

                                                        @if(!empty($opposing_product->vTeethInMouth))
                                                        <a href="javascript:;">
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <label class="form-control inner-label te-in-month lbl_btn_all" value="" id="upper_in_mouth" disabled> Teeth in mouth <br> <span id="upper_in_mouth_lbl">{{$opposing_product->vTeethInMouth}}</span></label>
                                                                    <input type="hidden" id="upper_in_mouth_value" value="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                        @endif


                                                        @if(!empty($opposing_product->vMissingTeeth))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lbl_btn_all" value="" id="upper_missing_teeth" disabled>missing teeth <br> <span id="upper_missing_teeth_lbl">{{$opposing_product->vMissingTeeth}}</span></label>
                                                                <input type="hidden" id="upper_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif

                                                        @if(!empty($opposing_product->vWillExtractOnDelivery))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lbl_btn_all" value="" id="upper_ectract_delivery" disabled>will extract on delivery <br><span id="upper_ectract_delivery_lbl">{{$opposing_product->vWillExtractOnDelivery}}</span></label>
                                                                <input type="hidden" id="upper_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>

                                                        @endif
                                                        @if(!empty($opposing_product->vHasBeenExtracted))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lbl_btn_all" value="" id="upper_been_extracted" disabled> has been extracted <br><span id="upper_been_extracted_lbl">{{$opposing_product->vHasBeenExtracted}}</span></label>
                                                                <input type="hidden" id="upper_been_extracted_value" value="">
                                                            </div>
                                                        </div>

                                                        @endif
                                                        @if(!empty($opposing_product->vFixOrAdd))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lbl_btn_all" value="" id="upper_fix" disabled> fix or add <br> <span id="upper_fix_lbl">{{$opposing_product->vFixOrAdd}}</span></label>
                                                                <input type="hidden" id="upper_fix_value" value="">
                                                            </div>
                                                        </div>

                                                        @endif
                                                        @if(!empty($opposing_product->vClasps))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lbl_btn_all" value="" id="upper_clasps" disabled>clasps<br> <span id="upper_clasps_lbl">{{$opposing_product->vClasps}}</span></label>
                                                                <input type="hidden" id="upper_clasps_value" value="">
                                                            </div>
                                                        </div>

                                                        @endif
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!-- view upper teeth End -->

                                <!-- middle inforamtion section start -->
                                <div class="col-xxl-6 col-lg-6 col-md-4 position-relative">
                                    <div class="row c-row-text">
                                        <div class="col-lg-12">
                                            <h3 class="card-title mb-0">
                                                CASE DESIGN
                                            </h3>
                                        </div>

                                        <div class="col-lg-12 mb-3" id="product_button_div" style="display: none;">
                                            <div class="row">
                                                <div class="col-lg-5 text-center">
                                                    <a href="javascript:;" class="btn add-btn me-2" id="product_upper_button_div"> Add Upper Product </a>
                                                </div>
                                                <div class="col-lg-2"></div>
                                                <div class="col-lg-5 text-center">
                                                    <a href="javascript:;" class="btn add-btn me-2" id="product_lower_button_div"> Add Lower Product </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @if(!empty($upper_product->vCategoryName) || !empty($lower_product->vCategoryName))
                                    <ul class="view-slip-mid-row-line mt-5">
                                        @if(!empty($upper_product->vCategoryName))
                                        <li class="view-slip-left-value">
                                            <span>
                                                {{$upper_product->vCategoryName}}
                                            </span>
                                        </li>
                                        @endif
                                        <li class="view-slip-mid-title">
                                            <span> <strong> Category</strong></span>
                                        </li>
                                        @if(!empty($lower_product->vCategoryName))
                                        <li class="view-slip-right-value">
                                            <span>
                                                {{$lower_product->vCategoryName}}
                                            </span>
                                        </li>
                                        @endif
                                    </ul>
                                    @endif

                                    @if(!empty($upper_product->vProductName) || !empty($lower_product->vProductName))
                                    <ul class="view-slip-mid-row-line">
                                        @if(!empty($upper_product->vProductName))
                                        <li class="view-slip-left-value">
                                            <span>
                                                {{$upper_product->vProductName}}
                                            </span>
                                        </li>
                                        @endif
                                        <li class="view-slip-mid-title">
                                            <span> <strong> Product</strong></span>
                                        </li>
                                        @if(!empty($lower_product->vProductName))
                                        <li class="view-slip-right-value">
                                            <span>
                                                {{$lower_product->vProductName}}
                                            </span>
                                        </li>
                                        @endif
                                    </ul>
                                    @endif


                                    @if(!empty($upper_product->vGradeName) || !empty($lower_product->vGradeName))
                                    <ul class="view-slip-mid-row-line">
                                        @if(!empty($upper_product->vGradeName))
                                        <li class="view-slip-left-value">
                                            <span>
                                                {{$upper_product->vGradeName}}
                                            </span>
                                        </li>
                                        @endif
                                        <li class="view-slip-mid-title">
                                            <span> <strong> Grade</strong></span>
                                        </li>
                                        @if(!empty($lower_product->vGradeName))
                                        <li class="view-slip-right-value">
                                            <span>
                                                {{$lower_product->vGradeName}}
                                            </span>
                                        </li>
                                        @endif
                                    </ul>
                                    @endif

                                    @if(!empty($upper_product->vStageName) || !empty($lower_product->vStageName))
                                    <ul class="view-slip-mid-row-line">
                                        @if(!empty($upper_product->vStageName))
                                        <li class="view-slip-left-value">
                                            <span>
                                                {{$upper_product->vStageName}}
                                            </span>
                                        </li>
                                        @endif
                                        <li class="view-slip-mid-title">
                                            <span> <strong> Stage</strong></span>
                                        </li>
                                        @if(!empty($lower_product->vStageName))
                                        <li class="view-slip-right-value">
                                            <span>
                                                {{$lower_product->vStageName}}
                                            </span>
                                        </li>
                                        @endif
                                    </ul>
                                    @endif

                                    @if(!empty($upper_impression) && sizeof($upper_impression) > 0 && !empty($lower_impression) && sizeof($lower_impression) > 0 )
                                    <ul class="view-slip-mid-row-line">
                                        <li class="view-slip-left-value">
                                            @foreach ($upper_impression as $value_impression)
                                            <span class="mr-20px">
                                                {{$value_impression->vImpressionName}}
                                            </span>
                                            <span> Qty {{$value_impression->iQuantity}}</span>
                                            <br>
                                            @endforeach

                                        </li>
                                        <li class="view-slip-mid-title">
                                            <span> <strong> Impression</strong></span>
                                        </li>
                                        <li class="view-slip-right-value">
                                            @foreach ($lower_impression as $lower_impression)
                                            <span class="mr-20px"> Qty {{$lower_impression->iQuantity}}</span>
                                            <span>
                                                {{$lower_impression->vImpressionName}}
                                            </span>
                                            <br>
                                            @endforeach
                                        </li>
                                    </ul>
                                    @endif
                                    @if(!empty($upper_product->vBrandName) || !empty($lower_product->vBrandName))
                                    <ul class="view-slip-mid-row-line">
                                        @if(!empty($upper_product->vBrandName))
                                        <li class="view-slip-left-value">
                                            <span class="mr-20px">
                                                {{$upper_product->vBrandName}}
                                            </span>

                                            <span> {{$upper_product->vToothShade}}</span>

                                        </li>
                                        @endif
                                        <li class="view-slip-mid-title">
                                            <span> <strong>Teeth shade </strong> </span>
                                        </li>
                                        @if(!empty($lower_product->vBrandName))
                                        <li class="view-slip-right-value">
                                            <span class="mr-20px"> {{$lower_product->vBrandName}}</span>
                                            <span>
                                                {{$lower_product->vToothShade}}
                                            </span>
                                        </li>
                                        @endif
                                    </ul>
                                    @endif

                                    @if(!empty($upper_product->vGumBrand) || !empty($lower_product->vGumBrand))

                                    <ul class="view-slip-mid-row-line">
                                        @if(!empty($upper_product->vGumBrand))
                                        <li class="view-slip-left-value">
                                            <span class="mr-20px">
                                                {{$upper_product->vGumBrand}}
                                            </span>

                                            <span> {{$upper_product->vToothShade}}</span>

                                        </li>
                                        @endif
                                        <li class="view-slip-mid-title">
                                            <span> <strong>Gum shade </strong> </span>
                                        </li>
                                        @if(!empty($lower_product->vGumBrand))
                                        <li class="view-slip-right-value">
                                            <span class="mr-20px"> {{$lower_product->vGumBrand}}</span>

                                            <span>
                                                {{$lower_product->vGumShade}}
                                            </span>
                                        </li>
                                        @endif
                                    </ul>
                                    @endif
                                    @if(!empty($upper_addons) && sizeof($upper_addons) > 0 && !empty($lower_addons) && sizeof($lower_addons) > 0)
                                    <ul class="view-slip-mid-row-line">

                                        <li class="view-slip-left-value">
                                            @foreach ($upper_addons as $value)
                                            <span>
                                                {{$value->vAddonCategoryName}}
                                            </span>
                                            &nbsp;
                                            <span> {{$value->vAddonName}}</span>
                                            &nbsp;
                                            <span>{{$value->iQuantity}}</span>
                                            <br>
                                            @endforeach
                                        </li>

                                        <li class="view-slip-mid-title">
                                            <span> <strong>Add ons</strong> </span>
                                        </li>

                                        <li class="view-slip-left-value text-start">
                                            @foreach ($lower_addons as $lower_value)

                                            <span>
                                                {{$lower_value->iQuantity}}
                                            </span>
                                            &nbsp;

                                            <span> {{$lower_value->vAddonName}}</span>
                                            &nbsp;
                                            <span>{{$lower_value->vAddonCategoryName}}</span>
                                            <br>
                                            @endforeach
                                        </li>

                                    </ul>
                                    @endif

                                    @if(!empty($upper_product->eStatus) || !empty($lower_product->eStatus))
                                    <ul class="view-slip-mid-row-line">
                                        @if(!empty($upper_product->eStatus))
                                        <li class="view-slip-left-value">
                                            <span>
                                                {{$upper_product->eStatus}}
                                            </span>

                                        </li>
                                        @endif
                                        <li class="view-slip-mid-title">
                                            <span> <strong> Stage status</strong></span>
                                        </li>
                                        @if(!empty($lower_product->eStatus))
                                        <li class="view-slip-right-value">
                                            <span>
                                                {{$lower_product->eStatus}}
                                            </span>
                                        </li>
                                        @endif
                                    </ul>
                                    @endif
                                    @if($upper_product->eRushDate == "Yes" || $lower_product->eRushDate == "Yes")
                                    <ul class="view-slip-mid-row-line">
                                        @if(!empty($upper_product))
                                        <li class="view-slip-left-value">
                                            @if($upper_product->eRushDate == "Yes")

                                            <span> {{date('m/d/Y', strtotime($upper_product->dDeliveryDate))}}</span>

                                            @endif
                                        </li>
                                        @endif
                                        <li class="view-slip-mid-title">
                                            <span> <strong> Delivery Date</strong> </span>
                                        </li>
                                        @if(!empty($lower_product))
                                        <li class="view-slip-right-value">
                                            @if($lower_product->eRushDate == "Yes")
                                            <span> {{date('m/d/Y', strtotime($lower_product->dDeliveryDate))}}</span>

                                            @endif
                                        </li>
                                        @endif
                                    </ul>
                                    <ul class="view-slip-mid-row-line">
                                        @if(!empty($upper_product))
                                        <li class="view-slip-left-value">
                                            @if($upper_product->eRushDate == "Yes")

                                            <span> {{$upper_product->tDeliveryTime}}</span>

                                            @endif
                                        </li>
                                        @endif
                                        <li class="view-slip-mid-title">
                                            <span> <strong>Delivery Time</strong> </span>
                                        </li>
                                        @if(!empty($lower_product))
                                        <li class="view-slip-right-value">
                                            @if($lower_product->eRushDate == "Yes")
                                            <span class="mr-20px"> {{$lower_product->tDeliveryTime}}</span>
                                            @endif
                                        </li>
                                        @endif
                                    </ul>
                                    @endif


                                    <div class="row position-absolute w-100" style="bottom: 0;">
                                        <div class="col-6 text-end">
                                            <a href="javascript:;" class="btn add-btn"> Previous Slip</a>
                                        </div>


                                        <div class="col-6 text-start">
                                            <a href="javascript:;" class="btn add-btn"> Next slip </a>
                                        </div>
                                    </div>
                                </div>

                                <!-- middle inforamtion section End -->


                                <!-- view Lower teeth start -->
                                <div class="col-xxl-3 col-lg-3 col-md-4">
                                    @if (!empty($lower_product))
                                    <div class="teeth-wrapper lower" id="lower_extraction">
                                        <h3 class="card-title mb-0">
                                            MANDIBULAR
                                        </h3>
                                        <svg width="100%" height="100%" viewBox="0 0 1500 790" class="teeth-svg">

                                            <g>
                                                <g></g>
                                                <!-- lower-teeth 1 -->
                                                <a href="javascript:;">
                                                    <image id="lower-17-teeth" data-id="teeth15" data-name="17" x="30" y="555" width="252" height="200" xlink:href="{{asset('admin/assets/images/teeth/d-17.png')}}" class="img-for-teeth  teeth_lower
                                                    
                                                    @if (in_array('17', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('17', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('17', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('17', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('17', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>

                                                <!--  image with cliap-1 -->
                                                <image id="lower-17-claps" data-name="17" x="15" y="540" width="80" height="190" xlink:href="{{asset('admin/assets/images/teeth/dc-17.png')}}" class=" clap-17 teeth_lower lower-claps" style="
                                                @if (in_array('17', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>

                                                <text id="lower-17-num" data-name="17" class="cls-1 teeth_lower" transform="translate(196.632 625.98) scale(1.199)">
                                                    <tspan x="-30">17</tspan>
                                                </text>


                                                <!-- lower-teeth 1 end-->

                                                <g></g>
                                                <!-- lower-teeth 2 -->
                                                <a href="javascript:;">
                                                    <image id="lower-18-teeth" data-id="teeth7" data-name="18" x="95" y="400" width="232" height="194" xlink:href="{{asset('admin/assets/images/teeth/d-18.png')}}" class="img-for-teeth teeth_lower
                                                    

                                                    @if (in_array('18', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('18', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('18', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('18', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('18', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>
                                                <!--  image with cliap-2 -->
                                                <image id="lower-18-claps" data-name="18" x="80" y="382" width="85" height="194" xlink:href="{{asset('admin/assets/images/teeth/dc-18.png')}}" class=" clap-18 teeth_lower lower-claps" style="
                                                
                                                    @if (in_array('18', $lower_vClasps))
                                                        {{' '}}
                                                    @else
                                                        {{'display: none; '}}
                                                        
                                                    @endif
                                                
                                                
                                                "></image>

                                                <text id="lower-18-num" data-name="18" class="cls-1 teeth_lower" transform="translate(263.156 536.61) scale(1.199)">
                                                    <tspan x="-35" y="-30">18</tspan>
                                                </text>

                                                <!-- lower-teeth 2 end-->

                                                <g></g>
                                                <!-- lower-teeth 3 -->
                                                <a href="javascript:;">
                                                    <image id="lower-19-teeth" data-id="teeth6" data-name="19" x="165" y="290" width="235" height="191" xlink:href="{{asset('admin/assets/images/teeth/d-19.png')}}" class="img-for-teeth teeth_lower
                                                    
                                                    @if (in_array('19', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('19', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('19', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('19', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('19', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    "></image>
                                                </a>

                                                <text id="lower-19-num" data-name="19" class="cls-1 teeth_lower" transform="translate(329.927 426.297) scale(1.199)">
                                                    <tspan x="-35" y="-35">19</tspan>
                                                </text>

                                                <!--  image with cliap-3 -->
                                                <image id="lower-19-claps" data-name="19" x="155" y="255" width="100" height="191" xlink:href="{{asset('admin/assets/images/teeth/dc-19.png')}}" class=" clap-19 teeth_lower lower-claps" style="
                                                @if (in_array('19', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>

                                                <!-- lower-teeth 3 end-->


                                                <g></g>
                                                <!-- lower-teeth 4 start-->
                                                <a href="javascript:;">
                                                    <image id="lower-20-teeth" data-id="teeth5" data-name="20" x="265" y="225" width="167" height="159" xlink:href="{{asset('admin/assets/images/teeth/d-20.png')}}" class="img-for-teeth teeth_lower
                                                    
                                                    @if (in_array('20', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('20', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('20', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('20', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('20', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    
                                                    "></image>
                                                </a>

                                                <text id="lower-20-num" data-name="20" class="cls-1 teeth_lower" transform="translate(401.061 332.076) scale(1.199)">
                                                    <tspan x="-30" y="-35">20</tspan>
                                                </text>

                                                <!--  image with cliap-4 -->
                                                <image id="lower-20-claps" data-name="20" x="240" y="175" width="100" height="159" xlink:href="{{asset('admin/assets/images/teeth/dc-20.png')}}" class=" clap-20 teeth_lower lower-claps" style="
                                                @if (in_array('20', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif

                                                
                                                "></image>

                                                <!-- lower-teeth 4 end-->

                                                <g></g>
                                                <!-- lower-teeth 5 -->
                                                <a href="javascript:;">
                                                    <image id="lower-21-teeth" data-id="teeth3" data-name="21" x="342" y="153" width="150" height="177" xlink:href="{{asset('admin/assets/images/teeth/d-21.png')}}" class="img-for-teeth teeth_lower
                                                    
                                                    @if (in_array('21', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('21', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('21', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('21', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('21', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    
                                                    "></image>
                                                </a>
                                                <!--  image with cliap-5 -->
                                                <image id="lower-21-claps" data-name="21" x="300" y="135" width="140" height="90" xlink:href="{{asset('admin/assets/images/teeth/dc-21.png')}}" class=" clap-21 teeth_lower lower-claps" style="

                                                @if (in_array('21', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-21-num" data-name="21" class="cls-1 teeth_lower" transform="translate(455.319 283.973) scale(1.199)">
                                                    <tspan x="-30" y="-50">21</tspan>
                                                </text>

                                                <g></g>
                                                <!-- lower-teeth 6 -->
                                                <a href="javascript:;">
                                                    <image id="lower-22-teeth" data-id="teeth4" data-name="22" x="435" y="93" width="135" height="200" xlink:href="{{asset('admin/assets/images/teeth/d-22.png')}}" class="img-for-teeth teeth_lower
                                                    
                                                    @if (in_array('22', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('22', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('22', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('22', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('22', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    
                                                    "></image>
                                                </a>

                                                <!--  image with cliap-22 -->
                                                <image id="lower-22-claps" data-name="22" x="430" y="15" width="100" height="200" xlink:href="{{asset('admin/assets/images/teeth/dc-22.png')}}" class=" clap-22 teeth_lower lower-claps" style="
                                                
                                                @if (in_array('22', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-22-num" data-name="22" class="cls-1 teeth_lower" transform="translate(517.12 257.438) scale(1.199)">
                                                    <tspan x="0" y="-50">22</tspan>
                                                </text>

                                                <g></g>
                                                <!-- lower-teeth 7 -->
                                                <a href="javascript:;">
                                                    <image id="lower-23-teeth" data-id="teeth2" data-name="23" x="540" y="60" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/d-23.png')}}" class="img-for-teeth teeth_lower
                                                    
                                                    @if (in_array('23', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('23', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('23', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('23', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('23', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    
                                                    "></image>
                                                </a>
                                                <!-- image with cliap-23 -->
                                                <image id="lower-23-claps" data-name="23" x="525" y="-33" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/dc-23.png')}}" class=" clap-23 teeth_lower lower-claps" style="
                                                @if (in_array('23', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-23-num" data-name="23" class="cls-1 teeth_lower" transform="translate(602.467 207.332) scale(1.199)">
                                                    <tspan x="0" y="-50">23</tspan>
                                                </text>


                                                <g></g>

                                                <!-- lower-teeth 8 -->
                                                <a href="javascript:;">
                                                    <image id="lower-24-teeth" data-id="teeth1" data-name="24" x="647" y="40" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/d-24.png')}}" class="img-for-teeth teeth_lower
                                                    

                                                    @if (in_array('24', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('24', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('24', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('24', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('24', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>
                                                <!-- image with cliap-8 -->
                                                <image id="lower-24-claps" data-name="24" x="647" y="-60" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/dc-24.png')}}" class=" clap-24 teeth_lower lower-claps" style="
                                                
                                                @if (in_array('24', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-24-num" data-name="24" class="cls-1 teeth_lower" transform="translate(703.596 190.046) scale(1.199)">
                                                    <tspan x="0" y="-40">24</tspan>
                                                </text>

                                                <g></g>
                                                <!-- lower-teeth 9 -->
                                                <a href="javascript:;">
                                                    <image id="lower-25-teeth" data-id="teeth8" data-name="25" x="760" y="42" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/d-25.png')}}" class="img-for-teeth teeth_lower
                                                    
                                                    @if (in_array('25', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('25', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('25', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('25', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('25', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif

                                                    "></image>
                                                </a>
                                                <!--  image with cliap-9 -->
                                                <image id="lower-25-claps" data-name="25" x="762" y="-58" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/dc-25.png')}}" class=" clap-25 teeth_lower lower-claps" style="
                                                
                                                @if (in_array('25', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-25-num" data-name="25" class="cls-1 teeth_lower" transform="translate(789.005 190.046) scale(1.199)">
                                                    <tspan x="10" y="-50">25</tspan>
                                                </text>
                                                <g></g>
                                                <!-- lower-teeth 10 -->
                                                <a href="javascript:;">
                                                    <image id="lower-26-teeth" data-id="teeth9" data-name="26" x="865" y="54" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/d-26.png')}}" class="img-for-teeth teeth_lower
                                                    

                                                    @if (in_array('26', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('26', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('26', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('26', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('26', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    
                                                    "></image>
                                                </a>
                                                <!--  image with cliap-11 -->
                                                <image id="lower-26-claps" data-name="26" x="880" y="-35" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/dc-26.png')}}" class=" clap-26 teeth_lower lower-claps" style="

                                                @if (in_array('26', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-26-num" data-name="26" class="cls-1 teeth_lower" transform="translate(881.008 207.152) scale(1.199)">
                                                    <tspan x="25" y="-50">26</tspan>
                                                </text>
                                                <!-- lower-teeth 11-->
                                                <a href="javascript:;">
                                                    <image id="lower-27-teeth" data-id="teeth11" data-name="27" x="955" y="80" width="135" height="200" xlink:href="{{asset('admin/assets/images/teeth/d-27.png')}}" class="img-for-teeth teeth_lower
                                                    
                                                    @if (in_array('27', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('27', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('27', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('27', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('27', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>
                                                <!--  image with cliap-11 -->
                                                <image id="lower-27-claps" data-name="27" x="990" y="-5" width="110" height="200" xlink:href="{{asset('admin/assets/images/teeth/dc-27.png')}}" class=" clap-27 teeth_lower lower-claps" style="

                                                @if (in_array('27', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-27-num" data-name="27" class="cls-1 teeth_lower" transform="translate(982.792 240.944) scale(1.199)">
                                                    <tspan x="25" y="-50">27</tspan>
                                                </text>
                                                <!-- lower-teeth 12 -->
                                                <a href="javascript:;">
                                                    <image id="lower-28-teeth" data-id="teeth10" data-name="28" x="1035" y="134" width="150" height="177" xlink:href="{{asset('admin/assets/images/teeth/d-28.png')}}" class="img-for-teeth teeth_lower
                                                    
                                                    @if (in_array('28', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('28', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('28', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('28', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('28', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>
                                                <!--  image with cliap-12 -->
                                                <image id="lower-28-claps" data-name="28" x="1100" y="60" width="100" height="177" xlink:href="{{asset('admin/assets/images/teeth/dc-28.png')}}" class=" clap-28 teeth_lower lower-claps" style="
                                                @if (in_array('28', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-28-num" data-name="28" class="cls-1 teeth_lower" transform="translate(1037.236 283.518) scale(1.199)">
                                                    <tspan x="40" y="-50">28</tspan>
                                                </text>
                                                <!-- lower-teeth 13 -->
                                                <a href="javascript:;">
                                                    <image id="lower-29-teeth" data-id="teeth12" data-name="29" x="1098" y="205" width="167" height="159" xlink:href="{{asset('admin/assets/images/teeth/d-29.png')}}" class="img-for-teeth teeth_lower
                                                    
                                                    @if (in_array('29', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('29', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('29', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('29', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('29', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>
                                                <!--  image with cliap-13 -->
                                                <image id="lower-29-claps" data-name="29" x="1190" y="155" width="100" height="159" xlink:href="{{asset('admin/assets/images/teeth/dc-29.png')}}" class=" clap-29 teeth_lower lower-claps" style="
                                                @if (in_array('29', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>
                                                <text id="lower-29-num" data-name="29" class="cls-1 teeth_lower" transform="translate(1097.072 329.95) scale(1.199)">
                                                    <tspan x="45" y="-50">29</tspan>
                                                </text>
                                                <!-- lower-teeth 14 -->
                                                <a href="javascript:;">
                                                    <image id="lower-30-teeth" data-id="teeth13" data-name="30" x="1130" y="278" width="234" height="191" xlink:href="{{asset('admin/assets/images/teeth/d-30.png')}}" class="img-for-teeth teeth_lower
                                                    
                                                    @if (in_array('30', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('30', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('30', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('30', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('30', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>
                                                <!--  image with cliap-14 -->
                                                <image id="lower-30-claps" data-name="30" x="1280" y="258" width="100" height="191" xlink:href="{{asset('admin/assets/images/teeth/dc-30.png')}}" class=" clap-30 teeth_lower lower-claps" style="
                                                @if (in_array('30', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                
                                                "></image>
                                                <text id="lower-30-num" data-name="30" class="cls-1 teeth_lower" transform="translate(1164.707 409.172) scale(1.199)">
                                                    <tspan x="45" y="-45">30</tspan>
                                                </text>
                                                <!-- lower-teeth 15 -->

                                                <a href="javascript:;">
                                                    <image id="lower-31-teeth" data-id="teeth14" data-name="31" x="1210" y="395" width="232" height="194" xlink:href="{{asset('admin/assets/images/teeth/d-31.png')}}" class="img-for-teeth teeth_lower
                                                    
                                                    @if (in_array('31', $lower_vTeethInMouth))
                                                    {{'in-mouth '}}
                                                    @endif
                                                    @if (in_array('31', $lower_vMissingTeeth))
                                                        {{'missing '}}
                                                    @endif
                                                    @if (in_array('31', $lower_vWillExtractOnDelivery))
                                                        {{'will-extract-on-deliver '}}
                                                    @endif
                                                    @if (in_array('31', $lower_vHasBeenExtracted))
                                                        {{'ben-extracted '}}
                                                    @endif
                                                    @if (in_array('31', $lower_vFixOrAdd))
                                                        {{'add-or-fix '}}
                                                    @endif
                                                    
                                                    "></image>
                                                </a>
                                                <!--  image with cliap-15 -->
                                                <image id="lower-31-claps" data-name="31" x="1390" y="420" width="80" height="130" xlink:href="{{asset('admin/assets/images/teeth/dc-30.png')}}" class=" clap-31 teeth_lower lower-claps" style="
                                                @if (in_array('31', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>
                                                <text id="lower-31-num" data-name="31" class="cls-1 teeth_lower" transform="translate(1238.134 502.8) scale(1.199)">
                                                    <tspan x="50">31</tspan>
                                                </text>
                                                <!-- lower-teeth 16 -->

                                                <a href="javascript:;">
                                                    <image id="lower-32-teeth" data-id="teeth16" data-name="32" x="1273" y="560" width="232" height="193" xlink:href="{{asset('admin/assets/images/teeth/d-32.png')}}" class="img-for-teeth teeth_lower
                                                    
                                                        @if (in_array('32', $lower_vTeethInMouth))
                                                        {{'in-mouth '}}
                                                        @endif
                                                        @if (in_array('32', $lower_vMissingTeeth))
                                                            {{'missing '}}
                                                        @endif
                                                        @if (in_array('32', $lower_vWillExtractOnDelivery))
                                                            {{'will-extract-on-deliver '}}
                                                        @endif
                                                        @if (in_array('32', $lower_vHasBeenExtracted))
                                                            {{'ben-extracted '}}
                                                        @endif
                                                        @if (in_array('32', $lower_vFixOrAdd))
                                                            {{'add-or-fix '}}
                                                        @endif

                                                       
                                                    "></image>
                                                </a>
                                                <!--  image with cliap-32 -->
                                                <image id="lower-32-claps" data-name="32" x="1440" y="580" width="114" height="92" xlink:href="{{asset('admin/assets/images/teeth/dc-32.png')}}" class=" clap-32 teeth_lower lower-claps" style="
                                                
                                                @if (in_array('32', $lower_vClasps))
                                                    {{' '}}
                                                @else
                                                    {{'display: none; '}}
                                                    
                                                @endif
                                                "></image>
                                                <text id="lower-32-num" data-name="32" class="cls-1 teeth_lower" transform="translate(1268.177 618.27) scale(1.199)">
                                                    <tspan x="80" y="20">32</tspan>
                                                </text>

                                                <!--lower teeth numbers ------------------------------ -->

                                            </g>

                                        </svg>

                                        <div class="d-block text-center teeth-text">
                                            <a href="javascript:;" class="add-btn p-2 mx-auto lower_mising_all d-none">
                                                Missing all teeth
                                            </a>
                                        </div>
                                        <div class="teeths-wrapper second-row p-0">
                                            <div class="row">
                                                <div class="col-md-12 col-xl-12 col-lg-12">
                                                    @if(!empty($lowerstage))
                                                    <div class="row gy-2">

                                                        @if(!empty($lower_vTeethInMouth))
                                                        @php
                                                        $parts=array_filter($lower_vTeethInMouth);
                                                        $lower_vTeethInMouth = (implode(",",$parts));
                                                        @endphp
                                                        @endif
                                                        @if(!empty($lower_vTeethInMouth) && $lowerstage->eTeethInMouth == "Yes")
                                                        <a href="javascript:;">
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <label class="form-control inner-label te-in-month lower_lbl_btn_all" value="" id="lower_in_mouth" disabled> Teeth in mouth <br> <span id="lower_in_mouth_lbl">{{$lower_vTeethInMouth}}</span></label>
                                                                    <input type="hidden" id="lower_in_mouth_value" value="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                        @elseif(!empty($lower_product->vTeethInMouth) && $lowerstage->eTeethInMouth == "No" && $lowerstage->eOpposingTeethInMouth == "Yes" && $lowerstage->eOpposingExtractions == "Yes")
                                                        <a href="javascript:;">
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <label class="form-control inner-label te-in-month lower_lbl_btn_all" value="" id="lower_in_mouth" disabled> Teeth in mouth <br> <span id="lower_in_mouth_lbl"></span></label>
                                                                    <input type="hidden" id="lower_in_mouth_value" value="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                        @endif
                                                        @if($lowerstage->eMessingTeeth == "Yes" && !empty($lower_product->vMissingTeeth))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lower_lbl_btn_all" value="" id="lower_missing_teeth" disabled>missing teeth <br> <span id="lower_missing_teeth_lbl">{{$lower_product->vMissingTeeth}}</span></label>
                                                                <input type="hidden" id="lower_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif($lowerstage->eMessingTeeth == "No" && !empty($lower_product->vMissingTeeth) && $lowerstage->eOpposingMessingTeeth == "Yes" && $lowerstage->eOpposingExtractions == "Yes")
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lower_lbl_btn_all" value="" id="lower_missing_teeth" disabled>missing teeth <br> <span id="lower_missing_teeth_lbl"></span></label>
                                                                <input type="hidden" id="lower_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($lowerstage->eExtractDelivery == "Yes" && !empty($lower_product->vWillExtractOnDelivery))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lower_lbl_btn_all" value="" id="lower_ectract_delivery" disabled>will extract on delivery <br><span id="lower_ectract_delivery_lbl">{{$lower_product->vWillExtractOnDelivery}}</span></label>
                                                                <input type="hidden" id="lower_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif($lowerstage->eExtractDelivery == "No" && !empty($lower_product->vWillExtractOnDelivery) && $lowerstage->eOpposingExtractDelivery == "Yes" && $lowerstage->eOpposingExtractions == "Yes")
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lower_lbl_btn_all" value="" id="lower_ectract_delivery" disabled>will extract on delivery <br><span id="lower_ectract_delivery_lbl"></span></label>
                                                                <input type="hidden" id="lower_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($lowerstage->eExtracted == "Yes" && !empty($lower_product->vHasBeenExtracted))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lower_lbl_btn_all" value="" id="lower_been_extracted" disabled> has been extracted <br><span id="lower_been_extracted_lbl">{{$lower_product->vHasBeenExtracted}}</span></label>
                                                                <input type="hidden" id="lower_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif($lowerstage->eExtracted == "No" && !empty($lower_product->vHasBeenExtracted) && $lowerstage->eOpposingExtract == "Yes" && $lowerstage->eOpposingExtractions == "Yes")
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lower_lbl_btn_all" value="" id="lower_been_extracted" disabled> has been extracted <br><span id="lower_been_extracted_lbl"></span></label>
                                                                <input type="hidden" id="lower_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($lowerstage->eFixorAdd == "Yes" && !empty($lower_product->vFixOrAdd))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lower_lbl_btn_all" value="" id="lower_fix" disabled> fix or add <br> <span id="lower_fix_lbl">{{$lower_product->vFixOrAdd}}</span></label>
                                                                <input type="hidden" id="lower_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif($lowerstage->eFixorAdd == "No" && !empty($lower_product->vFixOrAdd) && $lowerstage->eOpposingFixorAdd == "Yes" && $lowerstage->eOpposingExtractions == "Yes")
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lower_lbl_btn_all" value="" id="lower_fix" disabled> fix or add <br> <span id="lower_fix_lbl"></span></label>
                                                                <input type="hidden" id="lower_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($lowerstage->eClasps == "Yes" && !empty($lower_product->vClasps))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lower_lbl_btn_all" value="" id="lower_clasps" disabled>clasps<br> <span id="lower_clasps_lbl">{{$lower_product->vClasps}}</span></label>
                                                                <input type="hidden" id="lower_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @elseif($lowerstage->eClasps == "No" && !empty($lower_product->vClasps) && $lowerstage->eOpposingClasps == "Yes" && $lowerstage->eOpposingExtractions == "Yes")
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lower_lbl_btn_all" value="" id="lower_clasps" disabled>clasps<br> <span id="lower_clasps_lbl"></span></label>
                                                                <input type="hidden" id="lower_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                    @else
                                                    <div class="row gy-2">

                                                        @if(!empty($opposing_product->eTeethInMouth))
                                                        <a href="javascript:;">
                                                            <div class="col-md-12">
                                                                <div class="input-group">
                                                                    <label class="form-control inner-label te-in-month lower_lbl_btn_all" value="" id="lower_in_mouth" disabled> Teeth in mouth <br> <span id="lower_in_mouth_lbl">{{$opposing_product->eTeethInMouth}}</span></label>
                                                                    <input type="hidden" id="lower_in_mouth_value" value="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                        @endif
                                                        @if(!empty($opposing_product->vMissingTeeth))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label missing-te lower_lbl_btn_all" value="" id="lower_missing_teeth" disabled>missing teeth <br> <span id="lower_missing_teeth_lbl">{{$opposing_product->vMissingTeeth}}</span></label>
                                                                <input type="hidden" id="lower_missing_teeth_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if(!empty($opposing_product->vWillExtractOnDelivery))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ex-dekivery lower_lbl_btn_all" value="" id="lower_ectract_delivery" disabled>will extract on delivery <br><span id="lower_ectract_delivery_lbl">{{$opposing_product->vWillExtractOnDelivery}}</span></label>
                                                                <input type="hidden" id="lower_ectract_delivery_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if(!empty($opposing_product->vHasBeenExtracted))
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label ben-extracted lower_lbl_btn_all" value="" id="lower_been_extracted" disabled> has been extracted <br><span id="lower_been_extracted_lbl">{{$opposing_product->vHasBeenExtracted}}</span></label>
                                                                <input type="hidden" id="lower_been_extracted_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if(!empty($opposing_product->vFixOrAdd))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label fix-add lower_lbl_btn_all" value="" id="lower_fix" disabled> fix or add <br> <span id="lower_fix_lbl">{{$opposing_product->vFixOrAdd}}</span></label>
                                                                <input type="hidden" id="lower_fix_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if(!empty($opposing_product->vClasps))
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label clasps lower_lbl_btn_all" value="" id="lower_clasps" disabled>clasps<br> <span id="lower_clasps_lbl">{{$opposing_product->vClasps}}</span></label>
                                                                <input type="hidden" id="lower_clasps_value" value="">
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <!-- view Lower teeth end -->



                                <!-- <div class="col-lg-12 stagenote-text-area" id="notes_div">
                                    <div id="toolbar-container"></div>
                                    <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Description" disabled>
                                    {{$slip->tStagesNotes}}
                                    </textarea>
                                    <div class="stage-title-bold">
                                        <div class="row w-100">
                                            <div class="col-xxl-5 col-lg-5 col-md-5 text-center mx-auto position-relative">
                                                <div class="d-flex justify-content-end">
                                                    <h3 class="card-title text-start" style="width: 38%;">
                                                        Stage Notes
                                                    </h3>
                                                    <ul class="note-btn-group">
                                                        <li>
                                                            <a href="javascript:;" id="add_attachments_model_open">
                                                                <i class="fad fa-link"></i>
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="javascript:;" id="free_drawing_open">
                                                                <i class="fad fa-pencil"></i>
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="javascript:;" id="add_video_open">
                                                                <i class="fad fa-video"></i>
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="javascript:;" id="add_recording_open">
                                                                <i class="fad fa-microphone"></i>
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="javascript:;" id="link_model_open">
                                                                <i class="fad fa-external-link-alt"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="stagenote-bottom-btn">
                                        <a type="submit" class="btn submit-btn me-2">Add notes</a>
                                    </div>
                                </div> -->



                                <div class="col-lg-12 stagenote-text-area" id="notes_div">
                                    <div id="toolbar-container"></div>
                                    <div class="form-control" id="tDescription" name="tDescription" placeholder="Description" disabled>
                                        {{$slip->tStagesNotes}}
                                    </div>
                                    <div class="stage-title-bold">
                                        <div class="row w-100">
                                            <div class="col-xxl-5 col-lg-5 col-md-5 text-center mx-auto position-relative">
                                                <div class="d-flex justify-content-end">
                                                    <h3 class="card-title text-start" style="width: 38%;">
                                                        Stage Notes
                                                    </h3>
                                                    <ul class="note-btn-group">
                                                        <li>
                                                            <a href="javascript:;" id="add_attachments_model_open">
                                                                <i class="fad fa-link"></i>
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="javascript:;" id="free_drawing_open">
                                                                <i class="fad fa-pencil"></i>
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="javascript:;" id="add_video_open">
                                                                <i class="fad fa-video"></i>
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="javascript:;" id="add_recording_open">
                                                                <i class="fad fa-microphone"></i>
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="javascript:;" id="link_model_open">
                                                                <i class="fad fa-external-link-alt"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="stagenote-bottom-btn">
                                        <a type="submit" class="btn submit-btn me-2">Add notes</a>
                                    </div>
                                </div>





                            </div>





                            <div class="bottom-btn-group">
                                <div class="row w-100">
                                    <div class="col-lg-5">
                                        <ul>
                                            <li>
                                                <a href="{{route('admin.labcase')}}" class="btn back-btn red-btn">CLOSE SLIP</a>
                                            </li>
                                            <li>
                                                <a href="{{route('admin.labcase')}}" class="btn back-btn green-btn mb-1">NEW STAGE</a> <br>

                                                <a href="{{route('admin.labcase')}}" class="btn back-btn gray-btn">EDIT STAGE</a>

                                            </li>
                                            <li>
                                                <a href="{{route('admin.labcase')}}" class="btn back-btn gray-btn">LAST MODIFIED</a>
                                            </li>
                                            <li>
                                                <a href="{{route('admin.labcase')}}" class="btn back-btn green-btn mb-1">HISTORY CALL LOG</a> <br>

                                                <a href="{{route('admin.labcase')}}" class="btn back-btn gray-btn">+ ADD CALL LOG</a>
                                            </li>
                                            <li>
                                                <a href="{{route('admin.labcase')}}" class="btn back-btn add-btn">Ready to send</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-2">
                                        <ul class="justify-content-center">
                                            <li>
                                                <span>Driver history</span> <br>
                                                <i class="fal fa-truck"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-5">
                                        <ul>
                                            <li>
                                                <a href="{{route('admin.labcase')}}" class="btn back-btn add-btn mb-1">Pick up</a> <br>

                                                <a href="{{route('admin.labcase')}}" class="btn back-btn red-btn">Drop off</a>
                                            </li>
                                            <li>
                                                <a href="{{route('admin.labcase')}}" class="btn back-btn add-btn">Directions</a>
                                            </li>
                                            <li>
                                                <a href="{{route('admin.labcase')}}" class="btn back-btn add-btn mb-1">Submit slip</a> <br>
                                                <a href="{{route('admin.labcase')}}" class="btn back-btn add-btn">Update Slip</a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>







                    </div>


                    <!-- link-model start-->
                    <div class="modal fade" id="link_model" tabindex="-1">
                        <div class="modal-dialog modal-lg modal-dialog-centered">
                            <div class="modal-content" style="background: #f1f1f1">
                                <div class="link-model-wrapper all-icon-model">
                                    <div class="c-model-header">
                                        <h5>Inset video</h5>
                                        <a href="#" class="close-model">
                                            <i class="fal fa-times"></i>
                                        </a>
                                    </div>

                                    <div class="c-model-body p-3">
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto text-start">
                                                <label for="exampleInputEmail1" class="form-label model-label"> <strong> Video URl</strong> <span>(Youtube, vimeo ,Instagram,Dailymotion or youku)</span></label>
                                                <input type="text" class="form-control p-0" id="exampleInputEmail1" aria-describedby="emailHelp">
                                            </div>
                                            <div class="col-lg-11 mx-auto mt-1">
                                                <ul class="list-group">
                                                    <li class="list-group-item">A second item</li>
                                                    <li class="list-group-item">A third item</li>
                                                    <li class="list-group-item">A fourth item</li>
                                                    <li class="list-group-item">And a fifth one</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="c-model-footer py-3">
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto ">
                                                <div class="text-end">
                                                    <a href="#" class="model-btn">
                                                        Insert Video
                                                    </a>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- link-model end-->

                    <!-- add-attachments-model start-->
                    <div class="modal fade" id="add-attachments-model" tabindex="-1">
                        <div class="modal-dialog modal-lg modal-dialog-centered">
                            <div class="modal-content" style="background: #f1f1f1">
                                <div class="link-model-wrapper all-icon-model">
                                    <div class="c-model-header">
                                        <h5>Add attachments</h5>
                                        <a href="#" class="close-model">
                                            <i class="fal fa-times"></i>
                                        </a>
                                    </div>

                                    <div class="c-model-body p-3">
                                        <div class="row">

                                            <div class="col-md-11 mx-auto">
                                                <input type="hidden" name="iProductId" id="iProductId" value="">
                                                <div class="dropzone" id="my-dropzone" name="mainFileUploader">
                                                    <div class="fallback">
                                                        <input name="file" type="file" multiple />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="c-model-footer py-3">
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto ">
                                                <div class="text-end">

                                                    <a href="#" class="model-btn">
                                                        Insert Attachments
                                                    </a>
                                                    <a href="#" class="model-btn clear-sign">
                                                        Clear
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- add-attachments-model End-->

                    <!-- Free Drawing-model start-->
                    <div class="modal fade" id="free_drawing_model" tabindex="-1">
                        <div class="modal-dialog modal-lg modal-dialog-centered">
                            <div class="modal-content" style="background: #f1f1f1">
                                <div class="link-model-wrapper all-icon-model">
                                    <div class="c-model-header">
                                        <h5>Free Drawing</h5>
                                        <a href="#" class="close-model">
                                            <i class="fal fa-times"></i>
                                        </a>
                                    </div>

                                    <div class="c-model-body p-3">
                                        <div class="row">

                                            <div class="col-md-11 mx-auto">
                                                <canvas id="sig-canvas" width="500" height="200" style="background-color: #fff;width:100%;">
                                                    Get a better browser, bro.
                                                </canvas>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="c-model-footer py-3">
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto ">
                                                <div class="text-end">

                                                    <a href="#" class="model-btn">
                                                        Submit Signature
                                                    </a>
                                                    <a href="#" class="model-btn clear-sign" id="sig-clearBtn">
                                                        Clear Signature
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Free Drawing End-->

                    <!-- Add video model start-->
                    <div class="modal fade" id="add_video_model" tabindex="-1">
                        <div class="modal-dialog modal-lg modal-dialog-centered">
                            <div class="modal-content" style="background: #f1f1f1">
                                <div class="link-model-wrapper all-icon-model">
                                    <div class="c-model-header">
                                        <h5>Add video</h5>
                                        <a href="#" class="close-model">
                                            <i class="fal fa-times"></i>
                                        </a>
                                    </div>

                                    <div class="c-model-body p-3">
                                        <div class="row">

                                            <div class="col-md-11 mx-auto">
                                                <label for="formFile" class="form-label">chosen video</label>
                                                <input class="form-control" type="file" id="formFile">
                                            </div>

                                            <div class="col-lg-11 mx-auto mt-1">
                                                <ul class="list-group">
                                                    <li class="list-group-item">A second item</li>
                                                    <li class="list-group-item">A third item</li>
                                                    <li class="list-group-item">A fourth item</li>
                                                    <li class="list-group-item">And a fifth one</li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="c-model-footer py-3">
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto ">
                                                <div class="text-end">
                                                    <a href="#" class="model-btn">
                                                        Submit video
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Add video-model End-->

                    <!-- Add recording model start-->
                    <div class="modal fade" id="add_recording_model" tabindex="-1">
                        <div class="modal-dialog modal-lg modal-dialog-centered">
                            <div class="modal-content" style="background: #f1f1f1">
                                <div class="link-model-wrapper all-icon-model">
                                    <div class="c-model-header">
                                        <h5>chosen Recording</h5>
                                        <a href="#" class="close-model">
                                            <i class="fal fa-times"></i>
                                        </a>
                                    </div>

                                    <div class="c-model-body p-3">
                                        <div class="row">

                                            <div class="col-md-11 mx-auto">
                                                <input class="form-control" type="file" id="formFile">
                                            </div>

                                            <div class="col-lg-11 mx-auto mt-1">
                                                <ul class="list-group">
                                                    <li class="list-group-item">A second item</li>
                                                    <li class="list-group-item">A third item</li>
                                                    <li class="list-group-item">A fourth item</li>
                                                    <li class="list-group-item">And a fifth one</li>
                                                </ul>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="c-model-footer py-3">
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto ">
                                                <div class="text-end">
                                                    <a href="#" class="model-btn">
                                                        Submit Recording
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Add recording -model End-->



                </div>
                </form>

            </div>

        </div>
    </div>
</div>
</div>

@endsection
@section('custom-js')
<script>
    $(document).ready(function() {

        $('.open_model').trigger('click');

    });

    $(document).on('click', '#back', function() {
        window.location.href = " {{route('admin.labcase')}}";

    });




    // link model
    $(document).on('click', '#link_model_open', function() {
        $("#link_model").modal("show");

    });


    // add-attachments-model
    $(document).on('click', '#add_attachments_model_open', function() {
        $("#add-attachments-model").modal("show");

    });

    // free_drawing_model

    $(document).on('click', '#free_drawing_open', function() {
        $("#free_drawing_model").modal("show");

    });


    // add_video_model

    $(document).on('click', '#add_video_open', function() {
        $("#add_video_model").modal("show");

    });



    // add_recording_model

    $(document).on('click', '#add_recording_open', function() {
        $("#add_recording_model").modal("show");

    });






    Dropzone.autoDiscover = false;
    new Dropzone("#my-dropzone", {
        url: "{{url('admin/product/dropzoneStore')}}",
        parallelUploads: 10,
        uploadMultiple: true,
        autoProcessQueue: false,
        addRemoveLinks: true,
        init: function() {
            var submitButton = document.querySelector(".submit")
            myDropzone = this;
            submitButton.addEventListener("click", function() {
                myDropzone.processQueue();
            });
            myDropzone.on("addedfile", function(file) {
                if (!file.type.match(/image.*/)) {
                    if (file.type.match(/application.zip/)) {
                        myDropzone.emit("thumbnail", file, "path/to/img");
                    } else {
                        myDropzone.emit("thumbnail", file, "path/to/img");
                    }
                }
            });
            myDropzone.on("complete", function(file) {
                myDropzone.removeFile(file);
            });
        },
    });







    (function() {
        window.requestAnimFrame = (function(callback) {
            return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimaitonFrame ||
                function(callback) {
                    window.setTimeout(callback, 1000 / 60);
                };
        })();

        var canvas = document.getElementById("sig-canvas");
        var ctx = canvas.getContext("2d");
        ctx.strokeStyle = "#222222";
        ctx.lineWidth = 4;

        var drawing = false;
        var mousePos = {
            x: 0,
            y: 0
        };
        var lastPos = mousePos;

        canvas.addEventListener("mousedown", function(e) {
            drawing = true;
            lastPos = getMousePos(canvas, e);
        }, false);

        canvas.addEventListener("mouseup", function(e) {
            drawing = false;
        }, false);

        canvas.addEventListener("mousemove", function(e) {
            mousePos = getMousePos(canvas, e);
        }, false);

        // Add touch event support for mobile
        canvas.addEventListener("touchstart", function(e) {

        }, false);

        canvas.addEventListener("touchmove", function(e) {
            var touch = e.touches[0];
            var me = new MouseEvent("mousemove", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            canvas.dispatchEvent(me);
        }, false);

        canvas.addEventListener("touchstart", function(e) {
            mousePos = getTouchPos(canvas, e);
            var touch = e.touches[0];
            var me = new MouseEvent("mousedown", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            canvas.dispatchEvent(me);
        }, false);

        canvas.addEventListener("touchend", function(e) {
            var me = new MouseEvent("mouseup", {});
            canvas.dispatchEvent(me);
        }, false);

        function getMousePos(canvasDom, mouseEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: mouseEvent.clientX - rect.left,
                y: mouseEvent.clientY - rect.top
            }
        }

        function getTouchPos(canvasDom, touchEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: touchEvent.touches[0].clientX - rect.left,
                y: touchEvent.touches[0].clientY - rect.top
            }
        }

        function renderCanvas() {
            if (drawing) {
                ctx.moveTo(lastPos.x, lastPos.y);
                ctx.lineTo(mousePos.x, mousePos.y);
                ctx.stroke();
                lastPos = mousePos;
            }
        }

        // Prevent scrolling when touching the canvas
        document.body.addEventListener("touchstart", function(e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
        document.body.addEventListener("touchend", function(e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
        document.body.addEventListener("touchmove", function(e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);

        (function drawLoop() {
            requestAnimFrame(drawLoop);
            renderCanvas();
        })();

        function clearCanvas() {
            canvas.width = canvas.width;
        }

        // Set up the UI
        var sigText = document.getElementById("sig-dataUrl");
        var sigImage = document.getElementById("sig-image");
        var clearBtn = document.getElementById("sig-clearBtn");
        var submitBtn = document.getElementById("sig-submitBtn");
        clearBtn.addEventListener("click", function(e) {
            clearCanvas();
            sigText.innerHTML = "Data URL for your signature will go here!";
            sigImage.setAttribute("src", "");
        }, false);
        submitBtn.addEventListener("click", function(e) {
            var dataUrl = canvas.toDataURL();
            sigText.innerHTML = dataUrl;
            sigImage.setAttribute("src", dataUrl);
        }, false);

    })();
</script>
@endsection