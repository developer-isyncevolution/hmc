<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>

@php
	
	$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
    $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
    $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
    $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
    $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

    if( $iPod || $iPhone || $iPad){ 

    	if (stripos($_SERVER['HTTP_USER_AGENT'],"safari") && !stripos($_SERVER['HTTP_USER_AGENT'],"CriOS")) { @endphp

			<body onload="window.print();" style="color: #000;font-family:'Verdana';">
		
		@php }else{ @endphp
			<body onload="" style="color: #000;font-family:'Verdana';">
		@php }


    }else{ @endphp

		<body onload="window.print();" style="color: #000;font-family:'Verdana';">
    @php }

@endphp

	<style>
		@page {
			margin: 30px 15px;
		}

		@media print {

			/*@import url('https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap');*/

			@font-face {
				font-family: "Verdana";
				src: url("{{asset('admin/assets/fonts/verdana.ttf')}}");
			}

			/*@font-face {
				font-family: "comfortaa";
				src: url("{{asset('admin/assets/fonts/comfortaa.ttf')}}");
				font-weight: normal;
				font-style: normal;
			}*/

			body {
				width: 700px;
				max-width: 700px;
				margin: 10px 0px 0px 0px;
				padding: 0px;
				/*transform: rotate(90deg);*/
				/*text-transform: capitalize;*/
			}

			.font-size {
				font-size: 11px;
			}

			.inner-font-size {
				font-size: 6px;
			}

			.teeth-counter-text {
				font-size: 7px;
				font-weight: 700 !important;
			}


			.case-d-mid-font-size {
				font-size: 9px;
				font-weight: 800;
			}

			/*.comfota-font {
				font-family: 'Comfortaa';
			}*/

			.cen-section {
				position: absolute;
				top: 25%;
				left: 2%;
				font-size: 10px;
				height: auto;
				width: 18%;
				color: #000;
				border: 1px solid rgba(0, 0, 0, 0.527);
				box-shadow: 1px 1px 5px #0000001c;
				border-radius: 0px;
				text-transform: uppercase;
				margin: 0px;
				transform: rotate(-25deg);
				-webkit-border-radius: 0px;
				-moz-border-radius: 0px;
				-ms-border-radius: 0px;
				-o-border-radius: 0px;
				border-right: 0px;
				border-left: 0px;
				font-weight: normal;
				background-color: rgb(255, 255, 255);
				z-index: 10;
				/* backdrop-filter: blur(10px); */
				opacity: 0.8;
			}

			.cen-section.lower {
				left: 28%;			
				top: 20%;	
			}

			.cen-section.full {
				left: 0%;			
				top: 20%;
				width: 50%;	
				transform: rotate(0deg);
			}


			@media print {
				@page {
					size: landscape;
					margin:10px;
				}
			}
		}
	</style>

	<table style="max-width:500;width:500px;border-collapse: collapse;">
		<tr>
			<td colspan="3" style="text-align: center;">
				<span style="font-size: 14px;text-transform: capitalize;">@if(isset($CustomerLabData->vOfficeName) && !empty($CustomerLabData->vOfficeName)){{$CustomerLabData->vOfficeName}}@endif
				</span>
			</td>
		</tr>
	</table>

	<table style="max-width:500px;width:500px; margin: 0 auto 0px 0px;border-collapse:collapse;table-layout: fixed;">
		<tr>
			<td>
				<table style="width:100%;text-align: left;border-spacing: 0px;">
					<tr>
						<td style="font-size:7px;padding: 0px 0px 0px 5px;line-height: 11px;height:5px;vertical-align: bottom;" colspan="4">
							<span style="width:20px;display: inline-block;vertical-align: middle;">Ofc:</span>
							<span style="display: inline-block;font-size:11px;padding-left:0px;vertical-align: top;">  <td><span id="LabContent_lbcaseid">@if(isset($CustomerOfficeData->vOfficeName) && !empty($CustomerOfficeData->vOfficeName)){{ucfirst(strtolower($CustomerOfficeData->vOfficeName))}}@endif</span></td></span>
						</td>
					</tr>
				</table>
			</td>

		</tr>

		<tr>
			<td style="vertical-align:top;">



				<table style="text-align: left;table-layout: fixed;border-spacing: 0px;">
					<tr>
						<td style="width:40%;vertical-align:top;">
							<table style="text-align:left;table-layout: fixed;margin: 0px;padding: 0px;border: 1px soli;">
								<tr>
									<td style="font-size:7px;vertical-align:middle;line-height:11px;">
										<span style="width:20px;">Dr:</span>
									</td>

									<td style="vertical-align:top;line-height:11px;">
										<span style="font-size:11px;">{{ucwords(strtolower($Case->vFirstName." ".$Case->vLastName))}}</span>
									</td>
								</tr>

								<tr>
									<td style="font-size:7px;vertical-align: bottom;line-height:11px;">
										<span style="width:20px;display: inline-block;">Pt:</span>
									</td>
									<td style="vertical-align: top;line-height:11px;">
										<span style="font-size:11px;">{{ucwords(strtolower($Case->vPatientName))}}</span>
									</td>
								</tr>
								<tr>
									<td style="font-size:7px;vertical-align: middle;visibility: hidden;line-height:11px;">
										<span style="width:20px;display: inline-block;">Patient:</span>
									</td>
									<td style="vertical-align: top;visibility: hidden;line-height:11px;">
										<span style="font-size:11px;">{{ucwords(strtolower($Case->vPatientName))}}</span>
									</td>
								</tr>

							</table>

						</td>


						<td style="width:25%;vertical-align: top;">
							<table style="border-spacing: 0px;">
								<tr>
									<td style="font-size:7px;vertical-align: middle;line-height:11px;">
										<span style="width:35px;display: inline-block;">Pan #</span>
									</td>

									<td style="font-size:11px;vertical-align: top;line-height:11px;">
										<span style="padding:1px;border-radius:5px;">@if(!empty($Case->vCasePanNumber)){{$Case->vCasePanNumber}}@else----@endif</span>
									</td>
								</tr>

								<tr>

									<td style="font-size:7px;vertical-align: middle;line-height:11px;">
										<span style="width:35px;display: inline-block;">Case #</span>
									</td>
									<td style="line-height:11px;font-size:11px;vertical-align: top;">
										<span style="font-size:11px;">{{$Case->vCaseNumber}}</span>
									</td>
								</tr>

								<tr>
									<td style="font-size:7px;vertical-align: middle;line-height:11px;">
										<span style="width:35px;display: inline-block;">Slip #</span>
									</td>
									<td style="line-height:11px;vertical-align: top;">
										<span style="font-size:11px;">{{$Slip->vSlipNumber}}</span>
									</td>
								</tr>

							</table>
						</td>


						<td style="width:12%;vertical-align:top;">
							<table style="border-spacing: 0px;">
								<tr>
									<td style="font-size:7px;vertical-align: middle;line-height:11px;padding: 1px;">
										<span style="width:60px;display: inline-block;">Pick up date</span>
									</td>

									<td style="font-size:11px;vertical-align: top;line-height:11px;padding: 1px;">
										<span style="padding:1px;border-radius:5px;">
											@if(isset($final_Pickup_date)){{ date('m/d/Y', strtotime($final_Pickup_date)) }} @else
											@if (!empty($lower_product) AND empty($upper_product))
											{{date('m/d/Y', strtotime($lower_product->dPickUpDate))}}
											@elseif(!empty($upper_product) AND empty($lower_product))
											{{date('m/d/Y', strtotime($upper_product->dPickUpDate))}}
											@elseif(!empty($lower_product) && !empty($upper_product))
											@if($upper_product->dPickUpDate > $lower_product->dPickUpDate)
											{{date('m/d/Y', strtotime($upper_product->dPickUpDate))}}
											@elseif($upper_product->dPickUpDate < $lower_product->dPickUpDate)
												{{date('m/d/Y', strtotime($lower_product->dPickUpDate))}}
												@elseif($upper_product->dPickUpDate == $lower_product->dPickUpDate)
												{{date('m/d/Y', strtotime($upper_product->dPickUpDate))}}
												@endif
												@endif
												@endif
										</span>
									</td>
								</tr>

								<tr>
									<td style="font-size:7px;vertical-align: middle;line-height:11px;">
										<span style="width:60px;display: inline-block;">Delivery date</span>
									</td>
									<td style="line-height:11px;font-size:11px;vertical-align: top;">
										<span style="font-size:11px;">  

											@if(isset($upper_product->dDeliveryDateFinal) && !isset($lower_product->dDeliveryDateFinal))

		                                        {{ date('m/d/Y', strtotime($upper_product->dDeliveryDateFinal)) }} 

		                                    @elseif(isset($lower_product->dDeliveryDateFinal) && !isset($upper_product->dDeliveryDateFinal))

		                                        {{ date('m/d/Y', strtotime($lower_product->dDeliveryDateFinal)) }} 

		                                    @elseif((isset($final_delivery_date) && (isset($is_single_product) && $is_single_product =='yes')) )
												
												{{ date('m/d/Y', strtotime($final_delivery_date)) }}
											
											@else 
												{{"-----"}} 
											@endif</span>
									</td>
								</tr>

								<tr>
									<td style="font-size:7px;vertical-align: middle;line-height:11px;">
										<span style="width:60px;display: inline-block;">Delivery time</span>
									</td>
									<td style="line-height:11px;vertical-align: top;">
										<span style="font-size:11px;">  @if((isset($final_delivery_time) && (isset($is_single_product) && $is_single_product =='yes'))  )
											{{ date("h:i",strtotime($final_delivery_time))}}
										@else
											{{"-----"}}
										@endif</span>
									</td>
								</tr>

							</table>







						</td>
					</tr>

				</table>
			</td>

		</tr>

	</table>
	@php
	$hide_product = 'no';    
	@endphp
	@if(isset($Case->eStatus) && $Case->eStatus == "Canceled")
	@php
	$hide_product = 'yes';    
	@endphp
	<div class="cen-section full" style="transform:rotate(0deg)!important;width: 50%;top: 25%;text-align: center;">
		<p class="m-0 py-3">
			This slip was canceled 
		</p>
	</div>
	@elseif(isset($Case->eStatus) && $Case->eStatus == "On Hold")
	@php
	$hide_product = 'yes';    
	@endphp
	<div class="cen-section full" style="transform:rotate(0deg)!important;width: 50%;top: 27%;text-align: center;">
		<p class="m-0 py-3">
			This slip is on hold 
		</p>
	</div>
	@endif

	<table style="max-width:500px;width: 500px; margin: 0 auto 0 0;border-collapse: collapse;border-top: 1px solid #000;table-layout: fixed;">
		<tr>
			<td>
				<table style="max-width:500px;width: 500px; margin: 0 auto 0 0;border-collapse: collapse;table-layout: fixed;">
					<tr>
						<td style="vertical-align: top;text-align: center;padding:0px;width: 37.5%;">
							<span style="font-size:12px;">Maxillary</span>
						</td>
						<td colspan="2" style="vertical-align: top;text-align: center;padding: 0px;width: 25%;">
							<span style="font-size: 12px;">CASE DESIGN</span>
						</td>
						<td style="vertical-align: top;text-align: center;padding:0px;width: 37.5%;">
							<span style="font-size:12px;">Mandibular</span>
						</td>
					</tr>
				</table>
			</td>
		</tr>


		<td style="vertical-align:top;text-align: center;">
			
			 		
			<!-- table 2-2 -->
			<table style="width:500px;text-align: center;width: 500px;table-layout: fixed;position: relative;">
				@if((isset($upper_product->eStatus) && $upper_product->eStatus == "Canceled")  && $hide_product == 'no')
                    <div class="cen-section upper">
                        <p style="margin: 0px;">
                        This stage was canceled
                        </p>
                    </div>
                @elseif((isset($upper_product->eStatus) && $upper_product->eStatus == "On Hold")  && $hide_product == 'no')
                    <div class="cen-section upper">
                        <p style="margin: 0px;">
                        This stage is on hold
                        </p>
                    </div>
                @endif

				@if(!empty($upper_product->vProductName) || !empty($lower_product->vProductName))
				<tr>
					<td colspan="2" style="text-align:right;line-height:8px;padding:3px;margin: 0px;" class="font-size">
						@if(!empty($upper_product->vProductName))
						<span class="font-size">{{ucfirst(strtolower($upper_product->vProductName))}}</span>
						@endif
					</td>
					<td style="text-align:center;line-height:8px;padding:0px;margin: 0px;">
						<span style="display: inline-block;padding: 3px;" class="case-d-mid-font-size">PRODUCT</span>
					</td>
					<td colspan="2" style="text-align:left;line-height:8px;padding:3px;margin: 0px;" class="font-size">
						@if(!empty($lower_product->vProductName))
						<span> {{ucfirst(strtolower($lower_product->vProductName))}}</span>
						@endif
					</td>
				</tr>
				@endif
				@if(!empty($upper_product->vGradeName) || !empty($lower_product->vGradeName))
				<tr>
					<td colspan="2" style=" text-align:right;line-height:8px;padding:3px;margin: 0px;" class="font-size">
						@if(!empty($upper_product->vGradeName))
						<span class="font-size">{{ucfirst(strtolower($upper_product->vGradeName))}}</span>
						@endif
					</td>
					<td style=" text-align:center;line-height:8px;padding:3px;margin: 0px;"><span class="case-d-mid-font-size">GRADE</span>
					</td>
					<td colspan="2" style="text-align:left;line-height:8px;padding:3px;margin: 0px;">
						@if(!empty($lower_product->vGradeName))
						<span class="font-size"> {{ucfirst(strtolower($lower_product->vGradeName))}}</span>
						@endif
					</td>
				</tr>
				@endif
				@if(!empty($upper_product->vStageName) || !empty($lower_product->vStageName))
				<tr>
					<td colspan="2" style=" text-align:right;line-height:8px;padding:3px;margin: 0px;">
						@if(!empty($upper_product->vStageName))
						<span class="font-size">
							{{ucfirst(strtolower($upper_product->vStageName))}}
						</span>
						@endif
					</td>
					<td style=" text-align:center;line-height:8px;padding:3px;margin: 0px;"><span class="case-d-mid-font-size">STAGE</span>
					</td>
					<td colspan="2" style=" text-align:left;line-height:8px;padding:3px;margin: 0px;">
						@if(!empty($lower_product->vStageName))
						<span class="font-size">{{ucfirst(strtolower($lower_product->vStageName))}}</span>
						@endif
					</td>
				</tr>
				@endif
				@if(!empty($upper_product->vBrandName) || !empty($lower_product->vBrandName))
				<tr style="padding:0px;margin:0px;">
					<!-- TEETH SHADE -->
					<td style=" text-align:right;line-height:9px;padding:3px;margin: 0px;" colspan="2" class="font-size">
						@if(!empty($upper_product->vBrandName))
						<span class="font-size">{{ucfirst(strtolower($upper_product->vBrandName))}}-{{ucfirst(strtolower($upper_product->vCode))}}</span>
						@endif
					</td>
					<td style=" text-align:center;line-height:9px;padding:3px;margin: 0px;">
						<span class="case-d-mid-font-size">TEETH SHADE</span>
					</td>

					<td style=" text-align:left;line-height:9px;padding:3px;margin: 0px;" colspan="2">
						@if(!empty($lower_product->vBrandName))
						<span class="font-size">{{ucfirst(strtolower($lower_product->vBrandName))}} - {{ucfirst(strtolower($lower_product->vCode))}}</span>
						@endif
					</td>
				</tr>
				@endif
				@if(!empty($upper_product->vGumBrand) || !empty($lower_product->vGumBrand))
				<tr style="padding:0px;margin:0px;">
					<!--   GUM SHADE -->
					<td colspan="2" style="text-align:right;line-height:9px;padding:3px;margin: 0px;vertical-align: top;">
						@if(!empty($upper_product->vGumBrand))
						<span style="display: inline-block;" class="font-size"> {{ucfirst(strtolower($upper_product->vGumBrand))}} - {{ucfirst(strtolower($upper_product->vGumShade))}}</span>
						@endif
					</td>
					<td style=" text-align:center;line-height:9px;padding:3px;margin: 0px;vertical-align: top;"><span class="case-d-mid-font-size">GUM SHADE</span></td>

					<td colspan="2" style="text-align:left;line-height:9px;padding:3px;margin: 0px;vertical-align: top;">
						@if(!empty($lower_product->vGumBrand))
						<span class="font-size"> {{ucfirst(strtolower($lower_product->vGumBrand))}} - {{ucfirst(strtolower($lower_product->vGumShade))}}</span>
						@endif
					</td>
				</tr>
				@endif
				@if(!empty($upper_impression) && sizeof($upper_impression) > 0 OR !empty($lower_impression) && sizeof($lower_impression) > 0)
				<tr style="padding:0px;margin:0px;">
					<td colspan="2" style="vertical-align: top;line-height:9px;padding:0px;margin:0px;">
						<table style="width:100%;padding:0px;margin: 0px;border-spacing: 0px;">
							@if(empty($upper_impression))
							@php
							$upper_impression = $upper_impressionOp
							@endphp
							@endif

							@foreach ($upper_impression as $value_impression)
							<tr style="vertical-align: top;padding:3px;margin:0px;">
								<td class="p-0" style="text-align: right;line-height:9px;padding:3px;margin:0px;height: 100%;">
									<div style="display:flex;justify-content: flex-end;padding:0px;margin: 0px;">
										<span style="display: inline-block;" class="font-size">{{ucfirst(strtolower($value_impression->vImpressionName))}}</span>
										<span style="border-radius:5px;padding:1px;display:inline-block;margin-left:5px;" class="font-size">Qty {{$value_impression->iQuantity}}</span>
									</div>
								</td>
							</tr>
							@endforeach
						</table>
					</td>

					<td style="vertical-align: top; text-align:center;line-height:9px;padding:0px;margin:0px;">
						<span class="case-d-mid-font-size">IMPRESSIONS</span>
					</td>

					<td colspan="2" style="vertical-align: top;line-height:9px;padding:0px;margin: 0px;text-align:left;" class="font-size">
						<table style="width:100%;padding:0px;margin: 0px;border-spacing: 0px;">
							@if(empty($lower_impression))
							@php
							$lower_impression = $lower_impressionOp
							@endphp
							@endif
							@foreach ($lower_impression as $lower_impression)
							<tr style="vertical-align: top;padding:3px;margin:0px;">
								<td class="p-0" style="text-align:left;line-height:9px;padding:3px;margin: 0px;" class="font-size">
									<div style="display:flex;justify-content:flex-start;padding:0px;margin: 0px;">
										<span style="display: inline-block;padding:1px;margin-right:9px;" class="font-size">Qty {{ucfirst(strtolower($lower_impression->iQuantity))}}</span>
										<span style="border-radius:5px;padding:1px;display:inline-block;" class="font-size">{{ucfirst(strtolower($lower_impression->vImpressionName))}}</span>
									</div>
								</td>
							</tr>
							@endforeach
						</table>
					</td>
				</tr>
				@endif

				@if(isset($upper_addons) OR isset($lower_addons))

				@if(!isset($upper_addons))
				@php $upper_addons = array(); @endphp
				@endif

				@if(!isset($lower_addons))
				@php $lower_addons = array(); @endphp
				@endif

				@endif

				@if((!empty($upper_addons) && sizeof($upper_addons) > 0) OR (!empty($lower_addons) && sizeof($lower_addons) > 0))
				<tr>
					<td colspan="2" style="vertical-align: top; text-align:right;line-height:9px;padding:0px;margin:0px;">
						<table style="width:100%;border-spacing:0px;">
							@foreach ($upper_addons as $value)
							<tr style="vertical-align: top;padding:3px;margin:0px;">
								<td class="p-0" style="text-align: right;line-height:9px;padding:3px;margin:0px;">
									<div style="display:flex;justify-content: flex-end;padding:0px;margin: 0px;">
										<span style="padding:1px;display:inline-block;" class="font-size">{{ucfirst(strtolower($value->vAddonCategoryName))}}, {{ucfirst(strtolower($value->vAddonName))}}</span>
										<span style="border-radius:5px;padding:1px;display:inline-block;margin-left:5px;" class="font-size">Qty {{$value->iQuantity}}</span>
									</div>
								</td>
							</tr>
							@endforeach
						</table>
					</td>

					<td style="vertical-align:top;text-align:center;line-height:9px;padding:0px;margin:0px;">
						<span class="case-d-mid-font-size">ADD ONS</span>
					</td>

					<td colspan="2" style="vertical-align: top;line-height:9px;padding:0px;margin: 0px;text-align:left;" class="font-size">
						<table style="width:100%;padding:0px;margin: 0px;border-spacing: 0px;">
							@foreach ($lower_addons as $value)
							<tr style="vertical-align: top;padding:3px;margin:0px;">
								<td class="p-0" style="text-align:left;line-height:9px;padding:3px;margin: 0px;vertical-align: top" class="font-size">
									<div style="display:flex;justify-content:flex-start;padding:0px;margin: 0px;">
										<span style="display: inline-block;padding:1px;margin-right:9px;" class="font-size">Qty {{$value->iQuantity}}</span>
										<span style="border-radius:5px;padding:1px;display:inline-block;" class="font-size">{{ucfirst(strtolower($value->vAddonCategoryName))}},{{ucfirst(strtolower($value->vAddonName))}}</span>
									</div>
								</td>
							</tr>
							@endforeach
						</table>
					</td>
				</tr>
				@endif
				{{-- @if($rush == "Yes") --}}
				@if((isset($rush_product) && $rush_product =='yes') && (isset($is_single_product) && $is_single_product =='no') )
				<tr>
					<!--  DELIVERY DATE-->
					@if((isset($upper_rush_product) && $upper_rush_product =='yes') )
					<td colspan="2" style="text-align:right;line-height:9px;padding:3px;margin: 0px;"><span class="font-size">{{date('m/d/Y', strtotime($final_delivery_date))}} </span></td>
					@elseif(isset($final_delivery_date_upper) && !empty($final_delivery_date_upper)
					)
					<td colspan="2" style="text-align:right;line-height:9px;padding:3px;margin: 0px;"><span class="font-size">{{date('m/d/Y', strtotime($final_delivery_date_upper))}} </span></td>
					@elseif(isset($upper_product->dDeliveryDate) && $upper_product->dDeliveryDate !='')
					<td colspan="2" style="text-align:right;line-height:9px;padding:3px;margin: 0px;"><span class="font-size">{{date('m/d/Y', strtotime($upper_product->dDeliveryDate))}} </span></td>
					@else
					<td colspan="2"></td>
					@endif
					<td style="vertical-align: top; text-align:center;line-height:9px;padding:3px;margin: 0px;">
						<span style="display: inline-block;" class="case-d-mid-font-size">DELIVERY DATE</span>
					</td>
					@if((isset($lower_rush_product) && $lower_rush_product =='yes') )
					<td colspan="2" style="text-align:left;line-height:9px;padding:3px;margin: 0px;"><span class="font-size">{{date('m/d/Y', strtotime($final_delivery_date))}} </span></td>
					@elseif(isset($final_delivery_date_lower) && !empty($final_delivery_date_lower)
					)
					<td colspan="2" style="text-align:left;line-height:9px;padding:3px;margin: 0px;"><span class="font-size">{{date('m/d/Y', strtotime($final_delivery_date_lower))}} </span></td>
					@elseif(isset($lower_product->dDeliveryDate) && $lower_product->dDeliveryDate !='')
					<td colspan="2" style="text-align:left;line-height:9px;padding:3px;margin: 0px;"><span class="font-size">{{date('m/d/Y', strtotime($lower_product->dDeliveryDate))}} </span></td>
					@else
					<td colspan="2"></td>
					@endif

				</tr>
				<tr>
					<!--  DELIVERY DATE-->
					@if(isset($upper_product->dDeliveryDate) && $upper_product->dDeliveryDate !='')
					<td colspan="2" style="text-align:right;line-height:9px;padding:3px;margin: 0px;"><span class="font-size">{{date("h:i A",strtotime($final_delivery_time))}}</span></td>
					@else
					<td colspan="2"></td>
					@endif
					<td style="vertical-align: top; text-align:center;line-height:9px;padding:3px;margin: 0px;">
						<span class="case-d-mid-font-size">DELIVERY TIME</span>
					</td>
					@if(isset($lower_product->dDeliveryDate) && $lower_product->dDeliveryDate)
					<td colspan="2" style="text-align:left;line-height:9px;padding:3px;margin: 0px;">
						<span class="font-size">{{date("h:i A",strtotime($final_delivery_time))}} </span>
					</td>
					@endif
				</tr>
				@endif


			</table>
		</td>
		</tr>

	</table>

	<table style="max-width:500px;width:500px; margin: 0 auto 0 0;border-collapse: collapse;">
		<tr>
			<td style="vertical-align: top;">
				<table style="padding: 0px;width:500px;max-width: 500px;text-align: center;table-layout: fixed;">
					<tr>
						<td style="vertical-align:top;width: 40%;max-width:40%;">
							<table style="width: 100%;">
								<tr>
									<td style="text-align: right;margin-left: auto;width: 100%;">
										<div style="margin: 0px 0px 0px auto;height:auto;width:100%;text-align: right;">
											<svg width="100%" height="100%" viewBox="0 0 1500 822" class="teeth-svg" style="margin-left: auto;width: 100%;">

												<g>

													<g>
														<!-- upper-teeth 6 -->
														<a href="javascript:;">
															<image x="403" y="123" width="136" height="178" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-6.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>
													</g>

													<g>
														<!-- upper-teeth 5 -->
														<a href="javascript:;">
															<image x="325" y="202" width="149" height="152" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-5.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>
													</g>
													<g>
														<!-- upper-teeth 4 -->
														<a href="javascript:;">
															<image x="266" y="285" width="153" height="149" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-4.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>
													</g>
													<g>
														<!-- upper-teeth 3 -->
														<a href="javascript:;">
															<image x="176" y="367" width="220" height="199" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-3.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>
													</g>
													<g>
														<!-- upper-teeth 2 -->
														<a href="javascript:;">
															<image x="115" y="500" width="203" height="188" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-2.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>
													</g>

													<g>
														<!-- upper-teeth 1 -->
														<a href="javascript:;">
															<image x="57" y="640" width="192" height="156" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-1.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>
														<!-- upper-teeth 1 end-->
													</g>



													<g>
														<!-- upper-teeth 7 -->
														<a href="javascript:;">
															<image x="498" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-7.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>
													</g>

													<g>
														<!-- upper-teeth 8 -->
														<a href="javascript:;">
															<image x="607" y="35" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-8.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>

													</g>

													<g>
														<!-- upper-teeth 9 -->
														<a href="javascript:;">
															<image x="755" y="35" width="150" height="210" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-9.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>
													</g>

													<g>
														<!-- upper-teeth 10 -->
														<a href="javascript:;">
															<image x="889" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-10.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>
													</g>

													<g>
														<!-- upper-teeth 11 -->
														<a href="javascript:;">
															<image x="972" y="123" width="137" height="178" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-11.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>

													</g>

													<g>
														<!-- upper-teeth 12 -->
														<a href="javascript:;">
															<image x="1040" y="198" width="149" height="152" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-12.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>

													</g>

													<g>
														<!-- upper-teeth 13 -->
														<a href="javascript:;">
															<image x="1095" y="284" width="154" height="149" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-13.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>

													</g>

													<g>
														<!-- upper-teeth 14 -->
														<a href="javascript:;">
															<image x="1115" y="361" width="221" height="199" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-14.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>

													</g>

													<g>
														<!-- upper-teeth 15 -->
														<a href="javascript:;">
															<image x="1194" y="503" width="203" height="188" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-15.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>

													</g>

													<g>
														<!-- upper-teeth 16 -->
														<a href="javascript:;">
															<image x="1262" y="643" width="192" height="156" xlink:href="{{asset('admin/assets/images/paper2-teeth/up-16.png')}}" class="img-for-teeth teeth_upper"></image>
														</a>
													</g>
												</g>
											</svg>
										</div>
									</td>
								</tr>

								@if((isset($upper_vTeethInMouth) AND isset($upper_vMissingTeeth) AND isset($upper_vWillExtractOnDelivery) AND isset($upper_vHasBeenExtracted) AND isset($upper_vFixOrAdd)) OR (isset($upper_vTeethInMouthOp) AND isset($upper_vMissingTeethOp) AND isset($upper_vWillExtractOnDeliveryOp) AND isset($upper_vHasBeenExtractedOp) AND isset($upper_vFixOrAddOp)))

								@if((isset($upper_vTeethInMouth) AND isset($upper_vMissingTeeth) AND isset($upper_vWillExtractOnDelivery) AND isset($upper_vHasBeenExtracted) AND isset($upper_vFixOrAdd)))
								@else
								@php
								$upper_vTeethInMouth = $upper_vTeethInMouthOp;
								$upper_vMissingTeeth = $upper_vMissingTeethOp;
								$upper_vWillExtractOnDelivery = $upper_vWillExtractOnDeliveryOp;
								$upper_vHasBeenExtracted = $upper_vHasBeenExtractedOp;
								$upper_vFixOrAdd = $upper_vFixOrAddOp;
								$upper_vClasps = $upper_vClaspsOp;

								@endphp
								@endif
								@endif


								@if(!empty($upper_vTeethInMouth))
								@php
								$parts=array_filter($upper_vTeethInMouth);
								$upper_vTeethInMouth = (implode(",",$parts));
								@endphp
								@endif

								@if(!empty($upper_vTeethInMouth))
								<tr>
									<td style="text-align: center;">
										<div style="border-radius:5px;padding:1px;border: 1px solid #000;">
											<div style="margin-bottom:1px;" class="inner-font-size"> TEETH IN MOUTH</div>
											<div class="teeth-counter-text">{{$upper_vTeethInMouth}}</div>
										</div>
									</td>
								</tr>
								@endif
								@if (!empty(array_filter($upper_vMissingTeeth)))
								@php
								$parts=array_filter($upper_vMissingTeeth);
								$upper_vMissingTeeth = (implode(",",$parts));
								@endphp
								<tr>
									<td style="text-align: center;border: 1px solid #000; border-radius:5px;">
										<div style="padding:1px;">
											<div style="margin-bottom: 1px;" class="inner-font-size"> MISSING TEETH</div>
											<div class="teeth-counter-text">{{$upper_vMissingTeeth}}</div>
										</div>
									</td>
								</tr>
								@elseif(isset($upper_vMissingTeethOp))
								@if (!empty(array_filter($upper_vMissingTeethOp)))
								<tr>
									<td style="text-align: center;border: 1px solid #000;border-radius:5px">
										<div style="width:100%;padding:1px;">
											<div style="margin-bottom:1px;" class="inner-font-size"> MISSING TEETH</div>
											<div class="teeth-counter-text">{{$upper_product->vMissingTeethOp}}</div>
										</div>
									</td>
								</tr>
								@endif
								@endif
								@if (!empty(array_filter($upper_vWillExtractOnDelivery)))
								@php
								$parts=array_filter($upper_vWillExtractOnDelivery);
								$upper_vWillExtractOnDelivery = (implode(",",$parts));
								@endphp
								<tr>
									<td style=" text-align: center;border-radius:5px;border: 1px solid #000;">
										<div style="width:100%; padding:1px;">
											<div style="margin-bottom:1px;" class="inner-font-size"> WILL EXTRACT ON DELIVERY </div>
											<div class="teeth-counter-text">{{$upper_vWillExtractOnDelivery}}</div>
										</div>
									</td>
								</tr>
								@elseif(isset($upper_vWillExtractOnDeliveryOp))
								@if (!empty(array_filter($upper_vWillExtractOnDeliveryOp)))
								<tr>
									<td style=" text-align: center;border-radius:5px;border: 1px solid #000;">
										<div style="width:100%; padding:1px;">
											<div style="margin-bottom:1px;" class="inner-font-size"> WILL EXTRACT ON DELIVERY </div>
											<div class="teeth-counter-text">{{$upper_vWillExtractOnDeliveryOp}}</div>
										</div>
									</td>
								</tr>
								@endif
								@endif
								@if (!empty(array_filter($upper_vHasBeenExtracted)))
								@php
								$parts=array_filter($upper_vHasBeenExtracted);
								$upper_vHasBeenExtracted = (implode(",",$parts));
								@endphp
								<tr>
									<td style="text-align: center;border-radius:5px;border: 1px solid #000;">
										<div style="width:100%;padding:1px;">
											<div style="margin-bottom:1px;" class="inner-font-size"> HAS BEEN EXTRACTED</div>
											<div class="teeth-counter-text">{{$upper_vHasBeenExtracted}}</div>
										</div>
									</td>
								</tr>
								@elseif(isset($upper_vHasBeenExtractedOp))
								@if (!empty(array_filter($upper_vHasBeenExtractedOp)))
								<tr>
									<td style=" text-align: center; border-radius:5px;border: 1px solid #000;">
										<div style="width:100%;padding:1px;">
											<div style="margin-bottom:1px;" class="inner-font-size"> HAS BEEN EXTRACTED</div>
											<div class="teeth-counter-text">{{$upper_vHasBeenExtractedOp}}</div>
										</div>
									</td>
								</tr>
								@endif
								@endif
								<tr>
									<td>
										<table style="margin: 0px;padding: 0px;width: 100%;border-spacing:0px 0px;">
											<tr>
												@if (!empty(array_filter($upper_vFixOrAdd)))
												@php
												$parts=array_filter($upper_vFixOrAdd);
												$upper_vFixOrAdd = (implode(",",$parts));
												@endphp
												<td style="margin-left: 10px;border: 1px solid #000;border-radius:5px;width:50%;">
													<div style="padding:1px;text-align: center;">
														<div style="margin-bottom:1px;" class="inner-font-size"> FIX OR ADD</div>
														<div class="teeth-counter-text"> {{$upper_vFixOrAdd}}</div>
														</div>
												</td>
												@elseif(isset($upper_vFixOrAddOp))
												@if (!empty(array_filter($upper_vFixOrAddOp)))
												<td style="margin-right: 10px;border-radius:5px;border: 1px solid #000;width:50%;">
													<div style="padding:1px;text-align: center;">
														<div style="margin-bottom:1px;" class="inner-font-size"> FIX OR ADD</div>
														<div class="teeth-counter-text">{{$upper_vFixOrAddOp}}</div>
													</div>
												</td>
												@endif
												@endif

												@if (!empty(array_filter($upper_vClasps)))
												@php
												$parts=array_filter($upper_vClasps);
												$upper_vClasps = (implode(",",$parts));
												@endphp
												<td style="border-radius:5px;border: 1px solid #000;width:50%;">
													<div style="padding:1px;text-align: center;">
														<div style="margin-bottom: 1px;" class="inner-font-size">CLASPS</div>
														<div class="teeth-counter-text">{{$upper_vClasps}}</div>
													</div>
												</td>
												@elseif(isset($upper_vClaspsOp))
												@if (!empty(array_filter($upper_vClaspsOp)))
												<td style="border: 1px solid #000;border-radius:5px;width:50%;">
													<div style="padding:1px;text-align: center;">
														<div style="margin-bottom: 1px;" class="inner-font-size">CLASPS</div>
														<div class="teeth-counter-text">{{$upper_vClaspsOp}}</div>
													</div>
												</td>
												@endif
												@endif
											</tr>
										</table>
									</td>
								</tr>

							</table>
						</td>

						<td style="padding:0px;vertical-align: top;text-align: center;width: 20%;">
							 @php $qr_code = route('admin.labcase.virtual_slip',[$Slip->iCaseId,$Slip->iSlipId]); @endphp
							<a style="text-align:left;display:inline-block;padding: 0px;" target="_blank" href="https://chart.googleapis.com/chart?chs=500x500&cht=qr&choe=UTF-8&chl={{$qr_code}}">
								<img src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&choe=UTF-8&chl={{$qr_code}}" style="height:80px;width:80px;display:inline-block;margin:0px auto 0px auto;padding: 0px;" download>
							</a>
							<span style="text-align: center;font-size:15px;">V-Slip</span>
						</td>

						<td style=" vertical-align: top;width:40%;max-width: 40%;">
							@if((isset($lower_product->eStatus) && $lower_product->eStatus == "Canceled") && $hide_product == 'no')
                                <div class="cen-section lower">
                                    <p style="margin: 0px;">
                                    This stage was canceled
                                    </p>
                                </div>
                            @elseif((isset($lower_product->eStatus) && $lower_product->eStatus == "On Holds") && $hide_product == 'no')
                                <div class="cen-section lower">
                                    <p style="margin: 0px;">
                                    This stage is on hold
                                    </p>
                                </div>
                            @endif
							<!-- table 2-3 -->
							<table style="vertical-align: top;width: 100%;">
								<!-- <tr>
											<td style="vertical-align: top;text-align: center;font-size:15px;">
												<span> MANDIBULAR</span>
											</td>
										</tr> -->
								<tr>
									<td>
										<div style="margin: 0px auto 0px 0px;height:auto;width:100%;">
											<svg width="100%" height="100%" viewBox="0 0 1500 822" class="teeth-svg">
												<g>
													<g>
														<image id="lower-23-teeth-white" data-id="teeth2" data-name="23" x="540" y="60" width="115" height="186" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-23.png')}}" class="img-for-teeth teeth_lower lower-23-class-teeth-white"></image>
													</g>

													<g>
														<image id="lower-22-teeth-white" data-id="teeth4" data-name="22" x="435" y="93" width="135" height="200" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-22.png')}}" class="img-for-teeth teeth_lower lower-22-class-teeth-white"></image>
													</g>

													<g>
														<image id="lower-21-teeth-white" data-id="teeth3" data-name="21" x="342" y="153" width="150" height="177" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-21.png')}}" class="img-for-teeth teeth_lower lower-21-class-teeth-white"></image>
													</g>
													<g>
														<image id="lower-20-teeth-white" data-id="teeth5" data-name="20" x="265" y="225" width="167" height="159" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-20.png')}}" class="img-for-teeth teeth_lower"></image>
													</g>
													<g>
														<image x="165" y="290" width="235" height="191" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-19.png')}}" class="img-for-teeth teeth_lower"></image>
													</g>

													<g>
														<image x="95" y="400" width="232" height="194" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-18.png')}}" class="img-for-teeth teeth_lower"></image>
													</g>

													<g>
														<image x="30" y="555" width="252" height="200" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-17.png')}}" class="img-for-teeth  teeth_lower"></image>
													</g>

													<g>
														<image x="647" y="40" width="111" height="182" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-24.png')}}" class="img-for-teeth teeth_lower"></image>

													</g>

													<g>
														<image x="760" y="42" width="111" height="182" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-25.png')}}" class="img-for-teeth teeth_lower"></image>
													</g>

													<g>
														<image x="865" y="54" width="115" height="186" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-26.png')}}" class="img-for-teeth teeth_lower"></image>
													</g>

													<g>
														<image x="955" y="80" width="135" height="200" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-27.png')}}" class="img-for-teeth teeth_lower"></image>
													</g>

													<g>
														<image x="1035" y="134" width="150" height="177" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-28.png')}}" class="img-for-teeth teeth_lower"></image>
													</g>

													<g>
														<image x="1098" y="205" width="167" height="159" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-29.png')}}" class="img-for-teeth teeth_lower"></image>
													</g>

													<g>
														<image x="1130" y="278" width="234" height="191" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-30.png')}}" class="img-for-teeth teeth_lower"></image>
													</g>
													<g>
														<image id="lower-31-teeth-white" data-id="teeth14" data-name="31" x="1210" y="395" width="232" height="194" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-31.png')}}" class="img-for-teeth teeth_lower"></image>
													</g>

													<g>
														<image x="1273" y="560" width="232" height="193" xlink:href="{{asset('admin/assets/images/paper2-teeth/d-32.png')}}" class="img-for-teeth teeth_lower"></image>
													</g>

													<!--lower teeth numbers ------------------------------ -->

												</g>

											</svg>
										</div>
									</td>
								</tr>

								@if((isset($lower_vTeethInMouth) AND isset($lower_vMissingTeeth) AND isset($lower_vWillExtractOnDelivery) AND isset($lower_vHasBeenExtracted) AND isset($lower_vFixOrAdd)) OR (isset($lower_vTeethInMouthOp) AND isset($lower_vMissingTeethOp) AND isset($lower_vWillExtractOnDeliveryOp) AND isset($lower_vHasBeenExtractedOp) AND isset($lower_vFixOrAddOp)))

								@if((isset($lower_vTeethInMouth) AND isset($lower_vMissingTeeth) AND isset($lower_vWillExtractOnDelivery) AND isset($lower_vHasBeenExtracted) AND isset($lower_vFixOrAdd)))
								@else
								@php
								$lower_vTeethInMouth = $lower_vTeethInMouthOp;
								$lower_vMissingTeeth = $lower_vMissingTeethOp;
								$lower_vWillExtractOnDelivery = $lower_vWillExtractOnDeliveryOp;
								$lower_vHasBeenExtracted = $lower_vHasBeenExtractedOp;
								$lower_vFixOrAdd = $lower_vFixOrAddOp;
								$lower_vClasps = $lower_vClaspsOp;

								@endphp
								@endif
								@endif


								@if(!empty($lower_vTeethInMouth))
								@php
								$parts=array_filter($lower_vTeethInMouth);
								$lower_vTeethInMouth = (implode(",",$parts));
								@endphp
								@endif

								@if(!empty($lower_vTeethInMouth))
								<tr>
									<td style="text-align: center;border-radius:5px;border: 1px solid #000;">
										<div style="width:100%;padding:1px;">
											<div style="margin-bottom:1px;" class="inner-font-size"> TEETH IN MOUTH</div>
											<div class="teeth-counter-text">{{$lower_vTeethInMouth}}</div>
										</div>
									</td>
								</tr>
								@elseif(isset($lower_vTeethInMouth))
								@if (isset($upper_vMissingTeethOp) && !empty(array_filter($upper_vMissingTeethOp)))
								<tr>
									<td style="text-align: center;border-radius:5px;border: 1px solid #000;">
										<div style="width:100%;padding:1px;">
											<div style="margin-bottom:1px;" class="inner-font-size"> TEETH IN MOUTH</div>
											<div class="teeth-counter-text">{{isset($lower_vTeethInMouthOp)?$lower_vTeethInMouthOp:''}}</div>
										</div>
									</td>
								</tr>
								@endif
								@endif


								@if (!empty(array_filter($lower_vMissingTeeth)))
								@php
								$parts=array_filter($lower_vMissingTeeth);
								$lower_vMissingTeeth = (implode(",",$parts));
								@endphp
								<tr>
									<td style=" text-align: center;border-radius:5px;border: 1px solid #000;">
										<div style=" width:100%; padding:1px;">
											<div class="inner-font-size"> MISSING TEETH</div>
											<div class="teeth-counter-text">{{$lower_vMissingTeeth}}</div>
										</div>
									</td>
								</tr>
								@elseif(isset($lower_vMissingTeethOp))
								@if (!empty(array_filter($lower_vMissingTeethOp)))
								<tr>
									<td style=" text-align: center;border-radius:5px;border: 1px solid #000;">
										<div style="width:100%;padding:1px;">
											<div style="margin-bottom:1px;" class="inner-font-size"> MISSING TEETH</div>
											<div class="teeth-counter-text">{{$lower_vMissingTeethOp}}</div>
										</div>
									</td>
								</tr>
								@endif
								@endif
								@if (!empty(array_filter($lower_vWillExtractOnDelivery)))
								@php
								$parts=array_filter($lower_vWillExtractOnDelivery);
								$lower_vWillExtractOnDelivery = (implode(",",$parts));
								@endphp
								<tr>
									<td style=" text-align: center;border-radius:5px;border: 1px solid #000;">
										<div style=" width:100%;font-size: 10px;padding:1px;">
											<div style="margin-bottom:1px;" class="inner-font-size"> WILL EXTRACT ON DELIVERY </div>
											<div class="teeth-counter-text">{{$lower_vWillExtractOnDelivery}}</div>
										</div>
									</td>
								</tr>
								@elseif(isset($lower_vWillExtractOnDeliveryOp))
								@if (!empty(array_filter($lower_vWillExtractOnDeliveryOp)))

								<tr>
									<td style=" text-align: center;border-radius:5px;border: 1px solid #000;">
										<div style=" width:100%;padding:1px;">
											<div style="margin-bottom:1px;" class="inner-font-size"> WILL EXTRACT ON DELIVERY </div>
											<div class="teeth-counter-text">{{$lower_vWillExtractOnDeliveryOp}}</div>
										</div>
									</td>
								</tr>
								@endif
								@endif
								@if (!empty(array_filter($lower_vHasBeenExtracted)))
								@php
								$parts=array_filter($lower_vHasBeenExtracted);
								$lower_vHasBeenExtracted = (implode(",",$parts));
								@endphp
								<tr>
									<td style="text-align: center;border-radius:5px;border: 1px solid #000;">
										<div style="width:100%;padding:1px;">
											<div style="margin-bottom:1px;" class="inner-font-size"> HAS BEEN EXTRACTED</div>
											<div class="teeth-counter-text">{{$lower_vHasBeenExtracted}}</div>
										</div>
									</td>
								</tr>
								@elseif(isset($lower_vHasBeenExtractedOp))
								@if (!empty(array_filter($lower_vHasBeenExtractedOp)))
								<tr>
									<td style="text-align: center;border-radius:5px;border: 1px solid #000;">
										<div style="width:100%;padding:1px;">
											<div style="margin-bottom:1px;" class="inner-font-size"> HAS BEEN EXTRACTED</div>
											<div class="teeth-counter-text">{{$lower_vHasBeenExtractedOp}}</div>
										</div>
									</td>
								</tr>
								@endif
								@endif
								<tr>
									<table style="margin:0px;padding: 0px;width:100%;border-spacing: 0px;">
										<tr>
											@if (!empty(array_filter($lower_vFixOrAdd)))
											@php
											$parts=array_filter($lower_vFixOrAdd);
											$lower_vFixOrAdd = (implode(",",$parts));
											@endphp
											<td style="width: 50%;">
												<div style="padding:1px;text-align: center;border: 1px solid #000;border-radius:5px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> FIX OR ADD</div>
													<div class="teeth-counter-text">{{$lower_vFixOrAdd}}</div>
												</div>
											</td>
											@elseif(isset($lower_vFixOrAddOp))
											@if (!empty(array_filter($lower_vFixOrAddOp)))
											<td style="width: 50%;">
												<div style="padding:1px;text-align: center;border: 1px solid #000;border-radius:5px;">
													<div style="margin-bottom:1px;" class="inner-font-size"> FIX OR ADD</div>
													<div class="teeth-counter-text">{{$lower_vFixOrAddOp}}</div>
												</div>
											</td>
											@endif
											@endif

											@if (!empty(array_filter($lower_vClasps)))
											@php
											$parts=array_filter($lower_vClasps);
											$lower_vClasps = (implode(",",$parts));
											@endphp
											<td style="width: 50%;">
												<div style="padding:1px;margin-right: auto;text-align: center;border: 1px solid #000;border-radius:5px;">
													<div style="margin-bottom:1px;" class="inner-font-size">CLASPS</div>
													<div class="teeth-counter-text">{{$lower_vClasps}}</div>
												</div>
											</td>
											@elseif(isset($lower_vClaspsOp))
											@if (!empty(array_filter($lower_vClaspsOp)))
											<td style="width: 50%;">
												<div style="padding:1px;margin-right: auto;text-align: center;border: 1px solid #000;border-radius:5px;">
													<div style="margin-bottom:1px;" class="inner-font-size">CLASPS</div>
													<div class="teeth-counter-text">{{$lower_vClaspsOp}}</div>
												</div>
											</td>
											@endif
											@endif
										</tr>
										<tr>
											<td style="text-align: center;"></td>
										</tr>
										<tr>
											<td style="font-size:7px;text-align: right;"></td>
										</tr>

									</table>
								</tr>

							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>


	@if(empty($Slip->tStagesNotes))
	@php
	$tStagesNotes = "";
	@endphp
	@else
	@php
	$tStagesNotes = $Slip->tStagesNotes;
	@endphp
	@endif
	<table style="max-width:500px;width:500px; margin: 0 auto 0px 0px;border-collapse: collapse;">
		<tr>
			<td style="vertical-align:top;padding:0px;border-radius:5px;" colspan="2">
				<table style="margin: 0px;width: 500px;border:1px solid #000;border-radius: 10px;">
					<!-- <tr>
								<td style="text-align: center;">
									<span>STAGES NOTES </span>
								</td>
							</tr> -->
					<tr>
						<td style="padding: 0px 10px; text-align: center">
							<div style="text-align: center;border-radius:5px;padding:5px 5px;width:100%;">
								<h6 style="margin: 0px;padding: 0px;font-size: 8px;">STAGE NOTES</h6>
								<p style="text-align: justify;font-size: 8px;">
									{{$tStagesNotes}}
								</p>
							</div>
						</td>
						<!-- <td style="text-align: right;">
									<div style="text-align: right;">
										@if(isset($Doctor))
										<img alt="dr.'s signature" style="width:60px;height:60px;margin: auto;" / id="img" value="@if(old('vImage') == 'vImage') selected @endif" src="{{asset('uploads/doctor/'.$Doctor->vImage)}}">
										@else
										<img alt="dr.'s signature" style="width:60px;height:60px;margin: auto;" / id="img" value="" src="{{asset('images/no-image.gif')}}">
										@endif
		
									</div>
									<span style="border-top: 1px solid #000;font-size:7px;">Dr’s signature & license #</span>
									<span style="font-size:7px;border-top: 1px solid #000;"></span>
								</td> -->
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="padding-top: 20px;" >
				@php $qr_code = route('admin.labcase.view_driverhistory_by_qr',[$Slip->iCaseId,$Slip->iSlipId]); @endphp
				<div style="text-align:right;">
					<a style="padding: 0px;margin-bottom:0px;" target="_blank" href="https://chart.googleapis.com/chart?chs=500x500&cht=qr&choe=UTF-8&chl={{$qr_code}}">
						<img src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&choe=UTF-8&chl={{$qr_code}}" style="height:auto;width:80px;display:inline-block;margin:0px;padding: 0px;" download>
					</a>
					<p style="text-align: right;margin-top:0px !important;padding-top:0px !important">Driver-Slip</p>
				</div>	
			</td>
			<td style="text-align: right;padding-top: 20px;">
					
				<div style="text-align: right;margin-bottom: 5px;">
					@if(isset($Doctor->vImage) && @getimagesize(asset('uploads/doctor/'.$Doctor->vImage)))
						<img alt="dr.'s signature" style="width:150px;height:50px;margin: auto;object-fit: contain;" / id="img" value="@if(old('vImage') == 'vImage') selected @endif" src="{{asset('uploads/doctor/'.$Doctor->vImage)}}">
					@endif

				</div>
				@isset($Doctor->vLicence)
				<span style="border-top: 0.5px solid #000;font-size:11px;">Dr’s signature & license # {{$Doctor->vLicence}}</span>
				@endisset
			</td>
		</tr>
	</table>
	</td>

	</table>
</body>

</html>