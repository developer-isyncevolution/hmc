
@if(isset($data_new[0]->vLocation) && ($data_new[0]->vLocation =='In office ready to pickup' || $data_new[0]->vLocation == 'On route to the lab' | $data_new[0]->vLocation == 'In lab'))
<style>
  .delete{
    display: block;
  }
</style>
 @else
 <style>
  .delete{
    display: none;
  }
</style>
@endif

@if(count($data) > 0)

<table class="tslip driver-inner-model" style="border-bottom: 1px solid #eff2f5;">
  <tbody>
    <tr>
        <th>Office: <span>{{isset($data[0]->vOfficeName)?$data[0]->vOfficeName:'N/A'}}</span></th>
        <th>Patient: <span>{{isset($data[0]->vPatientName)?$data[0]->vPatientName:'N/A'}}</span></th>
        <th>Case # <span>{{isset($data[0]->vCaseNumber)?$data[0]->vCaseNumber:'N/A'}}</span></th>
        <th>Pan # <span>{{isset($data[0]->vCasePanNumber)?$data[0]->vCasePanNumber:'N/A'}}</span></th>
    </tr>
    </tbody>
</table>
@foreach($data as $key =>$data_val)
@if($key == 1)
<br>
@endif

{{-- @endif --}}
<div>
<table class="tslip driver-inner-model mb-5" style="table-layout: fixed">
<tbody>
  {{-- {{dd($show_history_val)}} --}}
  {{-- @if(isset($HistoryType) && $HistoryType =='ShowAll') --}}
    <tr>
      <td colspan="7">
        <table class="w-50 mx-auto my-2 text-center">
          <tr>
            <th class="text-center">Slip ID: {{isset($data_val->vSlipNumber)?$data_val->vSlipNumber:'N/A'}}</th>
            <th class="text-end">Stage: {{isset($data_val->vStageName)?$data_val->vStageName:'N/A'}}</th>
          </tr>
        </table>
      </td>
    </tr>
  </tbody>
</table>
    {{-- @endif --}}
    @php
    $data = $data_val->slip_attachment;
    @endphp
     <div class="row g-3 mb-3">
    @foreach($data as $value)
        @php
        $infoPath = pathinfo(asset('/uploads/slip_gallery/'.$value->vName));
        $ext = $infoPath['extension'];
        @endphp
     
        @if ($ext == 'mp4' || $ext == 'mov' || $ext == 'mkv')
            <div class="col-lg-4 galleryimg"  >
                <a target="_blank" href="{{asset('uploads/slip_gallery/'.$value->vName)}}" data-fancybox="group" class="fancybox-gallery" data-bs-toggle="tooltip" data-bs-placement="top" title="{{$value->vName}}">
                    <p class="text-center mt-2">
                        <video width="120" height="150" controls autoplay="" id="video">
                            <source id="vid" src="{{asset('uploads/slip_gallery/'.$value->vName)}}" type="video/mp4">
                        </video>
                        <a class="delete" id="attachment_delete" href="javascript:;" data-id="{{$value->iSlipAttachmentId}}"><i class="la la-trash text-danger"></i>
                        </a>
                    </p>
                </a>
            </div>
        @elseif ($ext == 'mp3' || $ext == 'ogg' || $ext == 'flac')
    
        <div class="col-lg-4 galleryimg">
            <a target="_blank" href="#audio" data-fancybox="group" data-bs-toggle="tooltip" data-bs-placement="top" title="{{$value->vName}}">
                <p class="text-center mt-2">
                    <video width="120" height="100" controls autoplay="" id="audio">
                        <source id="" src="{{asset('uploads/slip_gallery/'.$value->vName)}}" type="audio/mp3">
                    </video>
            </a> <a class="delete" id="attachment_delete" href="javascript:;" data-id="{{$value->iSlipAttachmentId}}"><i class="la la-trash text-danger"></i></a></p>
            </a>
        </div>
        @elseif($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg' || $ext == 'gif')  
        <div class="col-lg-4 galleryimg">
          <a class="delete" download="{{$value->vName}}" href="{{asset('uploads/slip_gallery/'.$value->vName)}}" style="right: 5px;top: 113px;" ><i style="color: #eff2f5" class="fa fa-download"></i>
          </a>
            <a target="_blank" href="{{asset('uploads/slip_gallery/'.$value->vName)}}" data-fancybox="group" data-bs-toggle="tooltip" data-bs-placement="top" title="{{$value->vName}}">
                <div class="mained">
                    <img src="{{asset('uploads/slip_gallery/'.$value->vName)}}" class="imgese" />
                </div>
            </a>
            <a class="delete" id="attachment_delete" href="javascript:;" data-id="{{$value->iSlipAttachmentId}}"><i class="la la-trash text-danger"></i></a>
        </div>    
        @else
        
        <div class="col-lg-4 galleryimg">
          
          <i style="font-size: 149px;color: #b3b3c1;" class="fas fa-file-alt">
            <a href="#" download="{{asset('uploads/slip_gallery/'.$value->vName)}}"> </a>
          </i>
          <a download="{{$value->vName}}" href="{{asset('uploads/slip_gallery/'.$value->vName)}}" style="right: 5px;top: 113px;" > <i style="color: #777785; font-size: 20px;" class="fas fa-download"></i>
          </a>
          {{-- <a class="delete" download="{{$value->vName}}" href="{{asset('uploads/slip_gallery/'.$value->vName)}}" style="right: 5px;top: 113px;" ><i style="color: #777785; font-size: 20px;" class="fas fa-download"></i>
          </a> --}}
            <a class="delete" id="attachment_delete" href="javascript:;" data-id="{{$value->iSlipAttachmentId}}"><i class="la la-trash text-danger"></i>
            </a>
           
        </div>  
        @endif
     
    @endforeach
  </div>
    {{-- @else
    <div class="col-md-12">
        <center><b>No record found</b></center>
    </div><br>&nbsp;
    @endif --}}
    

@endforeach
@else
<table class="tslip driver-inner-model" style="border-bottom: 1px solid #eff2f5;">
  <tbody>
    <tr>
        <th>Office: <span>{{isset($data_new[0]->vOfficeName)?$data_new[0]->vOfficeName:'N/A'}}</span></th>
        <th>Patient: <span>{{isset($data_new[0]->vPatientName)?$data_new[0]->vPatientName:'N/A'}}</span></th>
        <th>Case # <span>{{isset($data_new[0]->vCaseNumber)?$data_new[0]->vCaseNumber:'N/A'}}</span></th>
        <th>Pan # <span>{{isset($data_new[0]->vCasePanNumber)?$data_new[0]->vCasePanNumber:'N/A'}}</span></th>
      </tr>
    </tbody>
</table>
</div>

<h4 style="text-align: center">No Record Found</h4>
@endif









