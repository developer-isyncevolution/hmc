@if (!empty($toothshade))    
    @if (count($toothshade) > 1)
        <option value="">Shade</option>
    @endif
    @foreach($toothshade as $key => $toothshade_val)
        <option value="{{$toothshade_val->iToothShadesId}}"> {{$toothshade_val->vCode}}</option>
    @endforeach
@else
    <option value="">No Shade</option>
@endif
