@extends('layouts.admin.index')
@section('content')

<style>
     @import url('https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap');
   .red-btn{
      background: radial-gradient( circle, #ff0000 0%, rgba(250, 3, 3, 1) 0%, rgb(196 2 2) 100% );
   }
   
   .tableFixHead {
        overflow-y: auto;
        max-height: 406px;
      }
      .tableFixHead thead th {
        position: sticky;
        top: 0;
      }
      table {
        border-collapse: collapse;
        width: 100%;
      }
      th {
        background: #0061a0 !important;
      }
</style>

<!-- BEGIN: Content-->
<!-- DOM - jQuery events table -->
<div class="main-panel" style="padding: 20px; margin: 0px;">
   <div class="page-title text-center">
      <h3>
         Driver History
      </h3>
   </div>
   <div class="col-lg-12 mx-auto">
      <div class="card mb-5 mb-xl-4">
         <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="modal fade  show" id="CustomerDataModel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="CustomerDataModelLabel" bis_skin_checked="1" aria-modal="true" role="dialog" style="display: block;">
                     <div class="modal-dialog modal-xl" bis_skin_checked="1" style="width: 80%;
                     max-width: 80%;">
                        <div class="modal-content" bis_skin_checked="1">
                           <div class="modal-header" bis_skin_checked="1">
                              <h5 class="modal-title" id="CustomerDataModelLabel">Driver History</h5>
                              <a href="{{route('admin.labcase')}}" class="btn-close close-model"  aria-label="Close"></a>
                           </div>
                           <form id="frm" action="{{route('admin.labcase.store_multiple_driverHistory')}}" method="post">
                              @csrf
                           <div class="modal-body"  bis_skin_checked="1" >
                              <div class="table-data table-responsive tableFixHead" bis_skin_checked="1">
                                 <table class="table" >
                                    <thead style="background: #0061a0 ;">
                                       <tr>
                                          <th class="min-w-100px text-center">
                                             <span class="text-muted fw-bold text-muted d-block fs-7">Location </span>
                                          </th>
                                          <th class="w-40px" data-orderable="false">
                                             <div class="form-check form-check-sm form-check-custom form-check-solid">
                                                 <input id="selectall" type="checkbox" name="selectall" class="form-check-input">
                                                 <label for="selectall">&nbsp;</label>
                                             </div>
                                         </th>
                                          
                                          <th class="min-w-100px">
                                             <span class="text-muted fw-bold text-muted d-block fs-7">Office </span>
                                          </th>
                                          <th class="min-w-100px" >
                                             <span class="text-muted fw-bold text-muted d-block fs-7">Action</span>
                                          </th>
                                          {{-- <th class="min-w-100px">
                                             <span class="text-muted fw-bold text-muted d-block fs-7">Case #</span>
                                          </th>
                                          <th class="min-w-100px">
                                             <span class="text-muted fw-bold text-muted d-block fs-7">Slip #</span>
                                          </th> --}}
                                          <th class="min-w-100px">
                                             <span class="text-muted fw-bold text-muted d-block fs-7">Patient Name</span>
                                          </th>
                                          {{-- <th class="min-w-100px">
                                             <span class="text-muted fw-bold text-muted d-block fs-7">Pick Up Date</span>
                                          </th> --}}
                                         
                                          
                                       </tr>
                                    </thead>
                                    <tbody >
                                        @if(isset($data) && count($data)>0)
                                        @foreach ($data as $data_val)
                                        <tr>
                                             <td style="text-align:center">
                                                {{$data_val->vLocation}}
                                             </td>
                                             <td>
                                                <div class="form-check form-check-sm form-check-custom form-check-solid">
                                                   <input id="Slip_ID_{{$data_val->iSlipId}}" type="checkbox" name="Slip_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$data_val->iSlipId}}">
                                                   <label for="Slip_ID_{{$data_val->iSlipId}}">&nbsp;</label>
                                                </div>
                                             </td>
                                            <td>{{isset($data_val->vOffice__Code)&& !empty($data_val->vOffice__Code)?$data_val->vOffice__Code:$data_val->office_name__slip}}</td>
                                            <td>
                                             @if(isset($data_val->vLocation) && ( $data_val->vLocation =='In lab ready to pickup'))
                                             <a data-bs-toggle="tooltip" data-bs-placement="top" title="Pick up"  data-case-id="{{$data_val->iCaseId}}" data-slip-id="{{$data_val->iSlipId}}" href="#" class=" driverModel" data-name="LabPickUp"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.38 34.12" style="
                                                fill: green;
                                                height: 30px;
                                            "><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M1.83,23.08A1.1,1.1,0,0,0,3,23.77H5.6l.56,0c0,.24,0,.45,0,.65V34h.6v-.25c0-3.12,0-6.23,0-9.35v-.62H9.9a2.67,2.67,0,0,0,.4,0,1,1,0,0,0,.83-.66.83.83,0,0,0-.24-.9c-.69-.76-1.38-1.51-2.09-2.27-.53-.57-1.05-1.16-1.61-1.71a1,1,0,0,0-1.57,0L2.15,22C1.87,22.29,1.61,22.6,1.83,23.08Zm.63-.48a1.46,1.46,0,0,1,.16-.19c1.09-1.17,2.19-2.32,3.28-3.49.45-.48.61-.48,1.07,0l3.27,3.49a1.3,1.3,0,0,1,.18.23.3.3,0,0,1-.2.49,2.17,2.17,0,0,1-.44,0H3.09a2.48,2.48,0,0,1-.39,0C2.4,23.09,2.31,22.86,2.46,22.6Z" style="
                                                stroke: green;
                                                stroke-width: 0.25px;
                                            "></path><path d="M14.9,17.24a1.22,1.22,0,0,0,1.23-.73c.38-.73.77-1.45,1.15-2.17.28-.53.57-1,1.29-.83s.72.79.72,1.37c0,1.36,0,2.72,0,4.08a1.77,1.77,0,0,1-.73,1.53,27.36,27.36,0,0,0-2.39,2,3.35,3.35,0,0,0-1,1.53c-.41,2.08-.72,4.18-1,6.28A1,1,0,0,0,15,31.49a1,1,0,0,0,1.24-.62,2.9,2.9,0,0,0,.19-.73c.27-1.67.49-3.36.8-5a2.32,2.32,0,0,1,.68-1.18c1.41-1.27,2.87-2.49,4.32-3.72a3.09,3.09,0,0,0,1.25-2.32c0-2.32.06-4.65,0-7a2,2,0,0,0-2-2c-.48-.05-1,0-1.44,0A3.41,3.41,0,0,0,16.58,11c-.59,1.17-1.25,2.29-1.87,3.43a.9.9,0,0,1-1.42.38,3,3,0,0,1,.23-.57c.27-.39.62-.72.86-1.12.59-1,1.13-2,1.71-3A3.93,3.93,0,0,1,19.58,8c.51,0,1,0,1.53,0a3,3,0,0,1,3.26,3.3c0,2,0,3.91,0,5.87a4.51,4.51,0,0,1-1.66,3.65c-1.3,1.1-2.57,2.22-3.89,3.3A2.36,2.36,0,0,0,18,25.73c-.23,1.62-.5,3.24-.77,4.86a1.93,1.93,0,0,1-2.11,1.85A2,2,0,0,1,13.3,30c.24-1.71.51-3.42.74-5.13a5,5,0,0,1,1.83-3.33c.88-.71,1.7-1.47,2.47-2.14V14.68l-.17-.06c-.39.71-.81,1.41-1.16,2.14a2.21,2.21,0,0,1-2.23,1.35c-4.16,0-8.32,0-12.49,0C.68,18.09,0,17.47,0,15.83c0-2.52,0-5,0-7.56A2,2,0,0,1,2.25,6c2.61,0,5.22,0,7.82,0a1.9,1.9,0,0,1,2,2.1q0,3.78,0,7.56c0,.48,0,.95-.09,1.6C13,17.24,14,17.21,14.9,17.24Zm-5,0c.94,0,1.28-.26,1.29-1.17q.06-3.94,0-7.89C11.2,7.29,10.86,7,10,7Q6.06,7,2.11,7C1.31,7,.94,7.32.93,8.13q0,4,0,8c0,.8.38,1.11,1.18,1.12,1.33,0,2.66,0,4,0Z"></path><path d="M19.32,6.45a3.3,3.3,0,0,1-3.26-3.24A3.25,3.25,0,0,1,19.33,0a3.17,3.17,0,0,1,3.24,3.22A3.23,3.23,0,0,1,19.32,6.45Zm2.34-3.33A2.34,2.34,0,0,0,19.32.83,2.41,2.41,0,0,0,17,3.14a2.42,2.42,0,0,0,2.38,2.37A2.33,2.33,0,0,0,21.66,3.12Z"></path><path d="M24.37,22.61l-.77-.22c0,.38-.1.68-.11,1v6.28c0,.22,0,.45,0,.68-.07.79-.5,1.23-1.15,1.19s-1-.44-1-1.22c0-1.3,0-2.61,0-3.91a4,4,0,0,0-.22-.94l-.38,0a3.28,3.28,0,0,0-.25.83c0,1.38,0,2.77,0,4.16a2,2,0,0,0,1.92,2.05,2,2,0,0,0,2-2C24.39,27.89,24.37,25.31,24.37,22.61Z"></path></g></g></svg></a>
                                             @elseif(isset($data_val->vLocation) && ( $data_val->vLocation =='In office ready to pickup'))
                                             <a data-bs-toggle="tooltip" data-bs-placement="top" title="Pick up"  data-case-id="{{$data_val->iCaseId}}" data-slip-id="{{$data_val->iSlipId}}" href="#" class=" driverModel" data-name="OfficePickUp"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.38 34.12" style="
                                                fill: green;
                                                height: 30px;
                                            "><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M1.83,23.08A1.1,1.1,0,0,0,3,23.77H5.6l.56,0c0,.24,0,.45,0,.65V34h.6v-.25c0-3.12,0-6.23,0-9.35v-.62H9.9a2.67,2.67,0,0,0,.4,0,1,1,0,0,0,.83-.66.83.83,0,0,0-.24-.9c-.69-.76-1.38-1.51-2.09-2.27-.53-.57-1.05-1.16-1.61-1.71a1,1,0,0,0-1.57,0L2.15,22C1.87,22.29,1.61,22.6,1.83,23.08Zm.63-.48a1.46,1.46,0,0,1,.16-.19c1.09-1.17,2.19-2.32,3.28-3.49.45-.48.61-.48,1.07,0l3.27,3.49a1.3,1.3,0,0,1,.18.23.3.3,0,0,1-.2.49,2.17,2.17,0,0,1-.44,0H3.09a2.48,2.48,0,0,1-.39,0C2.4,23.09,2.31,22.86,2.46,22.6Z" style="
                                                stroke: green;
                                                stroke-width: 0.25px;
                                            "></path><path d="M14.9,17.24a1.22,1.22,0,0,0,1.23-.73c.38-.73.77-1.45,1.15-2.17.28-.53.57-1,1.29-.83s.72.79.72,1.37c0,1.36,0,2.72,0,4.08a1.77,1.77,0,0,1-.73,1.53,27.36,27.36,0,0,0-2.39,2,3.35,3.35,0,0,0-1,1.53c-.41,2.08-.72,4.18-1,6.28A1,1,0,0,0,15,31.49a1,1,0,0,0,1.24-.62,2.9,2.9,0,0,0,.19-.73c.27-1.67.49-3.36.8-5a2.32,2.32,0,0,1,.68-1.18c1.41-1.27,2.87-2.49,4.32-3.72a3.09,3.09,0,0,0,1.25-2.32c0-2.32.06-4.65,0-7a2,2,0,0,0-2-2c-.48-.05-1,0-1.44,0A3.41,3.41,0,0,0,16.58,11c-.59,1.17-1.25,2.29-1.87,3.43a.9.9,0,0,1-1.42.38,3,3,0,0,1,.23-.57c.27-.39.62-.72.86-1.12.59-1,1.13-2,1.71-3A3.93,3.93,0,0,1,19.58,8c.51,0,1,0,1.53,0a3,3,0,0,1,3.26,3.3c0,2,0,3.91,0,5.87a4.51,4.51,0,0,1-1.66,3.65c-1.3,1.1-2.57,2.22-3.89,3.3A2.36,2.36,0,0,0,18,25.73c-.23,1.62-.5,3.24-.77,4.86a1.93,1.93,0,0,1-2.11,1.85A2,2,0,0,1,13.3,30c.24-1.71.51-3.42.74-5.13a5,5,0,0,1,1.83-3.33c.88-.71,1.7-1.47,2.47-2.14V14.68l-.17-.06c-.39.71-.81,1.41-1.16,2.14a2.21,2.21,0,0,1-2.23,1.35c-4.16,0-8.32,0-12.49,0C.68,18.09,0,17.47,0,15.83c0-2.52,0-5,0-7.56A2,2,0,0,1,2.25,6c2.61,0,5.22,0,7.82,0a1.9,1.9,0,0,1,2,2.1q0,3.78,0,7.56c0,.48,0,.95-.09,1.6C13,17.24,14,17.21,14.9,17.24Zm-5,0c.94,0,1.28-.26,1.29-1.17q.06-3.94,0-7.89C11.2,7.29,10.86,7,10,7Q6.06,7,2.11,7C1.31,7,.94,7.32.93,8.13q0,4,0,8c0,.8.38,1.11,1.18,1.12,1.33,0,2.66,0,4,0Z"></path><path d="M19.32,6.45a3.3,3.3,0,0,1-3.26-3.24A3.25,3.25,0,0,1,19.33,0a3.17,3.17,0,0,1,3.24,3.22A3.23,3.23,0,0,1,19.32,6.45Zm2.34-3.33A2.34,2.34,0,0,0,19.32.83,2.41,2.41,0,0,0,17,3.14a2.42,2.42,0,0,0,2.38,2.37A2.33,2.33,0,0,0,21.66,3.12Z"></path><path d="M24.37,22.61l-.77-.22c0,.38-.1.68-.11,1v6.28c0,.22,0,.45,0,.68-.07.79-.5,1.23-1.15,1.19s-1-.44-1-1.22c0-1.3,0-2.61,0-3.91a4,4,0,0,0-.22-.94l-.38,0a3.28,3.28,0,0,0-.25.83c0,1.38,0,2.77,0,4.16a2,2,0,0,0,1.92,2.05,2,2,0,0,0,2-2C24.39,27.89,24.37,25.31,24.37,22.61Z"></path></g></g></svg></a>
                                            
                                             @elseif(isset($data_val->vLocation) && (  $data_val->vLocation =='On route to the lab'))
                                             <a data-bs-toggle="tooltip" data-bs-placement="top" title="Drop Off" href="#" data-case-id="{{$data_val->iCaseId}}" data-slip-id="{{$data_val->iSlipId}}" class=" driverModel" data-name="DropOff"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.38 34.13" style="&#10;    fill: red;&#10;    height: 30px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M22.55,28.77a1.1,1.1,0,0,0-1.13-.68c-.87,0-1.76,0-2.64,0l-.56,0a5.28,5.28,0,0,1,0-.66V17.85h-.59v.25c0,3.12,0,6.23,0,9.35v.63H14.47a2.54,2.54,0,0,0-.39,0,1,1,0,0,0-.84.65.85.85,0,0,0,.24.9l2.1,2.27c.53.57,1,1.16,1.6,1.71a1,1,0,0,0,1.58,0l3.47-3.72C22.51,29.57,22.77,29.25,22.55,28.77Zm-.64.48a1,1,0,0,1-.15.19l-3.28,3.49c-.45.48-.61.48-1.07,0-1.09-1.15-2.19-2.32-3.27-3.49a1.06,1.06,0,0,1-.19-.23.31.31,0,0,1,.21-.49,2.12,2.12,0,0,1,.44,0h6.69a2.55,2.55,0,0,1,.39,0C22,28.76,22.07,29,21.91,29.25Z" style="&#10;    stroke: red;&#10;    stroke-width: 0.25px;&#10;"/><path d="M12.36,17.24c0-.65-.09-1.12-.1-1.6V8.08A1.91,1.91,0,0,1,14.31,6c2.6,0,5.21,0,7.81,0a2,2,0,0,1,2.24,2.29q0,3.78,0,7.56c0,1.64-.66,2.26-2.27,2.26-4.17,0-8.33,0-12.5,0a2.19,2.19,0,0,1-2.22-1.35C7,16,6.6,15.33,6.21,14.62L6,14.68v4.73c.76.67,1.59,1.43,2.47,2.14a5,5,0,0,1,1.83,3.33c.23,1.71.49,3.42.73,5.13a2,2,0,0,1-1.76,2.43A1.93,1.93,0,0,1,7.2,30.59c-.27-1.62-.54-3.24-.78-4.86a2.29,2.29,0,0,0-.88-1.57C4.23,23.08,3,22,1.66,20.86A4.48,4.48,0,0,1,0,17.21c0-2,0-3.91,0-5.87A3,3,0,0,1,3.27,8c.51,0,1,0,1.53,0a3.93,3.93,0,0,1,3.49,2.05c.57,1,1.12,2,1.71,3,.24.4.59.73.86,1.12a3,3,0,0,1,.23.57.9.9,0,0,1-1.42-.38C9.05,13.29,8.39,12.17,7.8,11A3.42,3.42,0,0,0,4.32,8.93c-.48,0-1,0-1.44,0a2,2,0,0,0-2,2c-.05,2.32,0,4.65,0,7a3.09,3.09,0,0,0,1.25,2.32c1.45,1.23,2.9,2.45,4.31,3.72a2.25,2.25,0,0,1,.68,1.18c.32,1.67.54,3.36.81,5a2.89,2.89,0,0,0,.18.73,1,1,0,0,0,1.25.62,1,1,0,0,0,.79-1.15c-.3-2.1-.6-4.2-1-6.28a3.33,3.33,0,0,0-1-1.53,27.36,27.36,0,0,0-2.39-2A1.78,1.78,0,0,1,5.07,19c0-1.36,0-2.72,0-4.08,0-.58,0-1.18.73-1.37s1,.3,1.29.83c.37.72.77,1.44,1.15,2.17a1.22,1.22,0,0,0,1.22.73C10.43,17.21,11.39,17.24,12.36,17.24Zm5.92,0c1.33,0,2.66,0,4,0,.8,0,1.17-.32,1.17-1.12q0-4,0-8c0-.81-.37-1.12-1.17-1.12q-3.94,0-7.89,0c-.86,0-1.21.29-1.22,1.17q0,3.95,0,7.89c0,.91.36,1.17,1.3,1.17Z"/><path d="M1.81,3.22A3.16,3.16,0,0,1,5,0,3.25,3.25,0,0,1,8.32,3.21,3.3,3.3,0,0,1,5.05,6.45,3.22,3.22,0,0,1,1.81,3.22ZM5,5.51A2.42,2.42,0,0,0,7.42,3.14,2.42,2.42,0,0,0,5.06.83,2.34,2.34,0,0,0,5,5.51Z"/><path d="M0,22.61l.76-.22a8,8,0,0,1,.11,1q0,3.13,0,6.28c0,.22,0,.45,0,.68.07.79.5,1.23,1.15,1.19s1-.44,1-1.22c0-1.3,0-2.61,0-3.91a4.6,4.6,0,0,1,.23-.94l.38,0a3.75,3.75,0,0,1,.25.83c0,1.38,0,2.77,0,4.16A2,2,0,0,1,2,32.46a2,2,0,0,1-2-2C0,27.89,0,25.31,0,22.61Z"/></g></g></svg></a>
                                             @elseif(isset($data_val->vLocation) && (  $data_val->vLocation =='On route to the office'))
                                             <a data-bs-toggle="tooltip" data-bs-placement="top" title="Drop Off" href="#" data-case-id="{{$data_val->iCaseId}}" data-slip-id="{{$data_val->iSlipId}}" class=" driverModel" data-name="LabDropUp"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.38 34.13" style="&#10;    fill: red;&#10;    height: 30px;&#10;"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M22.55,28.77a1.1,1.1,0,0,0-1.13-.68c-.87,0-1.76,0-2.64,0l-.56,0a5.28,5.28,0,0,1,0-.66V17.85h-.59v.25c0,3.12,0,6.23,0,9.35v.63H14.47a2.54,2.54,0,0,0-.39,0,1,1,0,0,0-.84.65.85.85,0,0,0,.24.9l2.1,2.27c.53.57,1,1.16,1.6,1.71a1,1,0,0,0,1.58,0l3.47-3.72C22.51,29.57,22.77,29.25,22.55,28.77Zm-.64.48a1,1,0,0,1-.15.19l-3.28,3.49c-.45.48-.61.48-1.07,0-1.09-1.15-2.19-2.32-3.27-3.49a1.06,1.06,0,0,1-.19-.23.31.31,0,0,1,.21-.49,2.12,2.12,0,0,1,.44,0h6.69a2.55,2.55,0,0,1,.39,0C22,28.76,22.07,29,21.91,29.25Z" style="&#10;    stroke: red;&#10;    stroke-width: 0.25px;&#10;"/><path d="M12.36,17.24c0-.65-.09-1.12-.1-1.6V8.08A1.91,1.91,0,0,1,14.31,6c2.6,0,5.21,0,7.81,0a2,2,0,0,1,2.24,2.29q0,3.78,0,7.56c0,1.64-.66,2.26-2.27,2.26-4.17,0-8.33,0-12.5,0a2.19,2.19,0,0,1-2.22-1.35C7,16,6.6,15.33,6.21,14.62L6,14.68v4.73c.76.67,1.59,1.43,2.47,2.14a5,5,0,0,1,1.83,3.33c.23,1.71.49,3.42.73,5.13a2,2,0,0,1-1.76,2.43A1.93,1.93,0,0,1,7.2,30.59c-.27-1.62-.54-3.24-.78-4.86a2.29,2.29,0,0,0-.88-1.57C4.23,23.08,3,22,1.66,20.86A4.48,4.48,0,0,1,0,17.21c0-2,0-3.91,0-5.87A3,3,0,0,1,3.27,8c.51,0,1,0,1.53,0a3.93,3.93,0,0,1,3.49,2.05c.57,1,1.12,2,1.71,3,.24.4.59.73.86,1.12a3,3,0,0,1,.23.57.9.9,0,0,1-1.42-.38C9.05,13.29,8.39,12.17,7.8,11A3.42,3.42,0,0,0,4.32,8.93c-.48,0-1,0-1.44,0a2,2,0,0,0-2,2c-.05,2.32,0,4.65,0,7a3.09,3.09,0,0,0,1.25,2.32c1.45,1.23,2.9,2.45,4.31,3.72a2.25,2.25,0,0,1,.68,1.18c.32,1.67.54,3.36.81,5a2.89,2.89,0,0,0,.18.73,1,1,0,0,0,1.25.62,1,1,0,0,0,.79-1.15c-.3-2.1-.6-4.2-1-6.28a3.33,3.33,0,0,0-1-1.53,27.36,27.36,0,0,0-2.39-2A1.78,1.78,0,0,1,5.07,19c0-1.36,0-2.72,0-4.08,0-.58,0-1.18.73-1.37s1,.3,1.29.83c.37.72.77,1.44,1.15,2.17a1.22,1.22,0,0,0,1.22.73C10.43,17.21,11.39,17.24,12.36,17.24Zm5.92,0c1.33,0,2.66,0,4,0,.8,0,1.17-.32,1.17-1.12q0-4,0-8c0-.81-.37-1.12-1.17-1.12q-3.94,0-7.89,0c-.86,0-1.21.29-1.22,1.17q0,3.95,0,7.89c0,.91.36,1.17,1.3,1.17Z"/><path d="M1.81,3.22A3.16,3.16,0,0,1,5,0,3.25,3.25,0,0,1,8.32,3.21,3.3,3.3,0,0,1,5.05,6.45,3.22,3.22,0,0,1,1.81,3.22ZM5,5.51A2.42,2.42,0,0,0,7.42,3.14,2.42,2.42,0,0,0,5.06.83,2.34,2.34,0,0,0,5,5.51Z"/><path d="M0,22.61l.76-.22a8,8,0,0,1,.11,1q0,3.13,0,6.28c0,.22,0,.45,0,.68.07.79.5,1.23,1.15,1.19s1-.44,1-1.22c0-1.3,0-2.61,0-3.91a4.6,4.6,0,0,1,.23-.94l.38,0a3.75,3.75,0,0,1,.25.83c0,1.38,0,2.77,0,4.16A2,2,0,0,1,2,32.46a2,2,0,0,1-2-2C0,27.89,0,25.31,0,22.61Z"/></g></g></svg></a>
                                             @endif
                                            </td>
                                            {{-- <td>{{isset($data_val->vCaseNumber)&& !empty($data_val->vCaseNumber)?$data_val->vCaseNumber:'--'}}</td>
                                            <td>{{isset($data_val->vSlipNumber)&& !empty($data_val->vSlipNumber)?$data_val->vSlipNumber:'--'}}</td> --}}
                                            <td>{{isset($data_val->vPatientName)&& !empty($data_val->vPatientName)?$data_val->vPatientName:'--'}}</td>
                                            {{-- <td>{{isset($data_val->dPickUpDate)&& !empty($data_val->dPickUpDate)?date('m/d/Y',strtotime($data_val->dPickUpDate)):'--'}}</td> --}}
                                           
                                        </tr>
                                 
                                        @endforeach
                                        @else
                                        <tr>
                                          <td colspan="3"></td>
                                          <td >
                                             <span style="font-size: 20px;">No record founds</span>
                                          </td>
                                        </tr>
                                       @endif
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                           @if(isset($data) && count($data)>0)
                           <div class="col-lg-8 mx-auto text-center my-2">
                              <input type="text" id="vSignature" name="vSignature" placeholder="Signature" class="signature-input signature-font" >
                              <input type="hidden" name="slipIds" id="slipIds">
                              <input type="hidden" name="is_qr_record" value='Yes'>
                              <div class="text-danger" style="display: none; font-size:20px;" id="vSignature_error">Please enter signature</div>
                           </div>
                           @endif
                           <div class="modal-footer justify-content-between" bis_skin_checked="1">
                              <a href="{{route('admin.labcase')}}" class="btn back-btn"> close </a>
                              @if(isset($data) && count($data)>0)
                              <button type="submit" class="add-btn submit">Submit</button>
                              @endif
                           </div>
                          </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection
@section('custom-js')
<script type="text/javascript">
   $('document').ready(function(){
       $('#CustomerDataModel').show();
       $('#CustomerDataModel').addClass('show');
   });
   $("#selectall").click(function() {
      if (this.checked) {
            $('.checkboxall').each(function() {
               $(".checkboxall").prop('checked', true);
            });
      } 
      else 
      {
            $('.checkboxall').each(function() {
               $(".checkboxall").prop('checked', false);
            });
      }
   });   

   $('.driverModel').click(function(){
         var vSignature = $('#vSignature').val();
       
         if(vSignature.length == 0)
         {
            $('#vSignature_error').show();
            return false;
         }
         else
         {
            $('#vSignature_error').hide();
         }
        let iSlipId = $(this).attr("data-slip-id");
        let iCaseId = $(this).attr("data-case-id");
        let Status = $(this).attr("data-name");
        $.ajax({
            url: "{{route('admin.labcase.store_driverHistory')}}",
            type: "post",
            data: {
                is_qr_record:'Yes',  
                iCaseId: iCaseId,
                iSlipId: iSlipId,
                CurrentStatus:Status,
                vSignature:vSignature,
                _token: '{{csrf_token()}}',
            },
            success: function(response) {
               window.location.replace("{{route('admin.labcase')}}");
            }
        });
    });
    $(document).on('click','.submit',function(){

      var vSignature = $('#vSignature').val();
      if(vSignature.length == 0)
      {
         $('#vSignature_error').show();
         return false;
      }
      else
      {
         $('#vSignature_error').hide();
      }
      var id = [];
      $("input[name='Slip_ID[]']:checked").each(function() {
         id.push($(this).val());
      });
      var id = id.join(",");
      if (id.length == 0) {
         alert('Please select records.');
         return false;
      } else {
         $('#slipIds').val(id);
         $("#frm").submit();
      }
    });
</script>
@endsection