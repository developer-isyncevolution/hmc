@php
    $eVirualSlipAccess = \App\Libraries\General::check_permission_slip('eVirualSlip');
    $ePaperSlipAccess = \App\Libraries\General::check_permission_slip('ePaperSlip');
	$colspan = '7';
@endphp
@if(isset($ePaperSlipAccess) && $ePaperSlipAccess =='No' && isset($eVirualSlipAccess) && $eVirualSlipAccess =='No')
@php
$colspan = '5';
@endphp
@elseif(isset($ePaperSlipAccess) && $ePaperSlipAccess =='No')
@php
$colspan = '6';
@endphp
@elseif(isset($eVirualSlipAccess) && $eVirualSlipAccess =='No')
@php
$colspan = '6';
@endphp

@endif
@if(count($data) > 0)

@foreach($data as $key => $value)
<tr>
	@if(count($data) == 1)
		<input type="hidden" name="count_data" id="count_data" value="1">
	@endif
	<td>
		<span class="badge d-inline-block" style="background-color:{{ $value->vColor }}">@if(!empty($value->vCasePanNumber)){{ $value->vCasePanNumber }} @else {{"----"}}@endif</span>
	</td>
	<td>{{$value->vOfficeName}} </td>
	<td>{{$value->vPatientName}}</td>
	@if(isset($eVirualSlipAccess) && $eVirualSlipAccess =='Yes')
	<td>
		<a class="slip_redirect" href="{{route('admin.labcase.virtual_slip',[$value->iCaseId,$value->iSlipId])}}" >
			<i class="fal fa-eye" style="color: #054b97;font-size: 18px;"></i>
		</a>
	</td>
	@endif
	@if(isset($ePaperSlipAccess) && $ePaperSlipAccess =='Yes')
	<td>
		<div class="d-flex justify-content-center">
			{{-- <a href="{{route('admin.labcase.paper_slip_pdf'[$value->iCaseId,$value->iSlipId])}}" data-case="{{$value->iCaseId}}" data-slip="{{$value->iSlipId}}" class="me-5 ">
				<i class="fal fa-print" style="color: #054b97;font-size: 18px;"></i>
			</a> --}}
			<a href="javascript:;" data-case="{{$value->iCaseId}}" data-slip="{{$value->iSlipId}}" class=" print_paper">
				<i class="fal fa-print" style="color: #054b97;font-size: 18px;"></i>
			</a>
			{{-- <div class="form-check form-check-sm form-check-custom form-check-solid ms-2">
				<input id="Case_ID_{{$value->iCaseId}}" type="checkbox" name="Case_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iCaseId}}">
				<label for="Case_ID_{{$value->iCaseId}}">&nbsp;</label>
			</div> --}}
		</div>
	</td>
	@endif

	@php 
		$delivery_date = $value->dDeliveryDate;
		$current_date = date("Y-m-d");

		$difference =  date_diff(date_create($delivery_date), date_create($current_date));

		$days = $difference->days;
		$days_text = "";
		$color_class = "";
	@endphp

	@if($delivery_date > $current_date)
		@php 
			$days_text = $days ." days to ".date('m/d', strtotime($value->dDeliveryDate)); 
			$color_class = "text-primary";
		@endphp
	@elseif($delivery_date < $current_date)
		@php 
			$days_text = $days ." days past " .date('m/d', strtotime($value->dDeliveryDate)); 
			$color_class = "text-danger";
		@endphp
	@elseif($delivery_date == $current_date)
		@php 
			$days_text = "Due today ".date('m/d', strtotime($value->dDeliveryDate)); 
			$color_class = "text-success";
		@endphp
	@endif

	@if($value->vLocation == "In office")
		@php 
			$days_text = "Delivered"; 
			$color_class = " ";
		@endphp
	@endif

	<td>
		<span class="{{$color_class}}">{{$days_text}}</span>
	</td>

	<td>
		<span class="">{{$value->vLocation}}</span>
		
		@if($value->vLocation == "Draft")
			<a data-patient="{{$value->vPatientName}}" href="{{route('admin.labcase.edit_slip',[$value->iCaseId,$value->iSlipId])}}" class="me-5 print_paper">
				<i class="fad fa-pencil"></i>
			</a>			
		@endif
		@if(\App\Libraries\General::admin_info()['customerType'] == 'Lab Admin')
		<a data-patient="{{$value->vPatientName}}" style="text-align: end" href="{{route('admin.labcase.undo_delete_slip',[$value->iCaseId,$value->iSlipId])}}" class="btn-icon delete-icon  me-4">
			<i class="fa fa-undo" aria-hidden="true"></i>
		</a>
		@endif
	</td>
	<td> {{date('m/d/Y',strtotime($value->dtAddedDate))}} @ {{date('h:i:a',strtotime($value->dtAddedDate))}}</td>
</tr>
@endforeach
<tr>
	{{-- <td  class="text-center border-right-0">
		<a href="javascript:;" title="Multiple delete" id="delete_btn" class="btn-icon  delete_permission delete-icon">
			<i class="fad fa-trash-alt"></i>
		</a>
	</td> --}}
	<td class="border-0" colspan="{{$colspan}}">
		<div class="d-flex">
			{!! $paging !!}
		</div>
	</td>
	{{-- <td class="border-0"></td>
	<td colspan="5" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
	<td class="text-end border-left-0">
		{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}}  --}}
		<select class="w-100px show-drop" id ="page_limit">
			<option @if ((isset(Request()->limit_page) && Request()->limit_page == 10) || (isset($limit) && $limit ==10))
				selected
				@endif value="10">10</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 20) || (isset($limit) && $limit ==20))
				selected
				@endif value="20">20</option>
				<option  @if ((isset(Request()->limit_page) && Request()->limit_page == 50) || (isset($limit) && $limit ==50) )
					selected 
				@endif value="50">50</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 100) || (isset($limit) && $limit ==100))
					selected
				@endif value="100">100</option>
		</select>
	</td>
</tr>
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif