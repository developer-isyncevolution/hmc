@if (!empty($toothbrand))    
    @if (count($toothbrand) > 1)
        <option value="">Brand</option>
    @endif
    @foreach($toothbrand as $key => $toothbrand_val)
        <option value="{{$toothbrand_val->iToothBrandId}}"> {{$toothbrand_val->vName}}</option>
    @endforeach
@else
    <option value="">No Brand</option>
@endif