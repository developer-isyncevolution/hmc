@if (!empty($gumbrand))    
    @if (count($gumbrand) > 1)
        <option value="">Brand</option>
    @endif
    @foreach($gumbrand as $key => $gumbrand_val)
        <option value="{{$gumbrand_val->iGumBrandId}}"> {{$gumbrand_val->vName}}</option>
    @endforeach
@else
    <option value="">No Brand</option>
@endif