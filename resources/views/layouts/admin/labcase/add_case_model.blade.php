<div class="modal fade" id="add-to-slip" tabindex="-1" aria-labelledby="add-to-slipLabel" aria-hidden="true">
     <div class="modal-dialog modal-xl" style="width: 95%;max-width: 95%;">
        <div class="modal-content add-Case-model">
            <div class="modal-header p-2">
                <h5 class="modal-title" id="add-to-slipLabel"> {{isset($grades->iGradeId) ? 'Edit' : 'Add'}} Case</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0 m-0">
                <div class="main-panel add-case-listing" style="padding: 10px; margin: 0px;">
                    <div class="page-title text-center d-none">
                        <h3>
                            {{isset($grades->iGradeId) ? 'Edit' : 'Add'}} Case
                        </h3>
                    </div>
                    <div class="header-flex">

                        <div class="left-header">
                            <div class="com" style="width: 320px;">
                                <ul class="add-case-heder-ul">
                                    <li>
                                        <select name="iOfficeId" id="iOfficeId" class="w-100">
                                            <option value="">Select Office</option>
                                            @foreach($Office as $key => $office_value)
                                            <option value="{{$office_value->iOfficeId}}"> {{$office_value->vOfficeName}}</option>
                                            @endforeach
                                        </select>
                                    </li>
                                    <li>
                                        <div id="doctor_div" class="w-100">
                                            <select name="iDoctorId" id="iDoctorId" class="w-100">
                                                <option value="">Select Doctor</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li>

                                        <div id="vPatientName_div" class="w-100">
                                            <input type="text" class="form-control" id="vPatientName" name="vPatientName" placeholder="Type patient name">
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="com" style="width: 120px;">
                                <ul class="add-case-heder-ul w-auto">
                                    <li>
                                        <span class="li-title"> <strong>Pan #</strong> </span> 0001
                                    </li>
                                    <li>
                                        <span class="li-title"> <strong>Case #</strong> </span>
                                        0001
                                    </li>
                                    <li>
                                        <span class="li-title"><strong>Slip #</strong></span> 0001
                                    </li>
                                </ul>
                            </div>

                            <div class="com" style="width: 320px;">
                                <ul class="add-case-heder-ul borded-li">
                                    <li>
                                        <span class="li-title"> <strong> Created by</strong></span> Hamza Farooq
                                    </li>

                                    <li>
                                        <span>Location </span>


                                        <select name="vLocation" id="vLocation">
                                            <option value="">Draft</option>
                                            <option value="1">In office ready to pickup</option>
                                            <option value="2">On route to the lab</option>
                                            <option value="3">In Lab</option>
                                            <option value="4">lab ready to pku</option>
                                            <option value="5">On route to the office</option>
                                            <option value="6">In office</option>
                                            <option value="7">Delivered</option>
                                        </select>
                                    </li>
                                    <li>
                                        <span>Case Status</span>

                                        <select name="eStatus" id="eStatus">
                                            <option value="">Draft</option>
                                            <option value="Draft">Draft</option>
                                            <option value="On Process">On Process</option>
                                            <option value="Canceled">Canceled</option>
                                            <option value="Finished">Finished</option>
                                        </select>
                                    </li>
                                </ul>
                            </div>

                            <div class="com">
                                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{route('admin.stage.gumbrand_store')}}" id="frm_brand" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <input type="hidden" name="iOfficeId" value="">
                                                    <div class="mb-3">
                                                        <label>Brand Name</label>
                                                        <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="">
                                                    </div>
                                                    <div class="text-danger" id="vName_error">Enter Name</div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                <a type="submit" id="brand_submit" class="btn submit-btn me-2">Add</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="com" style="width:300px">
                                <ul class="add-case-heder-ul">
                                    <li>
                                        <span class="li-title"> <strong>Pick up Date</strong> </span> <span> 03/28/2022</span> <a href="#"> <i class="fad fa-calendar-alt"></i></a>

                                    </li>
                                    <li>
                                        <span class="li-title"> <strong>Delivery Date </strong> </span> <span>03/28/2022</span> <a href="#" class="rush-a" id="rush_model-open"> Rush </a>
                                    </li>
                                    <li>
                                        <span class="li-title"> <strong>Delivery Time </strong> </span> <span>---- </span><a href="#"> <i class="fad fa-alarm-clock"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>


                        <div class="right-header">
                            <ul class="add-case-heder-ul">
                                <li>
                                    <a href="#" class="p-icon">
                                        <i class="fad fa-print"></i>
                                    </a>
                                    <a href="#" class="bill-icon">
                                        <i class="fad fa-ballot-check"></i>
                                    </a>
                                </li>
                                <li></li>
                                <li>
                                    <a href="#" class="msg-icon">
                                        <i class="fad fa-comment-alt-edit"></i>
                                    </a>
                                    <a href="#" class="pic-icon">
                                        <i class="fad fa-image"></i>
                                    </a>
                                </li>

                            </ul>
                        </div>

                    </div>


                    <hr>
                    <form action="{{route('admin.labcase.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="teeth-wrapper upper">

                                    <h3 class="card-title mb-0">
                                        Upper
                                    </h3>

                                    <svg width="100%" height="100%" viewBox="0 0 1500 822" class="teeth-svg">

                                        <g>

                                            <g>
                                                <!-- upper-teeth 5 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-5-teeth" data-id="teeths3" data-name="5" x="325" y="202" width="149" height="152" xlink:href="{{asset('admin/assets/images/teeth/up-5.png')}}" class="img-for-teeth teeth_uppper"></image>


                                                    <text id="uppper-5-num" data-name="5" class="cls-1 teeth_uppper" transform="translate(405.696 287.468) scale(1.305)">
                                                        <tspan x="0">5</tspan>
                                                    </text>
                                                    <image id="uppper-5-claps" data-id="teeths3" data-name="5" x="315" y="190" xlink:href="{{asset('admin/assets/images/teeth/updc-5.png')}}" class="clapping clap-up-5 teeth_uppper upper-claps" style="display: none"></image>
                                                </a>
                                            </g>
                                            <g>
                                                <!-- upper-teeth 4 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-4-teeth" data-id="teeths4" data-name="4" x="266" y="285" width="153" height="149" xlink:href="{{asset('admin/assets/images/teeth/up-4.png')}}" class="img-for-teeth teeth_uppper"></image>

                                                    <text id="uppper-4-num" data-name="4" class="cls-1 teeth_uppper" transform="translate(346.65 357.771) scale(1.305)">
                                                        <tspan x="0">4</tspan>
                                                    </text>
                                                    <image id="uppper-4-claps" data-id="teeths4" data-name="4" x="250" y="280" xlink:href="{{asset('admin/assets/images/teeth/updc-4.png')}}" class="clapping clap-up-4 teeth_uppper upper-claps" style="display: none"></image>
                                                </a>
                                            </g>
                                            <g>
                                                <!-- upper-teeth 3 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-3-teeth" data-id="teeths7" data-name="3" x="176" y="367" width="220" height="199" xlink:href="{{asset('admin/assets/images/teeth/up-3.png')}}" class="img-for-teeth teeth_uppper"></image>

                                                    <text id="uppper-3-num" data-name="3" class="cls-1 teeth_uppper" transform="translate(289.371 467.923) scale(1.305)">
                                                        <tspan x="0">3</tspan>
                                                    </text>

                                                    <image id="uppper-3-claps" data-id="teeths7" data-name="3" x="160" y="367" xlink:href="{{asset('admin/assets/images/teeth/updc-3.png')}}" class="clapping clap-up-3 teeth_uppper upper-claps" style="display: none"></image>
                                                </a>
                                            </g>
                                            <g>
                                                <!-- upper-teeth 2 -->
                                                <a href="javascript:;" class="test">
                                                    <image id="uppper-2-teeth" data-id="teeths6" data-name="2" x="115" y="500" width="203" height="188" xlink:href="{{asset('admin/assets/images/teeth/up-2.png')}}" class="img-for-teeth teeth_uppper"></image>

                                                    <image id="uppper-2-claps" data-id="teeths6" data-name="2" x="100" y="500" xlink:href="{{asset('admin/assets/images/teeth/updc-2.png')}}" class="clapping clap-up-2 teeth_uppper upper-claps" style="display: none"></image>

                                                    <text id="uppper-2-num" data-name="2" class="cls-1 teeth_uppper" transform="translate(224.87 609.191) scale(1.305)">
                                                        <tspan x="0">2</tspan>
                                                    </text>
                                                </a>
                                            </g>

                                            <g>
                                                <!-- upper-teeth 1 -->
                                                <a href="javascript:;" class="test">

                                                    <image id="uppper-1-teeth" data-id="teeths5" data-name="1" x="57" y="640" width="192" height="156" xlink:href="{{asset('admin/assets/images/teeth/up-1.png')}}" class="img-for-teeth teeth_uppper"></image>


                                                    <text id="uppper-1-num" data-name="1" data-id="teeths5" class="cls-1 teeth_uppper" transform="translate(163.9 742.356) scale(1.305)">
                                                        <tspan x="0">1</tspan>
                                                    </text>

                                                    <image id="uppper-1-claps" data-id="teeths5" data-name="1" x="40" y="640" xlink:href="{{asset('admin/assets/images/teeth/up-dc-1.png')}}" class="clapping clap-up-1 teeth_uppper upper-claps" style="display: none"></image>

                                                </a>
                                                <!-- upper-teeth 1 end-->
                                            </g>


                                            <g>
                                                <!-- upper-teeth 6 -->
                                                <a href="javascript:;">
                                                    <image id="uppper-6-teeth" data-id="teeths2" data-name="6" x="403" y="123" width="136" height="178" xlink:href="{{asset('admin/assets/images/teeth/up-6.png')}}" class="img-for-teeth teeth_uppper"></image>
                                                    <image id="uppper-6-claps" data-id="teeths2" data-name="6" x="400" y="105" xlink:href="{{asset('admin/assets/images/teeth/updc-6.png')}}" class="clapping clap-up-6 teeth_uppper upper-claps" style="display: none"></image>

                                                    <text id="uppper-6-num" data-name="6" class="cls-1 teeth_uppper" transform="translate(476.76 240.092) scale(1.305)">
                                                        <tspan x="0">6</tspan>
                                                    </text>
                                                </a>
                                            </g>






                                            <!-- upper-teeth 7 -->
                                            <a href="javascript:;">
                                                <image id="uppper-7-teeth" data-id="teeths1" data-name="7" x="498" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/teeth/up-7.png')}}" class="img-for-teeth teeth_uppper"></image>
                                            </a>

                                            <image id="uppper-7-claps" data-id="teeths1" data-name="7" x="498" y="50" xlink:href="{{asset('admin/assets/images/teeth/updc-7.png')}}" class="clapping clap-up-7 teeth_uppper upper-claps" style="display: none"></image>

                                            <text id="uppper-7-num" data-name="7" class="cls-1 teeth_uppper" transform="translate(563.111 198.624) scale(1.305)">
                                                <tspan x="0">7</tspan>
                                            </text>

                                            <!-- upper-teeth 8 -->
                                            <a href="javascript:;">
                                                <image id="uppper-8-teeth" data-id="teeths8" data-name="8" x="607" y="35" xlink:href="{{asset('admin/assets/images/teeth/up-8.png')}}" class="img-for-teeth teeth_uppper"></image>
                                            </a>

                                            <image id="uppper-8-claps" data-id="teeths8" data-name="8" x="607" y="10" xlink:href="{{asset('admin/assets/images/teeth/updc-8.png')}}" class="clapping clap-up-7 teeth_uppper upper-claps" style="display: none"></image>

                                            <text id="uppper-8-num" data-name="8" class="cls-1 teeth_uppper" transform="translate(697.646 179.813) scale(1.305)">
                                                <tspan x="0">8</tspan>
                                            </text>

                                            <!-- upper-teeth 9 -->
                                            <a href="javascript:;">
                                                <image id="uppper-9-teeth" data-id="teeths16" data-name="9" x="755" y="35" width="150" height="210" xlink:href="{{asset('admin/assets/images/teeth/up-9.png')}}" class="img-for-teeth teeth_uppper"></image>
                                            </a>

                                            <image id="uppper-9-claps" data-id="teeths16" data-name="9" x="765" y="10" xlink:href="{{asset('admin/assets/images/teeth/updc-9.png')}}" class="clapping clap-up-9 teeth_uppper upper-claps" style="display: none"></image>

                                            <text id="uppper-9-num" data-name="9" class="cls-1 teeth_uppper" transform="translate(821.604 176.548) scale(1.305)">
                                                <tspan x="0">9</tspan>
                                            </text>

                                            <!-- upper-teeth 10 -->
                                            <a href="javascript:;">
                                                <image id="uppper-10-teeth" data-id="teeths9" data-name="10" x="889" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/teeth/up-10.png')}}" class="img-for-teeth teeth_uppper"></image>
                                            </a>

                                            <image id="uppper-10-claps" data-id="teeths9" data-name="10" x="910" y="50" xlink:href="{{asset('admin/assets/images/teeth/updc-10.png')}}" class="clapping clap-up-10 teeth_uppper upper-claps" style="display: none"></image>

                                            <text id="uppper-10-num" data-name="10" class="cls-1 teeth_uppper" transform="translate(934.785 195.164) scale(1.305)">
                                                <tspan x="0">10</tspan>
                                            </text>

                                            <!-- upper-teeth 11 -->
                                            <a href="javascript:;">
                                                <image id="uppper-11-teeth" data-id="teeths10" data-name="11" x="972" y="123" width="137" height="178" xlink:href="{{asset('admin/assets/images/teeth/up-11.png')}}" class="img-for-teeth teeth_uppper"></image>
                                            </a>

                                            <image id="uppper-11-claps" data-id="teeths10" data-name="11" x="1030" y="100" xlink:href="{{asset('admin/assets/images/teeth/updc-11.png')}}" class="clapping clap-up-11 teeth_uppper upper-claps" style="display: none"></image>

                                            <text id="uppper-11-num" data-name="11" class="cls-1 teeth_uppper" transform="translate(1031.677 240.099) scale(1.305)">
                                                <tspan x="0">11</tspan>
                                            </text>

                                            <!-- upper-teeth 12 -->
                                            <a href="javascript:;">
                                                <image id="uppper-12-teeth" data-id="teeths11" data-name="12" x="1040" y="198" width="149" height="152" xlink:href="{{asset('admin/assets/images/teeth/up-12.png')}}" class="img-for-teeth teeth_uppper"></image>
                                            </a>

                                            <image id="uppper-12-claps" data-id="teeths11" data-name="12" x="1115" y="175" xlink:href="{{asset('admin/assets/images/teeth/updc-12.png')}}" class="clapping clap-up-12 teeth_uppper upper-claps upper-claps" style="display: none"></image>

                                            <text id="uppper-12-num" data-name="12" class="cls-1 teeth_uppper" transform="translate(1103.168 282.619) scale(1.305)">
                                                <tspan x="0">12</tspan>
                                            </text>

                                            <!-- upper-teeth 13 -->
                                            <a href="javascript:;">
                                                <image id="uppper-13-teeth" data-id="teeths12" data-name="13" x="1095" y="284" width="154" height="149" xlink:href="{{asset('admin/assets/images/teeth/up-13.png')}}" class="img-for-teeth teeth_uppper"></image>
                                            </a>

                                            <image id="uppper-13-claps" data-id="teeths12" data-name="13" x="1215" y="270" xlink:href="{{asset('admin/assets/images/teeth/updc-13.png')}}" class="clapping clap-up-13 teeth_uppper upper-claps" style="display: none"></image>

                                            <text id="uppper-13-num" data-name="13" class="cls-1 teeth_uppper" transform="translate(1169.916 345.118) scale(1.305)">
                                                <tspan x="0">13</tspan>
                                            </text>
                                            <!-- upper-teeth 14 -->
                                            <a href="javascript:;">
                                                <image id="uppper-14-teeth" data-id="teeths15" data-name="14" x="1115" y="361" width="221" height="199" xlink:href="{{asset('admin/assets/images/teeth/up-14.png')}}" class="img-for-teeth teeth_uppper"></image>
                                            </a>

                                            <image id="uppper-14-claps" data-id="teeths15" data-name="14" x="1280" y="361" xlink:href="{{asset('admin/assets/images/teeth/updc-14.png')}}" class="clapping clap-up-14 teeth_uppper upper-claps" style="display: none"></image>

                                            <text id="uppper-14-num" data-name="14" class="cls-1 teeth_uppper" transform="translate(1210.87 477.036) scale(1.305)">
                                                <tspan x="0">14</tspan>
                                            </text>
                                            <!-- upper-teeth 15 -->
                                            <a href="javascript:;">
                                                <image id="uppper-15-teeth" data-id="teeths14" data-name="15" x="1194" y="503" width="203" height="188" xlink:href="{{asset('admin/assets/images/teeth/up-15.png')}}" class="img-for-teeth teeth_uppper"></image>
                                            </a>

                                            <image id="uppper-15-claps" data-id="teeths14" data-name="15" x="1350" y="503" xlink:href="{{asset('admin/assets/images/teeth/updc-15.png')}}" class="clapping clap-up-15 teeth_uppper upper-claps" style="display: none"></image>

                                            <text id="uppper-15-nuum" data-name="15" class="cls-1 teeth_uppper" transform="translate(1303.838 603.41) scale(1.305)">
                                                <tspan x="0">15</tspan>
                                            </text>

                                            <!-- upper-teeth 16 -->
                                            <a href="javascript:;">
                                                <image id="uppper-16-teeth" data-id="teeths13" data-name="16" x="1262" y="643" width="192" height="156" xlink:href="{{asset('admin/assets/images/teeth/up-16.png')}}" class="img-for-teeth teeth_uppper"></image>
                                            </a>

                                            <image id="uppper-16-claps" data-id="teeths13" data-name="16" x="1422" y="643" xlink:href="{{asset('admin/assets/images/teeth/updc-16.png')}}" class="clapping clap-up-16 teeth_uppper upper-claps" style="display: none"></image>

                                            <text id="uppper-16-num" data-name="16" class="cls-1 teeth_uppper" transform="translate(1370.808 743.76) scale(1.305)">
                                                <tspan x="0">16</tspan>
                                            </text>

                                            <!--upper teeth numbers  -->
                                        </g>
                                    </svg>

                                    <div class="d-block text-center teeth-text">
                                        <a href="javascript:;" class="add-btn p-2 mx-auto uppper_mising_all" id="uppper_mising_all">
                                            Missing all teeth
                                        </a>
                                    </div>
                                    <div class="teeths-wrapper second-row p-0">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12 col-lg-12">
                                                <div class="row g-5">
                                                    <a href="javascript:;">
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label te-in-month lbl_btn_all" value="" id="upper_in_mouth"> Teeth in mouth <br> <span id="upper_in_mouth_lbl"></span></label>
                                                                <input type="hidden" id="upper_in_mouth_value" value="">
                                                            </div>
                                                        </div>
                                                    </a>

                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <label class="form-control inner-label missing-te lbl_btn_all" value="" id="upper_missing_teeth">missing teeth <br> <span id="upper_missing_teeth_lbl"></span></label>
                                                            <input type="hidden" id="upper_missing_teeth_value" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <label class="form-control inner-label ex-dekivery lbl_btn_all" value="" id="upper_ectract_delivery">will extract on delivery <br><span id="upper_ectract_delivery_lbl"></span></label>
                                                            <input type="hidden" id="upper_ectract_delivery_value" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <label class="form-control inner-label ben-extracted lbl_btn_all" value="" id="upper_been_extracted"> has been extracted <br><span id="upper_been_extracted_lbl"></span></label>
                                                            <input type="hidden" id="upper_been_extracted_value" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <label class="form-control inner-label fix-add lbl_btn_all" value="" id="upper_fix"> fix or add <br> <span id="upper_fix_lbl"></span></label>
                                                            <input type="hidden" id="upper_fix_value" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <label class="form-control inner-label clasps lbl_btn_all" value="" id="upper_clasps">clasps<br> <span id="upper_clasps_lbl"></span></label>
                                                            <input type="hidden" id="upper_clasps_value" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h3 class="card-title mb-0">
                                            CASE DESIGN
                                        </h3>
                                    </div>

                                    <div class="col-lg-12 mb-3" id="product_button_div">
                                        <div class="row">
                                            <div class="col-lg-5 text-center">
                                                <a href="javascript:;" class="btn add-btn me-2" id="product_upper_button_div"> Add Upper Product </a>
                                            </div>
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-5 text-center">
                                                <a href="javascript:;" class="btn add-btn me-2" id="product_lower_button_div"> Add Lower Product </a>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row" id="category_div">
                                        <div class="col-lg-5">
                                            <div id="category_upper_div">
                                                <select name="iUpperCategoryId" id="iUpperCategoryId">
                                                    <option value="">Select category</option>
                                                    @foreach($Upper_category as $key => $value)
                                                    <option value="{{$value->iCategoryId}}"> {{$value->vName}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 align-self-center text-center">
                                            <label id="cat_lbl"> <strong> Category</strong> </label>
                                        </div>

                                        <div class="col-lg-5">
                                            <div id="category_lower_div">
                                                <select name="iLowerCategoryId" id="iLowerCategoryId">
                                                    <option value="">Select category</option>
                                                    @foreach($Lower_category as $key => $value)
                                                    <option value="{{$value->iCategoryId}}"> {{$value->vName}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div id="product_div" class="w-100">
                                                <div id="product_upper_div">
                                                    <select name="iUpperProductId" id="iUpperProductId" class="w-100">
                                                        <option value="">Select Product</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 align-self-center text-center">
                                            <label id="product_lbl"> <strong>Product</strong> </label>
                                        </div>
                                        <div class="col-lg-5">
                                            <div id="product_lower_div">
                                                <select name="iLowerProductId" id="iLowerProductId" class="w-100">
                                                    <option value="">Select Product</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div id="stage_div" class="w-100">
                                                <div id="stage_upper_div" class="w-100">
                                                    <select name="iUpperStageId" id="iUpperStageId" class="w-100">
                                                        <option value="">Select Stage</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 align-self-center text-center">
                                            <label id="stage_lbl"> <strong> Stage</strong> </label>
                                        </div>
                                        <div class="col-lg-5">
                                            <div id="stage_lower_div">
                                                <select name="iLowerStageId" id="iLowerStageId" class="w-100">
                                                    <option value="">Select Stage</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div id="grade_div" class="w-100">
                                                <div id="grade_upper_div">
                                                    <select name="iUpperGradeId" id="iUpperGradeId" class="w-100">
                                                        <option value="">Select Grade</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 align-self-center text-center">
                                            <label id="grade_lbl"> <strong> Grade</strong> </label>
                                        </div>
                                        <div class="col-lg-5">
                                            <div id="grade_lower_div">
                                                <select name="iLowerGradeId" id="iLowerGradeId" class="w-100">
                                                    <option value="">Select Grade</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div id="impression_div">
                                                <div id="upper_impression_div" class="w-100">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <select name="upper_impression" id="upper_impression">
                                                                <option value="">Select IMPRESSIONS</option>
                                                                <option value="ALGINATE">ALGINATE</option>
                                                                <option value="PVS">PVS</option>
                                                                <option value="RELINED">RELINED</option>
                                                                <option value="PICK UP">PICK UP</option>
                                                                <option value="RELINED & PICKUP">RELINED & PICKUP</option>
                                                                <option value="Other Impression">Other Impression</option>
                                                                <option value="Digital file">Digital file</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-5 align-self-center">
                                                            <input type="text" placeholder="Qty 1" class="w-100 h-100 qty-input">
                                                        </div>
                                                        <div class="col-lg-1 align-self-center">
                                                            <a href="#" class="btn-icon  edit_permission edit-icon">
                                                                <i class="fad fa-plus"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 align-self-center text-center">
                                            <label id="impression_lbl"> <strong> Impressions</strong> </label>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="row">
                                                <div class="col-lg-1 align-self-center">
                                                    <a href="#" class="btn-icon  edit_permission edit-icon">
                                                        <i class="fad fa-plus"></i>
                                                    </a>
                                                </div>
                                                <div class="col-lg-5 align-self-center">
                                                    <input type="text" placeholder="Qty 1" class="w-100 h-100 qty-input">
                                                </div>
                                                <div class="col-lg-6">
                                                    <div id="lower_impression_div" class="w-100">
                                                        <select name="lower_impression" id="lower_impression">
                                                            <option value="">Select IMPRESSIONS</option>
                                                            <option value="ALGINATE">ALGINATE</option>
                                                            <option value="PVS">PVS</option>
                                                            <option value="RELINED">RELINED</option>
                                                            <option value="PICK UP">PICK UP</option>
                                                            <option value="RELINED & PICKUP">RELINED & PICKUP</option>
                                                            <option value="Other Impression">Other Impression</option>
                                                            <option value="Digital file">Digital file</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5">

                                            <div id="teeth_shades_div" class="w-100">
                                                <div id="upper_tsbrand_div" class="w-100">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <select name="upper_tsbrand" id="upper_tsbrand">
                                                                <option value="">Select brand</option>
                                                                @foreach($toothbrand as $key => $value)
                                                                <option value="{{$value->iToothBrandId}}"> {{$value->vName}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <select name="upper_tshade" id="upper_tshade">
                                                                <option value="">Select shade</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-1 align-self-center">
                                                            <a href="#" class="btn-icon edit_permission edit-icon ">
                                                                <i class="fad fa-pencil"></i>
                                                            </a>
                                                        </div>
                                                        <!-- <div class="col-lg-1 align-self-center">
                                                            <a href="#" class="btn-icon  delete_permission delete-icon">
                                                                <i class="fad fa-trash-alt"></i>
                                                            </a>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 align-self-center text-center">
                                            <label id="teethshade_lbl"> <strong>Teeth shade </strong></label>
                                        </div>
                                        <div class="col-lg-5">
                                            <div id="lower_tsbrand_div" class="w-100">
                                                <div class="row">
                                                    <div class="col-lg-1 align-self-center">
                                                        <a href="#" class="btn-icon edit_permission edit-icon ">
                                                            <i class="fad fa-pencil"></i>
                                                        </a>
                                                    </div>
                                                    <!-- <div class="col-lg-1 align-self-center">
                                                        <a href="#" class="btn-icon  delete_permission delete-icon">
                                                            <i class="fad fa-trash-alt"></i>
                                                        </a>
                                                    </div> -->
                                                    <div class="col-lg-5">
                                                        <select name="lower_tsbrand" id="lower_tsbrand">
                                                            <option value="">Select brand</option>
                                                            @foreach($toothbrand as $key => $value)
                                                            <option value="{{$value->iToothBrandId}}"> {{$value->vName}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <select name="lower_tshade" id="lower_tshade">
                                                            <option value="">Select shade</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div id="gum_shades_div" class="w-100">
                                                <div id="upper_gsbrand_div">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <select name="upper_gsbrand" id="upper_gsbrand">
                                                                <option value="">Select brand</option>
                                                                @foreach($gumbrand as $key => $value)
                                                                <option value="{{$value->iGumBrandId}}"> {{$value->vName}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <select name="upper_gshade" id="upper_gshade">
                                                                <option value="">Select shade</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-1 align-self-center">
                                                            <a href="#" class="btn-icon edit_permission edit-icon ">
                                                                <i class="fad fa-pencil"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 align-self-center text-center">
                                            <label id="gumshade_lbl"> <strong> Gum shade</strong> </label>
                                        </div>
                                        <div class="col-lg-5 align-self-center">
                                            <div id="lower_gsbrand_div">
                                                <div class="row">
                                                    <div class="col-lg-1 align-self-center">
                                                        <a href="#" class="btn-icon edit_permission edit-icon ">
                                                            <i class="fad fa-pencil"></i>
                                                        </a>
                                                    </div>
                                                    <div class="col-lg-5">
                                                        <select name="lower_gsbrand" id="lower_gsbrand-17">
                                                            <option value="">Select brand</option>
                                                            @foreach($gumbrand as $key => $value)
                                                            <option value="{{$value->iGumBrandId}}"> {{$value->vName}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <select name="lower_gshade" id="lower_gshade">
                                                            <option value="">Select shade</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row mb-3 mt-2" id="category_addons_div">
                                        <div class="col-lg-5 text-center">
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    <select name="lower_gsbrand" id="lower_gsbrand-18">
                                                        <option value="">Select brand</option>
                                                        @foreach($gumbrand as $key => $value)
                                                        <option value="{{$value->iGumBrandId}}"> {{$value->vName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="text" placeholder="Qty 1" class="w-100 h-100 qty-input">
                                                </div>
                                                <div class="col-lg-1 align-self-center">
                                                    <a href="#" class="btn-icon edit_permission edit-icon ">
                                                        <i class="fad fa-pencil"></i>
                                                    </a>
                                                </div>
                                                <div class="col-lg-1 align-self-center">
                                                    <a href="#" class="btn-icon  delete_permission delete-icon">
                                                        <i class="fad fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center align-self-center">
                                            <label> <strong>Add ons</strong></label>
                                        </div>
                                        <div class="col-lg-5 text-center">
                                            <div class="row">

                                                <div class="col-lg-1  align-self-center">
                                                    <a href="#" class="btn-icon  delete_permission delete-icon">
                                                        <i class="fad fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                                <div class="col-lg-1 align-self-center">
                                                    <a href="#" class="btn-icon edit_permission edit-icon ">
                                                        <i class="fad fa-pencil"></i>
                                                    </a>
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="text" placeholder="Qty 1" class="w-100 h-100 qty-input">
                                                </div>
                                                <div class="col-lg-7">
                                                    <select name="lower_gsbrand" id="lower_gsbrand-11">
                                                        <option value="">Select brand</option>
                                                        @foreach($gumbrand as $key => $value)
                                                        <option value="{{$value->iGumBrandId}}"> {{$value->vName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-3 mt-2" id="category_addons_div">
                                        <div class="col-lg-5 text-center">
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    <select name="lower_gsbrand" id="lower_gsbrand-12">
                                                        <option value="">Select brand</option>
                                                        @foreach($gumbrand as $key => $value)
                                                        <option value="{{$value->iGumBrandId}}"> {{$value->vName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="text" placeholder="Qty 1" class="w-100 h-100 qty-input">
                                                </div>
                                                <div class="col-lg-1 align-self-center">
                                                    <a href="#" class="btn-icon edit_permission edit-icon ">
                                                        <i class="fad fa-pencil"></i>
                                                    </a>
                                                </div>
                                                <div class="col-lg-1 align-self-center">
                                                    <a href="#" class="btn-icon  delete_permission delete-icon">
                                                        <i class="fad fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center align-self-center">
                                        </div>
                                        <div class="col-lg-5 text-center">
                                            <div class="row">

                                                <div class="col-lg-1 align-self-center">
                                                    <a href="#" class="btn-icon  delete_permission delete-icon">
                                                        <i class="fad fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                                <div class="col-lg-1 align-self-center">
                                                    <a href="#" class="btn-icon edit_permission edit-icon ">
                                                        <i class="fad fa-pencil"></i>
                                                    </a>
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="text" placeholder="Qty 1" class="w-100 h-100 qty-input">
                                                </div>
                                                <div class="col-lg-7">
                                                    <select name="lower_gsbrand" id="lower_gsbrand">
                                                        <option value="">Select brand</option>
                                                        @foreach($gumbrand as $key => $value)
                                                        <option value="{{$value->iGumBrandId}}"> {{$value->vName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>


                                            </div>
                                        </div>
                                    </div>


                                    <div class="row mb-3 mt-2" id="category_addons_div">
                                        <div class="col-lg-5 text-center">
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    <select name="lower_gsbrand" id="lower_gsbrand-13">
                                                        <option value="">Select brand</option>
                                                        @foreach($gumbrand as $key => $value)
                                                        <option value="{{$value->iGumBrandId}}"> {{$value->vName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="text" placeholder="Qty 1" class="w-100 h-100 qty-input">
                                                </div>
                                                <div class="col-lg-1 align-self-center">
                                                    <a href="#" class="btn-icon edit_permission edit-icon ">
                                                        <i class="fad fa-pencil"></i>
                                                    </a>
                                                </div>
                                                <div class="col-lg-1 align-self-center">
                                                    <a href="#" class="btn-icon  delete_permission delete-icon">
                                                        <i class="fad fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center align-self-center">
                                        </div>
                                        <div class="col-lg-5 text-center">
                                            <div class="row">
                                                <div class="col-lg-1">
                                                    <a href="#" class="btn-icon  delete_permission delete-icon">
                                                        <i class="fad fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                                <div class="col-lg-1">
                                                    <a href="#" class="btn-icon edit_permission edit-icon ">
                                                        <i class="fad fa-pencil"></i>
                                                    </a>
                                                </div>

                                                <div class="col-lg-3">
                                                    <input type="text" placeholder="Qty 1" class="w-100 h-100 qty-input">
                                                </div>
                                                <div class="col-lg-7">
                                                    <select name="lower_gsbrand" id="lower_gsbrand-14">
                                                        <option value="">Select brand</option>
                                                        @foreach($gumbrand as $key => $value)
                                                        <option value="{{$value->iGumBrandId}}"> {{$value->vName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>


                                            </div>
                                        </div>
                                    </div>


                                    <div class="row mb-3 mt-2" id="category_addons_div">
                                        <div class="col-lg-5 text-center">
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    <select name="lower_gsbrand" id="lower_gsbrand-15">
                                                        <option value="">Select brand</option>
                                                        @foreach($gumbrand as $key => $value)
                                                        <option value="{{$value->iGumBrandId}}"> {{$value->vName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="text" placeholder="Qty 1" class="w-100 h-100 qty-input">
                                                </div>
                                                <div class="col-lg-1">
                                                    <a href="#" class="btn-icon  edit_permission edit-icon">
                                                        <i class="fad fa-pencil"></i>
                                                    </a>
                                                </div>
                                                <div class="col-lg-1">
                                                    <a href="#" class="btn-icon  delete_permission delete-icon">
                                                        <i class="fad fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center align-self-center">

                                        </div>
                                        <div class="col-lg-5 text-center">
                                            <div class="row">
                                                <div class="col-lg-1 align-self-center">
                                                    <a href="#" class="btn-icon  delete_permission delete-icon">
                                                        <i class="fad fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                                <div class="col-lg-1 align-self-center">
                                                    <a href="#" class="btn-icon edit_permission edit-icon ">
                                                        <i class="fad fa-pencil"></i>
                                                    </a>
                                                </div>

                                                <div class="col-lg-3">
                                                    <input type="text" placeholder="Qty 1" class="w-100 h-100 qty-input">
                                                </div>
                                                <div class="col-lg-7">
                                                    <select name="lower_gsbrand" id="lower_gsbrand-16">
                                                        <option value="">Select brand</option>
                                                        @foreach($gumbrand as $key => $value)
                                                        <option value="{{$value->iGumBrandId}}"> {{$value->vName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>


                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div id="upper_status_div" class="w-100">
                                                <select name="upper_status" id="upper_status" class="w-100">
                                                    <option value="">Draft</option>
                                                    <option value="Draft">Draft</option>
                                                    <option value="On Process">On Process</option>
                                                    <option value="Canceled">Canceled</option>
                                                    <option value="Finished">Finished</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 align-self-center text-center">
                                            <label id="status_lbl"> <strong> stage status </strong> </label>
                                        </div>
                                        <div class="col-lg-5">
                                            <div id="lower_status_div" class="w-100">
                                                <select name="lower_status" id="lower_status">
                                                    <option value="">Draft</option>
                                                    <option value="Draft">Draft</option>
                                                    <option value="On Process">On Process</option>
                                                    <option value="Canceled">Canceled</option>
                                                    <option value="Finished">Finished</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row mb-3 mt-2" id="category_addons_div">
                                        <div class="col-lg-5 text-center">
                                            <div id="upper_addons_div">
                                                <a id="upper_addons" value="upper_addons" class="btn add-btn me-2">Select Add Ons</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">

                                        </div>
                                        <div class="col-lg-5 text-center">
                                            <div id="lower_addons_div">
                                                <a id="lower_addons" value="lower_addons" class="btn add-btn me-2">Select Add Ons</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-1 mt-2" id="category_addons_div">
                                        <div class="col-lg-5 text-center">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <span>
                                                        <strong>Delivery Date</strong>
                                                    </span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <span> 03/28/2022</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <label> <strong>Delivery Date</strong></label>
                                        </div>
                                        <div class="col-lg-5 text-center">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <span>
                                                        <strong>Delivery Date</strong>
                                                    </span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <span> 03/28/2022</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="modal fade" id="upper_model" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Upper category addons</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{route('admin.stage.gumbrand_store')}}" id="frm_brand" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="mb-3">
                                                            <select name="upper_category_addons" id="upper_category_addons" name="upper_category_addons" value="">
                                                                <option value="">Select category addons</option>
                                                                @foreach($Upper_category_addons as $key => $value)
                                                                <option value="{{$value->iAddCategoryId}}"> {{$value->vCategoryName}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="mb-3">
                                                            <select name="upper_subaddons" id="upper_subaddons" name="upper_subaddons">
                                                                <option value="">Select Addons</option>
                                                            </select>
                                                        </div>
                                                        <div class="mb-3">
                                                            <input type="number" id="iUpperQty" value="" name="iUpperQty">
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <a type="a" class="btn btn-secondary" data-bs-dismiss="modal">Close</a>
                                                    <a type="submit" id="upper_submit" class="btn submit-btn me-2">Add</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="lower_model" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Lower category addons</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{route('admin.stage.gumbrand_store')}}" id="frm_brand" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="mb-3">
                                                            <select name="lower_category_addons" id="lower_category_addons">
                                                                <option value="">Select category addons</option>
                                                                @foreach($Lower_category_addons as $key => $value)
                                                                <option value="{{$value->iAddCategoryId}}"> {{$value->vCategoryName}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="mb-3">
                                                            <select name="lower_subaddons" id="lower_subaddons">
                                                                <option value="">Select addons</option>
                                                            </select>
                                                        </div>
                                                        <div class="mb-3">
                                                            <input type="number" id="iLowerQty" value="">
                                                        </div>

                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <a type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</a>
                                                    <a type="submit" id="lower_submit" class="btn submit-btn me-2">Add</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="modal fade" id="rush_model" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-xl modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Please select date desired to rush case.</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <p class="rush-a text-center">
                                                                Rush
                                                            </p>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <h3 class="card-title mb-0">
                                                                Upper
                                                            </h3>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <h3 class="card-title mb-0">
                                                                Lower
                                                                
                                                            </h3>
                                                        </div>
                                                    </div>
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <ul class="add-case-heder-ul borded-li">
                                                                <li style="color: red;">
                                                                        DAY UNAVIAILABLE
                                                                    </li>
                                                                    <li>

                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                                DAY 2 = 80%
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                                    <li>

                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                            DAY 3 = 50%
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                                    <li>

                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                            DAY 4 = 20%
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                                    <li>

                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                            DAY 5 = 00%
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <ul class="add-case-heder-ul borded-li">
                                                                    <li>
                                                                        monday 03/31/2022
                                                                    </li>
                                                                    <li>
                                                                        Tuesday 02/31/2022
                                                                    </li>
                                                                    <li>
                                                                        Wednesday 03/31/2022
                                                                    </li>
                                                                    <li>
                                                                        Thursday 02/31/2022
                                                                    </li>
                                                                    <li>
                                                                        monday 03/31/2022
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <ul class="add-case-heder-ul borded-li">
                                                                    <li>
                                                                        monday 03/31/2022
                                                                    </li>
                                                                    <li>
                                                                        Tuesday 02/31/2022
                                                                    </li>
                                                                    <li>
                                                                        Wednesday 03/31/2022
                                                                    </li>
                                                                    <li>
                                                                        Thursday 02/31/2022
                                                                    </li>
                                                                    <li>
                                                                        monday 03/31/2022
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <ul class="add-case-heder-ul borded-li">
                                                                    <li style="color: red;">
                                                                        DAY UNAVIAILABLE
                                                                    </li>
                                                                    <li>

                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                                DAY 2 = 80%
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                                    <li>

                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                            DAY 3 = 50%
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                                    <li>

                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                            DAY 4 = 20%
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                                    <li>

                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                            DAY 5 = 00%
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <div class="add-case-bottom w-100">
                                                        <div>
                                                            <div class="d-flex justify-content-center align-items-center">
                                                                <i class="fas fa-exclamation-triangle"></i>
                                                                <label>
                                                                    By clicking this box you acknowledge all informations is correct
                                                                </label>
                                                                <input class="ms-5 c-checkbox" type="checkbox" value="">
                                                            </div>
                                                        </div>

                                                        <a href="javascript:;" class="btn add-btn"> Submit slip </a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>


                            <div class="col-lg-3">
                                <div class="teeth-wrapper lower">
                                    <h3 class="card-title mb-0">
                                        Lower
                                    </h3>
                                    <svg width="100%" height="100%" viewBox="0 0 1550 700" class="teeth-svg">

                                        <g>
                                            <g></g>
                                            <!-- lower-teeth 1 -->
                                            <a href="javascript:;">
                                                <image id="lower-17-teeth" data-id="teeth15" data-name="17" x="30" y="555" width="252" height="200" xlink:href="{{asset('admin/assets/images/teeth/d-17.png')}}" class="img-for-teeth  teeth_lower"></image>
                                            </a>

                                            <!--  image with cliap-1 -->
                                            <image id="lower-17-claps" data-name="17" x="15" y="540" width="80" height="190" xlink:href="{{asset('admin/assets/images/teeth/dc-17.png')}}" class="clapping clap-17 teeth_lower lower-claps" style="display: none"></image>

                                            <text id="lower-17-num" data-name="17" class="cls-1 teeth_lower" transform="translate(196.632 625.98) scale(1.199)">
                                                <tspan x="-30">17</tspan>
                                            </text>


                                            <!-- lower-teeth 1 end-->

                                            <g></g>
                                            <!-- lower-teeth 2 -->
                                            <a href="javascript:;">
                                                <image id="lower-18-teeth" data-id="teeth7" data-name="18" x="95" y="400" width="232" height="194" xlink:href="{{asset('admin/assets/images/teeth/d-18.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>
                                            <!--  image with cliap-2 -->
                                            <image id="lower-18-claps" data-name="18" x="80" y="382" width="85" height="194" xlink:href="{{asset('admin/assets/images/teeth/dc-18.png')}}" class="clapping clap-18 teeth_lower lower-claps" style="display: none"></image>

                                            <text id="lower-18-num" data-name="18" class="cls-1 teeth_lower" transform="translate(263.156 536.61) scale(1.199)">
                                                <tspan x="-35" y="-30">18</tspan>
                                            </text>

                                            <!-- lower-teeth 2 end-->

                                            <g></g>
                                            <!-- lower-teeth 3 -->
                                            <a href="javascript:;">
                                                <image id="lower-19-teeth" data-id="teeth6" data-name="19" x="165" y="290" width="235" height="191" xlink:href="{{asset('admin/assets/images/teeth/d-19.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>

                                            <text id="lower-19-num" data-name="19" class="cls-1 teeth_lower" transform="translate(329.927 426.297) scale(1.199)">
                                                <tspan x="-35" y="-35">19</tspan>
                                            </text>

                                            <!--  image with cliap-3 -->
                                            <image id="lower-19-claps" data-name="19" x="155" y="255" width="100" height="191" xlink:href="{{asset('admin/assets/images/teeth/dc-19.png')}}" class="clapping clap-19 teeth_lower lower-claps" style="display: none"></image>

                                            <!-- lower-teeth 3 end-->


                                            <g></g>
                                            <!-- lower-teeth 4 start-->
                                            <a href="javascript:;">
                                                <image id="lower-20-teeth" data-id="teeth5" data-name="20" x="265" y="225" width="167" height="159" xlink:href="{{asset('admin/assets/images/teeth/d-20.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>

                                            <text id="lower-20-num" data-name="20" class="cls-1 teeth_lower" transform="translate(401.061 332.076) scale(1.199)">
                                                <tspan x="-30" y="-35">20</tspan>
                                            </text>

                                            <!--  image with cliap-4 -->
                                            <image id="lower-20-claps" data-name="20" x="240" y="175" width="100" height="159" xlink:href="{{asset('admin/assets/images/teeth/dc-20.png')}}" class="clapping clap-20 teeth_lower lower-claps" style="display: none"></image>

                                            <!-- lower-teeth 4 end-->

                                            <g></g>
                                            <!-- lower-teeth 5 -->
                                            <a href="javascript:;">
                                                <image id="lower-21-teeth" data-id="teeth3" data-name="21" x="342" y="153" width="150" height="177" xlink:href="{{asset('admin/assets/images/teeth/d-21.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>
                                            <!--  image with cliap-5 -->
                                            <image id="lower-21-claps" data-name="21" x="300" y="135" width="140" height="90" xlink:href="{{asset('admin/assets/images/teeth/dc-21.png')}}" class="clapping clap-21 teeth_lower lower-claps" style="display: none"></image>
                                            <text id="lower-21-num" data-name="21" class="cls-1 teeth_lower" transform="translate(455.319 283.973) scale(1.199)">
                                                <tspan x="-30" y="-50">21</tspan>
                                            </text>

                                            <g></g>
                                            <!-- lower-teeth 6 -->
                                            <a href="javascript:;">
                                                <image id="lower-22-teeth" data-id="teeth4" data-name="22" x="435" y="93" width="135" height="200" xlink:href="{{asset('admin/assets/images/teeth/d-22.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>

                                            <!--  image with cliap-22 -->
                                            <image id="lower-22-claps" data-name="22" x="430" y="15" width="100" height="200" xlink:href="{{asset('admin/assets/images/teeth/dc-22.png')}}" class="clapping clap-22 teeth_lower lower-claps" style="display: none"></image>
                                            <text id="lower-22-num" data-name="22" class="cls-1 teeth_lower" transform="translate(517.12 257.438) scale(1.199)">
                                                <tspan x="0" y="-50">22</tspan>
                                            </text>

                                            <g></g>
                                            <!-- lower-teeth 7 -->
                                            <a href="javascript:;">
                                                <image id="lower-23-teeth" data-id="teeth2" data-name="23" x="540" y="60" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/d-23.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>
                                            <!-- image with cliap-23 -->
                                            <image id="lower-23-claps" data-name="23" x="525" y="-33" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/dc-23.png')}}" class="clapping clap-23 teeth_lower lower-claps" style="display: none"></image>
                                            <text id="lower-23-num" data-name="23" class="cls-1 teeth_lower" transform="translate(602.467 207.332) scale(1.199)">
                                                <tspan x="0" y="-50">23</tspan>
                                            </text>


                                            <g></g>

                                            <!-- lower-teeth 8 -->
                                            <a href="javascript:;">
                                                <image id="lower-24-teeth" data-id="teeth1" data-name="24" x="647" y="40" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/d-24.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>
                                            <!-- image with cliap-8 -->
                                            <image id="lower-24-claps" data-name="24" x="647" y="-60" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/dc-24.png')}}" class="clapping clap-24 teeth_lower lower-claps" style="display: none"></image>
                                            <text id="lower-24-num" data-name="24" class="cls-1 teeth_lower" transform="translate(703.596 190.046) scale(1.199)">
                                                <tspan x="0" y="-40">24</tspan>
                                            </text>

                                            <g></g>
                                            <!-- lower-teeth 9 -->
                                            <a href="javascript:;">
                                                <image id="lower-25-teeth" data-id="teeth8" data-name="25" x="760" y="42" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/d-25.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>
                                            <!--  image with cliap-9 -->
                                            <image id="lower-25-claps" data-name="25" x="762" y="-58" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/dc-25.png')}}" class="clapping clap-25 teeth_lower lower-claps" style="display: none"></image>
                                            <text id="lower-25-num" data-name="25" class="cls-1 teeth_lower" transform="translate(789.005 190.046) scale(1.199)">
                                                <tspan x="10" y="-50">25</tspan>
                                            </text>
                                            <g></g>
                                            <!-- lower-teeth 10 -->
                                            <a href="javascript:;">
                                                <image id="lower-26-teeth" data-id="teeth9" data-name="26" x="865" y="54" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/d-26.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>
                                            <!--  image with cliap-11 -->
                                            <image id="lower-26-claps" data-name="26" x="880" y="-35" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/dc-26.png')}}" class="clapping clap-26 teeth_lower lower-claps" style="display: none"></image>
                                            <text id="lower-26-num" data-name="26" class="cls-1 teeth_lower" transform="translate(881.008 207.152) scale(1.199)">
                                                <tspan x="25" y="-50">26</tspan>
                                            </text>
                                            <!-- lower-teeth 11-->
                                            <a href="javascript:;">
                                                <image id="lower-27-teeth" data-id="teeth11" data-name="27" x="955" y="80" width="135" height="200" xlink:href="{{asset('admin/assets/images/teeth/d-27.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>
                                            <!--  image with cliap-11 -->
                                            <image id="lower-27-claps" data-name="27" x="990" y="-5" width="110" height="200" xlink:href="{{asset('admin/assets/images/teeth/dc-27.png')}}" class="clapping clap-27 teeth_lower lower-claps" style="display: none"></image>
                                            <text id="lower-27-num" data-name="27" class="cls-1 teeth_lower" transform="translate(982.792 240.944) scale(1.199)">
                                                <tspan x="25" y="-50">27</tspan>
                                            </text>
                                            <!-- lower-teeth 12 -->
                                            <a href="javascript:;">
                                                <image id="lower-28-teeth" data-id="teeth10" data-name="28" x="1035" y="134" width="150" height="177" xlink:href="{{asset('admin/assets/images/teeth/d-28.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>
                                            <!--  image with cliap-12 -->
                                            <image id="lower-28-claps" data-name="28" x="1100" y="60" width="100" height="177" xlink:href="{{asset('admin/assets/images/teeth/dc-28.png')}}" class="clapping clap-28 teeth_lower lower-claps" style="display: none"></image>
                                            <text id="lower-28-num" data-name="28" class="cls-1 teeth_lower" transform="translate(1037.236 283.518) scale(1.199)">
                                                <tspan x="40" y="-50">28</tspan>
                                            </text>
                                            <!-- lower-teeth 13 -->
                                            <a href="javascript:;">
                                                <image id="lower-29-teeth" data-id="teeth12" data-name="29" x="1098" y="205" width="167" height="159" xlink:href="{{asset('admin/assets/images/teeth/d-29.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>
                                            <!--  image with cliap-13 -->
                                            <image id="lower-29-claps" data-name="29" x="1190" y="155" width="100" height="159" xlink:href="{{asset('admin/assets/images/teeth/dc-29.png')}}" class="clapping clap-29 teeth_lower lower-claps" style="display: none"></image>
                                            <text id="lower-29-num" data-name="29" class="cls-1 teeth_lower" transform="translate(1097.072 329.95) scale(1.199)">
                                                <tspan x="45" y="-50">29</tspan>
                                            </text>
                                            <!-- lower-teeth 14 -->
                                            <a href="javascript:;">
                                                <image id="lower-30-teeth" data-id="teeth13" data-name="30" x="1130" y="278" width="234" height="191" xlink:href="{{asset('admin/assets/images/teeth/d-30.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>
                                            <!--  image with cliap-14 -->
                                            <image id="lower-30-claps" data-name="30" x="1280" y="258" width="100" height="191" xlink:href="{{asset('admin/assets/images/teeth/dc-30.png')}}" class="clapping clap-30 teeth_lower lower-claps" style="display: none"></image>
                                            <text id="lower-30-num" data-name="30" class="cls-1 teeth_lower" transform="translate(1164.707 409.172) scale(1.199)">
                                                <tspan x="45" y="-45">30</tspan>
                                            </text>
                                            <!-- lower-teeth 15 -->

                                            <a href="javascript:;">
                                                <image id="lower-31-teeth" data-id="teeth14" data-name="31" x="1210" y="395" width="232" height="194" xlink:href="{{asset('admin/assets/images/teeth/d-31.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>
                                            <!--  image with cliap-15 -->
                                            <image id="lower-31-claps" data-name="31" x="1390" y="420" width="80" height="130" xlink:href="{{asset('admin/assets/images/teeth/dc-30.png')}}" class="clapping clap-31 teeth_lower lower-claps" style="display: none"></image>
                                            <text id="lower-31-num" data-name="31" class="cls-1 teeth_lower" transform="translate(1238.134 502.8) scale(1.199)">
                                                <tspan x="50">31</tspan>
                                            </text>
                                            <!-- lower-teeth 16 -->

                                            <a href="javascript:;">
                                                <image id="lower-32-teeth" data-id="teeth16" data-name="32" x="1273" y="560" width="232" height="193" xlink:href="{{asset('admin/assets/images/teeth/d-32.png')}}" class="img-for-teeth teeth_lower"></image>
                                            </a>
                                            <!--  image with cliap-32 -->
                                            <image id="lower-32-claps" data-name="32" x="1440" y="580" width="114" height="92" xlink:href="{{asset('admin/assets/images/teeth/dc-32.png')}}" class="clapping clap-32 teeth_lower lower-claps" style="display: none"></image>
                                            <text id="lower-32-num" data-name="32" class="cls-1 teeth_lower" transform="translate(1268.177 618.27) scale(1.199)">
                                                <tspan x="80" y="20">32</tspan>
                                            </text>

                                            <!--lower teeth numbers ------------------------------ -->

                                        </g>

                                    </svg>

                                    <div class="d-block text-center teeth-text">
                                        <a href="javascript:;" class="add-btn p-2 mx-auto lower_mising_all">
                                            Missing all teeth
                                        </a>
                                    </div>
                                    <div class="teeths-wrapper second-row p-0">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-12 col-lg-12">
                                                <div class="row g-5">
                                                    <a href="javascript:;">
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <label class="form-control inner-label te-in-month lower_lbl_btn_all" value="" id="lower_in_mouth"> Teeth in mouth <br> <span id="lower_in_mouth_lbl"></span></label>
                                                                <input type="hidden" id="lower_in_mouth_value" value="">
                                                            </div>
                                                        </div>
                                                    </a>

                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <label class="form-control inner-label missing-te lower_lbl_btn_all" value="" id="lower_missing_teeth">missing teeth <br> <span id="lower_missing_teeth_lbl"></span></label>
                                                            <input type="hidden" id="lower_missing_teeth_value" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <label class="form-control inner-label ex-dekivery lower_lbl_btn_all" value="" id="lower_ectract_delivery">will extract on delivery <br><span id="lower_ectract_delivery_lbl"></span></label>
                                                            <input type="hidden" id="lower_ectract_delivery_value" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <label class="form-control inner-label ben-extracted lower_lbl_btn_all" value="" id="lower_been_extracted"> has been extracted <br><span id="lower_been_extracted_lbl"></span></label>
                                                            <input type="hidden" id="lower_been_extracted_value" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <label class="form-control inner-label fix-add lower_lbl_btn_all" value="" id="lower_fix"> fix or add <br> <span id="lower_fix_lbl"></span></label>
                                                            <input type="hidden" id="lower_fix_value" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <label class="form-control inner-label clasps lower_lbl_btn_all" value="" id="lower_clasps">clasps<br> <span id="lower_clasps_lbl"></span></label>
                                                            <input type="hidden" id="lower_clasps_value" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <hr>


                            <div class="col-lg-12" id="notes_div">
                                <h3 class="card-title mb-1">
                                    Stage Notes
                                </h3>
                                <div id="toolbar-container"></div>
                                <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Description"></textarea>
                            </div>



                            <div class="col-lg-4 align-self-end d-inline-block mt-3" id="submit_div">
                                <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
                                <a href="{{route('admin.labcase')}}" class="btn back-btn me-2">Back</a>
                            </div>

                            <div class="col-lg-8 align-self-end d-inline-block">
                                <div class="add-case-bottom">
                                    <div>
                                        <div class="d-flex justify-content-center align-items-center">
                                            <i class="fas fa-exclamation-triangle"></i>
                                            <label>
                                                By clicking this box you acknowledge all informations is correct
                                            </label>
                                            <input class="ms-5 c-checkbox" type="checkbox" value="">
                                        </div>
                                    </div>

                                    <a href="javascript:;" class="btn add-btn"> Submit slip </a>

                                </div>
                            </div>
                        </div>

                </div>
                </form>

            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function() {
        upper_in_mouth_array = [];
        $('.teeth_uppper').addClass("img-for-teeth");
        $(".upper_in_mouth_lbl").addClass("active");

        for (i = 1; i <= 16; i++) {
            upper_in_mouth_array.push(parseInt(i));
        }
        upper_in_mouth_array = $.unique(upper_in_mouth_array.sort());
        $("#upper_in_mouth_lbl").text(upper_in_mouth_array.join(","));

        lower_in_mouth_array = [];
        $('.teeth_lower').addClass("img-for-teeth");
        $(".lower_in_mouth_lbl").addClass("active");

        for (i = 17; i <= 32; i++) {
            lower_in_mouth_array.push(parseInt(i));
        }
        lower_in_mouth_array = $.unique(lower_in_mouth_array.sort());
        $("#lower_in_mouth_lbl").text(lower_in_mouth_array.join(","));
    });
    // Upper Teeth
    upper_in_mouth_array = [];
    upper_missing_teeth_array = [];
    upper_ectract_delivery_array = [];
    upper_been_extracted_array = [];
    upper_fix_array = [];
    upper_clasps_array = [];

    $(document).on('click', '.lbl_btn_all', function() {
        $(".lbl_btn_all").removeClass("selected-button-upper");
        $(this).addClass("selected-button-upper");
        $(".lbl_btn_all").removeClass("active");
        $(this).addClass('active');
    });
    $(document).on('click', '.uppper_mising_all', function() {


        upper_in_mouth_array = [];
        upper_missing_teeth_array = [];
        upper_ectract_delivery_array = [];
        upper_been_extracted_array = [];
        upper_fix_array = [];
        upper_clasps_array = [];

        $('.teeth_uppper').addClass("missing");
        $(".upper-claps").hide();
        $(".lbl_btn_all").removeClass("active");

        for (i = 1; i <= 16; i++) {
            upper_missing_teeth_array.push(parseInt(i));
        }

        $('.teeth_uppper').removeClass("in-mouth");
        $('.teeth_uppper').removeClass("will-extract-on-deliver");
        $('.teeth_uppper').removeClass("ben-extracted");
        $('.teeth_uppper').removeClass("add-or-fix");
        $('.teeth_uppper').removeClass("calps");

        $("#upper_in_mouth_lbl").text('');
        $("#upper_ectract_delivery_lbl").text('');
        $("#upper_been_extracted_lbl").text('');
        $("#upper_fix_lbl").text('');
        $("#upper_clasps_lbl").text('');

        upper_missing_teeth_array = $.unique(upper_missing_teeth_array.sort());
        $("#upper_missing_teeth_lbl").text(upper_missing_teeth_array.join(","));

    });

    $(document).on('click', '.teeth_uppper', function() {
        selected_tab = $(".selected-button-upper").attr("id");
        teeth_num = $(this).data("name");
        teeth_image = $(this).attr('id');
        if (selected_tab == "upper_in_mouth") {
            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-teeth").removeClass("in-mouth");
                $("#uppper-" + teeth_num + "-claps").hide();

                if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                    var index = upper_clasps_array.indexOf(teeth_num);
                    if (index !== -1) {
                        upper_clasps_array.splice(index, 1);
                    }
                }

            } else {
                upper_in_mouth_array.push(teeth_num);
                $("#uppper-" + teeth_num + "-teeth").addClass("in-mouth");
            }

            // $(this).addClass("in-mouth");


            $("#uppper-" + teeth_num + "-teeth").removeClass("missing");
            $("#uppper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#uppper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            $("#uppper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            $("#uppper-" + teeth_num + "-teeth").removeClass("calps");
        } else if (selected_tab == "upper_missing_teeth") {

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-claps").hide();
            }


            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-teeth").removeClass("missing");
            } else {
                upper_missing_teeth_array.push(teeth_num);
                $("#uppper-" + teeth_num + "-teeth").addClass("missing");
            }



            // $("#uppper-"+teeth_num+"-teeth").addClass("active");

            $("#uppper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#uppper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            $("#uppper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            $("#uppper-" + teeth_num + "-teeth").removeClass("calps");
            $("#uppper-" + teeth_num + "-teeth").removeClass("in-mouth");
        } else if (selected_tab == "upper_ectract_delivery") {

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-claps").hide();
            }

            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            } else {
                upper_ectract_delivery_array.push(teeth_num);
                $("#uppper-" + teeth_num + "-teeth").addClass("will-extract-on-deliver");
            }

            // $("#uppper-"+teeth_num+"-teeth").addClass("active");

            $("#uppper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            $("#uppper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            $("#uppper-" + teeth_num + "-teeth").removeClass("calps");
            $("#uppper-" + teeth_num + "-teeth").removeClass("missing");
            $("#uppper-" + teeth_num + "-teeth").removeClass("in-mouth");
        } else if (selected_tab == "upper_been_extracted") {

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-claps").hide();
            }


            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            } else {
                upper_been_extracted_array.push(teeth_num)
                $("#uppper-" + teeth_num + "-teeth").addClass("ben-extracted");
            }


            $("#uppper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            $("#uppper-" + teeth_num + "-teeth").removeClass("calps");
            $("#uppper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#uppper-" + teeth_num + "-teeth").removeClass("missing");
            $("#uppper-" + teeth_num + "-teeth").removeClass("in-mouth");

        } else if (selected_tab == "upper_fix") {

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-claps").hide();
            }


            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            } else {
                upper_fix_array.push(teeth_num)
                $("#uppper-" + teeth_num + "-teeth").addClass("add-or-fix");
            }


            $("#uppper-" + teeth_num + "-teeth").removeClass("calps");
            $("#uppper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            $("#uppper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#uppper-" + teeth_num + "-teeth").removeClass("missing");
            $("#uppper-" + teeth_num + "-teeth").removeClass("in-mouth");
        } else if (selected_tab == "upper_clasps") {

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-claps").hide();
            } else if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                upper_clasps_array.push(teeth_num);
                $("#uppper-" + teeth_num + "-claps").show();
            }
        }


        upper_in_mouth_array = $.unique(upper_in_mouth_array.sort());
        upper_missing_teeth_array = $.unique(upper_missing_teeth_array.sort());
        upper_ectract_delivery_array = $.unique(upper_ectract_delivery_array.sort());
        upper_been_extracted_array = $.unique(upper_been_extracted_array.sort());
        upper_fix_array = $.unique(upper_fix_array.sort());
        upper_clasps_array = $.unique(upper_clasps_array.sort());


        $("#upper_in_mouth_lbl").text(upper_in_mouth_array.join(","));
        $("#upper_missing_teeth_lbl").text(upper_missing_teeth_array.join(","));
        $("#upper_ectract_delivery_lbl").text(upper_ectract_delivery_array.join(","));
        $("#upper_been_extracted_lbl").text(upper_been_extracted_array.join(","));
        $("#upper_fix_lbl").text(upper_fix_array.join(","));
        $("#upper_clasps_lbl").text(upper_clasps_array.join(","));

    });

    // Lower Teeth ========================================================================================

    lower_in_mouth_array = [];
    lower_missing_teeth_array = [];
    lower_ectract_delivery_array = [];
    lower_been_extracted_array = [];
    lower_fix_array = [];
    lower_clasps_array = [];

    $(document).on('click', '.lower_lbl_btn_all', function() {
        $(".lower_lbl_btn_all").removeClass("selected-button-lower");
        $(this).addClass("selected-button-lower");
        $(".lower_lbl_btn_all").removeClass("active");
        $(this).addClass('active');
    });
    $(document).on('click', '.lower_mising_all', function() {

        lower_in_mouth_array = [];
        lower_missing_teeth_array = [];
        lower_ectract_delivery_array = [];
        lower_been_extracted_array = [];
        lower_fix_array = [];
        lower_clasps_array = [];

        $('.teeth_lower').addClass("missing");
        $(".lower-claps").hide();
        $(".lower_lbl_btn_all").removeClass("active");

        for (i = 17; i <= 32; i++) {
            lower_missing_teeth_array.push(parseInt(i));
        }

        $("#lower_missing_teeth_lbl").text('17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32');

        $('.teeth_lower').removeClass("in-mouth");
        $('.teeth_lower').removeClass("will-extract-on-deliver");
        $('.teeth_lower').removeClass("ben-extracted");
        $('.teeth_lower').removeClass("add-or-fix");
        $('.teeth_lower').removeClass("calps");

        $("#lower_in_mouth_lbl").text('');
        $("#lower_ectract_delivery_lbl").text('');
        $("#lower_been_extracted_lbl").text('');
        $("#lower_fix_lbl").text('');
        $("#lower_clasps_lbl").text('');

        lower_missing_teeth_array = $.unique(lower_missing_teeth_array.sort());
        $("#lower_missing_teeth_lbl").text(lower_missing_teeth_array.join(","));

    });

    $(document).on('click', '.teeth_lower', function() {
        lower_selected_tab = $(".selected-button-lower").attr("id");
        lower_teeth_num = $(this).data("name");
        teeth_image = $(this).attr('id')

        if (lower_selected_tab == "lower_in_mouth") {


            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
            }


            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {

                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }

                if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                    var index = lower_clasps_array.indexOf(lower_teeth_num);
                    if (index !== -1) {
                        lower_clasps_array.splice(index, 1);
                    }
                }

                $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
                $("#lower-" + lower_teeth_num + "-claps").hide();

            } else {
                lower_in_mouth_array.push(lower_teeth_num);
                $("#lower-" + lower_teeth_num + "-teeth").addClass("in-mouth");
            }

            $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");

        } else if (lower_selected_tab == "lower_missing_teeth") {

            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            }


            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            } else {
                lower_missing_teeth_array.push(lower_teeth_num)
                $("#lower-" + lower_teeth_num + "-teeth").addClass("missing");
            }


            $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");
        } else if (lower_selected_tab == "lower_ectract_delivery") {

            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            }


            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            } else {
                lower_ectract_delivery_array.push(lower_teeth_num);
                $("#lower-" + lower_teeth_num + "-teeth").addClass("will-extract-on-deliver");
            }


            $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");
        } else if (lower_selected_tab == "lower_been_extracted") {


            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            }


            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            } else {
                lower_been_extracted_array.push(lower_teeth_num)
                $("#lower-" + lower_teeth_num + "-teeth").addClass("ben-extracted");
            }


            $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");
        } else if (lower_selected_tab == "lower_fix") {


            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            }


            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            } else {
                lower_fix_array.push(lower_teeth_num)
                $("#lower-" + lower_teeth_num + "-teeth").addClass("add-or-fix");
            }


            $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");
        } else if (lower_selected_tab == "lower_clasps") {

            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            } else if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                lower_clasps_array.push(lower_teeth_num);
                $("#lower-" + lower_teeth_num + "-claps").show();
            }
        }


        lower_in_mouth_array = $.unique(lower_in_mouth_array.sort());
        lower_missing_teeth_array = $.unique(lower_missing_teeth_array.sort());
        lower_ectract_delivery_array = $.unique(lower_ectract_delivery_array.sort());
        lower_been_extracted_array = $.unique(lower_been_extracted_array.sort());
        lower_fix_array = $.unique(lower_fix_array.sort());
        lower_clasps_array = $.unique(lower_clasps_array.sort());


        $("#lower_in_mouth_lbl").text(lower_in_mouth_array.join(","));
        $("#lower_missing_teeth_lbl").text(lower_missing_teeth_array.join(","));
        $("#lower_ectract_delivery_lbl").text(lower_ectract_delivery_array.join(","));
        $("#lower_been_extracted_lbl").text(lower_been_extracted_array.join(","));
        $("#lower_fix_lbl").text(lower_fix_array.join(","));
        $("#lower_clasps_lbl").text(lower_clasps_array.join(","));

    });
    // 
</script>
<script>
    
    $(document).on('click', '#upper_addons', function() {
        $("#upper_model").modal("show");

    });
    $(document).on('click', '#lower_addons', function() {
        $("#lower_model").modal("show");

    });

    $(document).on('click', '#rush_model-open', function() {
        alert();
        $("#rush_model").modal("show");

    });



    $(document).on('change', '#iOfficeId', function() {
        var iOfficeId = $("#iOfficeId").val();
        $.ajax({
            url: "{{route('admin.labcase.getdoc')}}",
            type: "post",
            data: {
                iOfficeId: iOfficeId,
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#iDoctorId').selectize()[0].selectize.destroy();
                $('#iDoctorId').html(result);
                $('#iDoctorId').selectize();
                $('#doctor_div').show();

                iDoctorId = $('#iDoctorId').val();
                if (iDoctorId != "") {
                    $('#vPatientName_div').show();
                }

            }
        });
    });


    $(document).on('change', '#iDoctorId', function() {
        var iDoctorId = $("#iDoctorId").val();
        if (iDoctorId == "") {
            $('#vPatientName_div').hide();
        } else {
            $('#vPatientName_div').show();
        }
    });

    $(document).on('change', '#iUpperCategoryId', function() {
        var iUpperCategoryId = $("#iUpperCategoryId").val();
        $.ajax({
            url: "{{route('admin.labcase.getproduct')}}",
            type: "post",
            data: {
                iCategoryId: iUpperCategoryId,
                eType: "Upper",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#iUpperProductId').selectize()[0].selectize.destroy();
                $('#iUpperProductId').html(result);
                $('#iUpperProductId').selectize();
                $('#product_div').show();
                $('#product_lbl').show();
                $('#product_upper_div').show();
            }
        });
    });

    $(document).on('change', '#iLowerCategoryId', function() {
        var iLowerCategoryId = $("#iLowerCategoryId").val();
        $.ajax({
            url: "{{route('admin.labcase.getproduct')}}",
            type: "post",
            data: {
                iCategoryId: iLowerCategoryId,
                eType: "Lower",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#iLowerProductId').selectize()[0].selectize.destroy();
                $('#iLowerProductId').html(result);
                $('#iLowerProductId').selectize();
                $('#product_div').show();
                $('#product_lbl').show();
                $('#product_lower_div').show();
            }
        });
    });

    $(document).on('change', '#iUpperStageId', function() {
        var iUpperProductId = $("#iUpperProductId").val();
        $.ajax({
            url: "{{route('admin.labcase.getgrade')}}",
            type: "post",
            data: {
                iCategoryProductId: iUpperProductId,
                eType: "Upper",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#iUpperGradeId').selectize()[0].selectize.destroy();
                $('#iUpperGradeId').html(result);
                $('#iUpperGradeId').selectize();
                $('#grade_div').show();
                $('#grade_lbl').show();
                $('#grade_upper_div').show();
                $('#submit_div').show();
                $('#notes_div').show();
                $('#category_addons_div').show();
                $('#upper_addons_div').show();
                $('#stage_status_div').show();
                $('#upper_status_div').show();
                $('#submit_div').show();
                $('#upper_status_div').show();
                $('#status_lbl').show();
                var iUpperStageId = $("#iUpperStageId").val();
                $.ajax({
                    url: "{{route('admin.labcase.getshadetype')}}",
                    type: "post",
                    dataType: "json",
                    data: {
                        iProductStageId: iUpperStageId,
                        eType: "Upper",
                        _token: '{{csrf_token()}}'
                    },
                    success: function(response) {
                        if (response[0].eImpressions == 'Yes') {
                            $('#impression_div').show();
                            $('#impression_lbl').show();
                            $('#upper_impression_div').show();
                        } else {
                            $('#impression_div').hide();
                            $('#upper_impression_div').hide();
                        }
                        if (response[0].eTeethShades == 'Yes') {
                            $('#teeth_shades_div').show();
                            $('#teethshade_lbl').show();
                            $('#upper_tsbrand_div').show();
                        } else {
                            $('#teeth_shades_div').hide();
                            $('#upper_tsbrand_div').hide();
                        }
                        if (response[0].eGumShades == 'Yes') {
                            $('#gum_shades_div').show();
                            $('#gumshade_lbl').show();
                            $('#upper_gsbrand_div').show();
                        } else {
                            $('#gum_shades_div').hide();
                            $('#upper_gsbrand_div').hide();
                        }

                    }
                });
            }
        });
    });
    $(document).on('change', '#iLowerStageId', function() {
        var iLowerProductId = $("#iLowerProductId").val();
        $.ajax({
            url: "{{route('admin.labcase.getgrade')}}",
            type: "post",
            data: {
                iCategoryProductId: iLowerProductId,
                eType: "Lower",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#iLowerGradeId').selectize()[0].selectize.destroy();
                $('#iLowerGradeId').html(result);
                $('#iLowerGradeId').selectize();
                $('#grade_div').show();
                $('#grade_lower_div').show();
                $('#submit_div').show();
                $('#notes_div').show();
                $('#category_addons_div').show();
                $('#lower_addons_div').show();
                $('#stage_status_div').show();
                $('#lower_status_div').show();
                $('#lower_status_div').show();
                $('#status_lbl').show();
                $('#submit_div').show();
                var iLowerStageId = $("#iLowerStageId").val();
                $.ajax({
                    url: "{{route('admin.labcase.getshadetype')}}",
                    type: "post",
                    dataType: "json",
                    data: {
                        iProductStageId: iLowerStageId,
                        eType: "Lower",
                        _token: '{{csrf_token()}}'
                    },
                    success: function(response) {
                        if (response[0].eImpressions == 'Yes') {
                            $('#impression_div').show();
                            $('#impression_lbl').show();
                            $('#lower_impression_div').show();
                        } else {
                            $('#impression_div').hide();
                            $('#lower_impression_div').show();
                        }
                        if (response[0].eTeethShades == 'Yes') {
                            $('#teeth_shades_div').show();
                            $('#teethshade_lbl').show();
                            $('#lower_tsbrand_div').show();
                        } else {
                            $('#teeth_shades_div').hide();
                            $('#lower_tsbrand_div').hide();
                        }
                        if (response[0].eGumShades == 'Yes') {
                            $('#gum_shades_div').show();
                            $('#lower_gsbrand_div').show();
                        } else {
                            $('#gum_shades_div').hide();
                            $('#lower_gsbrand_div').hide();
                        }

                    }
                });
            }
        });
    });


    $(document).on('change', '#iUpperProductId', function() {
        var iUpperProductId = $("#iUpperProductId").val();
        var iUpperCategoryId = $("#iUpperCategoryId").val();
        $.ajax({
            url: "{{route('admin.labcase.getstage')}}",
            type: "post",
            data: {
                iCategoryProductId: iUpperProductId,
                iCategoryId: iUpperCategoryId,
                eType: "Upper",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#iUpperStageId').selectize()[0].selectize.destroy();
                $('#iUpperStageId').html(result);
                $('#iUpperStageId').selectize();
                $('#stage_div').show();
                $('#stage_lbl').show();
                $('#stage_upper_div').show();
            }
        });
    });

    $(document).on('change', '#iLowerProductId', function() {
        var iLowerProductId = $("#iLowerProductId").val();
        var iLowerCategoryId = $("#iLowerCategoryId").val();
        $.ajax({
            url: "{{route('admin.labcase.getstage')}}",
            type: "post",
            data: {
                iCategoryProductId: iLowerProductId,
                iCategoryId: iLowerCategoryId,
                eType: "Lower",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#iLowerStageId').selectize()[0].selectize.destroy();
                $('#iLowerStageId').html(result);
                $('#iLowerStageId').selectize();
                $('#stage_div').show();
                $('#stage_lbl').show();
                $('#stage_lower_div').show();
            }
        });
    });
    $(document).on('change', '#upper_tsbrand', function() {
        var iToothBrandId = $("#upper_tsbrand").val();
        $.ajax({
            url: "{{route('admin.labcase.gettoothshades')}}",
            type: "post",
            data: {
                iToothBrandId: iToothBrandId,
                eType: "Upper",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#upper_tshade').selectize()[0].selectize.destroy();
                $('#upper_tshade').html(result);
                $('#upper_tshade').selectize();
            }
        });
    });
    $(document).on('change', '#lower_tsbrand', function() {
        var iToothBrandId = $("#lower_tsbrand").val();
        $.ajax({
            url: "{{route('admin.labcase.gettoothshades')}}",
            type: "post",
            data: {
                iToothBrandId: iToothBrandId,
                eType: "Lower",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#lower_tshade').selectize()[0].selectize.destroy();
                $('#lower_tshade').html(result);
                $('#lower_tshade').selectize();
            }
        });
    });
    $(document).on('change', '#upper_gsbrand', function() {
        var iGumBrandId = $("#upper_gsbrand").val();
        $.ajax({
            url: "{{route('admin.labcase.getgumshades')}}",
            type: "post",
            data: {
                iGumBrandId: iGumBrandId,
                eType: "Upper",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#upper_gshade').selectize()[0].selectize.destroy();
                $('#upper_gshade').html(result);
                $('#upper_gshade').selectize();
            }
        });
    });
    $(document).on('change', '#lower_gsbrand', function() {
        var iGumBrandId = $("#lower_gsbrand").val();
        $.ajax({
            url: "{{route('admin.labcase.getgumshades')}}",
            type: "post",
            data: {
                iGumBrandId: iGumBrandId,
                eType: "Lower",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#lower_gshade').selectize()[0].selectize.destroy();
                $('#lower_gshade').html(result);
                $('#lower_gshade').selectize();
            }
        });
    });
    $(document).on('change', '#upper_category_addons', function() {
        var iAddCategoryId = $("#upper_category_addons").val();
        $.ajax({
            url: "{{route('admin.labcase.getcategoryaddons')}}",
            type: "post",
            data: {
                iAddCategoryId: iAddCategoryId,
                eType: "Upper",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#upper_subaddons').selectize()[0].selectize.destroy();
                $('#upper_subaddons').html(result);
                $('#upper_subaddons').selectize();
            }
        });
    });
    $(document).on('change', '#lower_category_addons', function() {
        var iAddCategoryId = $("#lower_category_addons").val();
        $.ajax({
            url: "{{route('admin.labcase.getcategoryaddons')}}",
            type: "post",
            data: {
                iAddCategoryId: iAddCategoryId,
                eType: "Lower",
                _token: '{{csrf_token()}}'
            },
            success: function(result) {
                $('#lower_subaddons').selectize()[0].selectize.destroy();
                $('#lower_subaddons').html(result);
                $('#lower_subaddons').selectize();
            }
        });
    });
    $("#vPatientName").keyup(function() {

        var vPatientName = $("#vPatientName").val();
        $("#product_button_div").show();
    });

    $(document).on('click', '#product_upper_button_div', function() {
        $("#category_div").show();
        $("#category_upper_div").show();
        $("#cat_lbl").show();
    });

    $(document).on('click', '#product_lower_button_div', function() {
        $("#category_div").show();
        $("#cat_lbl").show();
        $("#category_lower_div").show();
    });


    var upperaddons = [];
    $(document).on('click', '#upper_submit', function() {
        upper_category_addons = $("#upper_category_addons").val();
        upper_subaddons = $("#upper_subaddons").val();
        iUpperQty = $("#iUpperQty").val();

        upperaddons.push(upper_category_addons, upper_subaddons, iUpperQty);

        console.log(upperaddons);
    });
    // $("#table div").append("<tr>" +
    //     "<td>" + $("#upper_category_addons").val(upper_category_addons) + "</td>" +
    //     "<td>" + $("#upper_subaddons").val(upper_subaddons) + "</td>" +
    //     "<td>" + $("#iUpperQty").val(iUpperQty) + "</td>" +
    //     "</tr>");
    $(document).on('click', '#submit', function() {
        var error = false;

        // if (vQualityName.length == 0) {
        //     $("#vQualityName_error").show();
        //     error = true;
        // } else {
        //     $("#vQualityName_error").hide();
        // }
        // if (iSequence.length == 0) {
        //     $("#iSequence_error").show();
        //     error = true;
        // } else {
        //     $("#iSequence_error").hide();
        // }
        setTimeout(function() {
            if (error == true) {
                return false;
            } else {
                $("#frm").submit();
                return true;
            }
        }, 1000);

    });
</script>