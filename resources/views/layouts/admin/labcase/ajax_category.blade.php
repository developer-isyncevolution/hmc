@if (!empty($Category))    
    @if (count($Category) > 1)
        <option value="">Select Category</option>
    @endif
    @foreach($Category as $key => $Category_val)
        <option value="{{$Category_val->iCategoryId}}"> {{$Category_val->vName}}</option>
    @endforeach
@else
    <option value="">No category Found</option>
@endif