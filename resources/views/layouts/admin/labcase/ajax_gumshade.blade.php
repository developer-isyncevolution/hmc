@if (!empty($gumshade))    
    @if (count($gumshade) > 1)
        <option value="">Shade</option>
    @endif
    @foreach($gumshade as $key => $gumshade_val)
        <option value="{{$gumshade_val->iGumShadesId}}"> {{$gumshade_val->vGumShade}}</option>
    @endforeach
@else
    <option value="">No Shade</option>
@endif
