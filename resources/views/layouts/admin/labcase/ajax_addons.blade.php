@if (!empty($addons))    
    @if (count($addons) > 1)
        <option value="">Select Addon</option>
    @endif
    @foreach($addons as $key => $addons_val)
        <option value="{{$addons_val->iSubAddCategoryId}}"> {{$addons_val->vAddonName}}</option>
    @endforeach
@else
    <option value="">No addons Found</option>
@endif
