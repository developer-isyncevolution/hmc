<!--begin::Global Javascript Bundle(used by all pages)-->

<script src="{{asset('admin/assets/plugins/global/plugins.bundle.js')}}"></script>
<script src="{{asset('admin/assets/js/scripts.bundle.js')}}"></script>

<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<script src="{{asset('admin/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}"></script>
{{-- <!-- <script src="{{asset('admin/assets/plugins/custom/datatables/datatables.bundle.js')}}"></script> --> --}}
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
 {{-- <script src="{{asset('admin/assets/js/calendar/calendar.js')}}"></script> --}}
<script src="{{asset('admin/assets/js/custom/widgets.js')}}"></script>
{{-- <script src="{{asset('admin/assets/js/custom/lazyload.min.js')}}"></script> --}}
<script src="{{asset('admin/assets/js/custom/apps/chat/chat.js')}}"></script>
<script src="{{asset('admin/assets/js/custom/modals/upgrade-plan.js')}}"></script>
<script src="{{asset('admin/assets/js/custom/modals/create-app.js')}}"></script>
<script src="{{asset('admin/assets/js/custom/modals/users-search.js')}}"></script>
<script src="{{asset('admin/assets/js/custom/modals/sweetalert.min.js')}}"></script>

{{-- <script src="{{asset('admin/assets/js/custom/modals/toastr.min.js')}}"></script> --}}
<script src="{{asset('admin/assets/js/custom/modals/tinymce/tinymce.min.js')}}"></script>
<!--end::Page Custom Javascript-->
<!--end::Javascript-->

<script src="{{asset('admin/assets/js/toast.script.js')}}"></script>
<script src="{{asset('admin/assets/js/site.js')}}"></script>
