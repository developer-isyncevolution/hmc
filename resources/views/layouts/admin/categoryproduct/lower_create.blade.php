@extends('layouts.admin.index')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<div class="main-panel">
  <div class="card">
    <div class="col-lg-12 mx-auto">
      <div class="card mb-5 mb-xl-4">
        <div class="card-header border-0">
            <ul class="d-flex justify-content-start my-2 list-unstyled flex-wrap">
                <li class="">
                    <a href="{{route('admin.category')}}" class=" add-btn btn mx-2">
                        Categories
                    </a>
                </li>
                <li class="">
                    <a href="{{route('admin.categoryproduct')}}" class="active btn add-btn  mx-2">
                        Category Products
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.productstage')}}" class="btn add-btn mx-2">
                        Stages
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" class="btn add-btn mx-2">
                      Categories Add on
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" class="btn add-btn mx-2">
                      Add on
                    </a>
                  </li>
            </ul>
        </div>
    </div>
          <div class="card-header">
            <h4 class="card-title" id="row-separator-basic-form">{{isset($categoryproducts->iCategoryProductId) ? 'Edit' : 'Add'}} Lower Product</h4>
            
          </div>
        <form action="{{route('admin.categoryproduct.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($categoryproducts)) {{$categoryproducts->iCategoryProductId}} @endif">
            <input type="hidden" name="eType" value="Lower">
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label >Category</label>
            <select name="iCategoryId" id="iCategoryId" class="form-control">
              <option value="none">Select category</option>
              @foreach($category as $key => $categorys)
              <option value="{{$categorys->iCategoryId}}" @if(isset($categoryproducts)){{$categoryproducts->iCategoryId == $categorys->iCategoryId  ? 'selected' : ''}} @endif>{{$categorys->vName}}</option>
              @endforeach
            </select>
            <div class="text-danger" style="display: none;" id="iCategoryId_error">Please select category</div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Code</label>
            <input type="text" class="form-control" id="vCode" name="vCode" placeholder="Code" value="@if(old('vCode')!=''){{old('vCode')}}@elseif(isset($categoryproducts->vCode)){{$categoryproducts->vCode}}@else{{old('vCode')}}@endif">
            <div class="text-danger" style="display: none;" id="vCode_error">Please enter code</div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Name</label>
            <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($categoryproducts->vName)){{$categoryproducts->vName}}@else{{old('vName')}}@endif">
            <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
          </div>
          <div class="row">
            <div class="col-md-12" >
                <h5>Select Grades</h5>
            </div>
            @if(isset($prograde))
              @foreach ($prograde  as $key => $progrades)
                      @php $iGradeId = $progrades->iGradeId ;@endphp
              @endforeach
            @endif
            @foreach ($grades  as $key => $value)   
            <div class="col-md-4" style="text-align:center">
              <label>{{$value->vQualityName}}</label>
              <input type="checkbox" name="iGradeId[]" id="iGradeId" class="form-control" value="{{$value->iGradeId}}" @if(isset($categoryproducts)) {{$iGradeId}} checked @endif>
            </div>
            @endforeach
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Clasp to repair or add</label>
            <select id="eClaspAdd" name="eClaspAdd" class="form-control">
                <option value="Yes" @if(isset($categoryproducts)) @if($categoryproducts->eClaspAdd == 'Yes') selected @endif @endif>Yes</option>
                <option value="No" @if(isset($categoryproducts)) @if($categoryproducts->eClaspAdd == 'No') selected @endif @endif>No</option>
            </select>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Status</label>
            <select id="eStatus" name="eStatus" class="form-control">
                <option value="Active" @if(isset($categoryproducts)) @if($categoryproducts->eStatus == 'Active') selected @endif @endif>Active</option>
                <option value="Inactive" @if(isset($categoryproducts)) @if($categoryproducts->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
            </select>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Price</label>
            <div id="toolbar-container"></div>
            <input type="number" class="form-control" id="vPrice" name="vPrice" placeholder="Price" value="@if(old('vPrice')!=''){{old('vPrice')}}@elseif(isset($categoryproducts->vPrice)){{$categoryproducts->vPrice}}@else{{old('vPrice')}}@endif">              
            <div id="vPrice_error" class="text-danger" style="display: none;">Please enter price </div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Sequence</label>
            <div id="toolbar-container"></div>
            <input type="number" class="form-control" id="iSequence" name="iSequence" placeholder="Sequence" value="@if(old('iSequence')!=''){{old('iSequence')}}@elseif(isset($categoryproducts->iSequence)){{$categoryproducts->iSequence}}@else{{old('iSequence')}}@endif">
              
            <div id="iSequence_error" class="text-danger" style="display: none;">Please enter Sequence </div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label class="required fs-5 fw-bold mb-2">Category Products image</label>
            <input class="form-control" type="file" id="vImage" name="vImage" accept="image/*">
            @if(isset($categoryproducts))
               <img style="width: 100px;" id="img" value="@if(old('vImage') == 'vImage') selected @endif" src="{{asset('uploads/categoryproduct/'.$categoryproducts->vImage)}}">
            @else 
               <img style="width: 100px;" id="img" value="" src="{{asset('images/no-image.gif')}}">
            @endif
            <div id="vImage_error" class="text-danger" style="display: none;">Select banner image</div>
        </div>
            <div class="col-12 align-self-end d-inline-block ">
                <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
                <a href="{{route('admin.categoryproduct')}}" class="btn back-btn me-2">Back</a>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
   <script>
     $('#eStatus').select2({
        width: '100%',
        placeholder: "Select customer",
        allowClear: true
    });
    $('#eType').select2({
        width: '100%',
        placeholder: "Select customer",
        allowClear: true
    });
    $('#eClaspAdd').select2({
        width: '100%',
        placeholder: "Select customer",
        allowClear: true
    });
    $(document).on('change','#vImage',function()
    {
        if (this.files && this.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
            $('#img').attr('src', e.target.result);
          };
          reader.readAsDataURL(this.files[0]);
        }
    });
  $(document).on('click','#submit',function()
  {
    iCategoryProductId = $("#id").val();
    vCode           = $("#vCode").val();
    iCategoryId     = $("#iCategoryId").val();
    iQuantity       = $("#iQuantity").val();
    vName           = $("#vName").val();
    iSequence       = $("#iSequence").val();
    vImage          = $("#vImage").val();

    var error = false;

    if(vCode.length == 0){
      $("#vCode_error").show();
      error = true;
    } else {
      $("#vCode_error").hide();
    }
    if(iCategoryId == 'none'){
      $("#iCategoryId_error").show();
      error = true;
    } else {
      $("#iCategoryId_error").hide();
    }
    if (iSequence.length == 0) {
      $("#iSequence_error").show();
      error = true;
    } else {
      $("#iSequence_error").hide();
    }
    if(vName.length == 0){
      $("#vName_error").show();
      error = true;
    } else {
      $("#vName_error").hide();
    }
    <?php if(!isset($categoryproducts->iCategoryProductId)){?>
      if(vImage.length == 0){
        $("#vImage_error").show();
        error = true;
      } else {
        $("#vImage_error").hide();
      } 
    <?php } ?>
    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
    </script>
@endsection
