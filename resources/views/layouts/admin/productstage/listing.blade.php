@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
    <div class="page-title text-center">
        <h3>
            Lab Administrator
        </h3>
    </div>
    <div class="card mb-5 mb-xl-4">
        <div class="card-header border-0 p-0">
            <ul class="c-nav-tab">
                @if(\App\Libraries\General::check_permission('CategoryController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.category')}}" class=" add-btn btn mx-2">
                        Categories
                    </a>
                </li>
                @endif
                @if(\App\Libraries\General::check_permission('CategoryProductController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.categoryproduct')}}" class="  btn add-btn  mx-2">
                        Products
                    </a>
                </li>
                @endif
                @if(\App\Libraries\General::check_permission('ProductStageController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.productstage')}}" class=" active btn add-btn mx-2">
                        Stages
                    </a>
                </li>
                @endif
                @if(\App\Libraries\General::check_permission('AddoncategoryController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.addoncategory')}}" class="btn add-btn mx-2">
                        Categories Add ons
                    </a>
                </li>
                @endif
                @if(\App\Libraries\General::check_permission('SubaddoncategoryController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.subaddoncategory')}}" class="btn add-btn mx-2">
                        Add ons
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-mid">
                    <ul class="d-flex list-unstyled">
                        @if(\App\Libraries\General::check_permission('','eCreate') == true)
                        <li>
                            <a href="{{route('admin.productstage.create')}}" class="btn create_permission add-btn me-2">
                                <i class="fa fa-plus"></i> Add
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-6 mx-auto">
            <div class="card mb-5 mb-xl-4">
                <h3 class="card-title">
                    Upper Stage
                </h3>
                <div class="row g-4">
                    <div class="col-lg-8 mx-auto">
                        <select name="iCategoryId" id="iCategoryId">
                            <option value="">Select category</option>
                            @foreach($uppercategory as $key => $uppercategorys)
                            <option value="{{$uppercategorys->iCategoryId}}" @if(isset($categoryproductselect)){{$uppercategorys->iCategoryId == $categoryproductselect->iCategoryId  ? 'selected' : ''}} @endif>{{$uppercategorys->vName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-8 mx-auto">
                        <select name="iCategoryProductId" id="iCategoryProductId">
                            <option value="">Select product </option>
                            @if(isset($categoryproductselect))
                            @foreach($upperproduct as $key => $upperproducts)
                            <option value="{{$upperproducts->iCategoryProductId}}" @if(isset($categoryproductselect)){{$upperproducts->iCategoryProductId == $categoryproductselect->iCategoryProductId  ? 'selected' : ''}} @endif>{{$upperproducts->vCode}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-8 mx-auto">
                    <div class="custome-serce-box">
                        <input type="text" class="form-control" id="upperkeyword" name="search" class="search" placeholder="Search">
                        <span class="search-btn"><i class="fas fa-search"></i></span>
                    </div>
                    </div>
                </div>
                <!-- 
                    
                    <ul class="d-flex justify-content-between list-unstyled py-3 my-2"> -->


                <!-- <div class="row">
                            <div class="col-lg-6">

                            </div>
                            <div class="col-lg-6">

                            </div>

                        </div> -->



                <!-- 
                        <li class="ms-auto  me-2">
                         
                        </li>

                        <li class="ms-auto  me-2">
                        
                        </li>
                        <li class="d-flex">
                            <div class="d-flex mx-2">
                               
                            </div> -->

                <!-- <a href="javascript:;" id="delete_btn" class="btn delete-btn me-2"> Multiple Delete</a> -->
                <!-- <a href="{{route('admin.productstage.create')}}" class="btn add-btn me-2">
                                <i class="fa fa-plus"></i> Add
                            </a> -->
                <!-- </li>
                    </ul> -->


            </div>
            <div class="listing-page">
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <!-- <th class="w-25px" data-orderable="false">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input id="Uselectall" type="checkbox" name="Uselectall" class="form-check-input">
                                        <label for="selectall">&nbsp;</label>
                                    </div>
                                </th> -->
                                <th scope="col"><a id="vCode" class="upersort" data-column="vCode" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">Code </span> </a></th>
                                <th scope="col"><a id="vName" class="upersort" data-column="vName" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">Stage </span> </a></th>
                                <th scope="col"><a id="iSequence" class="upersort" data-column="iSequence" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Working Time </span> </a></th>
                                <th scope="col"><a id="vPrice" class="upersort" data-column="vPrice" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Price </span> </a></th>
                                <th scope="col"><a id="iSequence" class="upersort" data-column="iSequence" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Sequence </span> </a></th>
                                <th scope="col"><a id="eStatus" class="upersort" data-column="eStatus" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Status </span> </a></th>
                                <th scope="col"><span class="text-muted fw-bold text-muted d-block fs-7"> Action </span></th>
                            </tr>
                        </thead>
                        <tbody id="upper_table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 mx-auto">
            <div class="card mb-5 mb-xl-4">
                <h3 class="card-title">
                    Lower Stage
                </h3>
                <div class="row g-4">
                    <div class="col-lg-8 mx-auto">
                        <select name="iCategoryId" id="Category">
                            <option value="">Select category</option>
                            @foreach($lowercategory as $key => $lowercategorys)
                            <option value="{{$lowercategorys->iCategoryId}}" @if(isset($categoryproductselect)){{$lowercategorys->iCategoryId == $categoryproductselect->iCategoryId  ? 'selected' : ''}} @endif>{{$lowercategorys->vName}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-lg-8 mx-auto">
                        <select name="iCategoryProductId" id="CategoryProduct">
                            <option value="">Select product </option>
                          
                        </select>
                    </div>

                    <div class="col-lg-8 mx-auto">
                        <div class="custome-serce-box">
                            <input type="text" class="form-control" id="lower_keyword" name="search" class="search" placeholder="Search">
                            <span class="search-btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div>
         
            </div>
            <div class="listing-page">
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <!-- <th class="w-25px" data-orderable="false">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input id="Lselectall" type="checkbox" name="Lselectall" class="form-check-input">
                                        <label for="selectall">&nbsp;</label>
                                    </div>
                                </th> -->
                                <th scope="col"><a id="vCode" class="lowersort" data-column="vCode" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">Code </span> </a></th>
                                <th scope="col"><a id="vName" class="lowersort" data-column="vName" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">Stage </span> </a></th>
                                <th scope="col"><a id="iSequence" class="lowersort" data-column="iSequence" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Working Time </span> </a></th>
                                <th scope="col"><a id="vPrice" class="lowersort" data-column="vPrice" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Price </span> </a></th>
                                <th scope="col"><a id="iSequence" class="lowersort" data-column="iSequence" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Sequence </span> </a></th>
                                <th scope="col"><a id="eStatus" class="lowersort" data-column="eStatus" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Status </span> </a></th>
                                <th scope="col"><span class="text-muted fw-bold text-muted d-block fs-7"> Action </span></th>
                            </tr>
                        </thead>
                        <tbody id="lower_table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="lower-ajax-loader" width="250px" height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom-css')
<style></style>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
    $('#iCategoryId').selectize();
    $('#iCategoryProductId').selectize();
    $('#Category').selectize();
    $('#CategoryProduct').selectize();
    // $('#CategoryProduct').select2({
    //     width: '100%',
    //     placeholder: "Select product ",
    //     allowClear: true
    // });
    // $('#iCategoryId').select2({
    //     width: '100%',
    //     placeholder: "Select category",
    //     allowClear: true
    // });
    // $('#Category').select2({
    //     width: '100%',
    //     placeholder: "Select category",
    //     allowClear: true
    // });
    // $('#CategoryProduct').select2({
    //     width: '100%',
    //     placeholder: "Select product",
    //     allowClear: true
    // });
    $(document).ready(function() {
        var eType = 'Upper';
        var iCategoryId = $("#iCategoryId").val();
        var iCategoryProductId = $("#iCategoryProductId").val();
        $.ajax({
            url: "{{route('admin.productstage.ajaxListing')}}",
            type: "get",
            data: {
                eType: eType,
                iCategoryId:iCategoryId,
                iCategoryProductId:iCategoryProductId,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $("#Uselectall").click(function() {
        if (this.checked) {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', true);
            });
        } else {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', false);
            });
        }
    });

    $(document).on('click', '#delete_btn', function() {
        var eType = 'Upper';
        var id = [];

        $("input[name='ProductStage_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                    title: "Are you sure delete all productstage.?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{route('admin.productstage.ajaxListing')}}",
                            type: "get",
                            data: {
                                id: id,
                                eType: eType,
                                action: 'multiple_delete'
                            },
                            success: function(response) {
                                $("#upper_table_record").html(response);
                                $("#ajax-loader").hide();
                            }
                        });
                    }
                });
        }
    });

    $("#upperkeyword").keyup(function() {
        var upperkeyword = $("#upperkeyword").val();
        var eType = 'Upper';
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.productstage.ajaxListing')}}",
            type: "get",
            data: {
                upperkeyword: upperkeyword,
                eType: eType,
                action: 'search'
            },
            success: function(response) {
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '#delete', function() {
        swal({
                title: "Are you sure delete this upper productstage.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");
                    var eType = 'Upper';

                    $("#ajax-loader").show();

                    $.ajax({
                        url: "{{route('admin.productstage.ajaxListing')}}",
                        type: "get",
                        data: {
                            id: id,
                            eType: eType,
                            action: 'delete'
                        },
                        success: function(response) {
                            $("#upper_table_record").html(response);
                            $("#ajax-loader").hide();
                            notification_error("Product Stage Deleted Successfully");
                                setTimeout(function() {
                                   location.reload();
                                }, 1000);
                        }
                    });
                }
            })
    });
    $(document).on('click', '.upersort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');
        var eType = 'Upper';

        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.productstage.ajaxListing')}}",
            type: "get",
            data: {
                column: column,
                order,
                order,
                eType: eType,
                action: 'sort'
            },
            success: function(response) {
                console.log(response);
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '.ajax_page_upper', function() {
        pages = $(this).data("pages");
        var eType = 'Upper';
        $("#upper_table_record").html('');
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.productstage.ajaxListing')}}",
            type: "get",
            data: {
                pages: pages,
                eType: eType,
            },
            success: function(response) {
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#iCategoryId', function() {
        var eType = 'Upper';
        var iCategoryId = $("#iCategoryId").val();
       
        $("#ajax-loader").show();
        $.ajax({
            url: "{{route('admin.productstage.ajaxListing')}}",
            type: "get",
            data: {
                iCategoryId: iCategoryId,
                eType: eType,
            },
            success: function(response) {
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
        $.ajax({
            
                url:"{{route('admin.productstage.getproduct')}}",
                type: "post",
                data: {
                    iCategoryId: iCategoryId,
                    _token: '{{csrf_token()}}' 
                },
                success: function(result){

                    $('#iCategoryProductId').selectize()[0].selectize.destroy();
                    $('#iCategoryProductId').html(result);
                    $('#iCategoryProductId').selectize();

                    iCategoryProductId = $("#iCategoryProductId").val();

                    $.ajax({
                        url: "{{route('admin.productstage.ajaxListing')}}",
                        type: "get",
                        data: {
                            iCategoryProductId: iCategoryProductId,
                            eType: eType,
                        },
                        success: function(response) {
                            $("#upper_table_record").html(response);
                            $("#ajax-loader").hide();
                        }
                    });                 

                    // $('#iCategoryProductId').selectize()[0].selectize.destroy();
                    // $('#iCategoryProductId').html('<option value="">Select product</option>'); 
                    // $.each(result.categoryproduct,function(key,value){
                    //     $("#iCategoryProductId").append('<option selected value="'+value.iCategoryProductId+'">'+value.vName+'</option>');
                    // });
                    // $('#iCategoryProductId').selectize();
                 }
            });
    });
    $(document).on('change', '#iCategoryProductId', function() {
        var eType = 'Upper';
        var iCategoryProductId = $("#iCategoryProductId").val();
        $("#ajax-loader").show();

        $.ajax({
            url: "{{route('admin.productstage.ajaxListing')}}",
            type: "get",
            data: {
                iCategoryProductId: iCategoryProductId,
                eType: eType,
            },
            success: function(response) {
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#upper_page_limit', function() {
        upper_limit_page = this.value;
        var eType = 'Upper';
        $("#upper_table_record").html('');
        $("#ajax-loader").show();
        url = "{{route('admin.productstage.ajaxListing')}}";

        setTimeout(function() {
            $.ajax({
                url: url,
                type: "get",
                data: {
                    upper_limit_page: upper_limit_page,
                    eType: eType
                },
                success: function(response) {
                    $("#upper_table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            // hideLoader();
        }, 500);
    });
</script>
<script>
    $(document).ready(function() {
        var eType = 'Lower';
        var Category = $("#Category").val();
        var CategoryProduct = $("#CategoryProduct").val();
        $.ajax({
            url: "{{route('admin.productstage.LowerajaxListing')}}",
            type: "get",
            data: {
                eType: eType,
                Category:Category,
                CategoryProduct:CategoryProduct,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#lower_table_record").html(response);
                $("#lower-ajax-loader").hide();
            }
        });
    });

    $("#Lselectall").click(function() {
        if (this.checked) {
            $('.lcheckboxall').each(function() {
                $(".lcheckboxall").prop('checked', true);
            });
        } else {
            $('.lcheckboxall').each(function() {
                $(".lcheckboxall").prop('checked', false);
            });
        }
    });

    $(document).on('click', '#lower_delete_btn', function() {
        var id = [];
        var eType = 'Lower';
        $("input[name='Lower_ProductStage_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                    title: "Are you sure delete all lower productstage.?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{route('admin.productstage.LowerajaxListing')}}",
                            type: "get",
                            data: {
                                id: id,
                                eType: eType,
                                action: 'multiple_delete'
                            },
                            success: function(response) {
                                $("#lower_table_record").html(response);
                                $("#lower-ajax-loader").hide();
                            }
                        });
                    }
                });
        }
    });

    $("#lower_keyword").keyup(function() {
        var lower_keyword = $("#lower_keyword").val();
        var eType = 'Lower';
        $("#lower-ajax-loader").show();

        $.ajax({
            url: "{{route('admin.productstage.LowerajaxListing')}}",
            type: "get",
            data: {
                lower_keyword: lower_keyword,
                eType: eType,
                action: 'search'
            },
            success: function(response) {
                $("#lower_table_record").html(response);
                $("#lower-ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '#lowerdelete', function() {
        swal({
                title: "Are you sure delete this lower productstage.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");
                    var eType = 'Lower';
                    $("#lower-ajax-loader").show();

                    $.ajax({
                        url: "{{route('admin.productstage.LowerajaxListing')}}",
                        type: "get",
                        data: {
                            id: id,
                            eType: eType,
                            action: 'delete'
                        },
                        success: function(response) {
                            $("#lower_table_record").html(response);
                            $("#lower-ajax-loader").hide();
                            location.reload();
                        }
                    });
                }
            })
    });
    $(document).on('click', '.lowersort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');
        var eType = 'Lower';


        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#lower-ajax-loader").show();

        $.ajax({
            url: "{{route('admin.productstage.LowerajaxListing')}}",
            type: "get",
            data: {
                column: column,
                order,
                order,
                eType: eType,
                action: 'sort'
            },
            success: function(response) {
                console.log(response);
                $("#lower_table_record").html(response);
                $("#lower-ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '.ajax_page_lower', function() {
        pages = $(this).data("pages");
        var eType = 'Lower';
        $("#lower_table_record").html('');
        $("#lower-ajax-loader").show();

        $.ajax({
            url: "{{route('admin.productstage.LowerajaxListing')}}",
            type: "get",
            data: {
                pages: pages,
                eType: eType,
            },
            success: function(response) {
                $("#lower_table_record").html(response);
                $("#lower-ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#Category', function() {
        var eType = 'Lower';
        var Category = $("#Category").val();
        $("#lower-ajax-loader").show();

        $.ajax({
            url: "{{route('admin.productstage.LowerajaxListing')}}",
            type: "get",
            data: {
                Category: Category,
                eType: eType,
            },
            success: function(response) {
                $("#lower_table_record").html(response);
                $("#lower-ajax-loader").hide();
            }
        });
        $.ajax({
            url:"{{route('admin.productstage.getproduct')}}",
            type: "post",
            data: {
                iCategoryId: Category,
                _token: '{{csrf_token()}}' 
            },
            success: function(result){

                $('#CategoryProduct').selectize()[0].selectize.destroy();
                $('#CategoryProduct').html(result);
                $('#CategoryProduct').selectize(); 

                CategoryProduct = $("#CategoryProduct").val();

                $.ajax({
                    url: "{{route('admin.productstage.LowerajaxListing')}}",
                    type: "get",
                    data: {
                        CategoryProduct: CategoryProduct,
                        eType: eType,
                    },
                    success: function(response) {
                        $("#lower_table_record").html(response);
                        $("#lower-ajax-loader").hide();
                    }
                });


                // $('#CategoryProduct').selectize()[0].selectize.destroy();
                // $('#CategoryProduct').html('<option value="">Select product</option>'); 
                // $.each(result.categoryproduct,function(key,value){
                //     $("#CategoryProduct").append('<option selected value="'+value.iCategoryProductId+'">'+value.vName+'</option>');
                // });
                // $('#CategoryProduct').selectize();
             }
        });
    });
    $(document).on('change', '#CategoryProduct', function() {
        var eType = 'Lower';
        var CategoryProduct = $("#CategoryProduct").val();
        $("#lower-ajax-loader").show();

        $.ajax({
            url: "{{route('admin.productstage.LowerajaxListing')}}",
            type: "get",
            data: {
                CategoryProduct: CategoryProduct,
                eType: eType,
            },
            success: function(response) {
                $("#lower_table_record").html(response);
                $("#lower-ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#lower_page_limit', function() {
        lower_limit_page = this.value;
        $("#lower_table_record").html('');
        $("#lower-ajax-loader").show();
        url = "{{route('admin.productstage.LowerajaxListing')}}";

        setTimeout(function() {
            $.ajax({
                url: url,
                type: "get",
                data: {
                    lower_limit_page: lower_limit_page
                },
                success: function(response) {
                    $("#lower_table_record").html(response);
                    $("#lower-ajax-loader").hide();
                }
            });
            // hideLoader();
        }, 500);
    });
</script>
@endsection