@if(count($data) > 0)
@foreach($data as $key => $value)
@php
$totalday = $value->iDeliverDay + $value->iPickUpDay + $value->iProcessDay;
@endphp
<tr>
	<!-- <td>
		<div class="form-check form-check-sm form-check-custom form-check-solid">
			<input id="Lower_ProductStage_ID_{{$value->iProductStageId}}" type="checkbox" name="Lower_ProductStage_ID[]" class="form-check-input widget-9-check lcheckboxall" value="{{$value->iProductStageId}}">
			<label for="Lower_ProductStage_ID_{{$value->iProductStageId}}">&nbsp;</label>
		</div>
	</td> -->
	<td>
		{{$value->vCode}}
	</td>
	<td>{{ $value->vName }}</td>
	<td>{{ $totalday }}</td>
	<td>{{ $value->vPrice }}</td>
	<td>{{ $value->iSequence }}</td>
	<td>
		<span class="badge badge-light-success">
			{{$value->eStatus}}
		</span>
	</td>
	<td>
		<div class="d-inline-flex align-items-center">
			@if(\App\Libraries\General::check_permission('','eEdit') == true)
			<a href="{{route('admin.productstage.edit',$value->iProductStageId)}}" class="btn-icon edit_permission edit-icon me-4">
				<i class="fad fa-pencil"></i>
			</a>
			@endif
			@if(\App\Libraries\General::check_permission('','eDelete') == true)
			<a href="javascript:;" id="lowerdelete" data-id="{{$value->iProductStageId}}" class="btn-icon delete-icon delete_permission me-4">
			<i class="fad fa-trash-alt"></i>
			</a>
			@endif
		</div>
	</td>

</tr>
@endforeach
<tr>
	{{-- <td  class="text-center border-right-0">
		<a href="javascript:;" title="Multiple delete" id="lower_delete_btn" class="btn-icon  delete_permission delete-icon">
		<i class="fad fa-trash-alt"></i>
		</a>
	</td> --}}
	<td class="border-0" colspan="5">
		<div class="d-flex">
			{!! $paging !!}
		</div>
	</td>
	{{-- <td class="border-0"></td>
	<td colspan="5" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
	<td colspan="2"  class="text-end border-left-0">
		{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}}  --}}
		<select class="w-100px show-drop" id ="lower_page_limit">
			<option @if (isset(Request()->lower_limit_page) && Request()->lower_limit_page == 10)
				selected
				@endif value="10">10</option>
				<option @if (isset(Request()->lower_limit_page) && Request()->lower_limit_page == 20)
				selected
				@endif value="20">20</option>
				<option @if (isset(Request()->lower_limit_page) && Request()->lower_limit_page == 50)
					selected
				@endif value="50">50</option>
				<option @if (isset(Request()->lower_limit_page) && Request()->lower_limit_page == 100)
					selected
				@endif value="100">100</option>
		</select>
	</td>
</tr>
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif