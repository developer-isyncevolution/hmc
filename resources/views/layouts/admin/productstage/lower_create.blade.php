@extends('layouts.admin.index')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<div class="main-panel">
  <div class="card">
    <div class="col-lg-12 mx-auto">
      <div class="card mb-5 mb-xl-4">
        <div class="card-header border-0">
            <ul class="d-flex justify-content-start my-2 list-unstyled flex-wrap">
                <li class="">
                    <a href="{{route('admin.category')}}" class=" add-btn btn mx-2">
                        Categories
                    </a>
                </li>
                <li class="">
                    <a href="{{route('admin.categoryproduct')}}" class=" btn add-btn  mx-2">
                        Products
                    </a>
                </li>
                <li>
                  <a href="{{route('admin.productstage')}}" class="active btn add-btn mx-2">
                      Stages
                  </a>
                </li>
                <li>
                  <a href="{{route('admin.addoncategory')}}" class="btn add-btn mx-2">
                    Categories Add ons
                  </a>
                </li>
                <li>
                  <a href="{{route('admin.subaddoncategory')}}" class="btn add-btn mx-2">
                    Add ons
                  </a>
                </li>
            </ul>
        </div>
    </div>
          <div class="card-header">
            <h4 class="card-title" id="row-separator-basic-form">{{isset($productstage->iProductStageId) ? 'Edit' : 'Add'}} Lower Stage</h4>
            
          </div>
        <form action="{{route('admin.productstage.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($productstage)) {{$productstage->iProductStageId}} @endif">
            <input type="hidden" name="eType" value="Lower">
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label >Category</label>
            <select name="iCategoryId" id="iCategoryId" class="form-control">
              <option value="none">Select category</option>
              @foreach($category as $key => $categorys)
              <option value="{{$categorys->iCategoryId}}" @if(isset($productstage)){{$productstage->iCategoryId == $categorys->iCategoryId  ? 'selected' : ''}} @endif>{{$categorys->vName}}</option>
              @endforeach
            </select>
            <div class="text-danger" style="display: none;" id="iCategoryId_error">Please select category</div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label >Product Category</label>
            <select name="iCategoryProductId" id="iCategoryProductId" class="form-control">
              <option value="none">Select product</option>
              @foreach($categoryproduct as $key => $categoryproducts)
              <option value="{{$categoryproducts->iCategoryProductId}}" @if(isset($productstage)){{$productstage->iCategoryProductId == $categoryproducts->iCategoryProductId  ? 'selected' : ''}} @endif>{{$categoryproducts->vName}}</option>
              @endforeach
            </select>
            <div class="text-danger" style="display: none;" id="iCategoryProductId_error">Please select product</div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Code</label>
            <input type="text" class="form-control" id="vCode" name="vCode" placeholder="Code" value="@if(old('vCode')!=''){{old('vCode')}}@elseif(isset($productstage->vCode)){{$productstage->vCode}}@else{{old('vCode')}}@endif">
            <div class="text-danger" style="display: none;" id="vCode_error">Please enter code</div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Name</label>
            <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($productstage->vName)){{$productstage->vName}}@else{{old('vName')}}@endif">
            <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Daye To Pick Up</label>
            <input type="number" class="form-control" id="iPickUpDay" name="iPickUpDay" placeholder="pick up day" value="@if(old('iPickUpDay')!=''){{old('iPickUpDay')}}@elseif(isset($productstage->iPickUpDay)){{$productstage->iPickUpDay}}@else{{old('iPickUpDay')}}@endif">
            <div class="text-danger" style="display: none;" id="iPickUpDay_error">Please enter pick up day</div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Days To Process</label>
            <input type="number" class="form-control" id="iProcessDay" name="iProcessDay" placeholder="Process day" value="@if(old('iProcessDay')!=''){{old('iProcessDay')}}@elseif(isset($productstage->iProcessDay)){{$productstage->iProcessDay}}@else{{old('iProcessDay')}}@endif">
            <div class="text-danger" style="display: none;" id="iProcessDay_error">Please enter process day</div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Days To Deliver</label>
            <input type="number" class="form-control" id="iDeliverDay" name="iDeliverDay" placeholder="Deliver day" value="@if(old('iDeliverDay')!=''){{old('iDeliverDay')}}@elseif(isset($productstage->iDeliverDay)){{$productstage->iDeliverDay}}@else{{old('iDeliverDay')}}@endif">
            <div class="text-danger" style="display: none;" id="iDeliverDay_error">Please enter deliver day</div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Price</label>
            <div id="toolbar-container"></div>
            <input type="number" class="form-control" id="vPrice" name="vPrice" placeholder="Price" value="@if(old('vPrice')!=''){{old('vPrice')}}@elseif(isset($productstage->vPrice)){{$productstage->vPrice}}@else{{old('vPrice')}}@endif">              
            <div id="vPrice_error" class="text-danger" style="display: none;">Please enter price </div>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Status</label>
            <select id="eStatus" name="eStatus" class="form-control">
                <option value="Active" @if(isset($productstage)) @if($productstage->eStatus == 'Active') selected @endif @endif>Active</option>
                <option value="Inactive" @if(isset($productstage)) @if($productstage->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
            </select>
          </div>
          <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Sequence</label>
            <div id="toolbar-container"></div>
            <input type="number" class="form-control" id="iSequence" name="iSequence" placeholder="Sequence" value="@if(old('iSequence')!=''){{old('iSequence')}}@elseif(isset($productstage->iSequence)){{$productstage->iSequence}}@else{{old('iSequence')}}@endif">
            <div id="iSequence_error" class="text-danger" style="display: none;">Please enter Sequence </div>
          </div>
          <div class="row c-label-wrapper">
            <div class="col-lg-2">
              <div class="left-side-check-wrapper">
                <ul>
                  <li>
                    <label class="form-check">
                      <input class="form-check-input" type="checkbox" value="1">
                      <span class="form-check-label">Extractions:</span>
                    </label>
                  </li>
                  <li>
                    <label class="form-check">
                      <input class="form-check-input" type="checkbox" value="1">
                      <span class="form-check-label">Gum Shades:</span>
                    </label>
                  </li>
                  <li>
                    <label class="form-check">
                      <input class="form-check-input" type="checkbox" value="1">
                      <span class="form-check-label">Teeth Shades:</span>
                    </label>
                  </li>
                  <li>
                    <label class="form-check">
                      <input class="form-check-input" type="checkbox" value="1">
                      <span class="form-check-label">Impressions:</span>
                    </label>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-10">
    
              <div class="row ">
                <div class="col-md-2 mb-1">
                  <a href="javascript:;">
                    <div class="input-group">
                      <label class="form-control inner-label te-in-month" value="" id="upper_in_mouth"> TEETH IN MOUTH </label>
                      <input type="hidden" id="upper_in_mouth_value" value="">
                    </div>
                  </a>
    
                  <div class="check-d-requird">
                    <ul class="p-0 m-0 mt-3">
                      <li>
                        <label class="form-check">
                          <input class="form-check-input" type="checkbox" value="1">
                          <span class="form-check-label">Default</span>
                        </label>
                      </li>
                      <li>
                        <label class="form-check">
                          <input class="form-check-input" type="checkbox" value="1">
                          <span class="form-check-label">Requird:</span>
                        </label>
                      </li>
                    </ul>
                  </div>
    
                </div>
                <div class="col-md-2 mb-1">
                  <div class="input-group">
                    <label class="form-control inner-label missing-te" value="" id="upper_missing_teeth"> MISSING TEETH </label>
                    <input type="hidden" id="upper_missing_teeth_value" value="">
                  </div>
                  <div class="check-d-requird">
                    <ul class="p-0 m-0 mt-3">
                      <li>
                        <label class="form-check">
                          <input class="form-check-input" type="checkbox" value="1">
                          <span class="form-check-label">Default</span>
                        </label>
                      </li>
                      <li>
                        <label class="form-check">
                          <input class="form-check-input" type="checkbox" value="1">
                          <span class="form-check-label">Requird:</span>
                        </label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-2 mb-1">
                  <div class="input-group">
                    <label class="form-control inner-label ex-dekivery" value="" id="upper_ectract_delivery"> WILL EXTRACT ON DELIVERY </label>
                    <input type="hidden" id="upper_ectract_delivery_value" value="">
                  </div>
                  <div class="check-d-requird">
                    <ul class="p-0 m-0 mt-3">
                      <li>
                        <label class="form-check">
                          <input class="form-check-input" type="checkbox" value="1">
                          <span class="form-check-label">Default</span>
                        </label>
                      </li>
                      <li>
                        <label class="form-check">
                          <input class="form-check-input" type="checkbox" value="1">
                          <span class="form-check-label">Requird:</span>
                        </label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-2 mb-1">
                  <div class="input-group">
                    <label class="form-control inner-label ben-extracted" value="" id="upper_been_extracted"> HAS BEEN EXTRACTED </label>
                    <input type="hidden" id="upper_been_extracted_value" value="">
                  </div>
                  <div class="check-d-requird">
                    <ul class="p-0 m-0 mt-3">
                      <li>
                        <label class="form-check">
                          <input class="form-check-input" type="checkbox" value="1">
                          <span class="form-check-label">Default</span>
                        </label>
                      </li>
                      <li>
                        <label class="form-check">
                          <input class="form-check-input" type="checkbox" value="1">
                          <span class="form-check-label">Requird:</span>
                        </label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-2 mb-1">
                  <div class="input-group">
                    <label class="form-control inner-label fix-add" value="" id="upper_fix"> FIX OR ADD</label>
                    <input type="hidden" id="upper_fix_value" value="">
                  </div>
                  <div class="check-d-requird">
                    <ul class="p-0 m-0 mt-3">
                      <li>
                        <label class="form-check">
                          <input class="form-check-input" type="checkbox" value="1">
                          <span class="form-check-label">Default</span>
                        </label>
                      </li>
                      <li>
                        <label class="form-check">
                          <input class="form-check-input" type="checkbox" value="1">
                          <span class="form-check-label">Requird:</span>
                        </label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-2 mb-1">
                  <div class="input-group">
                    <label class="form-control inner-label clasps" value="" id="upper_clasps"> CLASPS</label>
                    <input type="hidden" id="upper_clasps_value" value="">
                  </div>
                  <div class="check-d-requird">
                    <ul class="p-0 m-0 mt-3">
                      <li>
                        <label class="form-check">
                          <input class="form-check-input" type="checkbox" value="1">
                          <span class="form-check-label">Default</span>
                        </label>
                      </li>
                      <li>
                        <label class="form-check">
                          <input class="form-check-input" type="checkbox" value="1">
                          <span class="form-check-label">Requird:</span>
                        </label>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
            <div class="col-12 align-self-end d-inline-block ">
                <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
                <a href="{{route('admin.productstage')}}" class="btn back-btn me-2">Back</a>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
   <script>
     $('#eStatus').select2({
        width: '100%',
        placeholder: "Select customer",
        allowClear: true
    });
     $('#iCategoryId').select2({
        width: '100%',
        placeholder: "Select category",
        allowClear: true
    });
     $('#iCategoryProductId').select2({
        width: '100%',
        placeholder: "Select product",
        allowClear: true
    });
    $('#eType').select2({
        width: '100%',
        placeholder: "Select customer",
        allowClear: true
    });
    $('#eClaspAdd').select2({
        width: '100%',
        placeholder: "Select customer",
        allowClear: true
    });
  $(document).on('click','#submit',function()
  {
    iProductStageId = $("#id").val();
    iCategoryId     = $("#iCategoryId").val();
    iCategoryProductId= $("#iCategoryProductId").val();
    vCode           = $("#vCode").val();
    vName           = $("#vName").val();
    vPrice          = $("#vPrice").val();
    iPickUpDay      = $("#iPickUpDay").val();
    iProcessDay     = $("#iProcessDay").val();
    iDeliverDay     = $("#iDeliverDay").val();
    iSequence       = $("#iSequence").val();

    var error = false;

    if(vCode.length == 0){
      $("#vCode_error").show();
      error = true;
    } else {
      $("#vCode_error").hide();
    }
    if(iCategoryId == "none"){
      $("#iCategoryId_error").show();
      error = true;
    } else {
      $("#iCategoryId_error").hide();
    }
    if(iCategoryProductId == "none"){
      $("#iCategoryProductId_error").show();
      error = true;
    } else {
      $("#iCategoryProductId_error").hide();
    }
    if (iSequence.length == 0) {
      $("#iSequence_error").show();
      error = true;
    } else {
      $("#iSequence_error").hide();
    }
    if(vName.length == 0){
      $("#vName_error").show();
      error = true;
    } else {
      $("#vName_error").hide();
    }
    if(vPrice.length == 0){
      $("#vPrice_error").show();
      error = true;
    } else {
      $("#vPrice_error").hide();
    }
    if(iPickUpDay.length == 0){
      $("#iPickUpDay_error").show();
      error = true;
    } else {
      $("#iPickUpDay_error").hide();
    }
    if(iProcessDay.length == 0){
      $("#iProcessDay_error").show();
      error = true;
    } else {
      $("#iProcessDay_error").hide();
    }
    if(iDeliverDay.length == 0){
      $("#iDeliverDay_error").show();
      error = true;
    } else {
      $("#iDeliverDay_error").hide();
    }
    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
    </script>
@endsection
