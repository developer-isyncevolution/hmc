@if(count($data) > 0)
<option value="">Select category</option>
@foreach($data as $key => $value)
	<option value="{{$value->iCategoryId}}">{{$value->vName}}</option>
@endforeach
@else
	<option value="">No record found</option>
@endif