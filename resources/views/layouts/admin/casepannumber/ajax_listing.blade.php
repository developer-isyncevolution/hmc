@if(count($data) > 0)
@foreach($data as $key => $value)

<tr>
	<td>
		<div class="form-check form-check-sm form-check-custom form-check-solid">
			<input id="CasepanNumber_ID_{{$value->iCasepanNumberId}}" type="checkbox" name="CasepanNumber_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iCasepanNumberId}}">
			<label for="CasepanNumber_ID_{{$value->iCasepanNumberId}}">&nbsp;</label>
		</div>
	</td>
	<td class="text-center value-name">
		<span class="badge" style="background-color:{{ $value->vColor }};">		
		</span>
		<label>
		{{$value->vNumber}}
		</label>
	</td>
	<td>{{$value->eUse == "Yes" ? "No" : "Yes"}}</td>
	<td>
	<div class="d-inline-flex align-items-center">
		@if(\App\Libraries\General::check_permission('','eEdit') == true)
		<a href="{{route('admin.casepannumber.edit',[$value->iCasepanId,$value->iCasepanNumberId])}}" class="btn-icon edit_permission edit-icon me-4">
		<i class="fad fa-pencil"></i>
		</a>
		@endif
		@if(\App\Libraries\General::check_permission('','eDelete') == true)
		<a href="javascript:;" id="delete" data-id="{{$value->iCasepanNumberId}}" class="btn-icon delete-icon delete_permission me-4">
		<i class="fad fa-trash-alt"></i>
		</a>
		@endif
</div>
	</td>

</tr>
@endforeach
<tr>
	<td  class="text-center border-right-0">
		<a href="javascript:;" title="Multiple delete" id="delete_btn" class="btn-icon  delete_permission delete-icon">
		<i class="fad fa-trash-alt"></i>
		</a>
	</td>
	<td class="border-0" colspan="2">
		<div class="d-flex">
			{!! $paging !!}
		</div>
	</td>
	{{-- <td class="border-0"></td>
	<td  class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
	<td   class="text-end border-left-0">
		{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}}  --}}
		<select class="w-100px show-drop" id ="page_limit">
			<option @if ((isset(Request()->limit_page) && Request()->limit_page == 10) || (isset($limit) && $limit ==10))
				selected
				@endif value="10">10</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 20) || (isset($limit) && $limit ==20))
				selected
				@endif value="20">20</option>
				<option  @if ((isset(Request()->limit_page) && Request()->limit_page == 50) || (isset($limit) && $limit ==50) )
					selected 
				@endif value="50">50</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 100) || (isset($limit) && $limit ==100))
					selected
				@endif value="100">100</option>
		</select>
	</td>
</tr>
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif