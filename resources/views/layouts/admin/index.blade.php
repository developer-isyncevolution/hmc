@php
$admin = \App\Libraries\General::admin_info();

$general_favicon = \App\Libraries\General::setting_info('Company');
// dd($general_favicon);
$favicon         = $general_favicon['COMPANY_FAVICON']['vValue'];
$general_info    = \App\Libraries\General::setting_info('Appearance');
@endphp
<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
		<base href="">
		@if(\App\Libraries\General::admin_info()['customerType'] == 'Super Admin')
		<title>{{$general_info['CPANEL_TITLE']['vValue']}}</title>
		@else
		<title>{{$admin['CustomerName']}}</title>
		@endif
		<meta charset="utf-8" />
		<meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}; {{ route('admin.logout', ['url' =>urlencode(url()->current())]) }}" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="icon" type="image/ico" href="{{asset('uploads/logo/'.$favicon)}}">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		@include('layouts.admin.css')
		
		<!-- inc css -->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" data-kt-aside-minimize="on" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed on_off_left" data-id="on" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->


				<!-- inc left -->
				@include('layouts.admin.left')
				

				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					

					<!-- inc header -->
					@include('layouts.admin.header')


					<!--end::Header-->
						
						<!-- contenrt inc -->
						@yield('content')


					<!--begin::Footer-->
					<!-- inc footer -->
					@include('layouts.admin.footer')


					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
	
		<script>var hostUrl = "assets/";</script>
		<!--begin::Global Javascript Bundle(used by all pages)-->
			<!-- js inc -->
			@include('layouts.admin.js')
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
	
		@yield('custom-js')
			@if ($message = Session::get('error'))
			<script>
				$( window ).on( "load", function() {
					notification_error("{{$message}}"); 
				});
			</script>
		@endif
		@if ($message = Session::get('success'))
			<script>
				$( window ).on( "load", function() {
					notification_success("{{$message}}"); 
				});
			</script>
		@endif
		{{--  For Front end side add/edit/delete permission start --}}
		@include('layouts.admin.crud_permission_js')
		{{-- For Front end side add/edit/delete permission end --}}
	</body>
	<!--end::Body-->
	</html>

	<script>
		// For Front end side add/edit/delete permission start
		
	
	$('#kt_aside_toggle').click(function(){
	
		const on_off_left = $('.on_off_left').attr('data-kt-aside-minimize');

		if(on_off_left !='' && on_off_left != 'undefind' && on_off_left =='on')
		{
			var set_left_menu_flag = 'off';
		}
		else
		{
			var set_left_menu_flag = 'on';
		}
		localStorage.setItem('check_show_flag', set_left_menu_flag);
	});
	$(document).ready(function() {
		
		var check_show_flag = localStorage.getItem('check_show_flag');
		
		if(check_show_flag == 'on')
		{
			$('.on_off_left').attr('data-kt-aside-minimize','on');
		}
		else
		{
			$('.on_off_left').attr('data-kt-aside-minimize','');
		}
	});
	// $(document).ready(function() {
	// 	var status = $(".on_off_left").data('id');
	// 	$.ajax({
    //         url: "{{route('admin.dashboard.index_data')}}",
    //         type: "get",
    //         data: {
    //             status: status,
    //         },
    //         success: function(response) {
	// 			if(response == 1){
	// 				$('.on_off_left').attr('data-kt-aside-minimize'.text(""));
	// 			}else if(response == 0){
	// 				$('.on_off_left').attr('data-kt-aside-minimize'.text("on"));

	// 			}
                
    //         }
    //     });
	// });
	// $(document).on('click', '#kt_aside_toggle', function() {
	// 	var status = $(".on_off_left").data('id');
	// 	$.ajax({
    //         url: "{{route('admin.dashboard.index_data')}}",
    //         type: "get",
    //         data: {
    //             status: status,
    //         },
    //         success: function(response) {
	// 			if(response == 1){
	// 			console.log(response);

	// 			}else{
	// 			console.log(response);

	// 			}
                
    //         }
    //     });
	// });
	
	
</script>