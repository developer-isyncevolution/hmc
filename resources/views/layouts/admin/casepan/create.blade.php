@extends('layouts.admin.index')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<div class="main-panel">
  <div class="page-title text-center">
    <h3>
      {{isset($casepans->iCasepanId) ? 'Edit' : 'Add'}} Casepan
    </h3>
</div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
          {{-- <div class="card-header">
            <h4 class="card-title" id="row-separator-basic-form"></h4>
            
          </div> --}}
        <form action="{{route('admin.casepan.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($casepans)) {{$casepans->iCasepanId}} @endif">
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label >Name</label>
                <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($casepans->vName)){{$casepans->vName}}@else{{old('vName')}}@endif">
                <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
              <input type="radio"  class="color_radio" id="withcolor" name="color" value="withcolor">
                <label for="withcolor">With Color</label><br>
              <input type="radio"  class="color_radio" id="withoutcolor" name="color" value="withoutcolor">
                <label for="withoutcolor">Without Color</label><br>
            </div>
            @if(isset($casepans->vColor))
            <div class="col-xxl-4 col-lg-6 col-md-12" id="color_select">
              <label>Color</label>
              <input type="text" class="form-control" id="vColor" name="vColor" placeholder="Color" value="@if(old('vColor')!=''){{old('vColor')}}@elseif(isset($casepans->vColor)){{$casepans->vColor}}@else{{old('vColor')}}@endif">
            </div>
            @else 
            <div class="col-xxl-4 col-lg-6 col-md-12" id="color_select"  style="display: none" >
              <label>Color</label>
              <input type="text" class="form-control" id="vColor" name="vColor" placeholder="Color" value="@if(old('vColor')!=''){{old('vColor')}}@elseif(isset($casepans->vColor)){{$casepans->vColor}}@else{{old('vColor')}}@endif">
            </div>
            @endif
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Quantity</label>
                <input type="text" class="form-control" id="iQuantity" name="iQuantity" placeholder="Quantity" value="@if(old('iQuantity')!=''){{old('iQuantity')}}@elseif(isset($casepans->iQuantity)){{$casepans->iQuantity}}@else{{old('iQuantity')}}@endif">
                <div class="text-danger" style="display: none;" id="iQuantity_error">Please enter quantity</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Sequence</label>
                <input type="text" class="form-control" id="iSequence" name="iSequence" placeholder="Sequence" value="@if(old('iSequence')!=''){{old('iSequence')}}@elseif(isset($casepans->iSequence)){{$casepans->iSequence}}@else{{old('iSequence')}}@endif">
                <div class="text-danger" style="display: none;" id="iSequence_error">Please enter sequence</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Status</label>
                <select id="eStatus" name="eStatus" class="form-control">
                    <option value="Active" @if(isset($casepans)) @if($casepans->eStatus == 'Active') selected @endif @endif>Active</option>
                    <option value="Inactive" @if(isset($casepans)) @if($casepans->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
                </select>
                <div class="mt-1">
                    @error('eStatus')
                        <code>{{ $message }}</code>
                    @enderror
                </div>
            </div>       
            <div class="col-12 align-self-end d-inline-block ">
                <a type="submit"  class="submit btn submit-btn me-2">Submit</a>
                <a href="{{route('admin.casepan')}}" class="btn back-btn me-2">Back</a>
               
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
   $('#eStatus').selectize();
     $(document).ready(function() {

        $('input[name=color]').prop('checked', true);

     });
     $(document).on('click', '.color_radio', function() {
        var color = $('input[name="color"]:checked').val();
        if(color == "withcolor"){
          $("#color_select").show();
          error = true;
        } else {
          $("#color_select").hide();
        }
     });
  $('#vColor').colorpicker();
  $(document).on('click','.submit',function()
  {
    
    iCasepanId = $("#id").val();
    vName           = $("#vName").val();
    iQuantity       = $("#iQuantity").val();
    iSequence       = $("#iSequence").val();
    
    var error = false;  

    if(vName.length == 0){
      $("#vName_error").show();
      error = true;
    } else {
      $("#vName_error").hide();
    }
    if(iQuantity.length == 0){
      $("#iQuantity_error").show();
      error = true;
    } else {
      $("#iQuantity_error").hide();
    }
    if(iSequence.length == 0){
      $("#iSequence_error").show();
      error = true;
    } else {
      $("#iSequence_error").hide();
    }
    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
    </script>
@endsection
