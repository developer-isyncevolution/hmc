@extends('layouts.admin.index')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<div class="main-panel">
  <div class="page-title text-center">
    <h3>
      {{isset($categorys->iCategoryId) ? 'Edit' : 'Add'}} category
    </h3>
  </div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
      <div class="card mb-5 mb-xl-4">
        <div class="card-header border-0">
          <ul class="c-nav-tab">
            <li class="">
              <a href="{{route('admin.category')}}" class="active add-btn btn mx-2">
                Categories
              </a>
            </li>
            <li class="">
              <a href="{{route('admin.categoryproduct')}}" class="btn add-btn  mx-2">
                Products
              </a>
            </li>
            <li>
              <a href="{{route('admin.productstage')}}" class="btn add-btn mx-2">
                Stages
              </a>
            </li>
            <li>
              <a href="{{route('admin.addoncategory')}}" class="btn add-btn mx-2">
                Categories Add ons
              </a>
            </li>
            <li>
              <a href="{{route('admin.subaddoncategory')}}" class="btn add-btn mx-2">
                Add ons
              </a>
            </li>
          </ul>
        </div>
      </div>
      <!-- <div class="card-header">
            <h4 class="card-title" id="row-separator-basic-form"></h4>            
          </div> -->
      <form action="{{route('admin.category.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($categorys)) {{$categorys->iCategoryId}} @endif">
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Name</label>
          <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($categorys->vName)){{$categorys->vName}}@else{{old('vName')}}@endif">
          <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Casepan</label>
          <select name="iCasepanId" id="iCasepanId" class="form-control">
            <option value="none">Select casepan</option>
            @foreach($casepan as $key => $casepans)
            <option value="{{$casepans->iCasepanId}}" style="background-color:{{$casepans->vColor}}" @if(isset($categorys)){{$categorys->iCasepanId == $casepans->iCasepanId  ? 'selected' : ''}} @endif>
              <td style="background-color:{{ $casepans->vColor }}">{{$casepans->vName}}
            </option>
            @endforeach
          </select>
          <div class="text-danger" style="display: none;" id="iCasepanId_error">Please select casepan</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Status</label>
          <select id="eStatus" name="eStatus" class="form-control">
            <option value="Active" @if(isset($categorys)) @if($categorys->eStatus == 'Active') selected @endif @endif>Active</option>
            <option value="Inactive" @if(isset($categorys)) @if($categorys->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
          </select>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Pick Up Time</label>
          <input type="time" class="form-control" id="tPickTime" name="tPickTime" placeholder="Name" value="@if(old('tPickTime')!=''){{old('tPickTime')}}@elseif(isset($categorys->tPickTime)){{$categorys->tPickTime}}@else{{old('tPickTime')}}@endif">
          <div class="text-danger" style="display: none;" id="tPickTime_error">Please enter time</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Delivery Time</label>
          <input type="time" class="form-control" id="tDeliveryTime" name="tDeliveryTime" placeholder="Name" value="@if(old('tDeliveryTime')!=''){{old('tDeliveryTime')}}@elseif(isset($categorys->tDeliveryTime)){{$categorys->tDeliveryTime}}@else{{old('tDeliveryTime')}}@endif">
          <div class="text-danger" style="display: none;" id="tDeliveryTime_error">Please enter time</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Sequence</label>
          <div id="toolbar-container"></div>
          <input type="number" class="form-control" id="iSequence" name="iSequence" placeholder="Sequence" value="@if(old('iSequence')!=''){{old('iSequence')}}@elseif(isset($categorys->iSequence)){{$categorys->iSequence}}@else{{old('iSequence')}}@endif">

          <div id="iSequence_error" class="text-danger" style="display: none;">Please enter Sequence </div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12 align-self-center">
          <label class="d-block mb-2">Category Type</label>
          <input type="radio" class="color_radio d-inline-block" id="Upper" name="eType" value="Upper" @if(isset($categorys)) @if($categorys->eType == 'Upper') checked @endif @endif>
          <label for="Upper" class="me-2">Upper</label>
          <input type="radio" class="color_radio d-inline-block" id="Lower" name="eType" value="Lower" @if(isset($categorys)) @if($categorys->eType == 'Lower') checked @endif @endif>
          <label for="Lower" class="me-2">Lower</label>
          <input type="radio" class="color_radio d-inline-block" id="Both" name="eType" value="Both" @if(isset($categorys)) @if($categorys->eType == 'Both') checked @endif @endif>
          <label for="Both" class="me-2">Both</label>
          <div class="text-danger" style="display: none;" id="eType_error">Please select category type</div>
        </div>
        <div class="col-12 align-self-end d-inline-block ">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.category')}}" class="btn back-btn me-2">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
   $('#eStatus').selectize();
   
  $('#eType').select2({
    width: '100%',
    placeholder: "Select customer",
    allowClear: true
  });
  $(document).on('click', '#submit', function() {
    iCategoryId = $("#id").val();
    tDeliveryTime = $("#tDeliveryTime").val();
    tPickTime = $("#tPickTime").val();
    iQuantity = $("#iQuantity").val();
    vName = $("#vName").val();
    iSequence = $("#iSequence").val();
    iCasepanId = $("#iCasepanId").val();
    eType = $('input[name="eType"]:checked').val();

    var error = false;

    if ($('input[name="eType"]:checked').length == 0) {
      $("#eType_error").show();
      error=true;
     }else {
      $("#eType_error").hide();
    }

    if (iCasepanId == "none") {
      $("#iCasepanId_error").show();
      error = true;
    } else {
      $("#iCasepanId_error").hide();
    }
    if (tPickTime.length == 0) {
      $("#tPickTime_error").show();
      error = true;
    } else {
      $("#tPickTime_error").hide();
    }
    if (tDeliveryTime.length == 0) {
      $("#tDeliveryTime_error").show();
      error = true;
    } else {
      $("#tDeliveryTime_error").hide();
    }
    if (iSequence.length == 0) {
      $("#iSequence_error").show();
      error = true;
    } else {
      $("#iSequence_error").hide();
    }
    if (vName.length == 0) {
      $("#vName_error").show();
      error = true;
    } else {
      $("#vName_error").hide();
    }
    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
</script>
@endsection