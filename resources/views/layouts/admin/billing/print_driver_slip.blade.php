<style type="text/css">
    
    @page { margin: 30px 5px !important; }

    @font-face {
    font-family: 'Verdana';
    src: url({{ public_path('admin/assets/fonts/verdana.ttf') }}) format("truetype");
    font-weight: 400; 
    font-style: normal; 
    }
    body {
        font-family: "Verdana !important";
      }

</style>
<!DOCTYPE html>
<html style="margin: 0px">
<body style="margin: 0px;">

    
{{-- <table style="width:100%;padding: 5px; margin-top:11px;"> --}}
<table style="width:100%;margin-top:14px;padding-left: 15px;">
    <tbody>
      
@if(isset($show_history) && !empty($show_history) && count($show_history)>0)
    @foreach ($show_history as $key => $show_history_inner_val)
        @php 
        $margin=4;
        // $margin=35;
        
        @endphp 
    @if($key != 0 && $key%4 ==0)
           @php 
            $margin=14;
           @endphp   
           @elseif($key != 0)
           @php 
            $margin=12;
           @endphp 
    @endif
        <tr style="padding: 0px;margin: 0px;">
            @foreach($show_history_inner_val as $show_history_val)
            
            <td style="padding: 0px;margin: 0px; ">
            <table class="table" style="border: 0px solid;width: 366px; height:165px;padding: 10px;margin: 0px; margin-top:{{$margin}}px;" > 
                <tbody>
                    <tr style="text-align: center;font-size: 16px;line-height: 23px;font-family: Verdana; ">
                        <td style="width:180px;border: 0px solid;text-align: right;">
                            Driver's Slip 
                        </td>   
                        <td rowspan="2" style="border: 0px solid;white-space: nowrap;text-align:right; width:70px;">@php $qr_code = route('admin.labcase.view_driverhistory_by_qr',[$show_history_val['iCaseId'],$show_history_val['iSlipId']]); @endphp
                            <a style="text-align:right;display:inline-block;padding: 0px;" target="_blank" href="https://chart.googleapis.com/chart?chs=500x500&cht=qr&choe=UTF-8&chl={{$qr_code}}">
                                <img  src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&choe=UTF-8&chl={{$qr_code}}" style="height:88px;display:block;margin:0px auto 0px auto;padding: 0px;" download>
                            </a>
                        </td>
                    </tr>
                    <tr style="text-align:left ; line-height: 15px;font-family: Verdana;font-size: 17px;">
                        <td style="white-space: nowrap;" >
                            <span style="text-transform: uppercase;">{{isset($lab_name)?$lab_name:'--'}}</span>
                        </td>
                    </tr>
                    <tr style="text-align:left ; line-height: 17px;">
                        <td ><span style="font-family: Verdana;font-size: 12px;">Ofc: </span><span style="font-family: Verdana;font-size: 14px;">{{isset($show_history_val['vOfficeCode']) && !empty($show_history_val['vOfficeCode'])? substr($show_history_val['vOfficeCode'],0,20):'--'}}</span> 
                        </td>
                        @if(isset($show_history_val['vCasePanNumber']) && !empty($show_history_val['vCasePanNumber']))
                            <td style="white-space: nowrap;text-align:right;" ><span style="font-family: Verdana;font-size: 12px;">Pan # </span><span style="font-family: Verdana;font-size: 14px;">{{$show_history_val['vCasePanNumber']}}</span></td>
                        @else
                            <td style="white-space: nowrap;text-align:right;" ><span style="font-family: Verdana;font-size: 12px;">Pan # </span><span style="font-family: Verdana;font-size: 14px;">--</span></td>
                        @endif
                    </tr>
                    <tr style="text-align:left ;line-height: 18px;">
                        <td style="white-space: nowrap;" ><span style="font-family: Verdana;font-size: 12px;">Dr:</span> <span style="font-family: Verdana;font-size: 14px;">{{isset($show_history_val['vDoctorFirstName']) && isset($show_history_val['vDoctorLastName'])?$show_history_val['vDoctorFirstName'].' '.$show_history_val['vDoctorLastName']:'--'}}</span></td>
                        <td style="white-space: nowrap;text-align:right;" ><span style="font-family: Verdana;font-size: 12px;">Case # </span><span style="font-family: Verdana;font-size: 14px;">{{isset($show_history_val['vCaseNumber'])?$show_history_val['vCaseNumber']:'--'}}</span></td>
                    </tr>
                    <tr style="text-align:left ;line-height: 18px;">
                        <td style="white-space: nowrap;"  ><span style="font-family: Verdana;font-size: 12px;">Pt: </span><span style="font-family: Verdana;font-size: 14px;">{{isset($show_history_val['vPatientName'])?$show_history_val['vPatientName']:'--'}}</span></td>
                        <td style="white-space: nowrap;text-align:right;" ><span style="font-family: Verdana;font-size: 12px;">Slip # </span><span style="font-family: Verdana;font-size: 14px;">{{isset($show_history_val['vSlipNumber'])?$show_history_val['vSlipNumber']:'--'}}</span></td>
                        
                    </tr>
                    <tr style="text-align:left ; line-height: 18px;" >
                        <td style="white-space: nowrap;" colspan="2" style="" >
                            <span style="font-family: Verdana;font-size: 12px;">Pku: </span>
                            <span style="font-family: Verdana;font-size: 14px;white-space: nowrap;">
                                
                                {{isset($show_history_val['dPickUpDate'])?date('m/d/Y',strtotime($show_history_val['dPickUpDate'])):'--'}}
                                
                            </span>
                            &nbsp;&nbsp;&nbsp;
                        <span style="font-family: Verdana;font-size: 12px;">Del :</span><span style="white-space: nowrap;"><span style="font-family: Verdana;font-size: 14px;">{{isset($show_history_val['dDeliveryDate'])?date('m/d/Y',strtotime(trim($show_history_val['dDeliveryDate']))):'--'}} </span><span style="font-family: Verdana;font-size: 12px;">{{isset($show_history_val['tDeliveryTime'])?date('h:i A', strtotime($show_history_val['tDeliveryTime'])):''}}</span></span></td>
                      
                    </tr>
                    
                  
                </tbody>
                {{-- <hr> --}}
            </table>
            </td>
            @endforeach    
        </tr>
    @endforeach
</table>
 </tbody>
@endif
</body>
</html>