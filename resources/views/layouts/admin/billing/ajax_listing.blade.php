@if(count($data) > 0)
@php
$i = 1;
$MainTotal = 0; 
$AllMainTotal = 0; 
@endphp

@foreach ($data as $key => $value)
       
<tr style="margin: 0px;padding:0px;">
	<td >
        <div class="form-check form-check-sm form-check-custom form-check-solid">
            <input id="Billing_Id{{$value->iBillingId}}" type="checkbox" name="Billing_Id[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iBillingId}}">
            <label for="Billing_Id{{$value->iBillingId}}">&nbsp;</label>
        </div>
    </td>
	<td >
		<span
			style="font-weight: normal;color:black;font-size: 12px;">{{ isset($value->vPatientName)?$value->vPatientName:'--' }}
		</span>
	</td>
	<td >
		<span
			style="font-weight: normal;color:black;font-size: 12px;">@if(isset($value->eType) && $value->eType =='Upper')U @elseif(isset($value->eType) && $value->eType =='Lower')L @else -- @endif
		</span>
	</td>
	<td >
		<span
			style="font-weight: normal;color:black;font-size: 12px;">{{isset($value->vProductName)?$value->vProductName:'--'}}
		</span>
	</td>
	<td >
		<span
			style="font-weight: normal;color:black;font-size: 12px;">{{isset($value->vGradeName)?$value->vGradeName:'--'}}
		</span>
	</td>
	<td >
		<span
			style="font-weight: normal;color:black;font-size: 12px;">{{isset($value->vStageName)?$value->vStageName:'--'}}
		</span>
	</td>
	<td >
		@php
			$StageTotal = 0;
		@endphp
		<span
			style="font-weight: normal;color:black;font-size: 12px;"> @if(isset($value->iStageTotalWithRush) &&
			!empty($value->iStageTotalWithRush) &&
			$value->iStageTotalWithRush != null)
			${{ $value->iStageTotalWithRush }}
			@else ${{ $value->iStagePrice }}@endif

			@if(isset($value->iStageTotalWithRush) &&
			!empty($value->iStageTotalWithRush) &&
			$value->iStageTotalWithRush != null)
				@php
				$StageTotal = $value->iStageTotalWithRush 
				@endphp
			@else
				@php
				$StageTotal = $value->iStagePrice
				@endphp
			@endif
			
		</span>
	</td>
	<td >
		<span style="font-weight: normal;color:black;font-size: 12px;"> -- </span>
	</td>
	<td >
		<span
			style="font-weight: normal;color:black;font-size: 12px;">
			@if(isset($value->billing_addons) && count($value->billing_addons)>0)
			@foreach ($value->billing_addons as $value_addon)
				{{isset($value_addon->vAddonCode)?$value_addon->vAddonCode:$value_addon->vSlipAddonsName}}
				@if(count($value->billing_addons)>1)
					
					<br>
				@endif    
			@endforeach
			@else
			<span style="font-weight: normal;color:black;font-size: 12px;">--</span>
			@endif
		</span>
	</td>
	<td >
		<span
			style="font-weight: normal;color:black;font-size: 12px;">
			@if(isset($value->billing_addons) && count($value->billing_addons)>0)
			@foreach ($value->billing_addons as $value_addon)
				{{$value_addon->iAddOnsQty}}
				@if(count($value->billing_addons)>1)
					
					<br>
				@endif   
			@endforeach
			@else
			<span style="font-weight: normal;color:black;font-size: 12px;">--</span>
			@endif
		</span>
	</td>
	<td >
		<span
			style="font-weight: normal;color:black;font-size: 12px;">
			@php
			 $AddonTotal = 0;
			@endphp
			@if(isset($value->billing_addons) && count($value->billing_addons)>0)
			@foreach ($value->billing_addons as $value_addon)
				${{$value_addon->iAddOnsTotal}}
				@if(count($value->billing_addons)>1)
				
					<br>
				@endif   
				@php
				$AddonTotal += $value_addon->iAddOnsTotal;
				@endphp
			@endforeach
			@else
			<span style="font-weight: normal;color:black;font-size: 12px;">--</span>
			@endif
			@php
			 $MainTotal = $AddonTotal + $StageTotal;
			@endphp
		 
		</span>
	</td>
   
	<td >
		<span style="font-weight: normal;color:black;font-size: 12px;">
			{{ date('m/d/Y', strtotime($value->dDeliveryDate)) }}
		</span>
	</td>
	<td >
		<span style="font-weight: normal;color:black;font-size: 12px;" id="Billing_Main_Total{{$value->iBillingId}}" data-price="{{$MainTotal}}" >
			${{$MainTotal}}
			@php
				$AllMainTotal +=$MainTotal;
			@endphp
		</span>
	</td>
	<td >
		<div class="d-inline-flex align-items-center">	
			<a data-toggle="tooltip" data-placement="top" title="Virtual Slip" target="_b" class="slip_redirect" href="{{route('admin.labcase.virtual_slip',[$value->iCaseId,$value->iSlipId])}}" >
				<i class="fal fa-eye" style="color: #054b97;font-size: 18px;"></i>
			</a>
			&nbsp;
			&nbsp;
			<a data-toggle="tooltip" data-placement="top" title="Print Invoice" target="_b" href="{{route('admin.billing.PrintBillingSingleInvoice',$value->iBillingId)}}">
				<i style="font-size: 23px; color:#2c60a5" class="fal fa-file-invoice-dollar" aria-hidden="true"></i>
			</a>
			&nbsp;
			&nbsp;
			@if(\App\Libraries\General::check_permission('BillingController','eEdit') == true)
			<a href="{{route('admin.billing.edit',$value->iBillingId)}}" class="btn-icon me-4  edit_permission edit-icon">
				<i style="font-size: 20px;" class="fad fa-pencil"></i>
			</a>
			@endif
		</div>
	</td>
	

</tr>
@endforeach

	{{-- <td class="border-0"></td>
	<td colspan="0" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
	{{-- <td class="border-0" colspan="12">
		<div class="d-flex">
			{!! $paging !!}
		</div>
	</td> --}}
	<td class="border-0" colspan="12">
	</td>
	<td>Total : <span style="font-weight: 400" class="total_count">$0</span></td>
	{{-- <td colspan="2"  class="text-end border-left-0"> --}}
		{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}}  --}}
		{{-- <select class="w-100px show-drop" id ="page_limit">
			<option @if ((isset(Request()->limit_page) && Request()->limit_page == 10) || (isset($limit) && $limit ==10))
				selected
				@endif value="10">10</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 20) || (isset($limit) && $limit ==20))
				selected
				@endif value="20">20</option>
				<option  @if ((isset(Request()->limit_page) && Request()->limit_page == 50) || (isset($limit) && $limit ==50) )
					selected 
				@endif value="50">50</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 100) || (isset($limit) && $limit ==100))
					selected
				@endif value="100">100</option>
		</select>
	</td> --}}
</tr>
@else
<tr class="text-center">
	<td colspan="14">No Record Found</td>
</tr>
@endif
<script>
			$('.checkboxall').change(function() {
				MainPriceCount();
            });
			$('#selectall').change(function() {
				MainPriceCount();
            });
			function MainPriceCount()
			{
				var total = 0;
				$("input[name='Billing_Id[]']:checked").each( function () {
					
					total_new = $('#Billing_Main_Total'+$(this).val()).attr('data-price');
					total += Number(total_new);
                });
                $('.total_count').text('$'+total)
			}
</script>
