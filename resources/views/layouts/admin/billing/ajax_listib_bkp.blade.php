@if(count($data) > 0)
@php
	$i=1;
@endphp
@foreach($data as $key => $value)

@php
	$billing_product = count($value->billing_product);
@endphp

@if($billing_product == 1)
	@php 
		$product_1_row = count($value->billing_product[0]->billing_addons);
		$total_row = $product_1_row;
	@endphp
@elseif($billing_product == 2)
	@php 

		$product_1_row = count($value->billing_product[0]->billing_addons);
		$product_2_row = count($value->billing_product[1]->billing_addons);
		$total_row = $product_1_row+$product_2_row;
	@endphp
@endif

<tr>
	<td >
        <div class="form-check form-check-sm form-check-custom form-check-solid">
            <input id="Billing_Id{{$value->iBillingId}}" type="checkbox" name="Billing_Id[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iBillingId}}">
            <label for="Billing_Id{{$value->iBillingId}}">&nbsp;</label>
        </div>
    </td>
	<td  >{{$value->vSlipNumber}}<br>{{$value->vPatientName}}</td>
	@if(isset($value->billing_product) && !empty($value->billing_product))
	
		<td style="padding: 0px !important;"> 
			<table style="width: 100%;!important;white-space: nowrap !important;" >
				@foreach ($value->billing_product as $key => $billing_product_val)
				<tr @if($key %2==0) style="" @else style="
				" @endif class="{{$billing_product_val->eType}}ProductHeight{{$i}}">
					<td   style="height: 50px;">{{$billing_product_val->eType.' - '.$billing_product_val->vGradeName}}</td>
				</tr>	
				@endforeach
			</table>
		</td>
	
	@endif
	
	@if(isset($value->billing_product) && !empty($value->billing_product))
	
		<td  style="padding: 0px !important;">
			<table style="width: 100%;!important;white-space: nowrap !important;" >
				@foreach ($value->billing_product as $key => $billing_product_val)
				<tr @if($key %2==0) style="" @else style="
				" @endif class="{{$billing_product_val->eType}}ProductHeight{{$i}}">
					<td data-toggle="tooltip" data-placement="top" title="{{$billing_product_val->vStageName}}" style="height: 50px;">{{$billing_product_val->vProductName.' - '.$billing_product_val->vStageCode}}</td>
				</tr>	
				@endforeach
			</table>
		</td>
	
	@endif
	@if(isset($value->billing_product) && !empty($value->billing_product))
	
		<td style="padding: 0px !important;">
			<table style="width: 100%;!important;white-space: nowrap !important;" >
				@foreach ($value->billing_product as $key => $billing_product_val)
				<tr @if($key %2==0) style="" @else style="
				" @endif class="{{$billing_product_val->eType}}ProductHeight{{$i}}">
					@if(isset($billing_product_val->iStageTotalWithRush) && !empty($billing_product_val->iStageTotalWithRush) && $billing_product_val->iStageTotalWithRush != null)
					<td style="height: 50px;">{{$billing_product_val->iStageTotalWithRush}}</td>
					@else
					<td style="height: 50px;">{{$billing_product_val->iStagePrice}}</td>
					@endif
				</tr>	
				@endforeach
			</table>
		</td>
	
	@endif
	@if(isset($value->billing_product) && !empty($value->billing_product))
		<td style="padding: 0px !important;">
		@foreach ($value->billing_product as $key=> $billing_addonsVal)
		@if(isset($billing_addonsVal->billing_addons) && count($billing_addonsVal->billing_addons) >0 && !empty($billing_addonsVal->billing_addons))
				<table style="width: 100%;!important;white-space: nowrap !important;" class="{{$billing_addonsVal->eType}}AddonsHeight{{$i}}">
					@foreach ($billing_addonsVal->billing_addons as $key_b => $billing_addons_val)
					<tr @if($billing_addonsVal->eType =='Upper') style="" @else style="
					" @endif >
						<td data-toggle="tooltip" data-placement="top" title="{{$billing_addons_val->vSlipAddonsName}}" style="height: 50px;">{{isset($billing_addons_val->vAddonCode)?$billing_addons_val->vAddonCode.' * '.$billing_addons_val->iAddOnsQty:$billing_addons_val->vSlipAddonsName.' * '.$billing_addons_val->iAddOnsQty}}</td>
					</tr>
					@endforeach
				</table>
		@elseif($key !=1)
		--
		@endif	
		@endforeach
		</td>
	@endif

	@if(isset($value->billing_product) && !empty($value->billing_product))
	
		<td style="padding: 0px !important;">
		@foreach ($value->billing_product as $key=> $billing_addonsVal)
		@if(isset($billing_addonsVal->billing_addons) && count($billing_addonsVal->billing_addons)>0)
				<table style="width: 100%;!important;white-space: nowrap !important;" >
					@foreach ($billing_addonsVal->billing_addons as $key_b => $billing_addons_val)
					<tr @if($billing_addonsVal->eType =='Upper') style="" @else style="
					" @endif>
						<td style="height: 50px;">{{$billing_addons_val->iAddOnsTotal}}</td>
					</tr>	
					@endforeach
				</table>
		@elseif($key !=1)
		--		
		@endif	
		@endforeach
		</td>
	@endif
	<td  >{{date("m/d/Y",strtotime($value->dDeliveryDate)); }}</td>
	<td  >{{isset($value->iMainTotal) && !empty($value->iMainTotal)?$value->iMainTotal:'--'}}</td>

	<td >
		<div class="d-inline-flex align-items-center">	
			<a data-toggle="tooltip" data-placement="top" title="Virtual Slip" target="_b" class="slip_redirect" href="{{route('admin.labcase.virtual_slip',[$value->iCaseId,$value->iSlipId])}}" >
				<i class="fas fa-eye" style="color: #054b97;font-size: 18px;"></i>
			</a>
			&nbsp;
			&nbsp;
			<a data-toggle="tooltip" data-placement="top" title="Print Invoice" target="_b" href="{{route('admin.billing.PrintBillingSingleInvoice',$value->iBillingId)}}">
				<i style="font-size: 23px; color:#2c60a5" class="fa fa-print" aria-hidden="true"></i>
			</a>
			&nbsp;
			&nbsp;
			<a href="{{route('admin.billing.edit',$value->iBillingId)}}" class="btn-icon me-4  edit_permission edit-icon">
				<i style="font-size: 20px;" class="fad fa-pencil"></i>
			</a>
		</div>
	</td>

@php
$i++;
@endphp
@endforeach
<tr>

	{{-- <td class="border-0"></td>
	<td colspan="0" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
	<td class="border-0" colspan="8">
		<div class="d-flex">
			{!! $paging !!}
		</div>
	</td>
	<td colspan="2"  class="text-end border-left-0">
		{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}}  --}}
		<select class="w-100px show-drop" id ="page_limit">
			<option @if ((isset(Request()->limit_page) && Request()->limit_page == 10) || (isset($limit) && $limit ==10))
				selected
				@endif value="10">10</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 20) || (isset($limit) && $limit ==20))
				selected
				@endif value="20">20</option>
				<option  @if ((isset(Request()->limit_page) && Request()->limit_page == 50) || (isset($limit) && $limit ==50) )
					selected 
				@endif value="50">50</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 100) || (isset($limit) && $limit ==100))
					selected
				@endif value="100">100</option>
		</select>
	</td>
</tr>
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif

<script>
	$(document).ready(function() {   
		// For dynamic table data start
                for (let i = 1; i <= {{count($data)}}; i++) {
                    $('.UpperProductHeight'+i).css('height',$('.UpperAddonsHeight'+i).height())
                    $('.LowerProductHeight'+i).css('height',$('.LowerAddonsHeight'+i).height())
                }

    	// For dynamic table data end
	   });
	   $(".checkboxall").click(function() {
            if (this.checked) {
				// var action_print = $('#action_print').val();
				// if(action_print =='print_statement')
				// {
				// 	$("#print_form").submit();
				// }
            } 
			else {
                $("#selectall").prop('checked', false);
            }
        });
</script>