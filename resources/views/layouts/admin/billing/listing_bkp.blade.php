@extends('layouts.admin.index')
<link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
@section('content')
    <div class="main-panel">
        <div class="page-title text-center">
            <h3>
                Billing
            </h3>
        </div>
        <div class="col-lg-12 mx-auto">
            <div class="card mb-5 mb-xl-4">
                {{-- <div class="row">
                    <div class="col-lg-4 ms-auto">
                        <div class="custome-serce-box">
                            <input type="text" class="form-control" id="keyword" name="search" placeholder="Search">
                            <span class="search-btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div> --}}
                <div class="row g-3 mt-2">

                    <div class="col-lg-4 mx-auto">
                        @if (\App\Libraries\General::admin_info()['customerType'] == 'Lab Admin')
                            <div class="custome-serce-box">
                                <select name="iOfficeId" id="iOfficeId" class="w-100 CustomerChange">
                                    <option value="">Select Office</option>
                                    @if (isset($office) && !empty($office))
                                        @foreach ($office as $key => $office_value)
                                            <option @if (isset($iChangeId) && $iChangeId == $office_value->iCustomerId) selected @endif
                                                value="{{ $office_value->iCustomerId }}"> {{ $office_value->vOfficeName }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        @else
                            <div class="custome-serce-box">

                                <select name="iLabId" id="iLabId" class="w-100 CustomerChange">
                                    <option value="">Select Lab</option>
                                    @if (isset($labs) && !empty($labs))
                                        @foreach ($labs as $key => $labs_value)
                                            <option @if (isset($iChangeId) && $iChangeId == $labs_value->iCustomerId) selected @endif
                                                value="{{ $labs_value->iCustomerId }}"> {{ $labs_value->vOfficeName }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-4 ms-auto">
                        <div class="custome-serce-box">
                            <input style="height: 37px;" type="text" class="form-control" id="keyword" name="search" placeholder="Search">
                            <span class="search-btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
              
                    <div class="col-lg-4 mx-auto">
                        <div class="custome-serce-box">
                            <select style="height: 37px;" name="action_print" id="action_print" class="form-control w-100">
                                <option value="">Select Action</option>
                                <option value="print_statement">Print Statement</option>
                                <option value="print_invoice">Print Invoice</option>
                               
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 mx-auto">
                        <select style="height: 37px;" name="duration" id="duration" class="form-control custome-serce-box w-100">
                            <option value="Today">Today</option>
                            <option value="Yesterday">Yesterday</option>
                            <option value="ThisWeek">This Week</option>
                            <option value="LastWeek">Last Week</option>
                            <option value="ThisMonth">This Month</option>
                            <option value="LastMonth">Last Month</option>
                            <option value="ThisYear">This Year</option>
                            <option value="LastYear">Last Year</option>
                            <option value="Custom">Custom</option>
                        </select>
                    </div>
                    <div class="col-lg-8 mx-auto HideForDate">
                      
                    </div>
                    
                    <div class="col-lg-3  customeDateShow" style="display: none">
                        <div class="form-group">
                            <input style="height: 37px;" class="form-control changeDate" type="text" placeholder="Start Date" id="startdate" />
                        </div>
                    </div>
                    <div class="col-lg-3  customeDateShow" style="display: none">
                        <div class="form-group">
                            <input style="height: 37px;" class="form-control changeDate" type="text" placeholder="End Date" id="enddate" />
                        </div>
                    </div>
                    <div class="col-lg-2  customeDateShow" style="display: none">
                        <a style="height: 37px;" href="javascript:;" id="SerchSlip" class="btn add-btn mx-2 "> Search</a>
                    </div>
                </div>
            </div>
            <div class="listing-page">
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="w-25px" data-orderable="false">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input class="form-check-input all_checked" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" id="selectall" type="checkbox" name="selectall">
                                        <label for="selectall">&nbsp;</label>
                                    </div>
                                </th>
                                <th scope="col" class="min-w-150px"><a id="vSlipNumber" class=""
                                        data-column="vSlipNumber" data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7">Slip# - Patient</span> </a>
                                </th>
                                <th scope="col" class="min-w-150px"><a id="eType" class="" data-column="eType"
                                        data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7"> Jaw - Grade</span> </a></th>
                                <th scope="col" class="min-w-150px"><a id="vProductName" class=""
                                        data-column="vProductName" data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7"> Product - Stage </span>
                                    </a>
                                </th>
                                <th scope="col" class="min-w-150px"><a id="iToalAddOnsValue" class=""
                                        data-column="iToalAddOnsValue" data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7"> Total </span> </a></th>
                                <th scope="col" class="min-w-150px"><a id="iSlipAddonsName" class=""
                                        data-column="iSlipAddonsName" data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7"> Addon * Qty</span> </a>
                                </th>
                                <th scope="col" class="min-w-150px"><a id="iAddOnsTotal" class=""
                                        data-column="iAddOnsTotal" data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7"> Total </span> </a></th>
                                <th scope="col" class="min-w-150px"><a id="dtAddedDate" class=""
                                        data-column="dtAddedDate" data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7"> Due Date </span> </a></th>
                                <th scope="col" class="min-w-150px"><a id="iMainTotal" class=""
                                        data-column="iMainTotal" data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7"> Total </span> </a></th>
                                <th scope="col" class="min-w-150px"><a href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7"> Actions </span> </a></th>

                            </tr>
                        </thead>
                        <tbody id="table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader" width="250px"
                                height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="{{route('admin.billing.PrintBilling')}}" method="post" id="print_form" target="_blank">
        @csrf
        <input type="hidden" name="checked_id" id="checked_id" class="checked_id">
       
        <input type="hidden" name="duration_val" id="duration_val">
    </form>

    <form action="{{route('admin.billing.PrintBillingInvoice')}}" method="post" id="print_invoice_form" target="_blank">
        @csrf
        <input type="hidden" name="checked_id" id="checked_id" class="checked_id">
        <input type="hidden" name="duration_val" id="duration_val">
    </form>
@endsection

@section('custom-css')
    <style>
    </style>
@endsection

@section('custom-js')
    <script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script>
        // $('#duration').selectize();
        $('#iOfficeId').selectize();
        $('#iLabId').selectize();

        $(document).ready(function() {

            $.ajax({
                url: "{{ route('admin.billing.ajaxListing') }}",
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $("#selectall").click(function() {
            if (this.checked) {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', true);
                });
            } else {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', false);
                });
            }
        });
        

       

        $("#keyword").keyup(function() {
            var keyword = $("#keyword").val();
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.billing.ajaxListing') }}",
                type: "get",
                data: {
                    keyword: keyword,
                    action: 'search'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        // Print Bill start
            $(document).on('change','#action_print',function(){
            var id = [];
                
            $("input[name='Billing_Id[]']:checked").each( function () {
                id.push($(this).val());
            });

            var id = id.join(",");
            var action_print = $('#action_print').val();
            var duration = $('#duration').val();
            if(duration !='Custom')
            {
                $('#duration_val').val(duration);
            }
            if(action_print =='print_statement')
            {
                if (id.length == 0) {
                    alert('Please select records.')
                }else{
                        $('.checked_id').val(id);
                        $("#print_form").submit();
                        return true;
                    }
            }
            if(action_print =='print_invoice')
            {
                if (id.length == 0) {
                    alert('Please select records.')
                }else{
                        $('.checked_id').val(id);
                        $("#print_invoice_form").submit();
                        return true;
                    }
            }
            });
        // Print Bill end
        $(document).on('click', '.sort', function() {
            column = $(this).data("column");
            order = $(this).attr('data-order');


            if (order == "ASC") {
                $(this).attr('data-order', 'DESC');
            } else {
                $(this).attr('data-order', 'ASC');
            }

            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.billing.ajaxListing') }}",
                type: "get",
                data: {
                    column: column,
                    order,
                    order,
                    action: 'sort'
                },
                success: function(response) {
                    console.log(response);
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '.ajax_page', function() {
            pages = $(this).data("pages");

            $("#table_record").html('');
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.billing.ajaxListing') }}",
                type: "get",
                data: {
                    pages: pages
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });
        $(document).on('change', '#page_limit', function() {
            limit_page = this.value;
            $("#table_record").html('');
            $("#ajax-loader").show();
            url = "{{ route('admin.billing.ajaxListing') }}";

            setTimeout(function() {
                $.ajax({
                    url: url,
                    type: "get",
                    data: {
                        limit_page: limit_page
                    },
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
                // hideLoader();
            }, 500);
        });
        // Search by onchange date
        $(document).on('change', '#duration', function() {
            var duration = $("#duration").val();
            if (duration == 'Custom') {
                $('.customeDateShow').show();
                $('.HideForDate').hide();
            } else {
                $('.customeDateShow').hide();
                $('.HideForDate').show();
                var duration = $("#duration").val();
                var keyword = $("#keyword").val();
                $("#ajax-loader").show();
                $.ajax({
                    url: "{{ route('admin.billing.ajaxListing') }}",
                    type: "get",
                    data: {
                        keyword: keyword,
                        duration: duration,
                        action: 'search'
                    },
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
            }
        });
        // $('.changeDate').datepicker();
        $('#startdate').datepicker({
            orientation: "bottom auto",
            
        });
        $('#enddate').datepicker({
            orientation: "bottom auto",
        });
        // var start_date = $('#startdate').val();
        // $('#enddate').datepicker('destroy').datepicker({
        //     orientation: "bottom auto",
        //     dateFormat: "mm/dd/yy",
        //     startDate: new Date(start_date),
        // });
        $('#SerchSlip').click(function() {
            var startdate = $('#startdate').val();
            var enddate = $('#enddate').val();
            $('#duration_val').val(startdate+' - '+enddate);
            if (startdate != '' && enddate != '') {
                $('.customeDateShow').hide();
                var keyword = $("#keyword").val();
                $("#ajax-loader").show();
                $.ajax({
                    url: "{{ route('admin.billing.ajaxListing') }}",
                    type: "get",
                    data: {
                        keyword: keyword,
                        startdate: startdate,
                        enddate: enddate,
                        action: 'search'
                    },
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                        $('.customeDateShow').show();
                    }
                });
            } else if (startdate == '') {
                alert('please select start date')
            } else if (enddate == '') {
                alert('please select end date')
            }
        });

        $(document).on('change', '.CustomerChange', function() {
            var iCustomerId = $(this).val();
            var startdate = $('#startdate').val();
            var enddate = $('#enddate').val();
            var keyword = $("#keyword").val();
            $("#ajax-loader").show();
            $.ajax({
                url: "{{ route('admin.billing.ajaxListing') }}",
                type: "get",
                data: {
                    keyword: keyword,
                    startdate: startdate,
                    enddate: enddate,
                    iChangeId: iCustomerId,
                    action: 'search'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });
    </script>
@endsection
