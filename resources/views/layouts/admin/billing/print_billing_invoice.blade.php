@php
$i = 1;
$MainTotal = 0; 
$AllMainTotal = 0; 
@endphp
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Billing Invoice</title>
   </head>
   <style>
   </style>
   <body style=" font-size: 14px;color: rgb(158, 157, 157);font-family:
      sans-serif;">
      <table style="max-width: 1000px;width: 100%; margin: 0 auto;
         border-collapse: collapse;">
         <tr>
            <td style="width:80%;">
               <div style="width: 150px; height:60px">
                @if (isset($labs->vImage) && !empty($labs->vImage))
                    <img src="{{ public_path('uploads/customer/' . $labs->vImage) }}" alt="logo"
                        style="object-fit: contain;width:100%;height:100%;">
                @endif
               </div>
               <h5 style="margin-left:20px;margin-top: 3px;color:#383838;font-size:12px;font-weight: 400; width:50%">
                {{ isset($labs->vAddress) ? $labs->vAddress : '--' }}{{ isset($labs->vZipCode) ? ', ' . $labs->vZipCode : '' }}
               </h5>
            </td>
            <td style="width:20%;">
               <div>
                  <h3 style="margin-top: 0px; margin-bottom: 8px;font-weight:500;color:black;font-size:22px;">Invoice</h3>
                  <h4 style="margin-top: 0px; margin-bottom: 5px;color:#383838;font-size:14px; font-weight: 400;;">No.11</h4>
               </div>
               <div>
                  <h4 style="margin-top: 0px; margin-bottom: 5px;color:#383838;font-size:14px;font-weight: 400;;">Date : {{date('m/d/Y')}}</h4>
                  {{-- {{isset($duration_month)&& !empty($duration_month) ?$duration_month:''}} --}}
               </div>
            </td>
         </tr>
         <tr>
            <td>
               <div style="width: 150px; height:60px;margin: auto; margin-top: 20px; margin-left: 200px;">
                {{-- @if (isset($labs->vImage) && !empty($labs->vImage))
                    <img src="{{ public_path('uploads/customer/' . $labs->vImage) }}" alt="logo"
                        style="object-fit: contain;width:100%;height:100%;">
                @endif --}}
          

                  <div>
                    <h3 style="margin-top: 0px; margin-bottom: 5px;color:#383838;font-size:15px; font-weight: 400;">{{ isset($office->vOfficeName) ? $office->vOfficeName : '--' }}</h3>
                 </div>
                 <div>
                    <h4 style="margin-top: 0px; margin-bottom: 5px;color:#383838;font-size:11px;font-weight: 400;"> 
                        {{ isset($office->vAddress) ? $office->vAddress : '--' }}{{ isset($office->vZipCode) ? ', ' . $office->vZipCode : '' }}
                    </h4>
                 </div>
                  
               </div>
            </td>
         </tr>
      </table>
      <table style="max-width: 1000px;width: 100%; margin: 0 auto;
         border-collapse: collapse;">
      </table>
      <table
         style="max-width: 1000px;width: 100%; margin: 0 auto;font-size:
         10px; margin-top:
         50px; border-collapse: collapse;border: 2px solid #ebebeb;">
         <tr>
            <td style="padding: 3px;border: 2px solid #ebebeb; background-color: #f3f3f3;">
               <span style="font-weight: 400;color:black;font-size: 11px;">Patient</span>
            </td>
            <td style="padding: 3px;border: 2px solid #ebebeb;background-color: #f3f3f3;">
               <span style="font-weight: 400;color:black;font-size: 11px;">
               U/L
               </span>
            </td>
            <td style="padding: 3px;border: 2px solid #ebebeb;background-color: #f3f3f3;">
               <span style="font-weight: 400;color:black;font-size: 11px;">
               Product
               </span>
            </td>
            <td style="padding: 3px;border: 2px solid #ebebeb;background-color: #f3f3f3;">
               <span style="font-weight: 400;color:black;font-size: 11px;">
               Grade
               </span>
            </td>
            <td style="padding: 3px;border: 2px solid #ebebeb;background-color: #f3f3f3;">
               <span style="font-weight: 400;color:black;font-size: 11px;">
               Stage
               </span>
            </td>
            <td style="padding: 3px;border: 2px solid #ebebeb;background-color: #f3f3f3;">
               <span style="font-weight: 400;color:black;font-size: 11px;">
               Total
               </span>
            </td>
            <td style="padding: 3px;border: 2px solid #ebebeb;background-color: #f3f3f3;">
               <span style="font-weight: 400;color:black;font-size: 11px;">
               R%
               </span>
            </td>
            <td style="padding: 3px;border: 2px solid #ebebeb;background-color: #f3f3f3;" width="15%">
               <span style="font-weight: 400;color:black;font-size: 11px;">
               Add on
               </span>
            </td>
            <td style="padding: 3px;border: 2px solid #ebebeb;background-color: #f3f3f3;">
               <span style="font-weight: 400;color:black;font-size: 11px;">
               Qty
               </span>
            </td>
            <td style="padding: 3px;border: 2px solid #ebebeb;background-color: #f3f3f3;">
               <span style="font-weight: 400;color:black;font-size: 11px;">
               Total
               </span>
            </td>
            <td width="10%" style="padding: 3px;border: 2px solid #ebebeb;background-color: #f3f3f3;">
               <span style="font-weight: 400;color:black;font-size: 11px;">
               Due Date
               </span>
            </td>
            <td style="padding: 3px;border: 2px solid #ebebeb;background-color: #f3f3f3;">
               <span style="font-weight: 400;color:black;font-size: 11px;">
               Total
               </span>
            </td>
         </tr>
       
         @foreach ($data as $key => $value)
       
         <tr style="margin: 0px;padding:0px;">
             <td style="padding: 3px;border: 2px solid #ebebeb;">
                 <span
                     style="font-weight: normal;color:black;">{{ isset($value->vPatientName)?$value->vPatientName:'--' }}
                 </span>
             </td>
             <td style="padding: 3px;border: 2px solid #ebebeb;">
                 <span
                     style="font-weight: normal;color:black;">@if(isset($value->eType) && $value->eType =='Upper')U @elseif(isset($value->eType) && $value->eType =='Lower')L @else -- @endif
                 </span>
             </td>
             <td style="padding: 3px;border: 2px solid #ebebeb;white-space:nowrap">
                 <span
                     style="font-weight: normal;color:black;">{{isset($value->vProductName)?$value->vProductName:'--'}}
                 </span>
             </td>
             <td style="padding: 3px;border: 2px solid #ebebeb;">
                 <span
                     style="font-weight: normal;color:black;">{{isset($value->vGradeName)?$value->vGradeName:'--'}}
                 </span>
             </td>
             <td style="padding: 3px;border: 2px solid #ebebeb;white-space:nowrap">
                 <span
                     style="font-weight: normal;color:black;">{{isset($value->vStageName)?$value->vStageName:'--'}}
                 </span>
             </td>
             <td style="padding: 3px;border: 2px solid #ebebeb;">
                 @php
                     $StageTotal = 0;
                 @endphp
                 <span
                     style="font-weight: normal;color:black;"> @if(isset($value->iStageTotalWithRush) &&
                     !empty($value->iStageTotalWithRush) &&
                     $value->iStageTotalWithRush != null)
                     ${{ $value->iStageTotalWithRush }}
                     @else ${{ $value->iStagePrice }}@endif

                     @if(isset($value->iStageTotalWithRush) &&
                     !empty($value->iStageTotalWithRush) &&
                     $value->iStageTotalWithRush != null)
                         @php
                         $StageTotal = $value->iStageTotalWithRush 
                         @endphp
                     @else
                         @php
                         $StageTotal = $value->iStagePrice
                         @endphp
                     @endif
                     
                 </span>
             </td>
             <td style="padding: 3px;border: 2px solid #ebebeb;">
                 <span style="font-weight: normal;color:black;"> -- </span>
             </td>
             <td style="padding: 3px;border: 2px solid #ebebeb;white-space:nowrap">
                 <span
                     style="font-weight: normal;color:black;">
                     @if(isset($value->billing_addons) && count($value->billing_addons)>0)
                     @foreach ($value->billing_addons as $value_addon)
                         {{isset($value_addon->vAddonCode)?$value_addon->vAddonCode:$value_addon->vSlipAddonsName}}
                         @if(count($value->billing_addons)>1)
                             <br>
                            
                         @endif    
                     @endforeach
                     @else
                     <span style="font-weight: normal;color:black;">--</span>
                     @endif
                 </span>
             </td>
             <td style="padding: 3px;border: 2px solid #ebebeb;">
                 <span
                     style="font-weight: normal;color:black;">
                     @if(isset($value->billing_addons) && count($value->billing_addons)>0)
                     @foreach ($value->billing_addons as $value_addon)
                         {{$value_addon->iAddOnsQty}}
                         @if(count($value->billing_addons)>1)
                             <br>
                           
                         @endif   
                     @endforeach
                     @else
                     <span style="font-weight: normal;color:black;">--</span>
                     @endif
                 </span>
             </td>
             <td style="padding: 3px;border: 2px solid #ebebeb;">
                 <span
                     style="font-weight: normal;color:black;">
                     @php
                      $AddonTotal = 0;
                     @endphp
                     @if(isset($value->billing_addons) && count($value->billing_addons)>0)
                     @foreach ($value->billing_addons as $value_addon)
                         ${{$value_addon->iAddOnsTotal}}
                         @if(count($value->billing_addons)>1)
                             <br>
                            
                         @endif   
                         @php
                         $AddonTotal += $value_addon->iAddOnsTotal;
                         @endphp
                     @endforeach
                     @else
                     <span style="font-weight: normal;color:black;">--</span>
                     @endif
                     @php
                      $MainTotal = $AddonTotal + $StageTotal;
                     @endphp
                  
                 </span>
             </td>
            
             <td style="padding: 3px;border: 2px solid #ebebeb;">
                 <span style="font-weight: normal;color:black;">
                     {{ date('m/d/Y', strtotime($value->dDeliveryDate)) }}
                 </span>
             </td>
             <td style="padding: 3px;border: 2px solid #ebebeb;">
                 <span style="font-weight: normal;color:black;">
                     ${{$MainTotal}}
                     @php
                         $AllMainTotal +=$MainTotal;
                     @endphp
                 </span>
             </td>
             

         </tr>
     @endforeach
      </table>
      <div
         style="max-width: 1000px;width: 100%; margin: 0 auto;font-size:
         12px; margin-top: 20px; border-collapse: collapse">
         <div style="margin-left: auto; width: 400px;">
            <table style="width: 100%;color:black;">
               <tr>
                  <td>
                    @php
                        $dt = strtotime(date('Y-m-d'));
                    @endphp
                     <h5 style="padding: 5px;font-weight: bold;color: #383838; font-size: 12px; margin: 0; text-align: left;">Date Due By :<span> {{date('m/d/Y',strtotime("+1 month", $dt))}}</span></h5>
                     <p style="padding: 1px; margin: 0; color: #383838;text-align: left;">3% late fee will be apply after due date</p>
                  </td>
                  <td>
                     <h6 style="padding: 5px;font-size: 12px;font-weight: 500; margin: 0;color: #383838">Total <span style="margin-left: 20px; color: black;">${{$AllMainTotal}}</span></h6>
                  </td>
               </tr>
               <!-- <tr>
                  <td style="padding: 5px;font-weight: bold;">Total</td>
                  <td style="padding: 5px;">
                    
                    </td>
                  </tr> -->
            </table>
         </div>
      </div>
   </body>
</html>