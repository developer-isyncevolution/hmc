@extends('layouts.admin.index')
@section('content')
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet"> -->

<div class="main-panel">
<div class="page-title text-center">
        <h3>
        {{isset($subaddon->iSubAddCategoryId) ? 'Edit' : 'Add'}} Add Ons
        </h3>
    </div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
      <div class="card mb-5 mb-xl-4">
        <div class="card-header border-0">
            <ul class="c-nav-tab">
                <li class="">
                    <a href="{{route('admin.category')}}" class=" add-btn btn mx-2">
                        Categories
                    </a>
                </li>
                <li class="">
                    <a href="{{route('admin.categoryproduct')}}" class=" btn add-btn  mx-2">
                        Products
                    </a>
                </li>
                <li>
                  <a href="{{route('admin.productstage')}}" class="btn add-btn mx-2">
                      Stages
                  </a>
                </li>
                <li>
                  <a href="{{route('admin.addoncategory')}}" class=" btn add-btn mx-2">
                    Categories Add ons
                  </a>
                </li>
                <li>
                  <a href="{{route('admin.subaddoncategory')}}" class="active btn add-btn mx-2">
                    Add ons
                  </a>
                </li>
            </ul>
        </div>
    </div>         
        <form action="{{route('admin.subaddoncategory.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="iSubAddCategoryId" value="@if(isset($subaddon)) {{$subaddon->iSubAddCategoryId}} @endif">
            <div class="col-xxl-4 col-lg-6 col-md-12 align-self-center">
                <label class="d-block mb-2"> Add on Type</label>
                <input type="radio"  class="color_radio d-inline-block" id="Upper" name="eType" value="Upper" @if(isset($subaddon)) @if($subaddon->eType == 'Upper') checked @endif @endif>
                <label for="Upper" class="me-2">Upper</label>
                <input type="radio"  class="color_radio d-inline-block" id="Lower" name="eType" value="Lower" @if(isset($subaddon)) @if($subaddon->eType == 'Lower') checked @endif @endif>
                <label for="Lower" class="me-2">Lower</label>
                <input type="radio"  class="color_radio d-inline-block" id="Both" name="eType" value="Both" @if(isset($subaddon)) @if($subaddon->eType == 'Both') checked @endif @endif>
                <label for="Both" class="me-2">Both</label>
                <div class="text-danger" style="display: none;" id="eType_error">Please Select Addon type</div>
            </div>    
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Addon Category</label><span> *</span>
                <select name="iAddCategoryId" id="iAddCategoryId">
                    <option value="none">Select First Addon Type</option>
                      
                      @foreach($category as $key => $categorys)
                      <option value="{{$categorys->iAddCategoryId}}" @if(isset($subaddon)){{ $subaddon->iAddCategoryId  == $categorys->iAddCategoryId  ? 'selected' : ''}} @endif>{{$categorys->vCategoryName}}</option>
                      @endforeach
                      
                    <div class="text-danger" style="display: none;" id="iAddCategoryId_error">Please Select Addon Category</div>
                </select>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Addon Name</label><span> *</span>
                <input type="text" class="form-control" id="vAddonName" name="vAddonName" placeholder="Addon Name" value="@if(old('vAddonName')!=''){{old('vAddonName')}}@elseif(isset($subaddon->vAddonName)){{$subaddon->vAddonName}}@else{{old('vAddonName')}}@endif">
                <div class="text-danger" style="display: none;" id="vAddonName_error">Please Enter Addon Name</div>
            </div>
           
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Code</label><span> *</span>
                <input type="text" class="form-control" id="vCode" name="vCode" placeholder="Code" value="@if(old('vCode')!=''){{old('vCode')}}@elseif(isset($subaddon->vCode)){{$subaddon->vCode}}@else{{old('vCode')}}@endif">
                <div class="text-danger" style="display: none;" id="vCode_error">Please Enter Code</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Price</label><span> *</span>
                <input type="number" class="form-control" id="iPrice" name="iPrice" placeholder="Price" value="@if(old('iPrice')!=''){{old('iPrice')}}@elseif(isset($subaddon->iPrice)){{$subaddon->iPrice}}@else{{old('iPrice')}}@endif">
                <div class="text-danger" style="display: none;" id="iPrice_error">Please Enter Price</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Sequence</label><span> *</span>
                <input type="number" class="form-control" id="vSequence" name="vSequence" placeholder="Sequence" value="@if(old('vSequence')!=''){{old('vSequence')}}@elseif(isset($subaddon->vSequence)){{$subaddon->vSequence}}@else{{old('vSequence')}}@endif">
                <div class="text-danger" style="display: none;" id="vSequence_error">Please Enter Sequenc</div>
            </div>
            {{-- <div class="col-xxl-4 col-lg-6 col-md-12">
              <label>Jaws</label>
              @if(isset($subaddon))
              @php $Jaws = explode(',', $subaddon->vJaws); @endphp
              @endif
              <div class="d-flex align-items-center mt-3">
									<label class="formvalue-check form-check-inline form-check-solid me-5">
                    <input class="form-check-input" name="vJaws[]" type="checkbox" value="Maxillary"  @if(isset($subaddon)) @if(in_array("Maxillary", $Jaws)) checked @endif @endif>
                    <span class="fw-bold ps-2 fs-6">Maxillary</span>
                  </label>
							</div>
            </div> --}}
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Status</label>
                <select id="eStatus" name="eStatus">
                    <option value="Active" @if(isset($subaddon)) @if($subaddon->eStatus == 'Active') selected @endif @endif>Active</option>
                    <option value="Inactive" @if(isset($subaddon)) @if($subaddon->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
                </select>
                
            </div>     
             {{-- Office Prices show start --}}
             <hr>
             <div class="col-lg-6 ">
                 <h5 class="pt-3">Office Prices</h5>
                 <div class="listing-page">
                    <div class="table-data table-responsive">
                       <table class="table">
                          <thead>
                             <tr>
                                <th class="min-w-100px">
                                   <span class="text-muted fw-bold text-muted d-block fs-7">Office Name</span>
                                </th>
                                <th class="min-w-100px">
                                   <span class="text-muted fw-bold text-muted d-block fs-7">Office Price</span>
                                </th>
                             </tr>
                          </thead>
                          <tbody>
                             @if(isset($office) && !empty($office) && count($office) >0)
                                 @foreach($office as $key => $office_val)
                                 <tr>
                                     <td>{{isset($office_val->vOfficeName)?$office_val->vOfficeName:'N/A'}}</td>
                                     <td>
                                         <input type="number" name="Office[{{$key}}][vOfficePrice]"  class="form-control vOfficePrice" id="vOfficePrice" value="{{isset($office_val->vOfficePrice)?$office_val->vOfficePrice:''}}" required>
                                     </td>
                                     <input type="hidden" name="Office[{{$key}}][iOfficeId]" value="{{isset($office_val->iCustomerId)?$office_val->iCustomerId:$office_val->iOfficeId}}">
                                     <input type="hidden" name="Office[{{$key}}][vOfficeName]" value="{{$office_val->vOfficeName}}">
                                     <input type="hidden" name="Office[{{$key}}][iOfficePriceId]" value="{{isset($office_val->iOfficePriceId)?$office_val->iOfficePriceId:''}}">
                                 </tr>
                                 @endforeach
                            
                             @endif
                          </tbody>
                       </table>
                    </div>
                 </div>
                 
              </div>
             {{-- Office Prices show end --}}       
            <div class="col-12 align-self-end d-inline-block ">
                <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
                <a href="{{route('admin.subaddoncategory')}}" class="btn back-btn me-2">Back</a>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
   $('#iAddCategoryId').selectize();
   $('#eStatus').selectize();
  //  $('#eType').select2({
  //       width: '100%',
  //       placeholder: "Select customer",
  //       allowClear: true
  //   });
  //   $('#eStatus').select2({
  //       width: '100%',
  //       placeholder: "Select customer",
  //       allowClear: true
  //   });
    // $('#iAddCategoryId').select2({
    //     width: '100%',
    //     placeholder: "Select customer",
    //     allowClear: true
    // });
    $(document).on('click', '.color_radio', function() {
    var eType = $('input[name="eType"]:checked').val();
    $.ajax({
      url: "{{route('admin.subaddoncategory.fetch_category') }}",
      type: "POST",
      data: {
        eType: eType,
        "_token": "{{ csrf_token() }}",
      },
      success: function(response) {
        $('#iAddCategoryId').selectize()[0].selectize.destroy();
        $("#iAddCategoryId").html(response);
        $('#iAddCategoryId').selectize();
      }
    });
  });
  $(document).on('click','#submit',function()
  {
   
    iAddCategoryId             = $("#iAddCategoryId").val();
    vAddonName                  = $("#vAddonName").val();
    vCode                  = $("#vCode").val();
    iPrice                  = $("#iPrice").val();
    vSequence                  = $("#vSequence").val();
    eType = $('input[name="eType"]:checked').val();

    var error = false;

    if ($('input[name="eType"]:checked').length == 0) {
    eType = $('input[name="eType"]:checked').val();
      $("#eType_error").show();
      error=true;
     }else {
      $("#eType_error").hide();
    }
    
    if(iAddCategoryId.length == 0){
      $("#iAddCategoryId_error").show();
      error = true;
    } else {
      $("#iAddCategoryId_error").hide();
    }
    if(vAddonName.length == 0){
      $("#vAddonName_error").show();
      error = true;
    } else {
      $("#vAddonName_error").hide();
    }
    if(vCode.length == 0){
      $("#vCode_error").show();
      error = true;
    } else {
      $("#vCode_error").hide();
    }
    if(iPrice.length == 0){
      $("#iPrice_error").show();
      error = true;
    } else {
      $("#iPrice_error").hide();
    }
    if(vSequence.length == 0){
      $("#vSequence_error").show();
      error = true;
    } else {
      $("#vSequence_error").hide();
    }
    
    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
   //For update office price start
      @if(!isset($subaddon))
        $("#iPrice").keyup(function(){
            $('.vOfficePrice').val(this.value);
        });
      @endif
      //For update office price end
    </script>
@endsection
