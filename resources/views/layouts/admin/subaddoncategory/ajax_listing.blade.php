@if(count($data) > 0)

@foreach($data as $key => $value)

	<tr>
		<!-- <td>
			<div class="form-check form-check-sm form-check-custom form-check-solid">
				<input id="subcategory_ID_{{$value->iSubAddCategoryId}}" type="checkbox" name="subcategory_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iSubAddCategoryId}}">
				<label for="subcategory_ID_{{$value->iSubAddCategoryId}}">&nbsp;</label>
			</div>
		</td> -->
		<td>{{$value->vAddonName}}</td>
		<td>{{$value->vCode}}</td>
		{{-- <td>{{$value->vJaws}}</td> --}}
		<td>{{$value->iPrice}}</td>
		<td>{{$value->vSequence}}</td>
		<td>
			<span class="badge badge-light-success">
				{{$value->eStatus}}
			</span>
		</td>
		<td>
			<div class="d-inline-flex align-items-center">
				@if(\App\Libraries\General::check_permission('','eEdit') == true)
				<a href="{{route('admin.subaddoncategory.edit',$value->iSubAddCategoryId)}}" class="btn-icon me-4  edit_permission edit-icon">
					<i class="fad fa-pencil"></i>
				</a>
				@endif
				@if(\App\Libraries\General::check_permission('','eDelete') == true)
				<a href="javascript:;" id="delete" data-id="{{$value->iSubAddCategoryId}}" class="btn-icon delete-icon delete_permission me-4">
					<i class="fad fa-trash-alt"></i>
				</a>
				@endif
			</div>
		</td>
	</tr>
@endforeach
		<tr>
			{{-- <td  class="text-center border-right-0">
				<a href="javascript:;" title="Multiple delete" id="delete_btn" class="btn-icon  delete_permission delete-icon">
				<i class="fad fa-trash-alt"></i>
				</a>
			</td> --}}			
			<td class="border-0" colspan="4">
				<div class="d-flex">
					{!! $paging !!}
				</div>
			</td>
			{{-- <td class="border-0"></td>
			<td colspan="4" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
			<td colspan="2"  class="text-end border-left-0">
				{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}}  --}}
				<select class="w-100px show-drop" id ="upper_page_limit">
					<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 10)
						selected
						@endif value="10">10</option>
						<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 20)
						selected
						@endif value="20">20</option>
						<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 50)
							selected
						@endif value="50">50</option>
						<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 100)
							selected
						@endif value="100">100</option>
				</select>
			</td>
		</tr>
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif