@extends('layouts.admin.index')
@section('content')
    <div class="main-panel">
        <div class="page-title text-center">
            <h3>
                Lab Admin
            </h3>
        </div>
        <div class="col-lg-12 mx-auto">
            <div class="card mb-5 mb-xl-4">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-mid">
                                <ul class="d-flex list-unstyled">
                                    @if(\App\Libraries\General::check_permission('','eCreate') == true)
                                    <li>
                                        <a href="{{ route('admin.lab_admin.create') }}" class="btn create_permission add-btn me-2">
                                            <i class="fa fa-plus"></i> Add
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    @if(\App\Libraries\General::admin_info()['customerType']=='Super Admin')
                    <div class="col-lg-6 mx-auto">
                        <select name="iCustomerId" id="iCustomerId_list">
                            <option value="">Select Customer</option>
                            @foreach ($customer as $key => $customers)
                                <option value="{{ $customers->iCustomerId }}"
                                    @if (isset($iCustomerId)) {{ $customers->iCustomerId == $iCustomerId ? 'selected' : '' }} @endif>
                                    {{ $customers->vOfficeName }}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    <div class="col-lg-6 mx-auto">
                        <div class="custome-serce-box">
                            <input type="text" class="form-control" id="keyword" name="search" placeholder="Search">
                            <span class="search-btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="listing-page">
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="w-25px" data-orderable="false">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input id="selectall" type="checkbox" name="selectall" class="form-check-input">
                                        <label for="selectall">&nbsp;</label>
                                    </div>
                                </th>
                                <th scope="col" class="min-w-150px"><a id="iCustomerId" class="sort"
                                    data-column="iCustomerId" data-order="ASC" href="#"> <span
                                        class="text-muted fw-bold text-muted d-block fs-7">Customer </span> </a></th>
                                <th scope="col" class="min-w-150px"><a id="vFirstName" class="sort"
                                        data-column="vFirstName" data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7">Name </span> </a></th>
                                <th scope="col" class="min-w-150px"><a id="vEmail" class="sort"
                                        data-column="vEmail" data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7">Email </span> </a></th>
                                <th scope="col" class="min-w-150px"><a id="vMobile" class="sort"
                                        data-column="vMobile" data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7">Mobile </span> </a></th>
                                <th scope="col" class="min-w-150px"><a id="eStatus" class="sort"
                                        data-column="eStatus" data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7"> Status </span> </a></th>
                                <th scope="col" class="max-w-100px"><span
                                        class="text-muted fw-bold text-muted d-block fs-7"> Action </span></th>
                            </tr>
                        </thead>
                        <tbody id="table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader" width="250px"
                                height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <style></style>
@endsection
@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>
    <script>
        $('#iCustomerId_list').selectize();
        $(document).ready(function() {
            var iCustomerId  = '';
            @if(isset($iCustomerId) && !empty($iCustomerId))
            var iCustomerId = {{$iCustomerId}};
            @endif
           
            $.ajax({
                url: "{{ route('admin.lab_admin.ajaxListing') }}",
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}",
                    iCustomerId:iCustomerId
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $("#selectall").click(function() {
            if (this.checked) {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', true);
                });
            } else {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', false);
                });
            }
        });

        $(document).on('click', '#delete_btn', function() {
            var id = [];

            $("input[name='LabAdmin_ID[]']:checked").each(function() {
                id.push($(this).val());
            });

            var id = id.join(",");

            if (id.length == 0) {
                alert('Please select records.')
            } else {
                swal({
                        title: "Are you sure delete all department ?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "{{ route('admin.lab_admin.ajaxListing') }}",
                                type: "get",
                                data: {
                                    id: id,
                                    action: 'multiple_delete'
                                },
                                success: function(response) {
                                    $("#table_record").html(response);
                                    $("#ajax-loader").hide();
                                    notification_error("Department Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                                }
                            });
                        }
                    });
            }
        });

        $("#keyword").keyup(function() {
            var keyword = $("#keyword").val();
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.lab_admin.ajaxListing') }}",
                type: "get",
                data: {
                    keyword: keyword,
                    action: 'search'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '#delete', function() {
            swal({
                    title: "Are you sure delete this department ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        id = $(this).data("id");

                        $("#ajax-loader").show();

                        $.ajax({
                            url: "{{ route('admin.lab_admin.ajaxListing') }}",
                            type: "get",
                            data: {
                                id: id,
                                action: 'delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("Department Deleted Successfully");
                                setTimeout(function() {
                                    location.reload();
                                }, 1000);
                            }
                        });
                    }
                })
        });
        $(document).on('click', '.sort', function() {
            column = $(this).data("column");
            order = $(this).attr('data-order');


            if (order == "ASC") {
                $(this).attr('data-order', 'DESC');
            } else {
                $(this).attr('data-order', 'ASC');
            }

            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.lab_admin.ajaxListing') }}",
                type: "get",
                data: {
                    column: column,
                    order,
                    order,
                    action: 'sort'
                },
                success: function(response) {
                    console.log(response);
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '.ajax_page', function() {
            pages = $(this).data("pages");

            $("#table_record").html('');
            $("#ajax-loader").show();
            $.ajax({
                url: "{{ route('admin.lab_admin.ajaxListing') }}",
                type: "get",
                data: {
                    pages: pages
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });
        $(document).on('change', '#page_limit', function() {
            limit_page = this.value;
            $("#table_record").html('');
            $("#ajax-loader").show();
            url = "{{ route('admin.lab_admin.ajaxListing') }}";

            setTimeout(function() {
                $.ajax({
                    url: url,
                    type: "get",
                    data: {
                        limit_page: limit_page
                    },
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
                // hideLoader();
            }, 500);
        });
        $(document).on('change', '#iCustomerId_list', function() {
        var iCustomerId = $("#iCustomerId_list").val();
            $("#table_record").html('');
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.lab_admin.ajaxListing') }}",
                type: "get",
                data: {
                    iCustomerId: iCustomerId
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });
    </script>
@endsection
