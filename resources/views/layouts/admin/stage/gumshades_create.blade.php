@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
<div class="page-title text-center">
        <h3>
        Add Gum Shades
        </h3>
    </div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
      <div class="card-header border-0">
       <ul class="c-nav-tab">
        <li class="">
            <a href="{{route('admin.stage')}}" class=" add-btn btn mx-2">
              Extractions
            </a>
          </li>  
          <li class="">
            <a href="{{route('admin.impression')}}" class="btn add-btn  mx-2">
              Impression
            </a>
          </li>
          <li>
            <a href="{{route('admin.toothshades')}}" class=" btn add-btn mx-2">
              Tooth Shades
            </a>
          </li>
          <li>
            <a href="{{route('admin.gumshades')}}" class="active btn add-btn mx-2">
              Gum Shades
            </a>
          </li>
          <li>
            <a href="{{route('admin.notes')}}" class="btn add-btn mx-2">
              Stage Notes
            </a>
          </li>
          <li>
            <a href="{{route('admin.rushdate')}}" class="btn add-btn mx-2">
              Rush Dates
            </a>
          </li>
        </ul>
      </div>

      <!-- <div class="card-header">
        @if(isset($gumshades->iGumShadesId))
        <h4 class="card-title" id="row-separator-basic-form">Edit Gum Shades</h4>
        @else
          <h4 class="card-title" id="row-separator-basic-form">Add Gum Shades</h4>
        @endif
      </div> -->


      <form action="{{route('admin.stage.gumshades_store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($gumshades)) {{$gumshades->iGumShadesId}} @endif">
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Brand Name</label>
          <select name="iGumBrandId" id="iGumBrandId">
            <option value="none">Select brand</option>
            <option id="addnew" value="addnew">Add New</option>
               @foreach($brand as $key => $brands)
               <option value="{{$brands->iGumBrandId}}" @if(isset($gumshades)){{$brands->iGumBrandId == $gumshades->iGumBrandId  ? 'selected' : ''}} @endif>{{$brands->vName}}</option>
                @endforeach
          </select>
          <div class="text-danger" style="display: none;" id="iGumBrandId_error">Please select brand</div>
        </div>
        {{-- <div class="col-xxl-4 col-lg-2 col-md-12">
          <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@mdo">Add New</button>
        </div> --}}
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Code</label>
          <input type="text" class="form-control" id="vCode" name="vCode" placeholder="Code" value="@if(old('vCode')!=''){{old('vCode')}}@elseif(isset($gumshades->vCode)){{$gumshades->vCode}}@else{{old('vCode')}}@endif">
          <div class="text-danger" style="display: none;" id="vCode_error">Please enter code</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Name</label>
          <input type="text" class="form-control" id="vGumShade" name="vGumShade" placeholder="Name" value="@if(old('vGumShade')!=''){{old('vGumShade')}}@elseif(isset($gumshades->vGumShade)){{$gumshades->vGumShade}}@else{{old('vGumShade')}}@endif">
          <div class="text-danger" style="display: none;" id="vGumShade_error">Please enter name </div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Status</label>
          <select id="eStatus" name="eStatus" class="form-control">
            <option value="Active" @if(isset($gumshades)) @if($gumshades->eStatus == 'Active') selected @endif @endif>Active</option>
            <option value="Inactive" @if(isset($gumshades)) @if($gumshades->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
          </select>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Sequence</label>
          <input type="number" class="form-control" id="iSequence" name="iSequence" placeholder="Sequence" value="@if(old('iSequence')!=''){{old('iSequence')}}@elseif(isset($gumshades->iSequence)){{$gumshades->iSequence}}@else{{old('iSequence')}}@endif">
          <div class="text-danger" style="display: none;" id="iSequence_error">Please enter Sequence</div>
        </div>
        <div class="col-12 align-self-end d-inline-block">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.gumshades')}}" class="btn back-btn">Back</a>
        </div>
      </form>
      <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">New message</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <form action="{{route('admin.stage.gumbrand_store')}}" id="frm_brand" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                  <label>Brand Name</label>
                  <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="">
                </div>
                <div class="text-danger" style="display: none;" id="vName_error">Enter Name</div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <a type="submit" id="brand_submit" class="btn submit-btn me-2">Add</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
{{-- {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}} --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>
<script>
  $('#eStatus').selectize();
   $('#iGumBrandId').selectize();

   $(document).on('change', '#iGumBrandId', function() {
    iGumBrandId          = $("#iGumBrandId").val();
    if (iGumBrandId== "addnew") {
      $("#exampleModal").modal("show");
    }

  });
    $(document).on('change', '#vImage', function() {

    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#img').attr('src', e.target.result);
      };
      reader.readAsDataURL(this.files[0]);
    }
  });
  $(document).on('click', '#submit', function() {
    id          = $("#id").val();
    vGumShade       = $("#vGumShade").val();
    vCode       = $("#vCode").val();
    iSequence   = $("#iSequence").val();
    iGumBrandId   = $("#iGumBrandId").val();
    
    var error = false;

    if (vGumShade.length == 0) {
      $("#vGumShade_error").show();
      error = true;
    } else {
      $("#vGumShade_error").hide();
    }
    if (iGumBrandId== "none" || iGumBrandId== "addnew") {
      $("#iGumBrandId_error").show();
      error = true;
    } else {
      $("#iGumBrandId_error").hide();
    }
    if (vCode.length == 0) {
      $("#vCode_error").show();
      error = true;
    } else {
      $("#vCode_error").hide();
    }
    if (iSequence.length == 0) {
      $("#iSequence_error").show();
      error = true;
    } else {
      $("#iSequence_error").hide();
    }

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
  $(document).on('click', '#brand_submit', function() {
    id          = $("#id").val();
    vName       = $("#vName").val();
    
    var error = false;

    if (vName.length == 0) {
      $("#vName_error").show();
      error = true;
    } else {
      $("#vName_error").hide();
    }
    // if (vCode.length == 0) {
    //   $("#vCode_error").show();
    //   error = true;
    // } else {
    //   $("#vCode_error").hide();
    // }
    // if (iToothBrandId== "none") {
    //   $("#iToothBrandId_error").show();
    //   error = true;
    // } else {
    //   $("#iToothBrandId_error").hide();
    // }
    // if (iSequence.length == 0) {
    //   $("#iSequence_error").show();
    //   error = true;
    // } else {
    //   $("#iSequence_error").hide();
    // }

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm_brand").submit();
        return true;
      }
    }, 1000);

  });
function validateEmail(vEmail) 
{
  var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  if (filter.test(vEmail)) {
    return true;
  }
  else {
    return false;
  }
}
$(document).ready(function() {
    var mobile = [{ "mask": "### ### ####"}, { "mask": "### ### ####"}];
    $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
   })
  $(document).ready(function() {
  var cellulor = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-####"}];
  $('#vCellulor').inputmask({ 
      mask: cellulor, 
      greedy: false, 
      definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
  })
</script>
@endsection