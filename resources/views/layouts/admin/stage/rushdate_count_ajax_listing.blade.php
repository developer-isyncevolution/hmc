@if(count($data) > 0)
@foreach($data as $key => $value)
<tr>
	<td class="text-center">
		<div class="form-check form-check-sm form-check-custom form-check-solid">
			<input id="Fees_ID_{{$value->iFeesId}}" type="checkbox" name="Fees_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iFeesId}}">
			<label for="Fees_ID_{{$value->iFeesId}}">&nbsp;</label>
		</div>
	</td>
	<td>{{$value->iStagePrice}}</td>
	<td>{{$value->vFee}}</td>
	<td>{{$value->iWorkingTime}}</td>
	<td>{{$value->vRushFee}}</td>
	<td>{{$value->vTotalRush}}</td>
	<td>
		<div class="d-inline-flex align-items-center">
			@if(\App\Libraries\General::check_permission('','eEdit','rush_dates') == 'true')
			<a href="{{route('admin.stage.uppercount_edit',$value->iFeesId)}}" class="btn-icon me-4  edit_permission edit-icon">
			<i class="fad fa-pencil"></i>
			</a>
			@endif
			@if(\App\Libraries\General::check_permission('','eDelete','rush_dates') == 'true')
			<a href="javascript:;" id="delete" data-id="{{$value->iFeesId}}" class="btn-icon delete-icon delete_permission me-4">
			<i class="fad fa-trash-alt"></i>
			</a>
			@endif
	    </div>
	</td>
</tr>
@endforeach
<tr>
	<td colspan="1" class="text-center border-right-0">
		@if(\App\Libraries\General::check_permission('','eDelete','rush_dates') == 'true')
		<a href="javascript:;" title="Multiple delete"  id="delete_btn" data-id="{{$value->iFeesId}}" class="btn-icon  delete_permission delete-icon">
		<i class="fad fa-trash-alt"></i>
		</a>
		@endif
	</td>
	{{-- <td colspan="4" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
	
	
</tr>
@else
<tr class="text-center">
	<td colspan="15">No Record Found</td>
</tr>
@endif