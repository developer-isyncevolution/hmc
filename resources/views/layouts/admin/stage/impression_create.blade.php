@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
<div class="page-title text-center">
        <h3>
        Add impression
        </h3>
    </div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
      <ul class="c-nav-tab">
        <li class="">
            <a href="{{route('admin.stage')}}" class=" add-btn btn mx-2">
              Extractions
            </a>
          </li>  
          <li class="">
            <a href="{{route('admin.impression')}}" class="active btn add-btn  mx-2">
              Impressions
            </a>
          </li>
          <li>
            <a href="{{route('admin.toothshades')}}" class="btn add-btn mx-2">
              Tooth Shades
            </a>
          </li>
          <li>
            <a href="{{route('admin.gumshades')}}" class="btn add-btn mx-2">
              Gum Shades
            </a>
          </li>
          <li>
            <a href="{{route('admin.notes')}}" class="btn add-btn mx-2">
              Stage Notes
            </a>
          </li>
          <li>
            <a href="{{route('admin.rushdate')}}" class="btn add-btn mx-2">
              Rush Dates
            </a>
          </li>
    </ul>
      </ul>
      <!-- <div class="card-header">
        @if(isset($impressions->iImpressionId))
        <ul class="d-flex justify-content-between my-2 list-unstyled">
          
          <h4 class="card-title" id="row-separator-basic-form">Edit impression</h4>
        </ul>
        @else
          <h4 class="card-title" id="row-separator-basic-form"></h4>
        @endif
      </div>
       -->
      <form action="{{route('admin.stage.impression_store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($impressions)) {{$impressions->iImpressionId}} @endif">
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Name</label>
          <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($impressions->vName)){{$impressions->vName}}@else{{old('vName')}}@endif">
          <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Code</label>
          <input type="text" class="form-control" id="vCode" name="vCode" placeholder="Code" value="@if(old('vCode')!=''){{old('vCode')}}@elseif(isset($impressions->vCode)){{$impressions->vCode}}@else{{old('vCode')}}@endif">
          <div class="text-danger" style="display: none;" id="vCode_error">Please enter code</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Status</label>
          <select id="eStatus" name="eStatus" class="form-control">
            <option value="Active" @if(isset($impressions)) @if($impressions->eStatus == 'Active') selected @endif @endif>Active</option>
            <option value="Inactive" @if(isset($impressions)) @if($impressions->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
          </select>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Sequence</label>
          <input type="number" class="form-control" id="iSequence" name="iSequence" placeholder="Sequence" value="@if(old('iSequence')!=''){{old('iSequence')}}@elseif(isset($impressions->iSequence)){{$impressions->iSequence}}@else{{old('iSequence')}}@endif">
          <div class="text-danger" style="display: none;" id="iSequence_error">Please enter Sequence</div>
        </div>
        <div class="col-12 align-self-end d-inline-block">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.impression')}}" class="btn back-btn">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>
<script>
   $('#eStatus').selectize();
 
  $(document).on('change', '#vImage', function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#img').attr('src', e.target.result);
      };
      reader.readAsDataURL(this.files[0]);
    }
  });
  $(document).on('click', '#submit', function() {
    id          = $("#id").val();
    vName       = $("#vName").val();
    vCode       = $("#vCode").val();
    iSequence   = $("#iSequence").val();
    
    var error = false;

    if (vName.length == 0) {
      $("#vName_error").show();
      error = true;
    } else {
      $("#vName_error").hide();
    }
    if (vCode.length == 0) {
      $("#vCode_error").show();
      error = true;
    } else {
      $("#vCode_error").hide();
    }
    if (iSequence.length == 0) {
      $("#iSequence_error").show();
      error = true;
    } else {
      $("#iSequence_error").hide();
    }

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
function validateEmail(vEmail) 
{
  var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  if (filter.test(vEmail)) {
    return true;
  }
  else {
    return false;
  }
}
$(document).ready(function() {
    var mobile = [{ "mask": "### ### ####"}, { "mask": "### ### ####"}];
    $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
   })
  $(document).ready(function() {
  var cellulor = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-####"}];
  $('#vCellulor').inputmask({ 
      mask: cellulor, 
      greedy: false, 
      definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
  })
</script>
@endsection