@if(count($data) > 0)
@foreach($data as $key => $value)
@php
	$totalday = $value->iDeliverDay + $value->iPickUpDay + $value->iProcessDay;
@endphp
<tr>
	<td>
		{{$value->vCode}}
	</td>
	<td>{{ $value->vName }}</td>
	<td>{{ $totalday }}</td>
	<td>{{ $value->vPrice }}</td>

</tr>
@endforeach
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif