@extends('layouts.admin.index')
@section('content')
    <!-- BEGIN: Content-->
    <!-- DOM - jQuery events table -->
    <div class="main-panel">
        <div class="page-title text-center">
            <h3>
                Toothshadesbrand
            </h3>
        </div>
        <div class="col-lg-12 mx-auto">
            <div class="listing-page">

                <div class="card mb-5 mb-xl-4">
                    <div class="card-header border-0">
                        <ul class="c-nav-tab">
                            @if(\App\Libraries\General::check_permission('','eRead','extractions') == 'true')
                            <li class="">
                                <a href="{{route('admin.stage')}}" class=" add-btn btn mx-2">
                                    Extractions
                                </a>
                            </li>
                            @endif
                            @if(\App\Libraries\General::check_permission('','eRead','impressions') == 'true')
                            <li class="">
                                <a href="{{route('admin.impression')}}" class="btn add-btn  mx-2">
                                    Impressions
                                </a>
                            </li>
                            @endif
                            @if(\App\Libraries\General::check_permission('','eRead','tooth_shades') == 'true')
                            <li>
                                <a href="{{route('admin.toothshadesbrand')}}" class="active btn add-btn mx-2 ">
                                    Tooth Shades
                                </a>
                            </li>
                            @endif
                            @if(\App\Libraries\General::check_permission('','eRead','gum_shades') == 'true')
                            <li>
                                <a href="{{route('admin.gumbrand')}}" class="btn add-btn mx-2">
                                    Gum Shades
                                </a>
                            </li>
                            @endif
                            @if(\App\Libraries\General::check_permission('','eRead','stage_notes') == 'true')
                            <li>
                                <a href="{{route('admin.notes')}}" class="btn add-btn mx-2">
                                    Stage Notes
                                </a>
                            </li>
                            @endif
                            @if(\App\Libraries\General::check_permission('','eRead','rush_dates') == 'true')
                            <li>
                                <a href="{{route('admin.rushdate')}}" class="btn add-btn mx-2">
                                    Rush Dates
                                </a>
                            </li>
                            @endif
                        </ul>
                        </ul>
                    </div>
                    {{-- <div class="card-header border-0 justify-content-center">
                    <div class="d-flex w-75 py-3 mx-auto">
                        <div class="col-xxl-4 col-md-12 col-lg-6 me-2">                                         
                            <select name="iToothBrandId" id="iToothBrandId" class="form-control">
                                <option value="">Select brand</option>
                                @foreach ($brand as $key => $brands)
                                <option value="{{$brands->iToothBrandId}}" @if (isset($toothshades)){{$brands->iToothBrandId == $toothshades->iToothBrandId  ? 'selected' : ''}} @endif>{{$brands->vName}}</option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="col-xxl-8 col-md-12 col-lg-6">     
                            <button type="button" id="brand" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Add </button>
                            <button type="button" id="tooth_delete" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal" >Delete </button>
                            <input type="hidden" id="toothbrand_delete" value="">
                        </div>
                    </div>
                </div> --}}

                    <div class="card-header border-0">
                        <ul class="d-flex justify-content-between my-2 list-unstyled py-3 w-100">

                            <li class=" me-2">
                                <a href="{{ route('admin.toothshadesbrand') }}" class="active btn add-btn">Brand
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.toothshades') }}" class=" btn add-btn mx-2">
                                    Tooth Shades
                                </a>
                            </li>
                            <li class="ms-auto me-2">
                                <div class="custome-serce-box">
                                    <input type="text" class="form-control" id="upperkeyword" name="search"
                                        placeholder="Search">
                                    <span class="search-btn"><i class="fas fa-search"></i></span>
                                </div>
                            </li>
                            @if(\App\Libraries\General::admin_info()['customerType']=='Super Admin')
                            <div class="col-lg-3 mx-auto">
                            </div>
                            <div class="col-lg-3 mx-auto">
                                <select name="iCustomerId" id="iCustomerId" class="w-100 filter_change">
                                    <option value="">Select Cutomer</option>
                                    @foreach ($customer as $key => $customers)
                                        <option value="{{ $customers->iCustomerId }}">
                                            {{ $customers->vOfficeName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                            <li class=" me-2">
                                <!-- <a href="javascript:;" id="delete_btn" class="btn btn-danger mx-2"> Multiple Delete</a> -->
                                <a href="{{ route('admin.stage.toothshadesbrand_create') }}" class="btn add-btn create_permission">
                                    <i class="fa fa-plus"></i> Add
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr class="fw-bolder text-muted">
                                <th class="w-25px text-center" data-orderable="false">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input class="form-check-input" type="checkbox" value="1" data-kt-check="true"
                                            data-kt-check-target=".widget-13-check" id="selectall" type="checkbox"
                                            name="selectall">
                                        <label for="selectall">&nbsp;</label>
                                    </div>
                                </th>
                                <th class="min-w-100px">
                                    <a id="vName" class="sort" data-column="vName" data-order="ASC" href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Brand Name</span>
                                    </a>
                                </th>
                                <th class="min-w-100px">
                                    <a id="iSequence" class="sort" data-column="iSequence" data-order="ASC"
                                        href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Sequence</span>
                                    </a>
                                </th>
                                <th class="min-w-100px">
                                    <a id="eStatus" class="sort" data-column="eStatus" data-order="ASC" href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Status</span>
                                    </a>
                                </th>
                                <th class="max-w-100px">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader" width="250px"
                                height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- DOM - jQuery events table -->
    <!-- END: Content-->
@endsection

@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script>
        $('#iCustomerId').selectize();
        $('#iToothBrandId').select2({
            width: '100%',
            placeholder: "Select brand",
            allowClear: true
        });
        $(document).ready(function() {

            $.ajax({
                url: "{{ route('admin.stage.toothshadesbrandajaxListing') }}",
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $("#selectall").click(function() {
            if (this.checked) {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', true);
                });
            } else {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', false);
                });
            }
        });


        $("#upperkeyword").keyup(function() {
            var keyword = $("#upperkeyword").val();

            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.stage.toothshadesbrandajaxListing') }}",
                type: "get",
                data: {
                    keyword: keyword,
                    action: 'search'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '#brand', function() {
            $('#exampleModal').modal('show');
        });
        $(document).on('click', '#delete_btn', function() {
            var id = [];

            $("input[name='ToothBrand_ID[]']:checked").each(function() {
                id.push($(this).val());
            });

            var id = id.join(",");

            if (id.length == 0) {
                alert('Please select records.')
            } else {
                swal({
                        title: "Are you sure delete all Tooth Brand ?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "{{ route('admin.stage.toothshadesbrandajaxListing') }}",
                                type: "get",
                                data: {
                                    id: id,
                                    action: 'multiple_delete'
                                },
                                success: function(response) {
                                    $("#table_record").html(response);
                                    $("#ajax-loader").hide();
                                    notification_error("Tooth Shades Brand Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                                }
                            });
                        }
                    });
            }
        });
        $(document).on('click', '#delete', function() {
            swal({
                    title: "Are you sure delete this Tooth Brand ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        id = $(this).data("id");

                        $("#ajax-loader").show();

                        $.ajax({
                            url: "{{ route('admin.stage.toothshadesbrandajaxListing') }}",
                            type: "get",
                            data: {
                                id: id,
                                action: 'delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("Tooth Shades Brand Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                            }
                        });
                    }
                })
        });
        $(document).on('click', '#tooth_delete', function() {
            var id = $("#toothbrand_delete").val();
            if (id.length == 0) {
                alert('Please select records.')
            } else {
                swal({
                        title: "Are you sure delete this Toothshades Brand ?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "{{ route('admin.stage.ToothshadesDelete') }}",
                                type: "get",
                                data: {
                                    id: id,
                                    "_token": "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    location.reload();
                                }
                            });
                        }
                    })
            }
        });
        $(document).on('click', '.sort', function() {
            column = $(this).data("column");
            order = $(this).attr('data-order');
            iCustomerId = $("#iCustomerId").val();
            iToothBrandId = $("#iToothBrandId").val();
            keyword = $('#upperkeyword').val();

            if (order == "ASC") {
                $(this).attr('data-order', 'DESC');
            } else {
                $(this).attr('data-order', 'ASC');
            }

            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.stage.toothshadesbrandajaxListing') }}",
                type: "get",
                data: {
                    column: column,
                    order,
                    order,
                    action: 'sort',
                    iCustomerId:iCustomerId,
                    iToothBrandId:iToothBrandId,
                    keyword:keyword,
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '.ajax_page', function() {
            pages = $(this).data("pages");

            $("#table_record").html('');
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.stage.toothshadesbrandajaxListing') }}",
                type: "get",
                data: {
                    pages: pages
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });
        $(document).on('change', '#page_limit', function() {
            limit_page = this.value;
            $("#table_record").html('');
            $("#ajax-loader").show();
            url = "{{ route('admin.stage.toothshadesbrandajaxListing') }}";

            setTimeout(function() {
                $.ajax({
                    url: url,
                    type: "get",
                    data: {
                        limit_page: limit_page
                    },
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
                hideLoader();
            }, 500);
        });

        $(document).on('change', '#iToothBrandId', function() {
            // var iDoctorId = $("#iDoctorId").val();
            var iToothBrandId = $("#iToothBrandId").val();


            $("#table_record").html('');
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.stage.toothshadesbrandajaxListing') }}",
                type: "get",
                data: {
                    iToothBrandId: iToothBrandId,
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                    $("#toothbrand_delete").val(iToothBrandId);
                }
            });
        });

        $(document).on('change', '.filter_change', function() {
        // var iDoctorId = $("#iDoctorId").val();
        var iCustomerId = $("#iCustomerId").val();
        var iToothBrandId = $("#iToothBrandId").val();
        $("#table_record").html('');
        $("#ajax-loader").show();

        $.ajax({
            url: "{{ route('admin.stage.toothshadesbrandajaxListing') }}",
            type: "get",
            data: {
                iCustomerId: iCustomerId,
                iToothBrandId: iToothBrandId,
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    }); 
        $(document).on('click', '#submit', function() {
            id = $("#id").val();
            vName = $("#vBrandName").val();
            iSequence = $("#iBrandSequence").val();

            var error = false;

            if (vName.length == 0) {
                $("#vName_error").show();
                error = true;
            } else {
                $("#vName_error").hide();
            }
            if (iSequence.length == 0) {
                $("#iSequence_error").show();
                error = true;
            } else {
                $("#iSequence_error").hide();
            }

            setTimeout(function() {
                if (error == true) {
                    return false;
                } else {
                    $("#frm").submit();
                    return true;
                }
            }, 1000);

        });
    </script>
@endsection
