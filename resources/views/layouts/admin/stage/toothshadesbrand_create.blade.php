@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
<div class="page-title text-center">
        <h3>
        Toothshadesbrand
        </h3>
    </div>


  <div class="card">
    <div class="col-lg-12 mx-auto">
      <div class="card-header border-0">
      <ul class="c-nav-tab">
        <li class="">
            <a href="{{route('admin.stage')}}" class=" add-btn btn mx-2">
              Extractions
            </a>
          </li>  
          <li class="">
            <a href="{{route('admin.impression')}}" class="btn add-btn  mx-2">
              Impression
            </a>
          </li>
          <li>
            <a href="{{route('admin.toothshades')}}" class="active btn add-btn mx-2">
              Tooth Shades
            </a>
          </li>
          <li>
            <a href="{{route('admin.gumshades')}}" class="btn add-btn mx-2">
              Gum Shades
            </a>
          </li>
          <li>
            <a href="{{route('admin.notes')}}" class="btn add-btn mx-2">
              Stage Notes
            </a>
          </li>
          <li>
            <a href="{{route('admin.rushdate')}}" class="btn add-btn mx-2">
              Rush Dates
            </a>
          </li>
    </ul>
      </ul>
      </div>
      <!-- <div class="card-header">
        @if(isset($toothBrands->iToothBrandId))
        <ul class="d-flex justify-content-between my-2 list-unstyled">
          
          <h4 class="card-title" id="row-separator-basic-form">Edit Tooth Shades Brand</h4>
        </ul>
        @else
          <h4 class="card-title" id="row-separator-basic-form">Add Tooth Shades Brand</h4>
        @endif
      </div> -->
      <form action="{{route('admin.stage.toothshadesbrand_store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($toothBrands)) {{$toothBrands->iToothBrandId}} @endif">
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Name</label>
          <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($toothBrands->vName)){{$toothBrands->vName}}@else{{old('vName')}}@endif">
          <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Sequence</label>
          <input type="number" class="form-control" id="iSequence" name="iSequence" placeholder="Sequence" value="@if(old('iSequence')!=''){{old('iSequence')}}@elseif(isset($toothBrands->iSequence)){{$toothBrands->iSequence}}@else{{old('iSequence')}}@endif">
          <div class="text-danger" style="display: none;" id="iSequence_error">Please enter Sequence</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Status</label>
          <select id="eStatus" name="eStatus">
            <option value="Active" @if(isset($toothBrands)) @if($toothBrands->eStatus == 'Active') selected @endif @endif>Active</option>
            <option value="Inactive" @if(isset($toothBrands)) @if($toothBrands->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
          </select>
        </div>
        <div class="col-12 align-self-end d-inline-block">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.toothshadesbrand')}}" class="btn back-btn">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
  $('#eStatus').selectize();
 
  $(document).on('change', '#vImage', function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#img').attr('src', e.target.result);
      };
      reader.readAsDataURL(this.files[0]);
    }
  });
  $(document).on('click', '#submit', function() {
    id          = $("#id").val();
    vName       = $("#vName").val();
    vName       = $("#vName").val();
    iSequence   = $("#iSequence").val();
    
    var error = false;

    // if (vName.length == 0) {
    //   $("#vName_error").show();
    //   error = true;
    // } else {
    //   $("#vName_error").hide();
    // }
    if (vName.length == 0) {
      $("#vName_error").show();
      error = true;
    } else {
      $("#vName_error").hide();
    }
    if (iSequence.length == 0) {
      $("#iSequence_error").show();
      error = true;
    } else {
      $("#iSequence_error").hide();
    }

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
function validateEmail(vEmail) 
{
  var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  if (filter.test(vEmail)) {
    return true;
  }
  else {
    return false;
  }
}
$(document).ready(function() {
    var mobile = [{ "mask": "### ### ####"}, { "mask": "### ### ####"}];
    $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
   })
  $(document).ready(function() {
  var cellulor = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-####"}];
  $('#vCellulor').inputmask({ 
      mask: cellulor, 
      greedy: false, 
      definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
  })
</script>
@endsection