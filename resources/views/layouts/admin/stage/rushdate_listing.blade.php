@extends('layouts.admin.index')
@section('content')
<!-- BEGIN: Content-->
<!-- DOM - jQuery events table -->
<div class="main-panel">
    <div class="page-title text-center">
        <h3>
            Rush Dates
        </h3>
    </div>
    <div class="col-lg-12 mx-auto">
        <div class="listing-page">

            <div class="card mb-5 mb-xl-4">
                <div class="card-header border-0">
                    <ul class="c-nav-tab">
                        @if(\App\Libraries\General::check_permission('','eRead','extractions') == 'true')
                        <li class="">
                            <a href="{{route('admin.stage')}}" class=" add-btn btn mx-2">
                                Extractions
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','impressions') == 'true')
                        <li class="">
                            <a href="{{route('admin.impression')}}" class=" btn add-btn  mx-2">
                                Impressions
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','tooth_shades') == 'true')
                        <li>
                            <a href="{{route('admin.toothshadesbrand')}}" class=" btn add-btn mx-2">
                                Tooth Shades
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','gum_shades') == 'true')
                        <li>
                            <a href="{{route('admin.gumbrand')}}" class="btn add-btn mx-2">
                                Gum Shades
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','stage_notes') == 'true')
                        <li>
                            <a href="{{route('admin.notes')}}" class="btn  add-btn mx-2">
                                Stage Notes
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','rush_dates') == 'true')
                        <li>
                            <a href="{{route('admin.rushdate')}}" class="btn active add-btn mx-2">
                                Rush Dates
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 mx-auto">
                    <div class="card mb-5 mb-xl-4">
                        <h3 class="card-title">
                            Upper Stage
                        </h3>
                               
                        <div class="row">
                            <div class="col-lg-6 mx-auto ">
                                <div class="row g-5">
                                    <div class="col-lg-12">
                                        <select name="iCategoryId" id="iCategoryId" class="form-control">
                                            <option value="none">Select category</option>
                                            @foreach($uppercategory as $key => $uppercategorys)
                                            <option value="{{$uppercategorys->iCategoryId}}" @if(isset($categoryproducts)){{$uppercategorys->iCategoryId == $categoryproducts->iCategoryId  ? 'selected' : ''}} @endif>{{$uppercategorys->vName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-12">
                                        <select name="iCategoryProductId" id="iCategoryProductId" class="form-control">
                                            <option value="none">Select Category First </option>
                                            {{-- @foreach($upperproduct as $key => $upperproducts)
                                      <option value="{{$upperproducts->iCategoryProductId}}" @if(isset($categoryproducts)){{$upperproducts->iCategoryProductId == $categoryproducts->iCategoryProductId  ? 'selected' : ''}} @endif>{{$upperproducts->vName}}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                    <div class="col-lg-12">
                                        <select name="iProductStageId" id="iProductStageId" class="form-control">
                                            <option value="none">Select Product First </option>
                                            {{-- @foreach($upperstage as $key => $upperstages)
                                    <option value="{{$upperstages->iProductStageId}}" @if(isset($categoryproducts)){{$upperstages->iProductStageId == $categoryproducts->iProductStageId  ? 'selected' : ''}} @endif>{{$upperstages->vName}}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="listing-page">
                        <div class="table-data table-responsive" id="uppertable" style="display: none">

                            </a>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"><a id="vCode" class="upersort" data-column="vCode" data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7">Code </span> </a></th>
                                        <th scope="col"><a id="vName" class="upersort" data-column="vName" data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7">Stage </span> </a></th>
                                        <th scope="col"><a id="iSequence" class="upersort" data-column="iSequence" data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7"> Working Time </span> </a></th>
                                        <th scope="col"><a id="vPrice" class="upersort" data-column="vPrice" data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7"> Price </span> </a></th>
                                    </tr>
                                </thead>
                                <tbody id="upper_table_record">
                                </tbody>
                            </table>
                            <div class="text-center loaderimg">
                                <div class="loaderinner">
                                    <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="listing-page">

                        <div class="table-data table-responsive" id="uppercounttable" style="display: none">
                           <form action="{{route('admin.stage.uppercountcreate')}}" method="POST" class="text-center">
                               @csrf
                               <input type="hidden" id="uppercountcreate" name="iProductStageId">
                           <button  class="add-btn create_permission btn mb-3 mx-auto">
                                Add
                           </button>                 
                           </form>
                            
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="w-25px text-center" data-orderable="false">
                                            <div class="form-check form-check-sm form-check-custom form-check-solid">
                                                <input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" id="selectall" type="checkbox" name="selectall">
                                                <label for="selectall">&nbsp;</label>
                                            </div>
                                        </th>
                                        <th scope="col"><a id="vFee" class="upersort" data-column="iStagePrice" data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7">Stage Price </span> </a></th>
                                        <th scope="col"><a id="vFee" class="upersort" data-column="vFee" data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7">% fee </span> </a></th>
                                        <th scope="col"><a id="iWorkingTime" class="upersort" data-column="iWorkingTime" data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7">Working Time </span> </a></th>
                                        <th scope="col"><a id="vRushFee" class="upersort" data-column="vRushFee" data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7"> Rush Fee </span> </a></th>
                                        <th scope="col"><a id="vTotalRush" class="upersort" data-column="vTotalRush" data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7"> Total </span> </a></th>
                                        <th scope="col"><a id="vTotalRush" class="upersort" data-column="vTotalRush" data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7"> Action </span> </a></th>

                                    </tr>
                                </thead>
                                <tbody id="uppercount_table_record">
                                </tbody>
                            </table>
                            <div class="text-center loaderimg">
                                <div class="loaderinner">
                                    <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="count-ajax-loader" width="250px" height="auto" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mx-auto">
                    <div class="card mb-5 mb-xl-4">
                        <h3 class="card-title">
                            Lower Stage
                        </h3>
                        <div class="row">
                            <div class="col-lg-6 mx-auto">
                                <div class="row g-5 ">
                                    <div class="col-lg-12">
                                        <select name="iCategoryId" id="Category" class="form-control">
                                            <option value="none">Select category</option>
                                            @foreach($lowercategory as $key => $lowercategorys)
                                            <option value="{{$lowercategorys->iCategoryId}}" @if(isset($categoryproducts)){{$lowercategorys->iCategoryId == $categoryproducts->iCategoryId  ? 'selected' : ''}} @endif>{{$lowercategorys->vName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-12">
                                        <select name="iCategoryProductId" id="CategoryProduct" class="form-control CategoryProduct_change">
                                            <option value="none">Select Category first </option>
                                            {{-- @foreach($lowerproduct as $key => $lowerproducts)
                                            <option value="{{$lowerproducts->iCategoryProductId}}" @if(isset($categoryproducts)){{$lowerproducts->iCategoryProductId == $categoryproducts->iCategoryProductId  ? 'selected' : ''}} @endif>{{$lowerproducts->vName}}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                    <div class="col-lg-12">
                                        <select name="iProductStageId" id="ProductStage" class="form-control">
                                            <option value="none">Select Product First </option>
                                            {{-- @foreach($lowerstage as $key => $lowerstages)
                                            <option value="{{$lowerstages->iProductStageId}}" @if(isset($categoryproducts)){{$lowerstages->iProductStageId == $categoryproducts->iProductStageId  ? 'selected' : ''}} @endif>{{$lowerstages->vName}}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="listing-page">
                        <div class="table-data table-responsive" id="lowertable" style="display: none">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"><a id="vCode" class="upersort" data-column="vCode" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">Code </span> </a></th>
                                        <th scope="col"><a id="vName" class="upersort" data-column="vName" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">Stage </span> </a></th>
                                        <th scope="col"><a id="iSequence" class="upersort" data-column="iSequence" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Working Time </span> </a></th>
                                        <th scope="col"><a id="vPrice" class="upersort" data-column="vPrice" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Price </span> </a></th>

                                    </tr>
                                </thead>
                                <tbody id="lower_table_record">
                                </tbody>
                            </table>
                            <div class="text-center loaderimg">
                                <div class="loaderinner">
                                    <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="lower-ajax-loader" width="250px" height="auto" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="listing-page">
                        <div class="table-data table-responsive" id="lowercounttable" style="display: none">
                        <form action="{{route('admin.stage.uppercountcreate')}}" method="POST" class="text-center">
                               @csrf
                               <input type="hidden" id="lowercountcreate" name="iProductStageId">
                           <button  class="add-btn btn mb-3 mx-auto">
                                Add
                           </button>                 
                           </form>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="w-25px text-center" data-orderable="false">
                                            <div class="form-check form-check-sm form-check-custom form-check-solid">
                                                <input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" id="selectall" type="checkbox" name="selectall">
                                                <label for="selectall">&nbsp;</label>
                                            </div>
                                        </th>
                                        <th scope="col"><a id="vFee" class="upersort" data-column="iStagePrice" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">Stage Price </span> </a></th>
                                        <th scope="col"><a id="vFee" class="upersort" data-column="vFee" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">% fee </span> </a></th>
                                        <th scope="col"><a id="iWorkingTime" class="upersort" data-column="iWorkingTime" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7">Working Time </span> </a></th>
                                        <th scope="col"><a id="vRushFee" class="upersort" data-column="vRushFee" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Rush Fee </span> </a></th>
                                        <th scope="col"><a id="vTotalRush" class="upersort" data-column="vTotalRush" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Total </span> </a></th>
                                        <th scope="col"><a id="vTotalRush" class="upersort" data-column="vTotalRush" data-order="ASC" href="javascript:;"> <span class="text-muted fw-bold text-muted d-block fs-7"> Action </span> </a></th>

                                    </tr>
                                </thead>
                                <tbody id="lowercount_table_record">
                                </tbody>
                            </table>
                            <div class="text-center loaderimg">
                                <div class="loaderinner">
                                    <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="lowercount-ajax-loader" width="250px" height="auto" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- DOM - jQuery events table -->
<!-- END: Content-->
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;

<script>
    $('#iCategoryId').selectize();
    $('#iCategoryProductId').selectize();
    $('#iCategoryId').selectize();
    $('#Category').selectize();
    $('#CategoryProduct').selectize();
    $('#ProductStage').selectize();
    $('#iProductStageId').selectize();
    

    // $('#iCategoryId').select2({

    //     width: '100%',
    //     placeholder: "Select category",
    //     allowClear: true
    // });
    // $('#iCategoryProductId').select2({
    //     width: '100%',
    //     placeholder: "Select product",
    //     allowClear: true
    // });
    // $('#iProductStageId').select2({
    //     width: '100%',
    //     placeholder: "Select product stage",
    //     allowClear: true
    // });
    // $('#Category').select2({
    //     width: '100%',
    //     placeholder: "Select category",
    //     allowClear: true
    // });
    // $('#CategoryProduct').select2({
    //     width: '100%',
    //     placeholder: "Select product",
    //     allowClear: true
    // });
    // $('#ProductStage').select2({
    //     width: '100%',
    //     placeholder: "Select product stage",
    //     allowClear: true
    // });
    $(document).ready(function() {
        $('#iCategoryId').on('change', function(e) {
            var iCategoryId = $("#iCategoryId").val();
            $.ajax({
                url: "{{route('admin.stage.fetch_categoryproduct') }}",
                type: "POST",
                data: {
                    iCategoryId: iCategoryId,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $('#iCategoryProductId').selectize()[0].selectize.destroy();
                    $("#iCategoryProductId").html(response);
                    $('#iCategoryProductId').selectize();
                    iCategoryProductId = $("#iCategoryProductId").val();
                    
                }
            });
        });
        $('#iCategoryProductId').on('change', function(e) {
            var iCategoryProductId = $("#iCategoryProductId").val();
           $.ajax({
                url: "{{route('admin.stage.fetch_productstage') }}",
                type: "POST",
                data: {
                    iCategoryProductId: iCategoryProductId,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $('#iProductStageId').selectize()[0].selectize.destroy();
                    $("#iProductStageId").html(response);
                    $('#iProductStageId').selectize();
                }
            });
            
        });
        $('#iProductStageId').on('change', function(e) {
            var iProductStageId = $("#iProductStageId").val();
            $("#uppercountcreate").val(iProductStageId);
            
        }); 
        $('#Category').on('change', function(e) {
            var iCategoryId = $("#Category").val();
            $.ajax({
                url: "{{route('admin.stage.fetch_categoryproduct') }}",
                type: "POST",
                data: {
                    iCategoryId: iCategoryId,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $('#CategoryProduct').selectize()[0].selectize.destroy();
                    $("#CategoryProduct").html(response);uppercounttable
                    $('#CategoryProduct').selectize();
                }
            });
        });
        $('#CategoryProduct').on('change', function(e) {
            var iCategoryProductId = $("#CategoryProduct").val();
           
            $.ajax({
                url: "{{route('admin.stage.fetch_productstage') }}",
                type: "POST",
                data: {
                    iCategoryProductId: iCategoryProductId,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $('#ProductStage').selectize()[0].selectize.destroy();
                    $("#ProductStage").html(response);uppercounttable
                    $('#ProductStage').selectize();

                }
            });
        });
    });
    $('#iProductStageId').on('change', function(e) {
        var eType = 'Upper';
        var iProductStageId = $("#iProductStageId").val();
        $.ajax({
            url: "{{route('admin.stage.rushdateajaxListing')}}",
            type: "get",
            data: {
                eType: eType,
                iProductStageId: iProductStageId,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
                $("#uppertable").show();
            }
        });
    });
    $('#iProductStageId').on('change', function(e) {
        var eType = 'Upper';
        var iProductStageId = $("#iProductStageId").val();
        $("#uppercounttable").show();
        $.ajax({
            url: "{{route('admin.stage.rushdatecountajaxListing')}}",
            type: "get",
            data: {
                eType: eType,
                iProductStageId: iProductStageId,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#uppercount_table_record ").html(response);
                $("#count-ajax-loader").hide();
            }
        });
    });
    $('#ProductStage').on('change', function(e) {
        var eType = 'Lower';
        var iProductStageId = $("#ProductStage").val();
        $.ajax({
            url: "{{route('admin.stage.rushdateajaxListing')}}",
            type: "get",
            data: {
                eType: eType,
                iProductStageId: iProductStageId,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#lower_table_record").html(response);
                $("#lower-ajax-loader").hide();
                $("#lowertable").show();
            }
        });
    });
    $('#ProductStage').on('change', function(e) {
            var iProductStageId = $("#ProductStage").val();
            $("#lowercountcreate").val(iProductStageId);
            
        }); 
    $('#ProductStage').on('change', function(e) {
        var eType = 'Lower';
        var iProductStageId = $("#ProductStage").val();
        $("#lowercounttable").show();
        $.ajax({
            url: "{{route('admin.stage.rushdatecountajaxListing')}}",
            type: "get",
            data: {
                eType: eType,
                iProductStageId: iProductStageId,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#lowercount_table_record ").html(response);
                $("#lowercount-ajax-loader").hide();
            }
        });
    });
    $("#selectall").click(function() {
        if (this.checked) {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', true);
            });
        } else {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', false);
            });
        }
    });
    $(document).on('click', '#delete_btn', function() {
        var id = [];

        $("input[name='Fees_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                    title: "Are you sure delete all count ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{route('admin.stage.rushdatecountajaxListing')}}",
                            type: "get",
                            data: {
                                id: id,
                                action: 'multiple_delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("RushDate Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                            }
                        });
                    }
                });
        }
    });
    $(document).on('click', '#delete', function() {
        swal({
                title: "Are you sure delete this count ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");

                    $("#ajax-loader").show();

                    $.ajax({
                        url: "{{route('admin.stage.rushdatecountajaxListing')}}",
                        type: "get",
                        data: {
                            id: id,
                            action: 'delete'
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                            notification_error("RushDate Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                        }
                    });
                }
            })
    });
</script>

{{-- <script>
    $(document).ready(function() {

            $.ajax({
                url: "{{route('admin.stage.ImpressionajaxListing')}}",
type: "get",
data: {
"_token": "{{ csrf_token() }}",
},
success: function(response) {
$("#table_record").html(response);
$("#ajax-loader").hide();
}
});
});

$("#selectall").click(function() {
if (this.checked) {
$('.checkboxall').each(function() {
$(".checkboxall").prop('checked', true);
});
} else {
$('.checkboxall').each(function() {
$(".checkboxall").prop('checked', false);
});
}
});


$("#keyword").keyup(function() {
var keyword = $("#keyword").val();

$("#ajax-loader").show();

$.ajax({
url: "{{route('admin.stage.ImpressionajaxListing')}}",
type: "get",
data: {
keyword: keyword,
action: 'search'
},
success: function(response) {
$("#table_record").html(response);
$("#ajax-loader").hide();
}
});
});

$(document).on('click', '#delete_btn', function() {
var id = [];

$("input[name='Impressiona_ID[]']:checked").each(function() {
id.push($(this).val());
});

var id = id.join(",");

if (id.length == 0) {
alert('Please select records.')
} else {
swal({
title: "Are you sure delete all impressiona.?",
icon: "warning",
buttons: true,
dangerMode: true,
})
.then((willDelete) => {
if (willDelete) {
$.ajax({
url: "{{route('admin.stage.ImpressionajaxListing')}}",
type: "get",
data: {
id: id,
action: 'multiple_delete'
},
success: function(response) {
$("#table_record").html(response);
$("#ajax-loader").hide();
}
});
}
});
}
});
$(document).on('click', '#delete', function() {
swal({
title: "Are you sure delete this impressiona.?",
icon: "warning",
buttons: true,
dangerMode: true,
})
.then((willDelete) => {
if (willDelete) {
id = $(this).data("id");

$("#ajax-loader").show();

$.ajax({
url: "{{route('admin.stage.ImpressionajaxListing')}}",
type: "get",
data: {
id: id,
action: 'delete'
},
success: function(response) {
$("#table_record").html(response);
$("#ajax-loader").hide();
}
});
}
})
});
$(document).on('click', '.sort', function() {
column = $(this).data("column");
order = $(this).attr('data-order');

if (order == "ASC") {
$(this).attr('data-order', 'DESC');
} else {
$(this).attr('data-order', 'ASC');
}

$("#ajax-loader").show();

$.ajax({
url: "{{route('admin.stage.ImpressionajaxListing')}}",
type: "get",
data: {
column: column,
order,
order,
action: 'sort'
},
success: function(response) {
$("#table_record").html(response);
$("#ajax-loader").hide();
}
});
});

$(document).on('click', '.ajax_page', function() {
pages = $(this).data("pages");

$("#table_record").html('');
$("#ajax-loader").show();

$.ajax({
url: "{{route('admin.stage.ImpressionajaxListing')}}",
type: "get",
data: {
pages: pages
},
success: function(response) {
$("#table_record").html(response);
$("#ajax-loader").hide();
}
});
});
$(document).on('change','#page_limit',function()
{
limit_page = this.value;
$("#table_record").html('');
$("#ajax-loader").show();
url = "{{route('admin.stage.ImpressionajaxListing')}}";

setTimeout(function(){
$.ajax({
url: url,
type: "get",
data: {limit_page: limit_page},
success: function(response) {
$("#table_record").html(response);
$("#ajax-loader").hide();
}
});
hideLoader();
}, 500);
});
</script> --}}
@endsection