@extends('layouts.admin.index')
@section('content')
<!-- BEGIN: Content-->
<!-- DOM - jQuery events table -->
<div class="main-panel">
<div class="page-title text-center">
        <h3>
        Stage Notes
        </h3>
    </div>
    <div class="col-lg-12 mx-auto">
        <div class="listing-page">
            <div class="card mb-5 mb-xl-4">
                <div class="card-header border-0">
                    <ul class="c-nav-tab">
                        @if(\App\Libraries\General::check_permission('','eRead','extractions') == 'true')
                        <li class="">
                            <a href="{{route('admin.stage')}}" class=" add-btn btn mx-2">
                                Extractions
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','impressions') == 'true')
                        <li class="">
                            <a href="{{route('admin.impression')}}" class=" btn add-btn  mx-2">
                                Impressions
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','tooth_shades') == 'true')
                        <li>
                            <a href="{{route('admin.toothshadesbrand')}}" class=" btn add-btn mx-2">
                                Tooth Shades
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','gum_shades') == 'true')
                        <li>
                            <a href="{{route('admin.gumbrand')}}" class="btn add-btn mx-2">
                                Gum Shades
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','stage_notes') == 'true')
                        <li>
                            <a href="{{route('admin.notes')}}" class="btn active add-btn mx-2">
                                Stage Notes
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','rush_dates') == 'true')
                        <li>
                            <a href="{{route('admin.rushdate')}}" class="btn  add-btn mx-2">
                                Rush Dates
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="table-data table-responsive">
                <div class="col-lg-12">                   
                    <div id="toolbar-container"></div>
                    <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Description">@if(old('tDescription')!=''){{old('tDescription')}}@elseif(isset($banners->tDescription)){{$banners->tDescription}}@else{{old('tDescription')}}@endif</textarea>
                    <div id="tDescription_error" class="text-danger" style="display: none;">Enter banner description </div>
                </div>  
            </div>
        </div>
    </div>
</div>
<!-- DOM - jQuery events table -->
<!-- END: Content-->
@endsection

@section('custom-js')
<script>
    tinymce.init({
        selector: 'textarea',
        height: 500,
        menubar: true,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help wordcount'
        ],
        toolbar: 'insert | undo redo | formatselect fontselect fontsizeselect | bold italic underline backcolor forecolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat | blockquote | help | code',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        theme_advanced_fonts : "Andale Mono=andale mono,times;"+
            "Arial=arial,helvetica,sans-serif;"+
            "Arial Black=arial black,avant garde;"+
            "Book Antiqua=book antiqua,palatino;"+
            "Comic Sans MS=comic sans ms,sans-serif;"+
            "Courier New=courier new,courier;"+
            "Georgia=georgia,palatino;"+
            "Helvetica=helvetica;"+
            "Impact=impact,chicago;"+
            "Symbol=symbol;"+
            "Tahoma=tahoma,arial,helvetica,sans-serif;"+
            "Terminal=terminal,monaco;"+
            "Times New Roman=times new roman,times;"+
            "Trebuchet MS=trebuchet ms,geneva;"+
            "Verdana=verdana,geneva;"+
            "Webdings=webdings;"+
            "Wingdings=wingdings,zapf dingbats",
        fontsize_formats: '11px 12px 14px 16px 18px 24px 36px 48px',
        textcolor_map: [
            "000000", "Black",
            "993300", "Burnt orange",
            "333300", "Dark olive",
            "003300", "Dark green",
            "003366", "Dark azure",
            "000080", "Navy Blue",
            "333399", "Indigo",
            "333333", "Very dark gray",
            "800000", "Maroon",
            "FF6600", "Orange",
            "808000", "Olive",
            "008000", "Green",
            "008080", "Teal",
            "0000FF", "Blue",
            "666699", "Grayish blue",
            "808080", "Gray",
            "FF0000", "Red",
            "FF9900", "Amber",
            "99CC00", "Yellow green",
            "339966", "Sea green",
            "33CCCC", "Turquoise",
            "3366FF", "Royal blue",
            "800080", "Purple",
            "999999", "Medium gray",
            "FF00FF", "Magenta",
            "FFCC00", "Gold",
            "FFFF00", "Yellow",
            "00FF00", "Lime",
            "00FFFF", "Aqua",
            "00CCFF", "Sky blue",
            "993366", "Red violet",
            "FFFFFF", "White",
            "FF99CC", "Pink",
            "FFCC99", "Peach",
            "FFFF99", "Light yellow",
            "CCFFCC", "Pale green",
            "CCFFFF", "Pale cyan",
            "99CCFF", "Light sky blue",
            "CC99FF", "Plum"
          ],
          file_picker_callback: function(callback, value, meta) 
            {
            if (meta.filetype == 'image') {
            $('#upload').trigger('click');
            $('#upload').on('change', function() {
            var file = this.files[0];
            var reader = new FileReader();
            reader.onload = function(e) {
            callback(e.target.result, {
            alt: ''
            });
            };
            reader.readAsDataURL(file);
            });
            }
            },
    });
</script>
{{-- <script>
    $(document).ready(function() {

            $.ajax({
                url: "{{route('admin.customer.ajaxListing')}}",
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });

    $("#selectall").click(function() {
        if (this.checked) {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', true);
            });
        } else {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', false);
            });
        }
    });


    $("#keyword").keyup(function() {
        var keyword = $("#keyword").val();

        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.customer.ajaxListing')}}",
                type: "get",
                data: {
                    keyword: keyword,
                    action: 'search'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });

    $(document).on('click', '#delete_btn', function() {
        var id = [];

        $("input[name='Customer_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                title: "Are you sure delete all customer.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "{{route('admin.customer.ajaxListing')}}",
                        type: "get",
                        data: {
                            id: id,
                            action: 'multiple_delete'
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                        }
                    });
                } 
            });
        }
    });
    $(document).on('click', '#delete', function() {
        swal({
                title: "Are you sure delete this customer.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");

                    $("#ajax-loader").show();

                        $.ajax({
                            url: "{{route('admin.customer.ajaxListing')}}",
                            type: "get",
                            data: {
                                id: id,
                                action: 'delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                            }
                        });
                }
            })
    });
    $(document).on('click', '.sort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');

        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.customer.ajaxListing')}}",
                type: "get",
                data: {
                    column: column,
                    order,
                    order,
                    action: 'sort'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });

    $(document).on('click', '.ajax_page', function() {
        pages = $(this).data("pages");

        $("#table_record").html('');
        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.customer.ajaxListing')}}",
                type: "get",
                data: {
                    pages: pages
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });
    $(document).on('change','#page_limit',function()
    {
        limit_page = this.value;
        $("#table_record").html('');
        $("#ajax-loader").show();
        url = "{{route('admin.customer.ajaxListing')}}";

        setTimeout(function(){
            $.ajax({
                url: url,
                type: "get",
                data:  {limit_page: limit_page}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            hideLoader();
            }, 500);
    });
</script> --}}
@endsection