@extends('layouts.admin.index')
@section('content')
<!-- BEGIN: Content-->
<!-- DOM - jQuery events table -->
<div class="main-panel">
    <div class="page-title text-center">
        <h3>
            Stage
        </h3>
    </div>
    <div class="col-lg-12 mx-auto">
        <div class="listing-page">
            <div class="card mb-5 mb-xl-4">
                <div class="card-header border-0">
                    <ul class="c-nav-tab">
                        @if(\App\Libraries\General::check_permission('','eRead','extractions') == 'true')
                        <li class="">
                            <a href="" class="active add-btn btn mx-2">
                                Extractions
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','impressions') == 'true')
                        <li class="">
                            <a href="{{route('admin.impression')}}" class="btn add-btn  mx-2">
                                Impressions
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','tooth_shades') == 'true')
                        <li>
                            <a href="{{route('admin.toothshadesbrand')}}" class="btn add-btn mx-2">
                                Tooth Shades
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','gum_shades') == 'true')
                        <li>
                            <a href="{{route('admin.gumbrand')}}" class="btn add-btn mx-2">
                                Gum Shades
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','stage_notes') == 'true')
                        <li>
                            <a href="{{route('admin.notes')}}" class="btn add-btn mx-2">
                                Stage Notes
                            </a>
                        </li>
                        @endif
                        @if(\App\Libraries\General::check_permission('','eRead','rush_dates') == 'true')
                        <li>
                            <a href="{{route('admin.rushdate')}}" class="btn add-btn mx-2">
                                Rush Dates
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>

            <div class="container">
                <div class="teeths-wrapper">
                    <div class="row g-5">
                        <div class="col-lg-6 col-xl-4 teeth-wrapper upper">

                            <h3 class="card-title mb-0" style="margin-top: 20px;">
                                Upper
                            </h3>

                            <svg width="100%" height="100%" viewBox="0 0 1500 822" class="teeth-svg">

                                <g>

                                    <g>
                                        <!-- upper-teeth 5 -->
                                        <a href="javascript:;">
                                            <image id="uppper-5-teeth" data-id="teeths3" data-name="5" x="325" y="202" width="149" height="152" xlink:href="{{asset('admin/assets/images/teeth/up-5.png')}}" class="img-for-teeth teeth_uppper"></image>


                                            <text id="uppper-5-num" data-name="5" class="cls-1 teeth_uppper" transform="translate(405.696 287.468) scale(1.305)">
                                                <tspan x="0">5</tspan>
                                            </text>
                                            <image id="uppper-5-claps" data-id="teeths3" data-name="5" x="315" y="190" xlink:href="{{asset('admin/assets/images/teeth/updc-5.png')}}" class="clapping clap-up-5 teeth_uppper upper-claps" style="display: none"></image>
                                        </a>
                                    </g>
                                    <g>
                                        <!-- upper-teeth 4 -->
                                        <a href="javascript:;">
                                            <image id="uppper-4-teeth" data-id="teeths4" data-name="4" x="266" y="285" width="153" height="149" xlink:href="{{asset('admin/assets/images/teeth/up-4.png')}}" class="img-for-teeth teeth_uppper"></image>

                                            <text id="uppper-4-num" data-name="4" class="cls-1 teeth_uppper" transform="translate(346.65 357.771) scale(1.305)">
                                                <tspan x="0">4</tspan>
                                            </text>
                                            <image id="uppper-4-claps" data-id="teeths4" data-name="4" x="250" y="280" xlink:href="{{asset('admin/assets/images/teeth/updc-4.png')}}" class="clapping clap-up-4 teeth_uppper upper-claps" style="display: none"></image>
                                        </a>
                                    </g>
                                    <g>
                                        <!-- upper-teeth 3 -->
                                        <a href="javascript:;">
                                            <image id="uppper-3-teeth" data-id="teeths7" data-name="3" x="176" y="367" width="220" height="199" xlink:href="{{asset('admin/assets/images/teeth/up-3.png')}}" class="img-for-teeth teeth_uppper"></image>

                                            <text id="uppper-3-num" data-name="3" class="cls-1 teeth_uppper" transform="translate(289.371 467.923) scale(1.305)">
                                                <tspan x="0">3</tspan>
                                            </text>

                                            <image id="uppper-3-claps" data-id="teeths7" data-name="3" x="160" y="367" xlink:href="{{asset('admin/assets/images/teeth/updc-3.png')}}" class="clapping clap-up-3 teeth_uppper upper-claps" style="display: none"></image>
                                        </a>
                                    </g>
                                    <g>
                                        <!-- upper-teeth 2 -->
                                        <a href="javascript:;" class="test">
                                            <image id="uppper-2-teeth" data-id="teeths6" data-name="2" x="115" y="500" width="203" height="188" xlink:href="{{asset('admin/assets/images/teeth/up-2.png')}}" class="img-for-teeth teeth_uppper"></image>

                                            <image id="uppper-2-claps" data-id="teeths6" data-name="2" x="100" y="500" xlink:href="{{asset('admin/assets/images/teeth/updc-2.png')}}" class="clapping clap-up-2 teeth_uppper upper-claps" style="display: none"></image>

                                            <text id="uppper-2-num" data-name="2" class="cls-1 teeth_uppper" transform="translate(224.87 609.191) scale(1.305)">
                                                <tspan x="0">2</tspan>
                                            </text>
                                        </a>
                                    </g>

                                    <g>
                                        <!-- upper-teeth 1 -->
                                        <a href="javascript:;" class="test">

                                            <image id="uppper-1-teeth" data-id="teeths5" data-name="1" x="57" y="640" width="192" height="156" xlink:href="{{asset('admin/assets/images/teeth/up-1.png')}}" class="img-for-teeth teeth_uppper"></image>


                                            <text id="uppper-1-num" data-name="1" data-id="teeths5" class="cls-1 teeth_uppper" transform="translate(163.9 742.356) scale(1.305)">
                                                <tspan x="0">1</tspan>
                                            </text>

                                            <image id="uppper-1-claps" data-id="teeths5" data-name="1" x="40" y="640" xlink:href="{{asset('admin/assets/images/teeth/up-dc-1.png')}}" class="clapping clap-up-1 teeth_uppper upper-claps" style="display: none"></image>

                                        </a>
                                        <!-- upper-teeth 1 end-->
                                    </g>


                                    <g>
                                        <!-- upper-teeth 6 -->
                                        <a href="javascript:;">
                                            <image id="uppper-6-teeth" data-id="teeths2" data-name="6" x="403" y="123" width="136" height="178" xlink:href="{{asset('admin/assets/images/teeth/up-6.png')}}" class="img-for-teeth teeth_uppper"></image>
                                            <image id="uppper-6-claps" data-id="teeths2" data-name="6" x="400" y="105" xlink:href="{{asset('admin/assets/images/teeth/updc-6.png')}}" class="clapping clap-up-6 teeth_uppper upper-claps" style="display: none"></image>

                                            <text id="uppper-6-num" data-name="6" class="cls-1 teeth_uppper" transform="translate(476.76 240.092) scale(1.305)">
                                                <tspan x="0">6</tspan>
                                            </text>
                                        </a>
                                    </g>






                                    <!-- upper-teeth 7 -->
                                    <a href="javascript:;">
                                        <image id="uppper-7-teeth" data-id="teeths1" data-name="7" x="498" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/teeth/up-7.png')}}" class="img-for-teeth teeth_uppper"></image>
                                    </a>

                                    <image id="uppper-7-claps" data-id="teeths1" data-name="7" x="498" y="50" xlink:href="{{asset('admin/assets/images/teeth/updc-7.png')}}" class="clapping clap-up-7 teeth_uppper upper-claps" style="display: none"></image>

                                    <text id="uppper-7-num" data-name="7" class="cls-1 teeth_uppper" transform="translate(563.111 198.624) scale(1.305)">
                                        <tspan x="0">7</tspan>
                                    </text>

                                    <!-- upper-teeth 8 -->
                                    <a href="javascript:;">
                                        <image id="uppper-8-teeth" data-id="teeths8" data-name="8" x="607" y="35" xlink:href="{{asset('admin/assets/images/teeth/up-8.png')}}" class="img-for-teeth teeth_uppper"></image>
                                    </a>

                                    <image id="uppper-8-claps" data-id="teeths8" data-name="8" x="607" y="10" xlink:href="{{asset('admin/assets/images/teeth/updc-8.png')}}" class="clapping clap-up-7 teeth_uppper upper-claps" style="display: none"></image>

                                    <text id="uppper-8-num" data-name="8" class="cls-1 teeth_uppper" transform="translate(697.646 179.813) scale(1.305)">
                                        <tspan x="0">8</tspan>
                                    </text>

                                    <!-- upper-teeth 9 -->
                                    <a href="javascript:;">
                                        <image id="uppper-9-teeth" data-id="teeths16" data-name="9" x="755" y="35" width="150" height="210" xlink:href="{{asset('admin/assets/images/teeth/up-9.png')}}" class="img-for-teeth teeth_uppper"></image>
                                    </a>

                                    <image id="uppper-9-claps" data-id="teeths16" data-name="9" x="765" y="10" xlink:href="{{asset('admin/assets/images/teeth/updc-9.png')}}" class="clapping clap-up-9 teeth_uppper upper-claps" style="display: none"></image>

                                    <text id="uppper-9-num" data-name="9" class="cls-1 teeth_uppper" transform="translate(821.604 176.548) scale(1.305)">
                                        <tspan x="0">9</tspan>
                                    </text>

                                    <!-- upper-teeth 10 -->
                                    <a href="javascript:;">
                                        <image id="uppper-10-teeth" data-id="teeths9" data-name="10" x="889" y="72" width="125" height="189" xlink:href="{{asset('admin/assets/images/teeth/up-10.png')}}" class="img-for-teeth teeth_uppper"></image>
                                    </a>

                                    <image id="uppper-10-claps" data-id="teeths9" data-name="10" x="910" y="50" xlink:href="{{asset('admin/assets/images/teeth/updc-10.png')}}" class="clapping clap-up-10 teeth_uppper upper-claps" style="display: none"></image>

                                    <text id="uppper-10-num" data-name="10" class="cls-1 teeth_uppper" transform="translate(934.785 195.164) scale(1.305)">
                                        <tspan x="0">10</tspan>
                                    </text>

                                    <!-- upper-teeth 11 -->
                                    <a href="javascript:;">
                                        <image id="uppper-11-teeth" data-id="teeths10" data-name="11" x="972" y="123" width="137" height="178" xlink:href="{{asset('admin/assets/images/teeth/up-11.png')}}" class="img-for-teeth teeth_uppper"></image>
                                    </a>

                                    <image id="uppper-11-claps" data-id="teeths10" data-name="11" x="1030" y="100" xlink:href="{{asset('admin/assets/images/teeth/updc-11.png')}}" class="clapping clap-up-11 teeth_uppper upper-claps" style="display: none"></image>

                                    <text id="uppper-11-num" data-name="11" class="cls-1 teeth_uppper" transform="translate(1031.677 240.099) scale(1.305)">
                                        <tspan x="0">11</tspan>
                                    </text>

                                    <!-- upper-teeth 12 -->
                                    <a href="javascript:;">
                                        <image id="uppper-12-teeth" data-id="teeths11" data-name="12" x="1040" y="198" width="149" height="152" xlink:href="{{asset('admin/assets/images/teeth/up-12.png')}}" class="img-for-teeth teeth_uppper"></image>
                                    </a>

                                    <image id="uppper-12-claps" data-id="teeths11" data-name="12" x="1115" y="175" xlink:href="{{asset('admin/assets/images/teeth/updc-12.png')}}" class="clapping clap-up-12 teeth_uppper upper-claps upper-claps" style="display: none"></image>

                                    <text id="uppper-12-num" data-name="12" class="cls-1 teeth_uppper" transform="translate(1103.168 282.619) scale(1.305)">
                                        <tspan x="0">12</tspan>
                                    </text>

                                    <!-- upper-teeth 13 -->
                                    <a href="javascript:;">
                                        <image id="uppper-13-teeth" data-id="teeths12" data-name="13" x="1095" y="284" width="154" height="149" xlink:href="{{asset('admin/assets/images/teeth/up-13.png')}}" class="img-for-teeth teeth_uppper"></image>
                                    </a>

                                    <image id="uppper-13-claps" data-id="teeths12" data-name="13" x="1215" y="270" xlink:href="{{asset('admin/assets/images/teeth/updc-13.png')}}" class="clapping clap-up-13 teeth_uppper upper-claps" style="display: none"></image>

                                    <text id="uppper-13-num" data-name="13" class="cls-1 teeth_uppper" transform="translate(1169.916 345.118) scale(1.305)">
                                        <tspan x="0">13</tspan>
                                    </text>
                                    <!-- upper-teeth 14 -->
                                    <a href="javascript:;">
                                        <image id="uppper-14-teeth" data-id="teeths15" data-name="14" x="1115" y="361" width="221" height="199" xlink:href="{{asset('admin/assets/images/teeth/up-14.png')}}" class="img-for-teeth teeth_uppper"></image>
                                    </a>

                                    <image id="uppper-14-claps" data-id="teeths15" data-name="14" x="1280" y="361" xlink:href="{{asset('admin/assets/images/teeth/updc-14.png')}}" class="clapping clap-up-14 teeth_uppper upper-claps" style="display: none"></image>

                                    <text id="uppper-14-num" data-name="14" class="cls-1 teeth_uppper" transform="translate(1210.87 477.036) scale(1.305)">
                                        <tspan x="0">14</tspan>
                                    </text>
                                    <!-- upper-teeth 15 -->
                                    <a href="javascript:;">
                                        <image id="uppper-15-teeth" data-id="teeths14" data-name="15" x="1194" y="503" width="203" height="188" xlink:href="{{asset('admin/assets/images/teeth/up-15.png')}}" class="img-for-teeth teeth_uppper"></image>
                                    </a>

                                    <image id="uppper-15-claps" data-id="teeths14" data-name="15" x="1350" y="503" xlink:href="{{asset('admin/assets/images/teeth/updc-15.png')}}" class="clapping clap-up-15 teeth_uppper upper-claps" style="display: none"></image>

                                    <text id="uppper-15-nuum" data-name="15" class="cls-1 teeth_uppper" transform="translate(1303.838 603.41) scale(1.305)">
                                        <tspan x="0">15</tspan>
                                    </text>

                                    <!-- upper-teeth 16 -->
                                    <a href="javascript:;">
                                        <image id="uppper-16-teeth" data-id="teeths13" data-name="16" x="1262" y="643" width="192" height="156" xlink:href="{{asset('admin/assets/images/teeth/up-16.png')}}" class="img-for-teeth teeth_uppper"></image>
                                    </a>

                                    <image id="uppper-16-claps" data-id="teeths13" data-name="16" x="1422" y="643" xlink:href="{{asset('admin/assets/images/teeth/updc-16.png')}}" class="clapping clap-up-16 teeth_uppper upper-claps" style="display: none"></image>

                                    <text id="uppper-16-num" data-name="16" class="cls-1 teeth_uppper" transform="translate(1370.808 743.76) scale(1.305)">
                                        <tspan x="0">16</tspan>
                                    </text>

                                    <!--upper teeth numbers  -->
                                </g>
                            </svg>

                            <div class="d-block text-center teeth-text">
                                <a href="javascript:;" class="add-btn p-2 mx-auto uppper_mising_all" id="uppper_mising_all">
                                    Missing all teeth
                                </a>
                            </div>
                            <div class="teeths-wrapper second-row p-0">
                                <div class="row g-5">
                                    <div class="col-md-12 col-xl-12 col-lg-12">
                                        <div class="row g-2">
                                            <a href="javascript:;">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <label class="form-control inner-label te-in-month lbl_btn_all" value="" id="upper_in_mouth"> Teeth in mouth <br> <span id="upper_in_mouth_lbl"></span></label>
                                                        <input type="hidden" id="upper_in_mouth_value" value="">
                                                    </div>
                                                </div>
                                            </a>

                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <label class="form-control inner-label missing-te lbl_btn_all" value="" id="upper_missing_teeth">missing teeth <br> <span id="upper_missing_teeth_lbl"></span></label>
                                                    <input type="hidden" id="upper_missing_teeth_value" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <label class="form-control inner-label ex-dekivery lbl_btn_all" value="" id="upper_ectract_delivery">will extract on delivery <br><span id="upper_ectract_delivery_lbl"></span></label>
                                                    <input type="hidden" id="upper_ectract_delivery_value" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <label class="form-control inner-label ben-extracted lbl_btn_all" value="" id="upper_been_extracted"> has been extracted <br><span id="upper_been_extracted_lbl"></span></label>
                                                    <input type="hidden" id="upper_been_extracted_value" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <label class="form-control inner-label fix-add lbl_btn_all" value="" id="upper_fix"> fix or add <br> <span id="upper_fix_lbl"></span></label>
                                                    <input type="hidden" id="upper_fix_value" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <label class="form-control inner-label clasps lbl_btn_all" value="" id="upper_clasps">clasps<br> <span id="upper_clasps_lbl"></span></label>
                                                    <input type="hidden" id="upper_clasps_value" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="offset-xl-4 col-lg-6 col-xl-4 teeth-wrapper lower">
                            <h3 class="card-title mb-0" style="margin-top: 20px;">
                                Lower
                            </h3>
                            <svg width="100%" height="100%" viewBox="0 0 1550 700" class="teeth-svg">

                                <g>
<g></g>
                                    <!-- lower-teeth 1 -->
                                    <a href="javascript:;">
                                        <image id="lower-17-teeth" data-id="teeth15" data-name="17" x="30" y="555" width="252" height="200" xlink:href="{{asset('admin/assets/images/teeth/d-17.png')}}" class="img-for-teeth  teeth_lower"></image>
                                    </a>

                                    <!--  image with cliap-1 -->
                                    <image id="lower-17-claps" data-name="17" x="15" y="540" width="80" height="190" xlink:href="{{asset('admin/assets/images/teeth/dc-17.png')}}" class="clapping clap-17 teeth_lower lower-claps" style="display: none"></image>

                                    <text id="lower-17-num" data-name="17" class="cls-1 teeth_lower" transform="translate(196.632 625.98) scale(1.199)">
                                        <tspan x="-30">17</tspan>
                                    </text>


                                    <!-- lower-teeth 1 end-->

                                    <g></g>
                                    <!-- lower-teeth 2 -->
                                    <a href="javascript:;">
                                        <image id="lower-18-teeth" data-id="teeth7" data-name="18" x="95" y="400" width="232" height="194" xlink:href="{{asset('admin/assets/images/teeth/d-18.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>
                                    <!--  image with cliap-2 -->
                                    <image id="lower-18-claps" data-name="18" x="80" y="382" width="85" height="194" xlink:href="{{asset('admin/assets/images/teeth/dc-18.png')}}" class="clapping clap-18 teeth_lower lower-claps" style="display: none"></image>

                                    <text id="lower-18-num" data-name="18" class="cls-1 teeth_lower" transform="translate(263.156 536.61) scale(1.199)">
                                        <tspan x="-35" y="-30">18</tspan>
                                    </text>

                                    <!-- lower-teeth 2 end-->

                                    <g></g>
                                    <!-- lower-teeth 3 -->
                                    <a href="javascript:;">
                                        <image id="lower-19-teeth" data-id="teeth6" data-name="19" x="165" y="290" width="235" height="191" xlink:href="{{asset('admin/assets/images/teeth/d-19.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>

                                    <text id="lower-19-num" data-name="19" class="cls-1 teeth_lower" transform="translate(329.927 426.297) scale(1.199)">
                                        <tspan x="-35" y="-35">19</tspan>
                                    </text>

                                    <!--  image with cliap-3 -->
                                    <image id="lower-19-claps" data-name="19" x="155" y="255" width="100" height="191" xlink:href="{{asset('admin/assets/images/teeth/dc-19.png')}}" class="clapping clap-19 teeth_lower lower-claps" style="display: none"></image>

                                    <!-- lower-teeth 3 end-->


<g></g>
                                    <!-- lower-teeth 4 start-->
                                    <a href="javascript:;">
                                        <image id="lower-20-teeth" data-id="teeth5" data-name="20" x="265" y="225" width="167" height="159" xlink:href="{{asset('admin/assets/images/teeth/d-20.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>

                                    <text id="lower-20-num" data-name="20" class="cls-1 teeth_lower" transform="translate(401.061 332.076) scale(1.199)">
                                        <tspan x="-30" y="-35">20</tspan>
                                    </text>

                                    <!--  image with cliap-4 -->
                                    <image id="lower-20-claps" data-name="20" x="240" y="175" width="100" height="159" xlink:href="{{asset('admin/assets/images/teeth/dc-20.png')}}" class="clapping clap-20 teeth_lower lower-claps" style="display: none"></image>

                                    <!-- lower-teeth 4 end-->

                                    <g></g>
                                    <!-- lower-teeth 5 -->
                                    <a href="javascript:;">
                                        <image id="lower-21-teeth" data-id="teeth3" data-name="21" x="342" y="153" width="150" height="177" xlink:href="{{asset('admin/assets/images/teeth/d-21.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>
                                    <!--  image with cliap-5 -->
                                    <image id="lower-21-claps" data-name="21" x="300" y="135" width="140" height="90" xlink:href="{{asset('admin/assets/images/teeth/dc-21.png')}}" class="clapping clap-21 teeth_lower lower-claps" style="display: none"></image>
                                    <text id="lower-21-num" data-name="21" class="cls-1 teeth_lower" transform="translate(455.319 283.973) scale(1.199)">
                                        <tspan x="-30" y="-50">21</tspan>
                                    </text>

                                    <g></g>
                                    <!-- lower-teeth 6 -->
                                    <a href="javascript:;">
                                        <image id="lower-22-teeth" data-id="teeth4" data-name="22" x="435" y="93" width="135" height="200" xlink:href="{{asset('admin/assets/images/teeth/d-22.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>

                                    <!--  image with cliap-22 -->
                                    <image id="lower-22-claps" data-name="22" x="430" y="15" width="100" height="200" xlink:href="{{asset('admin/assets/images/teeth/dc-22.png')}}" class="clapping clap-22 teeth_lower lower-claps" style="display: none"></image>
                                    <text id="lower-22-num" data-name="22" class="cls-1 teeth_lower" transform="translate(517.12 257.438) scale(1.199)">
                                        <tspan x="0" y="-50">22</tspan>
                                    </text>

                                    <g></g>
                                    <!-- lower-teeth 7 -->
                                    <a href="javascript:;">
                                        <image id="lower-23-teeth" data-id="teeth2" data-name="23" x="540" y="60" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/d-23.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>
                                    <!-- image with cliap-23 -->
                                    <image id="lower-23-claps" data-name="23" x="525" y="-33" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/dc-23.png')}}" class="clapping clap-23 teeth_lower lower-claps" style="display: none"></image>
                                    <text id="lower-23-num" data-name="23" class="cls-1 teeth_lower" transform="translate(602.467 207.332) scale(1.199)">
                                        <tspan x="0" y="-50">23</tspan>
                                    </text>


                                    <g></g>

                                    <!-- lower-teeth 8 -->
                                    <a href="javascript:;">
                                        <image id="lower-24-teeth" data-id="teeth1" data-name="24" x="647" y="40" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/d-24.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>
                                    <!-- image with cliap-8 -->
                                    <image id="lower-24-claps" data-name="24" x="647" y="-60" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/dc-24.png')}}" class="clapping clap-24 teeth_lower lower-claps" style="display: none"></image>
                                    <text id="lower-24-num" data-name="24" class="cls-1 teeth_lower" transform="translate(703.596 190.046) scale(1.199)">
                                        <tspan x="0" y="-40">24</tspan>
                                    </text>

                                    <g></g>
                                    <!-- lower-teeth 9 -->
                                    <a href="javascript:;">
                                        <image id="lower-25-teeth" data-id="teeth8" data-name="25" x="760" y="42" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/d-25.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>
                                    <!--  image with cliap-9 -->
                                    <image id="lower-25-claps" data-name="25" x="762" y="-58" width="111" height="182" xlink:href="{{asset('admin/assets/images/teeth/dc-25.png')}}" class="clapping clap-25 teeth_lower lower-claps" style="display: none"></image>
                                    <text id="lower-25-num" data-name="25" class="cls-1 teeth_lower" transform="translate(789.005 190.046) scale(1.199)">
                                        <tspan x="10" y="-50">25</tspan>
                                    </text>
                                    <g></g>
                                    <!-- lower-teeth 10 -->
                                    <a href="javascript:;">
                                        <image id="lower-26-teeth" data-id="teeth9" data-name="26" x="865" y="54" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/d-26.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>
                                    <!--  image with cliap-11 -->
                                    <image id="lower-26-claps" data-name="26" x="880" y="-35" width="115" height="186" xlink:href="{{asset('admin/assets/images/teeth/dc-26.png')}}" class="clapping clap-26 teeth_lower lower-claps" style="display: none"></image>
                                    <text id="lower-26-num" data-name="26" class="cls-1 teeth_lower" transform="translate(881.008 207.152) scale(1.199)">
                                        <tspan x="25" y="-50">26</tspan>
                                    </text>
                                    <!-- lower-teeth 11-->
                                    <a href="javascript:;">
                                        <image id="lower-27-teeth" data-id="teeth11" data-name="27" x="955" y="80" width="135" height="200" xlink:href="{{asset('admin/assets/images/teeth/d-27.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>
                                    <!--  image with cliap-11 -->
                                    <image id="lower-27-claps" data-name="27" x="990" y="-5" width="110" height="200" xlink:href="{{asset('admin/assets/images/teeth/dc-27.png')}}" class="clapping clap-27 teeth_lower lower-claps" style="display: none"></image>
                                    <text id="lower-27-num" data-name="27" class="cls-1 teeth_lower" transform="translate(982.792 240.944) scale(1.199)">
                                        <tspan x="25" y="-50">27</tspan>
                                    </text>
                                    <!-- lower-teeth 12 -->
                                    <a href="javascript:;">
                                        <image id="lower-28-teeth" data-id="teeth10" data-name="28" x="1035" y="134" width="150" height="177" xlink:href="{{asset('admin/assets/images/teeth/d-28.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>
                                    <!--  image with cliap-12 -->
                                    <image id="lower-28-claps" data-name="28" x="1100" y="60" width="100" height="177" xlink:href="{{asset('admin/assets/images/teeth/dc-28.png')}}" class="clapping clap-28 teeth_lower lower-claps" style="display: none"></image>
                                    <text id="lower-28-num" data-name="28" class="cls-1 teeth_lower" transform="translate(1037.236 283.518) scale(1.199)">
                                        <tspan x="40" y="-50">28</tspan>
                                    </text>
                                    <!-- lower-teeth 13 -->
                                    <a href="javascript:;">
                                        <image id="lower-29-teeth" data-id="teeth12" data-name="29" x="1098" y="205" width="167" height="159" xlink:href="{{asset('admin/assets/images/teeth/d-29.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>
                                    <!--  image with cliap-13 -->
                                    <image id="lower-29-claps" data-name="29" x="1190" y="155" width="100" height="159" xlink:href="{{asset('admin/assets/images/teeth/dc-29.png')}}" class="clapping clap-29 teeth_lower lower-claps" style="display: none"></image>
                                    <text id="lower-29-num" data-name="29" class="cls-1 teeth_lower" transform="translate(1097.072 329.95) scale(1.199)">
                                        <tspan x="45" y="-50">29</tspan>
                                    </text>
                                    <!-- lower-teeth 14 -->
                                    <a href="javascript:;">
                                        <image id="lower-30-teeth" data-id="teeth13" data-name="30" x="1130" y="278" width="234" height="191" xlink:href="{{asset('admin/assets/images/teeth/d-30.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>
                                    <!--  image with cliap-14 -->
                                    <image id="lower-30-claps" data-name="30" x="1280" y="258" width="100" height="191" xlink:href="{{asset('admin/assets/images/teeth/dc-30.png')}}" class="clapping clap-30 teeth_lower lower-claps" style="display: none"></image>
                                    <text id="lower-30-num" data-name="30" class="cls-1 teeth_lower" transform="translate(1164.707 409.172) scale(1.199)">
                                        <tspan x="45" y="-45">30</tspan>
                                    </text>
                                    <!-- lower-teeth 15 -->

                                    <a href="javascript:;">
                                        <image id="lower-31-teeth" data-id="teeth14" data-name="31" x="1210" y="395" width="232" height="194" xlink:href="{{asset('admin/assets/images/teeth/d-31.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>
                                    <!--  image with cliap-15 -->
                                    <image id="lower-31-claps" data-name="31" x="1390" y="420" width="80" height="130" xlink:href="{{asset('admin/assets/images/teeth/dc-30.png')}}" class="clapping clap-31 teeth_lower lower-claps" style="display: none"></image>
                                    <text id="lower-31-num" data-name="31" class="cls-1 teeth_lower" transform="translate(1238.134 502.8) scale(1.199)">
                                        <tspan x="50">31</tspan>
                                    </text>
                                    <!-- lower-teeth 16 -->

                                    <a href="javascript:;">
                                        <image id="lower-32-teeth" data-id="teeth16" data-name="32" x="1273" y="560" width="232" height="193" xlink:href="{{asset('admin/assets/images/teeth/d-32.png')}}" class="img-for-teeth teeth_lower"></image>
                                    </a>
                                    <!--  image with cliap-32 -->
                                    <image id="lower-32-claps" data-name="32" x="1440" y="580" width="114" height="92" xlink:href="{{asset('admin/assets/images/teeth/dc-32.png')}}" class="clapping clap-32 teeth_lower lower-claps" style="display: none"></image>
                                    <text id="lower-32-num" data-name="32" class="cls-1 teeth_lower" transform="translate(1268.177 618.27) scale(1.199)">
                                        <tspan x="80" y="20">32</tspan>
                                    </text>

                                    <!--lower teeth numbers ------------------------------ -->

                                </g>

                            </svg>

                            <div class="d-block text-center teeth-text">
                                <a href="javascript:;" class="add-btn p-2 mx-auto lower_mising_all">
                                    Missing all teeth
                                </a>
                            </div>
                            <div class="teeths-wrapper second-row p-0">
                                <div class="row g-5">
                                    <div class="col-md-12 col-xl-12 col-lg-12">
                                        <div class="row g-2">
                                            <a href="javascript:;">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <label class="form-control inner-label te-in-month lower_lbl_btn_all" value="" id="lower_in_mouth"> Teeth in mouth <br> <span id="lower_in_mouth_lbl"></span></label>
                                                        <input type="hidden" id="lower_in_mouth_value" value="">
                                                    </div>
                                                </div>
                                            </a>

                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <label class="form-control inner-label missing-te lower_lbl_btn_all" value="" id="lower_missing_teeth">missing teeth <br> <span id="lower_missing_teeth_lbl"></span></label>
                                                    <input type="hidden" id="lower_missing_teeth_value" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <label class="form-control inner-label ex-dekivery lower_lbl_btn_all" value="" id="lower_ectract_delivery">will extract on delivery <br><span id="lower_ectract_delivery_lbl"></span></label>
                                                    <input type="hidden" id="lower_ectract_delivery_value" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <label class="form-control inner-label ben-extracted lower_lbl_btn_all" value="" id="lower_been_extracted"> has been extracted <br><span id="lower_been_extracted_lbl"></span></label>
                                                    <input type="hidden" id="lower_been_extracted_value" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <label class="form-control inner-label fix-add lower_lbl_btn_all" value="" id="lower_fix"> fix or add <br> <span id="lower_fix_lbl"></span></label>
                                                    <input type="hidden" id="lower_fix_value" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <label class="form-control inner-label clasps lower_lbl_btn_all" value="" id="lower_clasps">clasps<br> <span id="lower_clasps_lbl"></span></label>
                                                    <input type="hidden" id="lower_clasps_value" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- DOM - jQuery events table -->
<!-- END: Content-->
@endsection

@section('custom-js')
<script>
     $(document).ready(function() {
        upper_in_mouth_array = [];
        $('.teeth_uppper').addClass("img-for-teeth");
        $(".upper_in_mouth_lbl").addClass("active");

        for (i = 1; i <= 16; i++) {
            upper_in_mouth_array.push(parseInt(i));
        }
        upper_in_mouth_array = $.unique(upper_in_mouth_array.sort());
        $("#upper_in_mouth_lbl").text(upper_in_mouth_array.join(","));

        lower_in_mouth_array = [];
        $('.teeth_lower').addClass("img-for-teeth");
        $(".lower_in_mouth_lbl").addClass("active");

        for (i = 17; i <= 32; i++) {
            lower_in_mouth_array.push(parseInt(i));
        }
        lower_in_mouth_array = $.unique(lower_in_mouth_array.sort());
        $("#lower_in_mouth_lbl").text(lower_in_mouth_array.join(","));
    });
    // Upper Teeth
    upper_in_mouth_array = [];
    upper_missing_teeth_array = [];
    upper_ectract_delivery_array = [];
    upper_been_extracted_array = [];
    upper_fix_array = [];
    upper_clasps_array = [];

    $(document).on('click', '.lbl_btn_all', function() {
        $(".lbl_btn_all").removeClass("selected-button-upper");
        $(this).addClass("selected-button-upper");
        $(".lbl_btn_all").removeClass("active");
        $(this).addClass('active');
    });
    $(document).on('click', '.uppper_mising_all', function() {


        upper_in_mouth_array = [];
        upper_missing_teeth_array = [];
        upper_ectract_delivery_array = [];
        upper_been_extracted_array = [];
        upper_fix_array = [];
        upper_clasps_array = [];

        $('.teeth_uppper').addClass("missing");
        $(".upper-claps").hide();
        $(".lbl_btn_all").removeClass("active");

        for (i = 1; i <= 16; i++) {
            upper_missing_teeth_array.push(parseInt(i));
        }

        $('.teeth_uppper').removeClass("in-mouth");
        $('.teeth_uppper').removeClass("will-extract-on-deliver");
        $('.teeth_uppper').removeClass("ben-extracted");
        $('.teeth_uppper').removeClass("add-or-fix");
        $('.teeth_uppper').removeClass("calps");

        $("#upper_in_mouth_lbl").text('');
        $("#upper_ectract_delivery_lbl").text('');
        $("#upper_been_extracted_lbl").text('');
        $("#upper_fix_lbl").text('');
        $("#upper_clasps_lbl").text('');

        upper_missing_teeth_array = $.unique(upper_missing_teeth_array.sort());
        $("#upper_missing_teeth_lbl").text(upper_missing_teeth_array.join(","));

    });

    $(document).on('click', '.teeth_uppper', function() {
        selected_tab = $(".selected-button-upper").attr("id");
        teeth_num = $(this).data("name");
        teeth_image = $(this).attr('id');
        if (selected_tab == "upper_in_mouth") {
            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-teeth").removeClass("in-mouth");
                $("#uppper-" + teeth_num + "-claps").hide();

                if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                    var index = upper_clasps_array.indexOf(teeth_num);
                    if (index !== -1) {
                        upper_clasps_array.splice(index, 1);
                    }
                }

            } else {
                upper_in_mouth_array.push(teeth_num);
                $("#uppper-" + teeth_num + "-teeth").addClass("in-mouth");
            }

            // $(this).addClass("in-mouth");


            $("#uppper-" + teeth_num + "-teeth").removeClass("missing");
            $("#uppper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#uppper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            $("#uppper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            $("#uppper-" + teeth_num + "-teeth").removeClass("calps");
        } else if (selected_tab == "upper_missing_teeth") {

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-claps").hide();
            }


            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-teeth").removeClass("missing");
            } else {
                upper_missing_teeth_array.push(teeth_num);
                $("#uppper-" + teeth_num + "-teeth").addClass("missing");
            }



            // $("#uppper-"+teeth_num+"-teeth").addClass("active");

            $("#uppper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#uppper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            $("#uppper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            $("#uppper-" + teeth_num + "-teeth").removeClass("calps");
            $("#uppper-" + teeth_num + "-teeth").removeClass("in-mouth");
        } else if (selected_tab == "upper_ectract_delivery") {

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-claps").hide();
            }

            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            } else {
                upper_ectract_delivery_array.push(teeth_num);
                $("#uppper-" + teeth_num + "-teeth").addClass("will-extract-on-deliver");
            }

            // $("#uppper-"+teeth_num+"-teeth").addClass("active");

            $("#uppper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            $("#uppper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            $("#uppper-" + teeth_num + "-teeth").removeClass("calps");
            $("#uppper-" + teeth_num + "-teeth").removeClass("missing");
            $("#uppper-" + teeth_num + "-teeth").removeClass("in-mouth");
        } else if (selected_tab == "upper_been_extracted") {

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-claps").hide();
            }


            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            } else {
                upper_been_extracted_array.push(teeth_num)
                $("#uppper-" + teeth_num + "-teeth").addClass("ben-extracted");
            }


            $("#uppper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            $("#uppper-" + teeth_num + "-teeth").removeClass("calps");
            $("#uppper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#uppper-" + teeth_num + "-teeth").removeClass("missing");
            $("#uppper-" + teeth_num + "-teeth").removeClass("in-mouth");

        } else if (selected_tab == "upper_fix") {

            if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                var index = upper_in_mouth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_missing_teeth_array) != -1) {
                var index = upper_missing_teeth_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_ectract_delivery_array) != -1) {
                var index = upper_ectract_delivery_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(teeth_num, upper_been_extracted_array) != -1) {
                var index = upper_been_extracted_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_been_extracted_array.splice(index, 1);
                }
            }

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-claps").hide();
            }


            if ($.inArray(teeth_num, upper_fix_array) != -1) {
                var index = upper_fix_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_fix_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-teeth").removeClass("add-or-fix");
            } else {
                upper_fix_array.push(teeth_num)
                $("#uppper-" + teeth_num + "-teeth").addClass("add-or-fix");
            }


            $("#uppper-" + teeth_num + "-teeth").removeClass("calps");
            $("#uppper-" + teeth_num + "-teeth").removeClass("ben-extracted");
            $("#uppper-" + teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#uppper-" + teeth_num + "-teeth").removeClass("missing");
            $("#uppper-" + teeth_num + "-teeth").removeClass("in-mouth");
        } else if (selected_tab == "upper_clasps") {

            if ($.inArray(teeth_num, upper_clasps_array) != -1) {
                var index = upper_clasps_array.indexOf(teeth_num);
                if (index !== -1) {
                    upper_clasps_array.splice(index, 1);
                }
                $("#uppper-" + teeth_num + "-claps").hide();
            } else if ($.inArray(teeth_num, upper_in_mouth_array) != -1) {
                upper_clasps_array.push(teeth_num);
                $("#uppper-" + teeth_num + "-claps").show();
            }
        }


        upper_in_mouth_array = $.unique(upper_in_mouth_array.sort());
        upper_missing_teeth_array = $.unique(upper_missing_teeth_array.sort());
        upper_ectract_delivery_array = $.unique(upper_ectract_delivery_array.sort());
        upper_been_extracted_array = $.unique(upper_been_extracted_array.sort());
        upper_fix_array = $.unique(upper_fix_array.sort());
        upper_clasps_array = $.unique(upper_clasps_array.sort());


        $("#upper_in_mouth_lbl").text(upper_in_mouth_array.join(","));
        $("#upper_missing_teeth_lbl").text(upper_missing_teeth_array.join(","));
        $("#upper_ectract_delivery_lbl").text(upper_ectract_delivery_array.join(","));
        $("#upper_been_extracted_lbl").text(upper_been_extracted_array.join(","));
        $("#upper_fix_lbl").text(upper_fix_array.join(","));
        $("#upper_clasps_lbl").text(upper_clasps_array.join(","));

    });

    // Lower Teeth ========================================================================================

    lower_in_mouth_array = [];
    lower_missing_teeth_array = [];
    lower_ectract_delivery_array = [];
    lower_been_extracted_array = [];
    lower_fix_array = [];
    lower_clasps_array = [];

    $(document).on('click', '.lower_lbl_btn_all', function() {
        $(".lower_lbl_btn_all").removeClass("selected-button-lower");
        $(this).addClass("selected-button-lower");
        $(".lower_lbl_btn_all").removeClass("active");
        $(this).addClass('active');
    });
    $(document).on('click', '.lower_mising_all', function() {

        lower_in_mouth_array = [];
        lower_missing_teeth_array = [];
        lower_ectract_delivery_array = [];
        lower_been_extracted_array = [];
        lower_fix_array = [];
        lower_clasps_array = [];

        $('.teeth_lower').addClass("missing");
        $(".lower-claps").hide();
        $(".lower_lbl_btn_all").removeClass("active");

        for (i = 17; i <= 32; i++) {
            lower_missing_teeth_array.push(parseInt(i));
        }

        $("#lower_missing_teeth_lbl").text('17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32');

        $('.teeth_lower').removeClass("in-mouth");
        $('.teeth_lower').removeClass("will-extract-on-deliver");
        $('.teeth_lower').removeClass("ben-extracted");
        $('.teeth_lower').removeClass("add-or-fix");
        $('.teeth_lower').removeClass("calps");

        $("#lower_in_mouth_lbl").text('');
        $("#lower_ectract_delivery_lbl").text('');
        $("#lower_been_extracted_lbl").text('');
        $("#lower_fix_lbl").text('');
        $("#lower_clasps_lbl").text('');

        lower_missing_teeth_array = $.unique(lower_missing_teeth_array.sort());
        $("#lower_missing_teeth_lbl").text(lower_missing_teeth_array.join(","));

    });

    $(document).on('click', '.teeth_lower', function() {
        lower_selected_tab = $(".selected-button-lower").attr("id");
        lower_teeth_num = $(this).data("name");
        teeth_image = $(this).attr('id')

        if (lower_selected_tab == "lower_in_mouth") {


            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
            }


            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {

                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }

                if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                    var index = lower_clasps_array.indexOf(lower_teeth_num);
                    if (index !== -1) {
                        lower_clasps_array.splice(index, 1);
                    }
                }

                $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
                $("#lower-" + lower_teeth_num + "-claps").hide();

            } else {
                lower_in_mouth_array.push(lower_teeth_num);
                $("#lower-" + lower_teeth_num + "-teeth").addClass("in-mouth");
            }

            $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");

        } else if (lower_selected_tab == "lower_missing_teeth") {

            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            }


            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            } else {
                lower_missing_teeth_array.push(lower_teeth_num)
                $("#lower-" + lower_teeth_num + "-teeth").addClass("missing");
            }


            $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");
        } else if (lower_selected_tab == "lower_ectract_delivery") {

            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            }


            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            } else {
                lower_ectract_delivery_array.push(lower_teeth_num);
                $("#lower-" + lower_teeth_num + "-teeth").addClass("will-extract-on-deliver");
            }


            $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");
        } else if (lower_selected_tab == "lower_been_extracted") {


            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            }


            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            } else {
                lower_been_extracted_array.push(lower_teeth_num)
                $("#lower-" + lower_teeth_num + "-teeth").addClass("ben-extracted");
            }


            $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");
        } else if (lower_selected_tab == "lower_fix") {


            if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                var index = lower_in_mouth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_in_mouth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_missing_teeth_array) != -1) {
                var index = lower_missing_teeth_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_missing_teeth_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_ectract_delivery_array) != -1) {
                var index = lower_ectract_delivery_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_ectract_delivery_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_been_extracted_array) != -1) {
                var index = lower_been_extracted_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_been_extracted_array.splice(index, 1);
                }
            }
            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            }


            if ($.inArray(lower_teeth_num, lower_fix_array) != -1) {
                var index = lower_fix_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_fix_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-teeth").removeClass("add-or-fix");
            } else {
                lower_fix_array.push(lower_teeth_num)
                $("#lower-" + lower_teeth_num + "-teeth").addClass("add-or-fix");
            }


            $("#lower-" + lower_teeth_num + "-teeth").removeClass("ben-extracted");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("will-extract-on-deliver");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("missing");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("in-mouth");
            $("#lower-" + lower_teeth_num + "-teeth").removeClass("calps");
        } else if (lower_selected_tab == "lower_clasps") {

            if ($.inArray(lower_teeth_num, lower_clasps_array) != -1) {
                var index = lower_clasps_array.indexOf(lower_teeth_num);
                if (index !== -1) {
                    lower_clasps_array.splice(index, 1);
                }
                $("#lower-" + lower_teeth_num + "-claps").hide();
            } else if ($.inArray(lower_teeth_num, lower_in_mouth_array) != -1) {
                lower_clasps_array.push(lower_teeth_num);
                $("#lower-" + lower_teeth_num + "-claps").show();
            }
        }


        lower_in_mouth_array = $.unique(lower_in_mouth_array.sort());
        lower_missing_teeth_array = $.unique(lower_missing_teeth_array.sort());
        lower_ectract_delivery_array = $.unique(lower_ectract_delivery_array.sort());
        lower_been_extracted_array = $.unique(lower_been_extracted_array.sort());
        lower_fix_array = $.unique(lower_fix_array.sort());
        lower_clasps_array = $.unique(lower_clasps_array.sort());


        $("#lower_in_mouth_lbl").text(lower_in_mouth_array.join(","));
        $("#lower_missing_teeth_lbl").text(lower_missing_teeth_array.join(","));
        $("#lower_ectract_delivery_lbl").text(lower_ectract_delivery_array.join(","));
        $("#lower_been_extracted_lbl").text(lower_been_extracted_array.join(","));
        $("#lower_fix_lbl").text(lower_fix_array.join(","));
        $("#lower_clasps_lbl").text(lower_clasps_array.join(","));

    });
    // 
</script>
@endsection