@php
  $setting_info = \App\Libraries\General::setting_info('Company');
@endphp
<div class="footer py-1" id="kt_footer">
	<!--begin::Container-->
	<div class="container-fluid">
		<!--begin::Copyright-->
		<div class="text-dark order-2 order-md-1">
			<span class="text-muted fw-bold me-1">{{$setting_info['COPYRIGHTED_TEXT']['vValue']}}</span>
			
			
		</div>
	</div>

</div>	