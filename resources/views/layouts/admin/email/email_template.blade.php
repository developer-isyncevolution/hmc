@php   
$setting_info = \App\Libraries\General::setting_info('Company');
// dd($general_logo);
$logo_main         = $setting_info['COMPANY_LOGO']['vValue'];
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$setting_info['COMPANY_NAME']['vValue']}}</title>
</head>

<body style="margin: 0;">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td style="background-color:#F5F5F5">
                <table style="width:620px; margin: 35px auto; height: 100vc;" cellspacing="0" cellpadding="0"
                    align="center">
                    <!-- <tr>
                        <td height="50"></td>
                    </tr> -->
                    <tr>
                        <td>
                            <table width="100% " cellspacing="2 " cellpadding="5 ">
                                <tr>
                                    <td style="background-color:#fff; padding: 50px 70px 30px;
                                    border-radius: 5px;
                                    text-align: center; box-shadow: 1px 0px 1px 0px rgb(0 0 0 / 2%);"
                                        style="padding:25px 25px 0; border: 0px solid #dedede; border-bottom:none; ">

                                        <img src="{{asset('uploads/logo/'.$logo_main)}}"
                                            style="text-align: center; display: block;margin: 0px auto 30px;"
                                            height="52" width="212" alt="">

                                        {!! $msg !!}
                                        
                                        <font face="Verdana, Geneva, sans-serif " color="#222222 "
                                            style="font-size: 12px;display: block; margin: 20px 0px;text-align: center;">
                                            <strong> if you have any quary please email us at</strong> <a href="mailto:admin@mailinator.com"
                                                style="text-decoration: underline; color: #3b6eb6; "
                                                target="_blank ">admin@mailinator.com</a>
                                        </font>
                                    </td>
                                </tr>
                                <tr>
                                    <td
                                        style="background-color:#fff; padding:10px 25px 25px; border: 0px solid #dedede; border-bottom: 2px solid #3b6eb6; border-top:none; ">
                                        <font face="Verdana, Geneva, sans-serif " color="#222222 "
                                            style="font-size: 13px; display: block; text-align: center;"> you are
                                            receiving this email because you made a registration on
                                            <a href="# " style="text-decoration: none; color: #3b6eb6;"
                                                target="_blank ">https://www.labslips.online/</a>.if this was sent to you by
                                            mistake please email us at <a href="mailto:admin@mailinator.com"
                                                style="text-decoration: underline; color: #3b6eb6; "
                                                target="_blank ">admin@mailinator.com</a>
                                        </font>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:#fff;" height="50px " align="center ">
                                        <font face="Verdana, Geneva, sans-serif " color="#959595 "
                                            style="font-size: 11px; "> Copyright 2022 &copy;{{$setting_info['COMPANY_NAME']['vValue']}} All Rights
                                            Reserved.
                                        </font>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>