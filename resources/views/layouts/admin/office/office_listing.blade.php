@extends('layouts.admin.index')
@section('content')
    <!-- BEGIN: Content-->
    <!-- DOM - jQuery events table -->
    <div class="main-panel">
        <div class="col-lg-12 mx-auto">
            <div class="listing-page">
                <div class="card mb-5 mb-xl-4">
                    <div class="card-header">
                        @if (isset($iCustomerId))
                            <ul class="c-nav-tab">
                                <li class="">
                                    <a href="{{ route('admin.customer.edit', $iCustomerId) }}" class=" add-btn btn mx-2">
                                        Customer
                                    </a>
                                </li>
                                <li class="">
                                    <a href="" class="active add-btn btn mx-2">
                                        Office
                                    </a>
                                </li>

                            </ul>


                            <ul class="d-flex justify-content-center my-2 list-unstyled flex-wrap w-100">
                                @if(\App\Libraries\General::check_permission('','eCreate') == true)
                                <li class="d-flex">
                                    <a href="{{ route('admin.office.office-create', $iCustomerId) }}"
                                        class="btn add-btn create_permission mx-2">
                                        <i class="fa fa-plus"></i> Add
                                    </a>
                                </li>
                                @endif
                            </ul>

                        @endif
                    </div>
                </div>
                @if (isset($iCustomerId))
                    <input type="hidden" name="customer" id="customer" value="{{ $iCustomerId }}">
                @endisset
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr class="fw-bolder text-muted">
                                <th class="w-25px text-center" data-orderable="false">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input class="form-check-input" type="checkbox" value="1" data-kt-check="true"
                                            data-kt-check-target=".widget-13-check" id="selectall" type="checkbox"
                                            name="selectall">
                                        <label for="selectall">&nbsp;</label>
                                    </div>
                                </th>

                                <th class="min-w-100px">
                                    <a id="vName" class="sort" data-column="vName" data-order="ASC"
                                        href="javascript:;">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Customer Name</span>
                                    </a>
                                </th>
                                <th class="min-w-100px">
                                    <a id="vOfficeName" class="sort" data-column="vOfficeName"
                                        data-order="ASC" href="javascript:;">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Office Name</span>
                                    </a>
                                </th>
                                <th class="min-w-100px">
                                    <a id="vName" class="sort" data-column="vName" data-order="ASC"
                                        href="javascript:;">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Contact Name</span>
                                    </a>
                                </th>
                                <th class="min-w-100px"><a id="vTitle" class="sort" data-column="vTitle"
                                        data-order="ASC" href="javascript:;">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Title</span>
                                    </a>
                                </th>
                                <th class="min-w-100px">
                                    <a id="eStatus" class="sort" data-column="eStatus" data-order="ASC"
                                        href="javascript:;">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Status</span>
                                    </a>
                                </th>
                                <th class="max-w-100px">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader" width="250px"
                                height="auto" />
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<!-- DOM - jQuery events table -->
<!-- END: Content-->
@endsection

@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
    $('#iCustomerId').selectize();
    $(document).ready(function() {
        var iCustomerId = $("#customer").val();
        $.ajax({
            url: "{{ route('admin.office.OfficeajaxListing') }}",
            type: "get",
            data: {
                iCustomerId: iCustomerId,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $("#selectall").click(function() {
        if (this.checked) {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', true);
            });
        } else {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', false);
            });
        }
    });


    $("#keyword").keyup(function() {
        var keyword = $("#keyword").val();
        var iCustomerId = $("#customer").val();

        $("#ajax-loader").show();

        $.ajax({
            url: "{{ route('admin.office.OfficeajaxListing') }}",
            type: "get",
            data: {
                keyword: keyword,
                iCustomerId: iCustomerId,
                action: 'search'
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '#delete_btn', function() {
        var id = [];

        $("input[name='Office_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");
        var iCustomerId = $("#customer").val();

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                    title: "Are you sure delete all office.?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{ route('admin.office.OfficeajaxListing') }}",
                            type: "get",
                            data: {
                                id: id,
                                iCustomerId: iCustomerId,
                                action: 'multiple_delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("Office Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                            }
                        });
                    }
                });
        }
    });
    $(document).on('click', '#delete', function() {
        swal({
                title: "Are you sure delete this office.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");
                    var iCustomerId = $("#customer").val();
                    $("#ajax-loader").show();

                    $.ajax({
                        url: "{{ route('admin.office.OfficeajaxListing') }}",
                        type: "get",
                        data: {
                            id: id,
                            iCustomerId: iCustomerId,
                            action: 'delete'
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                            notification_error("Office Deleted Successfully");
                            setTimeout(function() {
                                location.reload();
                            }, 1000);
                        }
                    });
                }
            })
    });
    $(document).on('click', '.sort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');
        var iCustomerId = $("#customer").val();

        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#ajax-loader").show();

        $.ajax({
            url: "{{ route('admin.office.OfficeajaxListing') }}",
            type: "get",
            data: {
                column: column,
                order,
                order,
                iCustomerId: iCustomerId,
                action: 'sort'
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '.ajax_page', function() {
        pages = $(this).data("pages");
        var iCustomerId = $("#customer").val();
        $("#table_record").html('');
        $("#ajax-loader").show();

        $.ajax({
            url: "{{ route('admin.office.OfficeajaxListing') }}",
            type: "get",
            data: {
                pages: pages,
                iCustomerId: iCustomerId,
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#iCustomerId', function() {
        var iCustomerId = $("#iCustomerId").val();

        $("#table_record").html('');
        $("#ajax-loader").show();

        $.ajax({
            url: "{{ route('admin.office.OfficeajaxListing') }}",
            type: "get",
            data: {
                iCustomerId: iCustomerId
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#page_limit', function() {
        var iCustomerId = $("#customer").val();
        limit_page = this.value;
        $("#table_record").html('');
        $("#ajax-loader").show();
        url = "{{ route('admin.office.OfficeajaxListing') }}";

        setTimeout(function() {
            $.ajax({
                url: url,
                type: "get",
                data: {
                    limit_page: limit_page,
                    iCustomerId: iCustomerId
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            hideLoader();
        }, 500);
    });
</script>
@endsection
