@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
  <div class="card">
    <div class="col-lg-12 mx-auto">
      <div class="card-header">
        <ul class="c-nav-tab">
          <li class="">
            <a href="{{route('admin.customer.edit',$iCustomerId)}}" class="add-btn btn mx-2">
              Customer
            </a>
          </li>  
          <li class="">
            <a href="" class="active add-btn btn mx-2">
              Office
            </a>
          </li>
          @if(isset($offices->iOfficeId))
          <li>
            <a href="{{route('admin.doctorlist',['iCustomerId' => $offices->iCustomerId,'iOfficeId' =>$offices->iOfficeId])}}" class="btn add-btn mx-2">
              Doctor
            </a>
          </li>
          {{-- <li>
            <a href="{{route('admin.userlist',['iCustomerId' => $offices->iCustomerId,'iOfficeId' =>$offices->iOfficeId])}}" class="btn add-btn mx-2">
              User
            </a>
          </li> --}}
          @endif
        </ul>
      </div>
      <form action="{{route('admin.office.officestore',$iCustomerId)}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="@if(isset($offices)) {{$offices->iOfficeId}} @endif">
        <div class="col-xl-12 col-lg-12 col-md-12">
          <label>Office</label>
          <input type="text" class="form-control" id="OfficeName" name="OfficeName" value="{{$id}}" readonly>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Customer</label>
            <select name="iCustomerId" id="iCustomerId">
              <option value="none">Select customer</option>
              @foreach($customer as $key => $customers)
              <option value="{{$customers->iCustomerId}}"{{$customers->iCustomerId == $iCustomerId  ? 'selected' : ''}}>{{$customers->vOfficeName}}</option>
              @endforeach
            </select>
            <div class="text-danger" style="display: none;" id="iCustomerId_error">Please select customer</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Status</label>
          <select id="eStatus" name="eStatus">
            <option value="Active" @if(isset($offices)) @if($offices->eStatus == 'Active') selected @endif @endif>Active</option>
            <option value="Inactive" @if(isset($offices)) @if($offices->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
            <option value="Stand By" @if(isset($offices)) @if($offices->eStatus == 'Stand By') selected @endif @endif>Stand By</option>
            <option value="Suspended" @if(isset($offices)) @if($offices->eStatus == 'Suspended') selected @endif @endif>Suspended</option>
          </select>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Office name</label>
          <input type="text" class="form-control" id="vOfficeName" name="vOfficeName" placeholder="Office name" value="@if(old('vOfficeName')!=''){{old('vOfficeName')}}@elseif(isset($offices->vOfficeName)){{$offices->vOfficeName}}@else{{old('vOfficeName')}}@endif">
          <div class="text-danger" style="display: none;" id="vOfficeName_error">Please enter office name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Office code</label>
          <input type="text" class="form-control" id="vOfficeCode" name="vOfficeCode" placeholder="Office code" value="@if(old('vOfficeCode')!=''){{old('vOfficeCode')}}@elseif(isset($offices->vOfficeCode)){{$offices->vOfficeCode}}@else{{old('vOfficeCode')}}@endif">
          <div class="text-danger" style="display: none;" id="vOfficeCode_error">Please enter office code</div>
          {{-- <div class="text-danger" style="display: none;" id="vOfficeCodeMax_error">Please enter max 5 latter</div> --}}
          
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Address</label>
          <input type="text" class="form-control" id="vAddress" name="vAddress" placeholder="Address" value="@if(old('vAddress')!=''){{old('vAddress')}}@elseif(isset($offices->vAddress)){{$offices->vAddress}}@else{{old('vAddress')}}@endif">
          <div class="text-danger" style="display: none;" id="vAddress_error">Please enter </div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>City</label>
          <input type="text" class="form-control" id="vCity" name="vCity" placeholder="City" value="@if(old('vCity')!=''){{old('vCity')}}@elseif(isset($offices->vCity)){{$offices->vCity}}@else{{old('vCity')}}@endif">
          <div class="text-danger" style="display: none;" id="vCity_error">Please enter city</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>State</label>
            <select name="iStateId" id="iStateId">
              @foreach($states as $key => $states)
              <option value="{{$states->iStateId}}" @if(isset($offices)){{$states->iStateId == $offices->iStateId  ? 'selected' : ''}} @endif>{{$states->vState}}</option>
              @endforeach
            </select>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Zip code</label>
          <input type="number" class="form-control" id="vZipCode" name="vZipCode" placeholder="zipcode" value="@if(old('vZipCode')!=''){{old('vZipCode')}}@elseif(isset($offices->vZipCode)){{$offices->vZipCode}}@else{{old('vZipCode')}}@endif">
          <div class="text-danger" style="display: none;" id="vZipCode_error">Please enter zipcode</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Contact name</label>
          <input type="text" class="form-control" id="vName" name="vName" placeholder="Name" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($offices->vName)){{$offices->vName}}@else{{old('vName')}}@endif">
          <div class="text-danger" style="display: none;" id="vName_error">Please enter name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Title</label>
          <input type="text" class="form-control" id="vTitle" name="vTitle" placeholder="Title" value="@if(old('vTitle')!=''){{old('vTitle')}}@elseif(isset($offices->vTitle)){{$offices->vTitle}}@else{{old('vTitle')}}@endif">
          <div class="text-danger" style="display: none;" id="vTitle_error">Please enter title</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Email</label>
          <input type="text" class="form-control" id="vEmail" name="vEmail" placeholder="Email" value="@if(old('vEmail')!=''){{old('vEmail')}}@elseif(isset($offices->vEmail)){{$offices->vEmail}}@else{{old('vEmail')}}@endif">
          <div class="text-danger" style="display: none;" id="vEmail_error">Please enter email</div>
          <div class="text-danger" style="display: none;" id="vEmail_valid_error">Please enter valid email</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Website name</label>
          <input type="text" class="form-control" id="vWebsiteName" name="vWebsiteName" placeholder="website name" value="@if(old('vWebsiteName')!=''){{old('vWebsiteName')}}@elseif(isset($offices->vWebsiteName)){{$offices->vWebsiteName}}@else{{old('vWebsiteName')}}@endif">
          <div class="text-danger" style="display: none;" id="vWebsiteName_error">Please enter website name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Mobile</label>
          <input type="text" class="form-control" id="vMobile" name="vMobile" placeholder="Mobile" value="@if(old('vMobile')!=''){{old('vMobile')}}@elseif(isset($offices->vMobile)){{$offices->vMobile}}@else{{old('vMobile')}}@endif">
          <div class="text-danger" style="display: none;" id="vMobile_error">Please enter mobile</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Cell</label>
          <input type="text" class="form-control" id="vCellulor" name="vCellulor" placeholder="Cell" value="@if(old('vCellulor')!=''){{old('vCellulor')}}@elseif(isset($offices->vCellulor)){{$offices->vCellulor}}@else{{old('vCellulor')}}@endif">
          <div class="text-danger" style="display: none;" id="vCellulor_error">Please enter cell</div>
        </div>
        <div class="col-xl-6 col-lg-12 col-md-12">
          <label>Note</label>
          <div id="toolbar-container"></div>
            <textarea class="form-control" id="tDescription" name="tDescription" placeholder="Note">@if(old('tDescription')!=''){{old('tDescription')}}@elseif(isset($offices->tDescription)){{$offices->tDescription}}@else{{old('tDescription')}}@endif</textarea>
          <div id="tDescription_error" class="text-danger" style="display: none;">Please enter note </div>
        </div>
        
        <div class="col-12 align-self-end d-inline-block">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.officelist',$iCustomerId)}}" class="btn back-btn">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
 $('#iCustomerId').selectize();
 $('#eStatus').selectize();
 $('#iStateId').selectize();
  $(document).on('change', '#vImage', function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#img').attr('src', e.target.result);
      };
      reader.readAsDataURL(this.files[0]);
    }
  });
  $(document).on('click', '#submit', function() {
    id          = $("#id").val();
    vOfficeName = $("#vOfficeName").val();
    iCustomerId = $("#iCustomerId").val();
    vOfficeCode = $("#vOfficeCode").val();
    vAddress    = $("#vAddress").val();
    vCity       = $("#vCity").val();
    vZipCode    = $("#vZipCode").val();
    vTitle      = $("#vTitle").val();
    vWebsiteName= $("#vWebsiteName").val();
    vName       = $("#vName").val();
    vEmail      = $("#vEmail").val();
    vMobile     = $("#vMobile").val();
    vCellulor   = $("#vCellulor").val();
    tDescription= $("#tDescription").val();
    
    var error = false;

    if (vOfficeName.length == 0) {
      $("#vOfficeName_error").show();
      error = true;
    } else {
      $("#vOfficeName_error").hide();
    }
    if (iCustomerId == "none") {
      $("#iCustomerId_error").show();
      error = true;
    } else {
      $("#iCustomerId_error").hide();
    }
    if(vOfficeCode.length == 0){
        $("#vOfficeCode_error").show();
        // $("#vOfficeCodeMax_error").hide();
        error = true;
      } else{
        // if(vOfficeCode.length > 5){
        //   $("#vOfficeCodeMax_error").show();
        //   $("#vOfficeCode_error").hide();
        //   error = true;
        // }else{
          // $("#vOfficeCodeMax_error").hide();
          $("#vOfficeCode_error").hide();
        }
    if (vAddress.length == 0) {
      $("#vAddress_error").show();
      error = true;
    } else {
      $("#vAddress_error").hide();
    }
    if (vCity.length == 0) {
      $("#vCity_error").show();
      error = true;
    } else {
      $("#vCity_error").hide();
    }
    if (vZipCode.length == 0) {
      $("#vZipCode_error").show();
      error = true;
    } else {
      $("#vZipCode_error").hide();
    }
    if (vTitle.length == 0) {
      $("#vTitle_error").show();
      error = true;
    } else {
      $("#vTitle_error").hide();
    }
    
    if (vName.length == 0) {
      $("#vName_error").show();
      error = true;
    } else {
      $("#vName_error").hide();
    }
    if (vEmail.length == 0) {
      $("#vEmail_error").show();
      $("#vEmail_valid_error").hide();
        error = true;
    }else{
      if(validateEmail(vEmail))
      {
        $("#vEmail_valid_error").hide();

      }else{
        $("#vEmail_valid_error").show();
        $("#vEmail_error").hide();
        error = true;
      }
    }
     
    
    
    
    

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
function validateEmail(vEmail) 
{
  var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  if (filter.test(vEmail)) {
    return true;
  }
  else {
    return false;
  }
}
$(document).ready(function() {
    var mobile = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-######"}];
    $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
   })
  $(document).ready(function() {
  var cellulor = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-######"}];
  $('#vCellulor').inputmask({ 
      mask: cellulor, 
      greedy: false, 
      definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
  })
</script>
@endsection