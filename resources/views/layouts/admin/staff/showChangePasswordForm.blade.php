@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
  <div class="page-title text-center">
    <h3>
    Changes password
    </h3>
</div>
  <div class="card">
    <div class="col-lg-12 mx-auto">
      @if(isset($staffs))
      <div class="card-header py-5">
      
        <ul class="c-nav-tab">
          <li>
          <a class="btn add-btn active  mx-2" href="{{route('admin.staff.ChangePassword',$staffs->iStaffId)}}" aria-controls="#tab-{{ $staffs->iStaffId }}" aria-controls="#tab-7"> Changes password</a>
          </li>
          <li>
          <a href="{{route('admin.staff')}}" aria-controls="#tab-7" class="btn add-btn  mx-2"> staff</a>
          </li>
         
        
      </ul>



      </div>
      @endif
      <form action="{{route('admin.staff.changePassword')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="iLoginAdminId" value="@if(isset($staffs->iLoginAdminId)) {{$staffs->iLoginAdminId}} @endif">
        <input type="hidden" name="iCustomerId" value="@if(isset($staffs->iCustomerId)) {{$staffs->iCustomerId}} @endif">
        <input type="hidden" name="id" value="@if(isset($staffs)) {{$staffs->iStaffId}} @endif">
        <div class="col-xl-6 col-lg-12 col-md-6">
          <label>New password</label>
          <input type="Password" class="form-control" id="vPassword" name="vPassword" placeholder="Password" value="">
          <div class="text-danger" style="display: none;" id="vPassword_error">Please enter password</div>
        </div>
        <div class="col-xl-6 col-lg-12 col-md-6">
          <label>Confirm new password</label>
          <input type="Password" class="form-control" id="vPassword2" name="vPassword2" placeholder="Confirm password" value="">
          <div class="text-danger" style="display: none;" id="vPassword2_error">Please enter confirm password</div>
          <div class="text-danger" id="vPassword2_same_error" style="display: none;">Password should match</div>
        </div>
        <div class="col-4 align-self-end d-inline-block mx-auto text-center">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.staff')}}" class="btn back-btn">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script>
  $(document).on('click', '#submit', function() {
    vPassword = $("#vPassword").val();
    vPassword2 = $("#vPassword2").val();

    var error = false;
    if (vPassword.length == 0) {
      $("#vPassword_error").show();
      error = true;
    } else {
      $("#vPassword_error").hide();
    }

    if (vPassword2.length == 0) {
      $("#vPassword2_error").show();
      error = true;
    } else {
      $("#vPassword2_error").hide();
    }

    if (vPassword.length != 0 && vPassword2.length != 0) {
      if (vPassword != vPassword2) {
        $("#vPassword2_same_error").show();
        return true;
      } else {
        $("#vPassword2_same_error").hide();
      }
    } else {
      $("#vPassword2_same_error").hide();
    }
    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
</script>
@endsection