@php
$admin = \App\Libraries\General::admin_info();
// dd($admin);
$admin_logo = \App\Libraries\General::get_user_logo();

$banner_detail = \App\Libraries\General::get_banner_data($admin['iCustomerId']);


@endphp
<style>
	 /* Scan css start */
	 video{
        /* width: 500px; */
    height: 60vh;
    }
		.modal-backdrop{
			/* z-index: 0 !important; */
		}
   
		@media (min-width: 992px)
		{
			.aside-fixed .aside {
			z-index: 10 !important ;
			}
		}
    /* Scan css end */
	/* slider css start */
		.siteload_popup {
		position: fixed;
		top: 0;
		bottom: 0;
		right: 0;
		left: 0;
		background: rgb(0 0 0 / 50%);
		text-align: center;
		z-index: 9999;
	}
	.inner_siteload_popup {
		display: flex;
		justify-content: center;
		align-items: center;
		width: 100%;
		height: 100%;
	}
@media (min-width: 1440px)
{
	.body_sitepopup {
	
		width: 600px;
	}
}

.body_sitepopup {
    width: 90%;
    margin: auto;
    background: transparent;
}
.body_sitepopup .siteload_popup_content {
    height: 80vh;
    display: inline-block;
    position: relative;
    width: 100%;
}
.siteheader {
    position: absolute;
    top: -6px;
    right: -6px;
    z-index: 99;
}

.mobileviews {
    position: relative;
}

.mobileviews .item img {
    height: 80vh;
    object-fit: contain;
}

.mobileviews .owl-nav button span {
    font-size: 60px;
    color: #ffffff;
    line-height: 1;
}
.siteheader a {
  display: inline-block;
  font-size: 26px;
  color: #ffffff;
  padding: 0 10px;
}
.mobileviews .owl-nav button.owl-next {
  right: 0 !important;
  left: auto !important;
}

	/* slider css end */
</style>
<link rel="stylesheet" type="text/css" href="https://www.evergreeneec.com.au/assets/front/css/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="https://www.evergreeneec.com.au/assets/front/css/owl.theme.default.min.css">


<div id="kt_header" class="header align-items-stretch">
	<!--begin::Container-->
	<div class="container-fluid d-flex align-items-stretch justify-content-between">
		<!--begin::Aside mobile toggle-->
		<div class="d-flex align-items-center d-lg-none ms-n2 me-2" title="Show aside menu">
			<div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" id="kt_aside_mobile_toggle">
				<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->	
				<span class="svg-icon svg-icon-1">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
						<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
						<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
					</svg>
				</span>
				<!--end::Svg Icon-->
			</div>
		</div>
		<!--end::Aside mobile toggle-->
		<!--begin::Mobile logo-->
		<div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
			{{-- <a href="../../demo1/dist/index.html" class="d-lg-none">
				<img alt="Logo" src="assets/media/logos/logo-2.svg" class="h-30px" />
			</a> --}}
		</div>
		
		<!--end::Mobile logo-->
		<!--begin::Wrapper-->
		<div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1">
			<!--begin::Navbar-->
			<div class="d-flex align-items-stretch w-100 text-center" id="kt_header_nav">
				<!--begin::Menu wrapper-->
				{{-- <h3 class="my-auto text-center w-100">{{$admin['CustomerName']}}</h3> --}}
				<!--end::Menu wrapper-->
			</div>
			<!--end::Navbar-->
			<!--begin::Topbar-->
			        {{-- Scan Driver Slip start --}}
                @if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == '1')
                <div class="col-lg-2 col-sm-6 align-self-center">
                    <a href="javascript:;" id="scan_driver_slip" class="btn add-btn mx-2 ">
                      <i class="fa fa-scanner-touchscreen"></i> Scan Driver Slip
                    </a>
                </div>
                @endif
                {{-- Scan Driver Slip End --}}
			<div class="d-flex align-items-stretch flex-shrink-0">
				
				<!--begin::Toolbar wrapper-->
				<div class="d-flex align-items-stretch flex-shrink-0">
					<!--begin::Search-->
					
					
					<!--begin::Notifications-->
						<div class="d-flex align-items-center ms-1 ms-lg-3" id="kt_header_user_menu_toggle" style="color:#0062a3">
						<!--begin::Menu wrapper-->
						<div class="cursor-pointer symbol symbol-30px symbol-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
							<div style="font-size: 19px;">
								<span>Hi </span>
								@if(isset($admin['vFirstName']) && !empty($admin['vFirstName']))
									<span>{{$admin['vFirstName']}}</span>
								@else
									<span>{{$admin['vName']}}</span>
								@endif
								<!-- <span>at {{$admin['CustomerName']}}</span> -->
							</div>
							<span>Today is {{date('l, F d, Y')}}</span>							
						</div>
						<div class="symbol symbol-50px mt-0 me-5" style="font-size: 14px;" >
							@if(isset($admin_logo) && $admin_logo!='LabslipLogo')
							<input type="file" name="vImage" id="vImage_Header" accept="image/*"  style="display:none" >
							<img  src="{{$admin_logo}}" id="upfile1" style="cursor:pointer"/>
							@else
							<input type="file" name="vImage_Header" id="vImage_Header" accept="image/*"  style="display:none" >
							<img  src="{{asset('admin/assets/images/logo/Deafult-user.jpg')}}" id="upfile1" style="cursor:pointer"/>
							@endif
							<span id="uploaded_image"></span>
						</div>
						<!--begin::Menu-->
						
						<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px" data-kt-menu="true">
							<!--begin::Menu item-->
							<div class="menu-item px-3">
								<div class="menu-content d-flex align-items-center px-3">

									<!--begin::Username-->
									<div class="d-flex flex-column">
										<div class="fw-bolder d-flex align-items-center fs-5">
	                                    {{$admin['vName']}} 
	                                </div>
										<a href="#" id="OpenUserDetail">{{$admin['vEmail']}}</a>
										{{-- <a href="#" id="OpenUserDetail" data-bs-toggle="modal" data-bs-target="#UserDetailModal">
											{{$admin['vEmail']}}
										  </a> --}}
										{{-- @if(isset($admin['customerType']) && $admin['customerType'] =='Super Admin')
										<a href="{{route('admin.admin.edit',$admin['user_id'])}}" class="fw-bold text-muted text-hover-primary fs-7">
										@else
										<a href="{{route('admin.staff.edit',$admin['user_id'])}}" class="fw-bold text-muted text-hover-primary fs-7">
											{{$admin['vEmail']}}
										@endif --}}
	                                </a>
									</div>
									<!--end::Username-->
								</div>
							</div>
							<div class="menu-item px-5">
								<a href="{{route('admin.logout')}}" class="menu-link px-5">Sign Out</a>
							</div>
							<!--end::Menu item-->
							<!--begin::Menu separator-->
							<div class="separator my-2"></div>
							<!--end::Menu separator-->
							
						</div>
						<!--end::Menu-->
						<!--end::Menu wrapper-->
					</div>
					<!--end::User -->
					<!--begin::Heaeder menu toggle-->
					<div class="d-flex align-items-center d-lg-none ms-2 me-n3" title="Show header menu">
						<div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" id="kt_header_menu_mobile_toggle">
							<!--begin::Svg Icon | path: icons/duotune/text/txt001.svg-->
							<span class="svg-icon svg-icon-1">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<path d="M13 11H3C2.4 11 2 10.6 2 10V9C2 8.4 2.4 8 3 8H13C13.6 8 14 8.4 14 9V10C14 10.6 13.6 11 13 11ZM22 5V4C22 3.4 21.6 3 21 3H3C2.4 3 2 3.4 2 4V5C2 5.6 2.4 6 3 6H21C21.6 6 22 5.6 22 5Z" fill="black" />
									<path opacity="0.3" d="M21 16H3C2.4 16 2 15.6 2 15V14C2 13.4 2.4 13 3 13H21C21.6 13 22 13.4 22 14V15C22 15.6 21.6 16 21 16ZM14 20V19C14 18.4 13.6 18 13 18H3C2.4 18 2 18.4 2 19V20C2 20.6 2.4 21 3 21H13C13.6 21 14 20.6 14 20Z" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
					</div>
					<!--end::Heaeder menu toggle-->
				</div>
				<!--end::Toolbar wrapper-->
			</div>
			<!--end::Topbar-->
		</div>
		<!--end::Wrapper-->
	</div>
	<!--end::Container-->
	{{-- Driver scan modal start --}}
{{-- <div class="modal fade " id="DriverModelScan" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="DriverModelScanLabel" aria-hidden="true"> --}}
<div class="modal fade  show" id="DriverModelScan" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="DriverModelScanLabel" bis_skin_checked="1" aria-modal="true" role="dialog" style="display: none;">
    <div class="modal-dialog modal-xl " style="max-width: 1000px;width: 700px; margin-top:100px;">
      <div class="modal-content" style="height: 70vh;overflow: hidden;">
        <div class="modal-header">
          <h5 class="modal-title" id="DriverModelScanLabel">Scan Your Qr Code</h5>
          <button type="button" class="btn-close close-cam close-model" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body p-0"  bis_skin_checked="1" >
            <div style=" text-align:center">
                <video class="p-0" id="preview"></video>
              
            </div>
            {{-- <div style="align-items: center; display: flex; justify-content: center;margin-top: 24px; ">
                    <label class="btn btn-primary active">
                     <input autocomplete="off"  name="options" type="radio" value="1" /> Front Camera
                      </label>
                      <label class="btn btn-secondary">    
                     <input autocomplete="off" checked="" name="options" type="radio" value="2" /> Back Camera
                      </label>
            </div> --}}
         </div>
        </div>
   
        </div>
</div>
      
{{-- Driver scan modal end --}}


{{-- User details modal start --}}

<div class="modal fade  show" id="UserDetailModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="UserDetailModalLabel" bis_skin_checked="1" aria-modal="true" role="dialog" style="display: none;">
    <div class="modal-dialog modal-xl" bis_skin_checked="1">
       <div class="modal-content mt-20" bis_skin_checked="1">
            <div class="modal-header" bis_skin_checked="1">
                <h5 class="modal-title" id="UserDetailModalLabel">User Details</h5>
                <button type="button" class="btn-close close-model" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
             <div class="modal-body" id="user_data" bis_skin_checked="1">
                
             </div>
             
       </div>
    </div>
</div>
{{-- User details modal end --}}

{{-- Banner modal start --}}


@if(isset($banner_detail) && count($banner_detail)>0)
<div class="siteload_popup" id="siteload_popup" style="display: none">
	<div class="inner_siteload_popup">
	  <div class="body_sitepopup">
		  <div class="siteload_popup_content">
		  <div class="siteheader text-right">
			  <a href="javascript:;" id="sitepopup_dissmiss">&times;</a>
			</div>
			<div class="mobileviews py-5">
				<div class="owl-carousel owl-theme">
					@foreach ($banner_detail as $banner_detail_val)
					<div class="item">
						<img src="{{asset('uploads/banner/'.$banner_detail_val->vImage)}}">
					</div>
					@endforeach
				</div>
			  </div>
			<div class="desktopviews">
			</div>
		</div>
	</div>	  
</div>
</div>
@endif
{{-- Banner modal end --}}

<!-- Button trigger modal -->


</div>

{{-- Driver scan start --}}
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 <script crossorigin="anonymous" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

<script type="text/javascript" src="https://www.evergreeneec.com.au/assets/front/js/owl.carousel.min.js"></script>
{{-- Driver scan end --}}

{{-- User Modal Js start --}}
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}}


<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script>
   $(document).on('change', '#vImage', function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#img').attr('src', e.target.result);
      };
      reader.readAsDataURL(this.files[0]);
    }
  });
     

  $(document).on('click', '#submit_staff_modal', function() {
    vFirstName  = $("#vFirstName_H").val();
    vLastName   = $("#vLastName_H").val();
    
    var error = false;

  
    if (vFirstName.length == 0) {
      $("#vFirstName_error").show();
      error = true;
    } else {
      $("#vFirstName_error").hide();
    }
    if (vLastName.length == 0) {
      $("#vLastName_error").show();
      error = true;
    } else {
      $("#vLastName_error").hide();
    }

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm_staff").submit();
        return true;
      }
    }, 1000);

  });





//User Modal Js end

	  // Scan driver slip start
	  var scanner;
	  $('#scan_driver_slip').click(function(){
            // $('#DriverModelScan').modal('show');
			
            scanner = new Instascan.Scanner({ video: document.getElementById('preview'), scanPeriod: 3, mirror: false,
            continuous: true,
            backgroundScan: false,
            captureImage: false,
            // mirror: true
            });
           
            scanner.addListener('scan',function(content){
            window.navigator.vibrate(300);
            window.location.href = content;
            });
            Instascan.Camera.getCameras().then(function (cameras){
				$('#DriverModelScan').modal('show');
           if(cameras.length>0){
			$('#DriverModelScan').show();
            $('#DriverModelScan').addClass('show');
            if(cameras[1]!= undefined && cameras[1]!= '')
            {
                scanner.start(cameras[1]);
            }else
            {
                scanner.start(cameras[0]);
            }
            
            $('[name="options"]').on('change',function(){
                
            if($(this).val()==1){
            if(cameras[0]!=""){
            scanner.start(cameras[0]);
            }else{
            alert('No Front camera found!');
            }
            }else if($(this).val()==2){
            if(cameras[1]!=""){
            scanner.start(cameras[1]);
            }else{
                alert('No Back camera found!');
            }
            }
            
        });
        }else{
        console.error('No cameras found.');
        alert('No cameras found.');
        }
        }).catch(function(e){
            console.error(e);
            alert(e);
        });
        });
        
        $('.close-cam').click(function(){
                scanner.stop()
            });
      
        // Scan driver slip end


		$(document).ready(function(e) {
		// Banner model show start
		@if(isset($admin['iCustomerTypeId']) && $admin['iCustomerTypeId'] == 6 && count($banner_detail)>0 && Session::has('FirstTimeLogin') && Session::get('FirstTimeLogin') =='Yes');
			$('body').addClass('popup_open')
			$('#siteload_popup').show();
		@endif
		// Banner model show end
			

        $(".showonhover").click(function(){
			$("#selectfile").trigger('click');
			});
    	});
		$('.close-model').click(function(){
				$('#DriverModelScan').hide();
				$('#DriverModelScan').removeClass('show');
		});


$("#upfile1").click(function () {
    $("#vImage_Header").trigger('click');
});



// var input = document.querySelector('input[type=file]'); // see Example 4
var input = document.getElementById('vImage_Header'); // see Example 4

input.onchange = function () {

  var file = input.files[0];
  UploadImage(file);   

};

function UploadImage(file) {
	
	var user_id = '{{$admin["user_id"]}}';
	var customerType = '{{$admin["customerType"]}}';
	var Image = file;

	var vImage = Image.name;
	var form_data = new FormData();
	var ext = vImage.split('.').pop().toLowerCase();
	if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
	{
		alert("Invalid Image File");
		return false;
	}

	var oFReader = new FileReader();
	oFReader.readAsDataURL(Image);
	var f = Image;
	var fsize = f.size||f.fileSize;
	if(fsize > 6000000)
	{
		alert("Image File Size is very big");
		return false;
	}
	else
	{
		form_data.append("vImage", Image);
		form_data.append("_token", '{{csrf_token()}}');
		form_data.append("user_id", user_id);
		form_data.append("customerType", customerType);
	$.ajax({
			url: "{{route('admin.labcase.UploadUserImage')}}",
			type: "post",
			data: form_data,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend:function(){
				showLoader();
			},  
			success: function(result) {
				hideLoader();
				displayAsImage(file);
			}
		});
	}
  };


function displayAsImage(file) {
  var imgURL = URL.createObjectURL(file),
  img = document.createElement('img');
  img.onload = function() {
    URL.revokeObjectURL(imgURL);
  };
  img.src = imgURL;
  $('#upfile1').attr('src',img.src);
}

function showLoader()
{
    $("#overlay").fadeIn(300);　
}
function hideLoader()
{
  // setTimeout(function(){
    $("#overlay").fadeOut(300);
  // },100);
}

$('#OpenUserDetail').click(function(){

	$('#UserDetailModal').show();
    $('#UserDetailModal').addClass('show');
	var iStaffId = '{{$admin["user_id"]}}'
	$.ajax({
		url: "{{route('admin.staff.ShowUSerDetail')}}",
		type: "post",
		data:{
			iStaffId :iStaffId,
			UserStaffModal:'Yes',
			_token :'{{csrf_token()}}'
		},
		success: function(response) {
				$("#user_data").html(response);
				
		}
	});
});

$('.btn-close').click(function(){
	$('#BannerModal').hide();
    $('#BannerModal').removeClass('show');
});


$('.mobileviews .owl-carousel').owlCarousel({
      items:1,
      loop:false,
      autoplay:false,
      margin:0,
      nav:true,
      dots:false,
      autoplayTimeout:3000,
      responsive:{
        0:{
          items:1
        },
        600:{
          items:1
        },
        768:{
          items:1
        },
        1000:{
          items:1
        }
      }
    });

$('#sitepopup_dissmiss').click(function(){
    $('#siteload_popup').hide();
    $('body').removeClass('popup_open');
	@if(Session::has('FirstTimeLogin') && count($banner_detail) !=0)
		@php
			Session::forget('FirstTimeLogin');
		@endphp
	@endif
  });

</script>