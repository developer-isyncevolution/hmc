@if(count($data) > 0)

@foreach($data as $key => $value)

<tr>
	<!-- <td>
				<div class="form-check form-check-sm form-check-custom form-check-solid">
					<input id="Category_ID_{{$value->iAddCategoryId}}" type="checkbox" name="Category_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iAddCategoryId}}">
					<label for="Category_ID_{{$value->iAddCategoryId}}">&nbsp;</label>
				</div>
			</td> -->
	<td>{{$value->vCategoryName}}</td>
	<td>{{$value->vSequenc}}</td>
	<td>
		<span class="badge badge-light-success">
			{{$value->eStatus}}
		</span>
	</td>
	<td>
		<div class="d-inline-flex align-items-center">
			<a href="{{route('admin.addoncategory.edit',$value->iAddCategoryId)}}" class="btn-icon edit_permission edit-icon me-4">
				<i class="fad fa-pencil"></i>
			</a>
			<a href="javascript:;" id="delete" data-id="{{$value->iAddCategoryId}}" class="btn-icon delete-icon delete_permission me-4">
				<i class="fad fa-trash-alt"></i>
			</a>
		</div>

	</td>
</tr>
@endforeach
<tr>
	{{-- <td  class="text-center border-right-0">
				<a href="javascript:;" title="Multiple delete" id="delete_btn" class="btn-icon  delete_permission delete-icon">
				<i class="fad fa-trash-alt"></i>
				</a>
			</td> --}}
			<td class="border-0" colspan="2">
				<div class="d-flex">
					{!! $paging !!}
				</div>
			</td>
			{{-- <td class="border-0"></td>
			<td colspan="1" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
			
			<td colspan="2"  class="text-end border-left-0">
		<select class="w-100px show-drop" id="upper_page_limit">
			<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 10)
				selected
				@endif value="10">10</option>
			<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 20)
				selected
				@endif value="20">20</option>
			<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 50)
				selected
				@endif value="50">50</option>
			<option @if (isset(Request()->upper_limit_page) && Request()->upper_limit_page == 100)
				selected
				@endif value="100">100</option>
		</select>
	</td>
</tr>
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif