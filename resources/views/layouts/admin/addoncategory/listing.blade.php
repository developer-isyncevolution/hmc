@extends('layouts.admin.index')
@section('content')
    <div class="main-panel">
        <div class="card mb-5 mb-xl-4">
            <div class="page-title text-center">
                <h3>
                    Lab Administrator
                </h3>
            </div>
            <div class="card-header border-0">
                <ul class="c-nav-tab">
                @if(\App\Libraries\General::check_permission('CategoryController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.category')}}" class=" add-btn btn mx-2">
                        Categories
                    </a>
                </li>
                @endif
                @if(\App\Libraries\General::check_permission('CategoryProductController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.categoryproduct')}}" class="  btn add-btn  mx-2">
                        Products
                    </a>
                </li>
                @endif
                @if(\App\Libraries\General::check_permission('ProductStageController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.productstage')}}" class="  btn add-btn mx-2">
                        Stages
                    </a>
                </li>
                @endif
                @if(\App\Libraries\General::check_permission('AddoncategoryController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.addoncategory')}}" class=" active btn add-btn mx-2">
                        Categories Add ons
                    </a>
                </li>
                @endif
                @if(\App\Libraries\General::check_permission('SubaddoncategoryController','eRead') == 'true')
                <li>
                    <a href="{{route('admin.subaddoncategory')}}" class="btn add-btn mx-2">
                        Add ons
                    </a>
                </li>
                @endif
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <ul class="d-flex justify-content-center my-2 list-unstyled">
                    <li>
                        <a href="{{ route('admin.addoncategory.create') }}" class="btn add-btn create_permission me-2">
                            <i class="fa fa-plus"></i> Add
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-6 mx-auto">
                <div class="card mb-5 mb-xl-4">

                    <h3 class="card-title">
                        Upper Categories Add on
                    </h3>

                    <div class="row g-4">
                        <div class="col-lg-8 mx-auto">
                            <div class="custome-serce-box">
                                <input type="text" class="form-control" id="upperkeyword" name="search"
                                    class="search" placeholder="Search">
                                <span class="search-btn"><i class="fas fa-search"></i></span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="listing-page">
                    <div class="table-data table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <!-- <th class="w-25px" data-orderable="false">
                                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                                        <input id="Uselectall" type="checkbox" name="Uselectall" class="form-check-input">
                                                        <label for="selectall">&nbsp;</label>
                                                    </div>
                                                </th> -->
                                    <th scope="col" class=""><a id="vCategoryName" class="upersort"
                                            data-column="vCategoryName" data-order="ASC" href="javascript:;"> <span
                                                class="text-muted fw-bold text-muted d-block fs-7">Name </span> </a></th>
                                    <th scope="col" class=""><a id="vSequenc" class="upersort"
                                            data-column="vSequenc" data-order="ASC" href="javascript:;"> <span
                                                class="text-muted fw-bold text-muted d-block fs-7"> Sequence </span> </a>
                                    </th>
                                    <th scope="col" class=""><a id="eStatus" class="upersort"
                                            data-column="eStatus" data-order="ASC" href="javascript:;"> <span
                                                class="text-muted fw-bold text-muted d-block fs-7"> Status </span> </a></th>
                                    <th scope="col" class="max-w-100px"><span
                                            class="text-muted fw-bold text-muted d-block fs-7"> Action </span></th>
                                </tr>
                            </thead>
                            <tbody id="upper_table_record">
                            </tbody>
                        </table>
                        <div class="text-center loaderimg">
                            <div class="loaderinner">
                                <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader"
                                    width="250px" height="auto" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mx-auto">
                <div class="card mb-5 mb-xl-4">


                    <h3 class="card-title">
                        Lower Categories Add on
                    </h3>

                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <div class="custome-serce-box">
                                <input type="text" class="form-control" id="lower_keyword" name="search"
                                    class="search" placeholder="Search">
                                <span class="search-btn"><i class="fas fa-search"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="listing-page">
                    <div class="table-data table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <!-- <th class="w-25px" data-orderable="false">
                                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                                        <input id="Lselectall" type="checkbox" name="Lselectall" class="form-check-input">
                                                        <label for="selectall">&nbsp;</label>
                                                    </div>
                                                </th> -->
                                    <th scope="col" class=""><a id="vCategoryName" class="lowersort"
                                            data-column="vCategoryName" data-order="ASC" href="javascript:;"> <span
                                                class="text-muted fw-bold text-muted d-block fs-7">Name </span> </a></th>
                                    <th scope="col" class=""><a id="vSequenc" class="lowersort"
                                            data-column="vSequenc" data-order="ASC" href="javascript:;"> <span
                                                class="text-muted fw-bold text-muted d-block fs-7"> Sequence </span> </a>
                                    </th>
                                    <th scope="col" class=""><a id="eStatus" class="lowersort"
                                            data-column="eStatus" data-order="ASC" href="javascript:;"> <span
                                                class="text-muted fw-bold text-muted d-block fs-7"> Status </span> </a></th>
                                    <th scope="col" class="max-w-100px"><span
                                            class="text-muted fw-bold text-muted d-block fs-7"> Action </span></th>
                                </tr>
                            </thead>
                            <tbody id="lower_table_record">
                            </tbody>
                        </table>
                        <div class="text-center loaderimg">
                            <div class="loaderinner">
                                <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="lower-ajax-loader"
                                    width="250px" height="auto" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function() {
            var eType = 'Upper';
            $.ajax({
                url: "{{ route('admin.addoncategory.ajaxListing') }}",
                type: "get",
                data: {
                    eType: eType,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $("#upper_table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });

        });

        $("#Uselectall").click(function() {
            if (this.checked) {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', true);
                });
            } else {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', false);
                });
            }
        });

        $(document).on('click', '#delete_btn', function() {
            var eType = 'Upper';
            var id = [];

            $("input[name='Category_ID[]']:checked").each(function() {
                id.push($(this).val());
            });

            var id = id.join(",");

            if (id.length == 0) {
                alert('Please select records.')
            } else {
                swal({
                        title: "Are you sure delete all upper categories add on.?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "{{ route('admin.addoncategory.ajaxListing') }}",
                                type: "get",
                                data: {
                                    id: id,
                                    eType: eType,
                                    action: 'multiple_delete'
                                },
                                success: function(response) {
                                    $("#upper_table_record").html(response);
                                    $("#ajax-loader").hide();
                                }
                            });
                        }
                    });
            }
        });
        $("#upperkeyword").keyup(function() {
            var upperkeyword = $("#upperkeyword").val();
            var eType = 'Upper';
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.addoncategory.ajaxListing') }}",
                type: "get",
                data: {
                    upperkeyword: upperkeyword,
                    eType: eType,
                    action: 'search'
                },
                success: function(response) {
                    $("#upper_table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '#delete', function() {
            swal({
                    title: "Are you sure delete this upper categories add on.?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        id = $(this).data("id");
                        var eType = 'Upper';

                        $("#ajax-loader").show();

                        $.ajax({
                            url: "{{ route('admin.addoncategory.ajaxListing') }}",
                            type: "get",
                            data: {
                                id: id,
                                eType: eType,
                                action: 'delete'
                            },
                            success: function(response) {
                                $("#upper_table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("Addon Category Deleted Successfully");
                                setTimeout(function() {
                                   location.reload();
                                }, 1000);
                            }
                        });
                    }
                })
        });
        $(document).on('click', '.upersort', function() {
            column = $(this).data("column");
            order = $(this).attr('data-order');
            var eType = 'Upper';


            if (order == "ASC") {
                $(this).attr('data-order', 'DESC');
            } else {
                $(this).attr('data-order', 'ASC');
            }

            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.addoncategory.LowerajaxListing') }}",
                type: "get",
                data: {
                    column: column,
                    order,
                    order,
                    eType: eType,
                    action: 'sort'
                },
                success: function(response) {
                    console.log(response);
                    $("#upper_table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '.ajax_page_upper', function() {
            pages = $(this).data("pages");
            var eType = 'Upper';
            $("#upper_table_record").html('');
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.addoncategory.ajaxListing') }}",
                type: "get",
                data: {
                    pages: pages,
                    eType: eType,
                },
                success: function(response) {
                    $("#upper_table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });
        $(document).on('change', '#upper_page_limit', function() {
            upper_limit_page = this.value;
            var eType = 'Upper';
            $("#upper_table_record").html('');
            $("#ajax-loader").show();
            url = "{{ route('admin.addoncategory.ajaxListing') }}";

            setTimeout(function() {
                $.ajax({
                    url: url,
                    type: "get",
                    data: {
                        upper_limit_page: upper_limit_page,
                        eType: eType
                    },
                    success: function(response) {
                        $("#upper_table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
                // hideLoader();
            }, 500);
        });
    </script>
    <script>
        $(document).ready(function() {
            var eType = 'Lower';

            $.ajax({
                url: "{{ route('admin.addoncategory.LowerajaxListing') }}",
                type: "get",
                data: {
                    eType: eType,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $("#lower_table_record").html(response);
                    $("#lower-ajax-loader").hide();
                }
            });
        });

        $("#Lselectall").click(function() {
            if (this.checked) {
                $('.lcheckboxall').each(function() {
                    $(".lcheckboxall").prop('checked', true);
                });
            } else {
                $('.lcheckboxall').each(function() {
                    $(".lcheckboxall").prop('checked', false);
                });
            }
        });

        $(document).on('click', '#lower_delete_btn', function() {
            var id = [];
            var eType = 'Lower';
            $("input[name='Lower_Category_ID[]']:checked").each(function() {
                id.push($(this).val());
            });

            var id = id.join(",");

            if (id.length == 0) {
                alert('Please select records.')
            } else {
                swal({
                        title: "Are you sure delete all lower categories add on.?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "{{ route('admin.addoncategory.LowerajaxListing') }}",
                                type: "get",
                                data: {
                                    id: id,
                                    eType: eType,
                                    action: 'multiple_delete'
                                },
                                success: function(response) {
                                    $("#lower_table_record").html(response);
                                    $("#lower-ajax-loader").hide();
                                }
                            });
                        }
                    });
            }
        });

        $("#lower_keyword").keyup(function() {
            var lower_keyword = $("#lower_keyword").val();
            var eType = 'Lower';
            $("#lower-ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.addoncategory.LowerajaxListing') }}",
                type: "get",
                data: {
                    lower_keyword: lower_keyword,
                    eType: eType,
                    action: 'search'
                },
                success: function(response) {
                    $("#lower_table_record").html(response);
                    $("#lower-ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '#lowerdelete', function() {
            swal({
                    title: "Are you sure delete this lower categories add on.?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        id = $(this).data("id");
                        var eType = 'Lower';
                        $("#lower-ajax-loader").show();

                        $.ajax({
                            url: "{{ route('admin.addoncategory.LowerajaxListing') }}",
                            type: "get",
                            data: {
                                id: id,
                                eType: eType,
                                action: 'delete'
                            },
                            success: function(response) {
                                $("#lower_table_record").html(response);
                                $("#lower-ajax-loader").hide();
                                notification_error("Addon Category Deleted Successfully");
                                setTimeout(function() {
                                   location.reload();
                                }, 1000);
                            }
                        });
                    }
                })
        });
        $(document).on('click', '.lowersort', function() {
            column = $(this).data("column");
            order = $(this).attr('data-order');
            var eType = 'Lower';


            if (order == "ASC") {
                $(this).attr('data-order', 'DESC');
            } else {
                $(this).attr('data-order', 'ASC');
            }

            $("#lower-ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.addoncategory.LowerajaxListing') }}",
                type: "get",
                data: {
                    column: column,
                    order,
                    order,
                    eType: eType,
                    action: 'sort'
                },
                success: function(response) {
                    console.log(response);
                    $("#lower_table_record").html(response);
                    $("#lower-ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '.ajax_page_lower', function() {
            pages = $(this).data("pages");
            var eType = 'Lower';
            $("#lower_table_record").html('');
            $("#lower-ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.addoncategory.LowerajaxListing') }}",
                type: "get",
                data: {
                    pages: pages,
                    eType: eType,
                },
                success: function(response) {
                    $("#lower_table_record").html(response);
                    $("#lower-ajax-loader").hide();
                }
            });
        });
        $(document).on('change', '#lower_page_limit', function() {
            lower_limit_page = this.value;
            $("#lower_table_record").html('');
            $("#lower-ajax-loader").show();
            url = "{{ route('admin.addoncategory.LowerajaxListing') }}";

            setTimeout(function() {
                $.ajax({
                    url: url,
                    type: "get",
                    data: {
                        lower_limit_page: lower_limit_page
                    },
                    success: function(response) {
                        $("#lower_table_record").html(response);
                        $("#lower-ajax-loader").hide();

                    }
                });
                // hideLoader();
            }, 500);
        });
    </script>
@endsection
