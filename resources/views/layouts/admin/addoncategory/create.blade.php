@extends('layouts.admin.index')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<div class="main-panel">
  <div class="card">
    <div class="col-lg-12 mx-auto">
      <div class="card mb-5 mb-xl-4">
        <div class="page-title text-center">
          <h3>
          {{isset($addon->iAddCategoryId) ? 'Edit' : 'Add'}} Addon Category
          </h3>
        </div>
        <div class="card-header border-0">
          <ul class="c-nav-tab">
            <li class="">
              <a href="{{route('admin.category')}}" class=" add-btn btn mx-2">
                Categories
              </a>
            </li>
            <li class="">
              <a href="{{route('admin.categoryproduct')}}" class=" btn add-btn  mx-2">
                Products
              </a>
            </li>
            <li>
              <a href="{{route('admin.productstage')}}" class="btn add-btn mx-2">
                Stages
              </a>
            </li>
            <li>
              <a href="{{route('admin.addoncategory')}}" class="active btn add-btn mx-2">
                Categories Add on
              </a>
            </li>
            <li>
              <a href="{{route('admin.subaddoncategory')}}" class="btn add-btn mx-2">
                Add on
              </a>
            </li>
          </ul>
        </div>
      </div>     
      <form action="{{route('admin.addoncategory.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="iAddCategoryId" value="@if(isset($addon)) {{$addon->iAddCategoryId}} @endif">

        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Category Name</label><span> *</span>
          <input type="text" class="form-control" id="vCategoryName" name="vCategoryName" placeholder="Category Name" value="@if(old('vCategoryName')!=''){{old('vCategoryName')}}@elseif(isset($addon->vCategoryName)){{$addon->vCategoryName}}@else{{old('vCategoryName')}}@endif">
          <div class="text-danger" style="display: none;" id="vCategoryName_error">Please Enter Category Name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Sequence</label><span> *</span>
          <input type="number" class="form-control" id="vSequenc" name="vSequenc" placeholder="Sequence" value="@if(old('vSequenc')!=''){{old('vSequenc')}}@elseif(isset($addon->vSequenc)){{$addon->vSequenc}}@else{{old('vSequenc')}}@endif">
          <div class="text-danger" style="display: none;" id="vSequenc_error">Please Enter Sequence</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Status</label>
          <select id="eStatus" name="eStatus" class="form-control">
            <option value="Active" @if(isset($addon)) @if($addon->eStatus == 'Active') selected @endif @endif>Active</option>
            <option value="Inactive" @if(isset($addon)) @if($addon->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
          </select>

        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12 align-self-center">
          <label class="d-block mb-2">Category Add on Type</label>
          <input type="radio"  class="color_radio d-inline-block" id="Upper" name="eType" value="Upper" @if(isset($addon)) @if($addon->eType == 'Upper') checked @endif @endif>
            <label for="Upper" class="me-2">Upper</label>
          <input type="radio"  class="color_radio d-inline-block" id="Lower" name="eType" value="Lower" @if(isset($addon)) @if($addon->eType == 'Lower') checked @endif @endif>
            <label for="Lower" class="me-2">Lower</label>
            <input type="radio"  class="color_radio d-inline-block" id="Both" name="eType" value="Both" @if(isset($addon)) @if($addon->eType == 'Both') checked @endif @endif>
            <label for="Both" class="me-2">Both</label>
            <div class="text-danger" style="display: none;" id="eType_error">Please select type</div>
        </div>
        <div class="col-12 align-self-end d-inline-block ">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.addoncategory')}}" class="btn back-btn me-2">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
  $('#eStatus').selectize();
  $(document).on('click', '#submit', function() {
    iScheduleId = $("#id").val();
    vCategoryName = $("#vCategoryName").val();
    vSequenc = $("#vSequenc").val();
    eType = $('input[name="eType"]:checked').val();

    var error = false;


     if ($('input[name="eType"]:checked').length == 0) {
      $("#eType_error").show();
      error=true;
     }else {
      $("#eType_error").hide();
    }

    if (vCategoryName.length == 0) {
      $("#vCategoryName_error").show();
      error = true;
    } else {
      $("#vCategoryName_error").hide();
    }
    if (vSequenc.length == 0) {
      $("#vSequenc_error").show();
      error = true;
    } else {
      $("#vSequenc_error").hide();
    }

    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
</script>
@endsection