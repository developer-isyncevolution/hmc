@if(count($data) > 0)

@foreach($data as $key => $value)

		<tr>
			<!-- <td>
				<div class="form-check form-check-sm form-check-custom form-check-solid">
					<input id="Lower_Category_ID_{{$value->iAddCategoryId}}" type="checkbox" name="Lower_Category_ID[]" class="form-check-input widget-9-check lcheckboxall" value="{{$value->iAddCategoryId}}">
					<label for="Lower_Category_ID_{{$value->iAddCategoryId}}">&nbsp;</label>
				</div>
			</td> -->
			<td>{{$value->vCategoryName}}</td>
			<td>{{$value->vSequenc}}</td>
			<td>
				<span class="badge badge-light-success">
					{{$value->eStatus}}
				</span>
			</td>
			<td>
				<div class="d-inline-flex align-items-center">
					<a href="{{route('admin.addoncategory.edit',$value->iAddCategoryId)}}" class="btn-icon edit_permission edit-icon me-4">
						<i class="fad fa-pencil"></i>
					</a>
					<a href="javascript:;" id="delete" data-id="{{$value->iAddCategoryId}}" class="btn-icon delete-icon delete_permission me-4">
						<i class="fad fa-trash-alt"></i>
					</a>
				</div>
			</td>
		</tr>
@endforeach
		<tr>
			{{-- <td  class="text-center border-right-0">
				<a href="javascript:;" title="Multiple delete" id="lower_delete_btn" class="btn-icon  delete_permission delete-icon">
					<span class="svg-icon svg-icon-3">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
							<path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="black"></path>
							<path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="black"></path>
							<path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="black"></path>
						</svg>
					</span>
				</a>
			</td> --}}
			<td class="border-0" colspan="2">
				<div class="d-flex">
					{!! $paging !!}
				</div>
			</td>
			{{-- <td class="border-0"></td>
			<td colspan="1" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
			<td colspan="2"  class="text-end border-left-0">
				
				<select class="w-100px show-drop" id ="lower_page_limit">
					<option @if (isset(Request()->lower_limit_page) && Request()->lower_limit_page == 10)
						selected
						@endif value="10">10</option>
						<option @if (isset(Request()->lower_limit_page) && Request()->lower_limit_page == 20)
						selected
						@endif value="20">20</option>
						<option @if (isset(Request()->lower_limit_page) && Request()->lower_limit_page == 50)
							selected
						@endif value="50">50</option>
						<option @if (isset(Request()->lower_limit_page) && Request()->lower_limit_page == 100)
							selected
						@endif value="100">100</option>
				</select>
			</td>
		</tr>
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif