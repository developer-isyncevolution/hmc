@extends('layouts.admin.index')

@section('content')
    <!--begin::Content-->






    <div class="content d-flex flex-column flex-column-fluid dashboard-admin" id="kt_content">
        <!--begin::Post-->
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <!--begin::Container-->
            <div id="kt_content_container" class="container-fluid">
                <!--begin::Row-->
                @if(\App\Libraries\General::admin_info()['customerType']=='Super Admin')
                <div class="row gy-5 g-xl-8">
                    <!--begin::Col-->
                    @if(\App\Libraries\General::check_permission('CustomerController','eRead') == 'true')
                    <div class="col-xl-3 col-lg-6 col-md-6 col-12 d-flex">
                        <div class="card pull-up w-100">
                            <div class="card-content">
                                <div class="card-body p-5">
                                    <a href="{{ route('admin.customer') }}" class="dash-info">
                                        <div class="media d-flex">
                                            <div class="align-items-start">
                                                <i class="fas fa-users"></i>
                                            </div>
                                            <div class="media-body text-end">
                                                <h5>Total Customer</h5>
                                                <h3>{{ $customers }}</h3>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(\App\Libraries\General::check_permission('OfficeController','eRead') == 'true')
                    <div class="col-xl-3 col-lg-6 col-md-6 col-12 d-flex">
                        <div class="card pull-up w-100">
                            <div class="card-content">
                                <div class="card-body p-5">
                                    <a href="{{ route('admin.office') }}" class="dash-info">
                                        <div class="media d-flex">
                                            <div class="align-items-start">
                                                <i class="fas fa-laptop-house"></i>
                                            </div>
                                            <div class="media-body text-end">
                                                <h5>Total Office</h5>
                                                <h3>{{ $offices }}</h3>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(\App\Libraries\General::check_permission('DoctorController','eRead') == 'true')
                    <div class="col-xl-3 col-lg-6 col-md-6 col-12 d-flex">
                        <div class="card pull-up w-100">
                            <div class="card-content">
                                <div class="card-body p-5">
                                    <a href="{{ route('admin.doctor') }}" class="dash-info">
                                        <div class="media d-flex">
                                            <div class="align-items-start">
                                                <i class="fas fa-user-md"></i>
                                            </div>
                                            <div class="media-body text-end">
                                                <h5>Total Doctor</h5>
                                                <h3>{{ $doctors }}</h3>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(\App\Libraries\General::check_permission('UserController','eRead') == 'true')
                    <div class="col-xl-3 col-lg-6 col-md-6 col-12 d-flex">
                        <div class="card pull-up w-100">
                            <div class="card-content">
                                <div class="card-body p-5">
                                    <a href="{{ route('admin.user') }}" class="dash-info">
                                        <div class="media d-flex">
                                            <div class="align-items-start">
                                                <i class="fas fa-user"></i>
                                            </div>
                                            <div class="media-body text-end">
                                                <h5>Total User</h5>
                                                <h3>{{ $users }}</h3>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                @endif
                <!--begin::Row-->
                @if(\App\Libraries\General::admin_info()['customerType']=='Super Admin')
                <div class="main-panel" style="padding: 0;">
                    <div class="col-lg-12 mx-auto">
                        <div class="listing-page">
                            <h3 class="text-center">Office Registration Request</h3>
                            <div class="table-data table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr class="fw-bolder text-muted">
                                            <th class="min-w-100px">
                                                <a id="vName" class="sort" data-column="vName" data-order="ASC" href="#">
                                                    <span class="text-muted fw-bold text-muted d-block fs-7">Type</span>
                                                </a>
                                            </th>
                                            <th class="min-w-100px">
                                                <a id="vOfficeName" class="sort" data-column="vOfficeName" data-order="ASC" href="#">
                                                    <span class="text-muted fw-bold text-muted d-block fs-7">Lab Name</span>
                                                </a>
                                            </th>
                                            <th class="min-w-100px">
                                                <a id="vName" class="sort" data-column="vName" data-order="ASC" href="#">
                                                    <span class="text-muted fw-bold text-muted d-block fs-7">Contact Name</span>
                                                </a>
                                            </th>
                                            <th class="min-w-100px"><a id="vTitle" class="sort" data-column="vTitle" data-order="ASC" href="#">
                                                    <span class="text-muted fw-bold text-muted d-block fs-7">Title</span>
                                                </a>
                                            </th>
                                            <th class="min-w-100px">
                                                <a id="eStatus" class="sort" data-column="eStatus" data-order="ASC" href="#">
                                                    <span class="text-muted fw-bold text-muted d-block fs-7">Status</span>
                                                </a>
                                            </th>
                                            <th class="max-w-100px">
                                                <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="table_record">
                                    </tbody>
                                </table>
                                <div class="text-center loaderimg">
                                    <div class="loaderinner">
                                        <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mx-auto">
                        <div class="listing-page">
                            <h3 class="text-center">Lab Registration Request</h3>
                            <div class="table-data table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr class="fw-bolder text-muted">
                                            <th class="min-w-100px">
                                                <a id="vName" class="sort" data-column="vName" data-order="ASC" href="#">
                                                    <span class="text-muted fw-bold text-muted d-block fs-7">Type</span>
                                                </a>
                                            </th>
                                            <th class="min-w-100px">
                                                <a id="vOfficeName" class="sort" data-column="vOfficeName" data-order="ASC" href="#">
                                                    <span class="text-muted fw-bold text-muted d-block fs-7">Lab Name</span>
                                                </a>
                                            </th>
                                            <th class="min-w-100px">
                                                <a id="vName" class="sort" data-column="vName" data-order="ASC" href="#">
                                                    <span class="text-muted fw-bold text-muted d-block fs-7">Contact Name</span>
                                                </a>
                                            </th>
                                            <th class="min-w-100px"><a id="vTitle" class="sort" data-column="vTitle" data-order="ASC" href="#">
                                                    <span class="text-muted fw-bold text-muted d-block fs-7">Title</span>
                                                </a>
                                            </th>
                                            <th class="min-w-100px">
                                                <a id="eStatus" class="sort" data-column="eStatus" data-order="ASC" href="#">
                                                    <span class="text-muted fw-bold text-muted d-block fs-7">Status</span>
                                                </a>
                                            </th>
                                            <th class="max-w-100px">
                                                <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="table_record_lab">
                                    </tbody>
                                </table>
                                <div class="text-center loaderimg">
                                    <div class="loaderinner">
                                        <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader_lab" width="250px" height="auto" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if(\App\Libraries\General::admin_info()['customerType']=='Office Admin')
                <form action="{{route('admin.dashboard.office_action')}}" method="post" class="enter_event">
                    @csrf
                <div class="col-lg-6 main-panel">
                    <div class="row">
                        <div class="col-md-4">
                            <h5 class="pt-3">Labs</h5>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" id="LabSearch" type="text" placeholder="Search labs..">
                        </div>
                    </div>
                    <div class="listing-page mt-4">
                      <div class="table-data table-responsive" id="Request_list_office">
                        <table class="table">
                          <thead>
                            <tr>
                                <th>#</th>
                                <th class="min-w-100px">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Lab Name</span>
                                </th>
                            
                                <th class="min-w-100px">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Connected</span>
                                </th>
                                
                            </tr>
                          </thead>
                          <tbody id="labTable">
                            @php
                            $i=1;
                            @endphp
                            @foreach($Labs as $key => $lab_value)
                              <tr>
                                <td style="color: #0067ad">{{$i}}</td>
                                  <td><a href="#" class="ShowLabOfficeDetail" data-type ='office' id="{{$lab_value->iCustomerId}}">{{$lab_value->vOfficeName}}</a></td>
                                  <td>
                                    @if(in_array($lab_value->iCustomerId,$LabOffice_labids))
                                      @foreach($LabOffice as $key => $LabOffice_value)
                                        @if($LabOffice_value->iLabId == $lab_value->iCustomerId && $LabOffice_value->eStatus == "Active")
                                        <label class="connected-check">
                                            Connected <i class="far fa-check ps-1"></i>
                                        </label>
                                        @elseif($LabOffice_value->iLabId == $lab_value->iCustomerId && $LabOffice_value->eStatus == "Pending")
                                          <label class="connected-check">
                                            Pending
                                        </label>
                                          <a href="#" data-name="Resend"  id="{{$lab_value->iCustomerId}}" class="connected-resend-btn action_btn_office">
                                            Resend Request
                                          </a>
                                        @elseif($LabOffice_value->iLabId == $lab_value->iCustomerId && $LabOffice_value->eStatus == "Inactive")
                                          Connection Rejected
                                        @elseif($LabOffice_value->iLabId == $lab_value->iCustomerId && $LabOffice_value->eStatus == "Hold")
                                        <label class="connected-check">
                                            Pending
                                        </label>
                                          <a href="#" data-name="Resend"  id="{{$lab_value->iCustomerId}}" class="connected-resend-btn action_btn_office">
                                            Resend Request
                                          </a>
                                        @endif
                                      @endforeach
                                    @else
                                      {{-- <input id="new_lab_{{$lab_value->iCustomerId}}" type="checkbox" name="NewLab[]" value="{{$lab_value->iCustomerId}}">
                                      <label for="new_lab_{{$lab_value->iCustomerId}}">Send Connection Request</label> --}}
                                      <a href="#" data-name="Pending"  id="{{$lab_value->iCustomerId}}" class="connected-reconnect-btn action_btn_office">
                                        Send Request
                                      </a>
                                    @endif
                                  </td>
                              </tr>
                              @php
                              $i++;
                              @endphp
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                    {{-- <div class="col-12 align-self-end d-inline-block">
                        <button type="submit" id="submit" class="btn submit-btn me-2">Submit</button>
                    </div> --}}
                </div>
                </form>
                @endif
                @if(\App\Libraries\General::admin_info()['customerType']=='Lab Admin')
                <form action="{{route('admin.dashboard.lab_action')}}" method="POST" class="enter_event">
                    @csrf
                <div class="col-lg-6 main-panel" >
                    <div class="row">
                        <div class="col-md-4">
                            <h5 class="pt-3">Office Connection Request</h5>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" id="OfficeSearch" type="text" placeholder="Search office..">
                        </div>
                    </div>
                    <div class="listing-page mt-4" id="Request_list">
                      <div class="table-data table-responsive">
                        <table class="table" >
                          <thead>
                                <th>#</th>
                                <th class="min-w-100px">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Office Name</span>
                                </th>
                                <th class="min-w-100px">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Connection</span>
                                </th>
                          </thead>
                          <tbody id="officeTable">
                            @php
                            $i=1;
                            @endphp
                            @foreach($Office_request as $key => $Office_value)
                              <tr>
                                <td style="color: #0067ad">{{$i}}</td>
                                  <td><a href="#" class="ShowLabOfficeDetail" data-type ='office' id="{{$Office_value->iOfficeId}}">{{$Office_value->vOfficeName}}</a></td>
                                  <td>
                                    @if($Office_value->eStatus == "Active")
          
                                    <label class="connected-check">
                                        Connected <i class="far fa-check ps-1"></i>
                                    </label>
                                    <a href="#" id="{{$Office_value->iLabOfficeId}}" data-name="Hold" class="connected-btn hold-btn action_btn">
                                        Hold
                                    </a>
          
                                    @elseif($Office_value->eStatus == "Pending")
          
                                      <a href="#" id="{{$Office_value->iLabOfficeId}}" data-name="Hold" class="connected-btn hold-btn action_btn">
                                        Hold
                                      </a>
        
                                      <a href="#" data-name="Active"  id="{{$Office_value->iLabOfficeId}}" class="connected-reconnect-btn action_btn">
                                        Accept
                                      </a>
                                      
                                    @elseif($Office_value->eStatus == "Inactive")
                                      Rejected
                                    @elseif($Office_value->eStatus == "Hold")
                                    <label class="connected-check">
                                        On Hold
                                    </label>&nbsp;
                                    <a href="#" data-name="Active"  id="{{$Office_value->iLabOfficeId}}" class="connected-reconnect-btn action_btn">
                                    Reconnect
                                    </a>
                                    @endif
                                  </td>

                              </tr>
                              @php
                              $i++;
                              @endphp
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col-12 align-self-end d-inline-block">
                        {{-- <button type="submit" id="submit" class="btn submit-btn me-2">Submit</button> --}}
                    </div>
                </div>
                
                </form>
                @endif
                <!--end::Header-->
            </div>
            <!--end::Modal body-->
        </div>
    </div>
    </div>
    <!--end::Modal - New Product-->
    <!--end::Modals-->
    </div>
    <!--end::Container-->
    </div>
    <!--end::Post-->
</div>
<div class="modal fade  show" id="CustomerDataModel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="CustomerDataModelLabel" bis_skin_checked="1" aria-modal="true" role="dialog" style="display: none;">
    <div class="modal-dialog modal-xl" bis_skin_checked="1">
       <div class="modal-content mt-20" bis_skin_checked="1">
            <div class="modal-header" bis_skin_checked="1">
                <h5 class="modal-title" id="CustomerDataModelLabel">Customer Details</h5>
                <button type="button" class="btn-close close-model" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
             <div class="modal-body" id="CustomerDataResponse" bis_skin_checked="1">
                
             </div>
             <div class="modal-footer justify-content-between" bis_skin_checked="1">
                <button type="button" class="close-model btn back-btn red-btn" data-bs-dismiss="modal">Close</button>
             </div>
       </div>
    </div>
</div>

  
    <!--end::Content-->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $(document).ready(function(){
                $("#OfficeSearch").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#officeTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
                $("#LabSearch").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#labTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });
            // notification_success('Log in done');

            $.ajax({
                url: "{{route('admin.dashboard.ajaxPendingListing')}}",
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}",
                    'iCustomerTypeId':'6'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            $.ajax({
                url: "{{route('admin.dashboard.ajaxPendingListing')}}",
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}",
                    'iCustomerTypeId':'1'
                },
                success: function(response) {
                    $("#table_record_lab").html(response);
                    $("#ajax-loader_lab").hide();
                }
            });
        });

        $("#selectall").click(function() {
            if (this.checked) {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', true);
                });
            } else {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', false);
                });
            }
        });
        $(".action_btn").click(function() {
            let action = $(this).attr("data-name");
            let iLabOfficeId = $(this).attr("id");
            $.ajax({
                url: "{{route('admin.dashboard.lab_action')}}",
                type: "post",
                data: {
                    "_token": "{{ csrf_token() }}",
                    action :action,
                    iLabOfficeId : iLabOfficeId
                },
                success: function(response) {
                    if(response == 'success')
                    {
                        // $("#Request_list").load(location.href + " #Request_list");
                        notification_success("Request update successfully.");
                        setTimeout(function() {
                            location.reload(true);
                        }, 500);
                    }
                    else
                    {
                        notification_error("Something Went Wrong");
                    }
                }
            });
        });
        $(".action_btn_office").click(function() {
            let action = $(this).attr("data-name");
            let iLabOfficeId = $(this).attr("id");
            $.ajax({
                url: "{{route('admin.dashboard.office_action')}}",
                type: "post",
                data: {
                    "_token": "{{ csrf_token() }}",
                    action :action,
                    iLabOfficeId : iLabOfficeId
                },
                success: function(response) {
                    if(response == 'success')
                    {
                        // $("#Request_list_office").load(location.href + " #Request_list_office");
                        notification_success("Request update successfully.")
                        setTimeout(function() {
                            location.reload(true);
                        }, 500);
                    }
                    else
                    {
                        notification_error("Something Went Wrong");
                    }
                }
            });
        });

       $('.ShowLabOfficeDetail').click(function(){
            $('#CustomerDataModel').show();
            $('#CustomerDataModel').addClass('show');
            let type = $(this).attr("data-type");
            let id = $(this).attr("id");
            $.ajax({
                url: "{{route('admin.dashboard.show_lab_office_details')}}",
                type: "post",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id :id,
                    type : type
                },
                success: function(response) {
                   $('#CustomerDataResponse').html(response);
                }
            });
       });
       $('.close-model').click(function(){
            $('#CustomerDataModel').hide();
            $('#CustomerDataModel').removeClass('show');
       });

       $(document).on('change', '#page_limit', function() {
            limit_page = this.value;
            $("#table_record").html('');
            $("#ajax-loader").show();
            url = "{{ route('admin.dashboard.ajaxPendingListing') }}";

            setTimeout(function() {
                $.ajax({
                    url: url,
                    type: "get",
                    data: {
                        'iCustomerTypeId':'6',
                        limit_page: limit_page
                    },
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
                // hideLoader();
            }, 500);
        });
       $(document).on('change', '#page_limit_lab', function() {
    
            limit_page = this.value;
            $("#table_record_lab").html('');
            $("#ajax-loader_lab").show();
            url = "{{ route('admin.dashboard.ajaxPendingListing') }}";

            setTimeout(function() {
                $.ajax({
                    url: url,
                    type: "get",
                    data: {
                        'iCustomerTypeId':'1',
                        limit_page: limit_page
                    },
                    success: function(response) {
                        $("#table_record_lab").html(response);
                        $("#ajax-loader_lab").hide();
                    }
                });
                // hideLoader();
            }, 500);
        });

        $(document).on('click', '.ajax_page', function() {
                    pages = $(this).data("pages");
                    var iDepartmentId = $("#iDepartmentId").val();
                    $("#table_record").html('');
                    $("#ajax-loader").show();

                    $.ajax({
                        url: "{{ route('admin.dashboard.ajaxPendingListing') }}",
                        type: "get",
                        data: {
                            'iCustomerTypeId':'6',
                            pages: pages,
                            iDepartmentId: iDepartmentId,
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                        }
                    });
                }); 
        $(document).on('click', '.ajax_page_lab', function() {
                    pages = $(this).data("pages");
                    var iDepartmentId = $("#iDepartmentId").val();
                    $("#table_record_lab").html('');
                    $("#ajax-loader_lab").show();

                    $.ajax({
                        url: "{{ route('admin.dashboard.ajaxPendingListing') }}",
                        type: "get",
                        data: {
                            'iCustomerTypeId':'1',
                            pages: pages,
                            iDepartmentId: iDepartmentId,
                        },
                        success: function(response) {
                            $("#table_record_lab").html(response);
                            $("#ajax-loader_lab").hide();
                        }
                    });
                }); 

                $('.enter_event').on('keyup keypress', function(e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) { 
                        e.preventDefault();
                        return false;
                    }
                });

    </script>
@endsection
