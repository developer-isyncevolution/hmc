@php
	
@endphp
@if(count($data) > 0)
@foreach($data as $key => $value)
<tr>
	
	<td>{{$value->vTypeName}}</td>
	<td>{{$value->vOfficeName}}</td>
	<td>
		@if(isset($value->vFirstName) && !empty($value->vFirstName) && isset($value->vLastName) && !empty($value->vLastName))
			{{$value->vFirstName.' '.$value->vLastName}}
		@else
			{{$value->vName}}
		@endif
	</td>
	<td>{{$value->vTitle}}</td>

	<td>
		<span class="badge badge-light-danger">{{$value->eStatus}}</span>
	</td>

	<td>
		<div class="d-inline-flex align-items-center">
			
			<a href="{{route('admin.approveCustomer',$value->iCustomerId)}}" class="list-label-btn2 me-4 act_btn">Approve</a>
			<a href="{{route('admin.viewCustomerRequest',$value->iCustomerId)}}" class="list-label-btn2 list_permission me-2 act_btn">View</a>
		</div>
	</td>

</tr>
@endforeach
<tr>
	
	<td class="border-0" colspan="5">
		<div class="d-flex">
			{!! $paging !!}
		</div>
	</td>
	{{-- <td colspan="5" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
	<td colspan="2" class="text-end border-left-0">
		{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}} --}}
		@if(isset($data[0]->iCustomerTypeId) && $data[0]->iCustomerTypeId ==6)
		<select class="w-100px show-drop" id="page_limit">
		@else
		<select class="w-100px show-drop" id="page_limit_lab">
		@endif
			<option @if ((isset(Request()->limit_page) && Request()->limit_page == 10) || (isset($limit) && $limit ==10))
				selected
				@endif value="10">10</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 20) || (isset($limit) && $limit ==20))
				selected
				@endif value="20">20</option>
				<option  @if ((isset(Request()->limit_page) && Request()->limit_page == 50) || (isset($limit) && $limit ==50) )
					selected 
				@endif value="50">50</option>
				<option @if ((isset(Request()->limit_page) && Request()->limit_page == 100) || (isset($limit) && $limit ==100))
					selected
				@endif value="100">100</option>
		</select>
	</td>
</tr>
@else
<tr class="text-center">
	<td colspan="15">No Record Found</td>
</tr>
@endif