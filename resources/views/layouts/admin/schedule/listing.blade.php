@extends('layouts.admin.index')
@section('content')
    <style>
        .schedule-model select {
            display: block;
            width: 100%;
            padding: 0.75rem 1rem;
            font-size: 1.1rem;
            font-weight: 500;
            line-height: 1.5;
            color: #181c32;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #e4e6ef;
            appearance: none;
            border-radius: 0.475rem;
            box-shadow: inset 0 1px 2px rgb(0 0 0 / 8%);
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
            width: 100%;
            appearance: auto;
            color: #181c32;
            border-radius: 5px;
            transition: color 0.2s ease, background-color 0.2s ease;
            padding: 5px;
        }

        .schedule-model select:focus {
            outline: none;
        }

        .schedule-model hr {
            background-color: #e4e6ef;
            border: 1px solid #e4e6ef;
        }

        .schedule-model {
            text-align: center;
        }

    </style>




    <div class="main-panel">

        <div class="page-title text-center">
            <h3>
                Lab Schedule
            </h3>
        </div>
        <div class="col-lg-12 mx-auto">

            <div class="card mb-5 mb-xl-4">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-mid">
                                <ul class="d-flex list-unstyled">
                                    @if(\App\Libraries\General::check_permission('','eCreate') == true)
                                    <li>
                                        <a href="{{ route('admin.schedule.create') }}" class="btn create_permission add-btn me-2">
                                            <i class="fa fa-plus"></i> Add
                                        </a>
                                    </li>
                                    @endif
                                    <li>
                                        <a id="days" value="days" class="btn add-btn me-2">Days</a>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mx-auto">
                        <div class="custome-serce-box">
                            <input type="text" class="form-control" id="keyword" name="search" placeholder="Search">
                            <span class="search-btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div>
                <div class="card-header">
                    <ul class="d-flex justify-content-start my-2 list-unstyled flex-wrap">
                        <li class="">
                            <a href="" class="active add-btn btn mx-2">
                                Lab Schedule
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ route('admin.calendar') }}" class="add-btn btn mx-2">
                                Calendar
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="listing-page">
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="w-25px" data-orderable="false">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input id="selectall" type="checkbox" name="selectall" class="form-check-input">
                                        <label for="selectall">&nbsp;</label>
                                    </div>
                                </th>
                                <th scope="col"><a id="iCustomerId" class="sort" data-column="iCustomerId"
                                        data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7">Customer Name </span> </a>
                                </th>
                                <th scope="col"><a id="dtHolidays" class="sort" data-column="dtHolidays"
                                        data-order="ASC" href="#"> <span
                                            class="text-muted fw-bold text-muted d-block fs-7">Holidays </span> </a></th>
                                <th scope="col"><a id="eWeek" class="sort" data-column="eWeek" data-order="ASC"
                                        href="#"> <span class="text-muted fw-bold text-muted d-block fs-7"> Day </span> </a>
                                </th>
                                <th scope="col"><a id="tiOpen" class="sort" data-column="tiOpen" data-order="ASC"
                                        href="#"> <span class="text-muted fw-bold text-muted d-block fs-7"> Open Form
                                        </span> </a></th>
                                <th scope="col"><a id="tiClose" class="sort" data-column="tiClose"
                                        data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7">
                                            Close At </span> </a></th>
                                <th scope="col"><a id="vTitle" class="sort" data-column="vTitle" data-order="ASC"
                                        href="#"> <span class="text-muted fw-bold text-muted d-block fs-7"> Title </span>
                                    </a></th>
                                <th scope="col"><a id="eStatus" class="sort" data-column="eStatus"
                                        data-order="ASC" href="#"> <span class="text-muted fw-bold text-muted d-block fs-7">
                                            Status </span> </a></th>
                                <th scope="col"><span class="text-muted fw-bold text-muted d-block fs-7"> Action </span>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader" width="250px"
                                height="auto" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="days_model" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog schedule-model modal-dialog-scrollable modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Days</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ route('admin.schedule.store_new') }}" id="frm" class="g-5 add-product mt-0"
                                method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            @php
                                                $week = $data[0]->eWeek;
                                            @endphp
                                            <div class="col-lg-4">
                                                <label>Sunday</label>
                                                <select id="SunStatus" name="eSunStatus">
                                                    <option value="OPEN"
                                                        @if (isset($data)) @if ($week == 'Sunday' && $data[0]->eStatus == 'OPEN') selected @endif
                                                        @endif>OPEN</option>
                                                    <option value="CLOSED"
                                                        @if (isset($data)) @if ($week == 'Sunday' && $data[0]->eStatus == 'CLOSED') selected @endif
                                                        @endif>CLOSED</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <div id="div_sunday_from">
                                                    <label>FROM HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiOpen" name="tSuniOpen"
                                                        placeholder="FROM HOUR"
                                                        value="@if (old('tiOpen') != '') {{ old('tiOpen') }}@elseif(isset($data[0]->tiOpen)){{ $data[0]->tiOpen }}@else{{ old('tiOpen') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiOpen_error">
                                                        Please enter From Hour</div>
                                                </div>
                                            </div>


                                            <div class="col-lg-4">
                                                <div id="div_sunday_to">
                                                    <label>TO HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiClose" name="tSuniClose"
                                                        placeholder="FROM HOUR"
                                                        value="@if (old('tiClose') != '') {{ old('tiClose') }}@elseif(isset($data[0]->tiClose)){{ $data[0]->tiClose }}@else{{ old('tiClose') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiClose_error">
                                                        Please enter To Hour</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label>Monday</label>
                                                @php
                                                    $week = $data[1]->eWeek;
                                                @endphp
                                                <select id="MonStatus" name="eMonStatus">
                                                    <option value="OPEN"
                                                        @if (isset($data)) @if ($week == 'Monday' && $data[1]->eStatus == 'OPEN')selected @endif
                                                        @endif>OPEN</option>
                                                    <option value="CLOSED"
                                                        @if (isset($data)) @if ($week == 'Monday' && $data[1]->eStatus == 'CLOSED')selected @endif
                                                        @endif>CLOSED</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-4">
                                                <div id="div_monday_from">
                                                    <label>FROM HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiOpen" name="tMoniOpen"
                                                        placeholder="FROM HOUR"
                                                        value="@if (old('tiOpen') != '') {{ old('tiOpen') }}@elseif(isset($data[1]->tiOpen)){{ $data[1]->tiOpen }}@else{{ old('tiOpen') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiOpen_error">
                                                        Please enter From Hour</div>
                                                </div>
                                            </div>


                                            <div class="col-lg-4">
                                                <div id="div_monday_to">
                                                    <label>TO HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiClose" name="tMoniClose"
                                                        placeholder="FROM HOUR"
                                                        value="@if (old('tiClose') != '') {{ old('tiClose') }}@elseif(isset($data[1]->tiClose)){{ $data[1]->tiClose }}@else{{ old('tiClose') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiClose_error">
                                                        Please enter To Hour</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label>Tuesday</label>
                                                @php
                                                    $week = $data[2]->eWeek;
                                                @endphp
                                                <select id="TueStatus" name="eTueStatus">
                                                    <option value="OPEN"
                                                        @if (isset($data)) @if ($week == 'Tuesday' && $data[2]->eStatus == 'OPEN')selected @endif
                                                        @endif>OPEN</option>
                                                    <option value="CLOSED"
                                                        @if (isset($data)) @if ($week == 'Tuesday' && $data[2]->eStatus == 'CLOSED')selected @endif
                                                        @endif>CLOSED</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-4">
                                                <div id="div_tueday_from">
                                                    <label>FROM HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tTueiOpen"
                                                        name="tTueiOpen" placeholder="FROM HOUR"
                                                        value="@if (old('tiOpen') != '') {{ old('tiOpen') }}@elseif(isset($data[2]->tiOpen)){{ $data[2]->tiOpen }}@else{{ old('tiOpen') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiOpen_error">
                                                        Please enter From Hour</div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <div id="div_tueday_to">
                                                    <label>TO HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiClose" name="tTueiClose"
                                                        placeholder="FROM HOUR"
                                                        value="@if (old('tiClose') != '') {{ old('tiClose') }}@elseif(isset($data[2]->tiClose)){{ $data[2]->tiClose }}@else{{ old('tiClose') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiClose_error">
                                                        Please enter To Hour</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-lg-12">
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label>Wednesday</label>
                                                @php
                                                    $week = $data[3]->eWeek;
                                                @endphp
                                                <select id="WedStatus" name="eWedStatus">
                                                    <option value="OPEN"
                                                        @if (isset($data)) @if ($week == 'Wednesday' && $data[3]->eStatus == 'OPEN')selected @endif
                                                        @endif>OPEN</option>
                                                    <option value="CLOSED"
                                                        @if (isset($data)) @if ($week == 'Wednesday' && $data[3]->eStatus == 'CLOSED')selected @endif
                                                        @endif>CLOSED</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-4">
                                                <div id="div_wednesday_from">
                                                    <label>FROM HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiOpen" name="tWediOpen"
                                                        placeholder="FROM HOUR"
                                                        value="@if (old('tiOpen') != '') {{ old('tiOpen') }}@elseif(isset($data[3]->tiOpen)){{ $data[3]->tiOpen }}@else{{ old('tiOpen') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiOpen_error">
                                                        Please enter From Hour</div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div id="div_wednesday_to">
                                                    <label>TO HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiClose" name="tWediClose"
                                                        placeholder="FROM HOUR"
                                                        value="@if (old('tiClose') != '') {{ old('tiClose') }}@elseif(isset($data[3]->tiClose)){{ $data[3]->tiClose }}@else{{ old('tiClose') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiClose_error">
                                                        Please enter To Hour</div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="col-lg-12">
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label>Thursday</label>
                                                @php
                                                    $week = $data[4]->eWeek;
                                                @endphp
                                                <select id="ThurStatus" name="eThuStatus">
                                                    <option value="OPEN"
                                                        @if (isset($data)) @if ($week == 'Thu/ko+654rsday' && $data[4]->eStatus == 'OPEN') selected @endif
                                                        @endif>OPEN</option>
                                                    <option value="CLOSED"
                                                        @if (isset($data)) @if ($week == 'Thursday' && $data[4]->eStatus == 'CLOSED')selected @endif
                                                        @endif>CLOSED</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-4">
                                                <div id="div_thursday_from">
                                                    <label>FROM HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiOpen" name="tThuiOpen"
                                                        placeholder="FROM HOUR"
                                                        value="@if (old('tiOpen') != '') {{ old('tiOpen') }}@elseif(isset($data[4]->tiOpen)){{ $data[4]->tiOpen }}@else{{ old('tiOpen') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiOpen_error">
                                                        Please enter From Hour</div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <div id="div_thursday_to">
                                                    <label>TO HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiClose" name="tThuiClose"
                                                        placeholder="FROM HOUR"
                                                        value="@if (old('tiClose') != '') {{ old('tiClose') }}@elseif(isset($data[4]->tiClose)){{ $data[4]->tiClose }}@else{{ old('tiClose') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiClose_error">
                                                        Please enter To Hour</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label>Friday</label>
                                                @php
                                                    $week = $data[5]->eWeek;
                                                @endphp
                                                <select id="FriStatus" name="eFriStatus">
                                                    <option value="OPEN"
                                                        @if (isset($data)) @if ($week == 'Friday' && $data[5]->eStatus == 'OPEN')selected @endif
                                                        @endif>OPEN</option>
                                                    <option value="CLOSED"
                                                        @if (isset($data)) @if ($week == 'Friday' && $data[5]->eStatus == 'CLOSED')selected @endif
                                                        @endif>CLOSED</option>
                                                </select>
                                            </div>


                                            <div class="col-lg-4">
                                                <div id="div_friday_from">
                                                    <label>FROM HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiOpen" name="tFriiOpen"
                                                        placeholder="FROM HOUR"
                                                        value="@if (old('tiOpen') != '') {{ old('tiOpen') }}@elseif(isset($data[5]->tiOpen)){{ $data[5]->tiOpen }}@else{{ old('tiOpen') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiOpen_error">
                                                        Please enter From Hour</div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <div id="div_friday_to">
                                                    <label>TO HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiClose"
                                                        name="tFriiClose" placeholder="FROM HOUR"
                                                        value="@if (old('tiClose') != '') {{ old('tiClose') }}@elseif(isset($data[5]->tiClose)){{ $data[5]->tiClose }}@else{{ old('tiClose') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiClose_error">
                                                        Please enter To Hour</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label>Saturday</label>
                                                @php
                                                    $week = $data[6]->eWeek;
                                                @endphp
                                                <select id="SatStatus" name="eSatStatus">
                                                    <option value="OPEN"
                                                        @if (isset($data)) @if ($week == 'Saturday' && $data[6]->eStatus == 'OPEN')selected @endif
                                                        @endif>OPEN</option>
                                                    <option value="CLOSED"
                                                        @if (isset($data)) @if ($week == 'Saturday' && $data[6]->eStatus == 'CLOSED')selected @endif
                                                        @endif>CLOSED</option>
                                                </select>
                                            </div>


                                            <div class="col-lg-4">
                                                <div id="div_saturday_from">
                                                    <label>FROM HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiOpen" name="tSatiOpen"
                                                        placeholder="FROM HOUR"
                                                        value="@if (old('tiOpen') != '') {{ old('tiOpen') }}@elseif(isset($data[6]->tiOpen)){{ $data[6]->tiOpen }}@else{{ old('tiOpen') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiOpen_error">
                                                        Please enter From Hour</div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div id="div_saturday_to">
                                                    <label>TO HOUR</label><span> *</span>
                                                    <input type="time" class="form-control" id="tiClose"
                                                        name="tSatiClose" placeholder="FROM HOUR"
                                                        value="@if (old('tiClose') != '') {{ old('tiClose') }}@elseif(isset($data[6]->tiClose)){{ $data[6]->tiClose }}@else{{ old('tiClose') }} @endif">
                                                    <div class="text-danger" style="display: none;" id="tiClose_error">
                                                        Please enter To Hour</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>





                            </form>
                        </div>
                        <div class="modal-footer" style="justify-content: flex-start;">
                            <a type="a" class="btn btn-secondary" data-bs-dismiss="modal">Close</a>
                            <a type="submit" id="submit" class="btn submit-btn me-2">Add</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function() {
            var week = "{{ $data[0]->eWeek }}";
            var status = "{{ $data[0]->eStatus }}";

            if (week == 'Sunday' && status == 'CLOSED') {
                $("#div_sunday_from").hide();
                $("#div_sunday_to").hide();
            } else {
                $("#div_sunday_from").show();
                $("#div_sunday_to").show();
            }

            var week = "{{ $data[1]->eWeek }}";
            var status = "{{ $data[1]->eStatus }}";

            if (week == 'Monday' && status == 'CLOSED') {
                $("#div_monday_from").hide();
                $("#div_monday_to").hide();
            } else {
                $("#div_monday_from").show();
                $("#div_monday_to").show();
            }
            var week = "{{ $data[2]->eWeek }}";
            var status = "{{ $data[2]->eStatus }}";

            if (week == 'Tuesday' && status == 'CLOSED') {
                $("#div_tuesday_from").hide();
                $("#div_tuesday_to").hide();
            } else {
                $("#div_tuesday_from").show();
                $("#div_tuesday_to").show();
            }
            var week = "{{ $data[3]->eWeek }}";
            var status = "{{ $data[3]->eStatus }}";

            if (week == 'Wednesday' && status == 'CLOSED') {
                $("#div_wednesday_from").hide();
                $("#div_wednesday_to").hide();
            } else {
                $("#div_wednesday_from").show();
                $("#div_wednesday_to").show();
            }
            var week = "{{ $data[4]->eWeek }}";
            var status = "{{ $data[4]->eStatus }}";
            if (week == 'Thursday' && status == 'CLOSED') {
                $("#div_thursday_from").hide();
                $("#div_thursday_to").hide();
            } else {
                $("#div_thursday_from").show();
                $("#div_thursday_to").show();
            }
            var week = "{{ $data[5]->eWeek }}";
            var status = "{{ $data[5]->eStatus }}";
            if (week == 'Friday' && status == 'CLOSED') {
                $("#div_friday_from").hide();
                $("#div_friday_to").hide();
            } else {
                $("#div_friday_from").show();
                $("#div_friday_to").show();
            }
            var week = "{{ $data[6]->eWeek }}";
            var status = "{{ $data[6]->eStatus }}";
            if (week == 'Saturday' && status == 'CLOSED') {
                $("#div_saturday_from").hide();
                $("#div_saturday_to").hide();
            } else {
                $("#div_saturday_from").show();
                $("#div_saturday_to").show();
            }
        });
        $(document).on('click', '#days', function() {
            $("#days_model").modal("show");

        });
        $(document).on('change', '#SunStatus', function() {
            var SunStatus = $("#SunStatus").val();

            if (SunStatus == 'OPEN') {
                $("#div_sunday_from").show();
                $("#div_sunday_to").show();
            } else {
                $("#div_sunday_from").hide();
                $("#div_sunday_to").hide();
            }

        });
        $(document).on('change', '#MonStatus', function() {
            var MonStatus = $("#MonStatus").val();

            if (MonStatus == 'OPEN') {
                $("#div_monday_from").show();
                $("#div_monday_to").show();
            } else {
                $("#div_monday_from").hide();
                $("#div_monday_to").hide();
            }

        });
        $(document).on('change', '#TueStatus', function() {
            var TueStatus = $("#TueStatus").val();

            if (TueStatus == 'OPEN') {
                $("#div_tueday_from").show();
                $("#div_tueday_to").show();
            } else {
                $("#div_tueday_from").hide();
                $("#div_tueday_to").hide();
            }

        });
        $(document).on('change', '#WedStatus', function() {
            var WedStatus = $("#WedStatus").val();

            if (WedStatus == 'OPEN') {
                $("#div_wednesday_from").show();
                $("#div_wednesday_to").show();
            } else {
                $("#div_wednesday_from").hide();
                $("#div_wednesday_to").hide();
            }

        });
        $(document).on('change', '#ThurStatus', function() {
            var ThurStatus = $("#ThurStatus").val();

            if (ThurStatus == 'OPEN') {
                $("#div_thursday_from").show();
                $("#div_thursday_to").show();
            } else {
                $("#div_thursday_from").hide();
                $("#div_thursday_to").hide();
            }

        });
        $(document).on('change', '#ThurStatus', function() {
            var ThurStatus = $("#ThurStatus").val();

            if (ThurStatus == 'OPEN') {
                $("#div_thursday_from").show();
                $("#div_thursday_to").show();
            } else {
                $("#div_thursday_from").hide();
                $("#div_thursday_to").hide();
            }

        });
        $(document).on('change', '#FriStatus', function() {
            var FriStatus = $("#FriStatus").val();

            if (FriStatus == 'OPEN') {
                $("#div_friday_from").show();
                $("#div_friday_to").show();
            } else {
                $("#div_friday_from").hide();
                $("#div_friday_to").hide();
            }

        });
        $(document).on('change', '#SatStatus', function() {
            var SatStatus = $("#SatStatus").val();

            if (SatStatus == 'OPEN') {
                $("#div_saturday_from").show();
                $("#div_saturday_to").show();
            } else {
                $("#div_saturday_from").hide();
                $("#div_saturday_to").hide();
            }

        });
        $(document).ready(function() {
            $.ajax({
                url: "{{ route('admin.schedule.ajaxListing') }}",
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $("#selectall").click(function() {
            if (this.checked) {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', true);
                });
            } else {
                $('.checkboxall').each(function() {
                    $(".checkboxall").prop('checked', false);
                });
            }
        });

        $(document).on('click', '#delete_btn', function() {
            var id = [];

            $("input[name='Schedule_ID[]']:checked").each(function() {
                id.push($(this).val());
            });

            var id = id.join(",");

            if (id.length == 0) {
                alert('Please select records.')
            } else {
                swal({
                        title: "Are you sure delete all Schedule ?",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: "{{ route('admin.schedule.ajaxListing') }}",
                                type: "get",
                                data: {
                                    id: id,
                                    action: 'multiple_delete'
                                },
                                success: function(response) {
                                    $("#table_record").html(response);
                                    $("#ajax-loader").hide();
                                    notification_error("Schedule Deleted Successfully");
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                                }
                            });
                        }
                    });
            }
        });

        $("#keyword").keyup(function() {
            var keyword = $("#keyword").val();
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.schedule.ajaxListing') }}",
                type: "get",
                data: {
                    keyword: keyword,
                    action: 'search'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '#delete', function() {
            swal({
                    title: "Are you sure delete this Schedule ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        id = $(this).data("id");

                        $("#ajax-loader").show();

                        $.ajax({
                            url: "{{ route('admin.schedule.ajaxListing') }}",
                            type: "get",
                            data: {
                                id: id,
                                action: 'delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("Schedule Deleted Successfully");
                                setTimeout(function() {
                                    location.reload();
                                }, 1000);
                            }
                        });
                    }
                })
        });
        $(document).on('click', '.sort', function() {
            column = $(this).data("column");
            order = $(this).attr('data-order');


            if (order == "ASC") {
                $(this).attr('data-order', 'DESC');
            } else {
                $(this).attr('data-order', 'ASC');
            }

            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.schedule.ajaxListing') }}",
                type: "get",
                data: {
                    column: column,
                    order,
                    order,
                    action: 'sort'
                },
                success: function(response) {
                    console.log(response);
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });

        $(document).on('click', '.ajax_page', function() {
            pages = $(this).data("pages");

            $("#table_record").html('');
            $("#ajax-loader").show();

            $.ajax({
                url: "{{ route('admin.schedule.ajaxListing') }}",
                type: "get",
                data: {
                    pages: pages
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        });
        $(document).on('change', '#page_limit', function() {
            limit_page = this.value;
            $("#table_record").html('');
            $("#ajax-loader").show();
            url = "{{ route('admin.schedule.ajaxListing') }}";

            setTimeout(function() {
                $.ajax({
                    url: url,
                    type: "get",
                    data: {
                        limit_page: limit_page
                    },
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                    }
                });
                // hideLoader();
            }, 500);
        });

        $(document).on('click', '#submit', function() {
            iScheduleId = $("#id").val();
            iCustomerId = $("#iCustomerId").val();
            vTitle = $("#vTitle").val();
            tiOpen = $("#tiOpen").val();
            tiClose = $("#tiClose").val();
            eWeekMonth = $("#eWeekMonth").val();

            var error = false;
            // if (iCustomerId == 'none') {
            //   $("#iCustomerId_error").show();
            //   error = true;
            // } else {
            //   $("#iCustomerId_error").hide();
            // }
            // if(eWeekMonth.length == 0){
            //   $("#eWeekMonth_error").show();
            //   error = true;
            // } else {
            //   $("#eWeekMonth_error").hide();
            // }
            // if(vTitle.length == 0){
            //   $("#vTitle_error").show();
            //   error = true;
            // } else {
            //   $("#vTitle_error").hide();
            // }
            // if(tiOpen.length == 0){
            //   $("#tiOpen_error").show();
            //   error = true;
            // } else {
            //   $("#tiOpen_error").hide();
            // }
            // if(tiClose.length == 0){
            //   $("#tiClose_error").show();
            //   error = true;
            // } else {
            //   $("#tiClose_error").hide();
            // }
            setTimeout(function() {
                if (error == true) {
                    return false;
                } else {
                    $("#frm").submit();
                    return true;
                }
            }, 1000);

        });
    </script>
@endsection
