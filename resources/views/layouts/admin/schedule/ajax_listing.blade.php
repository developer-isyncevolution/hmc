@if(count($data) > 0)

@foreach($data as $key => $value)

		<tr>
			<td>
				<div class="form-check form-check-sm form-check-custom form-check-solid">
					<input id="Schedule_ID_{{$value->iScheduleId}}" type="checkbox" name="Schedule_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iScheduleId}}">
					<label for="Schedule_ID_{{$value->iScheduleId}}">&nbsp;</label>
				</div>
			</td>
			<td>{{$value->vCustomerName}}</td>

			<td>
					@if(empty($value->eWeek) )

						@php  $dtHolidays = date('Y-m-d', strtotime($value->dtHolidays)); @endphp
				
						{{$dtHolidays}}
					@else 
						--
					@endif				

			</td>
			<td>{{$value->eWeek}}</td>
			<td>
				@if($value->eStatus !='CLOSED')
				@php $opentime = date('h:i A', strtotime($value->tiOpen));
				@endphp
				{{$opentime}}
				@else
				--
				@endif
		</td>
			<td>
				@php $closetime = date('h:i A', strtotime($value->tiClose));
				@endphp
				@if($value->eStatus =='OPEN')	
				{{$closetime}}
				@else
				--
				@endif
			</td>
			<td>@if($value->eStatus =='OPEN')
				Open 
				@else {{$value->vTitle}} @endif</td>
			{{-- <td>{{$value->vTitle}}</td> --}}
			<td>
				<span class="badge badge-light-success">
					{{$value->eStatus}}
				</span>
			</td>
			<td>
				@if($value->eStatus !='OPEN')
				<div class="d-inline-flex align-items-center">
					{{-- @if(\App\Libraries\General::check_permission('','eEdit') == true)
					<a href="{{route('admin.schedule.edit',$value->iScheduleId)}}" class="btn-icon me-4">
						<i class="fad fa-pencil"></i>
					</a>
					@endif --}}
					@if(\App\Libraries\General::check_permission('','eDelete') == true)
						@if(isset($value->dtHolidays) && !empty($value->dtHolidays))
							<a href="javascript:;" id="delete" data-id="{{$value->iScheduleId}}" class="btn-icon delete-icon delete_permission me-4" >
								<i class="fad fa-trash-alt"></i>
							</a>
						@endif
					@endif
					{{-- <a href="{{route('admin.Gradenumber',$value->iScheduleId)}}" class="list-label-btn2 list_permission me-4">
						Grade Number
					</a> --}}
				</div>
				@endif
			</td>
		</tr>
		@endforeach
		<tr>
			<td  class="text-center border-right-0">
				@if(\App\Libraries\General::check_permission('','eDelete') == true)
					<a href="javascript:;" title="Multiple delete" id="delete_btn" class="btn-icon  delete_permission delete-icon">
						<i class="fad fa-trash-alt"></i>
					</a>
				@endif
			</td>
			{{-- <td class="border-0"></td>
			<td colspan="0" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
			<td class="" colspan="6">
				<div class="d-flex justify-content-center">
					{!! $paging !!}
				</div>
			</td>
			<td colspan="2"  class="text-end border-left-0">
				{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}}  --}}
				<select class="w-100px show-drop" id ="page_limit">
					<option @if (isset(Request()->limit_page) && Request()->limit_page == 10)
						selected
						@endif value="10">10</option>
						<option @if (isset(Request()->limit_page) && Request()->limit_page == 20)
						selected
						@endif value="20">20</option>
						<option @if (isset(Request()->limit_page) && Request()->limit_page == 50)
							selected
						@endif value="50">50</option>
						<option @if (isset(Request()->limit_page) && Request()->limit_page == 100)
							selected
						@endif value="100">100</option>
				</select>
			</td>
		</tr>
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif