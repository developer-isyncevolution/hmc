@extends('layouts.admin.index')
@section('content')



<section class="content-header">
	<div class="container-fluid">	
        <div class="main-panel">
        	<div class="col-lg-12">
        		<div class="listing-page">
        			<div class="card">
        				<div class="card-header">
                            <!-- used component -->
                            <div class="col-lg-12" bis_skin_checked="1">
                                <div class="card" bis_skin_checked="1">
                                    <div class="card-mid" bis_skin_checked="1">
                                        <ul class="d-flex list-unstyled">
                                            <li>
                                                <a href="{{route('admin.systemEmail.add')}}" class="btn create_permission add-btn me-2">
                                                    <i class="fa fa-plus"></i> Add
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
        					
        				</div>
        				<!-- /.card-header -->
        				
                        <div class="table-data table-responsive">
                            <table class="table">
                                <thead>
                                    <tr class="fw-bolder text-muted">
                                        {{-- <th class="w-25px" data-orderable="false">
                                            <div class="form-check form-check-sm form-check-custom form-check-solid">
                                                <input class="form-check-input" type="checkbox" value="1" data-kt-check="true"
                                                    data-kt-check-target=".widget-13-check" id="selectall" type="checkbox"
                                                    name="selectall">
                                                <label for="selectall">&nbsp;</label>
                                            </div>
                                        </th> --}}
                                        {{-- <th class="min-w-140px">
                                        <a id="vName" class="sort" data-column="vName" data-order="ASC" href="#">
                                            <span class="text-muted fw-bold text-muted d-block fs-7">Doctor Name</span>
                                        </a>
                                    </th> --}}
                                        <th class="min-w-140px">
                                            <a id="vEmailTitle" class="sort" data-column="vEmailTitle" data-order="ASC"
                                                href="javascript:;">
                                                <span class="text-muted fw-bold text-muted d-block fs-7"> Email Title</span>
                                            </a>
                                        </th>
                                        <th class="min-w-140px">
                                            <a id="vEmailCode" class="sort" data-column="vEmailCode" data-order="ASC"
                                                href="javascript:;">
                                                <span class="text-muted fw-bold text-muted d-block fs-7">Email Code</span>
                                            </a>
                                        </th>
                                    
                                        <th class="min-w-120px">
                                            <a id="eStatus" class="sort" data-column="eStatus" data-order="ASC"
                                                href="javascript:;">
                                                <span class="text-muted fw-bold text-muted d-block fs-7">Status</span>
                                            </a>
                                        </th>
                                        <th class="min-w-100px">
                                            <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="table_record">
                                </tbody>
                            </table>
                            <div class="text-center loaderimg">
                                <div class="loaderinner">
                                    <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader" width="250px"
                                        height="auto" />
                                </div>
                            </div>
                        </div>
                        
        			</div>
        		</div>
        	</div>
        </div>
    </div>
</section>

@endsection

@section('custom-css')
<style></style>
@endsection

@section('custom-js')
<script>
    $(document).ready(function(){
        $("#ajax-loader").show();
        setTimeout(function() {
            $.ajax({
                url: "{{url('admin/system_email/ajax_listing')}}",
                type: "POST",
                headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                data: "",
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        }, 1000);
    });
    $(document).on('click', '#selectall', function(){
        if(this.checked)
        {
            $('.checkboxall').each(function()
            {
                $(".checkboxall").prop('checked', true);
            });
        }
        else
        {
            $('.checkboxall').each(function(){
                $(".checkboxall").prop('checked', false);
            });
        }
    });
    $(document).on('keyup', '.search', function() {
        var keyword         = $(this).val();
        var vEmailTitle     = $('#vEmailTitle').val();
        var vEmailCode      = $('#vEmailCode').val();
        var status_search   = $('#status_search').val();
        var eIsDeleted      = $('#eIsDeleted').val();
        var limit_page      = $('#page_limit').val();
        $("#table_record").hide();
        $("#ajax-loader").show();
        setTimeout(function() {
            $.ajax({
                url: "{{url('admin/system_email/ajax_listing')}}",
                type: "POST",
                data: {
                    keyword: keyword,
                    vEmailTitle: vEmailTitle,
                    vEmailCode:vEmailCode,
                    status_search:status_search,
                    eIsDeleted:eIsDeleted,
                    limit_page:limit_page,
                    action: 'search',
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                    $("#table_record").show();
                }
            });
        }, 1000);
    });
    $(document).on('change', '#status_search', function() {
        var search_status_val = $(this).val();
        var vEmailTitle       = $('#vEmailTitle').val();
        var vEmailCode        = $('#vEmailCode').val();
        var eIsDeleted        = $('#eIsDeleted').val();
        var limit_page        = $('#page_limit').val();
        $("#table_record").hide();
        $("#ajax-loader").show();
        setTimeout(function() {
            $.ajax({
                url: "{{url('admin/system_email/ajax_listing')}}",
                type: "POST",
                data: {
                    eStatus: search_status_val,
                    vEmailTitle: vEmailTitle,
                    vEmailCode:vEmailCode,
                    eIsDeleted : eIsDeleted,
                    limit_page:limit_page,
                    action: 'search_status',
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                    $("#table_record").show();
                }
            });
        }, 1000);
    }); 
    $(document).on('change', '#eIsDeleted', function() {
        var eIsDeleted_val  = $(this).val();
        var vEmailTitle     = $('#vEmailTitle').val();
        var vEmailCode      = $('#vEmailCode').val();
        var eStatus         = $('#status_search').val();
        var limit_page      = $('#page_limit').val();
        $("#table_record").hide();
        $("#ajax-loader").show();
        setTimeout(function() {
            $.ajax({
                url: "{{url('admin/system_email/ajax_listing')}}",
                type: "POST",
                data: {
                    vEmailTitle: vEmailTitle,
                    vEmailCode:vEmailCode,
                    eStatus:eStatus,
                    limit_page:limit_page,
                    eIsDeleted: eIsDeleted_val,
                    action: 'deleted_data',
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                    $("#table_record").show();
                    // $('#eIsDeleted').val('');
                    
                }
            });
        }, 1000);
    });
    $(document).on('click','.delete',function(){
        if (confirm('Are you sure delete this data?')) {
            vUniqueCode = $(this).data("id");
            $("#table_record").hide();
            $("#ajax-loader").show();
            setTimeout(function(){
                $.ajax({
                    url: "{{url('admin/system_email/ajax_listing')}}",
                    type: "POST",
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    data:  {vUniqueCode:vUniqueCode,action:'delete'}, 
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#table_record").show();
                        $("#ajax-loader").hide();
                        notification_error("Email Deleted Successfully");
                    }
                });
            }, 1000);
        }
    });
    $(document).on('change','#status',function(){
        if($("#selectall").is(':checked') == true || $("input[name='vUniqueCode[]']:checked").is(':checked') == true)
        {
            var eStatus         = $("#status").val();
            var vEmailTitle     = $('#vEmailTitle').val();
            var vEmailCode      = $('#vEmailCode').val();
            var eIsDeleted      = $('#eIsDeleted').val();
            var limit_page      = $('#page_limit').val();
            if(eStatus == "delete"){
                if(confirm("Are you sure you want to delete?")){
                    MultipleUpdate();
                }
            }else{
               if(confirm("Are you sure you want to change status?")){
                    MultipleUpdate();
                } 
            }
            function MultipleUpdate() {
                vUniqueCode = [];
                $("input[name='vUniqueCode[]']:checked").each(function() {
                    vUniqueCode.push($(this).val());
                });
                var vUniqueCode = vUniqueCode.join(",");
                $("#table_record").html('');
                $("#ajax-loader").show();
                setTimeout(function(){
                    $.ajax({
                        url: "{{url('admin/system_email/ajax_listing')}}",
                        type: "POST",
                        headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                        data:  {
                            vEmailTitle: vEmailTitle,
                            vEmailCode:vEmailCode,
                            eIsDeleted:eIsDeleted,
                            limit_page:limit_page,
                            vUniqueCode: vUniqueCode,
                            action: 'status',
                            eStatus: eStatus
                        }, 
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                            $("#status").val('');
                            $("#selectall").prop('checked', false);
                            if(eStatus == "delete"){
                                toastr.error('System Email Deleted Successfully.');
                            }
                            else{
                                toastr.success('Data Update Successfully.');
                            }
                        }
                    });
                }, 1000);
            }
        }else{
           alert('Please select records.') 
        }
    });
    $(document).on('change','#page_limit',function(){
        var limit_page      = this.value;
        var eIsDeleted      = $('#eIsDeleted').val();
        var status_search      = $('#status_search').val();
        $("#table_record").html('');
        $("#ajax-loader").show();
        setTimeout(function(){
            $.ajax({
                url: "{{url('admin/system_email/ajax_listing')}}",
                type: "POST",
                headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                data:  {
                    limit_page: limit_page,
                    eIsDeleted:eIsDeleted,
                    status_search:status_search,
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            hideLoader();
            }, 500);
    });
    $(document).on('click','.ajax_page',function(){
        var pages = $(this).data("pages");
        var vEmailTitle   = $('#vEmailTitle').val();
        var vEmailCode   = $('#vEmailCode').val();
        var eIsDeleted = $('#eIsDeleted').val();
        var status_search = $('#status_search').val();
        var limit_page = $('#page_limit').val();
        $("#ajax-loader").show();
        $("#table_record").html('');
        url = "{{url('admin/system_email/ajax_listing')}}";
        setTimeout(function(){
            $.ajax({
                url: url,
                type: "POST",
                headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                data:  {
                    pages: pages,
                    vEmailTitle:vEmailTitle,
                    vEmailCode:vEmailCode,
                    eIsDeleted:eIsDeleted,
                    status_search:status_search,
                    limit_page:limit_page,
                    action:'search'}, 
                success: function(response) {
                    console.log(response);
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
        }, 500);
    });
    $(document).on('click', '.recover', function() {
        if (confirm('Are you sure recover this data?')) {
            vUniqueCode = $(this).data("id");
            $("#table_record").hide();
            $("#ajax-loader").show();
            setTimeout(function() {
                $.ajax({
                    url: "{{url('admin/system_email/ajax_listing')}}",
                    type: "POST",
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    data: {
                        vUniqueCode: vUniqueCode,
                        action: 'recover'
                    },
                    success: function(response) {
                        $("#table_record").html(response);
                        $("#ajax-loader").hide();
                        $("#table_record").show();
                        $('#eIsDeleted').val('');
                        toastr.success('System Email Data recover Successfully.');
                        
                    }
                });
            }, 1000);
        }
    });
</script>
@endsection