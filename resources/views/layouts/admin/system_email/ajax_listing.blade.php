@php
$iRoleId  = Session::get('iRoleId');
@endphp

@if(($data->count()))
@foreach($data as $key => $value)
<tr align="center" class="text-center">
	{{-- <td>
		<div class="form-check form-check-sm form-check-custom form-check-solid">
			<input id="vUniqueCode_{{$value->vUniqueCode}}" type="checkbox" name="vUniqueCode[]" class="form-check-input widget-9-check checkboxall" value="{{$value->vUniqueCode}}">
			<label for="vUniqueCode_{{$value->vUniqueCode}}">&nbsp;</label>
		</div>
    </td> --}}
	<td>
	  @if(!empty($value->vEmailTitle))
      {{$value->vEmailTitle}}
	  @else
	  {{ "N/A" }}
	  @endif
	<td>
	  @if(!empty($value->vEmailCode))
      {{$value->vEmailCode}}
	  @else
	  {{ "N/A" }}
	  @endif
	  </td>
	<td><span class="badge @if($value->eStatus == 'Active') badge-success @else badge-danger @endif">{{$value->eStatus}}</span></td>
	<td>
		
		<a  href="{{url('admin/system_email/edit',$value->vUniqueCode)}}"><i class="fad fa-pencil"></i></a>
		@if($value->eIsDeleted == 'Yes')
		<a class="btn btn-success btn-sm recover"  data-id="{{$value->vUniqueCode}}" href="javascript:;"><i class="fa fa-reply"></i></a>
		@else
		<a class="delete"  data-id="{{$value->vUniqueCode}}" href="javascript:;"><i class="fad fa-trash-alt"></i></a>
		
		@endif
		
	</td>
</tr>
@endforeach
<tr>
    <td colspan="7" align="center">
        <div  class="paginations">
            <?php echo $paging;?>
        </div>
    </td>
</tr>
@else
<tr class="text-center">
	<td colspan="9">No Record Found</td>
</tr>
@endif