@php 
$url = Request::segment(2);
$url1 = Request::segment(3);
$url2 = Request::segment(4);
$admin_info = \App\Libraries\General::admin_info();
$iDepartmentId = $admin_info['iDepartmentId'];
$logo = \App\Libraries\General::get_customer_logo();

$general_logo = \App\Libraries\General::setting_info('Company');
// dd($general_logo);
$logo_main         = $general_logo['COMPANY_LOGO']['vValue'];

$general_info    = \App\Libraries\General::setting_info('Appearance');
@endphp


<div id="kt_aside" class="aside aside-dark aside-hoverable active" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
	<!--begin::Brand-->
	<div class="aside-logo flex-column-auto" id="kt_aside_logo">
		<!--begin::Logo-->
		@if($logo == "LabslipLogo")
			<img alt="Logo" src="{{asset('uploads/logo/'.$logo_main)}}" class="h-50px logo" />
		@else
			<img alt="Logo" src="{{asset('uploads/customer/'.$logo)}}" class="h-50px logo" />
		@endif
		<!--end::Logo-->
		<!--begin::Aside toggler-->
		<div id="kt_aside_toggle" class="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="aside-minimize">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr079.svg-->
			<span class="svg-icon svg-icon-1 rotate-180">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<path opacity="0.5" d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z" fill="black" />
					<path d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z" fill="black" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Aside toggler-->
	</div>
	<!--end::Brand-->
	<!--begin::Aside menu-->
	<div class="aside-menu flex-column-fluid">
		<!--begin::Aside Menu-->
		<div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer" data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
			<!--begin::Menu-->
			<div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true">
				@if(\App\Libraries\General::check_permission('DashboardController','eRead') == 'true')
				<div class="menu-item @if($url == 'dashboard') show @endif">
					<a class="menu-link" href="{{route('admin.dashboard')}}">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect x="2" y="2" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Dashboard</span>
					</a>
				</div>
				@endif

				
				<div class="menu-item">
					<div class="menu-content p-0">
						<div class="separator"></div>
					</div>
				</div>
				@if(\App\Libraries\General::admin_info()['customerType'] == 'Lab Admin')
				 <div data-kt-menu-trigger="click" class="menu-item menu-accordion @if($url == 'banner' ) hover show @endif">
					<span class="menu-link">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm007.svg-->
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<path d="M21 9V11C21 11.6 20.6 12 20 12H14V8H20C20.6 8 21 8.4 21 9ZM10 8H4C3.4 8 3 8.4 3 9V11C3 11.6 3.4 12 4 12H10V8Z" fill="black" />
									<path d="M15 2C13.3 2 12 3.3 12 5V8H15C16.7 8 18 6.7 18 5C18 3.3 16.7 2 15 2Z" fill="black" />
									<path opacity="0.3" d="M9 2C10.7 2 12 3.3 12 5V8H9C7.3 8 6 6.7 6 5C6 3.3 7.3 2 9 2ZM4 12V21C4 21.6 4.4 22 5 22H10V12H4ZM20 12V21C20 21.6 19.6 22 19 22H14V12H20Z" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Banner</span>
						<span class="menu-arrow"></span>
					</span>
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						 <div class="menu-item @if($url == 'banner') show @endif">
							<a class="menu-link" href="{{route('admin.banner')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Banner</span>
							</a>
						</div>
						
					</div>
				</div> 
				@endif

				@if($iDepartmentId == 3)
				<div class="menu-item @if($url == 'OfficeProfile') show @endif">
					<a class="menu-link" href="{{route('admin.OfficeProfile')}}">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect x="2" y="2" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Office Profile</span>
					</a>
				</div>
				@endif

				@if($iDepartmentId == 2)
				<div class="menu-item @if($url == 'LabProfile') show @endif">
					<a class="menu-link" href="{{route('admin.LabProfile')}}">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect x="2" y="2" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Lab Profile</span>
					</a>
				</div>
				@endif
				
				<div class="menu-item">
					<div class="menu-content p-0">
						<div class="separator"></div>
					</div>
				</div>
				@if(\App\Libraries\General::check_permission('AdminController','eRead') == 'true')
				<div class="menu-item @if($url == 'admin') show @endif">
					<a class="menu-link" href="{{route('admin.admin')}}">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect x="2" y="2" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Super Admin</span>
					</a>
				</div>
				@endif
				<div class="menu-item">
					<div class="menu-content p-0">
						<div class="separator"></div>
					</div>
				</div>
				@php
				$CustomerAccess = \App\Libraries\General::check_permission('CustomerController','eRead');
				$CustomerTypeAccess = \App\Libraries\General::check_permission('CustomerTypeController','eRead');
				$LabAdminAccess = \App\Libraries\General::check_permission('LabAdminController','eRead');
				$OfficeAdminAccess = \App\Libraries\General::check_permission('OfficeAdminController','eRead');
				$OfficeAccess = \App\Libraries\General::check_permission('OfficeController','eRead');
				$UsserAccess = \App\Libraries\General::check_permission('UserLabController','eRead');
				$DoctorAccess = \App\Libraries\General::check_permission('DoctorController','eRead');
				@endphp
				
				@if($CustomerAccess == false && $CustomerTypeAccess == false )
				@else
				<div data-kt-menu-trigger="click" class="menu-item menu-accordion @if($url == 'customertype' || $url == 'customer' ) hover show @endif">
					<span class="menu-link">
						<span class="menu-icon">
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
									<path opacity="0.3" d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z" fill="black" />
									<path d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Customer</span>
						<span class="menu-arrow"></span>
					</span>
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						@if(\App\Libraries\General::check_permission('CustomerTypeController','eRead') == 'true')
						<div class="menu-item @if($url == 'customertype') show @endif">
							<a class="menu-link" href="{{route('admin.customertype')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Customer Type</span>
							</a>
						</div>
						@endif
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
						@if(\App\Libraries\General::check_permission('CustomerController','eRead') == 'true')
						<div class="menu-item @if($url == 'customer' || $url == 'officelist') show @endif">
							<a class="menu-link" href="{{route('admin.customer')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Customer</span>
							</a>
						</div>
						@endif
					
					</div>
				</div>
				<div class="menu-item">
					<div class="menu-content p-0">
						<div class="separator"></div>
					</div>
				</div>
				@endif
				@if($LabAdminAccess == false && $UsserAccess == false )
				@else
				{{-- <div data-kt-menu-trigger="click" class="menu-item menu-accordion @if($url == 'user' || $url == 'userlist' || $url == 'lab_admin' ) hover show @endif"> --}}
					
				<div data-kt-menu-trigger="click" class="menu-item menu-accordion @if($url == 'lab' || $url1 == 'lab' || $url == 'userlist' || $url == 'lab_admin' ) hover show @endif">
					<span class="menu-link">
						<span class="menu-icon">
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
									<path opacity="0.3" d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z" fill="black" />
									<path d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Lab</span>
						<span class="menu-arrow"></span>
					</span>
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						@if(\App\Libraries\General::check_permission('LabAdminController','eRead') == 'true')
						<div class="menu-item @if($url == 'lab_admin') show @endif">
							<a class="menu-link" href="{{route('admin.lab_admin')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Lab Admin</span>
							</a>
						</div>
						@endif
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
						@if(\App\Libraries\General::check_permission('UserLabController','eRead') == 'true')
						<div class="menu-item @if($url == 'lab' || $url1 == 'lab') show @endif">
							{{-- <a class="menu-link" href="{{route('admin.user')}}"> --}}
							<a class="menu-link" href="{{ url('admin/lab/user') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">User</span>
							</a>
						</div>
						@endif
					
					</div>
				</div>
				<div class="menu-item">
					<div class="menu-content p-0">
						<div class="separator"></div>
					</div>
				</div>
				@endif
				@if($OfficeAdminAccess == false  && $OfficeAccess == false && $DoctorAccess == false)
				@else
				{{-- <div data-kt-menu-trigger="click" class="menu-item menu-accordion @if($url == 'office_admin' || $url == 'doctor' || $url == 'user' || $url == 'userlist' || $url == 'doctorlist' ) hover show @endif"> --}}
			
				<div data-kt-menu-trigger="click" class="menu-item menu-accordion @if($url == 'office_admin' || $url == 'doctor' || $url == 'office' || $url1 == 'office' || $url == 'userlist' || $url == 'doctorlist' ) hover show @endif">
					<span class="menu-link">
						<span class="menu-icon">
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
									<path opacity="0.3" d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z" fill="black" />
									<path d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Office</span>
						<span class="menu-arrow"></span>
					</span>
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						
						@if(\App\Libraries\General::check_permission('OfficeAdminController','eRead') == 'true')
						<div class="menu-item @if($url == 'office_admin') show @endif">
							<a class="menu-link" href="{{route('admin.office_admin')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Office Admin</span>
							</a>
						</div>
						@endif
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
						
						@if(\App\Libraries\General::check_permission('DoctorController','eRead') == 'true')
						<div class="menu-item @if($url == 'doctor' || $url == 'userlist') show @endif">
							<a class="menu-link" href="{{route('admin.doctor')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Doctor</span>
							</a>
						</div>
						@endif
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
						@if(\App\Libraries\General::check_permission('UserController','eRead') == 'true')
						<div class="menu-item @if($url == 'office' || $url1 == 'office') show @endif">
							{{-- <a class="menu-link" href="{{route('admin.user')}}"> --}}
							<a class="menu-link" href="{{ url('admin/office/user') }}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">User</span>
							</a>
						</div>
						@endif
					</div>
				</div>
				<div class="menu-item">
					<div class="menu-content p-0">
						<div class="separator"></div>
					</div>
				</div>
				@endif
				@if(\App\Libraries\General::check_permission_slip('eReadSlip') == 'Yes')
				<div class="menu-item @if($url == 'labcase') show @endif">
					@if(\App\Libraries\General::admin_info()['iCustomerTypeId'] == 6)
					<a class="menu-link" href="{{route('admin.labcase.office_admin_listing')}}">
					@else	
					<a class="menu-link" href="{{route('admin.labcase')}}">
					@endif	
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect x="2" y="2" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="black" />
									<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Slips</span>
					</a>
				</div>
				@endif
				<div class="menu-item">
					<div class="menu-content p-0">
						<div class="separator"></div>
					</div>
				</div>
				@if(\App\Libraries\General::check_permission('CategoryController','eRead') == false && \App\Libraries\General::check_permission('CategoryProductController','eRead') == false && \App\Libraries\General::check_permission('ProductStageController','eRead') == false && \App\Libraries\General::check_permission('AddoncategoryController','eRead') == false && \App\Libraries\General::check_permission('SubaddoncategoryController','eRead') == false && \App\Libraries\General::check_permission('','eRead','extractions') == false && \App\Libraries\General::check_permission('','eRead','impressions') == false && \App\Libraries\General::check_permission('','eRead','tooth_shades') == false && \App\Libraries\General::check_permission('','eRead','gum_shades') == false && \App\Libraries\General::check_permission('','eRead','stage_notes') == false && \App\Libraries\General::check_permission('','eRead','rush_dates') == false && \App\Libraries\General::check_permission('CasepanController','eRead') == false && \App\Libraries\General::check_permission('DepartmentController','eRead') == false && \App\Libraries\General::check_permission('StaffController','eRead') == false && \App\Libraries\General::check_permission('GradeController','eRead') == false && \App\Libraries\General::check_permission('ScheduleController','eRead') == false && \App\Libraries\General::check_permission('CallController','eRead') == false)
				@else
				<div data-kt-menu-trigger="click" class="menu-item menu-accordion @if($url == 'casepan' || $url == 'casepannumber' || $url == 'department' || $url == 'staff' || $url == 'grade' || $url == 'schedule' ||$url =='calendar' || $url == 'call' || $url == 'stage' || $url == 'category' || $url == 'categoryproduct' ||$url == 'categoryproductselect' || $url == 'addoncategory' || $url == 'subaddoncategory' || $url == 'productstage') hover show @endif">
					<span class="menu-link">
						<span class="menu-icon">
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
									<path opacity="0.3" d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z" fill="black" />
									<path d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Lab Administrator</span>
						<span class="menu-arrow"></span>
					</span>
				
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						<div class="menu-item @if($url == 'category' || $url == 'categoryproduct' || $url == 'categoryproductselect' || $url == 'productstage' || $url == 'addoncategory' || $url == 'subaddoncategory') show @endif">
							@if(\App\Libraries\General::check_permission('CategoryController','eRead') == 'true')
							<a class="menu-link" href="{{route('admin.category')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Products</span>
							</a>
							@elseif(\App\Libraries\General::check_permission('CategoryProductController','eRead') == 'true')
							<a class="menu-link" href="{{route('admin.categoryproduct')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Products</span>
							</a>
							@elseif(\App\Libraries\General::check_permission('ProductStageController','eRead') == 'true')
							<a class="menu-link" href="{{route('admin.productstage')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Products</span>
							</a>
							@elseif(\App\Libraries\General::check_permission('AddoncategoryController','eRead') == 'true')
							<a class="menu-link" href="{{route('admin.addoncategory')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Products</span>
							</a>
							@elseif(\App\Libraries\General::check_permission('SubaddoncategoryController','eRead') == 'true')
							<a class="menu-link" href="{{route('admin.subaddoncategory')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Products</span>
							</a>
							@endif
						</div>
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
					</div>
					
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						<div class="menu-item @if($url == 'stage') show @endif">
							
							@if(\App\Libraries\General::check_permission('','eRead','extractions') == 'true')
							<a class="menu-link" href="{{route('admin.stage')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Stage</span>
							</a>
							@elseif(\App\Libraries\General::check_permission('','eRead','impressions') == 'true')
							<a class="menu-link" href="{{route('admin.impression')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Stage</span>
							</a>
							@elseif(\App\Libraries\General::check_permission('','eRead','tooth_shades') == 'true')
							<a class="menu-link" href="{{route('admin.toothshades')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Stage</span>
							</a>
							@elseif(\App\Libraries\General::check_permission('','eRead','gum_shades') == 'true')
							<a class="menu-link" href="{{route('admin.gumshades')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Stage</span>
							</a>
							@elseif(\App\Libraries\General::check_permission('','eRead','stage_notes') == 'true')
							<a class="menu-link" href="{{route('admin.notes')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Stage</span>
							</a>
							@elseif(\App\Libraries\General::check_permission('','eRead','rush_dates') == 'true')
							<a class="menu-link" href="{{route('admin.rushdate')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Stage</span>
							</a>
							@endif
						</div>
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
					</div>
					@if(\App\Libraries\General::check_permission('CasepanController','eRead') == 'true')
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						<div class="menu-item @if($url == 'casepan') show @endif">
							<a class="menu-link" href="{{route('admin.casepan')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Case pan</span>
							</a>
						</div>
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
					</div>
					@endif
					@if(\App\Libraries\General::check_permission('DepartmentController','eRead') == 'true')
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						<div class="menu-item @if($url == 'department') show @endif">
							<a class="menu-link" href="{{route('admin.department')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Departments</span>
							</a>
						</div>
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
					</div>
					@endif
					@if(\App\Libraries\General::check_permission('StaffController','eRead') == 'true')
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						<div class="menu-item @if($url == 'staff') show @endif">
							<a class="menu-link" href="{{route('admin.staff')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Staff</span>
							</a>
						</div>
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
					</div>
					@endif
					
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						@if(\App\Libraries\General::check_permission('GradeController','eRead') == 'true')
						<div class="menu-item @if($url == 'grade') show @endif">
							<a class="menu-link" href="{{route('admin.grade')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Grade</span>
							</a>
						</div>
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
						@endif
						@if(\App\Libraries\General::check_permission('ScheduleController','eRead') == 'true')
						<div class="menu-item @if($url == 'schedule' || $url == 'calendar') show @endif">
							<a class="menu-link" href="{{route('admin.schedule')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Lab Schedule</span>
							</a>
						</div>
						@endif
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
					</div>
					@if(\App\Libraries\General::check_permission('CallController','eRead') == 'true')
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						<div class="menu-item @if($url == 'call') show @endif">
							<a class="menu-link" href="{{route('admin.call')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Call Log</span>
							</a>
						</div>
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
					</div>
					@endif
				</div>
				@endif
				{{-- Lab For Office Start --}}
				{{-- <div data-kt-menu-trigger="click" class="menu-item menu-accordion @if($url == 'categoryproductoffice' || $url == 'categoryproductoffice_list') hover show @endif">
					<span class="menu-link">
						<span class="menu-icon">
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
									<path opacity="0.3" d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z" fill="black" />
									<path d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Lab </span>
						<span class="menu-arrow"></span>
					</span>
				
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						<div class="menu-item @if($url == 'categoryproductoffice' || $url =='categoryproductoffice_list') show @endif">
							<a class="menu-link" href="{{route('admin.officelab')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Labs</span>
							</a>
						
						</div>
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
					</div>
					
				
				</div> --}}
				{{-- Lab For Office End --}}
				{{-- Billing start --}}
				<div class="menu-item">
					<div class="menu-content p-0">
						<div class="separator"></div>
					</div>
				</div>
				@if(\App\Libraries\General::check_permission('BillingController','eRead') == true)
				<div data-kt-menu-trigger="click" class="menu-item menu-accordion @if($url == 'billing') hover show @endif">
					<span class="menu-link">
						<span class="menu-icon">
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
									<path opacity="0.3" d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z" fill="black" />
									<path d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Billing </span>
						<span class="menu-arrow"></span>
					</span>
				
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						<div class="menu-item @if($url == 'billing') show @endif">
							<a class="menu-link" href="{{route('admin.billing')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Billing</span>
							</a>
						
						</div>
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
					</div>
				</div>
				@endif
				{{-- Billing end --}}
				<div class="menu-item">
					<div class="menu-content p-0">
						<div class="separator"></div>
					</div>
				</div>
				@if(\App\Libraries\General::check_permission('ModulePermissionController','eRead') == 'true')
				<div data-kt-menu-trigger="click" class="menu-item menu-accordion @if($url == 'role' || $url == 'module-permission' || $url == 'module-master') hover show @endif">
					<span class="menu-link">
						<span class="menu-icon">
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
									<path opacity="0.3" d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z" fill="black" />
									<path d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Permission</span>
						<span class="menu-arrow"></span>
					</span>
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						{{-- <div class="menu-item @if($url == 'role') show @endif">
							<a class="menu-link" href="{{route('admin.role')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Role</span>
							</a>
						</div> --}}
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
					</div>
					@if(\App\Libraries\General::check_permission('ModuleMasterController','eRead') == 'true')
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						<div class="menu-item @if($url == 'module-master') show @endif">
							<a class="menu-link" href="{{route('admin.moduleMaster')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Module Master</span>
							</a>
						</div>
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
					</div>
					@endif
					@if(\App\Libraries\General::check_permission('ModulePermissionController','eRead') == 'true')
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						<div class="menu-item @if($url == 'module-permission') show @endif">
							<a class="menu-link" href="{{route('admin.modulePermission.create')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Module Permission</span>
							</a>
						</div>
						<div class="menu-item">
							<div class="menu-content p-0">
								<div class="separator"></div>
							</div>
						</div>
					</div>
					@endif
				</div>
				@endif
			
				{{-- <div data-kt-menu-trigger="click" class="menu-item menu-accordion  @if($url == 'page' || $url == 'setting') hover show @endif">
					<span class="menu-link">
						<span class="menu-icon">
							<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg-->
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
									<rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Settings</span>
						<span class="menu-arrow"></span>
					</span>
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu">
						<div class="menu-item @if($url1 == 'Company') show @endif">
							<a class="menu-link" href="{{url('admin/setting/Company')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Company Info</span>
							</a>
						</div>
						<div class="menu-item @if($url1 == 'Email') show @endif">
							<a class="menu-link" href="{{url('admin/setting/Email')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Email Settings</span>
							</a>
						</div>
						<div class="menu-item @if($url1 == 'Meta') show @endif">
							<a class="menu-link" href="{{url('admin/setting/Meta')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Meta Info</span>
							</a>
						</div>
						<div class="menu-item @if($url1 == 'Social') show @endif">
							<a class="menu-link" href="{{url('admin/setting/Social')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Social Info</span>
							</a>
						</div>
					</div>
				</div> --}}
				
				@if(\App\Libraries\General::check_permission('SettingController','eRead') == 'true')
				<div data-kt-menu-trigger="click" class="menu-item menu-accordion @if($url == 'system_email' || $url1 == 'Appearance' || $url1 == 'Email' || $url1 =='Company') hover show @endif" bis_skin_checked="1">
					<span class="menu-link">
						<span class="menu-icon">
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
									<path opacity="0.3" d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z" fill="black"></path>
									<path d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z" fill="black"></path>
								</svg>
							</span>
							<!--end::Svg Icon-->
						</span>
						<span class="menu-title">Settings </span>
						<span class="menu-arrow"></span>
					</span>
					
					<div class="menu-sub menu-sub-accordion menu-active-bg c-side-menu @if($url == 'system_email') show @endif" bis_skin_checked="1" kt-hidden-height="40" style="">
						<div class="menu-item @if($url == 'system_email') show @endif" bis_skin_checked="1">
							<a class="menu-link"  href="{{route('admin.systemEmail.listing')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">System Email </span>
							</a>
						
						</div>
						<div class="menu-item" bis_skin_checked="1">
							<div class="menu-content p-0" bis_skin_checked="1">
								<div class="separator" bis_skin_checked="1"></div>
							</div>
						</div>
					
						<div class="menu-item @if($url1 == 'Appearance') show @endif" bis_skin_checked="1">
							<a class="menu-link"  href="{{url('admin/setting/Appearance')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">General Setting</span>
							</a>
						</div>
						<div class="menu-item " bis_skin_checked="1">
							<div class="menu-content p-0" bis_skin_checked="1">
								<div class="separator" bis_skin_checked="1"></div>
							</div>
						</div>
						<div class="menu-item @if($url1 == 'Email') show @endif" bis_skin_checked="1">
							<a class="menu-link"  href="{{url('admin/setting/Email')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Email Setting</span>
							</a>
						</div>
						<div class="menu-item" bis_skin_checked="1">
							<div class="menu-content p-0" bis_skin_checked="1">
								<div class="separator" bis_skin_checked="1"></div>
							</div>
						</div>
						<div class="menu-item @if($url1 == 'Company') show @endif" bis_skin_checked="1">
							<a class="menu-link"  href="{{url('admin/setting/Company')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Company Info</span>
							</a>
						</div>
						{{-- <div class="menu-item" bis_skin_checked="1">
							<div class="menu-content p-0" bis_skin_checked="1">
								<div class="separator" bis_skin_checked="1"></div>
							</div>
						</div> --}}
						{{-- <div class="menu-item @if($url1 == 'Social') show @endif" bis_skin_checked="1">
							<a class="menu-link"  href="{{url('admin/setting/Social')}}">
								<span class="menu-bullet">
									<span class="bullet bullet-dot"></span>
								</span>
								<span class="menu-title">Social Info</span>
							</a>
						</div> --}}
						{{-- <div class="menu-item" bis_skin_checked="1">
							<div class="menu-content p-0" bis_skin_checked="1">
								<div class="separator" bis_skin_checked="1"></div>
							</div>
						</div> --}}
					</div>
				
				</div>
				@endif
				<div class="menu-item">
					<div class="menu-content p-0">
						<div class="separator"></div>
					</div>
				</div>
				
				<a class="menu-link" href="{{route('admin.logout')}}">
					<div class="menu-item menu-accordion">
						<span class="menu-link">
							<span class="menu-icon">
								<!--begin::Svg Icon | path: icons/duotune/finance/fin006.svg-->
								
								<span class="svg-icon svg-icon-2"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo4/dist/../src/media/svg/icons/Navigation/Sign-out.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" viewBox="0 0 24 24">
										<rect x="0" y="0" width="24" height="24"/>
										<path d="M14.0069431,7.00607258 C13.4546584,7.00607258 13.0069431,6.55855153 13.0069431,6.00650634 C13.0069431,5.45446114 13.4546584,5.00694009 14.0069431,5.00694009 L15.0069431,5.00694009 C17.2160821,5.00694009 19.0069431,6.7970243 19.0069431,9.00520507 L19.0069431,15.001735 C19.0069431,17.2099158 17.2160821,19 15.0069431,19 L3.00694311,19 C0.797804106,19 -0.993056895,17.2099158 -0.993056895,15.001735 L-0.993056895,8.99826498 C-0.993056895,6.7900842 0.797804106,5 3.00694311,5 L4.00694793,5 C4.55923268,5 5.00694793,5.44752105 5.00694793,5.99956624 C5.00694793,6.55161144 4.55923268,6.99913249 4.00694793,6.99913249 L3.00694311,6.99913249 C1.90237361,6.99913249 1.00694311,7.89417459 1.00694311,8.99826498 L1.00694311,15.001735 C1.00694311,16.1058254 1.90237361,17.0008675 3.00694311,17.0008675 L15.0069431,17.0008675 C16.1115126,17.0008675 17.0069431,16.1058254 17.0069431,15.001735 L17.0069431,9.00520507 C17.0069431,7.90111468 16.1115126,7.00607258 15.0069431,7.00607258 L14.0069431,7.00607258 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.006943, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-9.006943, -12.000000) "/>
										<rect fill="#000000" opacity="0.3" transform="translate(14.000000, 12.000000) rotate(-270.000000) translate(-14.000000, -12.000000) " x="13" y="6" width="2" height="12" rx="1"/>
										<path d="M21.7928932,9.79289322 C22.1834175,9.40236893 22.8165825,9.40236893 23.2071068,9.79289322 C23.5976311,10.1834175 23.5976311,10.8165825 23.2071068,11.2071068 L20.2071068,14.2071068 C19.8165825,14.5976311 19.1834175,14.5976311 18.7928932,14.2071068 L15.7928932,11.2071068 C15.4023689,10.8165825 15.4023689,10.1834175 15.7928932,9.79289322 C16.1834175,9.40236893 16.8165825,9.40236893 17.2071068,9.79289322 L19.5,12.0857864 L21.7928932,9.79289322 Z" fill="#000000" fill-rule="nonzero" transform="translate(19.500000, 12.000000) rotate(-90.000000) translate(-19.500000, -12.000000) "/>
									</g>
								</svg><!--end::Svg Icon--></span>
								<!--end::Svg Icon-->
							</span>
							<span class="menu-title">Sign Out</span>
						</span>
					</div>
				</a>
				
			
				<div class="menu-item">
					<div class="menu-content p-0">
						<div class="separator"></div>
					</div>
				</div>
			</div>
			<!--end::Menu-->
		</div>
		<!--end::Aside Menu-->
	</div>
</div>
