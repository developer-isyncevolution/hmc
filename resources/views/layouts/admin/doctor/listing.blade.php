@extends('layouts.admin.index')
@section('content')
<!-- BEGIN: Content-->
<!-- DOM - jQuery events table -->
<div class="main-panel">
    <div class="page-title text-center">
        <h3>
            Doctor
        </h3>
    </div>

    <div class="col-lg-12 mx-auto">
        <div class="listing-page">
            <div class="card mb-5 mb-xl-4">
        
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-mid">
                                <ul class="d-flex list-unstyled">
                                    @if(\App\Libraries\General::check_permission('','eCreate') == true)
                                    <li>
                                        <a href="{{route('admin.doctor.create')}}" class="btn create_permission add-btn">
                                            <i class="fa fa-plus"></i> Add
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    @if(\App\Libraries\General::admin_info()['customerType']=='Super Admin')
                    <div class="col-lg-6">
                        <select name="iCustomerId" id="iCustomerId">
                            <option value="">Select Customer</option>
                            @foreach($customer as $key => $customers)
                            <option value="{{$customers->iCustomerId}}" @if(isset($iCustomerId)){{$customers->iCustomerId == $iCustomerId  ? 'selected' : ''}} @endif>{{$customers->vOfficeName}}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    {{-- <div class="col-lg-4">
                        <select name="iOfficeId" id="iOfficeId">
                            <option value="">Select office</option>
                            @foreach($office as $key => $offices)
                            <option value="{{$offices->iOfficeId}}" @if(isset($doctors)){{$offices->iOfficeId == $doctors->iOfficeId  ? 'selected' : ''}} @endif>{{$offices->vOfficeName}}</option>
                            @endforeach
                        </select>
                    </div> --}}
                    <div class="col-lg-6">
                        <div class="custome-serce-box">
                            <input type="text" class="form-control" id="keyword" name="search" class="search" placeholder="Search">
                            <span class="search-btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($iOfficeId))
                <input type="hidden" name="office" id="office" value="{{$iOfficeId}}">
            @endisset
            @if(isset($iCustomerId))
                <input type="hidden" name="customer" id="customer" value="{{$iCustomerId}}">
            @endisset
            <!-- <div class="col-xl-6 col-lg-12 col-md-6">
                <label>office</label>
                  <select name="iOfficeId" id="iOfficeId" class="form-control">
                    @foreach($office as $key => $offices)
                    <option value="{{$offices->iOfficeId}}" @if(isset($doctors)){{$offices->iOfficeId == $doctors->iOfficeId  ? 'selected' : ''}} @endif>{{$offices->vName}}</option>
                    @endforeach
                  </select>
              </div> -->
              
            <div class="table-data table-responsive">
                <table class="table">
                    <thead>
                        <tr class="fw-bolder text-muted">
                            <th class="w-25px text-center" data-orderable="false">
                                <div class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-13-check" id="selectall" type="checkbox" name="selectall">
                                    <label for="selectall">&nbsp;</label>
                                </div>
                            </th>
                            <th class="min-w-100px">
                                <a id="vFirstName" class="sort" data-column="vFirstName" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Office Name</span>
                                </a>
                            </th>
                            <th class="min-w-100px">
                                <a id="vFirstName"  href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Email</span>
                                </a>
                            </th>
                            <th class="min-w-100px">
                                <a id="vFirstName" class="sort" data-column="vFirstName" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7"> Name</span>
                                </a>
                            </th>
                            <th class="min-w-100px">
                                <a id="vLicence" class="sort" data-column="vLicence" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Licence</span>
                                </a>
                            </th>
                            <th class="min-w-100px">
                                <a id="eStatus" class="sort" data-column="eStatus" data-order="ASC" href="#">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Status</span>
                                </a>
                            </th>
                            <th class="max-w-100px">
                                <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="table_record">
                    </tbody>
                </table>
                <div class="text-center loaderimg">
                    <div class="loaderinner">
                        <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- DOM - jQuery events table -->
<!-- END: Content-->
@endsection

@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
 $('#iCustomerId').selectize();
 $('#iOfficeId').selectize();
    
    $(document).ready(function() {
        var iOfficeId = $("#office").val();
        var iCustomerId = $("#customer").val();
        $.ajax({
            url: "{{route('admin.doctor.ajaxListing')}}",
            type: "get",
            data: {
                iOfficeId:iOfficeId,iCustomerId:iCustomerId,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $("#selectall").click(function() {
        if (this.checked) {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', true);
            });
        } else {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', false);
            });
        }
    });


    $("#keyword").keyup(function() {
        var keyword = $("#keyword").val();
        var iOfficeId = $("#iOfficeId").val();
        var iCustomerId = $("#iCustomerId").val();

        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.doctor.ajaxListing')}}",
                type: "get",
                data: {
                    keyword: keyword,
                    iOfficeId:iOfficeId,
                    iCustomerId:iCustomerId,
                    action: 'search'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });

    $(document).on('click', '#delete_btn', function() {
        var id = [];

        $("input[name='Doctor_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");
        var iOfficeId = $("#iOfficeId").val();
        var iCustomerId = $("#iCustomerId").val();

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                title: "Are you sure delete all doctor ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "{{route('admin.doctor.ajaxListing')}}",
                        type: "get",
                        data: {
                            id: id,
                            iOfficeId:iOfficeId,
                            iCustomerId:iCustomerId,
                            action: 'multiple_delete'
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                        }
                    });
                } 
            });
        }
    });
    $(document).on('click', '#delete', function() {
        swal({
                title: "Are you sure delete this doctor ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");
                    var iOfficeId = $("#iOfficeId").val();
                    var iCustomerId = $("#iCustomerId").val();

                    $("#ajax-loader").show();

                        $.ajax({
                            url: "{{route('admin.doctor.ajaxListing')}}",
                            type: "get",
                            data: {
                                id: id,
                                iOfficeId:iOfficeId,
                                iCustomerId:iCustomerId,
                                action: 'delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("Doctor Deleted Successfully");
                                setTimeout(function() {
                                   location.reload();
                                }, 1000);
                            }
                        });
                }
            })
    });
    $(document).on('click', '.sort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');
        var iCustomerId = $("#iCustomerId").val();
        var iOfficeId = $("#iOfficeId").val();

        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.doctor.ajaxListing')}}",
                type: "get",
                data: {
                    column: column,
                    order:order,
                    iOfficeId:iOfficeId,
                    iCustomerId:iCustomerId,
                    action: 'sort'
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });

    $(document).on('click', '.ajax_page', function() {
        pages = $(this).data("pages");
        var iOfficeId = $("#iOfficeId").val();
        var iCustomerId = $("#iCustomerId").val();

        $("#table_record").html('');
        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.doctor.ajaxListing')}}",
                type: "get",
                data: {
                    pages: pages,iOfficeId:iOfficeId,iCustomerId:iCustomerId,
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });
    $(document).on('change', '#iOfficeId', function() {
        var iOfficeId = $("#iOfficeId").val();

        $("#table_record").html('');
        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.doctor.ajaxListing')}}",
                type: "get",
                data: {
                    iOfficeId:iOfficeId
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });
    $(document).on('change', '#iCustomerId', function() {
        var iCustomerId = $("#iCustomerId").val();

        $("#table_record").html('');
        $("#ajax-loader").show();

            $.ajax({
                url: "{{route('admin.doctor.ajaxListing')}}",
                type: "get",
                data: {
                    iCustomerId:iCustomerId
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
    });
    $(document).on('change','#page_limit',function()
    {
        limit_page = this.value;
        $("#table_record").html('');
        $("#ajax-loader").show();
        url = "{{route('admin.doctor.ajaxListing')}}";

        setTimeout(function(){
            $.ajax({
                url: url,
                type: "get",
                data:  {limit_page: limit_page}, 
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            // hideLoader();
            }, 500);
    });
</script>
@endsection