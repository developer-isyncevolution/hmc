@extends('layouts.admin.index')
@section('content')
    <!-- BEGIN: Content-->
    <!-- DOM - jQuery events table -->
    <div class="main-panel">
        <div class="page-title text-center">
            <h3>
                Customer Type
            </h3>
        </div>
        <div class="col-lg-12 mx-auto">
            <div class="listing-page">
                <div class="card mb-5 mb-xl-4">
                    <div class="card-header">
                        <ul class="c-nav-tab">
                            @if(\App\Libraries\General::check_permission('CustomerController','eRead') == 'true')
                            <li class="">
                                <a href="{{ route('admin.customer.edit', $iCustomerId) }}" class="add-btn btn mx-2">
                                    Customer
                                </a>
                            </li>
                            @endif
                            @if(\App\Libraries\General::check_permission('OfficeController','eRead') == 'true')
                            <li class="">
                                <a href="{{ route('admin.office.officeedit', [$iCustomerId, $iOfficeId]) }}"
                                    class="add-btn btn mx-2">
                                    Office
                                </a>
                            </li>
                            @endif
                            @if(\App\Libraries\General::check_permission('DoctorController','eRead') == 'true')
                            <li>
                                <a href="" class="active add-btn btn mx-2">
                                    Doctor
                                </a>
                            </li>
                            @endif

                        </ul>
                        <ul class="my-2 list-unstyled w-100">
                            <li class="d-flex justify-content-center">
                                <a href="{{ route('admin.doctor.doctor-create', ['iCustomerId' => $iCustomerId, 'iOfficeId' => $iOfficeId]) }}"
                                    class="btn add-btn create_permission mx-2">
                                    <i class="fa fa-plus"></i> Add
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                @if (isset($iOfficeId))
                    <input type="hidden" name="customer" id="customer" value="{{ $iCustomerId }}">
                    <input type="hidden" name="office" id="office" value="{{ $iOfficeId }}">
                @endisset
                <!-- <div class="col-xl-6 col-lg-12 col-md-6">
                <label>office</label>
                  <select name="iOfficeId" id="iOfficeId" class="form-control">
                    @foreach ($office as $key => $offices)
<option value="{{ $offices->iOfficeId }}" @if (isset($doctors)) {{ $offices->iOfficeId == $doctors->iOfficeId ? 'selected' : '' }} @endif>{{ $offices->vOfficeName }}</option>
@endforeach
                  </select>
              </div> -->

                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr class="fw-bolder text-muted">
                                <th class="w-25px text-center" data-orderable="false">
                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                        <input class="form-check-input" type="checkbox" value="1" data-kt-check="true"
                                            data-kt-check-target=".widget-13-check" id="selectall" type="checkbox"
                                            name="selectall">
                                        <label for="selectall">&nbsp;</label>
                                    </div>
                                </th>
                                <th class="min-w-100px">
                                    <a id="vFirstName" class="sort" data-column="vFirstName" data-order="ASC"
                                        href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Office Name</span>
                                    </a>
                                </th>
                                <th class="min-w-100px">
                                    <a id="vFirstName" class="sort" data-column="vFirstName" data-order="ASC"
                                        href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">First Name</span>
                                    </a>
                                </th>
                                <th class="min-w-100px">
                                    <a id="vLastName" class="sort" data-column="vLastName" data-order="ASC"
                                        href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Last Name</span>
                                    </a>
                                </th>
                                <th class="min-w-100px">
                                    <a id="vLicence" class="sort" data-column="vLicence" data-order="ASC"
                                        href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Licence</span>
                                    </a>
                                </th>
                                <th class="min-w-100px">
                                    <a id="eStatus" class="sort" data-column="eStatus" data-order="ASC"
                                        href="#">
                                        <span class="text-muted fw-bold text-muted d-block fs-7">Status</span>
                                    </a>
                                </th>
                                <th class="max-w-100px">
                                    <span class="text-muted fw-bold text-muted d-block fs-7">Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{ asset('admin/assets/images/ajax-loader.gif') }}" id="ajax-loader" width="250px"
                                height="auto" />
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<!-- DOM - jQuery events table -->
<!-- END: Content-->
@endsection

@section('custom-js')
<script>
    $(document).ready(function() {
        var iOfficeId = $("#office").val();
        $.ajax({
            url: "{{ route('admin.doctor.doctorajaxListing') }}",
            type: "get",
            data: {
                iOfficeId: iOfficeId,
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $("#selectall").click(function() {
        if (this.checked) {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', true);
            });
        } else {
            $('.checkboxall').each(function() {
                $(".checkboxall").prop('checked', false);
            });
        }
    });


    $("#keyword").keyup(function() {
        var keyword = $("#keyword").val();
        var iOfficeId = $("#office").val();

        $("#ajax-loader").show();

        $.ajax({
            url: "{{ route('admin.doctor.doctorajaxListing') }}",
            type: "get",
            data: {
                keyword: keyword,
                iOfficeId: iOfficeId,
                action: 'search'
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '#delete_btn', function() {
        var id = [];

        $("input[name='Doctor_ID[]']:checked").each(function() {
            id.push($(this).val());
        });

        var id = id.join(",");
        var iOfficeId = $("#office").val();

        if (id.length == 0) {
            alert('Please select records.')
        } else {
            swal({
                    title: "Are you sure delete all doctor ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{ route('admin.doctor.doctorajaxListing') }}",
                            type: "get",
                            data: {
                                id: id,
                                iOfficeId: iOfficeId,
                                action: 'multiple_delete'
                            },
                            success: function(response) {
                                $("#table_record").html(response);
                                $("#ajax-loader").hide();
                                notification_error("Doctor Deleted Successfully");
                                setTimeout(function() {
                                    location.reload();
                                }, 1000);
                            }
                        });
                    }
                });
        }
    });
    $(document).on('click', '#delete', function() {
        swal({
                title: "Are you sure delete this doctor ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    id = $(this).data("id");
                    var iOfficeId = $("#office").val();

                    $("#ajax-loader").show();

                    $.ajax({
                        url: "{{ route('admin.doctor.doctorajaxListing') }}",
                        type: "get",
                        data: {
                            id: id,
                            iOfficeId: iOfficeId,
                            action: 'delete'
                        },
                        success: function(response) {
                            $("#table_record").html(response);
                            $("#ajax-loader").hide();
                            notification_error("Doctor Deleted Successfully");
                            setTimeout(function() {
                                location.reload();
                            }, 1000);
                        }
                    });
                }
            })
    });
    $(document).on('click', '.sort', function() {
        column = $(this).data("column");
        order = $(this).attr('data-order');
        var iOfficeId = $("#office").val();


        if (order == "ASC") {
            $(this).attr('data-order', 'DESC');
        } else {
            $(this).attr('data-order', 'ASC');
        }

        $("#ajax-loader").show();

        $.ajax({
            url: "{{ route('admin.doctor.doctorajaxListing') }}",
            type: "get",
            data: {
                column: column,
                iOfficeId: iOfficeId,
                order,
                order,
                action: 'sort'
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });

    $(document).on('click', '.ajax_page', function() {
        pages = $(this).data("pages");
        var iOfficeId = $("#office").val();

        $("#table_record").html('');
        $("#ajax-loader").show();

        $.ajax({
            url: "{{ route('admin.doctor.doctorajaxListing') }}",
            type: "get",
            data: {
                pages: pages,
                iOfficeId: iOfficeId,
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#iOfficeId', function() {
        var iOfficeId = $("#iOfficeId").val();
        var iCustomerId = $("#iCustomerId").val();

        $("#table_record").html('');
        $("#ajax-loader").show();

        $.ajax({
            url: "{{ route('admin.doctor.doctorajaxListing') }}",
            type: "get",
            data: {
                iOfficeId: iOfficeId
            },
            success: function(response) {
                $("#table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });
    $(document).on('change', '#page_limit', function() {
        var iOfficeId = $("#iOfficeId").val();
        var iCustomerId = $("#iCustomerId").val();
        limit_page = this.value;
        $("#table_record").html('');
        $("#ajax-loader").show();
        url = "{{ route('admin.doctor.doctorajaxListing') }}";

        setTimeout(function() {
            $.ajax({
                url: url,
                type: "get",
                data: {
                    limit_page: limit_page,
                    iCustomerId: iCustomerId,
                    iOfficeId: iOfficeId
                },
                success: function(response) {
                    $("#table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            hideLoader();
        }, 500);
    });
</script>
@endsection
