@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
    <div class="col-lg-10 mx-auto">
        <div class="card-header" bis_skin_checked="1">
            <h3 class="card-title">Settings</h3>
        </div>

        <form action="{{route('admin.setting.store')}}" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" id="eConfigType" name="eConfigType" value="<?php echo $eConfigType;?>">
            @foreach ($settings as $setting)
                <div class="col-xl-6 col-lg-12 col-md-6">
                    @if($setting['eDisplayType'] == 'text')
                        <label class="required fs-5 fw-bold mb-2" for="{{ $setting['vName'] }}">{{ $setting['vDesc'] }}</label>
                        <input type="text" class="form-control" id="{{ $setting['vName'] }}" name="{{ $setting['vName'] }}" placeholder="Please Enter {{ $setting['vName'] }}" value="{{ $setting['vValue'] }}">

                    @elseif($setting['eDisplayType'] == 'textarea')
                        <label class="required fs-5 fw-bold mb-2" for="{{ $setting['vName'] }}">{{ $setting['vDesc'] }}</label>
                        <textarea class="form-control" id="{{ $setting['vName'] }}" name="{{ $setting['vName'] }}" placeholder="Please enter {{ $setting['vName'] }}">
                            {{ $setting['vValue'] }}
                        </textarea>

                    @elseif($setting['eDisplayType'] == 'checkbox') 
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="{{ $setting['vName'] }}" name="{{ $setting['vName'] }}" value="Y" @if( $setting['vValue'] == 'Y') checked @endif>
                            <label class="form-check-label required fs-5 fw-bold mb-2" for="{{ $setting['vName'] }}">{{ $setting['vName'] }}</label>
                        </div>

                    @elseif($setting['eDisplayType'] == 'selectbox') 
                        @php
                            $select = explode(",", $setting['vSourceValue']);
                        @endphp
                        <label class="label-control required fs-5 fw-bold mb-2" for="{{ $setting['vName'] }}">{{ $setting['vDesc'] }}</label>
                        <select class="form-control show-tick" id="{{ $setting['vName'] }}" name="{{ $setting['vName'] }}">
                            @foreach ($select as $key => $value)
                              <option value="{{ $value }}" @if($value == $setting['vValue']) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>

                    @elseif($setting['eDisplayType'] == 'file') 
                        <label class="label-control required fs-5 fw-bold mb-2" for="{{ $setting['vName'] }}">{{ $setting['vDesc'] }}</label>

                        <div class="custome-files">
                            <input type="file" class="form-control" id="{{ $setting['vName'] }}" name="{{ $setting['vName'] }}">
                        </div>
                        @if($setting['vValue'] != "")
                            <img id="img" src="{{asset('uploads/logo/'.$setting['vValue'])}}" class="admin-logo" width="50px" height="auto"/>
                        @endif 

                    @elseif($setting['eDisplayType'] == 'hidden') 
                        <input type="hidden" id="{{ $setting['vName'] }}" name="{{ $setting['vName'] }}" value="{{ $setting['vValue'] }}">

                    @elseif($setting['eDisplayType'] == 'password') 
                        <label class="label-control required fs-5 fw-bold mb-2" for="{{ $setting['vName'] }}">{{ $setting['vDesc'] }}</label>

                        <input type="password" class="form-control" id="{{ $setting['vName'] }}" name="{{ $setting['vName'] }}" value="{{ $setting['vValue'] }}">
                    @else 
                        <label class="label-control required fs-5 fw-bold mb-2" for="{{ $setting['vName'] }}">{{ $setting['vDesc'] }}</label>
                        <input type="text" class="form-control" id="{{ $setting['vName'] }}" name="{{ $setting['vName'] }}" value="{{ $setting['vValue'] }}" readonly>
                    @endif
                </div>
            @endforeach
            
            
           
            <div class="card-footer col-12 align-self-end d-inline-block mt-3" bis_skin_checked="1">
           
              <button type="submit" class="btn submit-btn me-2 submit">Submit</button>
              {{-- <a href="javascript:;" class="btn back-btn me-2">Back</a> --}}
            </div>
        </form>
    </div>
</div>
@endsection

@section('custom-js')
@endsection
