@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
    <div class="page-title text-center">
        <h3>
            Lab 
        </h3>
    </div>

    <div class="row">

        <div class="col-lg-12 mx-auto">
    
            <div class="listing-page">
                <div class="table-data table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                               
                                <th>Lab Name</th>
                           
                                <th scope="col" class=""><span class="text-muted fw-bold text-muted d-block fs-7"> Action </span></th>
                            </tr>
                        </thead>
                        <tbody id="upper_table_record">
                        </tbody>
                    </table>
                    <div class="text-center loaderimg">
                        <div class="loaderinner">
                            <img src="{{asset('admin/assets/images/ajax-loader.gif')}}" id="ajax-loader" width="250px" height="auto" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
  
    </div>
</div>

@endsection

@section('custom-css')
<style></style>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;

<script>

    $(document).ready(function() {
        $.ajax({
            url: "{{route('admin.categoryproductoffice.ajaxListing')}}",
            type: "get",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                $("#upper_table_record").html(response);
                $("#ajax-loader").hide();
            }
        });
    });


  
    $(document).on('change', '#upper_page_limit', function() {
        upper_limit_page = this.value;
  
        $("#upper_table_record").html('');
        $("#ajax-loader").show();
        url = "{{route('admin.categoryproduct.ajaxListing')}}";

        setTimeout(function() {
            $.ajax({
                url: url,
                type: "get",
                data: {
                    upper_limit_page: upper_limit_page,
                    eType: eType
                },
                success: function(response) {
                    $("#upper_table_record").html(response);
                    $("#ajax-loader").hide();
                }
            });
            // hideLoader();
        }, 500);
    });
</script>

@endsection