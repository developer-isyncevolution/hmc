@if(count($data) > 0)
@foreach($data as $key => $value)
<tr>

	<td>{{ $value->vOfficeName }}</td>

	<td>
		<div class="d-inline-flex align-items-center">
		<a href="{{route('admin.categoryproductoffice_list',$value->iCustomerId)}}" class="list-label-btn2 edit_permission edit-icon me-4">Products</a>
		<a href="{{route('admin.productstageoffice_list',$value->iCustomerId)}}" class="list-label-btn2 edit_permission edit-icon me-4">Stage</a>
		<a href="{{route('admin.productstageselectoffice_list',$value->iCustomerId)}}" class="list-label-btn2 edit_permission edit-icon me-4">Add ons</a>
		</div>
	</td>

</tr>
@endforeach

@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif