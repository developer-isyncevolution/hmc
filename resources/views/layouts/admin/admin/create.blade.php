@extends('layouts.admin.index')
@section('content')

<div class="main-panel">
  <div class="page-title text-center">
    <h3>
      Add Super Admin
    </h3>
  </div>
  <div class="card">
  @if(isset($admins->iAdminId))
    <div class="card-header border-0 p-0">
      <ul class="c-nav-tab">
        <li>
          
          <a href="{{route('admin.admin.edit',$admins->iAdminId)}}" aria-controls="#tab-{{ $admins->iAdminId }}" class="add-btn btn mx-2 active">Admin</a>
        </li>
        <li>
          <a href="{{route('admin.admin.ChangePassword',$admins->iAdminId)}}" aria-controls="#tab-{{ $admins->iAdminId }}" class="add-btn btn mx-2">Changes password</a>
        </li>
      </ul>
    </div>
    @endif
    <div class="col-lg-12 mx-auto">
      
      <form action="{{route('admin.admin.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
        @csrf
        @if(isset($admins))
        <!-- <div class="card-header py-5">
          <h3 class="card-title align-items-start flex-column">
            
          </h3>
          <ul class="d-flex justify-content-between my-2">
            <li class="d-flex">
             
            </li>
          </ul>
        </div> -->
        @endif
         <input type="hidden" name="id" value="@if(isset($admins)) {{$admins->iAdminId}} @endif">
        {{--<div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Admin name</label>
          <input type="text" class="form-control" id="vName" name="vName" placeholder="Admin name" value="@if(old('vName')!=''){{old('vName')}}@elseif(isset($admins->vName)){{$admins->vName}}@else{{old('vName')}}@endif">
          <div class="text-danger" style="display: none;" id="vName_error">Enter name</div>
        </div> --}}
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>First name</label>
          <input type="text" class="form-control" id="vFirstName" name="vFirstName" placeholder="First name" value="@if(old('vFirstName')!=''){{old('vFirstName')}}@elseif(isset($admins->vFirstName)){{$admins->vFirstName}}@else{{old('vFirstName')}}@endif">
          <div class="text-danger" style="display: none;" id="vFirstName_error">Please enter first name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Last name</label>
          <input type="text" class="form-control" id="vLastName" name="vLastName" placeholder="Last name" value="@if(old('vLastName')!=''){{old('vLastName')}}@elseif(isset($admins->vLastName)){{$admins->vLastName}}@else{{old('vLastName')}}@endif">
          <div class="text-danger" style="display: none;" id="vLastName_error">Please enter last name</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Admin email</label>
          <input type="text" class="form-control" id="email" name="email" placeholder="Admin email" value="@if(old('email')!=''){{old('email')}}@elseif(isset($admins->email)){{$admins->email}}@else{{old('email')}}@endif"  @if (\App\Libraries\General::admin_info()['customerType'] != 'Super Admin') readonly @endif>
          <div class="text-danger" style="display: none;" id="email_error">Enter email</div>
          <div class="text-danger" style="display: none;" id="email_valid_error">Enter valid email</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Admin mobile</label>
          <input type="text" class="form-control" id="vMobile" name="vMobile" placeholder="Admin mobile" value="@if(old('vMobile')!=''){{old('vMobile')}}@elseif(isset($admins->vMobile)){{$admins->vMobile}}@else{{old('vMobile')}}@endif">
          <div class="text-danger" style="display: none;" id="vMobile_error">Enter mobile</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Admin image</label>
          <input class="form-control" type="file" id="vImage" name="vImage" accept="image/*">
          @if(isset($admins))
          <img style="width: 100px;margin-top:10px;" id="img" value="@if(old('vImage') == 'vImage') selected @endif" src="{{asset('uploads/admin/'.$admins->vImage)}}">
          @else
          <img style="width: 100px;margin-top:10px;" id="img" value="" src="{{asset('images/no-image.gif')}}">
          @endif
          <div class="text-danger" style="display: none;" id="vImage_error">Select image</div>
        </div>
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Status</label>
          <select id="eStatus" name="eStatus" >
            <option value="Active" @if(isset($admins)) @if($admins->eStatus == 'Active') selected @endif @endif>Active</option>
            <option value="Inactive" @if(isset($admins)) @if($admins->eStatus == 'Inactive') selected @endif @endif>Inactive</option>
          </select>
        </div>
        @if(!isset($admins->iAdminId))
        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Admin password</label>
          <input type="Password" class="form-control" id="vPassword" name="vPassword" placeholder="Admin password" value="@if(old('vPassword')!=''){{old('vPassword')}}@elseif(isset($admins->vPassword)){{$admins->vPassword}}@else{{old('vPassword')}}@endif">
          <div class="text-danger" style="display: none;" id="vPassword_error">Enter password</div>
        </div>

        <div class="col-xxl-4 col-lg-6 col-md-12">
          <label>Confirm password</label>
          <input type="Password" class="form-control" id="vPassword2" name="vPassword2" placeholder="Confirm password" value="@if(old('vPassword2')!=''){{old('vPassword2')}}@elseif(isset($admins->vPassword2)){{$admins->vPassword}}@else{{old('vPassword2')}}@endif">
          <div class="text-danger" style="display: none;" id="vPassword2_error">Enter confirm password</div>
          <div class="text-danger" id="vPassword2_same_error" style="display: none;">Password should match</div>
        </div>
        @endif
        <div class="col-12 align-self-end d-inline-block text-center">
          <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
          <a href="{{route('admin.admin')}}" class="btn back-btn me-2">Back</a>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="{{asset('admin/assets/js/selectize.min.js')}}"></script>;
<script>
 $('#eStatus').selectize();
  $(document).on('change', '#vImage', function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#img').attr('src', e.target.result);
      };
      reader.readAsDataURL(this.files[0]);
    }
  });
  $(document).on('click', '#submit', function() {
    iAdminId = $("#id").val();
    // vName = $("#vName").val();
    email = $("#email").val();
    vMobile = $("#vMobile").val();
    vImage = $("#vImage").val();
    vPassword = $("#vPassword").val();
    vPassword2 = $("#vPassword2").val();
    vFirstName    = $("#vFirstName").val();
    vLastName     = $("#vLastName").val();

    var error = false;

    // if (vName.length == 0) {
    //   $("#vName_error").show();
    //   error = true;
    // } else {
    //   $("#vName_error").hide();
    // }

    
    if (vFirstName.length == 0) {
      $("#vFirstName_error").show();
      error = true;
    } else {
      $("#vFirstName_error").hide();
    }
    if (vLastName.length == 0) {
      $("#vLastName_error").show();
      error = true;
    } else {
      $("#vLastName_error").hide();
    }

     // Email validtion start
     if (email.length == 0) {
      $("#email_error").text("Please enter email");
      $("#email_error").show();
      $("#email_valid_error").hide();
        error = true;
    }else{
      if(validateEmail(email))
      {
        $("#email_valid_error").hide();
        @if(!isset($admins))
          $.ajax({
            url: "{{route('admin.staff.CheckExistEmail')}}",
            type: "post",
            data: {
                vEmail : $('#email').val(),
                "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
              if(response == 1)
              {
                $('#email_error').show();
                $("#email_error").text("This email alredy exist");
                error = true;
                return false;
              }
              else
              {
                $('#email_error').hide();
                $('#email_valid_error').hide();
                return true;
              }
            }
          }); 
        @endif  
      }else{
        $("#email_valid_error").show();
        $("#email_error").hide();
        error = true;
      }
    }
// Email validtion end

    

    <?php if (!isset($admins->iAdminId)) { ?>
      if (vImage.length == 0) {
        $("#vImage_error").show();
        error = true;
      } else {
        $("#vImage_error").hide();
      }
    <?php } ?>
    <?php
    if (!isset($admins->iAdminId)) { ?>
      if (vPassword.length == 0) {
        $("#vPassword_error").show();
        error = true;
      } else {
        $("#vPassword_error").hide();
      }

      if (vPassword2.length == 0) {
        $("#vPassword2_error").show();
        error = true;
      } else {
        $("#vPassword2_error").hide();
      }

      if (vPassword.length != 0 && vPassword2.length != 0) {
        if (vPassword != vPassword2) {
          $("#vPassword2_same_error").show();
          return true;
        } else {
          $("#vPassword2_same_error").hide();
        }
      } else {
        $("#vPassword2_same_error").hide();
      }

    <?php }
    ?>
    setTimeout(function() {
      if (error == true) {
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });

  function validateEmail(email) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,10})(\]?)$/;
    if (filter.test(email)) {
      return true;
    } else {
      return false;
    }
  }
  $('document').ready(function(){
    var mobile = [{ "mask": "### ### ####"}, { "mask": "### ### ####"}];
    $('#vMobile').inputmask({ 
        mask: mobile, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
  });
</script>
@endsection