@if(count($data) > 0)

@foreach($data as $key => $value)

		<tr>
			<td>
				<div class="form-check form-check-sm form-check-custom form-check-solid">
					<input id="Schedule_ID_{{$value->iScheduleId}}" type="checkbox" name="Schedule_ID[]" class="form-check-input widget-9-check checkboxall" value="{{$value->iScheduleId}}">
					<label for="Schedule_ID_{{$value->iScheduleId}}">&nbsp;</label>
				</div>
			</td>
			<td>@if($value->iYear==0) All @else {{$value->iYear}} @endif</td>
			<td>
				@if($value->iMonth==0) All
				@elseif($value->iMonth==1) January
				@elseif($value->iMonth==2) February
				@elseif($value->iMonth==3) March
				@elseif($value->iMonth==4) April
				@elseif($value->iMonth==5) May
				@elseif($value->iMonth==6) June
				@elseif($value->iMonth==7) July
				@elseif($value->iMonth==8) August
				@elseif($value->iMonth==9) September
				@elseif($value->iMonth==10) October
				@elseif($value->iMonth==11) November
				@elseif($value->iMonth==12) December
				@endif
			</td>
			<td>{{$value->iDay}}</td>
			<td>
				@if($value->iWeek==0) All
				@elseif($value->iMonth==1) Monday
				@elseif($value->iMonth==2) Tuesday
				@elseif($value->iMonth==3) Wednesday
				@elseif($value->iMonth==4) Thursday
				@elseif($value->iMonth==5) Friday
				@elseif($value->iMonth==6) Saturday
				@elseif($value->iMonth==7) Sunday
				@endif
			</td>
			<td>@php $opentime = date('h:i A', strtotime($value->tiOpen));
				@endphp
			{{$opentime}}</td>
			<td>@php $closetime = date('h:i A', strtotime($value->tiClose));
				@endphp
			{{$closetime}}</td>
			<td>{{$value->vTitle}}</td>
			<td>
				<span class="badge badge-light-success">
					{{$value->eStatus}}
				</span>
			</td>
			<td>
				<div class="d-inline-flex align-items-center">
					@if(\App\Libraries\General::check_permission('','eEdit') == true)
					<a href="{{route('admin.schedule.edit',$value->iScheduleId)}}" class="btn-icon me-4  edit_permission edit-icon">
						<i class="fad fa-pencil"></i>
					</a>
					@endif
					@if(\App\Libraries\General::check_permission('','eDelete') == true)
					<a href="javascript:;" id="delete" data-id="{{$value->iScheduleId}}" class="btn-icon delete-icon delete_permission me-4">
					<i class="fad fa-trash-alt"></i>
					</a>
					@endif
					{{-- <a href="{{route('admin.Gradenumber',$value->iScheduleId)}}" class="list-label-btn2 list_permission me-4">
						Grade Number
					</a> --}}
				</div>
			</td>
		</tr>
@endforeach
		<tr>
			<td  class="text-center border-right-0">
				@if(\App\Libraries\General::check_permission('','eDelete') == true)
				<a href="javascript:;" title="Multiple delete" id="delete_btn" class="btn-icon  delete_permission delete-icon">
				<i class="fad fa-trash-alt"></i>
				</a>
				@endif
			</td>
			<td class="border-0" colspan="2">
				<div class="d-flex">
					{!! $paging !!}
				</div>
			</td>
			{{-- <td class="border-0"></td>
			<td colspan="0" class="text-center border-left-0 border-right-0">{!! $paging !!}</td> --}}
			<td colspan="7"  class="text-end border-left-0">
				{{-- {!! $start !!} to {!! $limit !!} of {{count($data)}}  --}}
				<select class="w-100px show-drop" id ="page_limit">
					<option @if (isset(Request()->limit_page) && Request()->limit_page == 10)
						selected
						@endif value="10">10</option>
						<option @if (isset(Request()->limit_page) && Request()->limit_page == 20)
						selected
						@endif value="20">20</option>
						<option @if (isset(Request()->limit_page) && Request()->limit_page == 50)
							selected
						@endif value="50">50</option>
						<option @if (isset(Request()->limit_page) && Request()->limit_page == 100)
							selected
						@endif value="100">100</option>
				</select>
			</td>
		</tr>
@else
<tr class="text-center">
	<td colspan="10">No Record Found</td>
</tr>
@endif