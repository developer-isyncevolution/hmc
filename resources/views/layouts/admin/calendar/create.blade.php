@extends('layouts.admin.index')
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<div class="main-panel">

  <div class="page-title text-center">
    <h3>
      {{isset($schedule->iScheduleId) ? 'Edit' : 'Add'}} Schedule
    </h3>
</div>


  <div class="card">
    <div class="col-lg-12 mx-auto">         
        <form action="{{route('admin.schedule.store')}}" id="frm" class="row g-5 add-product mt-0" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="@if(isset($schedule)) {{$schedule->iScheduleId}} @endif">
            <div class="col-xxl-4 col-lg-6 col-md-12">
            <label>Customer</label>
            <select name="iCustomerId" id="iCustomerId" class="form-control">
              <option value="none">Select customer</option>
              @foreach($customer as $key => $customers)
              <option value="{{$customers->iCustomerId}}" @if(isset($schedule)){{$customers->iCustomerId == $schedule->iCustomerId  ? 'selected' : ''}} @endif>{{$customers->vOfficeName}}</option>
              @endforeach
            </select>
            <div class="text-danger" style="display: none;" id="iCustomerId_error">Please select customer</div>
        </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label >YEAR</label>
                <select id="iYear" name="iYear" class="form-control">
                  <option value="0">All</option>
                    @for($x = 2022; $x <= 2030; $x++)
                    <option value="{{ $x; }}" @if(isset($schedule))   @if($schedule->iYear == $x) selected @endif @endif>{{ $x; }}</option>
                    @endfor
                </select>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>MONTH</label>
                <select id="eMonth" name="eMonth" class="form-control">
                    <option value="All" @if(isset($schedule))   @if($schedule->eMonth == "All") selected @endif @endif>All</option>
                    <option value="January" @if(isset($schedule)) @if($schedule->eMonth == "January") selected @endif @endif>January</option>
                    <option value="February" @if(isset($schedule)) @if($schedule->eMonth == "February") selected @endif @endif>February</option>
                    <option value="March" @if(isset($schedule)) @if($schedule->eMonth == "March") selected @endif @endif>March</option>
                    <option value="April" @if(isset($schedule)) @if($schedule->eMonth == "April") selected @endif @endif>April</option>
                    <option value="May" @if(isset($schedule)) @if($schedule->eMonth == "May") selected @endif @endif>May</option>
                    <option value="June" @if(isset($schedule)) @if($schedule->eMonth == "June") selected @endif @endif>June</option>
                    <option value="July" @if(isset($schedule)) @if($schedule->eMonth == "July") selected @endif @endif>July</option>
                    <option value="August" @if(isset($schedule)) @if($schedule->eMonth == "August") selected @endif @endif>August</option>
                    <option value="September" @if(isset($schedule)) @if($schedule->eMonth == "September") selected @endif @endif>September</option>
                    <option value="October" @if(isset($schedule)) @if($schedule->eMonth == "October") selected @endif @endif>October</option>
                    <option value="November" @if(isset($schedule)) @if($schedule->eMonth == "November") selected @endif @endif>November</option>
                    <option value="December" @if(isset($schedule)) @if($schedule->eMonth == "December") selected @endif @endif>December</option>
                </select>
            </div>
            
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label >DAY</label>
                <select id="iDay" name="iDay" class="form-control">
                    @for($x = 01; $x <= 31; $x++)
                    <option value="{{ $x; }}" @if(isset($schedule))   @if($schedule->iDay == $x) selected @endif @endif>{{ $x; }}</option>
                    @endfor
                </select>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>DAY OF THE WEEK</label>
                <select id="eWeek" name="eWeek" class="form-control">
                    <option value="All" @if(isset($schedule))   @if($schedule->eWeek == "All") selected @endif @endif>All</option>
                    <option value="Sunday" @if(isset($schedule)) @if($schedule->eWeek == "Sunday") selected @endif @endif>Sunday</option>
                    <option value="Monday" @if(isset($schedule)) @if($schedule->eWeek == "Monday") selected @endif @endif>Monday</option>
                    <option value="Tuesday" @if(isset($schedule)) @if($schedule->eWeek == "Tuesday") selected @endif @endif>Tuesday</option>
                    <option value="Wednesday" @if(isset($schedule)) @if($schedule->eWeek == "Wednesday") selected @endif @endif>Wednesday</option>
                    <option value="Thursday" @if(isset($schedule)) @if($schedule->eWeek == "Thursday") selected @endif @endif>Thursday</option>
                    <option value="Friday" @if(isset($schedule)) @if($schedule->eWeek == "Friday") selected @endif @endif>Friday</option>
                    <option value="Saturday" @if(isset($schedule)) @if($schedule->eWeek == "Saturday") selected @endif @endif>Saturday</option>
                </select>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>WEEK MONTH</label><span> *</span>
                  <select id="eWeekMonth" name="eWeekMonth" class="form-control">
                     <option value="">Select Week Month</option>
                      <option value="All" @if(isset($schedule))@if($schedule->eWeekMonth == "All") selected @endif @endif>All</option>
                      <option value="First Week" @if(isset($schedule)) @if($schedule->eWeekMonth == "First Week") selected @endif @endif>First Week</option>
                      <option value="Second Week" @if(isset($schedule)) @if($schedule->eWeekMonth == "Second Week") selected @endif @endif>Second Week</option>
                      <option value="Third Week" @if(isset($schedule)) @if($schedule->eWeekMonth == "Third Week") selected @endif @endif>Third Week</option>
                      <option value="Fourth Week" @if(isset($schedule)) @if($schedule->eWeekMonth == "Fourth Week") selected @endif @endif>Fourth Week</option>
                      <option value="Fifth Week" @if(isset($schedule)) @if($schedule->eWeekMonth == "Fifth Week") selected @endif @endif>Fifth Week</option>
                  </select>
                  <div class="text-danger" style="display: none;" id="eWeekMonth_error">Please Select Week Month</div>
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>DAY TITLE</label><span> *</span>
                <input type="text" class="form-control" id="vTitle" name="vTitle" placeholder="Day Title" value="@if(old('vTitle')!=''){{old('vTitle')}}@elseif(isset($schedule->vTitle)){{$schedule->vTitle}}@else{{old('vTitle')}}@endif">
                <div class="text-danger" style="display: none;" id="vTitle_error">Please enter Day Title</div>
                
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>FROM HOUR</label><span> *</span>
                <input type="time" class="form-control" id="tiOpen" name="tiOpen" placeholder="FROM HOUR" value="@if(old('tiOpen')!=''){{old('tiOpen')}}@elseif(isset($schedule->tiOpen)){{$schedule->tiOpen}}@else{{old('tiOpen')}}@endif">
                <div class="text-danger" style="display: none;" id="tiOpen_error">Please enter From Hour</div>
                
            </div>
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>TO HOUR</label><span> *</span>
                <input type="time" class="form-control" id="tiClose" name="tiClose" placeholder="FROM HOUR" value="@if(old('tiClose')!=''){{old('tiClose')}}@elseif(isset($schedule->tiClose)){{$schedule->tiClose}}@else{{old('tiClose')}}@endif">
                <div class="text-danger" style="display: none;" id="tiClose_error">Please enter To Hour</div>
                
            </div>
            
            <div class="col-xxl-4 col-lg-6 col-md-12">
                <label>Status</label>
                <select id="eStatus" name="eStatus" class="form-control">
                    <option value="OPEN" @if(isset($schedule)) @if($schedule->eStatus == 'OPEN') selected @endif @endif>OPEN</option>
                    <option value="CLOSED" @if(isset($schedule)) @if($schedule->eStatus == 'CLOSED') selected @endif @endif>CLOSED</option>
                </select>
                <div class="mt-1">
                    @error('eStatus')
                        <code>{{ $message }}</code>
                    @enderror
                </div>
            </div>       
            <div class="col-12 align-self-end d-inline-block ">
                <a type="submit" id="submit" class="btn submit-btn me-2">Submit</a>
                <a href="{{route('admin.schedule')}}" class="btn back-btn me-2">Back</a>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

<script>
    // $('#iCustomerId').select2({
    //     width: '100%',
    //     placeholder: "Select customer",
    //     allowClear: true
    // });
    $('#iCustomerId').selectize();
  
  $(document).on('click','#submit',function()
  {
    iScheduleId             = $("#id").val();
    iCustomerId             = $("#iCustomerId").val();
    vTitle                  = $("#vTitle").val();
    tiOpen                  = $("#tiOpen").val();
    tiClose                 = $("#tiClose").val();
    eWeekMonth              = $("#eWeekMonth").val();

    var error = false;
    if (iCustomerId == 'none') {
      $("#iCustomerId_error").show();
      error = true;
    } else {
      $("#iCustomerId_error").hide();
    }
    if(eWeekMonth.length == 0){
      $("#eWeekMonth_error").show();
      error = true;
    } else {
      $("#eWeekMonth_error").hide();
    }
    if(vTitle.length == 0){
      $("#vTitle_error").show();
      error = true;
    } else {
      $("#vTitle_error").hide();
    }
    if(tiOpen.length == 0){
      $("#tiOpen_error").show();
      error = true;
    } else {
      $("#tiOpen_error").hide();
    }
    if(tiClose.length == 0){
      $("#tiClose_error").show();
      error = true;
    } else {
      $("#tiClose_error").hide();
    }
    setTimeout(function(){
      if(error == true){
        return false;
      } else {
        $("#frm").submit();
        return true;
      }
    }, 1000);

  });
    </script>
@endsection
