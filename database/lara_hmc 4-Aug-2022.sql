-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 04, 2022 at 09:26 AM
-- Server version: 8.0.28
-- PHP Version: 8.0.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lara_hmc`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon_category`
--

CREATE TABLE `addon_category` (
  `iAddCategoryId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vCategoryName` varchar(255) NOT NULL,
  `vSequenc` int NOT NULL,
  `dtAddedDate` datetime NOT NULL,
  `dtUpdatedDate` datetime NOT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL,
  `eType` enum('Upper','Lower','Both') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `addon_category`
--

INSERT INTO `addon_category` (`iAddCategoryId`, `iCustomerId`, `vCategoryName`, `vSequenc`, `dtAddedDate`, `dtUpdatedDate`, `eStatus`, `eType`) VALUES
(3, 11, 'Meshes', 10, '2022-04-12 03:56:47', '2022-08-01 22:48:58', 'Active', 'Both'),
(5, NULL, 'Implants', 15, '2022-04-23 14:28:49', '2022-04-23 14:28:49', 'Active', 'Upper'),
(6, NULL, 'Implants', 15, '2022-04-23 14:29:00', '2022-04-23 14:29:00', 'Active', 'Lower'),
(8, NULL, 'Gold teeth', 35, '2022-04-27 03:49:44', '2022-05-01 20:06:10', 'Active', 'Both'),
(19, 11, 'Clasps', 5, '2022-05-01 20:05:33', '2022-08-01 22:48:53', 'Active', 'Both'),
(25, NULL, 'category add on test', 20, '2022-05-27 04:14:31', '2022-05-27 04:14:31', 'Active', 'Both');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `iAdminId` int NOT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `vImage` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `vMobile` varchar(255) DEFAULT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`iAdminId`, `vName`, `vImage`, `email`, `password`, `vMobile`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(1, 'Horacio', '1641794640.offers-1.png', 'admin@mailinator.com', '0192023a7bbd73250516f069df18b500', '9723486463', 'Active', '2022-07-22 05:08:51', '2022-01-10 06:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `iBannerId` int NOT NULL,
  `vTitle` varchar(255) DEFAULT NULL,
  `tDescription` varchar(255) DEFAULT NULL,
  `vLocation` varchar(255) DEFAULT NULL,
  `vImage` varchar(255) DEFAULT NULL,
  `dtAddedDate` datetime NOT NULL,
  `dtUpdatedDate` datetime NOT NULL,
  `eStatus` enum('Active','Inactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`iBannerId`, `vTitle`, `tDescription`, `vLocation`, `vImage`, `dtAddedDate`, `dtUpdatedDate`, `eStatus`) VALUES
(10, 'demo', '<p>demo</p>', 'demo', '1645100153.top-banner.jpg', '2022-02-23 12:53:54', '2022-02-17 00:00:00', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `calendar_event`
--

CREATE TABLE `calendar_event` (
  `iEventId` int NOT NULL,
  `iCustomerId` int NOT NULL,
  `iOfficeId` int NOT NULL,
  `vTitle` varchar(255) NOT NULL,
  `tDescription` text NOT NULL,
  `vColor` varchar(255) NOT NULL,
  `dtStartDate` date NOT NULL,
  `tiStartTime` time NOT NULL,
  `vCreatedBy` varchar(255) DEFAULT NULL,
  `vUpdatedBy` varchar(255) DEFAULT NULL,
  `dtAddedDate` datetime NOT NULL,
  `dtUpdatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `call`
--

CREATE TABLE `call` (
  `iCallId` int NOT NULL,
  `iSlipId` int DEFAULT NULL,
  `iStageId` int NOT NULL,
  `vStageName` varchar(255) NOT NULL,
  `vSlipNumber` varchar(255) NOT NULL,
  `iCaseId` int DEFAULT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `vCallTaken` varchar(255) NOT NULL,
  `dDate` date DEFAULT NULL,
  `tTime` time DEFAULT NULL,
  `tDescription` text,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `case`
--

CREATE TABLE `case` (
  `iCaseId` int NOT NULL,
  `iOfficeId` int NOT NULL,
  `iDoctorId` int NOT NULL,
  `vPatientName` varchar(255) DEFAULT NULL,
  `iCasepanId` int DEFAULT NULL,
  `vCasePanNumber` varchar(255) DEFAULT NULL,
  `vCaseNumber` varchar(255) DEFAULT NULL,
  `iCreatedById` int DEFAULT NULL,
  `vCreatedByName` varchar(255) DEFAULT NULL,
  `eCreatedByType` enum('Admin','Customer','Office','Doctor','User') NOT NULL DEFAULT 'Admin',
  `eStatus` enum('Draft','On Process','Canceled','Finished','On Hold') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Draft',
  `vLocation` varchar(255) DEFAULT NULL,
  `dtAddedDate` datetime NOT NULL,
  `dtUpdatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `case`
--

INSERT INTO `case` (`iCaseId`, `iOfficeId`, `iDoctorId`, `vPatientName`, `iCasepanId`, `vCasePanNumber`, `vCaseNumber`, `iCreatedById`, `vCreatedByName`, `eCreatedByType`, `eStatus`, `vLocation`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(1, 11, 34, 'abbas', 1, 'A01', '0001', 11, 'Horacio', 'Admin', 'On Process', 'On route to the lab', '2022-07-25 22:41:26', '2022-07-29 11:41:09'),
(2, 11, 34, 'new test', 1, 'A02', '0002', 11, 'Horacio', 'Admin', 'On Process', 'In office ready to pickup', '2022-07-26 06:43:39', '2022-07-26 06:43:39'),
(3, 9, 25, 'abbas', 1, 'A03', '0003', 11, 'Horacio', 'Admin', 'On Process', 'In office ready to pickup', '2022-07-26 06:46:37', '2022-07-26 06:46:37'),
(4, 9, 25, 'abbas', 1, 'A04', '0003', 11, 'Horacio', 'Admin', 'On Process', 'In office ready to pickup', '2022-07-26 06:46:57', '2022-07-26 06:46:57'),
(5, 9, 25, 'abbas', NULL, NULL, '0003', 11, 'Horacio', 'Admin', 'On Process', 'In office ready to pickup', '2022-07-26 06:47:28', '2022-07-26 06:47:28'),
(6, 9, 25, 'abbas', NULL, NULL, '0003', 11, 'Horacio', 'Admin', 'On Process', 'In office ready to pickup', '2022-07-26 06:47:40', '2022-07-26 06:47:40'),
(7, 9, 25, 'abbas', NULL, NULL, '0003', 11, 'Horacio', 'Admin', 'On Process', 'In office ready to pickup', '2022-07-26 06:47:55', '2022-07-26 06:47:55'),
(8, 11, 34, 'qasim', 1, 'A05', '0008', 11, 'Horacio', 'Admin', 'On Process', 'In office', '2022-07-29 23:50:49', '2022-07-30 12:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `casepan`
--

CREATE TABLE `casepan` (
  `iCasepanId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `vColor` varchar(255) DEFAULT NULL,
  `iQuantity` int DEFAULT NULL,
  `iSequence` int DEFAULT NULL,
  `eStatus` enum('Active','Inactive','Stand By','Suspended') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `casepannumber`
--

CREATE TABLE `casepannumber` (
  `iCasepanNumberId` int NOT NULL,
  `iCasepanId` int DEFAULT NULL,
  `vNumber` varchar(255) DEFAULT NULL,
  `eStatus` enum('Active','Inactive','Stand By','Suspended') NOT NULL DEFAULT 'Active',
  `eUse` enum('Yes','No') NOT NULL DEFAULT 'No',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `iCategoryId` int NOT NULL,
  `iCasepanId` int DEFAULT NULL,
  `iSequence` int DEFAULT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `tPickTime` time DEFAULT NULL,
  `tDeliveryTime` time DEFAULT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `eType` enum('Upper','Lower','Both') DEFAULT NULL,
  `iCustomerId` int DEFAULT NULL,
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`iCategoryId`, `iCasepanId`, `iSequence`, `vName`, `tPickTime`, `tDeliveryTime`, `eStatus`, `eType`, `iCustomerId`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(13, 1, 10, 'Partial Dentures', '11:00:00', '04:00:00', 'Active', 'Both', 11, '2022-08-01 05:43:54', '2022-03-10 13:06:48'),
(16, 1, 5, 'Full Dentures', '11:00:00', '16:00:00', 'Active', 'Both', 11, '2022-08-01 05:44:02', '2022-03-10 13:08:36'),
(17, 3, 15, 'Same day services', '10:00:00', '16:00:00', 'Active', 'Both', 11, '2022-08-01 05:44:06', '2022-03-14 06:03:05'),
(18, 4, 20, 'Guards', '11:00:00', '15:30:00', 'Active', 'Both', NULL, '2022-03-23 05:15:49', '2022-03-23 05:15:49'),
(19, 3, 15, 'Flippers', '10:00:00', '16:00:00', 'Active', 'Both', NULL, '2022-05-04 04:39:08', '2022-05-01 19:26:07'),
(22, 5, 1, 'Krupesh Test Category', '00:20:00', '16:00:00', 'Active', 'Both', 11, '2022-08-01 05:28:28', '2022-05-23 07:26:45');

-- --------------------------------------------------------

--
-- Table structure for table `categoryproduct`
--

CREATE TABLE `categoryproduct` (
  `iCategoryProductId` int NOT NULL,
  `iCategoryId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vCode` varchar(255) DEFAULT NULL,
  `iSequence` int DEFAULT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `vPrice` varchar(255) DEFAULT NULL,
  `vImage` varchar(255) DEFAULT NULL,
  `vLowerImage` varchar(255) DEFAULT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `eType` enum('Upper','Lower','Both') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `eClaspAdd` enum('Yes','No') DEFAULT NULL,
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `categoryproduct`
--

INSERT INTO `categoryproduct` (`iCategoryProductId`, `iCategoryId`, `iCustomerId`, `vCode`, `iSequence`, `vName`, `vPrice`, `vImage`, `vLowerImage`, `eStatus`, `eType`, `eClaspAdd`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(1, 13, NULL, 'Ab123', 5, 'Hmc', '123456', '1648129599.1643107519.jpeg', NULL, 'Active', 'Upper', 'Yes', '2022-03-24 13:46:39', '2022-03-24 13:46:39'),
(2, 13, NULL, 'MF', 5, 'Metal Frame Acrylic', '240', '1648215413.2022-02-27 (25).png', NULL, 'Active', 'Both', 'Yes', '2022-03-25 13:36:53', '2022-03-25 13:36:53'),
(3, 16, NULL, 'FD', 10, 'Full denture acrylic', '220', '1648231410.2BE71388-8503-44BC-B449-A79CA666B3B7.jpeg', NULL, 'Active', 'Both', 'Yes', '2022-03-25 06:03:30', '2022-03-25 18:02:21'),
(4, 17, NULL, 'HR', 5, 'Hard Reline', '70', '1649820988.2022-02-28.png', '1649820988.2022-02-28.png', 'Active', 'Both', 'No', '2022-04-13 01:26:00', '2022-04-13 03:36:28'),
(5, 13, NULL, 'FX', 10, 'Flexible Partial', '240', NULL, NULL, 'Active', 'Both', 'No', '2022-04-13 13:25:36', '2022-04-13 13:25:36'),
(6, 17, NULL, 'BKD', 10, 'Broken full denture  Acrylic', '50', NULL, NULL, 'Active', 'Both', 'No', '2022-05-06 05:14:17', '2022-04-14 04:30:49'),
(7, 16, NULL, 'IFD', 15, 'Immediate full denture', '220', NULL, NULL, 'Active', 'Both', 'Yes', '2022-05-01 17:54:01', '2022-05-01 17:54:01'),
(8, 19, NULL, 'FP1', 5, 'Flipper 1 tooth', '50', NULL, NULL, 'Active', 'Both', 'Yes', '2022-05-01 19:27:16', '2022-05-01 19:27:16'),
(9, 19, NULL, 'FP2', 10, 'Flipper 2 teeth', '70', NULL, NULL, 'Active', 'Both', 'Yes', '2022-05-01 19:28:07', '2022-05-01 19:28:07'),
(10, 19, NULL, 'FP3', 15, 'Flipper 3 teeth', '80', NULL, NULL, 'Active', 'Both', 'Yes', '2022-05-01 19:28:53', '2022-05-01 19:28:53'),
(11, 17, NULL, 'AOT', 15, 'Add one tooth to acrylic', '50', NULL, NULL, 'Active', 'Both', 'Yes', '2022-05-03 06:32:57', '2022-05-03 06:32:57'),
(12, 16, NULL, 'ISD', 20, 'Implant supported full denture', '250', NULL, NULL, 'Active', 'Both', 'Yes', '2022-05-24 01:33:16', '2022-05-14 04:50:17'),
(14, 22, 11, 'Product1', 1, 'Product 1 KT', '10', '1653299413.banner.jpg', '1653290870.Screenshot from 2022-05-09 09-50-46.png', 'Active', 'Both', 'Yes', '2022-08-01 10:12:16', '2022-05-23 07:27:50'),
(23, 25, NULL, 'TC', 25, 'test product', '200', '1653567479.Cake PHP Developers.jpg', '1653567479.Laravel.jpg', 'Active', 'Both', 'No', '2022-05-26 12:18:57', '2022-05-26 12:17:59'),
(24, 27, NULL, 'P01', 20, 'product 1', '200', NULL, NULL, 'Active', 'Both', 'Yes', '2022-05-27 09:56:35', '2022-05-27 09:56:35'),
(25, 28, NULL, 'P02', 20, 'product 2', '300', '1653645568.Laravel.jpg', NULL, 'Active', 'Both', 'No', '2022-05-27 09:59:28', '2022-05-27 09:59:28'),
(26, 33, NULL, 'P02', 90, 'product 2', '60', '1653654160.Laravel.jpg', '1653654160.Cake PHP Developers.jpg', 'Active', 'Both', 'Yes', '2022-05-27 12:22:40', '2022-05-27 12:22:40');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `iCountryId` int NOT NULL,
  `vCountry` varchar(64) NOT NULL,
  `vCountryCode` char(3) NOT NULL,
  `vCountryISDCode` char(3) DEFAULT NULL,
  `eDefault` enum('Yes','No') DEFAULT NULL,
  `eStatus` enum('Active','Inactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`iCountryId`, `vCountry`, `vCountryCode`, `vCountryISDCode`, `eDefault`, `eStatus`) VALUES
(1, 'Afghanistan', 'AF', '+44', '', 'Inactive'),
(3, 'Algeria', 'DZ', '+44', NULL, 'Inactive'),
(4, 'American Samoa', 'AS', '+44', '', 'Inactive'),
(5, 'Andorra', 'AD', '+44', NULL, 'Inactive'),
(6, 'Angola', 'AO', '+44', NULL, 'Inactive'),
(7, 'Anguilla', 'AI', '+44', NULL, 'Inactive'),
(8, 'Antarctica', 'AQ', '+44', NULL, 'Inactive'),
(9, 'Antigua and Barbuda', 'AG', '+44', NULL, 'Inactive'),
(10, 'Argentina', 'AR', '+44', NULL, 'Inactive'),
(11, 'Armenia', 'AM', '+44', NULL, 'Inactive'),
(12, 'Aruba', 'AW', '+44', NULL, 'Inactive'),
(13, 'Australia', 'AU', '+44', NULL, 'Inactive'),
(14, 'Austria', 'AT', '+44', NULL, 'Inactive'),
(15, 'Azerbaijan', 'AZ', '+44', NULL, 'Inactive'),
(16, 'Bahamas', 'BS', '+44', NULL, 'Inactive'),
(17, 'Bahrain', 'BH', '+44', NULL, 'Inactive'),
(18, 'Bangladesh', 'BD', '+44', NULL, 'Inactive'),
(19, 'Barbados', 'BB', '+44', NULL, 'Inactive'),
(20, 'Belarus', 'BY', '+44', NULL, 'Inactive'),
(21, 'Belgium', 'BE', '+44', NULL, 'Inactive'),
(22, 'Belize', 'BZ', '+44', NULL, 'Inactive'),
(23, 'Benin', 'BJ', '+44', NULL, 'Inactive'),
(24, 'Bermuda', 'BM', '+44', NULL, 'Inactive'),
(25, 'Bhutan', 'BT', '+44', NULL, 'Inactive'),
(26, 'Bolivia', 'BO', '+44', NULL, 'Inactive'),
(27, 'Bosnia and Herzegowina', 'BA', '+44', NULL, 'Inactive'),
(28, 'Botswana', 'BW', '+44', NULL, 'Inactive'),
(29, 'Bouvet Island', 'BV', '+44', NULL, 'Inactive'),
(30, 'Brazil', 'BR', '+44', NULL, 'Inactive'),
(31, 'British Indian Ocean Territory', 'IO', '+44', NULL, 'Inactive'),
(32, 'Brunei Darussalam', 'BN', '+44', NULL, 'Inactive'),
(33, 'Bulgaria', 'BG', '+44', NULL, 'Inactive'),
(34, 'Burkina Faso', 'BF', '+44', NULL, 'Inactive'),
(35, 'Burundi', 'BI', '+44', NULL, 'Inactive'),
(36, 'Cambodia', 'KH', '+44', NULL, 'Inactive'),
(37, 'Cameroon', 'CM', '+44', NULL, 'Inactive'),
(38, 'Canada', 'CA', '+44', NULL, 'Inactive'),
(39, 'Cape Verde', 'CV', '+44', NULL, 'Inactive'),
(40, 'Cayman Islands', 'KY', '+44', NULL, 'Inactive'),
(41, 'Central African Republic', 'CF', '+44', NULL, 'Inactive'),
(42, 'Chad', 'TD', '+44', NULL, 'Inactive'),
(43, 'Chile', 'CL', '+44', NULL, 'Inactive'),
(44, 'China', 'CN', '+44', NULL, 'Inactive'),
(45, 'Christmas Island', 'CX', '+44', NULL, 'Inactive'),
(46, 'Cocos (Keeling) Islands', 'CC', '+44', NULL, 'Inactive'),
(47, 'Colombia', 'CO', '+44', NULL, 'Inactive'),
(48, 'Comoros', 'KM', '+44', NULL, 'Inactive'),
(49, 'Congo', 'CG', '+44', NULL, 'Inactive'),
(50, 'Cook Islands', 'CK', '+44', NULL, 'Inactive'),
(51, 'Costa Rica', 'CR', '+44', NULL, 'Inactive'),
(52, 'Cote D\'Ivoire', 'CI', '+44', NULL, 'Inactive'),
(53, 'Croatia', 'HR', '+44', NULL, 'Inactive'),
(54, 'Cuba', 'CU', '+44', NULL, 'Inactive'),
(55, 'Cyprus', 'CY', '+44', NULL, 'Inactive'),
(56, 'Czech Republic', 'CZ', '+44', NULL, 'Inactive'),
(57, 'Denmark', 'DK', '+44', NULL, 'Inactive'),
(58, 'Djibouti', 'DJ', '+44', NULL, 'Inactive'),
(59, 'Dominica', 'DM', '+44', NULL, 'Inactive'),
(60, 'Dominican Republic', 'DO', '+44', NULL, 'Inactive'),
(61, 'East Timor', 'TP', '+44', NULL, 'Inactive'),
(62, 'Ecuador', 'EC', '+44', NULL, 'Inactive'),
(63, 'Egypt', 'EG', '+44', NULL, 'Inactive'),
(64, 'El Salvador', 'SV', '+44', NULL, 'Inactive'),
(65, 'Equatorial Guinea', 'GQ', '+44', NULL, 'Inactive'),
(66, 'Eritrea', 'ER', '+44', NULL, 'Inactive'),
(67, 'Estonia', 'EE', '+44', NULL, 'Inactive'),
(68, 'Ethiopia', 'ET', '+44', NULL, 'Inactive'),
(69, 'Falkland Islands (Malvinas)', 'FK', '+44', NULL, 'Inactive'),
(70, 'Faroe Islands', 'FO', '+44', NULL, 'Inactive'),
(71, 'Fiji', 'FJ', '+44', NULL, 'Inactive'),
(72, 'Finland', 'FI', '+44', NULL, 'Inactive'),
(73, 'France', 'FR', '+44', NULL, 'Inactive'),
(74, 'France, Metropolitan', 'FX', '+44', NULL, 'Inactive'),
(75, 'French Guiana', 'GF', '+44', NULL, 'Inactive'),
(76, 'French Polynesia', 'PF', '+44', NULL, 'Inactive'),
(77, 'French Southern Territories', 'TF', '+44', NULL, 'Inactive'),
(78, 'Gabon', 'GA', '+44', NULL, 'Inactive'),
(79, 'Gambia', 'GM', '+44', NULL, 'Inactive'),
(80, 'Georgia', 'GE', '+44', NULL, 'Inactive'),
(81, 'Germany', 'DE', '+44', NULL, 'Inactive'),
(82, 'Ghana', 'GH', '+44', NULL, 'Inactive'),
(83, 'Gibraltar', 'GI', '+44', NULL, 'Inactive'),
(84, 'Greece', 'GR', '+44', NULL, 'Inactive'),
(85, 'Greenland', 'GL', '+44', NULL, 'Inactive'),
(86, 'Grenada', 'GD', '+44', NULL, 'Inactive'),
(87, 'Guadeloupe', 'GP', '+44', NULL, 'Inactive'),
(88, 'Guam', 'GU', '+44', NULL, 'Inactive'),
(89, 'Guatemala', 'GT', '+44', NULL, 'Inactive'),
(90, 'Guinea', 'GN', '+44', NULL, 'Inactive'),
(91, 'Guinea-bissau', 'GW', '+44', NULL, 'Inactive'),
(92, 'Guyana', 'GY', '+44', NULL, 'Inactive'),
(93, 'Haiti', 'HT', '+44', NULL, 'Inactive'),
(94, 'Heard and Mc Donald Islands', 'HM', '+44', NULL, 'Inactive'),
(95, 'Honduras', 'HN', '+44', NULL, 'Inactive'),
(96, 'Hong Kong', 'HK', '+44', NULL, 'Inactive'),
(97, 'Hungary', 'HU', '+44', NULL, 'Inactive'),
(98, 'Iceland', 'IS', '+44', NULL, 'Inactive'),
(99, 'India', 'IN', '+91', '', 'Active'),
(100, 'Indonesia', 'ID', '+44', NULL, 'Inactive'),
(101, 'Iran (Islamic Republic of)', 'IR', '+44', NULL, 'Inactive'),
(102, 'Iraq', 'IQ', '+44', NULL, 'Inactive'),
(103, 'Ireland', 'IE', '+44', NULL, 'Active'),
(104, 'Israel', 'IL', '+44', NULL, 'Inactive'),
(105, 'Italy', 'IT', '+44', NULL, 'Inactive'),
(106, 'Jamaica', 'JM', '+44', NULL, 'Inactive'),
(107, 'Japan', 'JP', '+44', NULL, 'Inactive'),
(108, 'Jordan', 'JO', '+44', NULL, 'Inactive'),
(109, 'Kazakhstan', 'KZ', '+44', NULL, 'Inactive'),
(110, 'Kenya', 'KE', '+44', NULL, 'Inactive'),
(111, 'Kiribati', 'KI', '+44', NULL, 'Inactive'),
(112, 'Korea, Democratic People\'s Republic of', 'KP', '+44', NULL, 'Inactive'),
(113, 'Korea, Republic of', 'KR', '+44', NULL, 'Inactive'),
(114, 'Kuwait', 'KW', '+44', NULL, 'Inactive'),
(115, 'Kyrgyzstan', 'KG', '+44', NULL, 'Inactive'),
(116, 'Lao People\'s Democratic Republic', 'LA', '+44', NULL, 'Inactive'),
(117, 'Latvia', 'LV', '+44', NULL, 'Inactive'),
(118, 'Lebanon', 'LB', '+44', NULL, 'Inactive'),
(119, 'Lesotho', 'LS', '+44', NULL, 'Inactive'),
(120, 'Liberia', 'LR', '+44', NULL, 'Inactive'),
(121, 'Libyan Arab Jamahiriya', 'LY', '+44', NULL, 'Inactive'),
(122, 'Liechtenstein', 'LI', '+44', NULL, 'Inactive'),
(123, 'Lithuania', 'LT', '+44', NULL, 'Inactive'),
(124, 'Luxembourg', 'LU', '+44', NULL, 'Inactive'),
(125, 'Macau', 'MO', '+44', NULL, 'Inactive'),
(126, 'Macedonia, The Former Yugoslav Republic of', 'MK', '+44', NULL, 'Inactive'),
(127, 'Madagascar', 'MG', '+44', NULL, 'Inactive'),
(128, 'Malawi', 'MW', '+44', NULL, 'Inactive'),
(129, 'Malaysia', 'MY', '+44', NULL, 'Inactive'),
(130, 'Maldives', 'MV', '+44', NULL, 'Inactive'),
(131, 'Mali', 'ML', '+44', NULL, 'Inactive'),
(132, 'Malta', 'MT', '+44', NULL, 'Inactive'),
(133, 'Marshall Islands', 'MH', '+44', NULL, 'Inactive'),
(134, 'Martinique', 'MQ', '+44', NULL, 'Inactive'),
(135, 'Mauritania', 'MR', '+44', NULL, 'Inactive'),
(136, 'Mauritius', 'MU', '+44', NULL, 'Inactive'),
(137, 'Mayotte', 'YT', '+44', NULL, 'Inactive'),
(138, 'Mexico', 'MX', '+44', NULL, 'Inactive'),
(139, 'Micronesia, Federated States of', 'FM', '+44', NULL, 'Inactive'),
(140, 'Moldova, Republic of', 'MD', '+44', NULL, 'Inactive'),
(141, 'Monaco', 'MC', '+44', NULL, 'Inactive'),
(142, 'Mongolia', 'MN', '+44', NULL, 'Inactive'),
(143, 'Montserrat', 'MS', '+44', NULL, 'Inactive'),
(144, 'Morocco', 'MA', '+44', NULL, 'Inactive'),
(145, 'Mozambique', 'MZ', '+44', NULL, 'Inactive'),
(146, 'Myanmar', 'MM', '+44', NULL, 'Inactive'),
(147, 'Namibia', 'NA', '+44', NULL, 'Inactive'),
(148, 'Nauru', 'NR', '+44', NULL, 'Inactive'),
(149, 'Nepal', 'NP', '+44', NULL, 'Inactive'),
(150, 'Netherlands', 'NL', '+44', NULL, 'Inactive'),
(151, 'Netherlands Antilles', 'AN', '+44', NULL, 'Inactive'),
(152, 'New Caledonia', 'NC', '+44', NULL, 'Inactive'),
(153, 'New Zealand', 'NZ', '+44', NULL, 'Inactive'),
(154, 'Nicaragua', 'NI', '+44', NULL, 'Inactive'),
(155, 'Niger', 'NE', '+44', NULL, 'Inactive'),
(156, 'Nigeria', 'NG', '+44', NULL, 'Inactive'),
(157, 'Niue', 'NU', '+44', NULL, 'Inactive'),
(158, 'Norfolk Island', 'NF', '+44', NULL, 'Inactive'),
(159, 'Northern Mariana Islands', 'MP', '+44', NULL, 'Inactive'),
(160, 'Norway', 'NO', '+44', NULL, 'Inactive'),
(161, 'Oman', 'OM', '+44', NULL, 'Inactive'),
(162, 'Pakistan', 'PK', '+44', NULL, 'Inactive'),
(163, 'Palau', 'PW', '+44', NULL, 'Inactive'),
(164, 'Panama', 'PA', '+44', NULL, 'Inactive'),
(165, 'Papua New Guinea', 'PG', '+44', NULL, 'Inactive'),
(166, 'Paraguay', 'PY', '+44', NULL, 'Inactive'),
(167, 'Peru', 'PE', '+44', NULL, 'Inactive'),
(168, 'Philippines', 'PH', '+44', NULL, 'Inactive'),
(169, 'Pitcairn', 'PN', '+44', NULL, 'Inactive'),
(170, 'Poland', 'PL', '+44', NULL, 'Inactive'),
(171, 'Portugal', 'PT', '+44', NULL, 'Inactive'),
(172, 'Puerto Rico', 'PR', '+44', NULL, 'Inactive'),
(173, 'Qatar', 'QA', '+44', NULL, 'Inactive'),
(174, 'Reunion', 'RE', '+44', NULL, 'Inactive'),
(175, 'Romania', 'RO', '+44', NULL, 'Inactive'),
(176, 'Russian Federation', 'RU', '+44', NULL, 'Inactive'),
(177, 'Rwanda', 'RW', '+44', NULL, 'Inactive'),
(178, 'Saint Kitts and Nevis', 'KN', '+44', NULL, 'Inactive'),
(179, 'Saint Lucia', 'LC', '+44', NULL, 'Inactive'),
(180, 'Saint Vincent and the Grenadines', 'VC', '+44', NULL, 'Inactive'),
(181, 'Samoa', 'WS', '+44', NULL, 'Inactive'),
(182, 'San Marino', 'SM', '+44', NULL, 'Inactive'),
(183, 'Sao Tome and Principe', 'ST', '+44', NULL, 'Inactive'),
(184, 'Saudi Arabia', 'SA', '+44', NULL, 'Inactive'),
(185, 'Senegal', 'SN', '+44', NULL, 'Inactive'),
(186, 'Seychelles', 'SC', '+44', NULL, 'Inactive'),
(187, 'Sierra Leone', 'SL', '+44', NULL, 'Inactive'),
(188, 'Singapore', 'SG', '+44', NULL, 'Inactive'),
(189, 'Slovakia (Slovak Republic)', 'SK', '+44', NULL, 'Inactive'),
(190, 'Slovenia', 'SI', '+44', NULL, 'Inactive'),
(191, 'Solomon Islands', 'SB', '+44', NULL, 'Inactive'),
(192, 'Somalia', 'SO', '+44', NULL, 'Inactive'),
(193, 'South Africa', 'ZA', '+44', NULL, 'Inactive'),
(194, 'South Georgia and the South Sandwich Islands', 'GS', '+44', NULL, 'Inactive'),
(195, 'Spain', 'ES', '+44', NULL, 'Inactive'),
(196, 'Sri Lanka', 'LK', '+44', NULL, 'Inactive'),
(197, 'St. Helena', 'SH', '+44', NULL, 'Inactive'),
(198, 'St. Pierre and Miquelon', 'PM', '+44', NULL, 'Inactive'),
(199, 'Sudan', 'SD', '+44', NULL, 'Inactive'),
(200, 'Suriname', 'SR', '+44', NULL, 'Inactive'),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', '+44', NULL, 'Inactive'),
(202, 'Swaziland', 'SZ', '+44', NULL, 'Inactive'),
(203, 'Sweden', 'SE', '+44', NULL, 'Inactive'),
(204, 'Switzerland', 'CH', '+44', NULL, 'Inactive'),
(205, 'Syrian Arab Republic', 'SY', '+44', NULL, 'Inactive'),
(206, 'Taiwan', 'TW', '+44', NULL, 'Inactive'),
(207, 'Tajikistan', 'TJ', '+44', NULL, 'Inactive'),
(208, 'Tanzania, United Republic of', 'TZ', '+44', NULL, 'Inactive'),
(209, 'Thailand', 'TH', '+44', NULL, 'Inactive'),
(210, 'Togo', 'TG', '+44', NULL, 'Inactive'),
(211, 'Tokelau', 'TK', '+44', NULL, 'Inactive'),
(212, 'Tonga', 'TO', '+44', NULL, 'Inactive'),
(213, 'Trinidad and Tobago', 'TT', '+44', NULL, 'Inactive'),
(214, 'Tunisia', 'TN', '+44', NULL, 'Inactive'),
(215, 'Turkey', 'TR', '+44', NULL, 'Inactive'),
(216, 'Turkmenistan', 'TM', '+44', NULL, 'Inactive'),
(217, 'Turks and Caicos Islands', 'TC', '+44', NULL, 'Inactive'),
(218, 'Tuvalu', 'TV', '+44', NULL, 'Inactive'),
(219, 'Uganda', 'UG', '+44', NULL, 'Inactive'),
(220, 'Ukraine', 'UA', '+44', NULL, 'Inactive'),
(221, 'United Arab Emirates', 'AE', '+44', NULL, 'Inactive'),
(222, 'United Kingdom', 'GB', '+44', 'Yes', 'Active'),
(223, 'United States', 'US', '+1', NULL, 'Active'),
(224, 'United States Minor Outlying Islands', 'UM', '+44', NULL, 'Inactive'),
(225, 'Uruguay', 'UY', '+44', NULL, 'Inactive'),
(226, 'Uzbekistan', 'UZ', '+44', NULL, 'Inactive'),
(227, 'Vanuatu', 'VU', '+44', NULL, 'Inactive'),
(228, 'Vatican City State (Holy See)', 'VA', '+44', NULL, 'Inactive'),
(229, 'Venezuela', 'VE', '+44', NULL, 'Inactive'),
(230, 'Viet Nam', 'VN', '+44', NULL, 'Inactive'),
(231, 'Virgin Islands (British)', 'VG', '+44', NULL, 'Inactive'),
(232, 'Virgin Islands (U.S.)', 'VI', '+44', NULL, 'Inactive'),
(233, 'Wallis and Futuna Islands', 'WF', '+44', NULL, 'Inactive'),
(234, 'Western Sahara', 'EH', '+44', NULL, 'Inactive'),
(235, 'Yemen', 'YE', '+44', NULL, 'Inactive'),
(236, 'Yugoslavia', 'YU', '+44', NULL, 'Inactive'),
(237, 'Zaire', 'ZR', '+44', NULL, 'Inactive'),
(238, 'Zambia', 'ZM', '+44', NULL, 'Inactive'),
(239, 'Zimbabwe', 'ZW', '+44', NULL, 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `iCustomerId` int NOT NULL,
  `iCustomerTypeId` int DEFAULT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `vTitle` varchar(255) DEFAULT NULL,
  `vWebsiteName` varchar(255) DEFAULT NULL,
  `tDescription` text,
  `vImage` varchar(255) DEFAULT NULL,
  `vEmail` varchar(255) DEFAULT NULL,
  `vPassword` varchar(255) DEFAULT NULL,
  `vMobile` varchar(255) DEFAULT NULL,
  `vCellulor` varchar(255) DEFAULT NULL,
  `vOfficeName` varchar(255) DEFAULT NULL,
  `vAddress` varchar(255) DEFAULT NULL,
  `vCity` varchar(255) DEFAULT NULL,
  `iStateId` int DEFAULT NULL,
  `vZipCode` varchar(255) DEFAULT NULL,
  `eStatus` enum('Active','Inactive','Stand By','Suspended') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`iCustomerId`, `iCustomerTypeId`, `vName`, `vTitle`, `vWebsiteName`, `tDescription`, `vImage`, `vEmail`, `vPassword`, `vMobile`, `vCellulor`, `vOfficeName`, `vAddress`, `vCity`, `iStateId`, `vZipCode`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(11, 1, 'Horacio', 'owner', 'hmc', 'demo', '1646285998.favicon.ico', 'horacio@mailinator.com', NULL, '(214) 563-2111', '(214) 222-2222', 'hmc innovs llc', 'las vegas', 'aero', 3615, '321045', 'Active', '2022-03-14 06:15:32', '2022-03-02 10:30:02'),
(12, 1, 'Krupesh Thakkar', 'Web Developer', 'isyncevolution.com', 'Note', '1646297017.logo.jpg', 'krupesh.thakkar@isyncevolution.com', NULL, '(123) 456-8797', '(123) 456-7899', 'iSyncEvolution LLP', 'Ahmedabad', 'Ahmedabad', 3617, '123456', 'Active', '2022-03-03 10:06:34', '2022-03-03 08:43:37'),
(52, 1, 'abbas', 'test', 'test', 'test', NULL, 'abbas@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '(155) 652-2105', '(323) 233-3330', 'isynce', 'test', 'test', 2722, '987456', 'Active', '2022-07-21 05:31:07', '2022-07-20 22:37:18'),
(54, 1, 'dsfds', 'test', 'test', 'dfsfsd', NULL, 'demo@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(666) 623-2333', '(112) 212-2222', 'isynce', 'test', 'test', 2722, '132332', 'Active', '2022-07-21 04:43:50', '2022-07-21 04:43:50'),
(55, 1, 'dsfds', 'test', 'test', 'dfsfsd', NULL, 'demo@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(666) 623-2333', '(112) 212-2222', 'isynce', 'test', 'test', 2722, '132332', 'Active', '2022-07-21 05:28:48', '2022-07-21 04:45:58'),
(56, 1, 'kkk', 'kkk', 'kkk', 'kkk', NULL, 'demo@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(254) 512-1212', '(121) 222-2221', 'kkk', 'kkk', 'kkk', 2722, '989892', 'Active', '2022-07-21 05:32:44', '2022-07-21 05:32:44'),
(57, 1, 'ronaldo', 'ronaldo', 'ronaldo', 'ronaldo', NULL, 'ronaldo@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '(145) 488-4512', '(445) 545-5221', 'cr7', 'test', 'test', 2722, '789652', 'Active', '2022-07-21 23:34:39', '2022-07-21 23:34:39'),
(58, 1, 'Krupesh Thakkar', 'MD', 'krupeshthakkar.com', 'this is testing lab', NULL, 'krupesh.thakkar@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(121) 212-1212', '(122) 212-1212', 'Toothsi', '601, Venus Benecia', 'Ahmedabad', 3656, '365985', 'Active', '2022-07-22 02:08:17', '2022-07-22 02:08:17'),
(59, 1, 'ab test', 'ab test', 'ab test', 'ab test', NULL, 'abtest@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '(544) 466-2622', '(121) 221-2122', 'ab test', 'ab test', 'ab test', 2722, '789654', 'Active', '2022-07-22 03:23:36', '2022-07-22 03:23:36'),
(60, 1, 'test', 'test', 'test', '11', NULL, 'demo@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(151) 215-1212', '(225) 121-2121', 'isynce', 'test', 'test', 2722, '155512', 'Active', '2022-07-23 02:53:32', '2022-07-23 02:53:32'),
(61, 1, 'dsasda', 'test', 'test', 'asdasd', NULL, 'demo@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(521) 212-1212', '(211) 212-1212', 'isynce', 'test', 'test', 2722, '4512121', 'Active', '2022-07-23 02:54:31', '2022-07-23 02:54:31'),
(62, 1, 'Lab Admin', 'Lab Admin', 'LabAdmin', 'LabAdmin@mailinator.com', NULL, 'LabAdmin@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(454) 512-1212', '(551) 512-1212', 'Lab Admin', 'Lab Admin', 'Lab Admin', 3614, '545451', 'Active', '2022-08-01 05:11:50', '2022-08-01 05:11:50'),
(63, 6, 'Office Contact Name', 'affordable Dental 2', 'affordable.com', 'Office Customer 1', NULL, 'officecustomer1@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(232) 323-2323', '(232) 323-2323', 'affordable Dental 2', 'Las Vegas', 'Las Vegas', 3652, '100001', 'Active', '2022-08-02 23:20:33', '2022-08-02 23:20:33'),
(64, 6, 'New Office', 'New Office', 'NewOffice.com', 'New Office', NULL, 'NewOffice@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(451) 121-2112', '(212) 212-1212', 'New Office', 'New Office', 'test', 2722, '454512', 'Active', '2022-08-02 23:45:11', '2022-08-02 23:45:11'),
(65, 6, 'Office 601', 'Office 601', '601.com', 'Office 601', NULL, 'Office601@eosbuzz.com', 'd41d8cd98f00b204e9800998ecf8427e', '(154) 511-2121', '(545) 121-2121', 'Office 601', 'Office 601', 'Office 601', 2722, '1555451', 'Active', '2022-08-03 03:02:50', '2022-08-03 03:02:50'),
(66, 6, 'Test Office', 'Test Office', 'testoffice.com', 'Test office', NULL, 'officetest@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(323) 233-3233', '(232) 323-2332', 'Test Office', 'LA', 'LA', 3652, '323232', 'Active', '2022-08-03 06:48:14', '2022-08-03 06:48:14'),
(67, 6, 'Offffffice', 'Offffffice', 'Offffffice', 'Offffffice', NULL, 'Offffffice@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '(121) 212-1212', '(121) 212-1212', 'Offffffice', 'Offffffice', 'Offffffice', 2722, '323232', 'Active', '2022-08-03 06:50:12', '2022-08-03 06:50:12'),
(68, 6, 'Test user', 'Test Office 123', 'test.com', 'Testing', NULL, 'testof123@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '(123) 132-1231', '(123) 123-1231', 'Test Office 123', 'Test Office 123', 'Test Office 123', 3617, '323232', 'Active', '2022-08-03 07:09:42', '2022-08-03 07:09:42'),
(69, 1, 'Test Lab customer', 'Test Lab customer', 'a.com', '565656', NULL, 'labcus@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '(656) 565-6565', '(565) 656-5656', 'Test Lab customer', 'Test Lab customer', 'Test Lab customer', 2722, '12', 'Active', '2022-08-03 22:33:30', '2022-08-03 22:33:30'),
(70, 1, 'Harvy Spector', 'MD', 'pearsonspector.com', '23232', NULL, 'harvy@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '(323) 232-3232', '(232) 323-2323', 'Pearson Spector', '10001, NY', 'NY', 3656, '100001', 'Active', '2022-08-03 22:37:34', '2022-08-03 22:37:34');

-- --------------------------------------------------------

--
-- Table structure for table `customertype`
--

CREATE TABLE `customertype` (
  `iCustomerTypeId` int NOT NULL,
  `vTypeName` varchar(255) DEFAULT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `customertype`
--

INSERT INTO `customertype` (`iCustomerTypeId`, `vTypeName`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(1, 'LAB', 'Active', '2022-02-24 06:34:46', '2022-02-24 06:34:46'),
(6, 'Office', 'Active', '2022-03-23 12:00:06', '2022-03-01 11:54:26');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `iDepartmentId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`iDepartmentId`, `iCustomerId`, `vName`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(1, 0, 'Super Admin', 'Active', '2022-07-21 00:03:32', '2022-07-21 00:03:32'),
(2, 0, 'Lab Admin/Customer', 'Active', '2022-07-21 00:03:46', '2022-07-21 00:03:46'),
(3, 0, 'Doctor', 'Active', '2022-07-21 00:03:55', '2022-07-21 00:03:55'),
(4, 0, 'User', 'Active', '2022-07-21 00:04:04', '2022-07-21 00:04:04'),
(5, NULL, 'Driver', 'Active', '2022-07-22 06:11:47', '2022-07-22 06:11:47'),
(6, NULL, 'Office Admin/Customer', 'Active', '2022-08-03 22:40:27', '2022-08-03 22:40:27');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `iDoctorId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `iOfficeId` int DEFAULT NULL,
  `vImage` varchar(255) DEFAULT NULL,
  `vSignature` varchar(255) DEFAULT NULL,
  `tSigned` text,
  `vFirstName` varchar(255) DEFAULT NULL,
  `vLastName` varchar(255) DEFAULT NULL,
  `vEmail` varchar(255) DEFAULT NULL,
  `vPassword` varchar(255) DEFAULT NULL,
  `vLicence` varchar(255) DEFAULT NULL,
  `vMobile` varchar(255) DEFAULT NULL,
  `vCellulor` varchar(255) DEFAULT NULL,
  `vMiddleInitial` varchar(255) DEFAULT NULL,
  `tDescription` text,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`iDoctorId`, `iCustomerId`, `iOfficeId`, `vImage`, `vSignature`, `tSigned`, `vFirstName`, `vLastName`, `vEmail`, `vPassword`, `vLicence`, `vMobile`, `vCellulor`, `vMiddleInitial`, `tDescription`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(18, 11, 7, '622ad5384538e.png', NULL, NULL, 'Shanon', 'Lee', 'krupesh.thakkar@isyncevolution.com', 'd41d8cd98f00b204e9800998ecf8427e', '6789', '(987) 654-3210', '(987) 654-3210', 'J', 'Note Description', 'Active', '2022-03-11 04:51:40', '2022-03-02 10:48:06'),
(19, 11, 7, '622ad4f7634b2.png', '6221bb4888702.png', NULL, 'John', 'Bui', 'ab@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '1234', '(123) 456-6666', '(123) 123-1313', 'ab', 'demo', 'Active', '2022-03-11 04:49:59', '2022-03-04 07:10:00'),
(21, 11, 6, '6285d5d54d2a1.png', NULL, NULL, 'Don Tiburcio', 'Tiburcio', 'kailash@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '0000', '(123) 456-1111', '(111) 111-1111', 'demo', 'demo', 'Active', '2022-05-19 05:29:57', '2022-03-07 05:21:40'),
(25, 11, 9, '62a742e9a7b12.png', NULL, NULL, 'Evangeline', 'Chang', 'admin@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '0000', '(000) 000-0000', '(000) 000-0000', 'L', 'none', 'Active', '2022-06-13 07:30:09', '2022-05-01 19:14:24'),
(34, 11, 11, '628f66e71001b.png', NULL, NULL, 'first name', NULL, 'doctoremail@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '2023', '(545) 464-5646', '(658) 646-4465', 'test middle initial', 'test note', 'Active', '2022-05-26 11:42:16', '2022-05-26 11:39:19'),
(35, 11, 12, '62a348e4d6314.png', NULL, NULL, 'Jonh', 'Rosales', 'jonrosa@mail.com', 'd41d8cd98f00b204e9800998ecf8427e', '0000', '(000) 000-0000', '(000) 000-0000', 'M', 'none', 'Active', '2022-06-10 07:06:36', '2022-05-27 18:59:47'),
(38, 56, 6, '62db94ebcf577.png', NULL, NULL, 'justin', 'justin', 'justin@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', 'justin1', '(545) 441-2121', NULL, 'justin', 'justin', 'Active', '2022-07-22 11:27:55', '2022-07-22 23:53:22'),
(39, 11, 6, '62dbbb706c677.png', NULL, NULL, 'ganpat', 'ganpat', 'ganpat@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', 'ganpat', '(454) 552-2231', '(445) 421-2121', 'lal', 'ganpat', 'Active', '2022-07-23 02:12:16', '2022-07-23 02:12:16');

-- --------------------------------------------------------

--
-- Table structure for table `driver_history`
--

CREATE TABLE `driver_history` (
  `iDriverHistoryId` int NOT NULL,
  `iCaseId` int NOT NULL,
  `iSlipId` int NOT NULL,
  `iStageId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `vStageName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `vSlipNumber` varchar(255) DEFAULT NULL,
  `iLoginId` int NOT NULL,
  `vLoginUserName` varchar(255) NOT NULL,
  `vPatientName` varchar(255) NOT NULL,
  `iOfficeId` int NOT NULL,
  `vOfficeName` varchar(255) NOT NULL,
  `iCasePanId` int NOT NULL,
  `vCasePanNumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `vLocation` varchar(255) NOT NULL,
  `dtAddedDate` datetime NOT NULL,
  `eUserType` enum('Driver','Receiver','Sender') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `vSignature` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `driver_history`
--

INSERT INTO `driver_history` (`iDriverHistoryId`, `iCaseId`, `iSlipId`, `iStageId`, `vStageName`, `vSlipNumber`, `iLoginId`, `vLoginUserName`, `vPatientName`, `iOfficeId`, `vOfficeName`, `iCasePanId`, `vCasePanNumber`, `vLocation`, `dtAddedDate`, `eUserType`, `vSignature`) VALUES
(1, 1, 1, '16', 'Custom Tray', '0001', 11, 'Horacio', 'abbas', 11, 'text contact', 1, 'A01', 'In office ready to pickup', '2022-07-25 10:41:35', 'Driver', 'test'),
(2, 1, 1, '16', 'Custom Tray', '0001', 11, 'Horacio', 'abbas', 11, 'text contact', 1, 'A01', 'On route to the lab', '2022-07-25 10:41:39', 'Receiver', 'test'),
(3, 1, 1, NULL, NULL, '0001', 11, 'Horacio', 'abbas', 11, 'text contact', 1, NULL, 'In lab', '2022-07-25 10:41:43', 'Sender', NULL),
(4, 1, 1, '16', 'Custom Tray', '0001', 11, 'Horacio', 'abbas', 11, 'text contact', 1, 'A01', 'In lab ready to pickup', '2022-07-25 10:41:48', 'Sender', 'john'),
(5, 1, 1, '16', 'Custom Tray', '0001', 11, 'Horacio', 'abbas', 11, 'text contact', 1, 'A01', 'On route to the office', '2022-07-25 10:41:52', 'Driver', 'test'),
(6, 1, 2, '13/16', 'Bite Block/Custom Tray', '0002', 11, 'Horacio', 'abbas', 11, 'text contact', 1, 'A01', 'In office ready to pickup', '2022-07-25 10:42:44', 'Driver', 'test'),
(7, 1, 2, '13/16', 'Bite Block/Custom Tray', '0002', 11, 'Horacio', 'abbas', 11, 'text contact', 1, 'A01', 'On route to the lab', '2022-07-25 10:42:48', 'Receiver', 'smith'),
(8, 1, 2, NULL, NULL, '0002', 11, 'Horacio', 'abbas', 11, 'text contact', 1, NULL, 'In lab', '2022-07-25 10:42:51', 'Sender', NULL),
(9, 1, 2, '13/16', 'Bite Block/Custom Tray', '0002', 11, 'Horacio', 'abbas', 11, 'text contact', 1, 'A01', 'In lab ready to pickup', '2022-07-25 10:42:56', 'Sender', 'smith'),
(10, 1, 2, '13/16', 'Bite Block/Custom Tray', '0002', 11, 'Horacio', 'abbas', 11, 'text contact', 1, 'A01', 'On route to the office', '2022-07-25 10:43:00', 'Driver', 'done'),
(11, 1, 6, '14', 'Try in  with teeth', '0006', 11, 'Horacio', 'abbas', 11, 'text contact', 1, 'A01', 'In office ready to pickup', '2022-07-29 11:41:09', 'Driver', 'done'),
(12, 8, 7, '6', 'Custom Tray', '0007', 11, 'Horacio', 'qasim', 11, 'text contact', 1, 'A05', 'In office ready to pickup', '2022-07-29 11:50:59', 'Driver', 'test'),
(13, 8, 7, '6', 'Custom Tray', '0007', 11, 'Horacio', 'qasim', 11, 'text contact', 1, 'A05', 'On route to the lab', '2022-07-29 11:52:14', 'Receiver', 'john'),
(14, 8, 7, NULL, NULL, '0007', 11, 'Horacio', 'qasim', 11, 'text contact', 1, NULL, 'In lab', '2022-07-29 11:57:23', 'Sender', NULL),
(15, 8, 7, '6', 'Custom Tray', '0007', 11, 'Horacio', 'qasim', 11, 'text contact', 1, 'A05', 'In lab ready to pickup', '2022-07-30 12:00:02', 'Sender', 'john'),
(16, 8, 7, '6', 'Custom Tray', '0007', 11, 'Horacio', 'qasim', 11, 'text contact', 1, 'A05', 'On route to the office', '2022-07-30 12:02:08', 'Driver', 'test'),
(17, 8, 8, '17', 'Bite block', '0008', 11, 'Horacio', 'qasim', 11, 'text contact', 1, 'A05', 'In office ready to pickup', '2022-07-30 12:05:51', 'Driver', 'test'),
(18, 8, 8, '17', 'Bite block', '0008', 11, 'Horacio', 'qasim', 11, 'text contact', 1, 'A05', 'On route to the lab', '2022-07-30 12:05:55', 'Receiver', 'done'),
(19, 8, 8, NULL, NULL, '0008', 11, 'Horacio', 'qasim', 11, 'text contact', 1, NULL, 'In lab', '2022-07-30 12:05:59', 'Sender', NULL),
(20, 8, 8, '17', 'Bite block', '0008', 11, 'Horacio', 'qasim', 11, 'text contact', 1, 'A05', 'In lab ready to pickup', '2022-07-30 12:06:18', 'Sender', 'done'),
(21, 8, 8, '17', 'Bite block', '0008', 11, 'Horacio', 'qasim', 11, 'text contact', 1, 'A05', 'On route to the office', '2022-07-30 12:06:25', 'Driver', 'tudor');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `iEmailId` int NOT NULL,
  `vEmailCode` varchar(255) DEFAULT NULL,
  `vTitle` varchar(255) DEFAULT NULL,
  `vFromName` varchar(255) DEFAULT NULL,
  `vFromEmail` varchar(255) DEFAULT NULL,
  `vCcEmail` varchar(255) DEFAULT NULL,
  `vBccEmail` varchar(255) DEFAULT NULL,
  `vSubject` varchar(255) DEFAULT NULL,
  `tMessage` text,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`iEmailId`, `vEmailCode`, `vTitle`, `vFromName`, `vFromEmail`, `vCcEmail`, `vBccEmail`, `vSubject`, `tMessage`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(24, 'CONTACT_FRONT', 'Contact Form Front', 'Administrador', 'email.isyncevolution@gmail.com', 'demo', 'demo', '#SYSTEM.COMPANY_NAME# - Inquiry', '<p><strong>demo</strong></p>', 'Active', '2022-02-01 11:49:00', '2022-01-05 12:44:12');

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `iFeesId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vRushFee` varchar(255) DEFAULT NULL,
  `vFee` varchar(255) DEFAULT NULL,
  `iStagePrice` int NOT NULL,
  `iProductStageId` int DEFAULT NULL,
  `iWorkingTime` int DEFAULT NULL,
  `vTotalRush` varchar(255) DEFAULT NULL,
  `eStatus` enum('Active','Inactive','Stand By','Suspended') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `fees`
--

INSERT INTO `fees` (`iFeesId`, `iCustomerId`, `vRushFee`, `vFee`, `iStagePrice`, `iProductStageId`, `iWorkingTime`, `vTotalRush`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(1, NULL, '100', '10', 0, 5, 5, '50', 'Active', '2022-03-16 00:00:00', '2022-03-16 00:00:00'),
(2, NULL, '5', 'abc', 0, 5, 5, '500', 'Active', '2022-03-16 11:55:33', '2022-03-16 11:42:55'),
(3, NULL, '5', 'demo', 0, 1, 5, '5', 'Active', '2022-03-16 11:54:21', '2022-03-16 11:54:21'),
(4, NULL, '12.5', '50', 25, 6, 3, '37', 'Active', '2022-04-14 05:13:50', '2022-03-25 13:43:32'),
(5, NULL, '7.2', '30', 24, 10, 3, '31', 'Active', '2022-05-06 05:59:40', '2022-04-13 13:11:08'),
(6, NULL, '25', '100', 25, 6, 1, '50', 'Active', '2022-04-14 05:14:26', '2022-04-14 05:14:26'),
(7, NULL, '27.5', '50', 55, 13, 3, '82', 'Active', '2022-04-23 14:45:45', '2022-04-23 14:45:45'),
(8, NULL, '44', '80', 55, 13, 2, '99', 'Active', '2022-04-23 14:46:17', '2022-04-23 14:46:17'),
(9, NULL, '55', '100', 55, 13, 1, '110', 'Active', '2022-04-23 14:46:56', '2022-04-23 14:46:56'),
(10, NULL, '7.5', '30', 25, 16, 3, '32', 'Active', '2022-05-06 05:54:31', '2022-05-06 05:54:31'),
(11, NULL, '15', '60', 25, 16, 2, '40', 'Active', '2022-05-06 05:55:02', '2022-05-06 05:55:02'),
(12, NULL, '25', '100', 25, 16, 1, '50', 'Active', '2022-05-06 05:55:22', '2022-05-06 05:55:22'),
(13, NULL, '15', '20', 75, 14, 5, '90', 'Active', '2022-05-06 05:56:02', '2022-05-06 05:56:02'),
(14, NULL, '22.5', '30', 75, 14, 4, '97', 'Active', '2022-05-06 05:56:41', '2022-05-06 05:56:41'),
(15, NULL, '37.5', '50', 75, 14, 3, '112', 'Active', '2022-05-06 05:57:11', '2022-05-06 05:57:11'),
(16, NULL, '60', '80', 75, 14, 2, '135', 'Active', '2022-05-06 05:57:32', '2022-05-06 05:57:32'),
(17, NULL, '75', '100', 75, 14, 1, '150', 'Active', '2022-05-06 05:57:55', '2022-05-06 05:57:55'),
(18, NULL, '14.4', '60', 24, 10, 3, '38', 'Active', '2022-05-06 06:00:04', '2022-05-06 05:58:35'),
(19, NULL, '24', '100', 24, 10, 1, '48', 'Active', '2022-05-06 05:59:03', '2022-05-06 05:59:03'),
(20, NULL, '16.5', '30', 55, 7, 3, '71', 'Active', '2022-05-06 06:01:07', '2022-05-06 06:01:07'),
(21, NULL, '50', '100', 50, 28, 1, '100', 'Active', '2022-05-23 07:31:44', '2022-05-23 07:31:44'),
(22, 11, '1.24', '2', 62, 58, 1, '63', 'Active', '2022-08-02 05:42:46', '2022-08-02 05:42:46');

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `iGradeId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vQualityName` varchar(255) DEFAULT NULL,
  `iSequence` int DEFAULT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`iGradeId`, `iCustomerId`, `vQualityName`, `iSequence`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(3, NULL, 'Premium', 5, 'Active', '2022-03-11 02:26:02', '2022-03-07 06:28:13'),
(4, 11, 'Mid Grade', 1, 'Active', '2022-08-02 06:26:54', '2022-03-11 14:25:42'),
(5, NULL, 'Premium ++', 15, 'Active', '2022-04-16 17:53:25', '2022-04-16 17:53:25');

-- --------------------------------------------------------

--
-- Table structure for table `gumbrand`
--

CREATE TABLE `gumbrand` (
  `iGumBrandId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `iSequence` int DEFAULT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `gumbrand`
--

INSERT INTO `gumbrand` (`iGumBrandId`, `iCustomerId`, `vName`, `iSequence`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(2, 11, 'St Geroge', 5, 'Active', '2022-08-02 04:16:26', '2022-03-09 12:03:43'),
(5, NULL, 'Denstply', 15, 'Active', '2022-03-11 04:30:13', '2022-03-11 04:30:13'),
(6, NULL, 'GC america', 15, 'Active', '2022-05-04 13:25:39', '2022-05-04 13:25:39'),
(8, NULL, 'Test Gum shade', 22, 'Active', '2022-05-26 12:43:53', '2022-05-26 12:43:53'),
(9, NULL, 'Valplast', 40, 'Active', '2022-05-31 04:55:08', '2022-05-31 04:55:08');

-- --------------------------------------------------------

--
-- Table structure for table `gumshades`
--

CREATE TABLE `gumshades` (
  `iGumShadesId` int NOT NULL,
  `iGumBrandId` int DEFAULT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vCode` varchar(255) DEFAULT NULL,
  `vGumShade` varchar(255) DEFAULT NULL,
  `iSequence` int DEFAULT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `gumshades`
--

INSERT INTO `gumshades` (`iGumShadesId`, `iGumBrandId`, `iCustomerId`, `vCode`, `vGumShade`, `iSequence`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(2, 2, 11, 'LV', 'Light vein', 5, 'Active', '2022-08-02 03:52:18', '2022-03-09 12:10:02'),
(3, 2, NULL, 'V', 'Vein', 10, 'Active', '2022-03-11 06:07:35', '2022-03-11 04:23:48'),
(4, 2, NULL, 'RD', 'Red vein', 15, 'Active', '2022-05-04 13:07:35', '2022-05-04 13:07:35'),
(5, 5, NULL, 'SP', 'Standard pink', 5, 'Active', '2022-05-04 13:27:05', '2022-05-04 13:27:05'),
(6, 6, NULL, 'LF', 'Light Fiebered', 5, 'Active', '2022-05-04 13:27:38', '2022-05-04 13:27:38'),
(8, 8, NULL, 'TGS', 'gum shade', 15, 'Active', '2022-05-26 12:44:38', '2022-05-26 12:44:38'),
(9, 9, NULL, 'V1', 'Standard pink', 5, 'Active', '2022-05-31 04:55:53', '2022-05-31 04:55:53');

-- --------------------------------------------------------

--
-- Table structure for table `impression`
--

CREATE TABLE `impression` (
  `iImpressionId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `vCode` varchar(255) DEFAULT NULL,
  `iSequence` int DEFAULT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `impression`
--

INSERT INTO `impression` (`iImpressionId`, `iCustomerId`, `vName`, `vCode`, `iSequence`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(3, NULL, 'No impression at this apt', 'np', 40, 'Active', '2022-05-21 03:04:48', '2022-04-30 05:05:40'),
(4, 11, 'Alginate', 'Alg', 2, 'Active', '2022-08-01 11:41:13', '2022-05-03 06:05:11'),
(5, 11, 'Medium body', 'Mb', 10, 'Active', '2022-08-01 11:41:17', '2022-05-03 06:05:59'),
(6, NULL, 'Light body', 'LB', 15, 'Active', '2022-05-03 06:06:27', '2022-05-03 06:06:27'),
(7, NULL, 'Pick up impression', 'PI', 20, 'Active', '2022-05-03 06:07:20', '2022-05-03 06:07:20'),
(8, NULL, 'STL File', 'STL', 30, 'Active', '2022-05-03 06:07:47', '2022-05-03 06:07:47'),
(10, NULL, 'test impression', 'TIM', 20, 'Active', '2022-05-27 04:09:10', '2022-05-27 04:09:10');

-- --------------------------------------------------------

--
-- Table structure for table `lab_admin`
--

CREATE TABLE `lab_admin` (
  `iLabAdminId` int NOT NULL,
  `iCustomerId` int NOT NULL,
  `vName` varchar(255) NOT NULL,
  `vEmail` varchar(255) NOT NULL,
  `vPassword` varchar(255) NOT NULL,
  `vMobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `vCellulor` varchar(255) NOT NULL,
  `eStatus` enum('Active','Inactive','Stand By','Suspended') NOT NULL,
  `dtAddedDate` datetime NOT NULL,
  `dtUpdatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `lab_admin`
--

INSERT INTO `lab_admin` (`iLabAdminId`, `iCustomerId`, `vName`, `vEmail`, `vPassword`, `vMobile`, `vCellulor`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(12, 11, 'Lab Admin', 'labAdmin@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(515) 112-1211', '(212) 222-1212', 'Active', '2022-08-01 04:46:11', '2022-08-01 04:46:11'),
(13, 62, 'LabAdmin', 'admin@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(454) 545-2121', '(212) 121-2112', 'Active', '2022-08-01 05:12:39', '2022-08-01 05:12:39'),
(16, 70, 'Harvy Spector', 'harvy@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '(123) 456-7890', '(123) 131-3213', 'Active', '2022-08-03 22:38:03', '2022-08-03 22:38:03');

-- --------------------------------------------------------

--
-- Table structure for table `module_master`
--

CREATE TABLE `module_master` (
  `iModuleId` int NOT NULL,
  `vTitle` varchar(255) DEFAULT NULL,
  `vModuleName` varchar(255) DEFAULT NULL,
  `vControllerName` varchar(255) NOT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_master`
--

INSERT INTO `module_master` (`iModuleId`, `vTitle`, `vModuleName`, `vControllerName`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(1, 'Dashboard', 'dashboard', 'DashboardController', 'Active', '2022-07-23 02:24:48', '2022-07-23 02:36:09'),
(2, 'Super Admin', 'super_admin', 'AdminController', 'Active', '2022-07-23 02:35:53', NULL),
(3, 'Customer', 'customer', 'CustomerController', 'Active', '2022-07-23 02:37:00', NULL),
(4, 'Customer Type', 'customer_type', 'CustomerTypeController', 'Active', '2022-07-23 02:37:31', NULL),
(5, 'Lab Admin', 'lab_admin', 'LabAdminController', 'Active', '2022-07-23 02:38:02', NULL),
(6, 'Office', 'office', 'OfficeController', 'Active', '2022-07-23 02:38:15', NULL),
(7, 'Doctor', 'doctor', 'DoctorController', 'Active', '2022-07-23 02:38:28', NULL),
(8, 'Office User', 'office_user', 'UserController', 'Active', '2022-07-23 02:38:50', NULL),
(9, 'Slip', 'slip', 'LabcaseController', 'Active', '2022-07-23 02:39:05', NULL),
(11, 'Products', 'products', 'CategoryProductController', 'Active', '2022-07-23 02:39:54', '2022-07-23 02:40:07'),
(12, 'Stage', 'stage', 'StageController', 'Active', '2022-07-23 02:40:53', NULL),
(13, 'Case Pan', 'case_pan', 'CasepanController', 'Active', '2022-07-23 02:41:21', NULL),
(14, 'Departments', 'departments', 'DepartmentController', 'Active', '2022-07-23 02:41:40', NULL),
(15, 'Staff', 'staff', 'StaffController', 'Active', '2022-07-23 02:41:54', NULL),
(16, 'Grade', 'grade', 'GradeController', 'Active', '2022-07-23 02:42:09', NULL),
(17, 'Lab Schedule', 'lab_schedule', 'ScheduleController', 'Active', '2022-07-23 02:42:33', NULL),
(18, 'Call Log', 'call_log', 'CallController', 'Active', '2022-07-23 02:42:55', NULL),
(19, 'Permission', 'permission', 'ModulePermissionController', 'Active', '2022-07-23 02:43:14', NULL),
(20, 'Module Master', 'module_master', 'ModuleMasterController', 'Active', '2022-07-23 02:45:11', NULL),
(22, 'Category Add ons', 'category_add_ons', 'AddoncategoryController', 'Active', NULL, NULL),
(23, 'Add ons', 'sub_addon_category', 'SubaddoncategoryController', 'Active', '2022-07-26 12:15:35', NULL),
(24, 'Product Stage', 'product_stage', 'ProductStageController', 'Active', NULL, NULL),
(25, 'CasepanNumber', 'casepan_number', 'CasepanNumberController', 'Active', '2022-07-26 14:59:18', NULL),
(26, 'Calendar', 'calendar', 'CalendarController', 'Active', '2022-07-26 15:53:48', NULL),
(27, 'Category', 'category', 'CategoryController', 'Active', '2022-07-28 12:21:51', NULL),
(28, 'Extractions', 'extractions', 'StageController', 'Active', '2022-07-28 14:41:31', NULL),
(29, 'Impressions', 'impressions', 'StageController', 'Active', '2022-07-28 14:44:30', NULL),
(30, 'Tooth Shades', 'tooth_shades', 'StageController', 'Active', '2022-07-28 14:44:30', NULL),
(31, 'Gum Shades', 'gum_shades', 'StageController', 'Active', '2022-07-28 14:44:30', NULL),
(32, 'Stage Notes', 'stage_notes', 'StageController', 'Active', '2022-07-28 14:44:30', NULL),
(33, 'Rush Dates', 'rush_dates', 'StageController', 'Active', '2022-07-28 14:44:30', NULL),
(34, 'Tooth Shades Brand', 'tooth_shades_brand', 'StageController', 'Active', '2022-07-28 17:11:41', NULL),
(35, 'Gum Shades Brand', 'gum_shades_brand', 'StageController', 'Active', '2022-07-28 14:44:30', NULL),
(36, 'Office Admin', 'office_admin', 'OfficeAdminController', 'Active', '2022-08-03 16:01:50', NULL),
(37, 'Lab User', 'lab_user', 'UserLabController', 'Active', '2022-08-04 11:12:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `module_permission`
--

CREATE TABLE `module_permission` (
  `iPermissionId` int NOT NULL,
  `iDepartmentId` int DEFAULT NULL,
  `iModuleId` int DEFAULT NULL,
  `eCreate` enum('Yes','No') NOT NULL DEFAULT 'No',
  `eRead` enum('Yes','No') NOT NULL DEFAULT 'No',
  `eEdit` enum('Yes','No') NOT NULL DEFAULT 'No',
  `eDelete` enum('Yes','No') DEFAULT 'No',
  `eAddSlip` enum('No','Yes') NOT NULL DEFAULT 'No',
  `eEditStage` enum('No','Yes') NOT NULL DEFAULT 'No',
  `eReadSlip` enum('No','Yes') NOT NULL DEFAULT 'No',
  `eVirualSlip` enum('No','Yes') NOT NULL DEFAULT 'No',
  `ePaperSlip` enum('No','Yes') NOT NULL DEFAULT 'No',
  `eNewStage` enum('No','Yes') NOT NULL DEFAULT 'No',
  `eReadyToSend` enum('No','Yes') NOT NULL DEFAULT 'No',
  `eViewDriverHistory` enum('No','Yes') NOT NULL DEFAULT 'No',
  `ePickUp` enum('No','Yes') NOT NULL DEFAULT 'No',
  `eDropOff` enum('No','Yes') NOT NULL DEFAULT 'No',
  `eDirection` enum('No','Yes') NOT NULL DEFAULT 'No',
  `eAddCallLog` enum('No','Yes') NOT NULL DEFAULT 'No',
  `eHistoryCall` enum('No','Yes') NOT NULL DEFAULT 'No',
  `eLastModified` enum('No','Yes') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_permission`
--

INSERT INTO `module_permission` (`iPermissionId`, `iDepartmentId`, `iModuleId`, `eCreate`, `eRead`, `eEdit`, `eDelete`, `eAddSlip`, `eEditStage`, `eReadSlip`, `eVirualSlip`, `ePaperSlip`, `eNewStage`, `eReadyToSend`, `eViewDriverHistory`, `ePickUp`, `eDropOff`, `eDirection`, `eAddCallLog`, `eHistoryCall`, `eLastModified`) VALUES
(1393, 5, 1, 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1394, 5, 2, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1395, 5, 3, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1396, 5, 4, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1397, 5, 5, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1398, 5, 6, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1399, 5, 7, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1400, 5, 8, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1401, 5, 9, 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'Yes'),
(1402, 5, 11, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1403, 5, 12, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1404, 5, 13, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1405, 5, 14, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1406, 5, 15, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1407, 5, 16, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1408, 5, 17, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1409, 5, 18, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1410, 5, 19, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1411, 5, 20, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1412, 5, 22, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1413, 5, 23, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1414, 5, 24, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1415, 5, 25, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1416, 5, 26, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1417, 5, 27, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1418, 5, 28, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1419, 5, 29, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1420, 5, 30, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1421, 5, 31, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1422, 5, 32, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1423, 5, 33, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1424, 5, 34, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(1425, 5, 35, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2515, 2, 1, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2516, 2, 2, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2517, 2, 3, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2518, 2, 4, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2519, 2, 5, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2520, 2, 6, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2521, 2, 7, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2522, 2, 8, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2523, 2, 9, 'No', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes'),
(2524, 2, 11, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2525, 2, 12, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2526, 2, 13, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2527, 2, 14, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2528, 2, 15, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2529, 2, 16, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2530, 2, 17, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2531, 2, 18, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2532, 2, 19, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2533, 2, 20, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2534, 2, 22, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2535, 2, 23, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2536, 2, 24, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2537, 2, 25, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2538, 2, 26, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2539, 2, 27, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2540, 2, 28, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2541, 2, 29, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2542, 2, 30, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2543, 2, 31, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2544, 2, 32, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2545, 2, 33, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2546, 2, 34, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2547, 2, 35, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2718, 1, 1, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2719, 1, 2, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2720, 1, 3, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2721, 1, 4, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2722, 1, 5, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2723, 1, 6, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2724, 1, 7, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2725, 1, 8, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2726, 1, 9, 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes'),
(2727, 1, 11, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2728, 1, 12, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2729, 1, 13, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2730, 1, 14, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2731, 1, 15, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2732, 1, 16, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2733, 1, 17, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2734, 1, 18, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2735, 1, 19, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2736, 1, 20, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2737, 1, 22, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2738, 1, 23, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2739, 1, 24, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2740, 1, 25, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2741, 1, 26, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2742, 1, 27, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2743, 1, 28, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2744, 1, 29, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2745, 1, 30, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2746, 1, 31, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2747, 1, 32, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2748, 1, 33, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2749, 1, 34, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2750, 1, 35, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2751, 1, 36, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No'),
(2752, 1, 37, 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `iNotificationId` int NOT NULL,
  `vNotificationCode` varchar(255) DEFAULT NULL,
  `eEmail` enum('Yes','No') DEFAULT NULL,
  `eSms` enum('Yes','No') NOT NULL,
  `dtAddedDate` date NOT NULL,
  `dtUpdatedDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`iNotificationId`, `vNotificationCode`, `eEmail`, `eSms`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(1, 'CONTACT_FRONT', 'Yes', 'No', '2022-02-01', '2022-01-05');

-- --------------------------------------------------------

--
-- Table structure for table `office`
--

CREATE TABLE `office` (
  `iOfficeId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `vTitle` varchar(255) DEFAULT NULL,
  `vOfficeCode` varchar(255) DEFAULT NULL,
  `vWebsiteName` varchar(255) DEFAULT NULL,
  `tDescription` text,
  `vImage` varchar(255) DEFAULT NULL,
  `vEmail` varchar(255) DEFAULT NULL,
  `vPassword` varchar(255) DEFAULT NULL,
  `vMobile` varchar(255) DEFAULT NULL,
  `vCellulor` varchar(255) DEFAULT NULL,
  `vOfficeName` varchar(255) DEFAULT NULL,
  `vAddress` varchar(255) DEFAULT NULL,
  `vCity` varchar(255) DEFAULT NULL,
  `iStateId` int DEFAULT NULL,
  `vZipCode` varchar(255) DEFAULT NULL,
  `eStatus` enum('Active','Inactive','Stand By','Suspended') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `office`
--

INSERT INTO `office` (`iOfficeId`, `iCustomerId`, `vName`, `vTitle`, `vOfficeCode`, `vWebsiteName`, `tDescription`, `vImage`, `vEmail`, `vPassword`, `vMobile`, `vCellulor`, `vOfficeName`, `vAddress`, `vCity`, `iStateId`, `vZipCode`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(6, 11, 'Veronica', 'Lead assistant', 'AFF2', 'hmc', 'demo', NULL, 'hmc@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(201) 222-222222', '(222) 222-222222', 'Affordable Dental 2', 'las veags', 'AHM', 3631, '25552', 'Active', '2022-05-29 01:54:45', '2022-08-03 02:38:25'),
(7, 11, 'isaac Parra', 'Lead assistant', 'ADGO', 'isyncevolution.com', 'Note', NULL, 'krupesh.thakkar@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '(123) 456-123644', '(123) 121-125425', 'Alexander dental Group and orthodontic', 'Las Vegas', 'Las Vegas', 3634, '132456', 'Active', '2022-05-29 01:55:04', '2022-03-02 10:44:21'),
(9, 11, 'Dr. Evangeline', 'Owner', 'GLVD', 'https://www.greaterlasvegasdental.com/', 'no notes', NULL, 'Drcahang@glvd.com', '566576e8e4b786cabf2f669259811145', '(000) 000-0000', '(000) 000-0000', 'Greater Las Vegas Dental', '8867 W. Flamingo Rd # 100', 'Las vegas', 3652, '89147', 'Active', '2022-05-29 01:55:14', '2022-05-01 19:12:51'),
(11, 11, 'text contact', 'test title', '3060', 'test', 'test note', NULL, 'testemail@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(155) 555-555556', '(656) 556-565656', 'test office', 'address test', 'test city', 3633, '12365', 'Active', '2022-05-26 11:30:35', '2022-05-26 11:30:35'),
(14, 64, 'Alexander', 'Alexander Office New', '4455', 'AlexanderOffice.com', 'Alexander', NULL, 'Alexande@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(454) 521-2155', '(545) 454-545515', 'Alexander Office New', 'Alexander Office New', 'Alexander Office New', 2722, '4545455', 'Active', '2022-08-03 02:26:40', '2022-08-03 02:28:18'),
(15, 65, 'Office 601', 'Office 601', '4455', 'Office601.com', 'Office 601', NULL, 'Office601@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(644) 551-212122', '(211) 212-1211', 'Office 601', 'test', 'test', 2722, '6562232', 'Active', '2022-08-03 03:03:56', '2022-08-03 03:03:56');

-- --------------------------------------------------------

--
-- Table structure for table `office_admin`
--

CREATE TABLE `office_admin` (
  `iOfficeAdminId` int NOT NULL,
  `iCustomerId` int NOT NULL,
  `vName` varchar(255) NOT NULL,
  `vEmail` varchar(255) NOT NULL,
  `vPassword` varchar(255) NOT NULL,
  `vMobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `vCellulor` varchar(255) NOT NULL,
  `eStatus` enum('Active','Inactive','Stand By','Suspended') NOT NULL,
  `dtAddedDate` datetime NOT NULL,
  `dtUpdatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `office_admin`
--

INSERT INTO `office_admin` (`iOfficeAdminId`, `iCustomerId`, `vName`, `vEmail`, `vPassword`, `vMobile`, `vCellulor`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(2, 11, 'abbas', 'abbas@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(545) 212-1212', '(121) 212-1222', 'Active', '2022-08-04 01:49:14', '2022-08-03 04:44:08'),
(3, 67, 'Offfice', 'Offfice@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '(123) 464-6464', '(464) 666-4644', 'Active', '2022-08-03 06:51:27', '2022-08-03 06:51:27'),
(4, 68, 'Test User for Office', 'testuseroffice@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '(121) 212-1212', '(121) 212-1212', 'Active', '2022-08-03 07:10:25', '2022-08-03 07:10:25'),
(5, 11, 'New Office Admin', 'officeAdmin@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(145) 451-2121', '(212) 212-1212', 'Active', '2022-08-04 01:50:12', '2022-08-04 01:50:12');

-- --------------------------------------------------------

--
-- Table structure for table `productstage`
--

CREATE TABLE `productstage` (
  `iProductStageId` int NOT NULL,
  `iCategoryId` int NOT NULL,
  `iCategoryProductId` int DEFAULT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vCode` varchar(255) DEFAULT NULL,
  `iSequence` int DEFAULT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `vPrice` varchar(255) DEFAULT NULL,
  `iPickUpDay` int DEFAULT NULL,
  `iProcessDay` int DEFAULT NULL,
  `iDeliverDay` int DEFAULT NULL,
  `eType` enum('Upper','Lower','Both') DEFAULT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `eExtractions` enum('Yes','No') DEFAULT NULL,
  `eGumShades` enum('Yes','No') DEFAULT NULL,
  `eImpressions` enum('Yes','No') DEFAULT NULL,
  `eTeethShades` enum('Yes','No') DEFAULT NULL,
  `eTeethInMouth` enum('Yes','No') NOT NULL,
  `eTeethInDefault` enum('Yes','No') NOT NULL,
  `eTeethInRequird` enum('Yes','No') NOT NULL,
  `eTeethInOptional` enum('Yes','No') NOT NULL,
  `eMessingTeeth` enum('Yes','No') NOT NULL,
  `eMessingDefault` enum('Yes','No') NOT NULL,
  `eMessingRequird` enum('Yes','No') NOT NULL,
  `eMessingOptional` enum('Yes','No') NOT NULL,
  `eExtractDelivery` enum('Yes','No') NOT NULL,
  `eExtractDefault` enum('Yes','No') NOT NULL,
  `eExtractRequird` enum('Yes','No') NOT NULL,
  `eExtractOptional` enum('Yes','No') NOT NULL,
  `eExtracted` enum('Yes','No') NOT NULL,
  `eExtractedDefault` enum('Yes','No') NOT NULL,
  `eExtractedRequird` enum('Yes','No') NOT NULL,
  `eExtractedOptional` enum('Yes','No') NOT NULL,
  `eFixorAdd` enum('Yes','No') NOT NULL,
  `eFixorAddDefault` enum('Yes','No') NOT NULL,
  `eFixorAddRequird` enum('Yes','No') NOT NULL,
  `eFixorAddOptional` enum('Yes','No') NOT NULL,
  `eClasps` enum('Yes','No') NOT NULL,
  `eClaspsDefault` enum('Yes','No') NOT NULL,
  `eClaspsRequird` enum('Yes','No') NOT NULL,
  `eClaspsOptional` enum('Yes','No') NOT NULL,
  `eOpposingExtractions` enum('Yes','No') NOT NULL,
  `eOpposingGumShades` enum('Yes','No') NOT NULL,
  `eOpposingTeethShades` enum('Yes','No') NOT NULL,
  `eOpposingImpressions` enum('Yes','No') NOT NULL,
  `eOpposingTeethInMouth` enum('Yes','No') NOT NULL,
  `eOpposingTeethInDefault` enum('Yes','No') NOT NULL,
  `eOpposingMessingTeeth` enum('Yes','No') NOT NULL,
  `eOpposingMessingDefault` enum('Yes','No') NOT NULL,
  `eOpposingExtractDelivery` enum('Yes','No') NOT NULL,
  `eOpposingExtractDefault` enum('Yes','No') NOT NULL,
  `eOpposingExtractedDefault` enum('Yes','No') NOT NULL,
  `eOpposingFixorAdd` enum('Yes','No') NOT NULL,
  `eOpposingFixorAddDefault` enum('Yes','No') NOT NULL,
  `eOpposingClasps` enum('Yes','No') NOT NULL,
  `eOpposingClaspsDefault` enum('Yes','No') NOT NULL,
  `eOpposingExtracted` enum('Yes','No') NOT NULL,
  `eReleasing` enum('Yes','No') NOT NULL DEFAULT 'No',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `productstage`
--

INSERT INTO `productstage` (`iProductStageId`, `iCategoryId`, `iCategoryProductId`, `iCustomerId`, `vCode`, `iSequence`, `vName`, `vPrice`, `iPickUpDay`, `iProcessDay`, `iDeliverDay`, `eType`, `eStatus`, `eExtractions`, `eGumShades`, `eImpressions`, `eTeethShades`, `eTeethInMouth`, `eTeethInDefault`, `eTeethInRequird`, `eTeethInOptional`, `eMessingTeeth`, `eMessingDefault`, `eMessingRequird`, `eMessingOptional`, `eExtractDelivery`, `eExtractDefault`, `eExtractRequird`, `eExtractOptional`, `eExtracted`, `eExtractedDefault`, `eExtractedRequird`, `eExtractedOptional`, `eFixorAdd`, `eFixorAddDefault`, `eFixorAddRequird`, `eFixorAddOptional`, `eClasps`, `eClaspsDefault`, `eClaspsRequird`, `eClaspsOptional`, `eOpposingExtractions`, `eOpposingGumShades`, `eOpposingTeethShades`, `eOpposingImpressions`, `eOpposingTeethInMouth`, `eOpposingTeethInDefault`, `eOpposingMessingTeeth`, `eOpposingMessingDefault`, `eOpposingExtractDelivery`, `eOpposingExtractDefault`, `eOpposingExtractedDefault`, `eOpposingFixorAdd`, `eOpposingFixorAddDefault`, `eOpposingClasps`, `eOpposingClaspsDefault`, `eOpposingExtracted`, `eReleasing`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(6, 16, 7, NULL, 'CT', 5, 'Custom Tray', '25', 1, 2, 1, 'Both', 'Active', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-07 04:28:24', '2022-03-25 13:39:21'),
(7, 13, 2, NULL, 'BB', 10, 'bite block', '55', 1, 2, 1, 'Both', 'Active', 'Yes', 'No', 'Yes', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'No', 'Yes', 'Yes', 'No', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'No', '2022-04-30 02:32:13', '2022-03-25 13:53:14'),
(8, 13, 2, NULL, 'TWT', 20, 'Try in  with teeth', '75', 1, 4, 1, 'Both', 'Active', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'Yes', 'No', 'Yes', 'No', 'Yes', 'No', '2022-05-12 04:19:19', '2022-03-30 13:52:00'),
(9, 13, 2, NULL, 'FN', 25, 'Finish', '75', 1, 4, 1, 'Both', 'Active', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '2022-05-07 05:25:20', '2022-04-02 01:35:10'),
(10, 13, 2, 11, 'CT', 2, 'Custom Tray', '24', 1, 2, 1, 'Both', 'Active', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', 'No', 'No', 'Yes', 'No', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-08-01 10:29:25', '2022-04-09 15:03:54'),
(11, 17, 4, NULL, 'FN', 5, 'Finish', '70', 0, 1, 0, 'Both', 'Active', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-06 05:45:26', '2022-04-13 03:37:55'),
(12, 17, 6, NULL, 'FN', 10, 'Finish', '50', 0, 1, 0, 'Both', 'Active', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-06 05:16:32', '2022-04-14 04:32:30'),
(13, 16, 3, NULL, 'BB', 10, 'Bite Block', '55', 1, 2, 1, 'Both', 'Active', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-08 05:21:38', '2022-04-23 14:05:37'),
(14, 16, 3, NULL, 'TWT', 15, 'Try in  with teeth', '75', 1, 4, 1, 'Both', 'Active', 'Yes', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-08 05:17:28', '2022-04-23 14:24:56'),
(15, 16, 3, NULL, 'FN', 20, 'Finish', '85', 1, 4, 2, 'Both', 'Active', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-08 05:23:39', '2022-04-23 14:37:52'),
(16, 16, 3, NULL, 'CT', 5, 'Custom Tray', '25', 1, 2, 1, 'Both', 'Active', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-08 05:17:00', '2022-05-01 17:55:24'),
(17, 16, 7, NULL, 'BB', 15, 'Bite block', '35', 1, 2, 1, 'Both', 'Active', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-07 04:28:44', '2022-05-01 17:56:41'),
(18, 16, 7, NULL, 'TWT', 20, 'Try in  with teeth', '75', 1, 4, 1, 'Both', 'Active', 'Yes', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-07 04:29:10', '2022-05-01 17:57:47'),
(19, 16, 7, NULL, 'FN', 25, 'Finish', '1', 1, 1, 4, 'Both', 'Active', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-07 04:29:32', '2022-05-01 17:59:06'),
(20, 19, 8, NULL, 'FN', 5, 'Finish', '50', 1, 1, 1, 'Both', 'Active', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-06 05:37:37', '2022-05-01 19:31:21'),
(21, 17, 11, NULL, 'FN', 5, 'Finish', '50', 0, 1, 0, 'Both', 'Active', 'Yes', 'No', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'No', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'Yes', 'No', 'Yes', 'No', '2022-05-03 06:39:33', '2022-05-03 06:35:24'),
(22, 19, 9, NULL, 'FN', 5, 'Finish', '50', 1, 1, 1, 'Both', 'Active', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'Yes', '2022-05-06 05:37:55', '2022-05-04 04:46:41'),
(23, 19, 10, NULL, 'FN', 5, 'Finish', '60', 1, 1, 1, 'Both', 'Active', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'Yes', '2022-05-06 05:38:28', '2022-05-04 04:48:43'),
(24, 16, 12, 11, 'CT', 5, 'Custom Tray', '25', 1, 3, 1, 'Both', 'Active', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-08-01 10:29:31', '2022-05-14 04:52:51'),
(25, 16, 12, NULL, 'BB', 10, 'Bite Block', '55', 1, 3, 1, 'Both', 'Active', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-14 04:54:13', '2022-05-14 04:54:13'),
(26, 16, 12, NULL, 'TWT', 20, 'Try in  with teeth', '75', 1, 4, 1, 'Both', 'Active', 'Yes', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', '2022-05-14 04:55:53', '2022-05-14 04:55:53'),
(27, 16, 12, NULL, 'FN', 25, 'Finish', '85', 1, 4, 1, 'Both', 'Active', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'Yes', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'Yes', '2022-05-14 04:56:56', '2022-05-14 04:56:56'),
(28, 22, 14, NULL, 'ST1', 10, 'ST1', '50', 2, 5, 2, 'Both', 'Active', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'Yes', '2022-05-27 07:12:43', '2022-05-23 07:30:04'),
(30, 25, 23, NULL, '002', 25, 'test stage', '50', 2, 2, 2, 'Both', 'Active', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'No', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', 'No', 'No', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', '2022-05-26 12:32:10', '2022-05-26 12:32:10'),
(58, 22, 14, 11, 'TEST', 22, 'TEST', '62', 6, 6, 6, 'Both', 'Active', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'Yes', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', '2022-08-02 05:15:45', '2022-08-02 05:15:45');

-- --------------------------------------------------------

--
-- Table structure for table `product_grade`
--

CREATE TABLE `product_grade` (
  `iCategoryProductId` int DEFAULT NULL,
  `iGradeId` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `product_grade`
--

INSERT INTO `product_grade` (`iCategoryProductId`, `iGradeId`) VALUES
(1, 3),
(2, 3),
(2, 4),
(3, 3),
(3, 4),
(3, 3),
(3, 4),
(5, 3),
(5, 4),
(7, 3),
(7, 4),
(13, 3),
(13, 4),
(13, 5),
(15, 3),
(16, 3),
(16, 5),
(12, 3),
(12, 4),
(19, 3),
(19, 4),
(19, 5),
(20, 3),
(20, 4),
(20, 5),
(21, 3),
(21, 4),
(21, 5),
(22, 3),
(22, 4),
(22, 5),
(23, 3),
(23, 4),
(23, 5),
(24, 3),
(24, 4),
(24, 5),
(25, 3),
(25, 4),
(25, 5),
(26, 3),
(26, 4),
(26, 5),
(27, 3),
(14, 3),
(14, 4);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `iRoleId` int NOT NULL,
  `vRole` varchar(255) DEFAULT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`iRoleId`, `vRole`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(1, 'Super Admin', 'Active', '2022-03-16 09:03:28', NULL),
(2, 'Customer', 'Active', '2022-03-16 09:03:41', NULL),
(3, 'Office', 'Active', '2022-03-16 09:03:57', NULL),
(4, 'Doctor', 'Active', '2022-03-16 09:04:06', NULL),
(5, 'User', 'Active', '2022-03-16 09:04:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `iScheduleId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `iYear` int DEFAULT NULL,
  `dtHolidays` date DEFAULT NULL,
  `eMonth` enum('January','February','March','April','May','June','July','August','September','October','November','December','All') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `iDay` int DEFAULT NULL,
  `eWeek` enum('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','All') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `eWeekMonth` enum('First Week','Second Week','Third Week','Fourth Week','Fifth Week','All') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tiOpen` time DEFAULT NULL,
  `tiClose` time DEFAULT NULL,
  `vTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `eStatus` enum('OPEN','CLOSED') NOT NULL,
  `dtAddedDate` datetime NOT NULL,
  `dtUpdatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`iScheduleId`, `iCustomerId`, `iYear`, `dtHolidays`, `eMonth`, `iDay`, `eWeek`, `eWeekMonth`, `tiOpen`, `tiClose`, `vTitle`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(1, 11, NULL, NULL, 'All', NULL, 'Sunday', 'All', '10:00:00', '03:00:00', 'Weekend', 'CLOSED', '2022-04-06 00:00:00', '2022-05-25 08:48:24'),
(2, 11, NULL, NULL, 'All', NULL, 'Monday', 'All', '10:00:00', '03:00:00', 'Open', 'OPEN', '2022-04-06 00:00:00', '2022-05-25 08:48:24'),
(3, 11, NULL, NULL, 'All', NULL, 'Tuesday', 'All', '10:00:00', '20:00:00', 'Open', 'OPEN', '2022-04-06 00:00:00', '2022-05-25 08:48:24'),
(4, 11, NULL, NULL, 'All', NULL, 'Wednesday', 'All', '10:00:00', '03:00:00', 'Open', 'OPEN', '2022-04-06 00:00:00', '2022-05-25 08:48:24'),
(5, 11, NULL, NULL, 'All', NULL, 'Thursday', 'All', '10:00:00', '04:00:00', 'Open', 'OPEN', '2022-04-06 00:00:00', '2022-05-25 08:48:24'),
(6, 11, NULL, NULL, 'All', NULL, 'Friday', 'All', '00:00:00', '00:00:00', 'Weekend', 'CLOSED', '2022-04-06 00:00:00', '2022-05-25 08:48:24'),
(7, 11, NULL, NULL, 'All', NULL, 'Saturday', 'All', '00:00:00', '00:00:00', 'Weekend', 'CLOSED', '2022-04-06 00:00:00', '2022-04-06 00:00:00'),
(9, NULL, NULL, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'OPEN', '2022-04-06 16:04:37', '2022-04-06 16:04:37'),
(10, 11, NULL, '2022-04-23', NULL, NULL, NULL, NULL, NULL, NULL, 'Holyday', 'CLOSED', '2022-04-06 14:44:27', '2022-04-06 14:44:27'),
(11, 11, NULL, '2022-05-14', NULL, NULL, NULL, NULL, NULL, NULL, 'wee', 'CLOSED', '2022-05-20 07:29:11', '2022-05-20 07:29:11'),
(12, 12, NULL, '2022-05-26', NULL, NULL, NULL, NULL, NULL, NULL, 'famous day', 'OPEN', '2022-05-24 09:19:55', '2022-05-24 09:20:58'),
(13, 12, NULL, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 'test', 'OPEN', '2022-05-24 09:21:50', '2022-05-24 09:46:39'),
(14, 12, NULL, '2022-06-22', NULL, NULL, NULL, NULL, NULL, NULL, 'end of day', 'OPEN', '2022-05-24 10:03:30', '2022-05-24 10:03:30'),
(15, 11, NULL, '2022-06-22', NULL, NULL, NULL, NULL, NULL, NULL, 'test', 'CLOSED', '2022-06-21 11:18:52', '2022-06-21 11:18:52');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int NOT NULL,
  `vName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `vDesc` varchar(255) NOT NULL,
  `vValue` text,
  `iSettingOrder` int DEFAULT NULL,
  `eConfigType` enum('Company','Appearance','Email','Meta','SMTP','SMS','Preferences','Config','Authenticate','Social') DEFAULT NULL,
  `eDisplayType` enum('text','selectbox','textarea','checkbox','hidden','editor','file','readonly','password') DEFAULT NULL,
  `eSource` varchar(255) DEFAULT NULL,
  `vSourceValue` text,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `vName`, `vDesc`, `vValue`, `iSettingOrder`, `eConfigType`, `eDisplayType`, `eSource`, `vSourceValue`, `eStatus`, `created_at`, `updated_at`) VALUES
(1, 'CPANEL_TITLE', 'Site Control Panel Title', 'MegaMart Admin Control', 1, 'Appearance', 'text', NULL, NULL, 'Active', NULL, '2022-01-06 12:32:21'),
(2, 'DEFAULT_LATITUDE', 'Default latitude', '38.8976805', 2, 'Appearance', 'text', NULL, NULL, 'Inactive', NULL, NULL),
(3, 'ADMIN_DATE_FORMAT', 'Date Format', 'dfmt_12::dfmt_12', 3, 'Appearance', 'selectbox', 'List', 'dfmt_1::dfmt_1,dfmt_2::dfmt_2,dfmt_3::dfmt_3,dfmt_4::dfmt_4,dfmt_5::dfmt_5,dfmt_6::dfmt_6,dfmt_7::dfmt_7,dfmt_8::dfmt_8,dfmt_9::dfmt_9,dfmt_10::dfmt_10,dfmt_11::dfmt_11,dfmt_12::dfmt_12,dfmt_13::dfmt_13,dfmt_14::dfmt_14,dfmt_15::dfmt_15,dfmt_16::dfmt_16,dfmt_17::dfmt_17,dfmt_18::dfmt_18,dfmt_19::dfmt_19,dfmt_20::dfmt_20', 'Inactive', NULL, '2022-01-04 09:39:37'),
(4, 'ADMIN_THEME_ACTIVATE', 'Activate Theme', 'Y', 5, 'Appearance', 'checkbox', NULL, NULL, 'Inactive', NULL, '2022-01-04 08:47:42'),
(5, 'COMPANY_FAVICON', 'Company Favicon', 'favicon.png', 6, 'Appearance', 'file', NULL, '{\"FILE_EXT\":\"\",\"FILE_SIZE\":\"500\"}', 'Inactive', NULL, '2022-01-04 09:30:03'),
(6, 'ADMIN_THEME_SETTINGS', 'Theme Settings', 'cit@default@', 7, 'Preferences', 'readonly', NULL, NULL, 'Inactive', NULL, NULL),
(7, 'ADMIN_SETTIMG_PASSWORD', 'Admin Setting Password', '123456', 8, 'Appearance', 'password', NULL, NULL, 'Inactive', NULL, NULL),
(8, 'COMPANY_ADDRESS', 'Company address', 'Shop 8/29-31 Porter St, Ryde NSW 2112', 1, 'Company', 'textarea', NULL, NULL, 'Active', NULL, '2022-02-23 12:41:50'),
(9, 'COMPANY_DESC', 'Company Description', 'hmc', 2, 'Company', 'text', NULL, NULL, 'Active', NULL, '2022-02-23 12:41:50'),
(10, 'COMPANY_EMAIL', 'Company Email ID', 'info@hmc.com', 3, 'Company', 'text', NULL, NULL, 'Active', NULL, '2022-02-23 12:41:50'),
(11, 'COMPANY_LOGO', 'Company Logo', 'logo.png', 4, 'Company', 'file', NULL, '{\"FILE_EXT\":\"\",\"FILE_SIZE\":\"500\"}', 'Active', NULL, '2022-01-04 09:57:07'),
(12, 'COMPANY_NAME', 'Company Name', 'hmc', 5, 'Company', 'text', NULL, NULL, 'Active', NULL, '2022-02-23 12:41:50'),
(13, 'ADMIN_EMAIL', 'Administrator Email Address', 'jay@mailinator.com', 9, 'Email', 'text', NULL, NULL, 'Active', NULL, NULL),
(14, 'EMAIL_PROTOCOL', 'Email Protocol', 'tls', 10, 'Email', 'text', '', '', 'Active', NULL, NULL),
(15, 'SENDGRID_API_KEY', 'Sendgrid API Key', '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', 1, 'Email', 'text', NULL, '', 'Inactive', NULL, NULL),
(16, 'SENDGRID_FROM_EMAIL', 'Sendgrid From Email', 'krupesh.isync@gmail.com', 2, 'Email', 'text', NULL, '', 'Inactive', NULL, NULL),
(17, 'SENDGRID_FROM_NAME', 'Sendgrid From Name', 'Support EESAuction', 3, 'Email', 'text', NULL, '', 'Inactive', NULL, NULL),
(18, 'SMTP_HOST', 'SMTP Server Host', 'smtp.hostinger.in', 4, 'Email', 'text', NULL, '', 'Active', NULL, NULL),
(19, 'SMTP_PASS', 'SMTP Server Password', 'Email@95959', 6, 'Email', 'text', NULL, '', 'Active', NULL, NULL),
(20, 'SMTP_PORT', 'SMTP Server Port', '587', 7, 'Email', 'text', NULL, '', 'Active', NULL, NULL),
(21, 'SMTP_USERNAME', 'SMTP Server User name', 'email@demo-available.com', 5, 'Email', 'text', NULL, '', 'Active', NULL, NULL),
(22, 'SUPPORT_EMAIL', 'Support Email Address', 'support@eesauction.com', 5, 'Email', 'text', NULL, '', 'Active', NULL, NULL),
(23, 'META_DESCRIPTION', 'Meta Description', 'hmc', 3, 'Meta', 'textarea', '', '', 'Active', NULL, '2022-02-23 12:42:16'),
(24, 'META_KEYWORD', 'Meta Keyword', 'hmc', 2, 'Meta', 'textarea', '', '', 'Active', NULL, '2022-02-23 12:42:16'),
(25, 'META_OTHER', 'Other SEO Related META Tags', '', 4, 'Meta', 'textarea', '', '', 'Inactive', NULL, NULL),
(26, 'META_TITLE', 'Meta Title', 'hmc.com.au', 1, 'Meta', 'textarea', '', '', 'Active', NULL, '2022-02-23 12:42:16'),
(27, 'SEO_FRIENDLY_URL', 'SEO Friendly URL', 'N', 5, 'Meta', 'checkbox', '', '', 'Inactive', NULL, NULL),
(34, 'SOCIAL_FACEBOOK', 'Facebook Profile URL', 'https://www.facebook.com/', 1, 'Social', 'text', NULL, NULL, 'Active', NULL, NULL),
(35, 'SOCIAL_GOOGLE_PLUS', 'Google Plus URL', 'https://plus.google.com', 3, 'Social', 'text', NULL, NULL, 'Active', NULL, NULL),
(36, 'SOCIAL_INSTAGRAM', 'Instagram Profile Url', 'https://www.instagram.com/', 7, 'Social', 'text', NULL, NULL, 'Active', NULL, NULL),
(37, 'SOCIAL_LINKEDIN', 'Linked URL', 'https://linkedin.com', 5, 'Social', 'text', NULL, NULL, 'Active', NULL, NULL),
(38, 'SOCIAL_PINTEREST', 'Pinterest URL', 'https://pinterest.com', 6, 'Social', 'text', NULL, NULL, 'Active', NULL, NULL),
(39, 'SOCIAL_TWITTER', 'Twitter Profile URL', 'https://www.twitter.com/', 2, 'Social', 'text', NULL, NULL, 'Active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slip`
--

CREATE TABLE `slip` (
  `iSlipId` int NOT NULL,
  `iCaseId` int DEFAULT NULL,
  `vSlipNumber` varchar(255) DEFAULT NULL,
  `iOfficeId` int DEFAULT NULL,
  `iDoctorId` int DEFAULT NULL,
  `vPatientName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `iCasepanId` int DEFAULT NULL,
  `vCasePanNumber` varchar(255) DEFAULT NULL,
  `vCaseNumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `iCreatedById` int DEFAULT NULL,
  `vCreatedByName` varchar(255) DEFAULT NULL,
  `eCreatedByType` enum('Admin','Customer','Office','Doctor','User') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `eStatus` enum('Draft','On Process','Canceled','Finished','On Hold') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `vLocation` varchar(255) DEFAULT NULL,
  `dPickUpDate` date DEFAULT NULL,
  `dDeliveryDate` date DEFAULT NULL,
  `tDeliveryTime` time DEFAULT NULL,
  `dUpperRushDate` date DEFAULT NULL,
  `dLowerRushDate` date DEFAULT NULL,
  `tStagesNotes` longtext,
  `dtAddedDate` datetime NOT NULL,
  `dtUpdatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `slip`
--

INSERT INTO `slip` (`iSlipId`, `iCaseId`, `vSlipNumber`, `iOfficeId`, `iDoctorId`, `vPatientName`, `iCasepanId`, `vCasePanNumber`, `vCaseNumber`, `iCreatedById`, `vCreatedByName`, `eCreatedByType`, `eStatus`, `vLocation`, `dPickUpDate`, `dDeliveryDate`, `tDeliveryTime`, `dUpperRushDate`, `dLowerRushDate`, `tStagesNotes`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(1, 1, '0001', 11, 34, 'abbas', 1, NULL, '0001', 11, 'Horacio', 'Admin', 'On Process', 'In office', '2022-07-26', '2022-08-01', '04:00:00', NULL, NULL, 'test', '2022-07-25 22:41:26', '2022-07-25 10:41:52'),
(2, 1, '0003', NULL, NULL, 'abbas', 1, NULL, '0001', 11, NULL, 'Admin', 'On Process', 'In office', '2022-07-26', '2022-08-01', '04:00:00', '2022-07-26', '2022-07-27', NULL, '2022-07-25 10:43:13', '2022-07-25 10:43:00'),
(3, 2, '0003', 11, 34, 'new test', 1, NULL, '0002', 11, 'Horacio', 'Admin', 'On Process', 'In office ready to pickup', '2022-07-26', '2022-08-01', '04:00:00', NULL, NULL, 'new test', '2022-07-26 06:43:39', '2022-07-26 06:43:39'),
(4, 3, '0004', 9, 25, 'abbas', 1, NULL, '0003', 11, 'Horacio', 'Admin', 'On Process', 'In office ready to pickup', '2022-07-26', '2022-08-01', '04:00:00', NULL, NULL, NULL, '2022-07-26 06:46:37', '2022-07-26 06:46:37'),
(5, 4, '0004', 9, 25, 'abbas', 1, NULL, '0003', 11, 'Horacio', 'Admin', 'On Process', 'In office ready to pickup', '2022-07-26', '2022-08-01', '04:00:00', NULL, NULL, NULL, '2022-07-26 06:46:57', '2022-07-26 06:46:57'),
(6, 1, '0006', 11, 34, 'abbas', 1, NULL, '0001', 11, 'Horacio', 'Admin', 'On Process', 'On route to the lab', NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-29 23:40:56', '2022-07-29 11:41:09'),
(7, 8, '0007', 11, 34, 'qasim', 1, NULL, '0008', 11, 'Horacio', 'Admin', 'On Process', 'In office', '2022-08-01', '2022-08-04', '04:00:00', NULL, NULL, 'qasim', '2022-07-29 23:50:49', '2022-07-30 12:02:08'),
(8, 8, '0008', 11, 34, 'qasim', 1, NULL, '0008', 11, 'Horacio', 'Admin', 'On Process', 'In office', NULL, NULL, NULL, NULL, NULL, 'ok', '2022-07-30 00:05:39', '2022-07-30 12:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `slip_addons`
--

CREATE TABLE `slip_addons` (
  `iSlipAddonsId` int NOT NULL,
  `iSlipId` int DEFAULT NULL,
  `iCaseId` int DEFAULT NULL,
  `iAddCategoryId` int DEFAULT NULL,
  `iSubAddCategoryId` int DEFAULT NULL,
  `iQuantity` int DEFAULT NULL,
  `eType` enum('Upper','Lower') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `slip_attachment`
--

CREATE TABLE `slip_attachment` (
  `iSlipAttachmentId` int NOT NULL,
  `iTampId` int DEFAULT NULL,
  `iSlipId` int DEFAULT NULL,
  `iCaseId` int DEFAULT NULL,
  `eType` enum('Image','Video','Signature','Audio') DEFAULT NULL,
  `vName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `slip_attachment`
--

INSERT INTO `slip_attachment` (`iSlipAttachmentId`, `iTampId`, `iSlipId`, `iCaseId`, `eType`, `vName`) VALUES
(1, 857444, 2, 1, 'Image', 'photo-1570295999919-56ceb5ecca61.jpeg'),
(2, NULL, 3, 0, 'Image', 'photo-1570295999919-56ceb5ecca61.jpeg'),
(3, 545053, NULL, NULL, 'Image', 'photo-1570295999919-56ceb5ecca61.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `slip_impression`
--

CREATE TABLE `slip_impression` (
  `iSlipImpressionId` int NOT NULL,
  `iSlipId` int DEFAULT NULL,
  `iImpressionId` int DEFAULT NULL,
  `iQuantity` int DEFAULT NULL,
  `eType` enum('Upper','Lower','Opposite') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `slip_impression`
--

INSERT INTO `slip_impression` (`iSlipImpressionId`, `iSlipId`, `iImpressionId`, `iQuantity`, `eType`) VALUES
(1, 1, 4, 3, 'Upper'),
(2, 2, 4, 3, 'Upper'),
(3, 2, 4, 3, 'Lower'),
(4, 3, 4, 3, 'Upper'),
(5, 3, 10, 8, 'Opposite'),
(6, 6, 6, 3, 'Upper'),
(7, 7, 4, 2, 'Upper'),
(8, 7, 7, 8, 'Opposite'),
(9, 8, 10, 9, 'Upper'),
(10, 8, 5, 8, 'Opposite');

-- --------------------------------------------------------

--
-- Table structure for table `slip_notes`
--

CREATE TABLE `slip_notes` (
  `iSlipNotesId` int NOT NULL,
  `iSlipId` int DEFAULT NULL,
  `iStageId` varchar(255) DEFAULT NULL,
  `iCaseId` int DEFAULT NULL,
  `vSlipNumber` varchar(255) DEFAULT NULL,
  `vStageName` varchar(255) DEFAULT NULL,
  `tStagesNotes` longtext,
  `vCaseNumber` varchar(255) DEFAULT NULL,
  `dDeliveryDate` date DEFAULT NULL,
  `eNewSlipAdded` enum('No','Yes') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `vCreatedByName` varchar(255) DEFAULT NULL,
  `dtAddedDate` datetime DEFAULT NULL,
  `tAddedTime` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `slip_notes`
--

INSERT INTO `slip_notes` (`iSlipNotesId`, `iSlipId`, `iStageId`, `iCaseId`, `vSlipNumber`, `vStageName`, `tStagesNotes`, `vCaseNumber`, `dDeliveryDate`, `eNewSlipAdded`, `vCreatedByName`, `dtAddedDate`, `tAddedTime`) VALUES
(1, 1, '16', 1, '0001', 'Custom Tray', 'test', '0001', '2022-08-01', 'Yes', 'Horacio', '2022-07-25 10:41:26', '10:41:00'),
(2, 2, '13/16', 1, '0003', 'Bite Block/Custom Tray', 'ok', '0001', '1969-12-31', 'No', 'Horacio', '2022-07-25 22:44:58', '22:44:00'),
(3, 3, '16', 2, '0003', 'Custom Tray', 'new test', '0002', '2022-08-01', 'Yes', 'Horacio', '2022-07-26 06:43:40', '06:43:00'),
(4, 7, '6', 8, '0007', 'Custom Tray', 'qasim', '0008', '2022-08-04', 'Yes', 'Horacio', '2022-07-29 11:50:49', '11:50:00'),
(5, 8, '17', 8, '0008', 'Bite block', 'ok', '0009', '2022-08-04', 'No', 'Horacio', '2022-07-30 12:05:39', '12:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `slip_product`
--

CREATE TABLE `slip_product` (
  `iSlipProductId` int NOT NULL,
  `iSlipId` int DEFAULT NULL,
  `eType` enum('Upper','Lower','Oppising') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `iCategoryId` int DEFAULT NULL,
  `iProductId` int DEFAULT NULL,
  `iGradeId` int DEFAULT NULL,
  `iStageId` int DEFAULT NULL,
  `eExtraction` enum('Yes','No') NOT NULL DEFAULT 'No',
  `eGumShade` enum('Yes','No') NOT NULL DEFAULT 'No',
  `eTeethShade` enum('Yes','No') NOT NULL DEFAULT 'No',
  `eImpression` enum('Yes','No') NOT NULL DEFAULT 'No',
  `iToothBrandId` int DEFAULT NULL,
  `iToothShadesId` int DEFAULT NULL,
  `iGumBrandId` int DEFAULT NULL,
  `iGumShadesId` int DEFAULT NULL,
  `vTeethInMouth` varchar(255) DEFAULT NULL,
  `vMissingTeeth` varchar(255) DEFAULT NULL,
  `vWillExtractOnDelivery` varchar(255) DEFAULT NULL,
  `vHasBeenExtracted` varchar(255) DEFAULT NULL,
  `vFixOrAdd` varchar(255) DEFAULT NULL,
  `vClasps` varchar(255) DEFAULT NULL,
  `vTeethInMouthOp` varchar(255) DEFAULT NULL,
  `vMissingTeethOp` varchar(255) DEFAULT NULL,
  `vWillExtractOnDeliveryOp` varchar(255) DEFAULT NULL,
  `vHasBeenExtractedOp` varchar(255) DEFAULT NULL,
  `vFixOrAddOp` varchar(255) DEFAULT NULL,
  `vClaspsOp` varchar(255) DEFAULT NULL,
  `eStatus` enum('Draft','On Process','Canceled','Finished','On Hold') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Draft',
  `dPickUpDate` date DEFAULT NULL,
  `dDeliveryDate` date DEFAULT NULL,
  `tDeliveryTime` time DEFAULT NULL,
  `eRushDate` enum('Yes','No') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'No',
  `dDeliveryRushDate` date DEFAULT NULL,
  `eCustomDate` enum('No','Yes') NOT NULL,
  `dtAddedDate` datetime NOT NULL,
  `dtUpdatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `slip_product`
--

INSERT INTO `slip_product` (`iSlipProductId`, `iSlipId`, `eType`, `iCategoryId`, `iProductId`, `iGradeId`, `iStageId`, `eExtraction`, `eGumShade`, `eTeethShade`, `eImpression`, `iToothBrandId`, `iToothShadesId`, `iGumBrandId`, `iGumShadesId`, `vTeethInMouth`, `vMissingTeeth`, `vWillExtractOnDelivery`, `vHasBeenExtracted`, `vFixOrAdd`, `vClasps`, `vTeethInMouthOp`, `vMissingTeethOp`, `vWillExtractOnDeliveryOp`, `vHasBeenExtractedOp`, `vFixOrAddOp`, `vClaspsOp`, `eStatus`, `dPickUpDate`, `dDeliveryDate`, `tDeliveryTime`, `eRushDate`, `dDeliveryRushDate`, `eCustomDate`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(1, 1, 'Upper', 16, 3, 3, 16, 'Yes', 'No', 'No', 'Yes', NULL, NULL, NULL, NULL, NULL, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16', NULL, NULL, NULL, NULL, '17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32', NULL, NULL, NULL, NULL, NULL, 'Finished', '2022-07-26', '2022-08-01', '16:00:00', 'No', NULL, 'No', '2022-07-25 22:41:26', '2022-07-25 10:41:52'),
(2, 2, 'Upper', 16, 3, 3, 13, 'Yes', 'No', 'No', 'Yes', NULL, NULL, NULL, NULL, NULL, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finished', '2022-07-26', '2022-08-01', '16:00:00', 'Yes', '2022-07-26', 'No', '2022-07-25 22:42:35', '2022-07-25 10:43:00'),
(3, 2, 'Lower', 16, 3, 3, 16, 'Yes', 'No', 'No', 'Yes', NULL, NULL, NULL, NULL, NULL, '17,18,19,20,21,22,23,24,25,26,28,29,31,32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finished', '2022-07-26', '2022-08-01', '16:00:00', 'Yes', '2022-07-27', 'No', '2022-07-25 22:42:35', '2022-07-25 10:43:00'),
(4, 3, 'Upper', 16, 3, 3, 16, 'Yes', 'No', 'No', 'Yes', NULL, NULL, NULL, NULL, NULL, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16', NULL, NULL, NULL, NULL, '17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32', NULL, NULL, NULL, NULL, NULL, 'Draft', '2022-07-26', '2022-08-01', '16:00:00', 'No', NULL, 'No', '2022-07-26 06:43:39', '2022-07-26 06:43:39'),
(5, 6, 'Upper', 16, 3, 3, 14, 'Yes', 'No', 'Yes', 'Yes', 6, 3, NULL, NULL, NULL, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'On Process', '2022-08-01', '2022-08-09', '16:00:00', 'No', NULL, 'No', '2022-07-29 23:40:56', '2022-07-29 11:41:09'),
(6, 6, 'Lower', 16, 3, 3, NULL, 'No', 'No', 'No', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'On Process', NULL, NULL, NULL, 'No', NULL, 'No', '2022-07-29 23:40:56', '2022-07-29 11:41:09'),
(7, 7, 'Upper', 16, 7, 3, 6, 'Yes', 'No', 'No', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16', NULL, NULL, NULL, '17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32', NULL, NULL, NULL, NULL, NULL, 'Finished', '2022-08-01', '2022-08-04', '16:00:00', 'No', NULL, 'No', '2022-07-29 23:50:49', '2022-07-30 12:02:08'),
(8, 8, 'Upper', 16, 7, 3, 17, 'Yes', 'No', 'No', 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finished', '2022-08-01', '2022-08-04', '16:00:00', 'No', NULL, 'No', '2022-07-30 00:05:39', '2022-07-30 12:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `iStaffId` int NOT NULL,
  `iLoginAdminId` int DEFAULT NULL,
  `iCustomerId` int DEFAULT NULL,
  `iDepartmentId` int DEFAULT NULL,
  `vMiddleInitial` varchar(255) DEFAULT NULL,
  `vFirstName` varchar(255) DEFAULT NULL,
  `vLastName` varchar(255) DEFAULT NULL,
  `vUserName` varchar(255) DEFAULT NULL,
  `vEmail` varchar(255) DEFAULT NULL,
  `vPassword` varchar(255) DEFAULT NULL,
  `vMobile` varchar(255) DEFAULT NULL,
  `tDescription` text,
  `eStatus` enum('Active','Inactive','In Vacation','Fired','Resigned','Sick','Suspended') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`iStaffId`, `iLoginAdminId`, `iCustomerId`, `iDepartmentId`, `vMiddleInitial`, `vFirstName`, `vLastName`, `vUserName`, `vEmail`, `vPassword`, `vMobile`, `tDescription`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(3, 6, 11, 1, 'G', 'Dr Krupesh', 'Thakkar', 'dgwegs', 'admin@mailinator.com', 'aaa', '(111) 111-1111', 'fafdfafa', 'Active', '2022-06-06 10:12:31', '2022-06-06 10:12:31'),
(5, 7, 11, 2, 'zayn malik', 'kasim abbas', 'test', 'zayn malik', 'zayn@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '(251) 545-6662', 'dads', 'Active', '2022-07-21 11:27:54', NULL),
(6, 8, 57, 2, 'ronaldo', 'Cristiano', 'Cristiano', 'Cristiano', 'ronaldo@ronaldo.com', 'e10adc3949ba59abbe56e057f20f883e', '(454) 212-1254', 'dsadas', 'Active', '2022-07-21 11:49:53', '2022-07-21 23:49:13'),
(7, 9, 58, 2, 'Krupesh Thakkar', 'Krupesh Thakkar', NULL, 'Krupesh Thakkar', 'krupesh001@mailinator.com', 'fbfeb7f1f5730254fbdd84ad458eacbf', '(121) 212-1212', NULL, 'Active', '2022-07-22 02:08:54', NULL),
(8, 10, 59, 2, 'ab test', 'ab test', NULL, 'ab test', 'ab_test@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(545) 212-2222', NULL, 'Active', '2022-07-22 03:23:57', NULL),
(9, 6, NULL, 1, 'test', 'test', NULL, 'test', 'test@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '77894452121', NULL, 'Active', '2022-07-22 03:40:07', NULL),
(10, 7, NULL, 1, 'zayn', 'zayn', 'zayn', 'zayn', 'zayn@gmail.com', NULL, '(788) 545-5121', 'zayn@gmail.com', 'Active', '2022-07-22 04:51:14', '2022-07-22 04:51:05'),
(11, 1, NULL, 1, 'Horacio', 'Horacio', NULL, 'Horacio', 'admin@mailinator.com', '0192023a7bbd73250516f069df18b500', '9723486463', NULL, 'Active', '2022-07-22 05:08:51', NULL),
(12, 38, 56, 3, 'justin', 'justin', 'test', 'justin', 'justin@mailinator.com', NULL, '(545) 441-2121', 'test', 'Active', '2022-07-22 11:53:22', '2022-07-22 23:27:55'),
(13, 30, 11, 4, 'KTTTT', 'KTTTT', 'KTTTT', 'KTTTT', 'KTTTT@mailinator.com', NULL, '(226) 262-3233', 'KTTTT', 'Active', '2022-07-23 01:39:29', '2022-07-23 00:32:07'),
(14, 39, 11, 3, 'ganpat', 'ganpat', NULL, 'ganpat', 'ganpat@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(454) 552-2231', NULL, 'Active', '2022-07-23 02:12:16', NULL),
(15, 31, 11, 4, 'Kaminey', 'Kaminey', NULL, 'Kaminey', 'Kaminey@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(253) 435-1212', NULL, 'Active', '2022-07-23 02:13:42', NULL),
(16, 11, 61, 2, 'hasnain', 'hasnain', NULL, 'hasnain', 'admin@mailinator.com', '0192023a7bbd73250516f069df18b500', '(545) 515-4212', NULL, 'Active', '2022-07-23 02:55:09', NULL),
(17, 8, NULL, 1, 'hasnain', 'hasnain', NULL, 'hasnain', 'test@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', 'hasnain', NULL, 'Active', '2022-07-25 03:25:09', NULL),
(18, 32, 11, 4, 'ab007', 'ab007', NULL, 'ab007', 'ab007@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(551) 212-1212', NULL, 'Active', '2022-07-26 04:11:23', NULL),
(19, NULL, NULL, 5, 'driver', 'driver', 'driver', 'driver', 'driver@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(445) 511-2121', 'driver', 'Active', '2022-07-30 01:34:27', '2022-07-30 01:34:27'),
(20, 12, 11, 2, 'Lab Admin', 'Lab Admin', NULL, 'Lab Admin', 'labAdmin@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(515) 112-1211', NULL, 'Active', '2022-08-01 04:46:11', NULL),
(21, 13, 62, 2, 'LabAdmin', 'LabAdmin', NULL, 'LabAdmin', 'admin@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(454) 545-2121', NULL, 'Active', '2022-08-01 05:12:39', NULL),
(22, NULL, NULL, 3, 'Docor Ab', 'Docor Ab', 'Docor Ab', 'Docor Ab', 'DocorAb@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(111) 212-1121', 'Docor Ab', 'Active', '2022-08-02 06:16:15', '2022-08-02 06:05:09'),
(23, 14, 0, 2, 'Alexander', 'Alexander', 'Alexander', 'Alexander', 'Alexande@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(454) 521-2155', 'Alexander', 'Active', '2022-08-03 02:28:18', '2022-08-03 00:27:08'),
(24, 15, 65, 2, 'Office 601', 'Office 601', NULL, 'Office 601', 'Office601@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(644) 551-212122', NULL, 'Active', '2022-08-03 03:03:56', NULL),
(25, 1, 11, 2, 'test', 'test', NULL, 'test', 'testoffice@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(545) 451-2121', NULL, 'Active', '2022-08-03 04:13:19', NULL),
(26, 2, 0, 2, 'abbas', 'abbas', 'abbas', 'abbas', 'abbas@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(545) 212-1212', 'abbas', 'Active', '2022-08-03 04:44:08', NULL),
(27, 3, 67, 2, 'Offfice', 'Offfice', NULL, 'Offfice', 'Offfice@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '(123) 464-6464', NULL, 'Active', '2022-08-03 06:51:27', NULL),
(28, 4, 68, 2, 'Test User for Office', 'Test User for Office', NULL, 'Test User for Office', 'testuseroffice@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '(121) 212-1212', NULL, 'Active', '2022-08-03 07:10:25', NULL),
(29, 14, 11, 2, 'hasnain', 'hasnain', NULL, 'hasnain', 'test@mailinator.com', '0192023a7bbd73250516f069df18b500', '(545) 212-1212', NULL, 'Active', '2022-08-03 10:32:17', NULL),
(30, 15, 69, 2, 'labcus@gmail.com', 'labcus@gmail.com', NULL, 'labcus@gmail.com', 'labcus@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '(121) 212-1212', NULL, 'Active', '2022-08-03 10:33:46', NULL),
(31, 16, 70, 2, 'Harvy Spector', 'Harvy Spector', NULL, 'Harvy Spector', 'harvy@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '(123) 456-7890', NULL, 'Active', '2022-08-03 10:38:03', NULL),
(32, 5, 11, 2, 'New Office Admin', 'New Office Admin', NULL, 'New Office Admin', 'officeAdmin@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(145) 451-2121', NULL, 'Active', '2022-08-04 01:50:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `iStateId` int NOT NULL,
  `vState` varchar(32) NOT NULL,
  `vStateCode` varchar(32) NOT NULL,
  `iCountryId` int DEFAULT NULL,
  `vCountryCode` varchar(10) DEFAULT NULL,
  `eStatus` enum('Active','Inactive') DEFAULT NULL,
  `iSysRecDeleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`iStateId`, `vState`, `vStateCode`, `iCountryId`, `vCountryCode`, `eStatus`, `iSysRecDeleted`) VALUES
(1, 'Badakhshan', 'BDS', 1, 'AF', 'Active', 0),
(2, 'Badghis', 'BDG', 1, 'AF', 'Active', 0),
(3, 'Baghlan', 'BGL', 1, 'AF', 'Active', 0),
(4, 'Balkh', 'BAL', 1, 'AF', 'Active', 0),
(5, 'Bamian', 'BAM', 1, 'AF', 'Active', 0),
(6, 'Farah', 'FRA', 1, 'AF', 'Active', 0),
(7, 'Faryab', 'FYB', 1, 'AF', 'Active', 0),
(8, 'Ghazni', 'GHA', 1, 'AF', 'Active', 0),
(9, 'Ghowr', 'GHO', 1, 'AF', 'Active', 0),
(10, 'Helmand', 'HEL', 1, 'AF', 'Active', 0),
(11, 'Herat', 'HER', 1, 'AF', 'Active', 0),
(12, 'Jowzjan', 'JOW', 1, 'AF', 'Active', 0),
(13, 'Kabul', 'KAB', 1, 'AF', 'Active', 0),
(14, 'Kandahar', 'KAN', 1, 'AF', 'Active', 0),
(15, 'Kapisa', 'KAP', 1, 'AF', 'Active', 0),
(16, 'Khost', 'KHO', 1, 'AF', 'Active', 0),
(17, 'Konar', 'KNR', 1, 'AF', 'Active', 0),
(18, 'Kondoz', 'KDZ', 1, 'AF', 'Active', 0),
(19, 'Laghman', 'LAG', 1, 'AF', 'Active', 0),
(20, 'Lowgar', 'LOW', 1, 'AF', 'Active', 0),
(21, 'Nangrahar', 'NAN', 1, 'AF', 'Active', 0),
(22, 'Nimruz', 'NIM', 1, 'AF', 'Active', 0),
(23, 'Nurestan', 'NUR', 1, 'AF', 'Active', 0),
(24, 'Oruzgan', 'ORU', 1, 'AF', 'Active', 0),
(25, 'Paktia', 'PIA', 1, 'AF', 'Active', 0),
(26, 'Paktika', 'PKA', 1, 'AF', 'Active', 0),
(27, 'Parwan', 'PAR', 1, 'AF', 'Active', 0),
(28, 'Samangan', 'SAM', 1, 'AF', 'Active', 0),
(29, 'Sar-e Pol', 'SAR', 1, 'AF', 'Active', 0),
(30, 'Takhar', 'TAK', 1, 'AF', 'Active', 0),
(31, 'Wardak', 'WAR', 1, 'AF', 'Active', 0),
(32, 'Zabol', 'ZAB', 1, 'AF', 'Active', 0),
(69, 'Adrar', 'ADR', 3, 'DZ', 'Active', 0),
(70, 'Ain Defla', 'ADE', 3, 'DZ', 'Active', 0),
(71, 'Ain Temouchent', 'ATE', 3, 'DZ', 'Active', 0),
(72, 'Alger', 'ALG', 3, 'DZ', 'Active', 0),
(73, 'Annaba', 'ANN', 3, 'DZ', 'Active', 0),
(74, 'Batna', 'BAT', 3, 'DZ', 'Active', 0),
(75, 'Bechar', 'BEC', 3, 'DZ', 'Active', 0),
(76, 'Bejaia', 'BEJ', 3, 'DZ', 'Active', 0),
(77, 'Biskra', 'BIS', 3, 'DZ', 'Active', 0),
(78, 'Blida', 'BLI', 3, 'DZ', 'Active', 0),
(79, 'Bordj Bou Arreridj', 'BBA', 3, 'DZ', 'Active', 0),
(80, 'Bouira', 'BOA', 3, 'DZ', 'Active', 0),
(81, 'Boumerdes', 'BMD', 3, 'DZ', 'Active', 0),
(82, 'Chlef', 'CHL', 3, 'DZ', 'Active', 0),
(83, 'Constantine', 'CON', 3, 'DZ', 'Active', 0),
(84, 'Djelfa', 'DJE', 3, 'DZ', 'Active', 0),
(85, 'El Bayadh', 'EBA', 3, 'DZ', 'Active', 0),
(86, 'El Oued', 'EOU', 3, 'DZ', 'Active', 0),
(87, 'El Tarf', 'ETA', 3, 'DZ', 'Active', 0),
(88, 'Ghardaia', 'GHA', 3, 'DZ', 'Active', 0),
(89, 'Guelma', 'GUE', 3, 'DZ', 'Active', 0),
(90, 'Illizi', 'ILL', 3, 'DZ', 'Active', 0),
(91, 'Jijel', 'JIJ', 3, 'DZ', 'Active', 0),
(92, 'Khenchela', 'KHE', 3, 'DZ', 'Active', 0),
(93, 'Laghouat', 'LAG', 3, 'DZ', 'Active', 0),
(94, 'Muaskar', 'MUA', 3, 'DZ', 'Active', 0),
(95, 'Medea', 'MED', 3, 'DZ', 'Active', 0),
(96, 'Mila', 'MIL', 3, 'DZ', 'Active', 0),
(97, 'Mostaganem', 'MOS', 3, 'DZ', 'Active', 0),
(98, 'M\'Sila', 'MSI', 3, 'DZ', 'Active', 0),
(99, 'Naama', 'NAA', 3, 'DZ', 'Active', 0),
(100, 'Oran', 'ORA', 3, 'DZ', 'Active', 0),
(101, 'Ouargla', 'OUA', 3, 'DZ', 'Active', 0),
(102, 'Oum el-Bouaghi', 'OEB', 3, 'DZ', 'Active', 0),
(103, 'Relizane', 'REL', 3, 'DZ', 'Active', 0),
(104, 'Saida', 'SAI', 3, 'DZ', 'Active', 0),
(105, 'Setif', 'SET', 3, 'DZ', 'Active', 0),
(106, 'Sidi Bel Abbes', 'SBA', 3, 'DZ', 'Active', 0),
(107, 'Skikda', 'SKI', 3, 'DZ', 'Active', 0),
(108, 'Souk Ahras', 'SAH', 3, 'DZ', 'Active', 0),
(109, 'Tamanghasset', 'TAM', 3, 'DZ', 'Active', 0),
(110, 'Tebessa', 'TEB', 3, 'DZ', 'Active', 0),
(111, 'Tiaret', 'TIA', 3, 'DZ', 'Active', 0),
(112, 'Tindouf', 'TIN', 3, 'DZ', 'Active', 0),
(113, 'Tipaza', 'TIP', 3, 'DZ', 'Active', 0),
(114, 'Tissemsilt', 'TIS', 3, 'DZ', 'Active', 0),
(115, 'Tizi Ouzou', 'TOU', 3, 'DZ', 'Active', 0),
(116, 'Tlemcen', 'TLE', 3, 'DZ', 'Active', 0),
(117, 'Eastern', 'E', 4, 'AS', 'Active', 0),
(118, 'Manu\'a', 'M', 4, 'AS', 'Active', 0),
(119, 'Rose Island', 'R', 4, 'AS', 'Active', 0),
(120, 'Swains Island', 'S', 4, 'AS', 'Active', 0),
(121, 'Western', 'W', 4, 'AS', 'Active', 0),
(122, 'Andorra la Vella', 'ALV', 5, 'AD', 'Active', 0),
(123, 'Canillo', 'CAN', 5, 'AD', 'Active', 0),
(124, 'Encamp', 'ENC', 5, 'AD', 'Active', 0),
(125, 'Escaldes-Engordany', 'ESE', 5, 'AD', 'Active', 0),
(126, 'La Massana', 'LMA', 5, 'AD', 'Active', 0),
(127, 'Ordino', 'ORD', 5, 'AD', 'Active', 0),
(128, 'Sant Julia de L?ria', 'SJL', 5, 'AD', 'Active', 0),
(129, 'Bengo', 'BGO', 6, 'AO', 'Active', 0),
(130, 'Benguela', 'BGU', 6, 'AO', 'Active', 0),
(131, 'Bie', 'BIE', 6, 'AO', 'Active', 0),
(132, 'Cabinda', 'CAB', 6, 'AO', 'Active', 0),
(133, 'Cuando-Cubango', 'CCU', 6, 'AO', 'Active', 0),
(134, 'Cuanza Norte', 'CNO', 6, 'AO', 'Active', 0),
(135, 'Cuanza Sul', 'CUS', 6, 'AO', 'Active', 0),
(136, 'Cunene', 'CNN', 6, 'AO', 'Active', 0),
(137, 'Huambo', 'HUA', 6, 'AO', 'Active', 0),
(138, 'Huila', 'HUI', 6, 'AO', 'Active', 0),
(139, 'Luanda', 'LUA', 6, 'AO', 'Active', 0),
(140, 'Lunda Norte', 'LNO', 6, 'AO', 'Active', 0),
(141, 'Lunda Sul', 'LSU', 6, 'AO', 'Active', 0),
(142, 'Malange', 'MAL', 6, 'AO', 'Active', 0),
(143, 'Moxico', 'MOX', 6, 'AO', 'Active', 0),
(144, 'Namibe', 'NAM', 6, 'AO', 'Active', 0),
(145, 'Uige', 'UIG', 6, 'AO', 'Active', 0),
(146, 'Zaire', 'ZAI', 6, 'AO', 'Active', 0),
(147, 'Saint George', 'ASG', 9, 'AG', 'Active', 0),
(148, 'Saint John', 'ASJ', 9, 'AG', 'Active', 0),
(149, 'Saint Mary', 'ASM', 9, 'AG', 'Active', 0),
(150, 'Saint Paul', 'ASL', 9, 'AG', 'Active', 0),
(151, 'Saint Peter', 'ASR', 9, 'AG', 'Active', 0),
(152, 'Saint Philip', 'ASH', 9, 'AG', 'Active', 0),
(153, 'Barbuda', 'BAR', 9, 'AG', 'Active', 0),
(154, 'Redonda', 'RED', 9, 'AG', 'Active', 0),
(155, 'Antartida e Islas del Atlantico', 'AN', 10, 'AR', 'Active', 0),
(156, 'Buenos Aires', 'BA', 10, 'AR', 'Active', 0),
(157, 'Catamarca', 'CA', 10, 'AR', 'Active', 0),
(158, 'Chaco', 'CH', 10, 'AR', 'Active', 0),
(159, 'Chubut', 'CU', 10, 'AR', 'Active', 0),
(160, 'Cordoba', 'CO', 10, 'AR', 'Active', 0),
(161, 'Corrientes', 'CR', 10, 'AR', 'Active', 0),
(162, 'Capital Federal', 'CF', 10, 'AR', 'Active', 0),
(163, 'Entre Rios', 'ER', 10, 'AR', 'Active', 0),
(164, 'Formosa', 'FO', 10, 'AR', 'Active', 0),
(165, 'Jujuy', 'JU', 10, 'AR', 'Active', 0),
(166, 'La Pampa', 'LP', 10, 'AR', 'Active', 0),
(167, 'La Rioja', 'LR', 10, 'AR', 'Active', 0),
(168, 'Mendoza', 'ME', 10, 'AR', 'Active', 0),
(169, 'Misiones', 'MI', 10, 'AR', 'Active', 0),
(170, 'Neuquen', 'NE', 10, 'AR', 'Active', 0),
(171, 'Rio Negro', 'RN', 10, 'AR', 'Active', 0),
(172, 'Salta', 'SA', 10, 'AR', 'Active', 0),
(173, 'San Juan', 'SJ', 10, 'AR', 'Active', 0),
(174, 'San Luis', 'SL', 10, 'AR', 'Active', 0),
(175, 'Santa Cruz', 'SC', 10, 'AR', 'Active', 0),
(176, 'Santa Fe', 'SF', 10, 'AR', 'Active', 0),
(177, 'Santiago del Estero', 'SD', 10, 'AR', 'Active', 0),
(178, 'Tierra del Fuego', 'TF', 10, 'AR', 'Active', 0),
(179, 'Tucuman', 'TU', 10, 'AR', 'Active', 0),
(180, 'Aragatsotn', 'AGT', 11, 'AM', 'Active', 0),
(181, 'Ararat', 'ARR', 11, 'AM', 'Active', 0),
(182, 'Armavir', 'ARM', 11, 'AM', 'Active', 0),
(183, 'Gegharkunik', 'GEG', 11, 'AM', 'Active', 0),
(184, 'Kotayk', 'KOT', 11, 'AM', 'Active', 0),
(185, 'Lorri', 'LOR', 11, 'AM', 'Active', 0),
(186, 'Shirak', 'SHI', 11, 'AM', 'Active', 0),
(187, 'Syunik', 'SYU', 11, 'AM', 'Active', 0),
(188, 'Tavush', 'TAV', 11, 'AM', 'Active', 0),
(189, 'Vayots\' Dzor', 'VAY', 11, 'AM', 'Active', 0),
(190, 'Yerevan', 'YER', 11, 'AM', 'Active', 0),
(191, 'Australian Capitol Territory', 'ACT', 13, 'AU', 'Active', 0),
(192, 'New South Wales', 'NSW', 13, 'AU', 'Active', 0),
(193, 'Northern Territory', 'NT', 13, 'AU', 'Active', 0),
(194, 'Queensland', 'QLD', 13, 'AU', 'Active', 0),
(195, 'South Australia', 'SA', 13, 'AU', 'Active', 0),
(196, 'Tasmania', 'TAS', 13, 'AU', 'Active', 0),
(197, 'Victoria', 'VIC', 13, 'AU', 'Active', 0),
(198, 'Western Australia', 'WA', 13, 'AU', 'Active', 0),
(199, 'Burgenland', 'BUR', 14, 'AT', 'Active', 0),
(200, 'K&auml;rnten', 'KAR', 14, 'AT', 'Active', 0),
(201, 'Nieder&ouml;esterreich', 'NOS', 14, 'AT', 'Active', 0),
(202, 'Ober&ouml;esterreich', 'OOS', 14, 'AT', 'Active', 0),
(203, 'Salzburg', 'SAL', 14, 'AT', 'Active', 0),
(204, 'Steiermark', 'STE', 14, 'AT', 'Active', 0),
(205, 'Tirol', 'TIR', 14, 'AT', 'Active', 0),
(206, 'Vorarlberg', 'VOR', 14, 'AT', 'Active', 0),
(207, 'Wien', 'WIE', 14, 'AT', 'Active', 0),
(208, 'Ali Bayramli', 'AB', 15, 'AZ', 'Active', 0),
(209, 'Abseron', 'ABS', 15, 'AZ', 'Active', 0),
(210, 'AgcabAdi', 'AGC', 15, 'AZ', 'Active', 0),
(211, 'Agdam', 'AGM', 15, 'AZ', 'Active', 0),
(212, 'Agdas', 'AGS', 15, 'AZ', 'Active', 0),
(213, 'Agstafa', 'AGA', 15, 'AZ', 'Active', 0),
(214, 'Agsu', 'AGU', 15, 'AZ', 'Active', 0),
(215, 'Astara', 'AST', 15, 'AZ', 'Active', 0),
(216, 'Baki', 'BA', 15, 'AZ', 'Active', 0),
(217, 'BabAk', 'BAB', 15, 'AZ', 'Active', 0),
(218, 'BalakAn', 'BAL', 15, 'AZ', 'Active', 0),
(219, 'BArdA', 'BAR', 15, 'AZ', 'Active', 0),
(220, 'Beylaqan', 'BEY', 15, 'AZ', 'Active', 0),
(221, 'Bilasuvar', 'BIL', 15, 'AZ', 'Active', 0),
(222, 'Cabrayil', 'CAB', 15, 'AZ', 'Active', 0),
(223, 'Calilabab', 'CAL', 15, 'AZ', 'Active', 0),
(224, 'Culfa', 'CUL', 15, 'AZ', 'Active', 0),
(225, 'Daskasan', 'DAS', 15, 'AZ', 'Active', 0),
(226, 'Davaci', 'DAV', 15, 'AZ', 'Active', 0),
(227, 'Fuzuli', 'FUZ', 15, 'AZ', 'Active', 0),
(228, 'Ganca', 'GA', 15, 'AZ', 'Active', 0),
(229, 'Gadabay', 'GAD', 15, 'AZ', 'Active', 0),
(230, 'Goranboy', 'GOR', 15, 'AZ', 'Active', 0),
(231, 'Goycay', 'GOY', 15, 'AZ', 'Active', 0),
(232, 'Haciqabul', 'HAC', 15, 'AZ', 'Active', 0),
(233, 'Imisli', 'IMI', 15, 'AZ', 'Active', 0),
(234, 'Ismayilli', 'ISM', 15, 'AZ', 'Active', 0),
(235, 'Kalbacar', 'KAL', 15, 'AZ', 'Active', 0),
(236, 'Kurdamir', 'KUR', 15, 'AZ', 'Active', 0),
(237, 'Lankaran', 'LA', 15, 'AZ', 'Active', 0),
(238, 'Lacin', 'LAC', 15, 'AZ', 'Active', 0),
(239, 'Lankaran', 'LAN', 15, 'AZ', 'Active', 0),
(240, 'Lerik', 'LER', 15, 'AZ', 'Active', 0),
(241, 'Masalli', 'MAS', 15, 'AZ', 'Active', 0),
(242, 'Mingacevir', 'MI', 15, 'AZ', 'Active', 0),
(243, 'Naftalan', 'NA', 15, 'AZ', 'Active', 0),
(244, 'Neftcala', 'NEF', 15, 'AZ', 'Active', 0),
(245, 'Oguz', 'OGU', 15, 'AZ', 'Active', 0),
(246, 'Ordubad', 'ORD', 15, 'AZ', 'Active', 0),
(247, 'Qabala', 'QAB', 15, 'AZ', 'Active', 0),
(248, 'Qax', 'QAX', 15, 'AZ', 'Active', 0),
(249, 'Qazax', 'QAZ', 15, 'AZ', 'Active', 0),
(250, 'Qobustan', 'QOB', 15, 'AZ', 'Active', 0),
(251, 'Quba', 'QBA', 15, 'AZ', 'Active', 0),
(252, 'Qubadli', 'QBI', 15, 'AZ', 'Active', 0),
(253, 'Qusar', 'QUS', 15, 'AZ', 'Active', 0),
(254, 'Saki', 'SA', 15, 'AZ', 'Active', 0),
(255, 'Saatli', 'SAT', 15, 'AZ', 'Active', 0),
(256, 'Sabirabad', 'SAB', 15, 'AZ', 'Active', 0),
(257, 'Sadarak', 'SAD', 15, 'AZ', 'Active', 0),
(258, 'Sahbuz', 'SAH', 15, 'AZ', 'Active', 0),
(259, 'Saki', 'SAK', 15, 'AZ', 'Active', 0),
(260, 'Salyan', 'SAL', 15, 'AZ', 'Active', 0),
(261, 'Sumqayit', 'SM', 15, 'AZ', 'Active', 0),
(262, 'Samaxi', 'SMI', 15, 'AZ', 'Active', 0),
(263, 'Samkir', 'SKR', 15, 'AZ', 'Active', 0),
(264, 'Samux', 'SMX', 15, 'AZ', 'Active', 0),
(265, 'Sarur', 'SAR', 15, 'AZ', 'Active', 0),
(266, 'Siyazan', 'SIY', 15, 'AZ', 'Active', 0),
(267, 'Susa', 'SS', 15, 'AZ', 'Active', 0),
(268, 'Susa', 'SUS', 15, 'AZ', 'Active', 0),
(269, 'Tartar', 'TAR', 15, 'AZ', 'Active', 0),
(270, 'Tovuz', 'TOV', 15, 'AZ', 'Active', 0),
(271, 'Ucar', 'UCA', 15, 'AZ', 'Active', 0),
(272, 'Xankandi', 'XA', 15, 'AZ', 'Active', 0),
(273, 'Xacmaz', 'XAC', 15, 'AZ', 'Active', 0),
(274, 'Xanlar', 'XAN', 15, 'AZ', 'Active', 0),
(275, 'Xizi', 'XIZ', 15, 'AZ', 'Active', 0),
(276, 'Xocali', 'XCI', 15, 'AZ', 'Active', 0),
(277, 'Xocavand', 'XVD', 15, 'AZ', 'Active', 0),
(278, 'Yardimli', 'YAR', 15, 'AZ', 'Active', 0),
(279, 'Yevlax', 'YEV', 15, 'AZ', 'Active', 0),
(280, 'Zangilan', 'ZAN', 15, 'AZ', 'Active', 0),
(281, 'Zaqatala', 'ZAQ', 15, 'AZ', 'Active', 0),
(282, 'Zardab', 'ZAR', 15, 'AZ', 'Active', 0),
(283, 'Naxcivan', 'NX', 15, 'AZ', 'Active', 0),
(284, 'Acklins', 'ACK', 16, 'BS', 'Active', 0),
(285, 'Berry Islands', 'BER', 16, 'BS', 'Active', 0),
(286, 'Bimini', 'BIM', 16, 'BS', 'Active', 0),
(287, 'Black Point', 'BLK', 16, 'BS', 'Active', 0),
(288, 'Cat Island', 'CAT', 16, 'BS', 'Active', 0),
(289, 'Central Abaco', 'CAB', 16, 'BS', 'Active', 0),
(290, 'Central Andros', 'CAN', 16, 'BS', 'Active', 0),
(291, 'Central Eleuthera', 'CEL', 16, 'BS', 'Active', 0),
(292, 'City of Freeport', 'FRE', 16, 'BS', 'Active', 0),
(293, 'Crooked Island', 'CRO', 16, 'BS', 'Active', 0),
(294, 'East Grand Bahama', 'EGB', 16, 'BS', 'Active', 0),
(295, 'Exuma', 'EXU', 16, 'BS', 'Active', 0),
(296, 'Grand Cay', 'GRD', 16, 'BS', 'Active', 0),
(297, 'Harbour Island', 'HAR', 16, 'BS', 'Active', 0),
(298, 'Hope Town', 'HOP', 16, 'BS', 'Active', 0),
(299, 'Inagua', 'INA', 16, 'BS', 'Active', 0),
(300, 'Long Island', 'LNG', 16, 'BS', 'Active', 0),
(301, 'Mangrove Cay', 'MAN', 16, 'BS', 'Active', 0),
(302, 'Mayaguana', 'MAY', 16, 'BS', 'Active', 0),
(303, 'Moore\'s Island', 'MOO', 16, 'BS', 'Active', 0),
(304, 'North Abaco', 'NAB', 16, 'BS', 'Active', 0),
(305, 'North Andros', 'NAN', 16, 'BS', 'Active', 0),
(306, 'North Eleuthera', 'NEL', 16, 'BS', 'Active', 0),
(307, 'Ragged Island', 'RAG', 16, 'BS', 'Active', 0),
(308, 'Rum Cay', 'RUM', 16, 'BS', 'Active', 0),
(309, 'San Salvador', 'SAL', 16, 'BS', 'Active', 0),
(310, 'South Abaco', 'SAB', 16, 'BS', 'Active', 0),
(311, 'South Andros', 'SAN', 16, 'BS', 'Active', 0),
(312, 'South Eleuthera', 'SEL', 16, 'BS', 'Active', 0),
(313, 'Spanish Wells', 'SWE', 16, 'BS', 'Active', 0),
(314, 'West Grand Bahama', 'WGB', 16, 'BS', 'Active', 0),
(315, 'Capital', 'CAP', 17, 'BH', 'Active', 0),
(316, 'Central', 'CEN', 17, 'BH', 'Active', 0),
(317, 'Muharraq', 'MUH', 17, 'BH', 'Active', 0),
(318, 'Northern', 'NOR', 17, 'BH', 'Active', 0),
(319, 'Southern', 'SOU', 17, 'BH', 'Active', 0),
(320, 'Barisal', 'BAR', 18, 'BD', 'Active', 0),
(321, 'Chittagong', 'CHI', 18, 'BD', 'Active', 0),
(322, 'Dhaka', 'DHA', 18, 'BD', 'Active', 0),
(323, 'Khulna', 'KHU', 18, 'BD', 'Active', 0),
(324, 'Rajshahi', 'RAJ', 18, 'BD', 'Active', 0),
(325, 'Sylhet', 'SYL', 18, 'BD', 'Active', 0),
(326, 'Christ Church', 'CC', 19, 'BB', 'Active', 0),
(327, 'Saint Andrew', 'AND', 19, 'BB', 'Active', 0),
(328, 'Saint George', 'GEO', 19, 'BB', 'Active', 0),
(329, 'Saint James', 'JAM', 19, 'BB', 'Active', 0),
(330, 'Saint John', 'JOH', 19, 'BB', 'Active', 0),
(331, 'Saint Joseph', 'JOS', 19, 'BB', 'Active', 0),
(332, 'Saint Lucy', 'LUC', 19, 'BB', 'Active', 0),
(333, 'Saint Michael', 'MIC', 19, 'BB', 'Active', 0),
(334, 'Saint Peter', 'PET', 19, 'BB', 'Active', 0),
(335, 'Saint Philip', 'PHI', 19, 'BB', 'Active', 0),
(336, 'Saint Thomas', 'THO', 19, 'BB', 'Active', 0),
(337, 'Brestskaya (Brest)', 'BR', 20, 'BY', 'Active', 0),
(338, 'Homyel\'skaya (Homyel\')', 'HO', 20, 'BY', 'Active', 0),
(339, 'Horad Minsk', 'HM', 20, 'BY', 'Active', 0),
(340, 'Hrodzyenskaya (Hrodna)', 'HR', 20, 'BY', 'Active', 0),
(341, 'Mahilyowskaya (Mahilyow)', 'MA', 20, 'BY', 'Active', 0),
(342, 'Minskaya', 'MI', 20, 'BY', 'Active', 0),
(343, 'Vitsyebskaya (Vitsyebsk)', 'VI', 20, 'BY', 'Active', 0),
(344, 'Antwerpen', 'VAN', 21, 'BE', 'Active', 0),
(345, 'Brabant Wallon', 'WBR', 21, 'BE', 'Active', 0),
(346, 'Hainaut', 'WHT', 21, 'BE', 'Active', 0),
(347, 'Liege', 'WLG', 21, 'BE', 'Active', 0),
(348, 'Limburg', 'VLI', 21, 'BE', 'Active', 0),
(349, 'Luxembourg', 'WLX', 21, 'BE', 'Active', 0),
(350, 'Namur', 'WNA', 21, 'BE', 'Active', 0),
(351, 'Oost-Vlaanderen', 'VOV', 21, 'BE', 'Active', 0),
(352, 'Vlaams Brabant', 'VBR', 21, 'BE', 'Active', 0),
(353, 'West-Vlaanderen', 'VWV', 21, 'BE', 'Active', 0),
(354, 'Belize', 'BZ', 22, 'BZ', 'Active', 0),
(355, 'Cayo', 'CY', 22, 'BZ', 'Active', 0),
(356, 'Corozal', 'CR', 22, 'BZ', 'Active', 0),
(357, 'Orange Walk', 'OW', 22, 'BZ', 'Active', 0),
(358, 'Stann Creek', 'SC', 22, 'BZ', 'Active', 0),
(359, 'Toledo', 'TO', 22, 'BZ', 'Active', 0),
(360, 'Alibori', 'AL', 23, 'BJ', 'Active', 0),
(361, 'Atakora', 'AK', 23, 'BJ', 'Active', 0),
(362, 'Atlantique', 'AQ', 23, 'BJ', 'Active', 0),
(363, 'Borgou', 'BO', 23, 'BJ', 'Active', 0),
(364, 'Collines', 'CO', 23, 'BJ', 'Active', 0),
(365, 'Donga', 'DO', 23, 'BJ', 'Active', 0),
(366, 'Kouffo', 'KO', 23, 'BJ', 'Active', 0),
(367, 'Littoral', 'LI', 23, 'BJ', 'Active', 0),
(368, 'Mono', 'MO', 23, 'BJ', 'Active', 0),
(369, 'Oueme', 'OU', 23, 'BJ', 'Active', 0),
(370, 'Plateau', 'PL', 23, 'BJ', 'Active', 0),
(371, 'Zou', 'ZO', 23, 'BJ', 'Active', 0),
(372, 'Devonshire', 'DS', 24, 'BM', 'Active', 0),
(373, 'Hamilton City', 'HC', 24, 'BM', 'Active', 0),
(374, 'Hamilton', 'HA', 24, 'BM', 'Active', 0),
(375, 'Paget', 'PG', 24, 'BM', 'Active', 0),
(376, 'Pembroke', 'PB', 24, 'BM', 'Active', 0),
(377, 'Saint George City', 'GC', 24, 'BM', 'Active', 0),
(378, 'Saint George\'s', 'SG', 24, 'BM', 'Active', 0),
(379, 'Sandys', 'SA', 24, 'BM', 'Active', 0),
(380, 'Smith\'s', 'SM', 24, 'BM', 'Active', 0),
(381, 'Southampton', 'SH', 24, 'BM', 'Active', 0),
(382, 'Warwick', 'WA', 24, 'BM', 'Active', 0),
(383, 'Bumthang', 'BUM', 25, 'BT', 'Active', 0),
(384, 'Chukha', 'CHU', 25, 'BT', 'Active', 0),
(385, 'Dagana', 'DAG', 25, 'BT', 'Active', 0),
(386, 'Gasa', 'GAS', 25, 'BT', 'Active', 0),
(387, 'Haa', 'HAA', 25, 'BT', 'Active', 0),
(388, 'Lhuntse', 'LHU', 25, 'BT', 'Active', 0),
(389, 'Mongar', 'MON', 25, 'BT', 'Active', 0),
(390, 'Paro', 'PAR', 25, 'BT', 'Active', 0),
(391, 'Pemagatshel', 'PEM', 25, 'BT', 'Active', 0),
(392, 'Punakha', 'PUN', 25, 'BT', 'Active', 0),
(393, 'Samdrup Jongkhar', 'SJO', 25, 'BT', 'Active', 0),
(394, 'Samtse', 'SAT', 25, 'BT', 'Active', 0),
(395, 'Sarpang', 'SAR', 25, 'BT', 'Active', 0),
(396, 'Thimphu', 'THI', 25, 'BT', 'Active', 0),
(397, 'Trashigang', 'TRG', 25, 'BT', 'Active', 0),
(398, 'Trashiyangste', 'TRY', 25, 'BT', 'Active', 0),
(399, 'Trongsa', 'TRO', 25, 'BT', 'Active', 0),
(400, 'Tsirang', 'TSI', 25, 'BT', 'Active', 0),
(401, 'Wangdue Phodrang', 'WPH', 25, 'BT', 'Active', 0),
(402, 'Zhemgang', 'ZHE', 25, 'BT', 'Active', 0),
(403, 'Beni', 'BEN', 26, 'BO', 'Active', 0),
(404, 'Chuquisaca', 'CHU', 26, 'BO', 'Active', 0),
(405, 'Cochabamba', 'COC', 26, 'BO', 'Active', 0),
(406, 'La Paz', 'LPZ', 26, 'BO', 'Active', 0),
(407, 'Oruro', 'ORU', 26, 'BO', 'Active', 0),
(408, 'Pando', 'PAN', 26, 'BO', 'Active', 0),
(409, 'Potosi', 'POT', 26, 'BO', 'Active', 0),
(410, 'Santa Cruz', 'SCZ', 26, 'BO', 'Active', 0),
(411, 'Tarija', 'TAR', 26, 'BO', 'Active', 0),
(412, 'Brcko district', 'BRO', 27, 'BA', 'Active', 0),
(413, 'Unsko-Sanski Kanton', 'FUS', 27, 'BA', 'Active', 0),
(414, 'Posavski Kanton', 'FPO', 27, 'BA', 'Active', 0),
(415, 'Tuzlanski Kanton', 'FTU', 27, 'BA', 'Active', 0),
(416, 'Zenicko-Dobojski Kanton', 'FZE', 27, 'BA', 'Active', 0),
(417, 'Bosanskopodrinjski Kanton', 'FBP', 27, 'BA', 'Active', 0),
(418, 'Srednjebosanski Kanton', 'FSB', 27, 'BA', 'Active', 0),
(419, 'Hercegovacko-neretvanski Kanton', 'FHN', 27, 'BA', 'Active', 0),
(420, 'Zapadnohercegovacka Zupanija', 'FZH', 27, 'BA', 'Active', 0),
(421, 'Kanton Sarajevo', 'FSA', 27, 'BA', 'Active', 0),
(422, 'Zapadnobosanska', 'FZA', 27, 'BA', 'Active', 0),
(423, 'Banja Luka', 'SBL', 27, 'BA', 'Active', 0),
(424, 'Doboj', 'SDO', 27, 'BA', 'Active', 0),
(425, 'Bijeljina', 'SBI', 27, 'BA', 'Active', 0),
(426, 'Vlasenica', 'SVL', 27, 'BA', 'Active', 0),
(427, 'Sarajevo-Romanija or Sokolac', 'SSR', 27, 'BA', 'Active', 0),
(428, 'Foca', 'SFO', 27, 'BA', 'Active', 0),
(429, 'Trebinje', 'STR', 27, 'BA', 'Active', 0),
(430, 'Central', 'CE', 28, 'BW', 'Active', 0),
(431, 'Ghanzi', 'GH', 28, 'BW', 'Active', 0),
(432, 'Kgalagadi', 'KD', 28, 'BW', 'Active', 0),
(433, 'Kgatleng', 'KT', 28, 'BW', 'Active', 0),
(434, 'Kweneng', 'KW', 28, 'BW', 'Active', 0),
(435, 'Ngamiland', 'NG', 28, 'BW', 'Active', 0),
(436, 'North East', 'NE', 28, 'BW', 'Active', 0),
(437, 'North West', 'NW', 28, 'BW', 'Active', 0),
(438, 'South East', 'SE', 28, 'BW', 'Active', 0),
(439, 'Southern', 'SO', 28, 'BW', 'Active', 0),
(440, 'Acre', 'AC', 30, 'BR', 'Active', 0),
(441, 'Alagoas', 'AL', 30, 'BR', 'Active', 0),
(442, 'Amapa', 'AP', 30, 'BR', 'Active', 0),
(443, 'Amazonas', 'AM', 30, 'BR', 'Active', 0),
(444, 'Bahia', 'BA', 30, 'BR', 'Active', 0),
(445, 'Ceara', 'CE', 30, 'BR', 'Active', 0),
(446, 'Distrito Federal', 'DF', 30, 'BR', 'Active', 0),
(447, 'Espirito Santo', 'ES', 30, 'BR', 'Active', 0),
(448, 'Goias', 'GO', 30, 'BR', 'Active', 0),
(449, 'Maranhao', 'MA', 30, 'BR', 'Active', 0),
(450, 'Mato Grosso', 'MT', 30, 'BR', 'Active', 0),
(451, 'Mato Grosso do Sul', 'MS', 30, 'BR', 'Active', 0),
(452, 'Minas Gerais', 'MG', 30, 'BR', 'Active', 0),
(453, 'Para', 'PA', 30, 'BR', 'Active', 0),
(454, 'Paraiba', 'PB', 30, 'BR', 'Active', 0),
(455, 'Parana', 'PR', 30, 'BR', 'Active', 0),
(456, 'Pernambuco', 'PE', 30, 'BR', 'Active', 0),
(457, 'Piaui', 'PI', 30, 'BR', 'Active', 0),
(458, 'Rio de Janeiro', 'RJ', 30, 'BR', 'Active', 0),
(459, 'Rio Grande do Norte', 'RN', 30, 'BR', 'Active', 0),
(460, 'Rio Grande do Sul', 'RS', 30, 'BR', 'Active', 0),
(461, 'Rondonia', 'RO', 30, 'BR', 'Active', 0),
(462, 'Roraima', 'RR', 30, 'BR', 'Active', 0),
(463, 'Santa Catarina', 'SC', 30, 'BR', 'Active', 0),
(464, 'Sao Paulo', 'SP', 30, 'BR', 'Active', 0),
(465, 'Sergipe', 'SE', 30, 'BR', 'Active', 0),
(466, 'Tocantins', 'TO', 30, 'BR', 'Active', 0),
(467, 'Peros Banhos', 'PB', 31, 'IO', 'Active', 0),
(468, 'Salomon Islands', 'SI', 31, 'IO', 'Active', 0),
(469, 'Nelsons Island', 'NI', 31, 'IO', 'Active', 0),
(470, 'Three Brothers', 'TB', 31, 'IO', 'Active', 0),
(471, 'Eagle Islands', 'EA', 31, 'IO', 'Active', 0),
(472, 'Danger Island', 'DI', 31, 'IO', 'Active', 0),
(473, 'Egmont Islands', 'EG', 31, 'IO', 'Active', 0),
(474, 'Diego Garcia', 'DG', 31, 'IO', 'Active', 0),
(475, 'Belait', 'BEL', 32, 'BN', 'Active', 0),
(476, 'Brunei and Muara', 'BRM', 32, 'BN', 'Active', 0),
(477, 'Temburong', 'TEM', 32, 'BN', 'Active', 0),
(478, 'Tutong', 'TUT', 32, 'BN', 'Active', 0),
(479, 'Blagoevgrad', 'Blagoevgrad', 33, 'BG', 'Active', 0),
(480, 'Burgas', 'Burgas', 33, 'BG', 'Active', 0),
(481, 'Dobrich', 'Dobrich', 33, 'BG', 'Active', 0),
(482, 'Gabrovo', 'Gabrovo', 33, 'BG', 'Active', 0),
(483, 'Haskovo', 'Haskovo', 33, 'BG', 'Active', 0),
(484, 'Kardjali', 'Kardjali', 33, 'BG', 'Active', 0),
(485, 'Kyustendil', 'Kyustendil', 33, 'BG', 'Active', 0),
(486, 'Lovech', 'Lovech', 33, 'BG', 'Active', 0),
(487, 'Montana', 'Montana', 33, 'BG', 'Active', 0),
(488, 'Pazardjik', 'Pazardjik', 33, 'BG', 'Active', 0),
(489, 'Pernik', 'Pernik', 33, 'BG', 'Active', 0),
(490, 'Pleven', 'Pleven', 33, 'BG', 'Active', 0),
(491, 'Plovdiv', 'Plovdiv', 33, 'BG', 'Active', 0),
(492, 'Razgrad', 'Razgrad', 33, 'BG', 'Active', 0),
(493, 'Shumen', 'Shumen', 33, 'BG', 'Active', 0),
(494, 'Silistra', 'Silistra', 33, 'BG', 'Active', 0),
(495, 'Sliven', 'Sliven', 33, 'BG', 'Active', 0),
(496, 'Smolyan', 'Smolyan', 33, 'BG', 'Active', 0),
(497, 'Sofia', 'Sofia', 33, 'BG', 'Active', 0),
(498, 'Sofia - town', 'Sofia - town', 33, 'BG', 'Active', 0),
(499, 'Stara Zagora', 'Stara Zagora', 33, 'BG', 'Active', 0),
(500, 'Targovishte', 'Targovishte', 33, 'BG', 'Active', 0),
(501, 'Varna', 'Varna', 33, 'BG', 'Active', 0),
(502, 'Veliko Tarnovo', 'Veliko Tarnovo', 33, 'BG', 'Active', 0),
(503, 'Vidin', 'Vidin', 33, 'BG', 'Active', 0),
(504, 'Vratza', 'Vratza', 33, 'BG', 'Active', 0),
(505, 'Yambol', 'Yambol', 33, 'BG', 'Active', 0),
(506, 'Bale', 'BAL', 34, 'BF', 'Active', 0),
(507, 'Bam', 'BAM', 34, 'BF', 'Active', 0),
(508, 'Banwa', 'BAN', 34, 'BF', 'Active', 0),
(509, 'Bazega', 'BAZ', 34, 'BF', 'Active', 0),
(510, 'Bougouriba', 'BOR', 34, 'BF', 'Active', 0),
(511, 'Boulgou', 'BLG', 34, 'BF', 'Active', 0),
(512, 'Boulkiemde', 'BOK', 34, 'BF', 'Active', 0),
(513, 'Comoe', 'COM', 34, 'BF', 'Active', 0),
(514, 'Ganzourgou', 'GAN', 34, 'BF', 'Active', 0),
(515, 'Gnagna', 'GNA', 34, 'BF', 'Active', 0),
(516, 'Gourma', 'GOU', 34, 'BF', 'Active', 0),
(517, 'Houet', 'HOU', 34, 'BF', 'Active', 0),
(518, 'Ioba', 'IOA', 34, 'BF', 'Active', 0),
(519, 'Kadiogo', 'KAD', 34, 'BF', 'Active', 0),
(520, 'Kenedougou', 'KEN', 34, 'BF', 'Active', 0),
(521, 'Komondjari', 'KOD', 34, 'BF', 'Active', 0),
(522, 'Kompienga', 'KOP', 34, 'BF', 'Active', 0),
(523, 'Kossi', 'KOS', 34, 'BF', 'Active', 0),
(524, 'Koulpelogo', 'KOL', 34, 'BF', 'Active', 0),
(525, 'Kouritenga', 'KOT', 34, 'BF', 'Active', 0),
(526, 'Kourweogo', 'KOW', 34, 'BF', 'Active', 0),
(527, 'Leraba', 'LER', 34, 'BF', 'Active', 0),
(528, 'Loroum', 'LOR', 34, 'BF', 'Active', 0),
(529, 'Mouhoun', 'MOU', 34, 'BF', 'Active', 0),
(530, 'Nahouri', 'NAH', 34, 'BF', 'Active', 0),
(531, 'Namentenga', 'NAM', 34, 'BF', 'Active', 0),
(532, 'Nayala', 'NAY', 34, 'BF', 'Active', 0),
(533, 'Noumbiel', 'NOU', 34, 'BF', 'Active', 0),
(534, 'Oubritenga', 'OUB', 34, 'BF', 'Active', 0),
(535, 'Oudalan', 'OUD', 34, 'BF', 'Active', 0),
(536, 'Passore', 'PAS', 34, 'BF', 'Active', 0),
(537, 'Poni', 'PON', 34, 'BF', 'Active', 0),
(538, 'Sanguie', 'SAG', 34, 'BF', 'Active', 0),
(539, 'Sanmatenga', 'SAM', 34, 'BF', 'Active', 0),
(540, 'Seno', 'SEN', 34, 'BF', 'Active', 0),
(541, 'Sissili', 'SIS', 34, 'BF', 'Active', 0),
(542, 'Soum', 'SOM', 34, 'BF', 'Active', 0),
(543, 'Sourou', 'SOR', 34, 'BF', 'Active', 0),
(544, 'Tapoa', 'TAP', 34, 'BF', 'Active', 0),
(545, 'Tuy', 'TUY', 34, 'BF', 'Active', 0),
(546, 'Yagha', 'YAG', 34, 'BF', 'Active', 0),
(547, 'Yatenga', 'YAT', 34, 'BF', 'Active', 0),
(548, 'Ziro', 'ZIR', 34, 'BF', 'Active', 0),
(549, 'Zondoma', 'ZOD', 34, 'BF', 'Active', 0),
(550, 'Zoundweogo', 'ZOW', 34, 'BF', 'Active', 0),
(551, 'Bubanza', 'BB', 35, 'BI', 'Active', 0),
(552, 'Bujumbura', 'BJ', 35, 'BI', 'Active', 0),
(553, 'Bururi', 'BR', 35, 'BI', 'Active', 0),
(554, 'Cankuzo', 'CA', 35, 'BI', 'Active', 0),
(555, 'Cibitoke', 'CI', 35, 'BI', 'Active', 0),
(556, 'Gitega', 'GI', 35, 'BI', 'Active', 0),
(557, 'Karuzi', 'KR', 35, 'BI', 'Active', 0),
(558, 'Kayanza', 'KY', 35, 'BI', 'Active', 0),
(559, 'Kirundo', 'KI', 35, 'BI', 'Active', 0),
(560, 'Makamba', 'MA', 35, 'BI', 'Active', 0),
(561, 'Muramvya', 'MU', 35, 'BI', 'Active', 0),
(562, 'Muyinga', 'MY', 35, 'BI', 'Active', 0),
(563, 'Mwaro', 'MW', 35, 'BI', 'Active', 0),
(564, 'Ngozi', 'NG', 35, 'BI', 'Active', 0),
(565, 'Rutana', 'RT', 35, 'BI', 'Active', 0),
(566, 'Ruyigi', 'RY', 35, 'BI', 'Active', 0),
(567, 'Phnom Penh', 'PP', 36, 'KH', 'Active', 0),
(568, 'Preah Seihanu (Kompong Som or Si', 'PS', 36, 'KH', 'Active', 0),
(569, 'Pailin', 'PA', 36, 'KH', 'Active', 0),
(570, 'Keb', 'KB', 36, 'KH', 'Active', 0),
(571, 'Banteay Meanchey', 'BM', 36, 'KH', 'Active', 0),
(572, 'Battambang', 'BA', 36, 'KH', 'Active', 0),
(573, 'Kampong Cham', 'KM', 36, 'KH', 'Active', 0),
(574, 'Kampong Chhnang', 'KN', 36, 'KH', 'Active', 0),
(575, 'Kampong Speu', 'KU', 36, 'KH', 'Active', 0),
(576, 'Kampong Som', 'KO', 36, 'KH', 'Active', 0),
(577, 'Kampong Thom', 'KT', 36, 'KH', 'Active', 0),
(578, 'Kampot', 'KP', 36, 'KH', 'Active', 0),
(579, 'Kandal', 'KL', 36, 'KH', 'Active', 0),
(580, 'Kaoh Kong', 'KK', 36, 'KH', 'Active', 0),
(581, 'Kratie', 'KR', 36, 'KH', 'Active', 0),
(582, 'Mondul Kiri', 'MK', 36, 'KH', 'Active', 0),
(583, 'Oddar Meancheay', 'OM', 36, 'KH', 'Active', 0),
(584, 'Pursat', 'PU', 36, 'KH', 'Active', 0),
(585, 'Preah Vihear', 'PR', 36, 'KH', 'Active', 0),
(586, 'Prey Veng', 'PG', 36, 'KH', 'Active', 0),
(587, 'Ratanak Kiri', 'RK', 36, 'KH', 'Active', 0),
(588, 'Siemreap', 'SI', 36, 'KH', 'Active', 0),
(589, 'Stung Treng', 'ST', 36, 'KH', 'Active', 0),
(590, 'Svay Rieng', 'SR', 36, 'KH', 'Active', 0),
(591, 'Takeo', 'TK', 36, 'KH', 'Active', 0),
(592, 'Adamawa (Adamaoua)', 'ADA', 37, 'CM', 'Active', 0),
(593, 'Centre', 'CEN', 37, 'CM', 'Active', 0),
(594, 'East (Est)', 'EST', 37, 'CM', 'Active', 0),
(595, 'Extreme North (Extr?me-Nord)', 'EXN', 37, 'CM', 'Active', 0),
(596, 'Littoral', 'LIT', 37, 'CM', 'Active', 0),
(597, 'North (Nord)', 'NOR', 37, 'CM', 'Active', 0),
(598, 'Northwest (Nord-Ouest)', 'NOT', 37, 'CM', 'Active', 0),
(599, 'West (Ouest)', 'OUE', 37, 'CM', 'Active', 0),
(600, 'South (Sud)', 'SUD', 37, 'CM', 'Active', 0),
(601, 'Southwest (Sud-Ouest).', 'SOU', 37, 'CM', 'Active', 0),
(602, 'Alberta', 'AB', 38, 'CA', 'Active', 0),
(603, 'British Columbia', 'BC', 38, 'CA', 'Active', 0),
(604, 'Manitoba', 'MB', 38, 'CA', 'Active', 0),
(605, 'New Brunswick', 'NB', 38, 'CA', 'Active', 0),
(606, 'Newfoundland and Labrador', 'NL', 38, 'CA', 'Active', 0),
(607, 'Northwest Territories', 'NT', 38, 'CA', 'Active', 0),
(608, 'Nova Scotia', 'NS', 38, 'CA', 'Active', 0),
(609, 'Nunavut', 'NU', 38, 'CA', 'Active', 0),
(610, 'Ontario', 'ON', 38, 'CA', 'Active', 0),
(611, 'Prince Edward Island', 'PE', 38, 'CA', 'Active', 0),
(612, 'Qu&eacute;bec', 'QC', 38, 'CA', 'Active', 0),
(613, 'Saskatchewan', 'SK', 38, 'CA', 'Active', 0),
(614, 'Yukon Territory', 'YT', 38, 'CA', 'Active', 0),
(615, 'Boa Vista', 'BV', 39, 'CV', 'Active', 0),
(616, 'Brava', 'BR', 39, 'CV', 'Active', 0),
(617, 'Calheta de Sao Miguel', 'CS', 39, 'CV', 'Active', 0),
(618, 'Maio', 'MA', 39, 'CV', 'Active', 0),
(619, 'Mosteiros', 'MO', 39, 'CV', 'Active', 0),
(620, 'Paul', 'PA', 39, 'CV', 'Active', 0),
(621, 'Porto Novo', 'PN', 39, 'CV', 'Active', 0),
(622, 'Praia', 'PR', 39, 'CV', 'Active', 0),
(623, 'Ribeira Grande', 'RG', 39, 'CV', 'Active', 0),
(624, 'Sal', 'SL', 39, 'CV', 'Active', 0),
(625, 'Santa Catarina', 'CA', 39, 'CV', 'Active', 0),
(626, 'Santa Cruz', 'CR', 39, 'CV', 'Active', 0),
(627, 'Sao Domingos', 'SD', 39, 'CV', 'Active', 0),
(628, 'Sao Filipe', 'SF', 39, 'CV', 'Active', 0),
(629, 'Sao Nicolau', 'SN', 39, 'CV', 'Active', 0),
(630, 'Sao Vicente', 'SV', 39, 'CV', 'Active', 0),
(631, 'Tarrafal', 'TA', 39, 'CV', 'Active', 0),
(632, 'Creek', 'CR', 40, 'KY', 'Active', 0),
(633, 'Eastern', 'EA', 40, 'KY', 'Active', 0),
(634, 'Midland', 'ML', 40, 'KY', 'Active', 0),
(635, 'South Town', 'ST', 40, 'KY', 'Active', 0),
(636, 'Spot Bay', 'SP', 40, 'KY', 'Active', 0),
(637, 'Stake Bay', 'SK', 40, 'KY', 'Active', 0),
(638, 'West End', 'WD', 40, 'KY', 'Active', 0),
(639, 'Western', 'WN', 40, 'KY', 'Active', 0),
(640, 'Bamingui-Bangoran', 'BBA', 41, 'CF', 'Active', 0),
(641, 'Basse-Kotto', 'BKO', 41, 'CF', 'Active', 0),
(642, 'Haute-Kotto', 'HKO', 41, 'CF', 'Active', 0),
(643, 'Haut-Mbomou', 'HMB', 41, 'CF', 'Active', 0),
(644, 'Kemo', 'KEM', 41, 'CF', 'Active', 0),
(645, 'Lobaye', 'LOB', 41, 'CF', 'Active', 0),
(646, 'Mambere-Kader', 'MKD', 41, 'CF', 'Active', 0),
(647, 'Mbomou', 'MBO', 41, 'CF', 'Active', 0),
(648, 'Nana-Mambere', 'NMM', 41, 'CF', 'Active', 0),
(649, 'Ombella-M\'Poko', 'OMP', 41, 'CF', 'Active', 0),
(650, 'Ouaka', 'OUK', 41, 'CF', 'Active', 0),
(651, 'Ouham', 'OUH', 41, 'CF', 'Active', 0),
(652, 'Ouham-Pende', 'OPE', 41, 'CF', 'Active', 0),
(653, 'Vakaga', 'VAK', 41, 'CF', 'Active', 0),
(654, 'Nana-Grebizi', 'NGR', 41, 'CF', 'Active', 0),
(655, 'Sangha-Mbaere', 'SMB', 41, 'CF', 'Active', 0),
(656, 'Bangui', 'BAN', 41, 'CF', 'Active', 0),
(657, 'Batha', 'BA', 42, 'TD', 'Active', 0),
(658, 'Biltine', 'BI', 42, 'TD', 'Active', 0),
(659, 'Borkou-Ennedi-Tibesti', 'BE', 42, 'TD', 'Active', 0),
(660, 'Chari-Baguirmi', 'CB', 42, 'TD', 'Active', 0),
(661, 'Guera', 'GU', 42, 'TD', 'Active', 0),
(662, 'Kanem', 'KA', 42, 'TD', 'Active', 0),
(663, 'Lac', 'LA', 42, 'TD', 'Active', 0),
(664, 'Logone Occidental', 'LC', 42, 'TD', 'Active', 0),
(665, 'Logone Oriental', 'LR', 42, 'TD', 'Active', 0),
(666, 'Mayo-Kebbi', 'MK', 42, 'TD', 'Active', 0),
(667, 'Moyen-Chari', 'MC', 42, 'TD', 'Active', 0),
(668, 'Ouaddai', 'OU', 42, 'TD', 'Active', 0),
(669, 'Salamat', 'SA', 42, 'TD', 'Active', 0),
(670, 'Tandjile', 'TA', 42, 'TD', 'Active', 0),
(671, 'Aisen del General Carlos Ibanez', 'AI', 43, 'CL', 'Active', 0),
(672, 'Antofagasta', 'AN', 43, 'CL', 'Active', 0),
(673, 'Araucania', 'AR', 43, 'CL', 'Active', 0),
(674, 'Atacama', 'AT', 43, 'CL', 'Active', 0),
(675, 'Bio-Bio', 'BI', 43, 'CL', 'Active', 0),
(676, 'Coquimbo', 'CO', 43, 'CL', 'Active', 0),
(677, 'Libertador General Bernardo O\'Hi', 'LI', 43, 'CL', 'Active', 0),
(678, 'Los Lagos', 'LL', 43, 'CL', 'Active', 0),
(679, 'Magallanes y de la Antartica Chi', 'MA', 43, 'CL', 'Active', 0),
(680, 'Maule', 'ML', 43, 'CL', 'Active', 0),
(681, 'Region Metropolitana', 'RM', 43, 'CL', 'Active', 0),
(682, 'Tarapaca', 'TA', 43, 'CL', 'Active', 0),
(683, 'Valparaiso', 'VS', 43, 'CL', 'Active', 0),
(684, 'Anhui', 'AN', 44, 'CN', 'Active', 0),
(685, 'Beijing', 'BE', 44, 'CN', 'Active', 0),
(686, 'Chongqing', 'CH', 44, 'CN', 'Active', 0),
(687, 'Fujian', 'FU', 44, 'CN', 'Active', 0),
(688, 'Gansu', 'GA', 44, 'CN', 'Active', 0),
(689, 'Guangdong', 'GU', 44, 'CN', 'Active', 0),
(690, 'Guangxi', 'GX', 44, 'CN', 'Active', 0),
(691, 'Guizhou', 'GZ', 44, 'CN', 'Active', 0),
(692, 'Hainan', 'HA', 44, 'CN', 'Active', 0),
(693, 'Hebei', 'HB', 44, 'CN', 'Active', 0),
(694, 'Heilongjiang', 'HL', 44, 'CN', 'Active', 0),
(695, 'Henan', 'HE', 44, 'CN', 'Active', 0),
(696, 'Hong Kong', 'HK', 44, 'CN', 'Active', 0),
(697, 'Hubei', 'HU', 44, 'CN', 'Active', 0),
(698, 'Hunan', 'HN', 44, 'CN', 'Active', 0),
(699, 'Inner Mongolia', 'IM', 44, 'CN', 'Active', 0),
(700, 'Jiangsu', 'JI', 44, 'CN', 'Active', 0),
(701, 'Jiangxi', 'JX', 44, 'CN', 'Active', 0),
(702, 'Jilin', 'JL', 44, 'CN', 'Active', 0),
(703, 'Liaoning', 'LI', 44, 'CN', 'Active', 0),
(704, 'Macau', 'MA', 44, 'CN', 'Active', 0),
(705, 'Ningxia', 'NI', 44, 'CN', 'Active', 0),
(706, 'Shaanxi', 'SH', 44, 'CN', 'Active', 0),
(707, 'Shandong', 'SA', 44, 'CN', 'Active', 0),
(708, 'Shanghai', 'SG', 44, 'CN', 'Active', 0),
(709, 'Shanxi', 'SX', 44, 'CN', 'Active', 0),
(710, 'Sichuan', 'SI', 44, 'CN', 'Active', 0),
(711, 'Tianjin', 'TI', 44, 'CN', 'Active', 0),
(712, 'Xinjiang', 'XI', 44, 'CN', 'Active', 0),
(713, 'Yunnan', 'YU', 44, 'CN', 'Active', 0),
(714, 'Zhejiang', 'ZH', 44, 'CN', 'Active', 0),
(715, 'Direction Island', 'D', 46, 'CC', 'Active', 0),
(716, 'Home Island', 'H', 46, 'CC', 'Active', 0),
(717, 'Horsburgh Island', 'O', 46, 'CC', 'Active', 0),
(718, 'South Island', 'S', 46, 'CC', 'Active', 0),
(719, 'West Island', 'W', 46, 'CC', 'Active', 0),
(720, 'Amazonas', 'AMZ', 47, 'CO', 'Active', 0),
(721, 'Antioquia', 'ANT', 47, 'CO', 'Active', 0),
(722, 'Arauca', 'ARA', 47, 'CO', 'Active', 0),
(723, 'Atlantico', 'ATL', 47, 'CO', 'Active', 0),
(724, 'Bogota D.C.', 'BDC', 47, 'CO', 'Active', 0),
(725, 'Bolivar', 'BOL', 47, 'CO', 'Active', 0),
(726, 'Boyaca', 'BOY', 47, 'CO', 'Active', 0),
(727, 'Caldas', 'CAL', 47, 'CO', 'Active', 0),
(728, 'Caqueta', 'CAQ', 47, 'CO', 'Active', 0),
(729, 'Casanare', 'CAS', 47, 'CO', 'Active', 0),
(730, 'Cauca', 'CAU', 47, 'CO', 'Active', 0),
(731, 'Cesar', 'CES', 47, 'CO', 'Active', 0),
(732, 'Choco', 'CHO', 47, 'CO', 'Active', 0),
(733, 'Cordoba', 'COR', 47, 'CO', 'Active', 0),
(734, 'Cundinamarca', 'CAM', 47, 'CO', 'Active', 0),
(735, 'Guainia', 'GNA', 47, 'CO', 'Active', 0),
(736, 'Guajira', 'GJR', 47, 'CO', 'Active', 0),
(737, 'Guaviare', 'GVR', 47, 'CO', 'Active', 0),
(738, 'Huila', 'HUI', 47, 'CO', 'Active', 0),
(739, 'Magdalena', 'MAG', 47, 'CO', 'Active', 0),
(740, 'Meta', 'MET', 47, 'CO', 'Active', 0),
(741, 'Narino', 'NAR', 47, 'CO', 'Active', 0),
(742, 'Norte de Santander', 'NDS', 47, 'CO', 'Active', 0),
(743, 'Putumayo', 'PUT', 47, 'CO', 'Active', 0),
(744, 'Quindio', 'QUI', 47, 'CO', 'Active', 0),
(745, 'Risaralda', 'RIS', 47, 'CO', 'Active', 0),
(746, 'San Andres y Providencia', 'SAP', 47, 'CO', 'Active', 0),
(747, 'Santander', 'SAN', 47, 'CO', 'Active', 0),
(748, 'Sucre', 'SUC', 47, 'CO', 'Active', 0),
(749, 'Tolima', 'TOL', 47, 'CO', 'Active', 0),
(750, 'Valle del Cauca', 'VDC', 47, 'CO', 'Active', 0),
(751, 'Vaupes', 'VAU', 47, 'CO', 'Active', 0),
(752, 'Vichada', 'VIC', 47, 'CO', 'Active', 0),
(753, 'Grande Comore', 'G', 48, 'KM', 'Active', 0),
(754, 'Anjouan', 'A', 48, 'KM', 'Active', 0),
(755, 'Moheli', 'M', 48, 'KM', 'Active', 0),
(756, 'Bouenza', 'BO', 49, 'CG', 'Active', 0),
(757, 'Brazzaville', 'BR', 49, 'CG', 'Active', 0),
(758, 'Cuvette', 'CU', 49, 'CG', 'Active', 0),
(759, 'Cuvette-Ouest', 'CO', 49, 'CG', 'Active', 0),
(760, 'Kouilou', 'KO', 49, 'CG', 'Active', 0),
(761, 'Lekoumou', 'LE', 49, 'CG', 'Active', 0),
(762, 'Likouala', 'LI', 49, 'CG', 'Active', 0),
(763, 'Niari', 'NI', 49, 'CG', 'Active', 0),
(764, 'Plateaux', 'PL', 49, 'CG', 'Active', 0),
(765, 'Pool', 'PO', 49, 'CG', 'Active', 0),
(766, 'Sangha', 'SA', 49, 'CG', 'Active', 0),
(767, 'Pukapuka', 'PU', 50, 'CK', 'Active', 0),
(768, 'Rakahanga', 'RK', 50, 'CK', 'Active', 0),
(769, 'Manihiki', 'MK', 50, 'CK', 'Active', 0),
(770, 'Penrhyn', 'PE', 50, 'CK', 'Active', 0),
(771, 'Nassau Island', 'NI', 50, 'CK', 'Active', 0),
(772, 'Surwarrow', 'SU', 50, 'CK', 'Active', 0),
(773, 'Palmerston', 'PA', 50, 'CK', 'Active', 0),
(774, 'Aitutaki', 'AI', 50, 'CK', 'Active', 0),
(775, 'Manuae', 'MA', 50, 'CK', 'Active', 0),
(776, 'Takutea', 'TA', 50, 'CK', 'Active', 0),
(777, 'Mitiaro', 'MT', 50, 'CK', 'Active', 0),
(778, 'Atiu', 'AT', 50, 'CK', 'Active', 0),
(779, 'Mauke', 'MU', 50, 'CK', 'Active', 0),
(780, 'Rarotonga', 'RR', 50, 'CK', 'Active', 0),
(781, 'Mangaia', 'MG', 50, 'CK', 'Active', 0),
(782, 'Alajuela', 'AL', 51, 'CR', 'Active', 0),
(783, 'Cartago', 'CA', 51, 'CR', 'Active', 0),
(784, 'Guanacaste', 'GU', 51, 'CR', 'Active', 0),
(785, 'Heredia', 'HE', 51, 'CR', 'Active', 0),
(786, 'Limon', 'LI', 51, 'CR', 'Active', 0),
(787, 'Puntarenas', 'PU', 51, 'CR', 'Active', 0),
(788, 'San Jose', 'SJ', 51, 'CR', 'Active', 0),
(789, 'Abengourou', 'ABE', 52, 'CI', 'Active', 0),
(790, 'Abidjan', 'ABI', 52, 'CI', 'Active', 0),
(791, 'Aboisso', 'ABO', 52, 'CI', 'Active', 0),
(792, 'Adiake', 'ADI', 52, 'CI', 'Active', 0),
(793, 'Adzope', 'ADZ', 52, 'CI', 'Active', 0),
(794, 'Agboville', 'AGB', 52, 'CI', 'Active', 0),
(795, 'Agnibilekrou', 'AGN', 52, 'CI', 'Active', 0),
(796, 'Alepe', 'ALE', 52, 'CI', 'Active', 0),
(797, 'Bocanda', 'BOC', 52, 'CI', 'Active', 0),
(798, 'Bangolo', 'BAN', 52, 'CI', 'Active', 0),
(799, 'Beoumi', 'BEO', 52, 'CI', 'Active', 0),
(800, 'Biankouma', 'BIA', 52, 'CI', 'Active', 0),
(801, 'Bondoukou', 'BDK', 52, 'CI', 'Active', 0),
(802, 'Bongouanou', 'BGN', 52, 'CI', 'Active', 0),
(803, 'Bouafle', 'BFL', 52, 'CI', 'Active', 0),
(804, 'Bouake', 'BKE', 52, 'CI', 'Active', 0),
(805, 'Bouna', 'BNA', 52, 'CI', 'Active', 0),
(806, 'Boundiali', 'BDL', 52, 'CI', 'Active', 0),
(807, 'Dabakala', 'DKL', 52, 'CI', 'Active', 0),
(808, 'Dabou', 'DBU', 52, 'CI', 'Active', 0),
(809, 'Daloa', 'DAL', 52, 'CI', 'Active', 0),
(810, 'Danane', 'DAN', 52, 'CI', 'Active', 0),
(811, 'Daoukro', 'DAO', 52, 'CI', 'Active', 0),
(812, 'Dimbokro', 'DIM', 52, 'CI', 'Active', 0),
(813, 'Divo', 'DIV', 52, 'CI', 'Active', 0),
(814, 'Duekoue', 'DUE', 52, 'CI', 'Active', 0),
(815, 'Ferkessedougou', 'FER', 52, 'CI', 'Active', 0),
(816, 'Gagnoa', 'GAG', 52, 'CI', 'Active', 0),
(817, 'Grand-Bassam', 'GBA', 52, 'CI', 'Active', 0),
(818, 'Grand-Lahou', 'GLA', 52, 'CI', 'Active', 0),
(819, 'Guiglo', 'GUI', 52, 'CI', 'Active', 0),
(820, 'Issia', 'ISS', 52, 'CI', 'Active', 0),
(821, 'Jacqueville', 'JAC', 52, 'CI', 'Active', 0),
(822, 'Katiola', 'KAT', 52, 'CI', 'Active', 0),
(823, 'Korhogo', 'KOR', 52, 'CI', 'Active', 0),
(824, 'Lakota', 'LAK', 52, 'CI', 'Active', 0),
(825, 'Man', 'MAN', 52, 'CI', 'Active', 0),
(826, 'Mankono', 'MKN', 52, 'CI', 'Active', 0),
(827, 'Mbahiakro', 'MBA', 52, 'CI', 'Active', 0),
(828, 'Odienne', 'ODI', 52, 'CI', 'Active', 0),
(829, 'Oume', 'OUM', 52, 'CI', 'Active', 0),
(830, 'Sakassou', 'SAK', 52, 'CI', 'Active', 0),
(831, 'San-Pedro', 'SPE', 52, 'CI', 'Active', 0),
(832, 'Sassandra', 'SAS', 52, 'CI', 'Active', 0),
(833, 'Seguela', 'SEG', 52, 'CI', 'Active', 0),
(834, 'Sinfra', 'SIN', 52, 'CI', 'Active', 0),
(835, 'Soubre', 'SOU', 52, 'CI', 'Active', 0),
(836, 'Tabou', 'TAB', 52, 'CI', 'Active', 0),
(837, 'Tanda', 'TAN', 52, 'CI', 'Active', 0),
(838, 'Tiebissou', 'TIE', 52, 'CI', 'Active', 0),
(839, 'Tingrela', 'TIN', 52, 'CI', 'Active', 0),
(840, 'Tiassale', 'TIA', 52, 'CI', 'Active', 0),
(841, 'Touba', 'TBA', 52, 'CI', 'Active', 0),
(842, 'Toulepleu', 'TLP', 52, 'CI', 'Active', 0),
(843, 'Toumodi', 'TMD', 52, 'CI', 'Active', 0),
(844, 'Vavoua', 'VAV', 52, 'CI', 'Active', 0),
(845, 'Yamoussoukro', 'YAM', 52, 'CI', 'Active', 0),
(846, 'Zuenoula', 'ZUE', 52, 'CI', 'Active', 0),
(847, 'Bjelovar-Bilogora', 'BB', 53, 'HR', 'Active', 0),
(848, 'City of Zagreb', 'CZ', 53, 'HR', 'Active', 0),
(849, 'Dubrovnik-Neretva', 'DN', 53, 'HR', 'Active', 0),
(850, 'Istra', 'IS', 53, 'HR', 'Active', 0),
(851, 'Karlovac', 'KA', 53, 'HR', 'Active', 0),
(852, 'Koprivnica-Krizevci', 'KK', 53, 'HR', 'Active', 0),
(853, 'Krapina-Zagorje', 'KZ', 53, 'HR', 'Active', 0),
(854, 'Lika-Senj', 'LS', 53, 'HR', 'Active', 0),
(855, 'Medimurje', 'ME', 53, 'HR', 'Active', 0),
(856, 'Osijek-Baranja', 'OB', 53, 'HR', 'Active', 0),
(857, 'Pozega-Slavonia', 'PS', 53, 'HR', 'Active', 0),
(858, 'Primorje-Gorski Kotar', 'PG', 53, 'HR', 'Active', 0),
(859, 'Sibenik', 'SI', 53, 'HR', 'Active', 0),
(860, 'Sisak-Moslavina', 'SM', 53, 'HR', 'Active', 0),
(861, 'Slavonski Brod-Posavina', 'SB', 53, 'HR', 'Active', 0),
(862, 'Split-Dalmatia', 'SD', 53, 'HR', 'Active', 0),
(863, 'Varazdin', 'VA', 53, 'HR', 'Active', 0),
(864, 'Virovitica-Podravina', 'VP', 53, 'HR', 'Active', 0),
(865, 'Vukovar-Srijem', 'VS', 53, 'HR', 'Active', 0),
(866, 'Zadar-Knin', 'ZK', 53, 'HR', 'Active', 0),
(867, 'Zagreb', 'ZA', 53, 'HR', 'Active', 0),
(868, 'Camaguey', 'CA', 54, 'CU', 'Active', 0),
(869, 'Ciego de Avila', 'CD', 54, 'CU', 'Active', 0),
(870, 'Cienfuegos', 'CI', 54, 'CU', 'Active', 0),
(871, 'Ciudad de La Habana', 'CH', 54, 'CU', 'Active', 0),
(872, 'Granma', 'GR', 54, 'CU', 'Active', 0),
(873, 'Guantanamo', 'GU', 54, 'CU', 'Active', 0),
(874, 'Holguin', 'HO', 54, 'CU', 'Active', 0),
(875, 'Isla de la Juventud', 'IJ', 54, 'CU', 'Active', 0),
(876, 'La Habana', 'LH', 54, 'CU', 'Active', 0),
(877, 'Las Tunas', 'LT', 54, 'CU', 'Active', 0),
(878, 'Matanzas', 'MA', 54, 'CU', 'Active', 0),
(879, 'Pinar del Rio', 'PR', 54, 'CU', 'Active', 0),
(880, 'Sancti Spiritus', 'SS', 54, 'CU', 'Active', 0),
(881, 'Santiago de Cuba', 'SC', 54, 'CU', 'Active', 0),
(882, 'Villa Clara', 'VC', 54, 'CU', 'Active', 0),
(883, 'Famagusta', 'F', 55, 'CY', 'Active', 0),
(884, 'Kyrenia', 'K', 55, 'CY', 'Active', 0),
(885, 'Larnaca', 'A', 55, 'CY', 'Active', 0),
(886, 'Limassol', 'I', 55, 'CY', 'Active', 0),
(887, 'Nicosia', 'N', 55, 'CY', 'Active', 0),
(888, 'Paphos', 'P', 55, 'CY', 'Active', 0),
(889, 'Ustecky', 'U', 56, 'CZ', 'Active', 0),
(890, 'Jihocesky', 'C', 56, 'CZ', 'Active', 0),
(891, 'Jihomoravsky', 'B', 56, 'CZ', 'Active', 0),
(892, 'Karlovarsky', 'K', 56, 'CZ', 'Active', 0),
(893, 'Kralovehradecky', 'H', 56, 'CZ', 'Active', 0),
(894, 'Liberecky', 'L', 56, 'CZ', 'Active', 0),
(895, 'Moravskoslezsky', 'T', 56, 'CZ', 'Active', 0),
(896, 'Olomoucky', 'M', 56, 'CZ', 'Active', 0),
(897, 'Pardubicky', 'E', 56, 'CZ', 'Active', 0),
(898, 'Plzensky', 'P', 56, 'CZ', 'Active', 0),
(899, 'Praha', 'A', 56, 'CZ', 'Active', 0),
(900, 'Stredocesky', 'S', 56, 'CZ', 'Active', 0),
(901, 'Vysocina', 'J', 56, 'CZ', 'Active', 0),
(902, 'Zlinsky', 'Z', 56, 'CZ', 'Active', 0),
(903, 'Arhus', 'AR', 57, 'DK', 'Active', 0),
(904, 'Bornholm', 'BH', 57, 'DK', 'Active', 0),
(905, 'Copenhagen', 'CO', 57, 'DK', 'Active', 0),
(906, 'Faroe Islands', 'FO', 57, 'DK', 'Active', 0),
(907, 'Frederiksborg', 'FR', 57, 'DK', 'Active', 0),
(908, 'Fyn', 'FY', 57, 'DK', 'Active', 0),
(909, 'Kobenhavn', 'KO', 57, 'DK', 'Active', 0),
(910, 'Nordjylland', 'NO', 57, 'DK', 'Active', 0),
(911, 'Ribe', 'RI', 57, 'DK', 'Active', 0),
(912, 'Ringkobing', 'RK', 57, 'DK', 'Active', 0),
(913, 'Roskilde', 'RO', 57, 'DK', 'Active', 0),
(914, 'Sonderjylland', 'SO', 57, 'DK', 'Active', 0),
(915, 'Storstrom', 'ST', 57, 'DK', 'Active', 0),
(916, 'Vejle', 'VK', 57, 'DK', 'Active', 0),
(917, 'Vestj&aelig;lland', 'VJ', 57, 'DK', 'Active', 0),
(918, 'Viborg', 'VB', 57, 'DK', 'Active', 0),
(919, 'Ali Sabih', 'S', 58, 'DJ', 'Active', 0),
(920, 'Dikhil', 'K', 58, 'DJ', 'Active', 0),
(921, 'Djibouti', 'J', 58, 'DJ', 'Active', 0),
(922, 'Obock', 'O', 58, 'DJ', 'Active', 0),
(923, 'Tadjoura', 'T', 58, 'DJ', 'Active', 0),
(924, 'Saint Andrew Parish', 'AND', 59, 'DM', 'Active', 0),
(925, 'Saint David Parish', 'DAV', 59, 'DM', 'Active', 0),
(926, 'Saint George Parish', 'GEO', 59, 'DM', 'Active', 0),
(927, 'Saint John Parish', 'JOH', 59, 'DM', 'Active', 0),
(928, 'Saint Joseph Parish', 'JOS', 59, 'DM', 'Active', 0),
(929, 'Saint Luke Parish', 'LUK', 59, 'DM', 'Active', 0),
(930, 'Saint Mark Parish', 'MAR', 59, 'DM', 'Active', 0),
(931, 'Saint Patrick Parish', 'PAT', 59, 'DM', 'Active', 0),
(932, 'Saint Paul Parish', 'PAU', 59, 'DM', 'Active', 0),
(933, 'Saint Peter Parish', 'PET', 59, 'DM', 'Active', 0),
(934, 'Distrito Nacional', 'DN', 60, 'DO', 'Active', 0),
(935, 'Azua', 'AZ', 60, 'DO', 'Active', 0),
(936, 'Baoruco', 'BC', 60, 'DO', 'Active', 0),
(937, 'Barahona', 'BH', 60, 'DO', 'Active', 0),
(938, 'Dajabon', 'DJ', 60, 'DO', 'Active', 0),
(939, 'Duarte', 'DU', 60, 'DO', 'Active', 0),
(940, 'Elias Pina', 'EL', 60, 'DO', 'Active', 0),
(941, 'El Seybo', 'SY', 60, 'DO', 'Active', 0),
(942, 'Espaillat', 'ET', 60, 'DO', 'Active', 0),
(943, 'Hato Mayor', 'HM', 60, 'DO', 'Active', 0),
(944, 'Independencia', 'IN', 60, 'DO', 'Active', 0),
(945, 'La Altagracia', 'AL', 60, 'DO', 'Active', 0),
(946, 'La Romana', 'RO', 60, 'DO', 'Active', 0),
(947, 'La Vega', 'VE', 60, 'DO', 'Active', 0),
(948, 'Maria Trinidad Sanchez', 'MT', 60, 'DO', 'Active', 0),
(949, 'Monsenor Nouel', 'MN', 60, 'DO', 'Active', 0),
(950, 'Monte Cristi', 'MC', 60, 'DO', 'Active', 0),
(951, 'Monte Plata', 'MP', 60, 'DO', 'Active', 0),
(952, 'Pedernales', 'PD', 60, 'DO', 'Active', 0),
(953, 'Peravia (Bani)', 'PR', 60, 'DO', 'Active', 0),
(954, 'Puerto Plata', 'PP', 60, 'DO', 'Active', 0),
(955, 'Salcedo', 'SL', 60, 'DO', 'Active', 0),
(956, 'Samana', 'SM', 60, 'DO', 'Active', 0),
(957, 'Sanchez Ramirez', 'SH', 60, 'DO', 'Active', 0),
(958, 'San Cristobal', 'SC', 60, 'DO', 'Active', 0),
(959, 'San Jose de Ocoa', 'JO', 60, 'DO', 'Active', 0),
(960, 'San Juan', 'SJ', 60, 'DO', 'Active', 0),
(961, 'San Pedro de Macoris', 'PM', 60, 'DO', 'Active', 0),
(962, 'Santiago', 'SA', 60, 'DO', 'Active', 0),
(963, 'Santiago Rodriguez', 'ST', 60, 'DO', 'Active', 0),
(964, 'Santo Domingo', 'SD', 60, 'DO', 'Active', 0),
(965, 'Valverde', 'VA', 60, 'DO', 'Active', 0),
(966, 'Aileu', 'AL', 61, 'TP', 'Active', 0),
(967, 'Ainaro', 'AN', 61, 'TP', 'Active', 0),
(968, 'Baucau', 'BA', 61, 'TP', 'Active', 0),
(969, 'Bobonaro', 'BO', 61, 'TP', 'Active', 0),
(970, 'Cova Lima', 'CO', 61, 'TP', 'Active', 0),
(971, 'Dili', 'DI', 61, 'TP', 'Active', 0),
(972, 'Ermera', 'ER', 61, 'TP', 'Active', 0),
(973, 'Lautem', 'LA', 61, 'TP', 'Active', 0),
(974, 'Liquica', 'LI', 61, 'TP', 'Active', 0),
(975, 'Manatuto', 'MT', 61, 'TP', 'Active', 0),
(976, 'Manufahi', 'MF', 61, 'TP', 'Active', 0),
(977, 'Oecussi', 'OE', 61, 'TP', 'Active', 0),
(978, 'Viqueque', 'VI', 61, 'TP', 'Active', 0),
(979, 'Azuay', 'AZU', 62, 'EC', 'Active', 0),
(980, 'Bolivar', 'BOL', 62, 'EC', 'Active', 0),
(981, 'Ca&ntilde;ar', 'CAN', 62, 'EC', 'Active', 0),
(982, 'Carchi', 'CAR', 62, 'EC', 'Active', 0),
(983, 'Chimborazo', 'CHI', 62, 'EC', 'Active', 0),
(984, 'Cotopaxi', 'COT', 62, 'EC', 'Active', 0),
(985, 'El Oro', 'EOR', 62, 'EC', 'Active', 0),
(986, 'Esmeraldas', 'ESM', 62, 'EC', 'Active', 0),
(987, 'Gal&aacute;pagos', 'GPS', 62, 'EC', 'Active', 0),
(988, 'Guayas', 'GUA', 62, 'EC', 'Active', 0),
(989, 'Imbabura', 'IMB', 62, 'EC', 'Active', 0),
(990, 'Loja', 'LOJ', 62, 'EC', 'Active', 0),
(991, 'Los R?os', 'LRO', 62, 'EC', 'Active', 0),
(992, 'Manab&iacute;', 'MAN', 62, 'EC', 'Active', 0),
(993, 'Morona Santiago', 'MSA', 62, 'EC', 'Active', 0),
(994, 'Napo', 'NAP', 62, 'EC', 'Active', 0),
(995, 'Orellana', 'ORE', 62, 'EC', 'Active', 0),
(996, 'Pastaza', 'PAS', 62, 'EC', 'Active', 0),
(997, 'Pichincha', 'PIC', 62, 'EC', 'Active', 0),
(998, 'Sucumb&iacute;os', 'SUC', 62, 'EC', 'Active', 0),
(999, 'Tungurahua', 'TUN', 62, 'EC', 'Active', 0),
(1000, 'Zamora Chinchipe', 'ZCH', 62, 'EC', 'Active', 0),
(1001, 'Ad Daqahliyah', 'DHY', 63, 'EG', 'Active', 0),
(1002, 'Al Bahr al Ahmar', 'BAM', 63, 'EG', 'Active', 0),
(1003, 'Al Buhayrah', 'BHY', 63, 'EG', 'Active', 0),
(1004, 'Al Fayyum', 'FYM', 63, 'EG', 'Active', 0),
(1005, 'Al Gharbiyah', 'GBY', 63, 'EG', 'Active', 0),
(1006, 'Al Iskandariyah', 'IDR', 63, 'EG', 'Active', 0),
(1007, 'Al Isma\'iliyah', 'IML', 63, 'EG', 'Active', 0),
(1008, 'Al Jizah', 'JZH', 63, 'EG', 'Active', 0),
(1009, 'Al Minufiyah', 'MFY', 63, 'EG', 'Active', 0),
(1010, 'Al Minya', 'MNY', 63, 'EG', 'Active', 0),
(1011, 'Al Qahirah', 'QHR', 63, 'EG', 'Active', 0),
(1012, 'Al Qalyubiyah', 'QLY', 63, 'EG', 'Active', 0),
(1013, 'Al Wadi al Jadid', 'WJD', 63, 'EG', 'Active', 0),
(1014, 'Ash Sharqiyah', 'SHQ', 63, 'EG', 'Active', 0),
(1015, 'As Suways', 'SWY', 63, 'EG', 'Active', 0),
(1016, 'Aswan', 'ASW', 63, 'EG', 'Active', 0),
(1017, 'Asyut', 'ASY', 63, 'EG', 'Active', 0),
(1018, 'Bani Suwayf', 'BSW', 63, 'EG', 'Active', 0),
(1019, 'Bur Sa\'id', 'BSD', 63, 'EG', 'Active', 0),
(1020, 'Dumyat', 'DMY', 63, 'EG', 'Active', 0),
(1021, 'Janub Sina', 'JNS', 63, 'EG', 'Active', 0),
(1022, 'Kafr ash Shaykh', 'KSH', 63, 'EG', 'Active', 0),
(1023, 'Matruh', 'MAT', 63, 'EG', 'Active', 0),
(1024, 'Qina', 'QIN', 63, 'EG', 'Active', 0),
(1025, 'Shamal Sina', 'SHS', 63, 'EG', 'Active', 0),
(1026, 'Suhaj', 'SUH', 63, 'EG', 'Active', 0),
(1027, 'Ahuachapan', 'AH', 64, 'SV', 'Active', 0),
(1028, 'Cabanas', 'CA', 64, 'SV', 'Active', 0),
(1029, 'Chalatenango', 'CH', 64, 'SV', 'Active', 0),
(1030, 'Cuscatlan', 'CU', 64, 'SV', 'Active', 0),
(1031, 'La Libertad', 'LB', 64, 'SV', 'Active', 0),
(1032, 'La Paz', 'PZ', 64, 'SV', 'Active', 0),
(1033, 'La Union', 'UN', 64, 'SV', 'Active', 0),
(1034, 'Morazan', 'MO', 64, 'SV', 'Active', 0),
(1035, 'San Miguel', 'SM', 64, 'SV', 'Active', 0),
(1036, 'San Salvador', 'SS', 64, 'SV', 'Active', 0),
(1037, 'San Vicente', 'SV', 64, 'SV', 'Active', 0),
(1038, 'Santa Ana', 'SA', 64, 'SV', 'Active', 0),
(1039, 'Sonsonate', 'SO', 64, 'SV', 'Active', 0),
(1040, 'Usulutan', 'US', 64, 'SV', 'Active', 0),
(1041, 'Provincia Annobon', 'AN', 65, 'GQ', 'Active', 0),
(1042, 'Provincia Bioko Norte', 'BN', 65, 'GQ', 'Active', 0),
(1043, 'Provincia Bioko Sur', 'BS', 65, 'GQ', 'Active', 0),
(1044, 'Provincia Centro Sur', 'CS', 65, 'GQ', 'Active', 0),
(1045, 'Provincia Kie-Ntem', 'KN', 65, 'GQ', 'Active', 0),
(1046, 'Provincia Litoral', 'LI', 65, 'GQ', 'Active', 0),
(1047, 'Provincia Wele-Nzas', 'WN', 65, 'GQ', 'Active', 0),
(1048, 'Central (Maekel)', 'MA', 66, 'ER', 'Active', 0),
(1049, 'Anseba (Keren)', 'KE', 66, 'ER', 'Active', 0),
(1050, 'Southern Red Sea (Debub-Keih-Bah', 'DK', 66, 'ER', 'Active', 0),
(1051, 'Northern Red Sea (Semien-Keih-Ba', 'SK', 66, 'ER', 'Active', 0),
(1052, 'Southern (Debub)', 'DE', 66, 'ER', 'Active', 0),
(1053, 'Gash-Barka (Barentu)', 'BR', 66, 'ER', 'Active', 0),
(1054, 'Harjumaa (Tallinn)', 'HA', 67, 'EE', 'Active', 0),
(1055, 'Hiiumaa (Kardla)', 'HI', 67, 'EE', 'Active', 0),
(1056, 'Ida-Virumaa (Johvi)', 'IV', 67, 'EE', 'Active', 0),
(1057, 'Jarvamaa (Paide)', 'JA', 67, 'EE', 'Active', 0),
(1058, 'Jogevamaa (Jogeva)', 'JO', 67, 'EE', 'Active', 0),
(1059, 'Laane-Virumaa (Rakvere)', 'LV', 67, 'EE', 'Active', 0),
(1060, 'Laanemaa (Haapsalu)', 'LA', 67, 'EE', 'Active', 0),
(1061, 'Parnumaa (Parnu)', 'PA', 67, 'EE', 'Active', 0),
(1062, 'Polvamaa (Polva)', 'PO', 67, 'EE', 'Active', 0),
(1063, 'Raplamaa (Rapla)', 'RA', 67, 'EE', 'Active', 0),
(1064, 'Saaremaa (Kuessaare)', 'SA', 67, 'EE', 'Active', 0),
(1065, 'Tartumaa (Tartu)', 'TA', 67, 'EE', 'Active', 0),
(1066, 'Valgamaa (Valga)', 'VA', 67, 'EE', 'Active', 0),
(1067, 'Viljandimaa (Viljandi)', 'VI', 67, 'EE', 'Active', 0),
(1068, 'Vorumaa (Voru)', 'VO', 67, 'EE', 'Active', 0),
(1069, 'Afar', 'AF', 68, 'ET', 'Active', 0),
(1070, 'Amhara', 'AH', 68, 'ET', 'Active', 0),
(1071, 'Benishangul-Gumaz', 'BG', 68, 'ET', 'Active', 0),
(1072, 'Gambela', 'GB', 68, 'ET', 'Active', 0),
(1073, 'Hariai', 'HR', 68, 'ET', 'Active', 0),
(1074, 'Oromia', 'OR', 68, 'ET', 'Active', 0),
(1075, 'Somali', 'SM', 68, 'ET', 'Active', 0),
(1076, 'Southern Nations - Nationalities', 'SN', 68, 'ET', 'Active', 0);
INSERT INTO `state` (`iStateId`, `vState`, `vStateCode`, `iCountryId`, `vCountryCode`, `eStatus`, `iSysRecDeleted`) VALUES
(1077, 'Tigray', 'TG', 68, 'ET', 'Active', 0),
(1078, 'Addis Ababa', 'AA', 68, 'ET', 'Active', 0),
(1079, 'Dire Dawa', 'DD', 68, 'ET', 'Active', 0),
(1080, 'Central Division', 'C', 71, 'FJ', 'Active', 0),
(1081, 'Northern Division', 'N', 71, 'FJ', 'Active', 0),
(1082, 'Eastern Division', 'E', 71, 'FJ', 'Active', 0),
(1083, 'Western Division', 'W', 71, 'FJ', 'Active', 0),
(1084, 'Rotuma', 'R', 71, 'FJ', 'Active', 0),
(1085, 'Ahvenanmaan Laani', 'AL', 72, 'FI', 'Active', 0),
(1086, 'Etela-Suomen Laani', 'ES', 72, 'FI', 'Active', 0),
(1087, 'Ita-Suomen Laani', 'IS', 72, 'FI', 'Active', 0),
(1088, 'Lansi-Suomen Laani', 'LS', 72, 'FI', 'Active', 0),
(1089, 'Lapin Lanani', 'LA', 72, 'FI', 'Active', 0),
(1090, 'Oulun Laani', 'OU', 72, 'FI', 'Active', 0),
(1091, 'Alsace', 'AL', 73, 'FR', 'Active', 0),
(1092, 'Aquitaine', 'AQ', 73, 'FR', 'Active', 0),
(1093, 'Auvergne', 'AU', 73, 'FR', 'Active', 0),
(1094, 'Brittany', 'BR', 73, 'FR', 'Active', 0),
(1095, 'Burgundy', 'BU', 73, 'FR', 'Active', 0),
(1096, 'Center Loire Valley', 'CE', 73, 'FR', 'Active', 0),
(1097, 'Champagne', 'CH', 73, 'FR', 'Active', 0),
(1098, 'Corse', 'CO', 73, 'FR', 'Active', 0),
(1099, 'France Comte', 'FR', 73, 'FR', 'Active', 0),
(1100, 'Languedoc Roussillon', 'LA', 73, 'FR', 'Active', 0),
(1101, 'Limousin', 'LI', 73, 'FR', 'Active', 0),
(1102, 'Lorraine', 'LO', 73, 'FR', 'Active', 0),
(1103, 'Midi Pyrenees', 'MI', 73, 'FR', 'Active', 0),
(1104, 'Nord Pas de Calais', 'NO', 73, 'FR', 'Active', 0),
(1105, 'Normandy', 'NR', 73, 'FR', 'Active', 0),
(1106, 'Paris / Ill de France', 'PA', 73, 'FR', 'Active', 0),
(1107, 'Picardie', 'PI', 73, 'FR', 'Active', 0),
(1108, 'Poitou Charente', 'PO', 73, 'FR', 'Active', 0),
(1109, 'Provence', 'PR', 73, 'FR', 'Active', 0),
(1110, 'Rhone Alps', 'RH', 73, 'FR', 'Active', 0),
(1111, 'Riviera', 'RI', 73, 'FR', 'Active', 0),
(1112, 'Western Loire Valley', 'WE', 73, 'FR', 'Active', 0),
(1113, 'Etranger', 'Et', 74, 'FX', 'Active', 0),
(1114, 'Ain', '01', 74, 'FX', 'Active', 0),
(1115, 'Aisne', '02', 74, 'FX', 'Active', 0),
(1116, 'Allier', '03', 74, 'FX', 'Active', 0),
(1117, 'Alpes de Haute Provence', '04', 74, 'FX', 'Active', 0),
(1118, 'Hautes-Alpes', '05', 74, 'FX', 'Active', 0),
(1119, 'Alpes Maritimes', '06', 74, 'FX', 'Active', 0),
(1120, 'Ard&egrave;che', '07', 74, 'FX', 'Active', 0),
(1121, 'Ardennes', '08', 74, 'FX', 'Active', 0),
(1122, 'Ari&egrave;ge', '09', 74, 'FX', 'Active', 0),
(1123, 'Aube', '10', 74, 'FX', 'Active', 0),
(1124, 'Aude', '11', 74, 'FX', 'Active', 0),
(1125, 'Aveyron', '12', 74, 'FX', 'Active', 0),
(1126, 'Bouches du Rh&ocirc;ne', '13', 74, 'FX', 'Active', 0),
(1127, 'Calvados', '14', 74, 'FX', 'Active', 0),
(1128, 'Cantal', '15', 74, 'FX', 'Active', 0),
(1129, 'Charente', '16', 74, 'FX', 'Active', 0),
(1130, 'Charente Maritime', '17', 74, 'FX', 'Active', 0),
(1131, 'Cher', '18', 74, 'FX', 'Active', 0),
(1132, 'Corr&egrave;ze', '19', 74, 'FX', 'Active', 0),
(1133, 'Corse du Sud', '2A', 74, 'FX', 'Active', 0),
(1134, 'Haute Corse', '2B', 74, 'FX', 'Active', 0),
(1135, 'C&ocirc;te d\'or', '21', 74, 'FX', 'Active', 0),
(1136, 'C&ocirc;tes d\'Armor', '22', 74, 'FX', 'Active', 0),
(1137, 'Creuse', '23', 74, 'FX', 'Active', 0),
(1138, 'Dordogne', '24', 74, 'FX', 'Active', 0),
(1139, 'Doubs', '25', 74, 'FX', 'Active', 0),
(1140, 'Dr&ocirc;me', '26', 74, 'FX', 'Active', 0),
(1141, 'Eure', '27', 74, 'FX', 'Active', 0),
(1142, 'Eure et Loir', '28', 74, 'FX', 'Active', 0),
(1143, 'Finist&egrave;re', '29', 74, 'FX', 'Active', 0),
(1144, 'Gard', '30', 74, 'FX', 'Active', 0),
(1145, 'Haute Garonne', '31', 74, 'FX', 'Active', 0),
(1146, 'Gers', '32', 74, 'FX', 'Active', 0),
(1147, 'Gironde', '33', 74, 'FX', 'Active', 0),
(1148, 'H&eacute;rault', '34', 74, 'FX', 'Active', 0),
(1149, 'Ille et Vilaine', '35', 74, 'FX', 'Active', 0),
(1150, 'Indre', '36', 74, 'FX', 'Active', 0),
(1151, 'Indre et Loire', '37', 74, 'FX', 'Active', 0),
(1152, 'Is&eacute;re', '38', 74, 'FX', 'Active', 0),
(1153, 'Jura', '39', 74, 'FX', 'Active', 0),
(1154, 'Landes', '40', 74, 'FX', 'Active', 0),
(1155, 'Loir et Cher', '41', 74, 'FX', 'Active', 0),
(1156, 'Loire', '42', 74, 'FX', 'Active', 0),
(1157, 'Haute Loire', '43', 74, 'FX', 'Active', 0),
(1158, 'Loire Atlantique', '44', 74, 'FX', 'Active', 0),
(1159, 'Loiret', '45', 74, 'FX', 'Active', 0),
(1160, 'Lot', '46', 74, 'FX', 'Active', 0),
(1161, 'Lot et Garonne', '47', 74, 'FX', 'Active', 0),
(1162, 'Loz&egrave;re', '48', 74, 'FX', 'Active', 0),
(1163, 'Maine et Loire', '49', 74, 'FX', 'Active', 0),
(1164, 'Manche', '50', 74, 'FX', 'Active', 0),
(1165, 'Marne', '51', 74, 'FX', 'Active', 0),
(1166, 'Haute Marne', '52', 74, 'FX', 'Active', 0),
(1167, 'Mayenne', '53', 74, 'FX', 'Active', 0),
(1168, 'Meurthe et Moselle', '54', 74, 'FX', 'Active', 0),
(1169, 'Meuse', '55', 74, 'FX', 'Active', 0),
(1170, 'Morbihan', '56', 74, 'FX', 'Active', 0),
(1171, 'Moselle', '57', 74, 'FX', 'Active', 0),
(1172, 'Ni&egrave;vre', '58', 74, 'FX', 'Active', 0),
(1173, 'Nord', '59', 74, 'FX', 'Active', 0),
(1174, 'Oise', '60', 74, 'FX', 'Active', 0),
(1175, 'Orne', '61', 74, 'FX', 'Active', 0),
(1176, 'Pas de Calais', '62', 74, 'FX', 'Active', 0),
(1177, 'Puy de D&ocirc;me', '63', 74, 'FX', 'Active', 0),
(1178, 'Pyr&eacute;n&eacute;es Atlantiqu', '64', 74, 'FX', 'Active', 0),
(1179, 'Hautes Pyr&eacute;n&eacute;es', '65', 74, 'FX', 'Active', 0),
(1180, 'Pyr&eacute;n&eacute;es Orientale', '66', 74, 'FX', 'Active', 0),
(1181, 'Bas Rhin', '67', 74, 'FX', 'Active', 0),
(1182, 'Haut Rhin', '68', 74, 'FX', 'Active', 0),
(1183, 'Rh&ocirc;ne', '69', 74, 'FX', 'Active', 0),
(1184, 'Haute Sa&ocirc;ne', '70', 74, 'FX', 'Active', 0),
(1185, 'Sa&ocirc;ne et Loire', '71', 74, 'FX', 'Active', 0),
(1186, 'Sarthe', '72', 74, 'FX', 'Active', 0),
(1187, 'Savoie', '73', 74, 'FX', 'Active', 0),
(1188, 'Haute Savoie', '74', 74, 'FX', 'Active', 0),
(1189, 'Paris', '75', 74, 'FX', 'Active', 0),
(1190, 'Seine Maritime', '76', 74, 'FX', 'Active', 0),
(1191, 'Seine et Marne', '77', 74, 'FX', 'Active', 0),
(1192, 'Yvelines', '78', 74, 'FX', 'Active', 0),
(1193, 'Deux S&egrave;vres', '79', 74, 'FX', 'Active', 0),
(1194, 'Somme', '80', 74, 'FX', 'Active', 0),
(1195, 'Tarn', '81', 74, 'FX', 'Active', 0),
(1196, 'Tarn et Garonne', '82', 74, 'FX', 'Active', 0),
(1197, 'Var', '83', 74, 'FX', 'Active', 0),
(1198, 'Vaucluse', '84', 74, 'FX', 'Active', 0),
(1199, 'Vend&eacute;e', '85', 74, 'FX', 'Active', 0),
(1200, 'Vienne', '86', 74, 'FX', 'Active', 0),
(1201, 'Haute Vienne', '87', 74, 'FX', 'Active', 0),
(1202, 'Vosges', '88', 74, 'FX', 'Active', 0),
(1203, 'Yonne', '89', 74, 'FX', 'Active', 0),
(1204, 'Territoire de Belfort', '90', 74, 'FX', 'Active', 0),
(1205, 'Essonne', '91', 74, 'FX', 'Active', 0),
(1206, 'Hauts de Seine', '92', 74, 'FX', 'Active', 0),
(1207, 'Seine St-Denis', '93', 74, 'FX', 'Active', 0),
(1208, 'Val de Marne', '94', 74, 'FX', 'Active', 0),
(1209, 'Val d\'Oise', '95', 74, 'FX', 'Active', 0),
(1210, 'Archipel des Marquises', 'M', 76, 'PF', 'Active', 0),
(1211, 'Archipel des Tuamotu', 'T', 76, 'PF', 'Active', 0),
(1212, 'Archipel des Tubuai', 'I', 76, 'PF', 'Active', 0),
(1213, 'Iles du Vent', 'V', 76, 'PF', 'Active', 0),
(1214, 'Iles Sous-le-Vent', 'S', 76, 'PF', 'Active', 0),
(1215, 'Iles Crozet', 'C', 77, 'TF', 'Active', 0),
(1216, 'Iles Kerguelen', 'K', 77, 'TF', 'Active', 0),
(1217, 'Ile Amsterdam', 'A', 77, 'TF', 'Active', 0),
(1218, 'Ile Saint-Paul', 'P', 77, 'TF', 'Active', 0),
(1219, 'Adelie Land', 'D', 77, 'TF', 'Active', 0),
(1220, 'Estuaire', 'ES', 78, 'GA', 'Active', 0),
(1221, 'Haut-Ogooue', 'HO', 78, 'GA', 'Active', 0),
(1222, 'Moyen-Ogooue', 'MO', 78, 'GA', 'Active', 0),
(1223, 'Ngounie', 'NG', 78, 'GA', 'Active', 0),
(1224, 'Nyanga', 'NY', 78, 'GA', 'Active', 0),
(1225, 'Ogooue-Ivindo', 'OI', 78, 'GA', 'Active', 0),
(1226, 'Ogooue-Lolo', 'OL', 78, 'GA', 'Active', 0),
(1227, 'Ogooue-Maritime', 'OM', 78, 'GA', 'Active', 0),
(1228, 'Woleu-Ntem', 'WN', 78, 'GA', 'Active', 0),
(1229, 'Banjul', 'BJ', 79, 'GM', 'Active', 0),
(1230, 'Basse', 'BS', 79, 'GM', 'Active', 0),
(1231, 'Brikama', 'BR', 79, 'GM', 'Active', 0),
(1232, 'Janjangbure', 'JA', 79, 'GM', 'Active', 0),
(1233, 'Kanifeng', 'KA', 79, 'GM', 'Active', 0),
(1234, 'Kerewan', 'KE', 79, 'GM', 'Active', 0),
(1235, 'Kuntaur', 'KU', 79, 'GM', 'Active', 0),
(1236, 'Mansakonko', 'MA', 79, 'GM', 'Active', 0),
(1237, 'Lower River', 'LR', 79, 'GM', 'Active', 0),
(1238, 'Central River', 'CR', 79, 'GM', 'Active', 0),
(1239, 'North Bank', 'NB', 79, 'GM', 'Active', 0),
(1240, 'Upper River', 'UR', 79, 'GM', 'Active', 0),
(1241, 'Western', 'WE', 79, 'GM', 'Active', 0),
(1242, 'Abkhazia', 'AB', 80, 'GE', 'Active', 0),
(1243, 'Ajaria', 'AJ', 80, 'GE', 'Active', 0),
(1244, 'Tbilisi', 'TB', 80, 'GE', 'Active', 0),
(1245, 'Guria', 'GU', 80, 'GE', 'Active', 0),
(1246, 'Imereti', 'IM', 80, 'GE', 'Active', 0),
(1247, 'Kakheti', 'KA', 80, 'GE', 'Active', 0),
(1248, 'Kvemo Kartli', 'KK', 80, 'GE', 'Active', 0),
(1249, 'Mtskheta-Mtianeti', 'MM', 80, 'GE', 'Active', 0),
(1250, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 80, 'GE', 'Active', 0),
(1251, 'Samegrelo-Zemo Svaneti', 'SZ', 80, 'GE', 'Active', 0),
(1252, 'Samtskhe-Javakheti', 'SJ', 80, 'GE', 'Active', 0),
(1253, 'Shida Kartli', 'SK', 80, 'GE', 'Active', 0),
(1270, 'Ashanti Region', 'AS', 82, 'GH', 'Active', 0),
(1271, 'Brong-Ahafo Region', 'BA', 82, 'GH', 'Active', 0),
(1272, 'Central Region', 'CE', 82, 'GH', 'Active', 0),
(1273, 'Eastern Region', 'EA', 82, 'GH', 'Active', 0),
(1274, 'Greater Accra Region', 'GA', 82, 'GH', 'Active', 0),
(1275, 'Northern Region', 'NO', 82, 'GH', 'Active', 0),
(1276, 'Upper East Region', 'UE', 82, 'GH', 'Active', 0),
(1277, 'Upper West Region', 'UW', 82, 'GH', 'Active', 0),
(1278, 'Volta Region', 'VO', 82, 'GH', 'Active', 0),
(1279, 'Western Region', 'WE', 82, 'GH', 'Active', 0),
(1280, 'Attica', 'AT', 84, 'GR', 'Active', 0),
(1281, 'Central Greece', 'CN', 84, 'GR', 'Active', 0),
(1282, 'Central Macedonia', 'CM', 84, 'GR', 'Active', 0),
(1283, 'Crete', 'CR', 84, 'GR', 'Active', 0),
(1284, 'East Macedonia and Thrace', 'EM', 84, 'GR', 'Active', 0),
(1285, 'Epirus', 'EP', 84, 'GR', 'Active', 0),
(1286, 'Ionian Islands', 'II', 84, 'GR', 'Active', 0),
(1287, 'North Aegean', 'NA', 84, 'GR', 'Active', 0),
(1288, 'Peloponnesos', 'PP', 84, 'GR', 'Active', 0),
(1289, 'South Aegean', 'SA', 84, 'GR', 'Active', 0),
(1290, 'Thessaly', 'TH', 84, 'GR', 'Active', 0),
(1291, 'West Greece', 'WG', 84, 'GR', 'Active', 0),
(1292, 'West Macedonia', 'WM', 84, 'GR', 'Active', 0),
(1293, 'Avannaa', 'A', 85, 'GL', 'Active', 0),
(1294, 'Tunu', 'T', 85, 'GL', 'Active', 0),
(1295, 'Kitaa', 'K', 85, 'GL', 'Active', 0),
(1296, 'Saint Andrew', 'A', 86, 'GD', 'Active', 0),
(1297, 'Saint David', 'D', 86, 'GD', 'Active', 0),
(1298, 'Saint George', 'G', 86, 'GD', 'Active', 0),
(1299, 'Saint John', 'J', 86, 'GD', 'Active', 0),
(1300, 'Saint Mark', 'M', 86, 'GD', 'Active', 0),
(1301, 'Saint Patrick', 'P', 86, 'GD', 'Active', 0),
(1302, 'Carriacou', 'C', 86, 'GD', 'Active', 0),
(1303, 'Petit Martinique', 'Q', 86, 'GD', 'Active', 0),
(1304, 'Alta Verapaz', 'AV', 89, 'GT', 'Active', 0),
(1305, 'Baja Verapaz', 'BV', 89, 'GT', 'Active', 0),
(1306, 'Chimaltenango', 'CM', 89, 'GT', 'Active', 0),
(1307, 'Chiquimula', 'CQ', 89, 'GT', 'Active', 0),
(1308, 'El Peten', 'PE', 89, 'GT', 'Active', 0),
(1309, 'El Progreso', 'PR', 89, 'GT', 'Active', 0),
(1310, 'El Quiche', 'QC', 89, 'GT', 'Active', 0),
(1311, 'Escuintla', 'ES', 89, 'GT', 'Active', 0),
(1312, 'Guatemala', 'GU', 89, 'GT', 'Active', 0),
(1313, 'Huehuetenango', 'HU', 89, 'GT', 'Active', 0),
(1314, 'Izabal', 'IZ', 89, 'GT', 'Active', 0),
(1315, 'Jalapa', 'JA', 89, 'GT', 'Active', 0),
(1316, 'Jutiapa', 'JU', 89, 'GT', 'Active', 0),
(1317, 'Quetzaltenango', 'QZ', 89, 'GT', 'Active', 0),
(1318, 'Retalhuleu', 'RE', 89, 'GT', 'Active', 0),
(1319, 'Sacatepequez', 'ST', 89, 'GT', 'Active', 0),
(1320, 'San Marcos', 'SM', 89, 'GT', 'Active', 0),
(1321, 'Santa Rosa', 'SR', 89, 'GT', 'Active', 0),
(1322, 'Solola', 'SO', 89, 'GT', 'Active', 0),
(1323, 'Suchitepequez', 'SU', 89, 'GT', 'Active', 0),
(1324, 'Totonicapan', 'TO', 89, 'GT', 'Active', 0),
(1325, 'Zacapa', 'ZA', 89, 'GT', 'Active', 0),
(1326, 'Conakry', 'CNK', 90, 'GN', 'Active', 0),
(1327, 'Beyla', 'BYL', 90, 'GN', 'Active', 0),
(1328, 'Boffa', 'BFA', 90, 'GN', 'Active', 0),
(1329, 'Boke', 'BOK', 90, 'GN', 'Active', 0),
(1330, 'Coyah', 'COY', 90, 'GN', 'Active', 0),
(1331, 'Dabola', 'DBL', 90, 'GN', 'Active', 0),
(1332, 'Dalaba', 'DLB', 90, 'GN', 'Active', 0),
(1333, 'Dinguiraye', 'DGR', 90, 'GN', 'Active', 0),
(1334, 'Dubreka', 'DBR', 90, 'GN', 'Active', 0),
(1335, 'Faranah', 'FRN', 90, 'GN', 'Active', 0),
(1336, 'Forecariah', 'FRC', 90, 'GN', 'Active', 0),
(1337, 'Fria', 'FRI', 90, 'GN', 'Active', 0),
(1338, 'Gaoual', 'GAO', 90, 'GN', 'Active', 0),
(1339, 'Gueckedou', 'GCD', 90, 'GN', 'Active', 0),
(1340, 'Kankan', 'KNK', 90, 'GN', 'Active', 0),
(1341, 'Kerouane', 'KRN', 90, 'GN', 'Active', 0),
(1342, 'Kindia', 'KND', 90, 'GN', 'Active', 0),
(1343, 'Kissidougou', 'KSD', 90, 'GN', 'Active', 0),
(1344, 'Koubia', 'KBA', 90, 'GN', 'Active', 0),
(1345, 'Koundara', 'KDA', 90, 'GN', 'Active', 0),
(1346, 'Kouroussa', 'KRA', 90, 'GN', 'Active', 0),
(1347, 'Labe', 'LAB', 90, 'GN', 'Active', 0),
(1348, 'Lelouma', 'LLM', 90, 'GN', 'Active', 0),
(1349, 'Lola', 'LOL', 90, 'GN', 'Active', 0),
(1350, 'Macenta', 'MCT', 90, 'GN', 'Active', 0),
(1351, 'Mali', 'MAL', 90, 'GN', 'Active', 0),
(1352, 'Mamou', 'MAM', 90, 'GN', 'Active', 0),
(1353, 'Mandiana', 'MAN', 90, 'GN', 'Active', 0),
(1354, 'Nzerekore', 'NZR', 90, 'GN', 'Active', 0),
(1355, 'Pita', 'PIT', 90, 'GN', 'Active', 0),
(1356, 'Siguiri', 'SIG', 90, 'GN', 'Active', 0),
(1357, 'Telimele', 'TLM', 90, 'GN', 'Active', 0),
(1358, 'Tougue', 'TOG', 90, 'GN', 'Active', 0),
(1359, 'Yomou', 'YOM', 90, 'GN', 'Active', 0),
(1360, 'Bafata Region', 'BF', 91, 'GW', 'Active', 0),
(1361, 'Biombo Region', 'BB', 91, 'GW', 'Active', 0),
(1362, 'Bissau Region', 'BS', 91, 'GW', 'Active', 0),
(1363, 'Bolama Region', 'BL', 91, 'GW', 'Active', 0),
(1364, 'Cacheu Region', 'CA', 91, 'GW', 'Active', 0),
(1365, 'Gabu Region', 'GA', 91, 'GW', 'Active', 0),
(1366, 'Oio Region', 'OI', 91, 'GW', 'Active', 0),
(1367, 'Quinara Region', 'QU', 91, 'GW', 'Active', 0),
(1368, 'Tombali Region', 'TO', 91, 'GW', 'Active', 0),
(1369, 'Barima-Waini', 'BW', 92, 'GY', 'Active', 0),
(1370, 'Cuyuni-Mazaruni', 'CM', 92, 'GY', 'Active', 0),
(1371, 'Demerara-Mahaica', 'DM', 92, 'GY', 'Active', 0),
(1372, 'East Berbice-Corentyne', 'EC', 92, 'GY', 'Active', 0),
(1373, 'Essequibo Islands-West Demerara', 'EW', 92, 'GY', 'Active', 0),
(1374, 'Mahaica-Berbice', 'MB', 92, 'GY', 'Active', 0),
(1375, 'Pomeroon-Supenaam', 'PM', 92, 'GY', 'Active', 0),
(1376, 'Potaro-Siparuni', 'PI', 92, 'GY', 'Active', 0),
(1377, 'Upper Demerara-Berbice', 'UD', 92, 'GY', 'Active', 0),
(1378, 'Upper Takutu-Upper Essequibo', 'UT', 92, 'GY', 'Active', 0),
(1379, 'Artibonite', 'AR', 93, 'HT', 'Active', 0),
(1380, 'Centre', 'CE', 93, 'HT', 'Active', 0),
(1381, 'Grand\'Anse', 'GA', 93, 'HT', 'Active', 0),
(1382, 'Nord', 'ND', 93, 'HT', 'Active', 0),
(1383, 'Nord-Est', 'NE', 93, 'HT', 'Active', 0),
(1384, 'Nord-Ouest', 'NO', 93, 'HT', 'Active', 0),
(1385, 'Ouest', 'OU', 93, 'HT', 'Active', 0),
(1386, 'Sud', 'SD', 93, 'HT', 'Active', 0),
(1387, 'Sud-Est', 'SE', 93, 'HT', 'Active', 0),
(1388, 'Flat Island', 'F', 94, 'HM', 'Active', 0),
(1389, 'McDonald Island', 'M', 94, 'HM', 'Active', 0),
(1390, 'Shag Island', 'S', 94, 'HM', 'Active', 0),
(1391, 'Heard Island', 'H', 94, 'HM', 'Active', 0),
(1392, 'Atlantida', 'AT', 95, 'HN', 'Active', 0),
(1393, 'Choluteca', 'CH', 95, 'HN', 'Active', 0),
(1394, 'Colon', 'CL', 95, 'HN', 'Active', 0),
(1395, 'Comayagua', 'CM', 95, 'HN', 'Active', 0),
(1396, 'Copan', 'CP', 95, 'HN', 'Active', 0),
(1397, 'Cortes', 'CR', 95, 'HN', 'Active', 0),
(1398, 'El Paraiso', 'PA', 95, 'HN', 'Active', 0),
(1399, 'Francisco Morazan', 'FM', 95, 'HN', 'Active', 0),
(1400, 'Gracias a Dios', 'GD', 95, 'HN', 'Active', 0),
(1401, 'Intibuca', 'IN', 95, 'HN', 'Active', 0),
(1402, 'Islas de la Bahia (Bay Islands)', 'IB', 95, 'HN', 'Active', 0),
(1403, 'La Paz', 'PZ', 95, 'HN', 'Active', 0),
(1404, 'Lempira', 'LE', 95, 'HN', 'Active', 0),
(1405, 'Ocotepeque', 'OC', 95, 'HN', 'Active', 0),
(1406, 'Olancho', 'OL', 95, 'HN', 'Active', 0),
(1407, 'Santa Barbara', 'SB', 95, 'HN', 'Active', 0),
(1408, 'Valle', 'VA', 95, 'HN', 'Active', 0),
(1409, 'Yoro', 'YO', 95, 'HN', 'Active', 0),
(1410, 'Central and Western Hong Kong Is', 'HCW', 96, 'HK', 'Active', 0),
(1411, 'Eastern Hong Kong Island', 'HEA', 96, 'HK', 'Active', 0),
(1412, 'Southern Hong Kong Island', 'HSO', 96, 'HK', 'Active', 0),
(1413, 'Wan Chai Hong Kong Island', 'HWC', 96, 'HK', 'Active', 0),
(1414, 'Kowloon City Kowloon', 'KKC', 96, 'HK', 'Active', 0),
(1415, 'Kwun Tong Kowloon', 'KKT', 96, 'HK', 'Active', 0),
(1416, 'Sham Shui Po Kowloon', 'KSS', 96, 'HK', 'Active', 0),
(1417, 'Wong Tai Sin Kowloon', 'KWT', 96, 'HK', 'Active', 0),
(1418, 'Yau Tsim Mong Kowloon', 'KYT', 96, 'HK', 'Active', 0),
(1419, 'Islands New Territories', 'NIS', 96, 'HK', 'Active', 0),
(1420, 'Kwai Tsing New Territories', 'NKT', 96, 'HK', 'Active', 0),
(1421, 'North New Territories', 'NNO', 96, 'HK', 'Active', 0),
(1422, 'Sai Kung New Territories', 'NSK', 96, 'HK', 'Active', 0),
(1423, 'Sha Tin New Territories', 'NST', 96, 'HK', 'Active', 0),
(1424, 'Tai Po New Territories', 'NTP', 96, 'HK', 'Active', 0),
(1425, 'Tsuen Wan New Territories', 'NTW', 96, 'HK', 'Active', 0),
(1426, 'Tuen Mun New Territories', 'NTM', 96, 'HK', 'Active', 0),
(1427, 'Yuen Long New Territories', 'NYL', 96, 'HK', 'Active', 0),
(1428, 'Bacs-Kiskun', 'BK', 97, 'HU', 'Active', 0),
(1429, 'Baranya', 'BA', 97, 'HU', 'Active', 0),
(1430, 'Bekes', 'BE', 97, 'HU', 'Active', 0),
(1431, 'Bekescsaba', 'BS', 97, 'HU', 'Active', 0),
(1432, 'Borsod-Abauj-Zemplen', 'BZ', 97, 'HU', 'Active', 0),
(1433, 'Budapest', 'BU', 97, 'HU', 'Active', 0),
(1434, 'Csongrad', 'CS', 97, 'HU', 'Active', 0),
(1435, 'Debrecen', 'DE', 97, 'HU', 'Active', 0),
(1436, 'Dunaujvaros', 'DU', 97, 'HU', 'Active', 0),
(1437, 'Eger', 'EG', 97, 'HU', 'Active', 0),
(1438, 'Fejer', 'FE', 97, 'HU', 'Active', 0),
(1439, 'Gyor', 'GY', 97, 'HU', 'Active', 0),
(1440, 'Gyor-Moson-Sopron', 'GM', 97, 'HU', 'Active', 0),
(1441, 'Hajdu-Bihar', 'HB', 97, 'HU', 'Active', 0),
(1442, 'Heves', 'HE', 97, 'HU', 'Active', 0),
(1443, 'Hodmezovasarhely', 'HO', 97, 'HU', 'Active', 0),
(1444, 'Jasz-Nagykun-Szolnok', 'JN', 97, 'HU', 'Active', 0),
(1445, 'Kaposvar', 'KA', 97, 'HU', 'Active', 0),
(1446, 'Kecskemet', 'KE', 97, 'HU', 'Active', 0),
(1447, 'Komarom-Esztergom', 'KO', 97, 'HU', 'Active', 0),
(1448, 'Miskolc', 'MI', 97, 'HU', 'Active', 0),
(1449, 'Nagykanizsa', 'NA', 97, 'HU', 'Active', 0),
(1450, 'Nograd', 'NO', 97, 'HU', 'Active', 0),
(1451, 'Nyiregyhaza', 'NY', 97, 'HU', 'Active', 0),
(1452, 'Pecs', 'PE', 97, 'HU', 'Active', 0),
(1453, 'Pest', 'PS', 97, 'HU', 'Active', 0),
(1454, 'Somogy', 'SO', 97, 'HU', 'Active', 0),
(1455, 'Sopron', 'SP', 97, 'HU', 'Active', 0),
(1456, 'Szabolcs-Szatmar-Bereg', 'SS', 97, 'HU', 'Active', 0),
(1457, 'Szeged', 'SZ', 97, 'HU', 'Active', 0),
(1458, 'Szekesfehervar', 'SE', 97, 'HU', 'Active', 0),
(1459, 'Szolnok', 'SL', 97, 'HU', 'Active', 0),
(1460, 'Szombathely', 'SM', 97, 'HU', 'Active', 0),
(1461, 'Tatabanya', 'TA', 97, 'HU', 'Active', 0),
(1462, 'Tolna', 'TO', 97, 'HU', 'Active', 0),
(1463, 'Vas', 'VA', 97, 'HU', 'Active', 0),
(1464, 'Veszprem', 'VE', 97, 'HU', 'Active', 0),
(1465, 'Zala', 'ZA', 97, 'HU', 'Active', 0),
(1466, 'Zalaegerszeg', 'ZZ', 97, 'HU', 'Active', 0),
(1467, 'Austurland', 'AL', 98, 'IS', 'Active', 0),
(1468, 'Hofuoborgarsvaeoi', 'HF', 98, 'IS', 'Active', 0),
(1469, 'Norourland eystra', 'NE', 98, 'IS', 'Active', 0),
(1470, 'Norourland vestra', 'NV', 98, 'IS', 'Active', 0),
(1471, 'Suourland', 'SL', 98, 'IS', 'Active', 0),
(1472, 'Suournes', 'SN', 98, 'IS', 'Active', 0),
(1473, 'Vestfiroir', 'VF', 98, 'IS', 'Active', 0),
(1474, 'Vesturland', 'VL', 98, 'IS', 'Active', 0),
(1475, 'Andaman and Nicobar Islands', 'AN', 99, 'IN', 'Active', 0),
(1476, 'Andhra Pradesh', 'AP', 99, 'IN', 'Active', 0),
(1477, 'Arunachal Pradesh', 'AR', 99, 'IN', 'Active', 0),
(1478, 'Assam', 'AS', 99, 'IN', 'Active', 0),
(1479, 'Bihar', 'BI', 99, 'IN', 'Active', 0),
(1480, 'Chandigarh', 'CH', 99, 'IN', 'Active', 0),
(1481, 'Dadra and Nagar Haveli', 'DA', 99, 'IN', 'Active', 0),
(1482, 'Daman and Diu', 'DM', 99, 'IN', 'Active', 0),
(1483, 'Delhi', 'DE', 99, 'IN', 'Active', 0),
(1484, 'Goa', 'GO', 99, 'IN', 'Active', 0),
(1485, 'Gujarat', 'GU', 99, 'IN', 'Active', 0),
(1486, 'Haryana', 'HA', 99, 'IN', 'Active', 0),
(1487, 'Himachal Pradesh', 'HP', 99, 'IN', 'Active', 0),
(1488, 'Jammu and Kashmir', 'JA', 99, 'IN', 'Active', 0),
(1489, 'Karnataka', 'KA', 99, 'IN', 'Active', 0),
(1490, 'Kerala', 'KE', 99, 'IN', 'Active', 0),
(1491, 'Lakshadweep Islands', 'LI', 99, 'IN', 'Active', 0),
(1492, 'Madhya Pradesh', 'MP', 99, 'IN', 'Active', 0),
(1493, 'Maharashtra', 'MA', 99, 'IN', 'Active', 0),
(1494, 'Manipur', 'MN', 99, 'IN', 'Active', 0),
(1495, 'Meghalaya', 'ME', 99, 'IN', 'Active', 0),
(1496, 'Mizoram', 'MI', 99, 'IN', 'Active', 0),
(1497, 'Nagaland', 'NA', 99, 'IN', 'Active', 0),
(1498, 'Orissa', 'OR', 99, 'IN', 'Active', 0),
(1499, 'Pondicherry', 'PO', 99, 'IN', 'Active', 0),
(1500, 'Punjab', 'PU', 99, 'IN', 'Active', 0),
(1501, 'Rajasthan', 'RA', 99, 'IN', 'Active', 0),
(1502, 'Sikkim', 'SI', 99, 'IN', 'Active', 0),
(1503, 'Tamil Nadu', 'TN', 99, 'IN', 'Active', 0),
(1504, 'Tripura', 'TR', 99, 'IN', 'Active', 0),
(1505, 'Uttar Pradesh', 'UP', 99, 'IN', 'Active', 0),
(1506, 'West Bengal', 'WB', 99, 'IN', 'Active', 0),
(1507, 'Aceh', 'AC', 100, 'ID', 'Active', 0),
(1508, 'Bali', 'BA', 100, 'ID', 'Active', 0),
(1509, 'Banten', 'BT', 100, 'ID', 'Active', 0),
(1510, 'Bengkulu', 'BE', 100, 'ID', 'Active', 0),
(1511, 'BoDeTaBek', 'BD', 100, 'ID', 'Active', 0),
(1512, 'Gorontalo', 'GO', 100, 'ID', 'Active', 0),
(1513, 'Jakarta Raya', 'JK', 100, 'ID', 'Active', 0),
(1514, 'Jambi', 'JA', 100, 'ID', 'Active', 0),
(1515, 'Jawa Barat', 'JB', 100, 'ID', 'Active', 0),
(1516, 'Jawa Tengah', 'JT', 100, 'ID', 'Active', 0),
(1517, 'Jawa Timur', 'JI', 100, 'ID', 'Active', 0),
(1518, 'Kalimantan Barat', 'KB', 100, 'ID', 'Active', 0),
(1519, 'Kalimantan Selatan', 'KS', 100, 'ID', 'Active', 0),
(1520, 'Kalimantan Tengah', 'KT', 100, 'ID', 'Active', 0),
(1521, 'Kalimantan Timur', 'KI', 100, 'ID', 'Active', 0),
(1522, 'Kepulauan Bangka Belitung', 'BB', 100, 'ID', 'Active', 0),
(1523, 'Lampung', 'LA', 100, 'ID', 'Active', 0),
(1524, 'Maluku', 'MA', 100, 'ID', 'Active', 0),
(1525, 'Maluku Utara', 'MU', 100, 'ID', 'Active', 0),
(1526, 'Nusa Tenggara Barat', 'NB', 100, 'ID', 'Active', 0),
(1527, 'Nusa Tenggara Timur', 'NT', 100, 'ID', 'Active', 0),
(1528, 'Papua', 'PA', 100, 'ID', 'Active', 0),
(1529, 'Riau', 'RI', 100, 'ID', 'Active', 0),
(1530, 'Sulawesi Selatan', 'SN', 100, 'ID', 'Active', 0),
(1531, 'Sulawesi Tengah', 'ST', 100, 'ID', 'Active', 0),
(1532, 'Sulawesi Tenggara', 'SG', 100, 'ID', 'Active', 0),
(1533, 'Sulawesi Utara', 'SA', 100, 'ID', 'Active', 0),
(1534, 'Sumatera Barat', 'SB', 100, 'ID', 'Active', 0),
(1535, 'Sumatera Selatan', 'SS', 100, 'ID', 'Active', 0),
(1536, 'Sumatera Utara', 'SU', 100, 'ID', 'Active', 0),
(1537, 'Yogyakarta', 'YO', 100, 'ID', 'Active', 0),
(1538, 'Tehran', 'TEH', 101, 'IR', 'Active', 0),
(1539, 'Qom', 'QOM', 101, 'IR', 'Active', 0),
(1540, 'Markazi', 'MKZ', 101, 'IR', 'Active', 0),
(1541, 'Qazvin', 'QAZ', 101, 'IR', 'Active', 0),
(1542, 'Gilan', 'GIL', 101, 'IR', 'Active', 0),
(1543, 'Ardabil', 'ARD', 101, 'IR', 'Active', 0),
(1544, 'Zanjan', 'ZAN', 101, 'IR', 'Active', 0),
(1545, 'East Azarbaijan', 'EAZ', 101, 'IR', 'Active', 0),
(1546, 'West Azarbaijan', 'WEZ', 101, 'IR', 'Active', 0),
(1547, 'Kurdistan', 'KRD', 101, 'IR', 'Active', 0),
(1548, 'Hamadan', 'HMD', 101, 'IR', 'Active', 0),
(1549, 'Kermanshah', 'KRM', 101, 'IR', 'Active', 0),
(1550, 'Ilam', 'ILM', 101, 'IR', 'Active', 0),
(1551, 'Lorestan', 'LRS', 101, 'IR', 'Active', 0),
(1552, 'Khuzestan', 'KZT', 101, 'IR', 'Active', 0),
(1553, 'Chahar Mahaal and Bakhtiari', 'CMB', 101, 'IR', 'Active', 0),
(1554, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 101, 'IR', 'Active', 0),
(1555, 'Bushehr', 'BSH', 101, 'IR', 'Active', 0),
(1556, 'Fars', 'FAR', 101, 'IR', 'Active', 0),
(1557, 'Hormozgan', 'HRM', 101, 'IR', 'Active', 0),
(1558, 'Sistan and Baluchistan', 'SBL', 101, 'IR', 'Active', 0),
(1559, 'Kerman', 'KRB', 101, 'IR', 'Active', 0),
(1560, 'Yazd', 'YZD', 101, 'IR', 'Active', 0),
(1561, 'Esfahan', 'EFH', 101, 'IR', 'Active', 0),
(1562, 'Semnan', 'SMN', 101, 'IR', 'Active', 0),
(1563, 'Mazandaran', 'MZD', 101, 'IR', 'Active', 0),
(1564, 'Golestan', 'GLS', 101, 'IR', 'Active', 0),
(1565, 'North Khorasan', 'NKH', 101, 'IR', 'Active', 0),
(1566, 'Razavi Khorasan', 'RKH', 101, 'IR', 'Active', 0),
(1567, 'South Khorasan', 'SKH', 101, 'IR', 'Active', 0),
(1568, 'Baghdad', 'BD', 102, 'IQ', 'Active', 0),
(1569, 'Salah ad Din', 'SD', 102, 'IQ', 'Active', 0),
(1570, 'Diyala', 'DY', 102, 'IQ', 'Active', 0),
(1571, 'Wasit', 'WS', 102, 'IQ', 'Active', 0),
(1572, 'Maysan', 'MY', 102, 'IQ', 'Active', 0),
(1573, 'Al Basrah', 'BA', 102, 'IQ', 'Active', 0),
(1574, 'Dhi Qar', 'DQ', 102, 'IQ', 'Active', 0),
(1577, 'Babil', 'BB', 102, 'IQ', 'Active', 0),
(1578, 'Al Karbala', 'KB', 102, 'IQ', 'Active', 0),
(1579, 'An Najaf', 'NJ', 102, 'IQ', 'Active', 0),
(1580, 'Al Anbar', 'AB', 102, 'IQ', 'Active', 0),
(1581, 'Ninawa', 'NN', 102, 'IQ', 'Active', 0),
(1582, 'Dahuk', 'DH', 102, 'IQ', 'Active', 0),
(1583, 'Arbil', 'AL', 102, 'IQ', 'Active', 0),
(1584, 'At Ta\'mim', 'TM', 102, 'IQ', 'Active', 0),
(1585, 'As Sulaymaniyah', 'SL', 102, 'IQ', 'Active', 0),
(1586, 'Carlow', 'CA', 103, 'IE', 'Active', 0),
(1587, 'Cavan', 'CV', 103, 'IE', 'Active', 0),
(1588, 'Clare', 'CL', 103, 'IE', 'Active', 0),
(1589, 'Cork', 'CO', 103, 'IE', 'Active', 0),
(1590, 'Donegal', 'DO', 103, 'IE', 'Active', 0),
(1591, 'Dublin', 'DU', 103, 'IE', 'Active', 0),
(1592, 'Galway', 'GA', 103, 'IE', 'Active', 0),
(1593, 'Kerry', 'KE', 103, 'IE', 'Active', 0),
(1594, 'Kildare', 'KI', 103, 'IE', 'Active', 0),
(1595, 'Kilkenny', 'KL', 103, 'IE', 'Active', 0),
(1596, 'Laois', 'LA', 103, 'IE', 'Active', 0),
(1597, 'Leitrim', 'LE', 103, 'IE', 'Active', 0),
(1598, 'Limerick', 'LI', 103, 'IE', 'Active', 0),
(1599, 'Longford', 'LO', 103, 'IE', 'Active', 0),
(1600, 'Louth', 'LU', 103, 'IE', 'Active', 0),
(1601, 'Mayo', 'MA', 103, 'IE', 'Active', 0),
(1602, 'Meath', 'ME', 103, 'IE', 'Active', 0),
(1603, 'Monaghan', 'MO', 103, 'IE', 'Active', 0),
(1604, 'Offaly', 'OF', 103, 'IE', 'Active', 0),
(1605, 'Roscommon', 'RO', 103, 'IE', 'Active', 0),
(1606, 'Sligo', 'SL', 103, 'IE', 'Active', 0),
(1607, 'Tipperary', 'TI', 103, 'IE', 'Active', 0),
(1608, 'Waterford', 'WA', 103, 'IE', 'Active', 0),
(1609, 'Westmeath', 'WE', 103, 'IE', 'Active', 0),
(1610, 'Wexford', 'WX', 103, 'IE', 'Active', 0),
(1611, 'Wicklow', 'WI', 103, 'IE', 'Active', 0),
(1612, 'Be\'er Sheva', 'BS', 104, 'IL', 'Active', 0),
(1613, 'Bika\'at Hayarden', 'BH', 104, 'IL', 'Active', 0),
(1614, 'Eilat and Arava', 'EA', 104, 'IL', 'Active', 0),
(1615, 'Galil', 'GA', 104, 'IL', 'Active', 0),
(1616, 'Haifa', 'HA', 104, 'IL', 'Active', 0),
(1617, 'Jehuda Mountains', 'JM', 104, 'IL', 'Active', 0),
(1618, 'Jerusalem', 'JE', 104, 'IL', 'Active', 0),
(1619, 'Negev', 'NE', 104, 'IL', 'Active', 0),
(1620, 'Semaria', 'SE', 104, 'IL', 'Active', 0),
(1621, 'Sharon', 'SH', 104, 'IL', 'Active', 0),
(1622, 'Tel Aviv (Gosh Dan)', 'TA', 104, 'IL', 'Active', 0),
(1623, 'Abruzzo', 'AB', 105, 'IT', 'Active', 0),
(1624, 'Basilicata', 'BA', 105, 'IT', 'Active', 0),
(1625, 'Calabria', 'CA', 105, 'IT', 'Active', 0),
(1626, 'Campania', 'CP', 105, 'IT', 'Active', 0),
(1627, 'Emilia Romagna', 'ER', 105, 'IT', 'Active', 0),
(1628, 'Friuli-Venezia Giulia', 'FV', 105, 'IT', 'Active', 0),
(1629, 'Lazio (Latium & Rome)', 'LA', 105, 'IT', 'Active', 0),
(1630, 'Le Marche (The Marches)', 'TM', 105, 'IT', 'Active', 0),
(1631, 'Liguria', 'LI', 105, 'IT', 'Active', 0),
(1632, 'Lombardia (Lombardy)', 'LO', 105, 'IT', 'Active', 0),
(1633, 'Molise', 'MO', 105, 'IT', 'Active', 0),
(1634, 'Piemonte (Piedmont)', 'PI', 105, 'IT', 'Active', 0),
(1635, 'Puglia (Apulia)', 'AP', 105, 'IT', 'Active', 0),
(1636, 'Sardegna (Sardinia)', 'SA', 105, 'IT', 'Active', 0),
(1637, 'Sicilia (Sicily)', 'SI', 105, 'IT', 'Active', 0),
(1638, 'Toscana (Tuscany)', 'TU', 105, 'IT', 'Active', 0),
(1639, 'Trentino Alto Adige', 'TR', 105, 'IT', 'Active', 0),
(1640, 'Umbria', 'UM', 105, 'IT', 'Active', 0),
(1641, 'Val d\'Aosta', 'VA', 105, 'IT', 'Active', 0),
(1642, 'Veneto', 'VE', 105, 'IT', 'Active', 0),
(1643, 'Clarendon Parish', 'CLA', 106, 'JM', 'Active', 0),
(1644, 'Hanover Parish', 'HAN', 106, 'JM', 'Active', 0),
(1645, 'Kingston Parish', 'KIN', 106, 'JM', 'Active', 0),
(1646, 'Manchester Parish', 'MAN', 106, 'JM', 'Active', 0),
(1647, 'Portland Parish', 'POR', 106, 'JM', 'Active', 0),
(1648, 'Saint Andrew Parish', 'AND', 106, 'JM', 'Active', 0),
(1649, 'Saint Ann Parish', 'ANN', 106, 'JM', 'Active', 0),
(1650, 'Saint Catherine Parish', 'CAT', 106, 'JM', 'Active', 0),
(1651, 'Saint Elizabeth Parish', 'ELI', 106, 'JM', 'Active', 0),
(1652, 'Saint James Parish', 'JAM', 106, 'JM', 'Active', 0),
(1653, 'Saint Mary Parish', 'MAR', 106, 'JM', 'Active', 0),
(1654, 'Saint Thomas Parish', 'THO', 106, 'JM', 'Active', 0),
(1655, 'Trelawny Parish', 'TRL', 106, 'JM', 'Active', 0),
(1656, 'Westmoreland Parish', 'WML', 106, 'JM', 'Active', 0),
(1657, 'Aichi', 'AI', 107, 'JP', 'Active', 0),
(1658, 'Akita', 'AK', 107, 'JP', 'Active', 0),
(1659, 'Aomori', 'AO', 107, 'JP', 'Active', 0),
(1660, 'Chiba', 'CH', 107, 'JP', 'Active', 0),
(1661, 'Ehime', 'EH', 107, 'JP', 'Active', 0),
(1662, 'Fukui', 'FK', 107, 'JP', 'Active', 0),
(1663, 'Fukuoka', 'FU', 107, 'JP', 'Active', 0),
(1664, 'Fukushima', 'FS', 107, 'JP', 'Active', 0),
(1665, 'Gifu', 'GI', 107, 'JP', 'Active', 0),
(1666, 'Gumma', 'GU', 107, 'JP', 'Active', 0),
(1667, 'Hiroshima', 'HI', 107, 'JP', 'Active', 0),
(1668, 'Hokkaido', 'HO', 107, 'JP', 'Active', 0),
(1669, 'Hyogo', 'HY', 107, 'JP', 'Active', 0),
(1670, 'Ibaraki', 'IB', 107, 'JP', 'Active', 0),
(1671, 'Ishikawa', 'IS', 107, 'JP', 'Active', 0),
(1672, 'Iwate', 'IW', 107, 'JP', 'Active', 0),
(1673, 'Kagawa', 'KA', 107, 'JP', 'Active', 0),
(1674, 'Kagoshima', 'KG', 107, 'JP', 'Active', 0),
(1675, 'Kanagawa', 'KN', 107, 'JP', 'Active', 0),
(1676, 'Kochi', 'KO', 107, 'JP', 'Active', 0),
(1677, 'Kumamoto', 'KU', 107, 'JP', 'Active', 0),
(1678, 'Kyoto', 'KY', 107, 'JP', 'Active', 0),
(1679, 'Mie', 'MI', 107, 'JP', 'Active', 0),
(1680, 'Miyagi', 'MY', 107, 'JP', 'Active', 0),
(1681, 'Miyazaki', 'MZ', 107, 'JP', 'Active', 0),
(1682, 'Nagano', 'NA', 107, 'JP', 'Active', 0),
(1683, 'Nagasaki', 'NG', 107, 'JP', 'Active', 0),
(1684, 'Nara', 'NR', 107, 'JP', 'Active', 0),
(1685, 'Niigata', 'NI', 107, 'JP', 'Active', 0),
(1686, 'Oita', 'OI', 107, 'JP', 'Active', 0),
(1687, 'Okayama', 'OK', 107, 'JP', 'Active', 0),
(1688, 'Okinawa', 'ON', 107, 'JP', 'Active', 0),
(1689, 'Osaka', 'OS', 107, 'JP', 'Active', 0),
(1690, 'Saga', 'SA', 107, 'JP', 'Active', 0),
(1691, 'Saitama', 'SI', 107, 'JP', 'Active', 0),
(1692, 'Shiga', 'SH', 107, 'JP', 'Active', 0),
(1693, 'Shimane', 'SM', 107, 'JP', 'Active', 0),
(1694, 'Shizuoka', 'SZ', 107, 'JP', 'Active', 0),
(1695, 'Tochigi', 'TO', 107, 'JP', 'Active', 0),
(1696, 'Tokushima', 'TS', 107, 'JP', 'Active', 0),
(1697, 'Tokyo', 'TK', 107, 'JP', 'Active', 0),
(1698, 'Tottori', 'TT', 107, 'JP', 'Active', 0),
(1699, 'Toyama', 'TY', 107, 'JP', 'Active', 0),
(1700, 'Wakayama', 'WA', 107, 'JP', 'Active', 0),
(1701, 'Yamagata', 'YA', 107, 'JP', 'Active', 0),
(1702, 'Yamaguchi', 'YM', 107, 'JP', 'Active', 0),
(1703, 'Yamanashi', 'YN', 107, 'JP', 'Active', 0),
(1704, 'Amman', 'AM', 108, 'JO', 'Active', 0),
(1705, 'Ajlun', 'AJ', 108, 'JO', 'Active', 0),
(1706, 'Al \'Aqabah', 'AA', 108, 'JO', 'Active', 0),
(1707, 'Al Balqa', 'AB', 108, 'JO', 'Active', 0),
(1708, 'Al Karak', 'AK', 108, 'JO', 'Active', 0),
(1709, 'Al Mafraq', 'AL', 108, 'JO', 'Active', 0),
(1710, 'At Tafilah', 'AT', 108, 'JO', 'Active', 0),
(1711, 'Az Zarqa\'', 'AZ', 108, 'JO', 'Active', 0),
(1712, 'Irbid', 'IR', 108, 'JO', 'Active', 0),
(1713, 'Jarash', 'JA', 108, 'JO', 'Active', 0),
(1714, 'Ma\'an', 'MA', 108, 'JO', 'Active', 0),
(1715, 'Madaba', 'MD', 108, 'JO', 'Active', 0),
(1716, 'Almaty', 'AL', 109, 'KZ', 'Active', 0),
(1717, 'Almaty City', 'AC', 109, 'KZ', 'Active', 0),
(1718, 'Aqmola', 'AM', 109, 'KZ', 'Active', 0),
(1719, 'Aqtobe', 'AQ', 109, 'KZ', 'Active', 0),
(1720, 'Astana City', 'AS', 109, 'KZ', 'Active', 0),
(1721, 'Atyrau', 'AT', 109, 'KZ', 'Active', 0),
(1722, 'Batys Qazaqstan', 'BA', 109, 'KZ', 'Active', 0),
(1723, 'Bayqongyr City', 'BY', 109, 'KZ', 'Active', 0),
(1724, 'Mangghystau', 'MA', 109, 'KZ', 'Active', 0),
(1725, 'Ongtustik Qazaqstan', 'ON', 109, 'KZ', 'Active', 0),
(1726, 'Pavlodar', 'PA', 109, 'KZ', 'Active', 0),
(1727, 'Qaraghandy', 'QA', 109, 'KZ', 'Active', 0),
(1728, 'Qostanay', 'QO', 109, 'KZ', 'Active', 0),
(1729, 'Qyzylorda', 'QY', 109, 'KZ', 'Active', 0),
(1730, 'Shyghys Qazaqstan', 'SH', 109, 'KZ', 'Active', 0),
(1731, 'Soltustik Qazaqstan', 'SO', 109, 'KZ', 'Active', 0),
(1732, 'Zhambyl', 'ZH', 109, 'KZ', 'Active', 0),
(1733, 'Central', 'CE', 110, 'KE', 'Active', 0),
(1734, 'Coast', 'CO', 110, 'KE', 'Active', 0),
(1735, 'Eastern', 'EA', 110, 'KE', 'Active', 0),
(1736, 'Nairobi Area', 'NA', 110, 'KE', 'Active', 0),
(1737, 'North Eastern', 'NE', 110, 'KE', 'Active', 0),
(1738, 'Nyanza', 'NY', 110, 'KE', 'Active', 0),
(1739, 'Rift Valley', 'RV', 110, 'KE', 'Active', 0),
(1740, 'Western', 'WE', 110, 'KE', 'Active', 0),
(1741, 'Abaiang', 'AG', 111, 'KI', 'Active', 0),
(1742, 'Abemama', 'AM', 111, 'KI', 'Active', 0),
(1743, 'Aranuka', 'AK', 111, 'KI', 'Active', 0),
(1744, 'Arorae', 'AO', 111, 'KI', 'Active', 0),
(1745, 'Banaba', 'BA', 111, 'KI', 'Active', 0),
(1746, 'Beru', 'BE', 111, 'KI', 'Active', 0),
(1747, 'Butaritari', 'bT', 111, 'KI', 'Active', 0),
(1748, 'Kanton', 'KA', 111, 'KI', 'Active', 0),
(1749, 'Kiritimati', 'KR', 111, 'KI', 'Active', 0),
(1750, 'Kuria', 'KU', 111, 'KI', 'Active', 0),
(1751, 'Maiana', 'MI', 111, 'KI', 'Active', 0),
(1752, 'Makin', 'MN', 111, 'KI', 'Active', 0),
(1753, 'Marakei', 'ME', 111, 'KI', 'Active', 0),
(1754, 'Nikunau', 'NI', 111, 'KI', 'Active', 0),
(1755, 'Nonouti', 'NO', 111, 'KI', 'Active', 0),
(1756, 'Onotoa', 'ON', 111, 'KI', 'Active', 0),
(1757, 'Tabiteuea', 'TT', 111, 'KI', 'Active', 0),
(1758, 'Tabuaeran', 'TR', 111, 'KI', 'Active', 0),
(1759, 'Tamana', 'TM', 111, 'KI', 'Active', 0),
(1760, 'Tarawa', 'TW', 111, 'KI', 'Active', 0),
(1761, 'Teraina', 'TE', 111, 'KI', 'Active', 0),
(1762, 'Chagang-do', 'CHA', 112, 'KP', 'Active', 0),
(1763, 'Hamgyong-bukto', 'HAB', 112, 'KP', 'Active', 0),
(1764, 'Hamgyong-namdo', 'HAN', 112, 'KP', 'Active', 0),
(1765, 'Hwanghae-bukto', 'HWB', 112, 'KP', 'Active', 0),
(1766, 'Hwanghae-namdo', 'HWN', 112, 'KP', 'Active', 0),
(1767, 'Kangwon-do', 'KAN', 112, 'KP', 'Active', 0),
(1768, 'P\'yongan-bukto', 'PYB', 112, 'KP', 'Active', 0),
(1769, 'P\'yongan-namdo', 'PYN', 112, 'KP', 'Active', 0),
(1770, 'Ryanggang-do (Yanggang-do)', 'YAN', 112, 'KP', 'Active', 0),
(1771, 'Rason Directly Governed City', 'NAJ', 112, 'KP', 'Active', 0),
(1772, 'P\'yongyang Special City', 'PYO', 112, 'KP', 'Active', 0),
(1773, 'Ch\'ungch\'ong-bukto', 'CO', 113, 'KR', 'Active', 0),
(1774, 'Ch\'ungch\'ong-namdo', 'CH', 113, 'KR', 'Active', 0),
(1775, 'Cheju-do', 'CD', 113, 'KR', 'Active', 0),
(1776, 'Cholla-bukto', 'CB', 113, 'KR', 'Active', 0),
(1777, 'Cholla-namdo', 'CN', 113, 'KR', 'Active', 0),
(1778, 'Inch\'on-gwangyoksi', 'IG', 113, 'KR', 'Active', 0),
(1779, 'Kangwon-do', 'KA', 113, 'KR', 'Active', 0),
(1780, 'Kwangju-gwangyoksi', 'KG', 113, 'KR', 'Active', 0),
(1781, 'Kyonggi-do', 'KD', 113, 'KR', 'Active', 0),
(1782, 'Kyongsang-bukto', 'KB', 113, 'KR', 'Active', 0),
(1783, 'Kyongsang-namdo', 'KN', 113, 'KR', 'Active', 0),
(1784, 'Pusan-gwangyoksi', 'PG', 113, 'KR', 'Active', 0),
(1785, 'Soul-t\'ukpyolsi', 'SO', 113, 'KR', 'Active', 0),
(1786, 'Taegu-gwangyoksi', 'TA', 113, 'KR', 'Active', 0),
(1787, 'Taejon-gwangyoksi', 'TG', 113, 'KR', 'Active', 0),
(1788, 'Al \'Asimah', 'AL', 114, 'KW', 'Active', 0),
(1789, 'Al Ahmadi', 'AA', 114, 'KW', 'Active', 0),
(1790, 'Al Farwaniyah', 'AF', 114, 'KW', 'Active', 0),
(1791, 'Al Jahra', 'AJ', 114, 'KW', 'Active', 0),
(1792, 'Hawalli', 'HA', 114, 'KW', 'Active', 0),
(1793, 'Bishkek', 'GB', 115, 'KG', 'Active', 0),
(1794, 'Batken', 'B', 115, 'KG', 'Active', 0),
(1795, 'Chu', 'CHU', 115, 'KG', 'Active', 0),
(1796, 'Jalal-Abad', 'J', 115, 'KG', 'Active', 0),
(1797, 'Naryn', 'N', 115, 'KG', 'Active', 0),
(1798, 'Osh', 'O', 115, 'KG', 'Active', 0),
(1799, 'Talas', 'T', 115, 'KG', 'Active', 0),
(1800, 'Ysyk-Kol', 'Y', 115, 'KG', 'Active', 0),
(1801, 'Vientiane', 'VT', 116, 'LA', 'Active', 0),
(1802, 'Attapu', 'AT', 116, 'LA', 'Active', 0),
(1803, 'Bokeo', 'BK', 116, 'LA', 'Active', 0),
(1804, 'Bolikhamxai', 'BL', 116, 'LA', 'Active', 0),
(1805, 'Champasak', 'CH', 116, 'LA', 'Active', 0),
(1806, 'Houaphan', 'HO', 116, 'LA', 'Active', 0),
(1807, 'Khammouan', 'KH', 116, 'LA', 'Active', 0),
(1808, 'Louang Namtha', 'LM', 116, 'LA', 'Active', 0),
(1809, 'Louangphabang', 'LP', 116, 'LA', 'Active', 0),
(1810, 'Oudomxai', 'OU', 116, 'LA', 'Active', 0),
(1811, 'Phongsali', 'PH', 116, 'LA', 'Active', 0),
(1812, 'Salavan', 'SL', 116, 'LA', 'Active', 0),
(1813, 'Savannakhet', 'SV', 116, 'LA', 'Active', 0),
(1814, 'Vientiane', 'VI', 116, 'LA', 'Active', 0),
(1815, 'Xaignabouli', 'XA', 116, 'LA', 'Active', 0),
(1816, 'Xekong', 'XE', 116, 'LA', 'Active', 0),
(1817, 'Xiangkhoang', 'XI', 116, 'LA', 'Active', 0),
(1818, 'Xaisomboun', 'XN', 116, 'LA', 'Active', 0),
(1819, 'Aizkraukles Rajons', 'AIZ', 117, 'LV', 'Active', 0),
(1820, 'Aluksnes Rajons', 'ALU', 117, 'LV', 'Active', 0),
(1821, 'Balvu Rajons', 'BAL', 117, 'LV', 'Active', 0),
(1822, 'Bauskas Rajons', 'BAU', 117, 'LV', 'Active', 0),
(1823, 'Cesu Rajons', 'CES', 117, 'LV', 'Active', 0),
(1824, 'Daugavpils Rajons', 'DGR', 117, 'LV', 'Active', 0),
(1825, 'Dobeles Rajons', 'DOB', 117, 'LV', 'Active', 0),
(1826, 'Gulbenes Rajons', 'GUL', 117, 'LV', 'Active', 0),
(1827, 'Jekabpils Rajons', 'JEK', 117, 'LV', 'Active', 0),
(1828, 'Jelgavas Rajons', 'JGR', 117, 'LV', 'Active', 0),
(1829, 'Kraslavas Rajons', 'KRA', 117, 'LV', 'Active', 0),
(1830, 'Kuldigas Rajons', 'KUL', 117, 'LV', 'Active', 0),
(1831, 'Liepajas Rajons', 'LPR', 117, 'LV', 'Active', 0),
(1832, 'Limbazu Rajons', 'LIM', 117, 'LV', 'Active', 0),
(1833, 'Ludzas Rajons', 'LUD', 117, 'LV', 'Active', 0),
(1834, 'Madonas Rajons', 'MAD', 117, 'LV', 'Active', 0),
(1835, 'Ogres Rajons', 'OGR', 117, 'LV', 'Active', 0),
(1836, 'Preilu Rajons', 'PRE', 117, 'LV', 'Active', 0),
(1837, 'Rezeknes Rajons', 'RZR', 117, 'LV', 'Active', 0),
(1838, 'Rigas Rajons', 'RGR', 117, 'LV', 'Active', 0),
(1839, 'Saldus Rajons', 'SAL', 117, 'LV', 'Active', 0),
(1840, 'Talsu Rajons', 'TAL', 117, 'LV', 'Active', 0),
(1841, 'Tukuma Rajons', 'TUK', 117, 'LV', 'Active', 0),
(1842, 'Valkas Rajons', 'VLK', 117, 'LV', 'Active', 0),
(1843, 'Valmieras Rajons', 'VLM', 117, 'LV', 'Active', 0),
(1844, 'Ventspils Rajons', 'VSR', 117, 'LV', 'Active', 0),
(1845, 'Daugavpils', 'DGV', 117, 'LV', 'Active', 0),
(1846, 'Jelgava', 'JGV', 117, 'LV', 'Active', 0),
(1847, 'Jurmala', 'JUR', 117, 'LV', 'Active', 0),
(1848, 'Liepaja', 'LPK', 117, 'LV', 'Active', 0),
(1849, 'Rezekne', 'RZK', 117, 'LV', 'Active', 0),
(1850, 'Riga', 'RGA', 117, 'LV', 'Active', 0),
(1851, 'Ventspils', 'VSL', 117, 'LV', 'Active', 0),
(1852, 'Berea', 'BE', 119, 'LS', 'Active', 0),
(1853, 'Butha-Buthe', 'BB', 119, 'LS', 'Active', 0),
(1854, 'Leribe', 'LE', 119, 'LS', 'Active', 0),
(1855, 'Mafeteng', 'MF', 119, 'LS', 'Active', 0),
(1856, 'Maseru', 'MS', 119, 'LS', 'Active', 0),
(1857, 'Mohale\'s Hoek', 'MH', 119, 'LS', 'Active', 0),
(1858, 'Mokhotlong', 'MK', 119, 'LS', 'Active', 0),
(1859, 'Qacha\'s Nek', 'QN', 119, 'LS', 'Active', 0),
(1860, 'Quthing', 'QT', 119, 'LS', 'Active', 0),
(1861, 'Thaba-Tseka', 'TT', 119, 'LS', 'Active', 0),
(1862, 'Bomi', 'BI', 120, 'LR', 'Active', 0),
(1863, 'Bong', 'BG', 120, 'LR', 'Active', 0),
(1864, 'Grand Bassa', 'GB', 120, 'LR', 'Active', 0),
(1865, 'Grand Cape Mount', 'CM', 120, 'LR', 'Active', 0),
(1866, 'Grand Gedeh', 'GG', 120, 'LR', 'Active', 0),
(1867, 'Grand Kru', 'GK', 120, 'LR', 'Active', 0),
(1868, 'Lofa', 'LO', 120, 'LR', 'Active', 0),
(1869, 'Margibi', 'MG', 120, 'LR', 'Active', 0),
(1870, 'Maryland', 'ML', 120, 'LR', 'Active', 0),
(1871, 'Montserrado', 'MS', 120, 'LR', 'Active', 0),
(1872, 'Nimba', 'NB', 120, 'LR', 'Active', 0),
(1873, 'River Cess', 'RC', 120, 'LR', 'Active', 0),
(1874, 'Sinoe', 'SN', 120, 'LR', 'Active', 0),
(1875, 'Ajdabiya', 'AJ', 121, 'LY', 'Active', 0),
(1876, 'Al \'Aziziyah', 'AZ', 121, 'LY', 'Active', 0),
(1877, 'Al Fatih', 'FA', 121, 'LY', 'Active', 0),
(1878, 'Al Jabal al Akhdar', 'JA', 121, 'LY', 'Active', 0),
(1879, 'Al Jufrah', 'JU', 121, 'LY', 'Active', 0),
(1880, 'Al Khums', 'KH', 121, '', 'Active', 0),
(1881, 'Al Kufrah', 'KU', 121, 'LY', 'Active', 0),
(1882, 'An Nuqat al Khams', 'NK', 121, 'LY', 'Active', 0),
(1883, 'Ash Shati\'', 'AS', 121, 'LY', 'Active', 0),
(1884, 'Awbari', 'AW', 121, 'LY', 'Active', 0),
(1885, 'Az Zawiyah', 'ZA', 121, 'LY', 'Active', 0),
(1886, 'Banghazi', 'BA', 121, 'LY', 'Active', 0),
(1887, 'Darnah', 'DA', 121, 'LY', 'Active', 0),
(1888, 'Ghadamis', 'GD', 121, 'LY', 'Active', 0),
(1889, 'Gharyan', 'GY', 121, 'LY', 'Active', 0),
(1890, 'Misratah', 'MI', 121, 'LY', 'Active', 0),
(1891, 'Murzuq', 'MZ', 121, 'LY', 'Active', 0),
(1892, 'Sabha', 'SB', 121, 'LY', 'Active', 0),
(1893, 'Sawfajjin', 'SW', 121, 'LY', 'Active', 0),
(1894, 'Surt', 'SU', 121, 'LY', 'Active', 0),
(1895, 'Tarabulus (Tripoli)', 'TL', 121, 'LY', 'Active', 0),
(1896, 'Tarhunah', 'TH', 121, 'LY', 'Active', 0),
(1897, 'Tubruq', 'TU', 121, 'LY', 'Active', 0),
(1898, 'Yafran', 'YA', 121, 'LY', 'Active', 0),
(1899, 'Zlitan', 'ZL', 121, 'LY', 'Active', 0),
(1900, 'Vaduz', 'V', 122, 'LI', 'Active', 0),
(1901, 'Schaan', 'A', 122, 'LI', 'Active', 0),
(1902, 'Balzers', 'B', 122, 'LI', 'Active', 0),
(1903, 'Triesen', 'N', 122, 'LI', 'Active', 0),
(1904, 'Eschen', 'E', 122, 'LI', 'Active', 0),
(1905, 'Mauren', 'M', 122, 'LI', 'Active', 0),
(1906, 'Triesenberg', 'T', 122, 'LI', 'Active', 0),
(1907, 'Ruggell', 'R', 122, 'LI', 'Active', 0),
(1908, 'Gamprin', 'G', 122, 'LI', 'Active', 0),
(1909, 'Schellenberg', 'L', 122, 'LI', 'Active', 0),
(1910, 'Planken', 'P', 122, 'LI', 'Active', 0),
(1911, 'Alytus', 'AL', 123, 'LT', 'Active', 0),
(1912, 'Kaunas', 'KA', 123, 'LT', 'Active', 0),
(1913, 'Klaipeda', 'KL', 123, 'LT', 'Active', 0),
(1914, 'Marijampole', 'MA', 123, 'LT', 'Active', 0),
(1915, 'Panevezys', 'PA', 123, 'LT', 'Active', 0),
(1916, 'Siauliai', 'SI', 123, 'LT', 'Active', 0),
(1917, 'Taurage', 'TA', 123, 'LT', 'Active', 0),
(1918, 'Telsiai', 'TE', 123, 'LT', 'Active', 0),
(1919, 'Utena', 'UT', 123, 'LT', 'Active', 0),
(1920, 'Vilnius', 'VI', 123, 'LT', 'Active', 0),
(1921, 'Diekirch', 'DD', 124, 'LU', 'Active', 0),
(1922, 'Clervaux', 'DC', 124, 'LU', 'Active', 0),
(1923, 'Redange', 'DR', 124, 'LU', 'Active', 0),
(1924, 'Vianden', 'DV', 124, 'LU', 'Active', 0),
(1925, 'Wiltz', 'DW', 124, 'LU', 'Active', 0),
(1926, 'Grevenmacher', 'GG', 124, 'LU', 'Active', 0),
(1927, 'Echternach', 'GE', 124, 'LU', 'Active', 0),
(1928, 'Remich', 'GR', 124, 'LU', 'Active', 0),
(1929, 'Luxembourg', 'LL', 124, 'LU', 'Active', 0),
(1930, 'Capellen', 'LC', 124, 'LU', 'Active', 0),
(1931, 'Esch-sur-Alzette', 'LE', 124, 'LU', 'Active', 0),
(1932, 'Mersch', 'LM', 124, 'LU', 'Active', 0),
(1933, 'Our Lady Fatima Parish', 'OLF', 125, 'MO', 'Active', 0),
(1934, 'St. Anthony Parish', 'ANT', 125, 'MO', 'Active', 0),
(1935, 'St. Lazarus Parish', 'LAZ', 125, 'MO', 'Active', 0),
(1936, 'Cathedral Parish', 'CAT', 125, 'MO', 'Active', 0),
(1937, 'St. Lawrence Parish', 'LAW', 125, 'MO', 'Active', 0),
(1938, 'Antananarivo', 'AN', 127, 'MG', 'Active', 0),
(1939, 'Antsiranana', 'AS', 127, 'MG', 'Active', 0),
(1940, 'Fianarantsoa', 'FN', 127, 'MG', 'Active', 0),
(1941, 'Mahajanga', 'MJ', 127, 'MG', 'Active', 0),
(1942, 'Toamasina', 'TM', 127, 'MG', 'Active', 0),
(1943, 'Toliara', 'TL', 127, 'MG', 'Active', 0),
(1944, 'Balaka', 'BLK', 128, 'MW', 'Active', 0),
(1945, 'Blantyre', 'BLT', 128, 'MW', 'Active', 0),
(1946, 'Chikwawa', 'CKW', 128, 'MW', 'Active', 0),
(1947, 'Chiradzulu', 'CRD', 128, 'MW', 'Active', 0),
(1948, 'Chitipa', 'CTP', 128, 'MW', 'Active', 0),
(1949, 'Dedza', 'DDZ', 128, 'MW', 'Active', 0),
(1950, 'Dowa', 'DWA', 128, 'MW', 'Active', 0),
(1951, 'Karonga', 'KRG', 128, 'MW', 'Active', 0),
(1952, 'Kasungu', 'KSG', 128, 'MW', 'Active', 0),
(1953, 'Likoma', 'LKM', 128, 'MW', 'Active', 0),
(1954, 'Lilongwe', 'LLG', 128, 'MW', 'Active', 0),
(1955, 'Machinga', 'MCG', 128, 'MW', 'Active', 0),
(1956, 'Mangochi', 'MGC', 128, 'MW', 'Active', 0),
(1957, 'Mchinji', 'MCH', 128, 'MW', 'Active', 0),
(1958, 'Mulanje', 'MLJ', 128, 'MW', 'Active', 0),
(1959, 'Mwanza', 'MWZ', 128, 'MW', 'Active', 0),
(1960, 'Mzimba', 'MZM', 128, 'MW', 'Active', 0),
(1961, 'Ntcheu', 'NTU', 128, 'MW', 'Active', 0),
(1962, 'Nkhata Bay', 'NKB', 128, 'MW', 'Active', 0),
(1963, 'Nkhotakota', 'NKH', 128, 'MW', 'Active', 0),
(1964, 'Nsanje', 'NSJ', 128, 'MW', 'Active', 0),
(1965, 'Ntchisi', 'NTI', 128, 'MW', 'Active', 0),
(1966, 'Phalombe', 'PHL', 128, 'MW', 'Active', 0),
(1967, 'Rumphi', 'RMP', 128, 'MW', 'Active', 0),
(1968, 'Salima', 'SLM', 128, 'MW', 'Active', 0),
(1969, 'Thyolo', 'THY', 128, 'MW', 'Active', 0),
(1970, 'Zomba', 'ZBA', 128, 'MW', 'Active', 0),
(1971, 'Johor', 'JO', 129, 'MY', 'Active', 0),
(1972, 'Kedah', 'KE', 129, 'MY', 'Active', 0),
(1973, 'Kelantan', 'KL', 129, 'MY', 'Active', 0),
(1974, 'Labuan', 'LA', 129, 'MY', 'Active', 0),
(1975, 'Melaka', 'ME', 129, 'MY', 'Active', 0),
(1976, 'Negeri Sembilan', 'NS', 129, 'MY', 'Active', 0),
(1977, 'Pahang', 'PA', 129, 'MY', 'Active', 0),
(1978, 'Perak', 'PE', 129, 'MY', 'Active', 0),
(1979, 'Perlis', 'PR', 129, 'MY', 'Active', 0),
(1980, 'Pulau Pinang', 'PP', 129, 'MY', 'Active', 0),
(1981, 'Sabah', 'SA', 129, 'MY', 'Active', 0),
(1982, 'Sarawak', 'SR', 129, 'MY', 'Active', 0),
(1983, 'Selangor', 'SE', 129, 'MY', 'Active', 0),
(1984, 'Terengganu', 'TE', 129, 'MY', 'Active', 0),
(1985, 'Wilayah Persekutuan', 'WP', 129, 'MY', 'Active', 0),
(1986, 'Thiladhunmathi Uthuru', 'THU', 130, 'MV', 'Active', 0),
(1987, 'Thiladhunmathi Dhekunu', 'THD', 130, 'MV', 'Active', 0),
(1988, 'Miladhunmadulu Uthuru', 'MLU', 130, 'MV', 'Active', 0),
(1989, 'Miladhunmadulu Dhekunu', 'MLD', 130, 'MV', 'Active', 0),
(1990, 'Maalhosmadulu Uthuru', 'MAU', 130, 'MV', 'Active', 0),
(1991, 'Maalhosmadulu Dhekunu', 'MAD', 130, 'MV', 'Active', 0),
(1992, 'Faadhippolhu', 'FAA', 130, 'MV', 'Active', 0),
(1993, 'Male Atoll', 'MAA', 130, 'MV', 'Active', 0),
(1994, 'Ari Atoll Uthuru', 'AAU', 130, 'MV', 'Active', 0),
(1995, 'Ari Atoll Dheknu', 'AAD', 130, 'MV', 'Active', 0),
(1996, 'Felidhe Atoll', 'FEA', 130, 'MV', 'Active', 0),
(1997, 'Mulaku Atoll', 'MUA', 130, 'MV', 'Active', 0),
(1998, 'Nilandhe Atoll Uthuru', 'NAU', 130, 'MV', 'Active', 0),
(1999, 'Nilandhe Atoll Dhekunu', 'NAD', 130, 'MV', 'Active', 0),
(2000, 'Kolhumadulu', 'KLH', 130, 'MV', 'Active', 0),
(2001, 'Hadhdhunmathi', 'HDH', 130, 'MV', 'Active', 0),
(2002, 'Huvadhu Atoll Uthuru', 'HAU', 130, 'MV', 'Active', 0),
(2003, 'Huvadhu Atoll Dhekunu', 'HAD', 130, 'MV', 'Active', 0),
(2004, 'Fua Mulaku', 'FMU', 130, 'MV', 'Active', 0),
(2005, 'Addu', 'ADD', 130, 'MV', 'Active', 0),
(2006, 'Gao', 'GA', 131, 'ML', 'Active', 0),
(2007, 'Kayes', 'KY', 131, 'ML', 'Active', 0),
(2008, 'Kidal', 'KD', 131, 'ML', 'Active', 0),
(2009, 'Koulikoro', 'KL', 131, 'ML', 'Active', 0),
(2010, 'Mopti', 'MP', 131, 'ML', 'Active', 0),
(2011, 'Segou', 'SG', 131, 'ML', 'Active', 0),
(2012, 'Sikasso', 'SK', 131, 'ML', 'Active', 0),
(2013, 'Tombouctou', 'TB', 131, 'ML', 'Active', 0),
(2014, 'Bamako Capital District', 'CD', 131, 'ML', 'Active', 0),
(2015, 'Attard', 'ATT', 132, 'MT', 'Active', 0),
(2016, 'Balzan', 'BAL', 132, 'MT', 'Active', 0),
(2017, 'Birgu', 'BGU', 132, 'MT', 'Active', 0),
(2018, 'Birkirkara', 'BKK', 132, 'MT', 'Active', 0),
(2019, 'Birzebbuga', 'BRZ', 132, 'MT', 'Active', 0),
(2020, 'Bormla', 'BOR', 132, 'MT', 'Active', 0),
(2021, 'Dingli', 'DIN', 132, 'MT', 'Active', 0),
(2022, 'Fgura', 'FGU', 132, 'MT', 'Active', 0),
(2023, 'Floriana', 'FLO', 132, 'MT', 'Active', 0),
(2024, 'Gudja', 'GDJ', 132, 'MT', 'Active', 0),
(2025, 'Gzira', 'GZR', 132, 'MT', 'Active', 0),
(2026, 'Gargur', 'GRG', 132, 'MT', 'Active', 0),
(2027, 'Gaxaq', 'GXQ', 132, 'MT', 'Active', 0),
(2028, 'Hamrun', 'HMR', 132, 'MT', 'Active', 0),
(2029, 'Iklin', 'IKL', 132, 'MT', 'Active', 0),
(2030, 'Isla', 'ISL', 132, 'MT', 'Active', 0),
(2031, 'Kalkara', 'KLK', 132, 'MT', 'Active', 0),
(2032, 'Kirkop', 'KRK', 132, 'MT', 'Active', 0),
(2033, 'Lija', 'LIJ', 132, 'MT', 'Active', 0),
(2034, 'Luqa', 'LUQ', 132, 'MT', 'Active', 0),
(2035, 'Marsa', 'MRS', 132, 'MT', 'Active', 0),
(2036, 'Marsaskala', 'MKL', 132, 'MT', 'Active', 0),
(2037, 'Marsaxlokk', 'MXL', 132, 'MT', 'Active', 0),
(2038, 'Mdina', 'MDN', 132, 'MT', 'Active', 0),
(2039, 'Melliea', 'MEL', 132, 'MT', 'Active', 0),
(2040, 'Mgarr', 'MGR', 132, 'MT', 'Active', 0),
(2041, 'Mosta', 'MST', 132, 'MT', 'Active', 0),
(2042, 'Mqabba', 'MQA', 132, 'MT', 'Active', 0),
(2043, 'Msida', 'MSI', 132, 'MT', 'Active', 0),
(2044, 'Mtarfa', 'MTF', 132, 'MT', 'Active', 0),
(2045, 'Naxxar', 'NAX', 132, 'MT', 'Active', 0),
(2046, 'Paola', 'PAO', 132, 'MT', 'Active', 0),
(2047, 'Pembroke', 'PEM', 132, 'MT', 'Active', 0),
(2048, 'Pieta', 'PIE', 132, 'MT', 'Active', 0),
(2049, 'Qormi', 'QOR', 132, 'MT', 'Active', 0),
(2050, 'Qrendi', 'QRE', 132, 'MT', 'Active', 0),
(2051, 'Rabat', 'RAB', 132, 'MT', 'Active', 0),
(2052, 'Safi', 'SAF', 132, 'MT', 'Active', 0),
(2053, 'San Giljan', 'SGI', 132, 'MT', 'Active', 0),
(2054, 'Santa Lucija', 'SLU', 132, 'MT', 'Active', 0),
(2055, 'San Pawl il-Bahar', 'SPB', 132, 'MT', 'Active', 0),
(2056, 'San Gwann', 'SGW', 132, 'MT', 'Active', 0),
(2057, 'Santa Venera', 'SVE', 132, 'MT', 'Active', 0),
(2058, 'Siggiewi', 'SIG', 132, 'MT', 'Active', 0),
(2059, 'Sliema', 'SLM', 132, 'MT', 'Active', 0),
(2060, 'Swieqi', 'SWQ', 132, 'MT', 'Active', 0),
(2061, 'Ta Xbiex', 'TXB', 132, 'MT', 'Active', 0),
(2062, 'Tarxien', 'TRX', 132, 'MT', 'Active', 0),
(2063, 'Valletta', 'VLT', 132, 'MT', 'Active', 0),
(2064, 'Xgajra', 'XGJ', 132, 'MT', 'Active', 0),
(2065, 'Zabbar', 'ZBR', 132, 'MT', 'Active', 0),
(2066, 'Zebbug', 'ZBG', 132, 'MT', 'Active', 0),
(2067, 'Zejtun', 'ZJT', 132, 'MT', 'Active', 0),
(2068, 'Zurrieq', 'ZRQ', 132, 'MT', 'Active', 0),
(2069, 'Fontana', 'FNT', 132, 'MT', 'Active', 0),
(2070, 'Ghajnsielem', 'GHJ', 132, 'MT', 'Active', 0),
(2071, 'Gharb', 'GHR', 132, 'MT', 'Active', 0),
(2072, 'Ghasri', 'GHS', 132, 'MT', 'Active', 0),
(2073, 'Kercem', 'KRC', 132, 'MT', 'Active', 0),
(2074, 'Munxar', 'MUN', 132, 'MT', 'Active', 0),
(2075, 'Nadur', 'NAD', 132, 'MT', 'Active', 0),
(2076, 'Qala', 'QAL', 132, 'MT', 'Active', 0),
(2077, 'Victoria', 'VIC', 132, 'MT', 'Active', 0),
(2078, 'San Lawrenz', 'SLA', 132, 'MT', 'Active', 0),
(2079, 'Sannat', 'SNT', 132, 'MT', 'Active', 0),
(2080, 'Xagra', 'ZAG', 132, 'MT', 'Active', 0),
(2081, 'Xewkija', 'XEW', 132, 'MT', 'Active', 0),
(2082, 'Zebbug', 'ZEB', 132, 'MT', 'Active', 0),
(2083, 'Ailinginae', 'ALG', 133, 'MH', 'Active', 0),
(2084, 'Ailinglaplap', 'ALL', 133, 'MH', 'Active', 0),
(2085, 'Ailuk', 'ALK', 133, 'MH', 'Active', 0),
(2086, 'Arno', 'ARN', 133, 'MH', 'Active', 0),
(2087, 'Aur', 'AUR', 133, 'MH', 'Active', 0),
(2088, 'Bikar', 'BKR', 133, 'MH', 'Active', 0),
(2089, 'Bikini', 'BKN', 133, 'MH', 'Active', 0),
(2090, 'Bokak', 'BKK', 133, 'MH', 'Active', 0),
(2091, 'Ebon', 'EBN', 133, 'MH', 'Active', 0);
INSERT INTO `state` (`iStateId`, `vState`, `vStateCode`, `iCountryId`, `vCountryCode`, `eStatus`, `iSysRecDeleted`) VALUES
(2092, 'Enewetak', 'ENT', 133, 'MH', 'Active', 0),
(2093, 'Erikub', 'EKB', 133, 'MH', 'Active', 0),
(2094, 'Jabat', 'JBT', 133, 'MH', 'Active', 0),
(2095, 'Jaluit', 'JLT', 133, 'MH', 'Active', 0),
(2096, 'Jemo', 'JEM', 133, 'MH', 'Active', 0),
(2097, 'Kili', 'KIL', 133, 'MH', 'Active', 0),
(2098, 'Kwajalein', 'KWJ', 133, 'MH', 'Active', 0),
(2099, 'Lae', 'LAE', 133, 'MH', 'Active', 0),
(2100, 'Lib', 'LIB', 133, 'MH', 'Active', 0),
(2101, 'Likiep', 'LKP', 133, 'MH', 'Active', 0),
(2102, 'Majuro', 'MJR', 133, 'MH', 'Active', 0),
(2103, 'Maloelap', 'MLP', 133, 'MH', 'Active', 0),
(2104, 'Mejit', 'MJT', 133, 'MH', 'Active', 0),
(2105, 'Mili', 'MIL', 133, 'MH', 'Active', 0),
(2106, 'Namorik', 'NMK', 133, 'MH', 'Active', 0),
(2107, 'Namu', 'NAM', 133, 'MH', 'Active', 0),
(2108, 'Rongelap', 'RGL', 133, 'MH', 'Active', 0),
(2109, 'Rongrik', 'RGK', 133, 'MH', 'Active', 0),
(2110, 'Toke', 'TOK', 133, 'MH', 'Active', 0),
(2111, 'Ujae', 'UJA', 133, 'MH', 'Active', 0),
(2112, 'Ujelang', 'UJL', 133, 'MH', 'Active', 0),
(2113, 'Utirik', 'UTK', 133, 'MH', 'Active', 0),
(2114, 'Wotho', 'WTH', 133, 'MH', 'Active', 0),
(2115, 'Wotje', 'WTJ', 133, 'MH', 'Active', 0),
(2116, 'Adrar', 'AD', 135, 'MR', 'Active', 0),
(2117, 'Assaba', 'AS', 135, 'MR', 'Active', 0),
(2118, 'Brakna', 'BR', 135, 'MR', 'Active', 0),
(2119, 'Dakhlet Nouadhibou', 'DN', 135, 'MR', 'Active', 0),
(2120, 'Gorgol', 'GO', 135, 'MR', 'Active', 0),
(2121, 'Guidimaka', 'GM', 135, 'MR', 'Active', 0),
(2122, 'Hodh Ech Chargui', 'HC', 135, 'MR', 'Active', 0),
(2123, 'Hodh El Gharbi', 'HG', 135, 'MR', 'Active', 0),
(2124, 'Inchiri', 'IN', 135, 'MR', 'Active', 0),
(2125, 'Tagant', 'TA', 135, 'MR', 'Active', 0),
(2126, 'Tiris Zemmour', 'TZ', 135, 'MR', 'Active', 0),
(2127, 'Trarza', 'TR', 135, 'MR', 'Active', 0),
(2128, 'Nouakchott', 'NO', 135, 'MR', 'Active', 0),
(2129, 'Beau Bassin-Rose Hill', 'BR', 136, 'MU', 'Active', 0),
(2130, 'Curepipe', 'CU', 136, 'MU', 'Active', 0),
(2131, 'Port Louis', 'PU', 136, 'MU', 'Active', 0),
(2132, 'Quatre Bornes', 'QB', 136, 'MU', 'Active', 0),
(2133, 'Vacoas-Phoenix', 'VP', 136, 'MU', 'Active', 0),
(2134, 'Agalega Islands', 'AG', 136, 'MU', 'Active', 0),
(2135, 'Cargados Carajos Shoals (Saint B', 'CC', 136, 'MU', 'Active', 0),
(2136, 'Rodrigues', 'RO', 136, 'MU', 'Active', 0),
(2137, 'Black River', 'BL', 136, 'MU', 'Active', 0),
(2138, 'Flacq', 'FL', 136, 'MU', 'Active', 0),
(2139, 'Grand Port', 'GP', 136, 'MU', 'Active', 0),
(2140, 'Moka', 'MO', 136, 'MU', 'Active', 0),
(2141, 'Pamplemousses', 'PA', 136, 'MU', 'Active', 0),
(2142, 'Plaines Wilhems', 'PW', 136, 'MU', 'Active', 0),
(2143, 'Port Louis', 'PL', 136, 'MU', 'Active', 0),
(2144, 'Riviere du Rempart', 'RR', 136, 'MU', 'Active', 0),
(2145, 'Savanne', 'SA', 136, 'MU', 'Active', 0),
(2146, 'Aguascalientes', 'AGU', 138, 'MX', 'Active', 0),
(2147, 'Baja California Norte', 'BCN', 138, 'MX', 'Active', 0),
(2148, 'Baja California Sur', 'BCS', 138, 'MX', 'Active', 0),
(2149, 'Campeche', 'CAM', 138, 'MX', 'Active', 0),
(2150, 'Chiapas', 'CHP', 138, 'MX', 'Active', 0),
(2151, 'Chihuahua', 'CHH', 138, 'MX', 'Active', 0),
(2152, 'Coahuila de Zaragoza', 'COA', 138, 'MX', 'Active', 0),
(2153, 'Colima', 'COL', 138, 'MX', 'Active', 0),
(2154, 'Distrito Federal', 'DIF', 138, 'MX', 'Active', 0),
(2155, 'Durango', 'DUR', 138, 'MX', 'Active', 0),
(2156, 'Guanajuato', 'GUA', 138, 'MX', 'Active', 0),
(2157, 'Guerrero', 'GRO', 138, 'MX', 'Active', 0),
(2158, 'Hidalgo', 'HID', 138, 'MX', 'Active', 0),
(2159, 'Jalisco', 'JAL', 138, 'MX', 'Active', 0),
(2160, 'Mexico', 'MEX', 138, 'MX', 'Active', 0),
(2161, 'Michoacan de Ocampo', 'MIC', 138, 'MX', 'Active', 0),
(2162, 'Morelos', 'MOR', 138, 'MX', 'Active', 0),
(2163, 'Nayarit', 'NAY', 138, 'MX', 'Active', 0),
(2164, 'Nuevo Leon', 'NLE', 138, 'MX', 'Active', 0),
(2165, 'Oaxaca', 'OAX', 138, 'MX', 'Active', 0),
(2166, 'Puebla', 'PUE', 138, 'MX', 'Active', 0),
(2167, 'Queretaro de Arteaga', 'QUE', 138, 'MX', 'Active', 0),
(2168, 'Quintana Roo', 'ROO', 138, 'MX', 'Active', 0),
(2169, 'San Luis Potosi', 'SLP', 138, 'MX', 'Active', 0),
(2170, 'Sinaloa', 'SIN', 138, 'MX', 'Active', 0),
(2171, 'Sonora', 'SON', 138, 'MX', 'Active', 0),
(2172, 'Tabasco', 'TAB', 138, 'MX', 'Active', 0),
(2173, 'Tamaulipas', 'TAM', 138, 'MX', 'Active', 0),
(2174, 'Tlaxcala', 'TLA', 138, 'MX', 'Active', 0),
(2175, 'Veracruz-Llave', 'VER', 138, 'MX', 'Active', 0),
(2176, 'Yucatan', 'YUC', 138, 'MX', 'Active', 0),
(2177, 'Zacatecas', 'ZAC', 138, 'MX', 'Active', 0),
(2178, 'Chuuk', 'C', 139, 'FM', 'Active', 0),
(2179, 'Kosrae', 'K', 139, 'FM', 'Active', 0),
(2180, 'Pohnpei', 'P', 139, 'FM', 'Active', 0),
(2181, 'Yap', 'Y', 139, 'FM', 'Active', 0),
(2182, 'Gagauzia', 'GA', 140, 'MD', 'Active', 0),
(2183, 'Chisinau', 'CU', 140, 'MD', 'Active', 0),
(2184, 'Balti', 'BA', 140, 'MD', 'Active', 0),
(2185, 'Cahul', 'CA', 140, 'MD', 'Active', 0),
(2186, 'Edinet', 'ED', 140, 'MD', 'Active', 0),
(2187, 'Lapusna', 'LA', 140, 'MD', 'Active', 0),
(2188, 'Orhei', 'OR', 140, 'MD', 'Active', 0),
(2189, 'Soroca', 'SO', 140, 'MD', 'Active', 0),
(2190, 'Tighina', 'TI', 140, 'MD', 'Active', 0),
(2191, 'Ungheni', 'UN', 140, 'MD', 'Active', 0),
(2192, 'St?nga Nistrului', 'SN', 140, 'MD', 'Active', 0),
(2193, 'Fontvieille', 'FV', 141, 'MC', 'Active', 0),
(2194, 'La Condamine', 'LC', 141, 'MC', 'Active', 0),
(2195, 'Monaco-Ville', 'MV', 141, 'MC', 'Active', 0),
(2196, 'Monte-Carlo', 'MC', 141, 'MC', 'Active', 0),
(2197, 'Ulanbaatar', '1', 142, 'MN', 'Active', 0),
(2198, 'Orhon', '035', 142, 'MN', 'Active', 0),
(2199, 'Darhan uul', '037', 142, 'MN', 'Active', 0),
(2200, 'Hentiy', '039', 142, 'MN', 'Active', 0),
(2201, 'Hovsgol', '041', 142, 'MN', 'Active', 0),
(2202, 'Hovd', '043', 142, 'MN', 'Active', 0),
(2203, 'Uvs', '046', 142, 'MN', 'Active', 0),
(2204, 'Tov', '047', 142, 'MN', 'Active', 0),
(2205, 'Selenge', '049', 142, 'MN', 'Active', 0),
(2206, 'Suhbaatar', '051', 142, 'MN', 'Active', 0),
(2207, 'Omnogovi', '053', 142, 'MN', 'Active', 0),
(2208, 'Ovorhangay', '055', 142, 'MN', 'Active', 0),
(2209, 'Dzavhan', '057', 142, 'MN', 'Active', 0),
(2210, 'DundgovL', '059', 142, 'MN', 'Active', 0),
(2211, 'Dornod', '061', 142, 'MN', 'Active', 0),
(2212, 'Dornogov', '063', 142, 'MN', 'Active', 0),
(2213, 'Govi-Sumber', '064', 142, 'MN', 'Active', 0),
(2214, 'Govi-Altay', '065', 142, 'MN', 'Active', 0),
(2215, 'Bulgan', '067', 142, 'MN', 'Active', 0),
(2216, 'Bayanhongor', '069', 142, 'MN', 'Active', 0),
(2217, 'Bayan-Olgiy', '071', 142, 'MN', 'Active', 0),
(2218, 'Arhangay', '073', 142, 'MN', 'Active', 0),
(2219, 'Saint Anthony', 'A', 143, 'MS', 'Active', 0),
(2220, 'Saint Georges', 'G', 143, 'MS', 'Active', 0),
(2221, 'Saint Peter', 'P', 143, 'MS', 'Active', 0),
(2222, 'Agadir', 'AGD', 144, 'MA', 'Active', 0),
(2223, 'Al Hoceima', 'HOC', 144, 'MA', 'Active', 0),
(2224, 'Azilal', 'AZI', 144, 'MA', 'Active', 0),
(2225, 'Beni Mellal', 'BME', 144, 'MA', 'Active', 0),
(2226, 'Ben Slimane', 'BSL', 144, 'MA', 'Active', 0),
(2227, 'Boulemane', 'BLM', 144, 'MA', 'Active', 0),
(2228, 'Casablanca', 'CBL', 144, 'MA', 'Active', 0),
(2229, 'Chaouen', 'CHA', 144, 'MA', 'Active', 0),
(2230, 'El Jadida', 'EJA', 144, 'MA', 'Active', 0),
(2231, 'El Kelaa des Sraghna', 'EKS', 144, 'MA', 'Active', 0),
(2232, 'Er Rachidia', 'ERA', 144, 'MA', 'Active', 0),
(2233, 'Essaouira', 'ESS', 144, 'MA', 'Active', 0),
(2234, 'Fes', 'FES', 144, 'MA', 'Active', 0),
(2235, 'Figuig', 'FIG', 144, 'MA', 'Active', 0),
(2236, 'Guelmim', 'GLM', 144, 'MA', 'Active', 0),
(2237, 'Ifrane', 'IFR', 144, 'MA', 'Active', 0),
(2238, 'Kenitra', 'KEN', 144, 'MA', 'Active', 0),
(2239, 'Khemisset', 'KHM', 144, 'MA', 'Active', 0),
(2240, 'Khenifra', 'KHN', 144, 'MA', 'Active', 0),
(2241, 'Khouribga', 'KHO', 144, 'MA', 'Active', 0),
(2242, 'Laayoune', 'LYN', 144, 'MA', 'Active', 0),
(2243, 'Larache', 'LAR', 144, 'MA', 'Active', 0),
(2244, 'Marrakech', 'MRK', 144, 'MA', 'Active', 0),
(2245, 'Meknes', 'MKN', 144, 'MA', 'Active', 0),
(2246, 'Nador', 'NAD', 144, 'MA', 'Active', 0),
(2247, 'Ouarzazate', 'ORZ', 144, 'MA', 'Active', 0),
(2248, 'Oujda', 'OUJ', 144, 'MA', 'Active', 0),
(2249, 'Rabat-Sale', 'RSA', 144, 'MA', 'Active', 0),
(2250, 'Safi', 'SAF', 144, 'MA', 'Active', 0),
(2251, 'Settat', 'SET', 144, 'MA', 'Active', 0),
(2252, 'Sidi Kacem', 'SKA', 144, 'MA', 'Active', 0),
(2253, 'Tangier', 'TGR', 144, 'MA', 'Active', 0),
(2254, 'Tan-Tan', 'TAN', 144, 'MA', 'Active', 0),
(2255, 'Taounate', 'TAO', 144, 'MA', 'Active', 0),
(2256, 'Taroudannt', 'TRD', 144, 'MA', 'Active', 0),
(2257, 'Tata', 'TAT', 144, 'MA', 'Active', 0),
(2258, 'Taza', 'TAZ', 144, 'MA', 'Active', 0),
(2259, 'Tetouan', 'TET', 144, 'MA', 'Active', 0),
(2260, 'Tiznit', 'TIZ', 144, 'MA', 'Active', 0),
(2261, 'Ad Dakhla', 'ADK', 144, 'MA', 'Active', 0),
(2262, 'Boujdour', 'BJD', 144, 'MA', 'Active', 0),
(2263, 'Es Smara', 'ESM', 144, 'MA', 'Active', 0),
(2264, 'Cabo Delgado', 'CD', 145, 'MZ', 'Active', 0),
(2265, 'Gaza', 'GZ', 145, 'MZ', 'Active', 0),
(2266, 'Inhambane', 'IN', 145, 'MZ', 'Active', 0),
(2267, 'Manica', 'MN', 145, 'MZ', 'Active', 0),
(2268, 'Maputo (city)', 'MC', 145, 'MZ', 'Active', 0),
(2269, 'Maputo', 'MP', 145, 'MZ', 'Active', 0),
(2270, 'Nampula', 'NA', 145, 'MZ', 'Active', 0),
(2271, 'Niassa', 'NI', 145, 'MZ', 'Active', 0),
(2272, 'Sofala', 'SO', 145, 'MZ', 'Active', 0),
(2273, 'Tete', 'TE', 145, 'MZ', 'Active', 0),
(2274, 'Zambezia', 'ZA', 145, 'MZ', 'Active', 0),
(2275, 'Ayeyarwady', 'AY', 146, 'MM', 'Active', 0),
(2276, 'Bago', 'BG', 146, 'MM', 'Active', 0),
(2277, 'Magway', 'MG', 146, 'MM', 'Active', 0),
(2278, 'Mandalay', 'MD', 146, 'MM', 'Active', 0),
(2279, 'Sagaing', 'SG', 146, 'MM', 'Active', 0),
(2280, 'Tanintharyi', 'TN', 146, 'MM', 'Active', 0),
(2281, 'Yangon', 'YG', 146, 'MM', 'Active', 0),
(2282, 'Chin State', 'CH', 146, 'MM', 'Active', 0),
(2283, 'Kachin State', 'KC', 146, 'MM', 'Active', 0),
(2284, 'Kayah State', 'KH', 146, 'MM', 'Active', 0),
(2285, 'Kayin State', 'KN', 146, 'MM', 'Active', 0),
(2286, 'Mon State', 'MN', 146, 'MM', 'Active', 0),
(2287, 'Rakhine State', 'RK', 146, 'MM', 'Active', 0),
(2288, 'Shan State', 'SH', 146, 'MM', 'Active', 0),
(2289, 'Caprivi', 'CA', 147, 'NA', 'Active', 0),
(2290, 'Erongo', 'ER', 147, 'NA', 'Active', 0),
(2291, 'Hardap', 'HA', 147, 'NA', 'Active', 0),
(2292, 'Karas', 'KR', 147, 'NA', 'Active', 0),
(2293, 'Kavango', 'KV', 147, 'NA', 'Active', 0),
(2294, 'Khomas', 'KH', 147, 'NA', 'Active', 0),
(2295, 'Kunene', 'KU', 147, 'NA', 'Active', 0),
(2296, 'Ohangwena', 'OW', 147, 'NA', 'Active', 0),
(2297, 'Omaheke', 'OK', 147, 'NA', 'Active', 0),
(2298, 'Omusati', 'OT', 147, 'NA', 'Active', 0),
(2299, 'Oshana', 'ON', 147, 'NA', 'Active', 0),
(2300, 'Oshikoto', 'OO', 147, 'NA', 'Active', 0),
(2301, 'Otjozondjupa', 'OJ', 147, 'NA', 'Active', 0),
(2302, 'Aiwo', 'AO', 148, 'NR', 'Active', 0),
(2303, 'Anabar', 'AA', 148, 'NR', 'Active', 0),
(2304, 'Anetan', 'AT', 148, 'NR', 'Active', 0),
(2305, 'Anibare', 'AI', 148, 'NR', 'Active', 0),
(2306, 'Baiti', 'BA', 148, 'NR', 'Active', 0),
(2307, 'Boe', 'BO', 148, 'NR', 'Active', 0),
(2308, 'Buada', 'BU', 148, 'NR', 'Active', 0),
(2309, 'Denigomodu', 'DE', 148, 'NR', 'Active', 0),
(2310, 'Ewa', 'EW', 148, 'NR', 'Active', 0),
(2311, 'Ijuw', 'IJ', 148, 'NR', 'Active', 0),
(2312, 'Meneng', 'ME', 148, 'NR', 'Active', 0),
(2313, 'Nibok', 'NI', 148, 'NR', 'Active', 0),
(2314, 'Uaboe', 'UA', 148, 'NR', 'Active', 0),
(2315, 'Yaren', 'YA', 148, 'NR', 'Active', 0),
(2316, 'Bagmati', 'BA', 149, 'NP', 'Active', 0),
(2317, 'Bheri', 'BH', 149, 'NP', 'Active', 0),
(2318, 'Dhawalagiri', 'DH', 149, 'NP', 'Active', 0),
(2319, 'Gandaki', 'GA', 149, 'NP', 'Active', 0),
(2320, 'Janakpur', 'JA', 149, 'NP', 'Active', 0),
(2321, 'Karnali', 'KA', 149, 'NP', 'Active', 0),
(2322, 'Kosi', 'KO', 149, 'NP', 'Active', 0),
(2323, 'Lumbini', 'LU', 149, 'NP', 'Active', 0),
(2324, 'Mahakali', 'MA', 149, 'NP', 'Active', 0),
(2325, 'Mechi', 'ME', 149, 'NP', 'Active', 0),
(2326, 'Narayani', 'NA', 149, 'NP', 'Active', 0),
(2327, 'Rapti', 'RA', 149, 'NP', 'Active', 0),
(2328, 'Sagarmatha', 'SA', 149, 'NP', 'Active', 0),
(2329, 'Seti', 'SE', 149, 'NP', 'Active', 0),
(2330, 'Drenthe', 'DR', 150, 'NL', 'Active', 0),
(2331, 'Flevoland', 'FL', 150, 'NL', 'Active', 0),
(2332, 'Friesland', 'FR', 150, 'NL', 'Active', 0),
(2333, 'Gelderland', 'GE', 150, 'NL', 'Active', 0),
(2334, 'Groningen', 'GR', 150, 'NL', 'Active', 0),
(2335, 'Limburg', 'LI', 150, 'NL', 'Active', 0),
(2336, 'Noord Brabant', 'NB', 150, 'NL', 'Active', 0),
(2337, 'Noord Holland', 'NH', 150, 'NL', 'Active', 0),
(2338, 'Overijssel', 'OV', 150, 'NL', 'Active', 0),
(2339, 'Utrecht', 'UT', 150, 'NL', 'Active', 0),
(2340, 'Zeeland', 'ZE', 150, 'NL', 'Active', 0),
(2341, 'Zuid Holland', 'ZH', 150, 'NL', 'Active', 0),
(2342, 'Iles Loyaute', 'L', 152, 'NC', 'Active', 0),
(2343, 'Nord', 'N', 152, 'NC', 'Active', 0),
(2344, 'Sud', 'S', 152, 'NC', 'Active', 0),
(2345, 'Auckland', 'AUK', 153, 'NZ', 'Active', 0),
(2346, 'Bay of Plenty', 'BOP', 153, 'NZ', 'Active', 0),
(2347, 'Canterbury', 'CAN', 153, 'NZ', 'Active', 0),
(2348, 'Coromandel', 'COR', 153, 'NZ', 'Active', 0),
(2349, 'Gisborne', 'GIS', 153, 'NZ', 'Active', 0),
(2350, 'Fiordland', 'FIO', 153, 'NZ', 'Active', 0),
(2351, 'Hawke\'s Bay', 'HKB', 153, 'NZ', 'Active', 0),
(2352, 'Marlborough', 'MBH', 153, 'NZ', 'Active', 0),
(2353, 'Manawatu-Wanganui', 'MWT', 153, 'NZ', 'Active', 0),
(2354, 'Mt Cook-Mackenzie', 'MCM', 153, 'NZ', 'Active', 0),
(2355, 'Nelson', 'NSN', 153, 'NZ', 'Active', 0),
(2356, 'Northland', 'NTL', 153, 'NZ', 'Active', 0),
(2357, 'Otago', 'OTA', 153, 'NZ', 'Active', 0),
(2358, 'Southland', 'STL', 153, 'NZ', 'Active', 0),
(2359, 'Taranaki', 'TKI', 153, 'NZ', 'Active', 0),
(2360, 'Wellington', 'WGN', 153, 'NZ', 'Active', 0),
(2361, 'Waikato', 'WKO', 153, 'NZ', 'Active', 0),
(2362, 'Wairprarapa', 'WAI', 153, 'NZ', 'Active', 0),
(2363, 'West Coast', 'WTC', 153, 'NZ', 'Active', 0),
(2364, 'Atlantico Norte', 'AN', 154, 'NI', 'Active', 0),
(2365, 'Atlantico Sur', 'AS', 154, 'NI', 'Active', 0),
(2366, 'Boaco', 'BO', 154, 'NI', 'Active', 0),
(2367, 'Carazo', 'CA', 154, 'NI', 'Active', 0),
(2368, 'Chinandega', 'CI', 154, 'NI', 'Active', 0),
(2369, 'Chontales', 'CO', 154, 'NI', 'Active', 0),
(2370, 'Esteli', 'ES', 154, 'NI', 'Active', 0),
(2371, 'Granada', 'GR', 154, 'NI', 'Active', 0),
(2372, 'Jinotega', 'JI', 154, 'NI', 'Active', 0),
(2373, 'Leon', 'LE', 154, 'NI', 'Active', 0),
(2374, 'Madriz', 'MD', 154, 'NI', 'Active', 0),
(2375, 'Managua', 'MN', 154, 'NI', 'Active', 0),
(2376, 'Masaya', 'MS', 154, 'NI', 'Active', 0),
(2377, 'Matagalpa', 'MT', 154, 'NI', 'Active', 0),
(2378, 'Nuevo Segovia', 'NS', 154, 'NI', 'Active', 0),
(2379, 'Rio San Juan', 'RS', 154, 'NI', 'Active', 0),
(2380, 'Rivas', 'RI', 154, 'NI', 'Active', 0),
(2381, 'Agadez', 'AG', 155, 'NE', 'Active', 0),
(2382, 'Diffa', 'DF', 155, 'NE', 'Active', 0),
(2383, 'Dosso', 'DS', 155, 'NE', 'Active', 0),
(2384, 'Maradi', 'MA', 155, 'NE', 'Active', 0),
(2385, 'Niamey', 'NM', 155, 'NE', 'Active', 0),
(2386, 'Tahoua', 'TH', 155, 'NE', 'Active', 0),
(2387, 'Tillaberi', 'TL', 155, 'NE', 'Active', 0),
(2388, 'Zinder', 'ZD', 155, 'NE', 'Active', 0),
(2389, 'Abia', 'AB', 156, 'NG', 'Active', 0),
(2390, 'Abuja Federal Capital Territory', 'CT', 156, 'NG', 'Active', 0),
(2391, 'Adamawa', 'AD', 156, 'NG', 'Active', 0),
(2392, 'Akwa Ibom', 'AK', 156, 'NG', 'Active', 0),
(2393, 'Anambra', 'AN', 156, 'NG', 'Active', 0),
(2394, 'Bauchi', 'BC', 156, 'NG', 'Active', 0),
(2395, 'Bayelsa', 'BY', 156, 'NG', 'Active', 0),
(2396, 'Benue', 'BN', 156, 'NG', 'Active', 0),
(2397, 'Borno', 'BO', 156, 'NG', 'Active', 0),
(2398, 'Cross River', 'CR', 156, 'NG', 'Active', 0),
(2399, 'Delta', 'DE', 156, 'NG', 'Active', 0),
(2400, 'Ebonyi', 'EB', 156, 'NG', 'Active', 0),
(2401, 'Edo', 'ED', 156, 'NG', 'Active', 0),
(2402, 'Ekiti', 'EK', 156, 'NG', 'Active', 0),
(2403, 'Enugu', 'EN', 156, 'NG', 'Active', 0),
(2404, 'Gombe', 'GO', 156, 'NG', 'Active', 0),
(2405, 'Imo', 'IM', 156, 'NG', 'Active', 0),
(2406, 'Jigawa', 'JI', 156, 'NG', 'Active', 0),
(2407, 'Kaduna', 'KD', 156, 'NG', 'Active', 0),
(2408, 'Kano', 'KN', 156, 'NG', 'Active', 0),
(2409, 'Katsina', 'KT', 156, 'NG', 'Active', 0),
(2410, 'Kebbi', 'KE', 156, 'NG', 'Active', 0),
(2411, 'Kogi', 'KO', 156, 'NG', 'Active', 0),
(2412, 'Kwara', 'KW', 156, 'NG', 'Active', 0),
(2413, 'Lagos', 'LA', 156, 'NG', 'Active', 0),
(2414, 'Nassarawa', 'NA', 156, 'NG', 'Active', 0),
(2415, 'Niger', 'NI', 156, 'NG', 'Active', 0),
(2416, 'Ogun', 'OG', 156, 'NG', 'Active', 0),
(2417, 'Ondo', 'ONG', 156, 'NG', 'Active', 0),
(2418, 'Osun', 'OS', 156, 'NG', 'Active', 0),
(2419, 'Oyo', 'OY', 156, 'NG', 'Active', 0),
(2420, 'Plateau', 'PL', 156, 'NG', 'Active', 0),
(2421, 'Rivers', 'RI', 156, 'NG', 'Active', 0),
(2422, 'Sokoto', 'SO', 156, 'NG', 'Active', 0),
(2423, 'Taraba', 'TA', 156, 'NG', 'Active', 0),
(2424, 'Yobe', 'YO', 156, 'NG', 'Active', 0),
(2425, 'Zamfara', 'ZA', 156, 'NG', 'Active', 0),
(2426, 'Northern Islands', 'N', 159, 'MP', 'Active', 0),
(2427, 'Rota', 'R', 159, 'MP', 'Active', 0),
(2428, 'Saipan', 'S', 159, 'MP', 'Active', 0),
(2429, 'Tinian', 'T', 159, 'MP', 'Active', 0),
(2430, 'Akershus', 'AK', 160, 'NO', 'Active', 0),
(2431, 'Aust-Agder', 'AA', 160, 'NO', 'Active', 0),
(2432, 'Buskerud', 'BU', 160, 'NO', 'Active', 0),
(2433, 'Finnmark', 'FM', 160, 'NO', 'Active', 0),
(2434, 'Hedmark', 'HM', 160, 'NO', 'Active', 0),
(2435, 'Hordaland', 'HL', 160, 'NO', 'Active', 0),
(2436, 'More og Romdal', 'MR', 160, 'NO', 'Active', 0),
(2437, 'Nord-Trondelag', 'NT', 160, 'NO', 'Active', 0),
(2438, 'Nordland', 'NL', 160, 'NO', 'Active', 0),
(2439, 'Ostfold', 'OF', 160, 'NO', 'Active', 0),
(2440, 'Oppland', 'OP', 160, 'NO', 'Active', 0),
(2441, 'Oslo', 'OL', 160, 'NO', 'Active', 0),
(2442, 'Rogaland', 'RL', 160, 'NO', 'Active', 0),
(2443, 'Sor-Trondelag', 'ST', 160, 'NO', 'Active', 0),
(2444, 'Sogn og Fjordane', 'SJ', 160, 'NO', 'Active', 0),
(2445, 'Svalbard', 'SV', 160, 'NO', 'Active', 0),
(2446, 'Telemark', 'TM', 160, 'NO', 'Active', 0),
(2447, 'Troms', 'TR', 160, 'NO', 'Active', 0),
(2448, 'Vest-Agder', 'VA', 160, 'NO', 'Active', 0),
(2449, 'Vestfold', 'VF', 160, 'NO', 'Active', 0),
(2450, 'Ad Dakhiliyah', 'DA', 161, 'OM', 'Active', 0),
(2451, 'Al Batinah', 'BA', 161, 'OM', 'Active', 0),
(2452, 'Al Wusta', 'WU', 161, 'OM', 'Active', 0),
(2453, 'Ash Sharqiyah', 'SH', 161, 'OM', 'Active', 0),
(2454, 'Az Zahirah', 'ZA', 161, 'OM', 'Active', 0),
(2455, 'Masqat', 'MA', 161, 'OM', 'Active', 0),
(2456, 'Musandam', 'MU', 161, 'OM', 'Active', 0),
(2457, 'Zufar', 'ZU', 161, 'OM', 'Active', 0),
(2458, 'Balochistan', 'B', 162, 'PK', 'Active', 0),
(2459, 'Federally Administered Tribal Ar', 'T', 162, 'PK', 'Active', 0),
(2460, 'Islamabad Capital Territory', 'I', 162, 'PK', 'Active', 0),
(2461, 'North-West Frontier', 'N', 162, 'PK', 'Active', 0),
(2462, 'Punjab', 'P', 162, 'PK', 'Active', 0),
(2463, 'Sindh', 'S', 162, 'PK', 'Active', 0),
(2464, 'Aimeliik', 'AM', 163, 'PW', 'Active', 0),
(2465, 'Airai', 'AR', 163, 'PW', 'Active', 0),
(2466, 'Angaur', 'AN', 163, 'PW', 'Active', 0),
(2467, 'Hatohobei', 'HA', 163, 'PW', 'Active', 0),
(2468, 'Kayangel', 'KA', 163, 'PW', 'Active', 0),
(2469, 'Koror', 'KO', 163, 'PW', 'Active', 0),
(2470, 'Melekeok', 'ME', 163, 'PW', 'Active', 0),
(2471, 'Ngaraard', 'NA', 163, 'PW', 'Active', 0),
(2472, 'Ngarchelong', 'NG', 163, 'PW', 'Active', 0),
(2473, 'Ngardmau', 'ND', 163, 'PW', 'Active', 0),
(2474, 'Ngatpang', 'NT', 163, 'PW', 'Active', 0),
(2475, 'Ngchesar', 'NC', 163, 'PW', 'Active', 0),
(2476, 'Ngeremlengui', 'NR', 163, 'PW', 'Active', 0),
(2477, 'Ngiwal', 'NW', 163, 'PW', 'Active', 0),
(2478, 'Peleliu', 'PE', 163, 'PW', 'Active', 0),
(2479, 'Sonsorol', 'SO', 163, 'PW', 'Active', 0),
(2480, 'Bocas del Toro', 'BT', 164, 'PA', 'Active', 0),
(2481, 'Chiriqui', 'CH', 164, 'PA', 'Active', 0),
(2482, 'Cocle', 'CC', 164, 'PA', 'Active', 0),
(2483, 'Colon', 'CL', 164, 'PA', 'Active', 0),
(2484, 'Darien', 'DA', 164, 'PA', 'Active', 0),
(2485, 'Herrera', 'HE', 164, 'PA', 'Active', 0),
(2486, 'Los Santos', 'LS', 164, 'PA', 'Active', 0),
(2487, 'Panama', 'PA', 164, 'PA', 'Active', 0),
(2488, 'San Blas', 'SB', 164, 'PA', 'Active', 0),
(2489, 'Veraguas', 'VG', 164, 'PA', 'Active', 0),
(2490, 'Bougainville', 'BV', 165, 'PG', 'Active', 0),
(2491, 'Central', 'CE', 165, 'PG', 'Active', 0),
(2492, 'Chimbu', 'CH', 165, 'PG', 'Active', 0),
(2493, 'Eastern Highlands', 'EH', 165, 'PG', 'Active', 0),
(2494, 'East New Britain', 'EB', 165, 'PG', 'Active', 0),
(2495, 'East Sepik', 'ES', 165, 'PG', 'Active', 0),
(2496, 'Enga', 'EN', 165, 'PG', 'Active', 0),
(2497, 'Gulf', 'GU', 165, 'PG', 'Active', 0),
(2498, 'Madang', 'MD', 165, 'PG', 'Active', 0),
(2499, 'Manus', 'MN', 165, 'PG', 'Active', 0),
(2500, 'Milne Bay', 'MB', 165, 'PG', 'Active', 0),
(2501, 'Morobe', 'MR', 165, 'PG', 'Active', 0),
(2502, 'National Capital', 'NC', 165, 'PG', 'Active', 0),
(2503, 'New Ireland', 'NI', 165, 'PG', 'Active', 0),
(2504, 'Northern', 'NO', 165, 'PG', 'Active', 0),
(2505, 'Sandaun', 'SA', 165, 'PG', 'Active', 0),
(2506, 'Southern Highlands', 'SH', 165, 'PG', 'Active', 0),
(2507, 'Western', 'WE', 165, 'PG', 'Active', 0),
(2508, 'Western Highlands', 'WH', 165, 'PG', 'Active', 0),
(2509, 'West New Britain', 'WB', 165, 'PG', 'Active', 0),
(2510, 'Alto Paraguay', 'AG', 166, 'PY', 'Active', 0),
(2511, 'Alto Parana', 'AN', 166, 'PY', 'Active', 0),
(2512, 'Amambay', 'AM', 166, 'PY', 'Active', 0),
(2513, 'Asuncion', 'AS', 166, 'PY', 'Active', 0),
(2514, 'Boqueron', 'BO', 166, 'PY', 'Active', 0),
(2515, 'Caaguazu', 'CG', 166, 'PY', 'Active', 0),
(2516, 'Caazapa', 'CZ', 166, 'PY', 'Active', 0),
(2517, 'Canindeyu', 'CN', 166, 'PY', 'Active', 0),
(2518, 'Central', 'CE', 166, 'PY', 'Active', 0),
(2519, 'Concepcion', 'CC', 166, 'PY', 'Active', 0),
(2520, 'Cordillera', 'CD', 166, 'PY', 'Active', 0),
(2521, 'Guaira', 'GU', 166, 'PY', 'Active', 0),
(2522, 'Itapua', 'IT', 166, 'PY', 'Active', 0),
(2523, 'Misiones', 'MI', 166, 'PY', 'Active', 0),
(2524, 'Neembucu', 'NE', 166, 'PY', 'Active', 0),
(2525, 'Paraguari', 'PA', 166, 'PY', 'Active', 0),
(2526, 'Presidente Hayes', 'PH', 166, 'PY', 'Active', 0),
(2527, 'San Pedro', 'SP', 166, 'PY', 'Active', 0),
(2528, 'Amazonas', 'AM', 167, 'PE', 'Active', 0),
(2529, 'Ancash', 'AN', 167, 'PE', 'Active', 0),
(2530, 'Apurimac', 'AP', 167, 'PE', 'Active', 0),
(2531, 'Arequipa', 'AR', 167, 'PE', 'Active', 0),
(2532, 'Ayacucho', 'AY', 167, 'PE', 'Active', 0),
(2533, 'Cajamarca', 'CJ', 167, 'PE', 'Active', 0),
(2534, 'Callao', 'CL', 167, 'PE', 'Active', 0),
(2535, 'Cusco', 'CU', 167, 'PE', 'Active', 0),
(2536, 'Huancavelica', 'HV', 167, 'PE', 'Active', 0),
(2537, 'Huanuco', 'HO', 167, 'PE', 'Active', 0),
(2538, 'Ica', 'IC', 167, 'PE', 'Active', 0),
(2539, 'Junin', 'JU', 167, 'PE', 'Active', 0),
(2540, 'La Libertad', 'LD', 167, 'PE', 'Active', 0),
(2541, 'Lambayeque', 'LY', 167, 'PE', 'Active', 0),
(2542, 'Lima', 'LI', 167, 'PE', 'Active', 0),
(2543, 'Loreto', 'LO', 167, 'PE', 'Active', 0),
(2544, 'Madre de Dios', 'MD', 167, 'PE', 'Active', 0),
(2545, 'Moquegua', 'MO', 167, 'PE', 'Active', 0),
(2546, 'Pasco', 'PA', 167, 'PE', 'Active', 0),
(2547, 'Piura', 'PI', 167, 'PE', 'Active', 0),
(2548, 'Puno', 'PU', 167, 'PE', 'Active', 0),
(2549, 'San Martin', 'SM', 167, 'PE', 'Active', 0),
(2550, 'Tacna', 'TA', 167, 'PE', 'Active', 0),
(2551, 'Tumbes', 'TU', 167, 'PE', 'Active', 0),
(2552, 'Ucayali', 'UC', 167, 'PE', 'Active', 0),
(2553, 'Abra', 'ABR', 168, 'PH', 'Active', 0),
(2554, 'Agusan del Norte', 'ANO', 168, 'PH', 'Active', 0),
(2555, 'Agusan del Sur', 'ASU', 168, 'PH', 'Active', 0),
(2556, 'Aklan', 'AKL', 168, 'PH', 'Active', 0),
(2557, 'Albay', 'ALB', 168, 'PH', 'Active', 0),
(2558, 'Antique', 'ANT', 168, 'PH', 'Active', 0),
(2559, 'Apayao', 'APY', 168, 'PH', 'Active', 0),
(2560, 'Aurora', 'AUR', 168, 'PH', 'Active', 0),
(2561, 'Basilan', 'BAS', 168, 'PH', 'Active', 0),
(2562, 'Bataan', 'BTA', 168, 'PH', 'Active', 0),
(2563, 'Batanes', 'BTE', 168, 'PH', 'Active', 0),
(2564, 'Batangas', 'BTG', 168, 'PH', 'Active', 0),
(2565, 'Biliran', 'BLR', 168, 'PH', 'Active', 0),
(2566, 'Benguet', 'BEN', 168, 'PH', 'Active', 0),
(2567, 'Bohol', 'BOL', 168, 'PH', 'Active', 0),
(2568, 'Bukidnon', 'BUK', 168, 'PH', 'Active', 0),
(2569, 'Bulacan', 'BUL', 168, 'PH', 'Active', 0),
(2570, 'Cagayan', 'CAG', 168, 'PH', 'Active', 0),
(2571, 'Camarines Norte', 'CNO', 168, 'PH', 'Active', 0),
(2572, 'Camarines Sur', 'CSU', 168, 'PH', 'Active', 0),
(2573, 'Camiguin', 'CAM', 168, 'PH', 'Active', 0),
(2574, 'Capiz', 'CAP', 168, 'PH', 'Active', 0),
(2575, 'Catanduanes', 'CAT', 168, 'PH', 'Active', 0),
(2576, 'Cavite', 'CAV', 168, 'PH', 'Active', 0),
(2577, 'Cebu', 'CEB', 168, 'PH', 'Active', 0),
(2578, 'Compostela', 'CMP', 168, 'PH', 'Active', 0),
(2579, 'Davao del Norte', 'DNO', 168, 'PH', 'Active', 0),
(2580, 'Davao del Sur', 'DSU', 168, 'PH', 'Active', 0),
(2581, 'Davao Oriental', 'DOR', 168, 'PH', 'Active', 0),
(2582, 'Eastern Samar', 'ESA', 168, 'PH', 'Active', 0),
(2583, 'Guimaras', 'GUI', 168, 'PH', 'Active', 0),
(2584, 'Ifugao', 'IFU', 168, 'PH', 'Active', 0),
(2585, 'Ilocos Norte', 'INO', 168, 'PH', 'Active', 0),
(2586, 'Ilocos Sur', 'ISU', 168, 'PH', 'Active', 0),
(2587, 'Iloilo', 'ILO', 168, 'PH', 'Active', 0),
(2588, 'Isabela', 'ISA', 168, 'PH', 'Active', 0),
(2589, 'Kalinga', 'KAL', 168, 'PH', 'Active', 0),
(2590, 'Laguna', 'LAG', 168, 'PH', 'Active', 0),
(2591, 'Lanao del Norte', 'LNO', 168, 'PH', 'Active', 0),
(2592, 'Lanao del Sur', 'LSU', 168, 'PH', 'Active', 0),
(2593, 'La Union', 'UNI', 168, 'PH', 'Active', 0),
(2594, 'Leyte', 'LEY', 168, 'PH', 'Active', 0),
(2595, 'Maguindanao', 'MAG', 168, 'PH', 'Active', 0),
(2596, 'Marinduque', 'MRN', 168, 'PH', 'Active', 0),
(2597, 'Masbate', 'MSB', 168, 'PH', 'Active', 0),
(2598, 'Mindoro Occidental', 'MIC', 168, 'PH', 'Active', 0),
(2599, 'Mindoro Oriental', 'MIR', 168, 'PH', 'Active', 0),
(2600, 'Misamis Occidental', 'MSC', 168, 'PH', 'Active', 0),
(2601, 'Misamis Oriental', 'MOR', 168, 'PH', 'Active', 0),
(2602, 'Mountain', 'MOP', 168, 'PH', 'Active', 0),
(2603, 'Negros Occidental', 'NOC', 168, 'PH', 'Active', 0),
(2604, 'Negros Oriental', 'NOR', 168, 'PH', 'Active', 0),
(2605, 'North Cotabato', 'NCT', 168, 'PH', 'Active', 0),
(2606, 'Northern Samar', 'NSM', 168, 'PH', 'Active', 0),
(2607, 'Nueva Ecija', 'NEC', 168, 'PH', 'Active', 0),
(2608, 'Nueva Vizcaya', 'NVZ', 168, 'PH', 'Active', 0),
(2609, 'Palawan', 'PLW', 168, 'PH', 'Active', 0),
(2610, 'Pampanga', 'PMP', 168, 'PH', 'Active', 0),
(2611, 'Pangasinan', 'PNG', 168, 'PH', 'Active', 0),
(2612, 'Quezon', 'QZN', 168, 'PH', 'Active', 0),
(2613, 'Quirino', 'QRN', 168, 'PH', 'Active', 0),
(2614, 'Rizal', 'RIZ', 168, 'PH', 'Active', 0),
(2615, 'Romblon', 'ROM', 168, 'PH', 'Active', 0),
(2616, 'Samar', 'SMR', 168, 'PH', 'Active', 0),
(2617, 'Sarangani', 'SRG', 168, 'PH', 'Active', 0),
(2618, 'Siquijor', 'SQJ', 168, 'PH', 'Active', 0),
(2619, 'Sorsogon', 'SRS', 168, 'PH', 'Active', 0),
(2620, 'South Cotabato', 'SCO', 168, 'PH', 'Active', 0),
(2621, 'Southern Leyte', 'SLE', 168, 'PH', 'Active', 0),
(2622, 'Sultan Kudarat', 'SKU', 168, 'PH', 'Active', 0),
(2623, 'Sulu', 'SLU', 168, 'PH', 'Active', 0),
(2624, 'Surigao del Norte', 'SNO', 168, 'PH', 'Active', 0),
(2625, 'Surigao del Sur', 'SSU', 168, 'PH', 'Active', 0),
(2626, 'Tarlac', 'TAR', 168, 'PH', 'Active', 0),
(2627, 'Tawi-Tawi', 'TAW', 168, 'PH', 'Active', 0),
(2628, 'Zambales', 'ZBL', 168, 'PH', 'Active', 0),
(2629, 'Zamboanga del Norte', 'ZNO', 168, 'PH', 'Active', 0),
(2630, 'Zamboanga del Sur', 'ZSU', 168, 'PH', 'Active', 0),
(2631, 'Zamboanga Sibugay', 'ZSI', 168, 'PH', 'Active', 0),
(2632, 'Dolnoslaskie', 'DO', 170, 'PL', 'Active', 0),
(2633, 'Kujawsko-Pomorskie', 'KP', 170, 'PL', 'Active', 0),
(2634, 'Lodzkie', 'LO', 170, 'PL', 'Active', 0),
(2635, 'Lubelskie', 'LL', 170, 'PL', 'Active', 0),
(2636, 'Lubuskie', 'LU', 170, 'PL', 'Active', 0),
(2637, 'Malopolskie', 'ML', 170, 'PL', 'Active', 0),
(2638, 'Mazowieckie', 'MZ', 170, 'PL', 'Active', 0),
(2639, 'Opolskie', 'OP', 170, 'PL', 'Active', 0),
(2640, 'Podkarpackie', 'PP', 170, 'PL', 'Active', 0),
(2641, 'Podlaskie', 'PL', 170, 'PL', 'Active', 0),
(2642, 'Pomorskie', 'PM', 170, 'PL', 'Active', 0),
(2643, 'Slaskie', 'SL', 170, 'PL', 'Active', 0),
(2644, 'Swietokrzyskie', 'SW', 170, 'PL', 'Active', 0),
(2645, 'Warminsko-Mazurskie', 'WM', 170, 'PL', 'Active', 0),
(2646, 'Wielkopolskie', 'WP', 170, 'PL', 'Active', 0),
(2647, 'Zachodniopomorskie', 'ZA', 170, 'PL', 'Active', 0),
(2648, 'Saint Pierre', 'P', 198, 'PM', 'Active', 0),
(2649, 'Miquelon', 'M', 198, 'PM', 'Active', 0),
(2651, 'Aveiro', 'AV', 171, 'PT', 'Active', 0),
(2652, 'Beja', 'BE', 171, 'PT', 'Active', 0),
(2653, 'Braga', 'BR', 171, 'PT', 'Active', 0),
(2654, 'Bragan&ccedil;a', 'BA', 171, 'PT', 'Active', 0),
(2655, 'Castelo Branco', 'CB', 171, 'PT', 'Active', 0),
(2656, 'Coimbra', 'CO', 171, 'PT', 'Active', 0),
(2659, 'Guarda', 'GU', 171, 'PT', 'Active', 0),
(2660, 'Leiria', 'LE', 171, 'PT', 'Active', 0),
(2661, 'Lisboa', 'LI', 171, 'PT', 'Active', 0),
(2662, 'Madeira', 'ME', 171, 'PT', 'Active', 0),
(2663, 'Portalegre', 'PO', 171, 'PT', 'Active', 0),
(2664, 'Porto', 'PR', 171, 'PT', 'Active', 0),
(2665, 'Santar&eacute;m', 'SA', 171, 'PT', 'Active', 0),
(2666, 'Set&uacute;bal', 'SE', 171, 'PT', 'Active', 0),
(2667, 'Viana do Castelo', 'VC', 171, 'PT', 'Active', 0),
(2668, 'Vila Real', 'VR', 171, 'PT', 'Active', 0),
(2669, 'Viseu', 'VI', 171, 'PT', 'Active', 0),
(2670, 'Ad Dawhah', 'DW', 173, 'QA', 'Active', 0),
(2671, 'Al Ghuwayriyah', 'GW', 173, 'QA', 'Active', 0),
(2672, 'Al Jumayliyah', 'JM', 173, 'QA', 'Active', 0),
(2673, 'Al Khawr', 'KR', 173, 'QA', 'Active', 0),
(2674, 'Al Wakrah', 'WK', 173, 'QA', 'Active', 0),
(2675, 'Ar Rayyan', 'RN', 173, 'QA', 'Active', 0),
(2676, 'Jarayan al Batinah', 'JB', 173, 'QA', 'Active', 0),
(2677, 'Madinat ash Shamal', 'MS', 173, 'QA', 'Active', 0),
(2678, 'Umm Sa\'id', 'UD', 173, 'QA', 'Active', 0),
(2679, 'Umm Salal', 'UL', 173, 'QA', 'Active', 0),
(2680, 'Alba', 'AB', 175, 'RO', 'Active', 0),
(2681, 'Arad', 'AR', 175, 'RO', 'Active', 0),
(2682, 'Arges', 'AG', 175, 'RO', 'Active', 0),
(2683, 'Bacau', 'BC', 175, 'RO', 'Active', 0),
(2684, 'Bihor', 'BH', 175, 'RO', 'Active', 0),
(2685, 'Bistrita-Nasaud', 'BN', 175, 'RO', 'Active', 0),
(2686, 'Botosani', 'BT', 175, 'RO', 'Active', 0),
(2687, 'Brasov', 'BV', 175, 'RO', 'Active', 0),
(2688, 'Braila', 'BR', 175, 'RO', 'Active', 0),
(2689, 'Bucuresti', 'B', 175, 'RO', 'Active', 0),
(2690, 'Buzau', 'BZ', 175, 'RO', 'Active', 0),
(2691, 'Caras-Severin', 'CS', 175, 'RO', 'Active', 0),
(2692, 'Calarasi', 'CL', 175, 'RO', 'Active', 0),
(2693, 'Cluj', 'CJ', 175, 'RO', 'Active', 0),
(2694, 'Constanta', 'CT', 175, 'RO', 'Active', 0),
(2695, 'Covasna', 'CV', 175, 'RO', 'Active', 0),
(2696, 'Dimbovita', 'DB', 175, 'RO', 'Active', 0),
(2697, 'Dolj', 'DJ', 175, 'RO', 'Active', 0),
(2698, 'Galati', 'GL', 175, 'RO', 'Active', 0),
(2699, 'Giurgiu', 'GR', 175, 'RO', 'Active', 0),
(2700, 'Gorj', 'GJ', 175, 'RO', 'Active', 0),
(2701, 'Harghita', 'HR', 175, 'RO', 'Active', 0),
(2702, 'Hunedoara', 'HD', 175, 'RO', 'Active', 0),
(2703, 'Ialomita', 'IL', 175, 'RO', 'Active', 0),
(2704, 'Iasi', 'IS', 175, 'RO', 'Active', 0),
(2705, 'Ilfov', 'IF', 175, 'RO', 'Active', 0),
(2706, 'Maramures', 'MM', 175, 'RO', 'Active', 0),
(2707, 'Mehedinti', 'MH', 175, 'RO', 'Active', 0),
(2708, 'Mures', 'MS', 175, 'RO', 'Active', 0),
(2709, 'Neamt', 'NT', 175, 'RO', 'Active', 0),
(2710, 'Olt', 'OT', 175, 'RO', 'Active', 0),
(2711, 'Prahova', 'PH', 175, 'RO', 'Active', 0),
(2712, 'Satu-Mare', 'SM', 175, 'RO', 'Active', 0),
(2713, 'Salaj', 'SJ', 175, 'RO', 'Active', 0),
(2714, 'Sibiu', 'SB', 175, 'RO', 'Active', 0),
(2715, 'Suceava', 'SV', 175, 'RO', 'Active', 0),
(2716, 'Teleorman', 'TR', 175, 'RO', 'Active', 0),
(2717, 'Timis', 'TM', 175, 'RO', 'Active', 0),
(2718, 'Tulcea', 'TL', 175, 'RO', 'Active', 0),
(2719, 'Vaslui', 'VS', 175, 'RO', 'Active', 0),
(2720, 'Valcea', 'VL', 175, 'RO', 'Active', 0),
(2721, 'Vrancea', 'VN', 175, 'RO', 'Active', 0),
(2722, 'Abakan', 'AB', 223, 'US', 'Active', 0),
(2723, 'Aginskoye', 'AG', 176, 'RU', 'Active', 0),
(2724, 'Anadyr', 'AN', 176, 'RU', 'Active', 0),
(2725, 'Arkahangelsk', 'AR', 176, 'RU', 'Active', 0),
(2726, 'Astrakhan', 'AS', 176, 'RU', 'Active', 0),
(2727, 'Barnaul', 'BA', 176, 'RU', 'Active', 0),
(2728, 'Belgorod', 'BE', 176, 'RU', 'Active', 0),
(2729, 'Birobidzhan', 'BI', 176, 'RU', 'Active', 0),
(2730, 'Blagoveshchensk', 'BL', 176, 'RU', 'Active', 0),
(2731, 'Bryansk', 'BR', 176, 'RU', 'Active', 0),
(2732, 'Cheboksary', 'CH', 176, 'RU', 'Active', 0),
(2733, 'Chelyabinsk', 'CL', 176, 'RU', 'Active', 0),
(2734, 'Cherkessk', 'CR', 176, 'RU', 'Active', 0),
(2735, 'Chita', 'CI', 176, 'RU', 'Active', 0),
(2736, 'Dudinka', 'DU', 176, 'RU', 'Active', 0),
(2737, 'Elista', 'EL', 176, 'RU', 'Active', 0),
(2738, 'Gomo-Altaysk', 'GO', 176, 'RU', 'Active', 0),
(2739, 'Gorno-Altaysk', 'GA', 176, 'RU', 'Active', 0),
(2740, 'Groznyy', 'GR', 176, 'RU', 'Active', 0),
(2741, 'Irkutsk', 'IR', 176, 'RU', 'Active', 0),
(2742, 'Ivanovo', 'IV', 176, 'RU', 'Active', 0),
(2743, 'Izhevsk', 'IZ', 176, 'RU', 'Active', 0),
(2744, 'Kalinigrad', 'KA', 176, 'RU', 'Active', 0),
(2745, 'Kaluga', 'KL', 176, 'RU', 'Active', 0),
(2746, 'Kasnodar', 'KS', 176, 'RU', 'Active', 0),
(2747, 'Kazan', 'KZ', 176, 'RU', 'Active', 0),
(2748, 'Kemerovo', 'KE', 176, 'RU', 'Active', 0),
(2749, 'Khabarovsk', 'KH', 176, 'RU', 'Active', 0),
(2750, 'Khanty-Mansiysk', 'KM', 176, 'RU', 'Active', 0),
(2751, 'Kostroma', 'KO', 176, 'RU', 'Active', 0),
(2752, 'Krasnodar', 'KR', 176, 'RU', 'Active', 0),
(2753, 'Krasnoyarsk', 'KN', 176, 'RU', 'Active', 0),
(2754, 'Kudymkar', 'KU', 176, 'RU', 'Active', 0),
(2755, 'Kurgan', 'KG', 176, 'RU', 'Active', 0),
(2756, 'Kursk', 'KK', 176, 'RU', 'Active', 0),
(2757, 'Kyzyl', 'KY', 176, 'RU', 'Active', 0),
(2758, 'Lipetsk', 'LI', 176, 'RU', 'Active', 0),
(2759, 'Magadan', 'MA', 176, 'RU', 'Active', 0),
(2760, 'Makhachkala', 'MK', 176, 'RU', 'Active', 0),
(2761, 'Maykop', 'MY', 176, 'RU', 'Active', 0),
(2762, 'Moscow', 'MO', 176, 'RU', 'Active', 0),
(2763, 'Murmansk', 'MU', 176, 'RU', 'Active', 0),
(2764, 'Nalchik', 'NA', 176, 'RU', 'Active', 0),
(2765, 'Naryan Mar', 'NR', 176, 'RU', 'Active', 0),
(2766, 'Nazran', 'NZ', 176, 'RU', 'Active', 0),
(2767, 'Nizhniy Novgorod', 'NI', 176, 'RU', 'Active', 0),
(2768, 'Novgorod', 'NO', 176, 'RU', 'Active', 0),
(2769, 'Novosibirsk', 'NV', 176, 'RU', 'Active', 0),
(2770, 'Omsk', 'OM', 176, 'RU', 'Active', 0),
(2771, 'Orel', 'OR', 176, 'RU', 'Active', 0),
(2772, 'Orenburg', 'OE', 176, 'RU', 'Active', 0),
(2773, 'Palana', 'PA', 176, 'RU', 'Active', 0),
(2774, 'Penza', 'PE', 176, 'RU', 'Active', 0),
(2775, 'Perm', 'PR', 176, 'RU', 'Active', 0),
(2776, 'Petropavlovsk-Kamchatskiy', 'PK', 176, 'RU', 'Active', 0),
(2777, 'Petrozavodsk', 'PT', 176, 'RU', 'Active', 0),
(2778, 'Pskov', 'PS', 176, 'RU', 'Active', 0),
(2779, 'Rostov-na-Donu', 'RO', 176, 'RU', 'Active', 0),
(2780, 'Ryazan', 'RY', 176, 'RU', 'Active', 0),
(2781, 'Salekhard', 'SL', 176, 'RU', 'Active', 0),
(2782, 'Samara', 'SA', 176, 'RU', 'Active', 0),
(2783, 'Saransk', 'SR', 176, 'RU', 'Active', 0),
(2784, 'Saratov', 'SV', 176, 'RU', 'Active', 0),
(2785, 'Smolensk', 'SM', 176, 'RU', 'Active', 0),
(2786, 'St. Petersburg', 'SP', 176, 'RU', 'Active', 0),
(2787, 'Stavropol', 'ST', 176, 'RU', 'Active', 0),
(2788, 'Syktyvkar', 'SY', 176, 'RU', 'Active', 0),
(2789, 'Tambov', 'TA', 176, 'RU', 'Active', 0),
(2790, 'Tomsk', 'TO', 176, 'RU', 'Active', 0),
(2791, 'Tula', 'TU', 176, 'RU', 'Active', 0),
(2792, 'Tura', 'TR', 176, 'RU', 'Active', 0),
(2793, 'Tver', 'TV', 176, 'RU', 'Active', 0),
(2794, 'Tyumen', 'TY', 176, 'RU', 'Active', 0),
(2795, 'Ufa', 'UF', 176, 'RU', 'Active', 0),
(2796, 'Ul\'yanovsk', 'UL', 176, 'RU', 'Active', 0),
(2797, 'Ulan-Ude', 'UU', 176, 'RU', 'Active', 0),
(2798, 'Ust\'-Ordynskiy', 'US', 176, 'RU', 'Active', 0),
(2799, 'Vladikavkaz', 'VL', 176, 'RU', 'Active', 0),
(2800, 'Vladimir', 'VA', 176, 'RU', 'Active', 0),
(2801, 'Vladivostok', 'VV', 176, 'RU', 'Active', 0),
(2802, 'Volgograd', 'VG', 176, 'RU', 'Active', 0),
(2803, 'Vologda', 'VD', 176, 'RU', 'Active', 0),
(2804, 'Voronezh', 'VO', 176, 'RU', 'Active', 0),
(2805, 'Vyatka', 'VY', 176, 'RU', 'Active', 0),
(2806, 'Yakutsk', 'YA', 176, 'RU', 'Active', 0),
(2807, 'Yaroslavl', 'YR', 176, 'RU', 'Active', 0),
(2808, 'Yekaterinburg', 'YE', 176, 'RU', 'Active', 0),
(2809, 'Yoshkar-Ola', 'YO', 176, 'RU', 'Active', 0),
(2810, 'Butare', 'BU', 177, 'RW', 'Active', 0),
(2811, 'Byumba', 'BY', 177, 'RW', 'Active', 0),
(2812, 'Cyangugu', 'CY', 177, 'RW', 'Active', 0),
(2813, 'Gikongoro', 'GK', 177, 'RW', 'Active', 0),
(2814, 'Gisenyi', 'GS', 177, 'RW', 'Active', 0),
(2815, 'Gitarama', 'GT', 177, 'RW', 'Active', 0),
(2816, 'Kibungo', 'KG', 177, 'RW', 'Active', 0),
(2817, 'Kibuye', 'KY', 177, 'RW', 'Active', 0),
(2818, 'Kigali Rurale', 'KR', 177, 'RW', 'Active', 0),
(2819, 'Kigali-ville', 'KV', 177, 'RW', 'Active', 0),
(2820, 'Ruhengeri', 'RU', 177, 'RW', 'Active', 0),
(2821, 'Umutara', 'UM', 177, 'RW', 'Active', 0),
(2822, 'Christ Church Nichola Town', 'CCN', 178, 'KN', 'Active', 0),
(2823, 'Saint Anne Sandy Point', 'SAS', 178, 'KN', 'Active', 0),
(2824, 'Saint George Basseterre', 'SGB', 178, 'KN', 'Active', 0),
(2825, 'Saint George Gingerland', 'SGG', 178, 'KN', 'Active', 0),
(2826, 'Saint James Windward', 'SJW', 178, 'KN', 'Active', 0),
(2827, 'Saint John Capesterre', 'SJC', 178, 'KN', 'Active', 0),
(2828, 'Saint John Figtree', 'SJF', 178, 'KN', 'Active', 0),
(2829, 'Saint Mary Cayon', 'SMC', 178, 'KN', 'Active', 0),
(2830, 'Saint Paul Capesterre', 'CAP', 178, 'KN', 'Active', 0),
(2831, 'Saint Paul Charlestown', 'CHA', 178, 'KN', 'Active', 0),
(2832, 'Saint Peter Basseterre', 'SPB', 178, 'KN', 'Active', 0),
(2833, 'Saint Thomas Lowland', 'STL', 178, 'KN', 'Active', 0),
(2834, 'Saint Thomas Middle Island', 'STM', 178, 'KN', 'Active', 0),
(2835, 'Trinity Palmetto Point', 'TPP', 178, 'KN', 'Active', 0),
(2836, 'Anse-la-Raye', 'AR', 179, 'LC', 'Active', 0),
(2837, 'Castries', 'CA', 179, 'LC', 'Active', 0),
(2838, 'Choiseul', 'CH', 179, 'LC', 'Active', 0),
(2839, 'Dauphin', 'DA', 179, 'LC', 'Active', 0),
(2840, 'Dennery', 'DE', 179, 'LC', 'Active', 0),
(2841, 'Gros-Islet', 'GI', 179, 'LC', 'Active', 0),
(2842, 'Laborie', 'LA', 179, 'LC', 'Active', 0),
(2843, 'Micoud', 'MI', 179, 'LC', 'Active', 0),
(2844, 'Praslin', 'PR', 179, 'LC', 'Active', 0),
(2845, 'Soufriere', 'SO', 179, 'LC', 'Active', 0),
(2846, 'Vieux-Fort', 'VF', 179, 'LC', 'Active', 0),
(2847, 'Charlotte', 'C', 180, 'VC', 'Active', 0),
(2848, 'Grenadines', 'R', 180, 'VC', 'Active', 0),
(2849, 'Saint Andrew', 'A', 180, 'VC', 'Active', 0),
(2850, 'Saint David', 'D', 180, 'VC', 'Active', 0),
(2851, 'Saint George', 'G', 180, 'VC', 'Active', 0),
(2852, 'Saint Patrick', 'P', 180, 'VC', 'Active', 0),
(2854, 'Aiga-i-le-Tai', 'AI', 181, 'WS', 'Active', 0),
(2855, 'Atua', 'AT', 181, 'WS', 'Active', 0),
(2856, 'Fa\'asaleleaga', 'FA', 181, 'WS', 'Active', 0),
(2857, 'Gaga\'emauga', 'GE', 181, 'WS', 'Active', 0),
(2858, 'Gagaifomauga', 'GF', 181, 'WS', 'Active', 0),
(2859, 'Palauli', 'PA', 181, 'WS', 'Active', 0),
(2860, 'Satupa\'itea', 'SA', 181, 'WS', 'Active', 0),
(2861, 'Tuamasaga', 'TU', 181, 'WS', 'Active', 0),
(2862, 'Va\'a-o-Fonoti', 'VF', 181, 'WS', 'Active', 0),
(2863, 'Vaisigano', 'VS', 181, 'WS', 'Active', 0),
(2864, 'Acquaviva', 'AC', 182, 'SM', 'Active', 0),
(2865, 'Borgo Maggiore', 'BM', 182, 'SM', 'Active', 0),
(2866, 'Chiesanuova', 'CH', 182, 'SM', 'Active', 0),
(2867, 'Domagnano', 'DO', 182, 'SM', 'Active', 0),
(2868, 'Faetano', 'FA', 182, 'SM', 'Active', 0),
(2869, 'Fiorentino', 'FI', 182, 'SM', 'Active', 0),
(2870, 'Montegiardino', 'MO', 182, 'SM', 'Active', 0),
(2871, 'Citta di San Marino', 'SM', 182, 'SM', 'Active', 0),
(2872, 'Serravalle', 'SE', 182, 'SM', 'Active', 0),
(2873, 'Sao Tome', 'S', 183, 'ST', 'Active', 0),
(2874, 'Principe', 'P', 183, 'ST', 'Active', 0),
(2875, 'Al Bahah', 'BH', 184, 'SA', 'Active', 0),
(2876, 'Al Hudud ash Shamaliyah', 'HS', 184, 'SA', 'Active', 0),
(2877, 'Al Jawf', 'JF', 184, 'SA', 'Active', 0),
(2878, 'Al Madinah', 'MD', 184, 'SA', 'Active', 0),
(2879, 'Al Qasim', 'QS', 184, 'SA', 'Active', 0),
(2880, 'Ar Riyad', 'RD', 184, 'SA', 'Active', 0),
(2881, 'Ash Sharqiyah (Eastern)', 'AQ', 184, 'SA', 'Active', 0),
(2886, 'Najran', 'NR', 184, 'SA', 'Active', 0),
(2887, 'Tabuk', 'TB', 184, 'SA', 'Active', 0),
(2888, 'Dakar', 'DA', 185, 'SN', 'Active', 0),
(2889, 'Diourbel', 'DI', 185, 'SN', 'Active', 0),
(2890, 'Fatick', 'FA', 185, 'SN', 'Active', 0),
(2891, 'Kaolack', 'KA', 185, 'SN', 'Active', 0),
(2892, 'Kolda', 'KO', 185, 'SN', 'Active', 0),
(2893, 'Louga', 'LO', 185, 'SN', 'Active', 0),
(2894, 'Matam', 'MA', 185, 'SN', 'Active', 0),
(2895, 'Saint-Louis', 'SL', 185, 'SN', 'Active', 0),
(2896, 'Tambacounda', 'TA', 185, 'SN', 'Active', 0),
(2897, 'Thies', 'TH', 185, 'SN', 'Active', 0),
(2898, 'Ziguinchor', 'ZI', 185, 'SN', 'Active', 0),
(2899, 'Anse aux Pins', 'AP', 186, 'SC', 'Active', 0),
(2900, 'Anse Boileau', 'AB', 186, 'SC', 'Active', 0),
(2901, 'Anse Etoile', 'AE', 186, 'SC', 'Active', 0),
(2902, 'Anse Louis', 'AL', 186, 'SC', 'Active', 0),
(2903, 'Anse Royale', 'AR', 186, 'SC', 'Active', 0),
(2904, 'Baie Lazare', 'BL', 186, 'SC', 'Active', 0),
(2905, 'Baie Sainte Anne', 'BS', 186, 'SC', 'Active', 0),
(2906, 'Beau Vallon', 'BV', 186, 'SC', 'Active', 0),
(2907, 'Bel Air', 'BA', 186, 'SC', 'Active', 0),
(2908, 'Bel Ombre', 'BO', 186, 'SC', 'Active', 0),
(2909, 'Cascade', 'CA', 186, 'SC', 'Active', 0),
(2910, 'Glacis', 'GL', 186, 'SC', 'Active', 0),
(2911, 'Grand\' Anse (on Mahe)', 'GM', 186, 'SC', 'Active', 0),
(2912, 'Grand\' Anse (on Praslin)', 'GP', 186, 'SC', 'Active', 0),
(2913, 'La Digue', 'DG', 186, 'SC', 'Active', 0),
(2914, 'La Riviere Anglaise', 'RA', 186, 'SC', 'Active', 0),
(2915, 'Mont Buxton', 'MB', 186, 'SC', 'Active', 0),
(2916, 'Mont Fleuri', 'MF', 186, 'SC', 'Active', 0),
(2917, 'Plaisance', 'PL', 186, 'SC', 'Active', 0),
(2918, 'Pointe La Rue', 'PR', 186, 'SC', 'Active', 0),
(2919, 'Port Glaud', 'PG', 186, 'SC', 'Active', 0),
(2920, 'Saint Louis', 'SL', 186, 'SC', 'Active', 0),
(2921, 'Takamaka', 'TA', 186, 'SC', 'Active', 0),
(2922, 'Eastern', 'E', 187, 'SL', 'Active', 0),
(2923, 'Northern', 'N', 187, 'SL', 'Active', 0),
(2924, 'Southern', 'S', 187, 'SL', 'Active', 0),
(2925, 'Western', 'W', 187, 'SL', 'Active', 0),
(2926, 'Banskobystricky', 'BA', 189, 'SK', 'Active', 0),
(2927, 'Bratislavsky', 'BR', 189, 'SK', 'Active', 0),
(2928, 'Kosicky', 'KO', 189, 'SK', 'Active', 0),
(2929, 'Nitriansky', 'NI', 189, 'SK', 'Active', 0),
(2930, 'Presovsky', 'PR', 189, 'SK', 'Active', 0),
(2931, 'Trenciansky', 'TC', 189, 'SK', 'Active', 0),
(2932, 'Trnavsky', 'TV', 189, 'SK', 'Active', 0),
(2933, 'Zilinsky', 'ZI', 189, 'SK', 'Active', 0),
(2934, 'Central', 'CE', 191, 'SB', 'Active', 0),
(2935, 'Choiseul', 'CH', 191, 'SB', 'Active', 0),
(2936, 'Guadalcanal', 'GC', 191, 'SB', 'Active', 0),
(2937, 'Honiara', 'HO', 191, 'SB', 'Active', 0),
(2938, 'Isabel', 'IS', 191, 'SB', 'Active', 0),
(2939, 'Makira', 'MK', 191, 'SB', 'Active', 0),
(2940, 'Malaita', 'ML', 191, 'SB', 'Active', 0),
(2941, 'Rennell and Bellona', 'RB', 191, 'SB', 'Active', 0),
(2942, 'Temotu', 'TM', 191, 'SB', 'Active', 0),
(2943, 'Western', 'WE', 191, 'SB', 'Active', 0),
(2944, 'Awdal', 'AW', 192, 'SO', 'Active', 0),
(2945, 'Bakool', 'BK', 192, 'SO', 'Active', 0),
(2946, 'Banaadir', 'BN', 192, 'SO', 'Active', 0),
(2947, 'Bari', 'BR', 192, 'SO', 'Active', 0),
(2948, 'Bay', 'BY', 192, 'SO', 'Active', 0),
(2949, 'Galguduud', 'GA', 192, 'SO', 'Active', 0),
(2950, 'Gedo', 'GE', 192, 'SO', 'Active', 0),
(2951, 'Hiiraan', 'HI', 192, 'SO', 'Active', 0),
(2952, 'Jubbada Dhexe', 'JD', 192, 'SO', 'Active', 0),
(2953, 'Jubbada Hoose', 'JH', 192, 'SO', 'Active', 0),
(2954, 'Mudug', 'MU', 192, 'SO', 'Active', 0),
(2955, 'Nugaal', 'NU', 192, 'SO', 'Active', 0),
(2956, 'Sanaag', 'SA', 192, 'SO', 'Active', 0),
(2957, 'Shabeellaha Dhexe', 'SD', 192, 'SO', 'Active', 0),
(2958, 'Shabeellaha Hoose', 'SH', 192, 'SO', 'Active', 0),
(2959, 'Sool', 'SL', 192, 'SO', 'Active', 0),
(2960, 'Togdheer', 'TO', 192, 'SO', 'Active', 0),
(2961, 'Woqooyi Galbeed', 'WG', 192, 'SO', 'Active', 0),
(2962, 'Eastern Cape', 'EC', 193, 'ZA', 'Active', 0),
(2963, 'Free State', 'FS', 193, 'ZA', 'Active', 0),
(2964, 'Gauteng', 'GT', 193, 'ZA', 'Active', 0),
(2965, 'KwaZulu-Natal', 'KN', 193, 'ZA', 'Active', 0),
(2966, 'Limpopo', 'LP', 193, 'ZA', 'Active', 0),
(2967, 'Mpumalanga', 'MP', 193, 'ZA', 'Active', 0),
(2968, 'North West', 'NW', 193, 'ZA', 'Active', 0),
(2969, 'Northern Cape', 'NC', 193, 'ZA', 'Active', 0),
(2970, 'Western Cape', 'WC', 193, 'ZA', 'Active', 0),
(2971, 'ACoruna', 'CA', 195, 'ES', 'Active', 0),
(2973, 'Albacete', 'AB', 195, 'ES', 'Active', 0),
(2974, 'Alicante', 'AC', 195, 'ES', 'Active', 0),
(2975, 'Almeria', 'AM', 195, 'ES', 'Active', 0),
(2976, 'Asturias', 'AS', 195, 'ES', 'Active', 0),
(2978, 'Badajoz', 'BJ', 195, 'ES', 'Active', 0),
(2979, 'Baleares', 'IB', 195, 'ES', 'Active', 0),
(2980, 'Barcelona', 'BA', 195, 'ES', 'Active', 0),
(2981, 'Burgos', 'BU', 195, 'ES', 'Active', 0),
(2982, 'C&aacute;ceres', 'CC', 195, 'ES', 'Active', 0),
(2983, 'C&aacute;diz', 'CZ', 195, 'ES', 'Active', 0),
(2984, 'Cantabria', 'CT', 195, 'ES', 'Active', 0),
(2985, 'Castell&oacute;n', 'CL', 195, 'ES', 'Active', 0),
(2986, 'Ceuta', 'CE', 195, 'ES', 'Active', 0),
(2987, 'Ciudad Real', 'CR', 195, 'ES', 'Active', 0),
(2988, 'C&oacute;rdoba', 'CD', 195, 'ES', 'Active', 0),
(2989, 'Cuenca', 'CU', 195, 'ES', 'Active', 0),
(2990, 'Girona', 'GI', 195, 'ES', 'Active', 0),
(2991, 'Granada', 'GD', 195, 'ES', 'Active', 0),
(2992, 'Guadalajara', 'GJ', 195, 'ES', 'Active', 0),
(2993, 'Guip&uacute;zcoa', 'GP', 195, 'ES', 'Active', 0),
(2994, 'Huelva', 'HL', 195, 'ES', 'Active', 0),
(2995, 'Huesca', 'HS', 195, 'ES', 'Active', 0),
(2996, 'Ja&eacute;n', 'JN', 195, 'ES', 'Active', 0),
(2997, 'La Rioja', 'RJ', 195, 'ES', 'Active', 0),
(2998, 'Las Palmas', 'PM', 195, 'ES', 'Active', 0),
(2999, 'Leon', 'LE', 195, 'ES', 'Active', 0),
(3000, 'Lleida', 'LL', 195, 'ES', 'Active', 0),
(3001, 'Lugo', 'LG', 195, 'ES', 'Active', 0),
(3002, 'Madrid', 'MD', 195, 'ES', 'Active', 0),
(3003, 'Malaga', 'MA', 195, 'ES', 'Active', 0),
(3004, 'Melilla', 'ML', 195, 'ES', 'Active', 0),
(3005, 'Murcia', 'MU', 195, 'ES', 'Active', 0),
(3006, 'Navarra', 'NV', 195, 'ES', 'Active', 0),
(3007, 'Ourense', 'OU', 195, 'ES', 'Active', 0),
(3008, 'Palencia', 'PL', 195, 'ES', 'Active', 0),
(3009, 'Pontevedra', 'PO', 195, 'ES', 'Active', 0),
(3010, 'Salamanca', 'SL', 195, 'ES', 'Active', 0),
(3011, 'Santa Cruz de Tenerife', 'SC', 195, 'ES', 'Active', 0),
(3012, 'Segovia', 'SG', 195, 'ES', 'Active', 0),
(3013, 'Sevilla', 'SV', 195, 'ES', 'Active', 0),
(3014, 'Soria', 'SO', 195, 'ES', 'Active', 0),
(3015, 'Tarragona', 'TA', 195, 'ES', 'Active', 0),
(3016, 'Teruel', 'TE', 195, 'ES', 'Active', 0),
(3017, 'Toledo', 'TO', 195, 'ES', 'Active', 0),
(3018, 'Valencia', 'VC', 195, 'ES', 'Active', 0),
(3019, 'Valladolid', 'VD', 195, 'ES', 'Active', 0),
(3020, 'Vizcaya', 'VZ', 195, 'ES', 'Active', 0),
(3021, 'Zamora', 'ZM', 195, 'ES', 'Active', 0),
(3022, 'Zaragoza', 'ZR', 195, 'ES', 'Active', 0),
(3023, 'Central', 'CE', 196, 'LK', 'Active', 0),
(3024, 'Eastern', 'EA', 196, 'LK', 'Active', 0),
(3025, 'North Central', 'NC', 196, 'LK', 'Active', 0),
(3026, 'Northern', 'NO', 196, 'LK', 'Active', 0),
(3027, 'North Western', 'NW', 196, 'LK', 'Active', 0),
(3028, 'Sabaragamuwa', 'SA', 196, 'LK', 'Active', 0),
(3029, 'Southern', 'SO', 196, 'LK', 'Active', 0),
(3030, 'Uva', 'UV', 196, 'LK', 'Active', 0),
(3031, 'Western', 'WE', 196, 'LK', 'Active', 0),
(3032, 'Ascension', 'A', 197, 'SH', 'Active', 0),
(3033, 'Saint Helena', 'S', 197, 'SH', 'Active', 0),
(3034, 'Tristan da Cunha', 'T', 197, 'SH', 'Active', 0),
(3036, 'Al Bahr al Ahmar', 'BAM', 199, 'SD', 'Active', 0),
(3037, 'Al Buhayrat', 'BRT', 199, 'SD', 'Active', 0),
(3038, 'Al Jazirah', 'JZR', 199, 'SD', 'Active', 0),
(3039, 'Al Khartum', 'KRT', 199, '', 'Active', 0),
(3040, 'Al Qadarif', 'QDR', 199, 'SD', 'Active', 0),
(3041, 'Al Wahdah', 'WDH', 199, 'SD', 'Active', 0),
(3042, 'An Nil al Abyad', 'ANB', 199, 'SD', 'Active', 0),
(3043, 'An Nil al Azraq', 'ANZ', 199, 'SD', 'Active', 0),
(3044, 'Ash Shamaliyah', 'ASH', 199, 'SD', 'Active', 0),
(3045, 'Bahr al Jabal', 'BJA', 199, 'SD', 'Active', 0),
(3046, 'Gharb al Istiwa\'iyah', 'GIS', 199, 'SD', 'Active', 0),
(3047, 'Gharb Bahr al Ghazal', 'GBG', 199, 'SD', 'Active', 0),
(3048, 'Gharb Darfur', 'GDA', 199, 'SD', 'Active', 0),
(3049, 'Gharb Kurdufan', 'GKU', 199, 'SD', 'Active', 0),
(3050, 'Janub Darfur', 'JDA', 199, 'SD', 'Active', 0),
(3051, 'Janub Kurdufan', 'JKU', 199, 'SD', 'Active', 0),
(3052, 'Junqali', 'JQL', 199, 'SD', 'Active', 0),
(3053, 'Kassala', 'KSL', 199, 'SD', 'Active', 0),
(3054, 'Nahr an Nil', 'NNL', 199, 'SD', 'Active', 0),
(3055, 'Shamal Bahr al Ghazal', 'SBG', 199, 'SD', 'Active', 0),
(3056, 'Shamal Darfur', 'SDA', 199, 'SD', 'Active', 0),
(3057, 'Shamal Kurdufan', 'SKU', 199, 'SD', 'Active', 0),
(3058, 'Sharq al Istiwa\'iyah', 'SIS', 199, 'SD', 'Active', 0),
(3059, 'Sinnar', 'SNR', 199, 'SD', 'Active', 0),
(3060, 'Warab', 'WRB', 199, 'SD', 'Active', 0),
(3061, 'Brokopondo', 'BR', 200, 'SR', 'Active', 0),
(3062, 'Commewijne', 'CM', 200, 'SR', 'Active', 0),
(3063, 'Coronie', 'CR', 200, 'SR', 'Active', 0),
(3064, 'Marowijne', 'MA', 200, 'SR', 'Active', 0),
(3065, 'Nickerie', 'NI', 200, 'SR', 'Active', 0),
(3066, 'Para', 'PA', 200, 'SR', 'Active', 0),
(3067, 'Paramaribo', 'PM', 200, 'SR', 'Active', 0),
(3068, 'Saramacca', 'SA', 200, 'SR', 'Active', 0),
(3069, 'Sipaliwini', 'SI', 200, 'SR', 'Active', 0),
(3070, 'Wanica', 'WA', 200, 'SR', 'Active', 0),
(3071, 'Hhohho', 'H', 202, 'SZ', 'Active', 0),
(3072, 'Lubombo', 'L', 202, 'SZ', 'Active', 0),
(3073, 'Manzini', 'M', 202, 'SZ', 'Active', 0),
(3074, 'Shishelweni', 'S', 202, 'SZ', 'Active', 0),
(3075, 'Blekinge', 'BL', 203, 'SE', 'Active', 0),
(3076, 'Dalama', 'DA', 203, 'SE', 'Active', 0),
(3077, 'Gavleborg', 'GA', 203, 'SE', 'Active', 0),
(3078, 'Gotland', 'GO', 203, 'SE', 'Active', 0),
(3079, 'Halland', 'HA', 203, 'SE', 'Active', 0),
(3080, 'Jamtland', 'JA', 203, 'SE', 'Active', 0),
(3081, 'Jonkping', 'JO', 203, 'SE', 'Active', 0),
(3082, 'Kalmar', 'KA', 203, 'SE', 'Active', 0),
(3083, 'Kronoberg', 'KR', 203, 'SE', 'Active', 0),
(3084, 'Norrbotten', 'NO', 203, 'SE', 'Active', 0),
(3085, 'Orebro', 'OR', 203, 'SE', 'Active', 0),
(3086, 'Ostergotland', 'OG', 203, 'SE', 'Active', 0),
(3087, 'Skane', 'SK', 203, 'SE', 'Active', 0),
(3088, 'Sodermanland', 'SO', 203, 'SE', 'Active', 0),
(3089, 'Stockholm', 'ST', 203, 'SE', 'Active', 0),
(3090, 'Uppdala', 'UP', 203, 'SE', 'Active', 0),
(3091, 'Varmland', 'VL', 203, 'SE', 'Active', 0),
(3092, 'Vasterbotten', 'VB', 203, 'SE', 'Active', 0),
(3093, 'Vasternorrland', 'VN', 203, 'SE', 'Active', 0),
(3094, 'Vastmanland', 'VM', 203, 'SE', 'Active', 0),
(3095, 'Vastra Gotaland', 'VG', 203, 'SE', 'Active', 0),
(3097, 'Appenzell Ausserrhoden', 'AR', 204, 'CH', 'Active', 0),
(3098, 'Appenzell Innerrhoden', 'AI', 204, 'CH', 'Active', 0),
(3099, 'Basel-Stadt', 'BS', 204, 'CH', 'Active', 0),
(3100, 'Basel-Landschaft', 'BL', 204, 'CH', 'Active', 0),
(3101, 'Bern', 'BE', 204, 'CH', 'Active', 0),
(3102, 'Fribourg', 'FR', 204, 'CH', 'Active', 0),
(3103, 'Gen&egrave;ve', 'GE', 204, 'CH', 'Active', 0),
(3104, 'Glarus', 'GL', 204, 'CH', 'Active', 0),
(3105, 'Graubunden', 'GR', 204, 'CH', 'Active', 0),
(3106, 'Jura', 'JU', 204, 'CH', 'Active', 0),
(3107, 'Luzern', 'LU', 204, 'CH', 'Active', 0),
(3108, 'Neuch?tel', 'NE', 204, 'CH', 'Active', 0),
(3109, 'Nidwald', 'NW', 204, 'CH', 'Active', 0),
(3110, 'Obwald', 'OW', 204, 'CH', 'Active', 0),
(3111, 'St. Gallen', 'SG', 204, 'CH', 'Active', 0),
(3112, 'Schaffhausen', 'SH', 204, 'CH', 'Active', 0),
(3113, 'Schwyz', 'SZ', 204, 'CH', 'Active', 0),
(3114, 'Solothurn', 'SO', 204, 'CH', 'Active', 0),
(3115, 'Thurgau', 'TG', 204, 'CH', 'Active', 0),
(3116, 'Ticino', 'TI', 204, 'CH', 'Active', 0);
INSERT INTO `state` (`iStateId`, `vState`, `vStateCode`, `iCountryId`, `vCountryCode`, `eStatus`, `iSysRecDeleted`) VALUES
(3117, 'Uri', 'UR', 204, 'CH', 'Active', 0),
(3118, 'Valais', 'VS', 204, 'CH', 'Active', 0),
(3119, 'Vaud', 'VD', 204, 'CH', 'Active', 0),
(3120, 'Zug', 'ZG', 204, 'CH', 'Active', 0),
(3121, 'Z&uuml;rich', 'ZH', 204, 'CH', 'Active', 0),
(3122, 'Al Hasakah', 'HA', 205, 'SY', 'Active', 0),
(3123, 'Al Ladhiqiyah', 'LA', 205, 'SY', 'Active', 0),
(3124, 'Al Qunaytirah', 'QU', 205, 'SY', 'Active', 0),
(3125, 'Ar Raqqah', 'RQ', 205, 'SY', 'Active', 0),
(3126, 'As Suwayda', 'SU', 205, 'SY', 'Active', 0),
(3127, 'Dara', 'DA', 205, 'SY', 'Active', 0),
(3128, 'Dayr az Zawr', 'DZ', 205, 'SY', 'Active', 0),
(3129, 'Dimashq', 'DI', 205, 'SY', 'Active', 0),
(3130, 'Halab', 'HL', 205, 'SY', 'Active', 0),
(3131, 'Hamah', 'HM', 205, 'SY', 'Active', 0),
(3132, 'Hims', 'HI', 205, 'SY', 'Active', 0),
(3133, 'Idlib', 'ID', 205, 'SY', 'Active', 0),
(3134, 'Rif Dimashq', 'RD', 205, 'SY', 'Active', 0),
(3135, 'Tartus', 'TA', 205, 'SY', 'Active', 0),
(3136, 'Chang-hua', 'CH', 206, 'TW', 'Active', 0),
(3137, 'Chia-i', 'CI', 206, 'TW', 'Active', 0),
(3138, 'Hsin-chu', 'HS', 206, 'TW', 'Active', 0),
(3139, 'Hua-lien', 'HL', 206, 'TW', 'Active', 0),
(3140, 'I-lan', 'IL', 206, 'TW', 'Active', 0),
(3141, 'Kao-hsiung county', 'KH', 206, 'TW', 'Active', 0),
(3142, 'Kin-men', 'KM', 206, 'TW', 'Active', 0),
(3143, 'Lien-chiang', 'LC', 206, 'TW', 'Active', 0),
(3144, 'Miao-li', 'ML', 206, 'TW', 'Active', 0),
(3145, 'Nan-t\'ou', 'NT', 206, 'TW', 'Active', 0),
(3146, 'P\'eng-hu', 'PH', 206, 'TW', 'Active', 0),
(3147, 'P\'ing-tung', 'PT', 206, 'TW', 'Active', 0),
(3148, 'T\'ai-chung', 'TG', 206, 'TW', 'Active', 0),
(3149, 'T\'ai-nan', 'TA', 206, 'TW', 'Active', 0),
(3150, 'T\'ai-pei county', 'TP', 206, 'TW', 'Active', 0),
(3151, 'T\'ai-tung', 'TT', 206, 'TW', 'Active', 0),
(3152, 'T\'ao-yuan', 'TY', 206, 'TW', 'Active', 0),
(3153, 'Yun-lin', 'YL', 206, 'TW', 'Active', 0),
(3154, 'Chia-i city', 'CC', 206, 'TW', 'Active', 0),
(3155, 'Chi-lung', 'CL', 206, 'TW', 'Active', 0),
(3156, 'Hsin-chu', 'HC', 206, 'TW', 'Active', 0),
(3157, 'T\'ai-chung', 'TH', 206, 'TW', 'Active', 0),
(3158, 'T\'ai-nan', 'TN', 206, 'TW', 'Active', 0),
(3159, 'Kao-hsiung city', 'KC', 206, 'TW', 'Active', 0),
(3160, 'T\'ai-pei city', 'TC', 206, 'TW', 'Active', 0),
(3161, 'Gorno-Badakhstan', 'GB', 207, 'TJ', 'Active', 0),
(3162, 'Khatlon', 'KT', 207, 'TJ', 'Active', 0),
(3163, 'Sughd', 'SU', 207, 'TJ', 'Active', 0),
(3164, 'Arusha', 'AR', 208, 'TZ', 'Active', 0),
(3165, 'Dar es Salaam', 'DS', 208, 'TZ', 'Active', 0),
(3166, 'Dodoma', 'DO', 208, 'TZ', 'Active', 0),
(3167, 'Iringa', 'IR', 208, 'TZ', 'Active', 0),
(3168, 'Kagera', 'KA', 208, 'TZ', 'Active', 0),
(3169, 'Kigoma', 'KI', 208, 'TZ', 'Active', 0),
(3170, 'Kilimanjaro', 'KJ', 208, 'TZ', 'Active', 0),
(3171, 'Lindi', 'LN', 208, 'TZ', 'Active', 0),
(3172, 'Manyara', 'MY', 208, 'TZ', 'Active', 0),
(3173, 'Mara', 'MR', 208, 'TZ', 'Active', 0),
(3174, 'Mbeya', 'MB', 208, 'TZ', 'Active', 0),
(3175, 'Morogoro', 'MO', 208, 'TZ', 'Active', 0),
(3176, 'Mtwara', 'MT', 208, 'TZ', 'Active', 0),
(3177, 'Mwanza', 'MW', 208, 'TZ', 'Active', 0),
(3178, 'Pemba North', 'PN', 208, 'TZ', 'Active', 0),
(3179, 'Pemba South', 'PS', 208, 'TZ', 'Active', 0),
(3180, 'Pwani', 'PW', 208, 'TZ', 'Active', 0),
(3181, 'Rukwa', 'RK', 208, 'TZ', 'Active', 0),
(3182, 'Ruvuma', 'RV', 208, 'TZ', 'Active', 0),
(3183, 'Shinyanga', 'SH', 208, 'TZ', 'Active', 0),
(3184, 'Singida', 'SI', 208, 'TZ', 'Active', 0),
(3185, 'Tabora', 'TB', 208, 'TZ', 'Active', 0),
(3186, 'Tanga', 'TN', 208, 'TZ', 'Active', 0),
(3187, 'Zanzibar Central/South', 'ZC', 208, 'TZ', 'Active', 0),
(3188, 'Zanzibar North', 'ZN', 208, 'TZ', 'Active', 0),
(3189, 'Zanzibar Urban/West', 'ZU', 208, 'TZ', 'Active', 0),
(3190, 'Amnat Charoen', 'Amnat Charoen', 209, 'TH', 'Active', 0),
(3191, 'Ang Thong', 'Ang Thong', 209, 'TH', 'Active', 0),
(3192, 'Ayutthaya', 'Ayutthaya', 209, 'TH', 'Active', 0),
(3193, 'Bangkok', 'Bangkok', 209, 'TH', 'Active', 0),
(3194, 'Buriram', 'Buriram', 209, 'TH', 'Active', 0),
(3195, 'Chachoengsao', 'Chachoengsao', 209, 'TH', 'Active', 0),
(3196, 'Chai Nat', 'Chai Nat', 209, 'TH', 'Active', 0),
(3197, 'Chaiyaphum', 'Chaiyaphum', 209, 'TH', 'Active', 0),
(3198, 'Chanthaburi', 'Chanthaburi', 209, 'TH', 'Active', 0),
(3199, 'Chiang Mai', 'Chiang Mai', 209, 'TH', 'Active', 0),
(3200, 'Chiang Rai', 'Chiang Rai', 209, 'TH', 'Active', 0),
(3201, 'Chon Buri', 'Chon Buri', 209, 'TH', 'Active', 0),
(3202, 'Chumphon', 'Chumphon', 209, 'TH', 'Active', 0),
(3203, 'Kalasin', 'Kalasin', 209, 'TH', 'Active', 0),
(3204, 'Kamphaeng Phet', 'Kamphaeng Phet', 209, 'TH', 'Active', 0),
(3205, 'Kanchanaburi', 'Kanchanaburi', 209, 'TH', 'Active', 0),
(3206, 'Khon Kaen', 'Khon Kaen', 209, 'TH', 'Active', 0),
(3207, 'Krabi', 'Krabi', 209, 'TH', 'Active', 0),
(3208, 'Lampang', 'Lampang', 209, 'TH', 'Active', 0),
(3209, 'Lamphun', 'Lamphun', 209, 'TH', 'Active', 0),
(3210, 'Loei', 'Loei', 209, 'TH', 'Active', 0),
(3211, 'Lop Buri', 'Lop Buri', 209, 'TH', 'Active', 0),
(3212, 'Mae Hong Son', 'Mae Hong Son', 209, 'TH', 'Active', 0),
(3213, 'Maha Sarakham', 'Maha Sarakham', 209, 'TH', 'Active', 0),
(3214, 'Mukdahan', 'Mukdahan', 209, 'TH', 'Active', 0),
(3215, 'Nakhon Nayok', 'Nakhon Nayok', 209, 'TH', 'Active', 0),
(3216, 'Nakhon Pathom', 'Nakhon Pathom', 209, 'TH', 'Active', 0),
(3217, 'Nakhon Phanom', 'Nakhon Phanom', 209, 'TH', 'Active', 0),
(3218, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 209, 'TH', 'Active', 0),
(3219, 'Nakhon Sawan', 'Nakhon Sawan', 209, 'TH', 'Active', 0),
(3220, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 209, 'TH', 'Active', 0),
(3221, 'Nan', 'Nan', 209, 'TH', 'Active', 0),
(3222, 'Narathiwat', 'Narathiwat', 209, 'TH', 'Active', 0),
(3223, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 209, 'TH', 'Active', 0),
(3224, 'Nong Khai', 'Nong Khai', 209, 'TH', 'Active', 0),
(3225, 'Nonthaburi', 'Nonthaburi', 209, 'TH', 'Active', 0),
(3226, 'Pathum Thani', 'Pathum Thani', 209, 'TH', 'Active', 0),
(3227, 'Pattani', 'Pattani', 209, 'TH', 'Active', 0),
(3228, 'Phangnga', 'Phangnga', 209, 'TH', 'Active', 0),
(3229, 'Phatthalung', 'Phatthalung', 209, 'TH', 'Active', 0),
(3230, 'Phayao', 'Phayao', 209, 'TH', 'Active', 0),
(3231, 'Phetchabun', 'Phetchabun', 209, 'TH', 'Active', 0),
(3232, 'Phetchaburi', 'Phetchaburi', 209, 'TH', 'Active', 0),
(3233, 'Phichit', 'Phichit', 209, 'TH', 'Active', 0),
(3234, 'Phitsanulok', 'Phitsanulok', 209, 'TH', 'Active', 0),
(3235, 'Phrae', 'Phrae', 209, 'TH', 'Active', 0),
(3236, 'Phuket', 'Phuket', 209, 'TH', 'Active', 0),
(3237, 'Prachin Buri', 'Prachin Buri', 209, 'TH', 'Active', 0),
(3238, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 209, 'TH', 'Active', 0),
(3239, 'Ranong', 'Ranong', 209, 'TH', 'Active', 0),
(3240, 'Ratchaburi', 'Ratchaburi', 209, 'TH', 'Active', 0),
(3241, 'Rayong', 'Rayong', 209, 'TH', 'Active', 0),
(3242, 'Roi Et', 'Roi Et', 209, 'TH', 'Active', 0),
(3243, 'Sa Kaeo', 'Sa Kaeo', 209, 'TH', 'Active', 0),
(3244, 'Sakon Nakhon', 'Sakon Nakhon', 209, 'TH', 'Active', 0),
(3245, 'Samut Prakan', 'Samut Prakan', 209, 'TH', 'Active', 0),
(3246, 'Samut Sakhon', 'Samut Sakhon', 209, 'TH', 'Active', 0),
(3247, 'Samut Songkhram', 'Samut Songkhram', 209, 'TH', 'Active', 0),
(3248, 'Sara Buri', 'Sara Buri', 209, 'TH', 'Active', 0),
(3249, 'Satun', 'Satun', 209, 'TH', 'Active', 0),
(3250, 'Sing Buri', 'Sing Buri', 209, 'TH', 'Active', 0),
(3251, 'Sisaket', 'Sisaket', 209, 'TH', 'Active', 0),
(3252, 'Songkhla', 'Songkhla', 209, 'TH', 'Active', 0),
(3253, 'Sukhothai', 'Sukhothai', 209, 'TH', 'Active', 0),
(3254, 'Suphan Buri', 'Suphan Buri', 209, 'TH', 'Active', 0),
(3255, 'Surat Thani', 'Surat Thani', 209, 'TH', 'Active', 0),
(3256, 'Surin', 'Surin', 209, 'TH', 'Active', 0),
(3257, 'Tak', 'Tak', 209, 'TH', 'Active', 0),
(3258, 'Trang', 'Trang', 209, 'TH', 'Active', 0),
(3259, 'Trat', 'Trat', 209, 'TH', 'Active', 0),
(3260, 'Ubon Ratchathani', 'Ubon Ratchathani', 209, 'TH', 'Active', 0),
(3261, 'Udon Thani', 'Udon Thani', 209, 'TH', 'Active', 0),
(3262, 'Uthai Thani', 'Uthai Thani', 209, 'TH', 'Active', 0),
(3263, 'Uttaradit', 'Uttaradit', 209, 'TH', 'Active', 0),
(3264, 'Yala', 'Yala', 209, 'TH', 'Active', 0),
(3265, 'Yasothon', 'Yasothon', 209, 'TH', 'Active', 0),
(3266, 'Kara', 'K', 210, 'TG', 'Active', 0),
(3267, 'Plateaux', 'P', 210, 'TG', 'Active', 0),
(3268, 'Savanes', 'S', 210, 'TG', 'Active', 0),
(3269, 'Centrale', 'C', 210, 'TG', 'Active', 0),
(3270, 'Maritime', 'M', 210, 'TG', 'Active', 0),
(3271, 'Atafu', 'A', 211, 'TK', 'Active', 0),
(3272, 'Fakaofo', 'F', 211, 'TK', 'Active', 0),
(3273, 'Nukunonu', 'N', 211, 'TK', 'Active', 0),
(3274, 'Ha\'apai', 'H', 212, 'TO', 'Active', 0),
(3275, 'Tongatapu', 'T', 212, 'TO', 'Active', 0),
(3276, 'Vava\'u', 'V', 212, 'TO', 'Active', 0),
(3277, 'Couva/Tabaquite/Talparo', 'CT', 213, 'TT', 'Active', 0),
(3278, 'Diego Martin', 'DM', 213, 'TT', 'Active', 0),
(3279, 'Mayaro/Rio Claro', 'MR', 213, 'TT', 'Active', 0),
(3280, 'Penal/Debe', 'PD', 213, 'TT', 'Active', 0),
(3281, 'Princes Town', 'PT', 213, 'TT', 'Active', 0),
(3282, 'Sangre Grande', 'SG', 213, 'TT', 'Active', 0),
(3283, 'San Juan/Laventille', 'SL', 213, 'TT', 'Active', 0),
(3284, 'Siparia', 'SI', 213, 'TT', 'Active', 0),
(3285, 'Tunapuna/Piarco', 'TP', 213, 'TT', 'Active', 0),
(3286, 'Port of Spain', 'PS', 213, 'TT', 'Active', 0),
(3287, 'San Fernando', 'SF', 213, 'TT', 'Active', 0),
(3288, 'Arima', 'AR', 213, 'TT', 'Active', 0),
(3289, 'Point Fortin', 'PF', 213, 'TT', 'Active', 0),
(3290, 'Chaguanas', 'CH', 213, 'TT', 'Active', 0),
(3291, 'Tobago', 'TO', 213, 'TT', 'Active', 0),
(3292, 'Ariana', 'AR', 214, 'TN', 'Active', 0),
(3293, 'Beja', 'BJ', 214, 'TN', 'Active', 0),
(3294, 'Ben Arous', 'BA', 214, 'TN', 'Active', 0),
(3295, 'Bizerte', 'BI', 214, 'TN', 'Active', 0),
(3296, 'Gabes', 'GB', 214, 'TN', 'Active', 0),
(3297, 'Gafsa', 'GF', 214, 'TN', 'Active', 0),
(3298, 'Jendouba', 'JE', 214, 'TN', 'Active', 0),
(3299, 'Kairouan', 'KR', 214, 'TN', 'Active', 0),
(3300, 'Kasserine', 'KS', 214, 'TN', 'Active', 0),
(3301, 'Kebili', 'KB', 214, 'TN', 'Active', 0),
(3302, 'Kef', 'KF', 214, 'TN', 'Active', 0),
(3303, 'Mahdia', 'MH', 214, 'TN', 'Active', 0),
(3304, 'Manouba', 'MN', 214, 'TN', 'Active', 0),
(3305, 'Medenine', 'ME', 214, 'TN', 'Active', 0),
(3306, 'Monastir', 'MO', 214, 'TN', 'Active', 0),
(3307, 'Nabeul', 'NA', 214, 'TN', 'Active', 0),
(3308, 'Sfax', 'SF', 214, 'TN', 'Active', 0),
(3309, 'Sidi', 'SD', 214, 'TN', 'Active', 0),
(3310, 'Siliana', 'SL', 214, 'TN', 'Active', 0),
(3311, 'Sousse', 'SO', 214, 'TN', 'Active', 0),
(3312, 'Tataouine', 'TA', 214, 'TN', 'Active', 0),
(3313, 'Tozeur', 'TO', 214, 'TN', 'Active', 0),
(3314, 'Tunis', 'TU', 214, 'TN', 'Active', 0),
(3315, 'Zaghouan', 'ZA', 214, 'TN', 'Active', 0),
(3316, 'Adana', 'ADA', 215, 'TR', 'Active', 0),
(3317, 'Adiyaman', 'ADI', 215, 'TR', 'Active', 0),
(3318, 'Afyonkarahisar', 'AFY', 215, 'TR', 'Active', 0),
(3319, 'Agri', 'AGR', 215, 'TR', 'Active', 0),
(3320, 'Aksaray', 'AKS', 215, 'TR', 'Active', 0),
(3321, 'Amasya', 'AMA', 215, 'TR', 'Active', 0),
(3322, 'Ankara', 'ANK', 215, 'TR', 'Active', 0),
(3323, 'Antalya', 'ANT', 215, 'TR', 'Active', 0),
(3324, 'Ardahan', 'ARD', 215, 'TR', 'Active', 0),
(3325, 'Artvin', 'ART', 215, 'TR', 'Active', 0),
(3326, 'Aydin', 'AYI', 215, 'TR', 'Active', 0),
(3327, 'Balikesir', 'BAL', 215, 'TR', 'Active', 0),
(3328, 'Bartin', 'BAR', 215, 'TR', 'Active', 0),
(3329, 'Batman', 'BAT', 215, 'TR', 'Active', 0),
(3330, 'Bayburt', 'BAY', 215, 'TR', 'Active', 0),
(3331, 'Bilecik', 'BIL', 215, 'TR', 'Active', 0),
(3332, 'Bingol', 'BIN', 215, 'TR', 'Active', 0),
(3333, 'Bitlis', 'BIT', 215, 'TR', 'Active', 0),
(3334, 'Bolu', 'BOL', 215, 'TR', 'Active', 0),
(3335, 'Burdur', 'BRD', 215, 'TR', 'Active', 0),
(3336, 'Bursa', 'BRS', 215, 'TR', 'Active', 0),
(3337, 'Canakkale', 'CKL', 215, 'TR', 'Active', 0),
(3338, 'Cankiri', 'CKR', 215, 'TR', 'Active', 0),
(3339, 'Corum', 'COR', 215, 'TR', 'Active', 0),
(3340, 'Denizli', 'DEN', 215, 'TR', 'Active', 0),
(3341, 'Diyarbakir', 'DIY', 215, 'TR', 'Active', 0),
(3342, 'Duzce', 'DUZ', 215, 'TR', 'Active', 0),
(3343, 'Edirne', 'EDI', 215, 'TR', 'Active', 0),
(3344, 'Elazig', 'ELA', 215, 'TR', 'Active', 0),
(3345, 'Erzincan', 'EZC', 215, 'TR', 'Active', 0),
(3346, 'Erzurum', 'EZR', 215, 'TR', 'Active', 0),
(3347, 'Eskisehir', 'ESK', 215, 'TR', 'Active', 0),
(3348, 'Gaziantep', 'GAZ', 215, 'TR', 'Active', 0),
(3349, 'Giresun', 'GIR', 215, 'TR', 'Active', 0),
(3350, 'Gumushane', 'GMS', 215, 'TR', 'Active', 0),
(3351, 'Hakkari', 'HKR', 215, 'TR', 'Active', 0),
(3352, 'Hatay', 'HTY', 215, 'TR', 'Active', 0),
(3353, 'Igdir', 'IGD', 215, 'TR', 'Active', 0),
(3354, 'Isparta', 'ISP', 215, 'TR', 'Active', 0),
(3355, 'Istanbul', 'IST', 215, 'TR', 'Active', 0),
(3356, 'Izmir', 'IZM', 215, 'TR', 'Active', 0),
(3357, 'Kahramanmaras', 'KAH', 215, 'TR', 'Active', 0),
(3358, 'Karabuk', 'KRB', 215, 'TR', 'Active', 0),
(3359, 'Karaman', 'KRM', 215, 'TR', 'Active', 0),
(3360, 'Kars', 'KRS', 215, 'TR', 'Active', 0),
(3361, 'Kastamonu', 'KAS', 215, 'TR', 'Active', 0),
(3362, 'Kayseri', 'KAY', 215, 'TR', 'Active', 0),
(3363, 'Kilis', 'KLS', 215, 'TR', 'Active', 0),
(3364, 'Kirikkale', 'KRK', 215, 'TR', 'Active', 0),
(3365, 'Kirklareli', 'KLR', 215, 'TR', 'Active', 0),
(3366, 'Kirsehir', 'KRH', 215, 'TR', 'Active', 0),
(3367, 'Kocaeli', 'KOC', 215, 'TR', 'Active', 0),
(3368, 'Konya', 'KON', 215, 'TR', 'Active', 0),
(3369, 'Kutahya', 'KUT', 215, 'TR', 'Active', 0),
(3370, 'Malatya', 'MAL', 215, 'TR', 'Active', 0),
(3371, 'Manisa', 'MAN', 215, 'TR', 'Active', 0),
(3372, 'Mardin', 'MAR', 215, 'TR', 'Active', 0),
(3373, 'Mersin', 'MER', 215, 'TR', 'Active', 0),
(3374, 'Mugla', 'MUG', 215, 'TR', 'Active', 0),
(3375, 'Mus', 'MUS', 215, 'TR', 'Active', 0),
(3376, 'Nevsehir', 'NEV', 215, 'TR', 'Active', 0),
(3377, 'Nigde', 'NIG', 215, 'TR', 'Active', 0),
(3378, 'Ordu', 'ORD', 215, 'TR', 'Active', 0),
(3379, 'Osmaniye', 'OSM', 215, 'TR', 'Active', 0),
(3380, 'Rize', 'RIZ', 215, 'TR', 'Active', 0),
(3381, 'Sakarya', 'SAK', 215, 'TR', 'Active', 0),
(3382, 'Samsun', 'SAM', 215, 'TR', 'Active', 0),
(3383, 'Sanliurfa', 'SAN', 215, 'TR', 'Active', 0),
(3384, 'Siirt', 'SII', 215, 'TR', 'Active', 0),
(3385, 'Sinop', 'SIN', 215, 'TR', 'Active', 0),
(3386, 'Sirnak', 'SIR', 215, 'TR', 'Active', 0),
(3387, 'Sivas', 'SIV', 215, 'TR', 'Active', 0),
(3388, 'Tekirdag', 'TEL', 215, 'TR', 'Active', 0),
(3389, 'Tokat', 'TOK', 215, 'TR', 'Active', 0),
(3390, 'Trabzon', 'TRA', 215, 'TR', 'Active', 0),
(3391, 'Tunceli', 'TUN', 215, 'TR', 'Active', 0),
(3392, 'Usak', 'USK', 215, 'TR', 'Active', 0),
(3393, 'Van', 'VAN', 215, 'TR', 'Active', 0),
(3394, 'Yalova', 'YAL', 215, 'TR', 'Active', 0),
(3395, 'Yozgat', 'YOZ', 215, 'TR', 'Active', 0),
(3396, 'Zonguldak', 'ZON', 215, 'TR', 'Active', 0),
(3397, 'Ahal Welayaty', 'A', 216, 'TM', 'Active', 0),
(3398, 'Balkan Welayaty', 'B', 216, 'TM', 'Active', 0),
(3399, 'Dashhowuz Welayaty', 'D', 216, 'TM', 'Active', 0),
(3400, 'Lebap Welayaty', 'L', 216, 'TM', 'Active', 0),
(3401, 'Mary Welayaty', 'M', 216, 'TM', 'Active', 0),
(3402, 'Ambergris Cays', 'AC', 217, 'TC', 'Active', 0),
(3403, 'Dellis Cay', 'DC', 217, 'TC', 'Active', 0),
(3404, 'French Cay', 'FC', 217, 'TC', 'Active', 0),
(3405, 'Little Water Cay', 'LW', 217, 'TC', 'Active', 0),
(3406, 'Parrot Cay', 'RC', 217, 'TC', 'Active', 0),
(3407, 'Pine Cay', 'PN', 217, 'TC', 'Active', 0),
(3408, 'Salt Cay', 'SL', 217, 'TC', 'Active', 0),
(3409, 'Grand Turk', 'GT', 217, 'TC', 'Active', 0),
(3410, 'South Caicos', 'SC', 217, 'TC', 'Active', 0),
(3411, 'East Caicos', 'EC', 217, 'TC', 'Active', 0),
(3412, 'Middle Caicos', 'MC', 217, 'TC', 'Active', 0),
(3413, 'North Caicos', 'NC', 217, 'TC', 'Active', 0),
(3414, 'Providenciales', 'PR', 217, 'TC', 'Active', 0),
(3415, 'West Caicos', 'WC', 217, 'TC', 'Active', 0),
(3416, 'Nanumanga', 'NMG', 218, 'TV', 'Active', 0),
(3417, 'Niulakita', 'NLK', 218, 'TV', 'Active', 0),
(3418, 'Niutao', 'NTO', 218, 'TV', 'Active', 0),
(3419, 'Funafuti', 'FUN', 218, 'TV', 'Active', 0),
(3420, 'Nanumea', 'NME', 218, 'TV', 'Active', 0),
(3421, 'Nui', 'NUI', 218, 'TV', 'Active', 0),
(3422, 'Nukufetau', 'NFT', 218, 'TV', 'Active', 0),
(3423, 'Nukulaelae', 'NLL', 218, 'TV', 'Active', 0),
(3424, 'Vaitupu', 'VAI', 218, 'TV', 'Active', 0),
(3425, 'Kalangala', 'KAL', 219, 'UG', 'Active', 0),
(3426, 'Kampala', 'KMP', 219, 'UG', 'Active', 0),
(3427, 'Kayunga', 'KAY', 219, 'UG', 'Active', 0),
(3428, 'Kiboga', 'KIB', 219, 'UG', 'Active', 0),
(3429, 'Luwero', 'LUW', 219, 'UG', 'Active', 0),
(3430, 'Masaka', 'MAS', 219, 'UG', 'Active', 0),
(3431, 'Mpigi', 'MPI', 219, 'UG', 'Active', 0),
(3432, 'Mubende', 'MUB', 219, 'UG', 'Active', 0),
(3433, 'Mukono', 'MUK', 219, 'UG', 'Active', 0),
(3434, 'Nakasongola', 'NKS', 219, 'UG', 'Active', 0),
(3435, 'Rakai', 'RAK', 219, 'UG', 'Active', 0),
(3436, 'Sembabule', 'SEM', 219, 'UG', 'Active', 0),
(3437, 'Wakiso', 'WAK', 219, 'UG', 'Active', 0),
(3438, 'Bugiri', 'BUG', 219, 'UG', 'Active', 0),
(3439, 'Busia', 'BUS', 219, 'UG', 'Active', 0),
(3440, 'Iganga', 'IGA', 219, 'UG', 'Active', 0),
(3441, 'Jinja', 'JIN', 219, 'UG', 'Active', 0),
(3442, 'Kaberamaido', 'KAB', 219, 'UG', 'Active', 0),
(3443, 'Kamuli', 'KML', 219, 'UG', 'Active', 0),
(3444, 'Kapchorwa', 'KPC', 219, 'UG', 'Active', 0),
(3445, 'Katakwi', 'KTK', 219, 'UG', 'Active', 0),
(3446, 'Kumi', 'KUM', 219, 'UG', 'Active', 0),
(3447, 'Mayuge', 'MAY', 219, 'UG', 'Active', 0),
(3448, 'Mbale', 'MBA', 219, 'UG', 'Active', 0),
(3449, 'Pallisa', 'PAL', 219, 'UG', 'Active', 0),
(3450, 'Sironko', 'SIR', 219, 'UG', 'Active', 0),
(3451, 'Soroti', 'SOR', 219, 'UG', 'Active', 0),
(3452, 'Tororo', 'TOR', 219, 'UG', 'Active', 0),
(3453, 'Adjumani', 'ADJ', 219, 'UG', 'Active', 0),
(3454, 'Apac', 'APC', 219, 'UG', 'Active', 0),
(3455, 'Arua', 'ARU', 219, 'UG', 'Active', 0),
(3456, 'Gulu', 'GUL', 219, 'UG', 'Active', 0),
(3457, 'Kitgum', 'KIT', 219, 'UG', 'Active', 0),
(3458, 'Kotido', 'KOT', 219, 'UG', 'Active', 0),
(3459, 'Lira', 'LIR', 219, 'UG', 'Active', 0),
(3460, 'Moroto', 'MRT', 219, 'UG', 'Active', 0),
(3461, 'Moyo', 'MOY', 219, 'UG', 'Active', 0),
(3462, 'Nakapiripirit', 'NAK', 219, 'UG', 'Active', 0),
(3463, 'Nebbi', 'NEB', 219, 'UG', 'Active', 0),
(3464, 'Pader', 'PAD', 219, 'UG', 'Active', 0),
(3465, 'Yumbe', 'YUM', 219, 'UG', 'Active', 0),
(3466, 'Bundibugyo', 'BUN', 219, 'UG', 'Active', 0),
(3467, 'Bushenyi', 'BSH', 219, 'UG', 'Active', 0),
(3468, 'Hoima', 'HOI', 219, 'UG', 'Active', 0),
(3469, 'Kabale', 'KBL', 219, 'UG', 'Active', 0),
(3470, 'Kabarole', 'KAR', 219, 'UG', 'Active', 0),
(3471, 'Kamwenge', 'KAM', 219, 'UG', 'Active', 0),
(3472, 'Kanungu', 'KAN', 219, 'UG', 'Active', 0),
(3473, 'Kasese', 'KAS', 219, 'UG', 'Active', 0),
(3474, 'Kibaale', 'KBA', 219, 'UG', 'Active', 0),
(3475, 'Kisoro', 'KIS', 219, 'UG', 'Active', 0),
(3476, 'Kyenjojo', 'KYE', 219, 'UG', 'Active', 0),
(3477, 'Masindi', 'MSN', 219, 'UG', 'Active', 0),
(3478, 'Mbarara', 'MBR', 219, 'UG', 'Active', 0),
(3479, 'Ntungamo', 'NTU', 219, 'UG', 'Active', 0),
(3484, 'Crimea', 'CR', 220, 'UA', 'Active', 0),
(3485, 'Dnipropetrovs\'k', 'DN', 220, 'UA', 'Active', 0),
(3486, 'Donets\'k', 'DO', 220, 'UA', 'Active', 0),
(3487, 'Ivano-Frankivs\'k', 'IV', 220, 'UA', 'Active', 0),
(3488, 'Kharkiv Kherson', 'KL', 220, 'UA', 'Active', 0),
(3489, 'Khmel\'nyts\'kyy', 'KM', 220, 'UA', 'Active', 0),
(3490, 'Kirovohrad', 'KR', 220, 'UA', 'Active', 0),
(3491, 'Kiev', 'KV', 220, 'UA', 'Active', 0),
(3492, 'Kyyiv', 'KY', 220, 'UA', 'Active', 0),
(3493, 'Luhans\'k', 'LU', 220, 'UA', 'Active', 0),
(3494, 'L\'viv', 'LV', 220, 'UA', 'Active', 0),
(3495, 'Mykolayiv', 'MY', 220, 'UA', 'Active', 0),
(3496, 'Odesa', 'OD', 220, 'UA', 'Active', 0),
(3497, 'Poltava', 'PO', 220, 'UA', 'Active', 0),
(3498, 'Rivne', 'RI', 220, 'UA', 'Active', 0),
(3499, 'Sevastopol', 'SE', 220, 'UA', 'Active', 0),
(3500, 'Sumy', 'SU', 220, 'UA', 'Active', 0),
(3501, 'Ternopil\'', 'TE', 220, 'UA', 'Active', 0),
(3502, 'Vinnytsya', 'VI', 220, 'UA', 'Active', 0),
(3503, 'Volyn\'', 'VO', 220, 'UA', 'Active', 0),
(3504, 'Zakarpattya', 'ZK', 220, 'UA', 'Active', 0),
(3505, 'Zaporizhzhya', 'ZA', 220, 'UA', 'Active', 0),
(3506, 'Zhytomyr', 'ZH', 220, 'UA', 'Active', 0),
(3507, 'Abu Zaby', 'AZ', 221, 'AE', 'Active', 0),
(3508, 'Ajman', 'AJ', 221, 'AE', 'Active', 0),
(3509, 'Al Fujayrah', 'FU', 221, 'AE', 'Active', 0),
(3510, 'Ash Shariqah', 'SH', 221, 'AE', 'Active', 0),
(3511, 'Dubayy', 'DU', 221, 'AE', 'Active', 0),
(3512, 'R\'as al Khaymah', 'RK', 221, 'AE', 'Active', 0),
(3513, 'Umm al Qaywayn', 'UQ', 221, 'AE', 'Active', 0),
(3614, 'Alabamas', 'AL', 223, 'US', 'Active', 0),
(3615, 'Alaska', 'AK', 223, 'US', 'Active', 0),
(3616, 'American Samoa', 'AS', 223, 'US', 'Active', 0),
(3617, 'Arizona', 'AZ', 223, 'US', 'Active', 0),
(3618, 'Arkansas', 'AR', 223, 'US', 'Active', 0),
(3619, 'Armed Forces Africa', 'AF', 223, 'US', 'Active', 0),
(3620, 'Armed Forces Americas', 'AA', 223, 'US', 'Active', 0),
(3621, 'Armed Forces Canada', 'AC', 223, 'US', 'Active', 0),
(3622, 'Armed Forces Europe', 'AE', 223, 'US', 'Active', 0),
(3623, 'Armed Forces Middle East', 'AM', 223, 'US', 'Active', 0),
(3624, 'Armed Forces Pacific', 'AP', 223, 'US', 'Active', 0),
(3625, 'California', 'CA', 223, 'US', 'Active', 0),
(3626, 'Colorado', 'CO', 223, 'US', 'Active', 0),
(3627, 'Connecticut', 'CT', 223, 'US', 'Active', 0),
(3628, 'Delaware', 'DE', 223, 'US', 'Active', 0),
(3629, 'District of Columbia', 'DC', 223, 'US', 'Active', 0),
(3630, 'Federated States Of Micronesia', 'FM', 223, 'US', 'Active', 0),
(3631, 'Florida', 'FL', 223, 'US', 'Active', 0),
(3632, 'Georgia', 'GA', 223, 'US', 'Active', 0),
(3633, 'Guam', 'GU', 223, 'US', 'Active', 0),
(3634, 'Hawaii', 'HI', 223, 'US', 'Active', 0),
(3635, 'Idaho', 'ID', 223, 'US', 'Active', 0),
(3636, 'Illinois', 'IL', 223, 'US', 'Active', 0),
(3637, 'Indiana', 'IN', 223, 'US', 'Active', 0),
(3638, 'Iowa', 'IA', 223, 'US', 'Active', 0),
(3639, 'Kansas', 'KS', 223, 'US', 'Active', 0),
(3640, 'Kentucky', 'KY', 223, 'US', 'Active', 0),
(3641, 'Louisiana', 'LA', 223, 'US', 'Active', 0),
(3642, 'Maine', 'ME', 223, 'US', 'Active', 0),
(3643, 'Marshall Islands', 'MH', 223, 'US', 'Active', 0),
(3644, 'Maryland', 'MD', 223, 'US', 'Active', 0),
(3645, 'Massachusetts', 'MA', 223, 'US', 'Active', 0),
(3646, 'Michigan', 'MI', 223, 'US', 'Active', 0),
(3647, 'Minnesota', 'MN', 223, 'US', 'Active', 0),
(3648, 'Mississippi', 'MS', 223, 'US', 'Active', 0),
(3649, 'Missouri', 'MO', 223, 'US', 'Active', 0),
(3650, 'Montana', 'MT', 223, 'US', 'Active', 0),
(3651, 'Nebraska', 'NE', 223, 'US', 'Active', 0),
(3652, 'Nevada', 'NV', 223, 'US', 'Active', 0),
(3653, 'New Hampshire', 'NH', 223, 'US', 'Active', 0),
(3654, 'New Jersey', 'NJ', 223, 'US', 'Active', 0),
(3655, 'New Mexico', 'NM', 223, 'US', 'Active', 0),
(3656, 'New York', 'NY', 223, 'US', 'Active', 0),
(3657, 'North Carolina', 'NC', 223, 'US', 'Active', 0),
(3658, 'North Dakota', 'ND', 223, 'US', 'Active', 0),
(3659, 'Northern Mariana Islands', 'MP', 223, 'US', 'Active', 0),
(3660, 'Ohio', 'OH', 223, 'US', 'Active', 0),
(3661, 'Oklahoma', 'OK', 223, 'US', 'Active', 0),
(3662, 'Oregon', 'OR', 223, 'US', 'Active', 0),
(3663, 'Palau', 'PW', 223, 'US', 'Active', 0),
(3664, 'Pennsylvania', 'PA', 223, 'US', 'Active', 0),
(3665, 'Puerto Rico', 'PR', 223, 'US', 'Active', 0),
(3666, 'Rhode Island', 'RI', 223, 'US', 'Active', 0),
(3667, 'South Carolina', 'SC', 223, 'US', 'Active', 0),
(3668, 'South Dakota', 'SD', 223, 'US', 'Active', 0),
(3669, 'Tennessee', 'TN', 223, 'US', 'Active', 0),
(3670, 'Texas', 'TX', 223, 'US', 'Active', 0),
(3671, 'Utah', 'UT', 223, 'US', 'Active', 0),
(3672, 'Vermont', 'VT', 223, 'US', 'Active', 0),
(3673, 'Virgin Islands', 'VI', 223, 'US', 'Active', 0),
(3674, 'Virginia', 'VA', 223, 'US', 'Active', 0),
(3675, 'Washington', 'WA', 223, 'US', 'Active', 0),
(3676, 'West Virginia', 'WV', 223, 'US', 'Active', 0),
(3677, 'Wisconsin', 'WI', 223, 'US', 'Active', 0),
(3678, 'Wyoming', 'WY', 223, 'US', 'Active', 0),
(3679, 'Baker Island', 'BI', 224, 'UM', 'Active', 0),
(3680, 'Howland Island', 'HI', 224, 'UM', 'Active', 0),
(3681, 'Jarvis Island', 'JI', 224, 'UM', 'Active', 0),
(3682, 'Johnston Atoll', 'JA', 224, 'UM', 'Active', 0),
(3683, 'Kingman Reef', 'KR', 224, 'UM', 'Active', 0),
(3684, 'Midway Atoll', 'MA', 224, 'UM', 'Active', 0),
(3685, 'Navassa Island', 'NI', 224, 'UM', 'Active', 0),
(3686, 'Palmyra Atoll', 'PA', 224, 'UM', 'Active', 0),
(3687, 'Wake Island', 'WI', 224, 'UM', 'Active', 0),
(3688, 'Artigas', 'AR', 225, 'UY', 'Active', 0),
(3689, 'Canelones', 'CA', 225, 'UY', 'Active', 0),
(3690, 'Cerro Largo', 'CL', 225, 'UY', 'Active', 0),
(3691, 'Colonia', 'CO', 225, 'UY', 'Active', 0),
(3692, 'Durazno', 'DU', 225, 'UY', 'Active', 0),
(3693, 'Flores', 'FS', 225, 'UY', 'Active', 0),
(3694, 'Florida', 'FA', 225, 'UY', 'Active', 0),
(3695, 'Lavalleja', 'LA', 225, 'UY', 'Active', 0),
(3696, 'Maldonado', 'MA', 225, 'UY', 'Active', 0),
(3697, 'Montevideo', 'MO', 225, 'UY', 'Active', 0),
(3698, 'Paysandu', 'PA', 225, 'UY', 'Active', 0),
(3699, 'Rio Negro', 'RN', 225, 'UY', 'Active', 0),
(3700, 'Rivera', 'RV', 225, 'UY', 'Active', 0),
(3701, 'Rocha', 'RO', 225, 'UY', 'Active', 0),
(3702, 'Salto', 'SL', 225, 'UY', 'Active', 0),
(3703, 'San Jose', 'SJ', 225, 'UY', 'Active', 0),
(3704, 'Soriano', 'SO', 225, 'UY', 'Active', 0),
(3705, 'Tacuarembo', 'TA', 225, 'UY', 'Active', 0),
(3706, 'Treinta y Tres', 'TT', 225, 'UY', 'Active', 0),
(3707, 'Andijon', 'AN', 226, 'UZ', 'Active', 0),
(3708, 'Buxoro', 'BU', 226, 'UZ', 'Active', 0),
(3709, 'Farg\'ona', 'FA', 226, 'UZ', 'Active', 0),
(3710, 'Jizzax', 'JI', 226, 'UZ', 'Active', 0),
(3711, 'Namangan', 'NG', 226, 'UZ', 'Active', 0),
(3712, 'Navoiy', 'NW', 226, 'UZ', 'Active', 0),
(3713, 'Qashqadaryo', 'QA', 226, 'UZ', 'Active', 0),
(3714, 'Qoraqalpog\'iston Republikasi', 'QR', 226, 'UZ', 'Active', 0),
(3715, 'Samarqand', 'SA', 226, 'UZ', 'Active', 0),
(3716, 'Sirdaryo', 'SI', 226, 'UZ', 'Active', 0),
(3717, 'Surxondaryo', 'SU', 226, 'UZ', 'Active', 0),
(3718, 'Toshkent City', 'TK', 226, 'UZ', 'Active', 0),
(3719, 'Toshkent Region', 'TO', 226, 'UZ', 'Active', 0),
(3720, 'Xorazm', 'XO', 226, 'UZ', 'Active', 0),
(3721, 'Malampa', 'MA', 227, 'VU', 'Active', 0),
(3722, 'Penama', 'PE', 227, 'VU', 'Active', 0),
(3723, 'Sanma', 'SA', 227, 'VU', 'Active', 0),
(3724, 'Shefa', 'SH', 227, 'VU', 'Active', 0),
(3725, 'Tafea', 'TA', 227, 'VU', 'Active', 0),
(3726, 'Torba', 'TO', 227, 'VU', 'Active', 0),
(3727, 'Amazonas', 'AM', 229, 'VE', 'Active', 0),
(3728, 'Anzoategui', 'AN', 229, 'VE', 'Active', 0),
(3729, 'Apure', 'AP', 229, 'VE', 'Active', 0),
(3730, 'Aragua', 'AR', 229, 'VE', 'Active', 0),
(3731, 'Barinas', 'BA', 229, 'VE', 'Active', 0),
(3732, 'Bolivar', 'BO', 229, 'VE', 'Active', 0),
(3733, 'Carabobo', 'CA', 229, 'VE', 'Active', 0),
(3734, 'Cojedes', 'CO', 229, 'VE', 'Active', 0),
(3735, 'Delta Amacuro', 'DA', 229, 'VE', 'Active', 0),
(3736, 'Dependencias Federales', 'DF', 229, 'VE', 'Active', 0),
(3737, 'Distrito Federal', 'DI', 229, 'VE', 'Active', 0),
(3738, 'Falcon', 'FA', 229, 'VE', 'Active', 0),
(3739, 'Guarico', 'GU', 229, 'VE', 'Active', 0),
(3740, 'Lara', 'LA', 229, 'VE', 'Active', 0),
(3741, 'Merida', 'ME', 229, 'VE', 'Active', 0),
(3742, 'Miranda', 'MI', 229, 'VE', 'Active', 0),
(3743, 'Monagas', 'MO', 229, 'VE', 'Active', 0),
(3744, 'Nueva Esparta', 'NE', 229, 'VE', 'Active', 0),
(3745, 'Portuguesa', 'PO', 229, 'VE', 'Active', 0),
(3746, 'Sucre', 'SU', 229, 'VE', 'Active', 0),
(3747, 'Tachira', 'TA', 229, 'VE', 'Active', 0),
(3748, 'Trujillo', 'TR', 229, 'VE', 'Active', 0),
(3749, 'Vargas', 'VA', 229, 'VE', 'Active', 0),
(3750, 'Yaracuy', 'YA', 229, 'VE', 'Active', 0),
(3751, 'Zulia', 'ZU', 229, 'VE', 'Active', 0),
(3752, 'An Giang', 'AG', 230, 'VN', 'Active', 0),
(3753, 'Bac Giang', 'BG', 230, 'VN', 'Active', 0),
(3754, 'Bac Kan', 'BK', 230, 'VN', 'Active', 0),
(3755, 'Bac Lieu', 'BL', 230, 'VN', 'Active', 0),
(3756, 'Bac Ninh', 'BC', 230, 'VN', 'Active', 0),
(3757, 'Ba Ria-Vung Tau', 'BR', 230, 'VN', 'Active', 0),
(3758, 'Ben Tre', 'BN', 230, 'VN', 'Active', 0),
(3759, 'Binh Dinh', 'BH', 230, 'VN', 'Active', 0),
(3760, 'Binh Duong', 'BU', 230, 'VN', 'Active', 0),
(3761, 'Binh Phuoc', 'BP', 230, 'VN', 'Active', 0),
(3762, 'Binh Thuan', 'BT', 230, 'VN', 'Active', 0),
(3763, 'Ca Mau', 'CM', 230, 'VN', 'Active', 0),
(3764, 'Can Tho', 'CT', 230, 'VN', 'Active', 0),
(3765, 'Cao Bang', 'CB', 230, 'VN', 'Active', 0),
(3766, 'Dak Lak', 'DL', 230, 'VN', 'Active', 0),
(3767, 'Dak Nong', 'DG', 230, 'VN', 'Active', 0),
(3768, 'Da Nang', 'DN', 230, 'VN', 'Active', 0),
(3769, 'Dien Bien', 'DB', 230, 'VN', 'Active', 0),
(3770, 'Dong Nai', 'DI', 230, 'VN', 'Active', 0),
(3771, 'Dong Thap', 'DT', 230, 'VN', 'Active', 0),
(3772, 'Gia Lai', 'GL', 230, 'VN', 'Active', 0),
(3773, 'Ha Giang', 'HG', 230, 'VN', 'Active', 0),
(3774, 'Hai Duong', 'HD', 230, 'VN', 'Active', 0),
(3775, 'Hai Phong', 'HP', 230, 'VN', 'Active', 0),
(3776, 'Ha Nam', 'HM', 230, 'VN', 'Active', 0),
(3777, 'Ha Noi', 'HI', 230, 'VN', 'Active', 0),
(3778, 'Ha Tay', 'HT', 230, 'VN', 'Active', 0),
(3779, 'Ha Tinh', 'HH', 230, 'VN', 'Active', 0),
(3780, 'Hoa Binh', 'HB', 230, 'VN', 'Active', 0),
(3781, 'Ho Chin Minh', 'HC', 230, 'VN', 'Active', 0),
(3782, 'Hau Giang', 'HU', 230, 'VN', 'Active', 0),
(3783, 'Hung Yen', 'HY', 230, 'VN', 'Active', 0),
(3784, 'Saint Croix', 'C', 232, 'VI', 'Active', 0),
(3785, 'Saint John', 'J', 232, 'VI', 'Active', 0),
(3786, 'Saint Thomas', 'T', 232, 'VI', 'Active', 0),
(3787, 'Alo', 'A', 233, 'WF', 'Active', 0),
(3788, 'Sigave', 'S', 233, 'WF', 'Active', 0),
(3789, 'Wallis', 'W', 233, 'WF', 'Active', 0),
(3790, 'Abyan', 'AB', 235, 'YE', 'Active', 0),
(3791, 'Adan', 'AD', 235, 'YE', 'Active', 0),
(3792, 'Amran', 'AM', 235, 'YE', 'Active', 0),
(3793, 'Al Bayda', 'BA', 235, 'YE', 'Active', 0),
(3794, 'Ad Dali', 'DA', 235, 'YE', 'Active', 0),
(3795, 'Dhamar', 'DH', 235, 'YE', 'Active', 0),
(3796, 'Hadramawt', 'HD', 235, 'YE', 'Active', 0),
(3797, 'Hajjah', 'HJ', 235, 'YE', 'Active', 0),
(3798, 'Al Hudaydah', 'HU', 235, 'YE', 'Active', 0),
(3799, 'Ibb', 'IB', 235, 'YE', 'Active', 0),
(3800, 'Al Jawf', 'JA', 235, 'YE', 'Active', 0),
(3801, 'Lahij', 'LA', 235, 'YE', 'Active', 0),
(3802, 'Ma\'rib', 'MA', 235, 'YE', 'Active', 0),
(3803, 'Al Mahrah', 'MR', 235, 'YE', 'Active', 0),
(3804, 'Al Mahwit', 'MW', 235, 'YE', 'Active', 0),
(3805, 'Sa\'dah', 'SD', 235, 'YE', 'Active', 0),
(3806, 'San\'a', 'SN', 235, 'YE', 'Active', 0),
(3807, 'Shabwah', 'SH', 235, 'YE', 'Active', 0),
(3808, 'Ta\'izz', 'TA', 235, 'YE', 'Active', 0),
(3809, 'Kosovo', 'KOS', 236, 'YU', 'Active', 0),
(3810, 'Montenegro', 'MON', 236, 'YU', 'Active', 0),
(3811, 'Serbia', 'SER', 236, 'YU', 'Active', 0),
(3812, 'Vojvodina', 'VOJ', 236, 'YU', 'Active', 0),
(3813, 'Bas-Congo', 'BC', 237, 'ZR', 'Active', 0),
(3814, 'Bandundu', 'BN', 237, 'ZR', 'Active', 0),
(3815, 'Equateur', 'EQ', 237, 'ZR', 'Active', 0),
(3816, 'Katanga', 'KA', 237, 'ZR', 'Active', 0),
(3817, 'Kasai-Oriental', 'KE', 237, 'ZR', 'Active', 0),
(3818, 'Kinshasa', 'KN', 237, 'ZR', 'Active', 0),
(3819, 'Kasai-Occidental', 'KW', 237, 'ZR', 'Active', 0),
(3820, 'Maniema', 'MA', 237, 'ZR', 'Active', 0),
(3821, 'Nord-Kivu', 'NK', 237, 'ZR', 'Active', 0),
(3822, 'Orientale', 'OR', 237, 'ZR', 'Active', 0),
(3823, 'Sud-Kivu', 'SK', 237, 'ZR', 'Active', 0),
(3824, 'Central', 'CE', 238, 'ZM', 'Active', 0),
(3825, 'Copperbelt', 'CB', 238, 'ZM', 'Active', 0),
(3826, 'Eastern', 'EA', 238, 'ZM', 'Active', 0),
(3827, 'Luapula', 'LP', 238, 'ZM', 'Active', 0),
(3828, 'Lusaka', 'LK', 238, 'ZM', 'Active', 0),
(3829, 'Northern', 'NO', 238, 'ZM', 'Active', 0),
(3830, 'North-Western', 'NW', 238, 'ZM', 'Active', 0),
(3831, 'Southern', 'SO', 238, 'ZM', 'Active', 0),
(3832, 'Western', 'WE', 238, 'ZM', 'Active', 0),
(3833, 'Bulawayo', 'BU', 239, 'ZW', 'Active', 0),
(3834, 'Harare', 'HA', 239, 'ZW', 'Active', 0),
(3835, 'Manicaland', 'ML', 239, 'ZW', 'Active', 0),
(3836, 'Mashonaland Central', 'MC', 239, 'ZW', 'Active', 0),
(3837, 'Mashonaland East', 'ME', 239, 'ZW', 'Active', 0),
(3838, 'Mashonaland West', 'MW', 239, 'ZW', 'Active', 0),
(3839, 'Masvingo', 'MV', 239, 'ZW', 'Active', 0),
(3840, 'Matabeleland North', 'MN', 239, 'ZW', 'Active', 0),
(3841, 'Matabeleland South', 'MS', 239, 'ZW', 'Active', 0),
(3842, 'Midlands', 'MD', 239, 'ZW', 'Active', 0),
(3846, 'East Midlands', 'UKF', 222, 'GB', 'Active', 0),
(3847, 'East of England', 'UKH', 222, 'GB', 'Active', 0),
(3848, 'Greater London', 'UKI', 222, 'GB', 'Active', 0),
(3849, 'North East England', 'UKC', 222, 'GB', 'Active', 0),
(3850, 'North West England', 'UKD', 222, 'GB', 'Active', 0),
(3851, 'South East England', 'UKJ', 222, 'GB', 'Active', 0),
(3852, 'South West England', 'UKK', 222, 'GB', 'Active', 0),
(3853, 'West Midlands', 'UKG', 222, 'GB', 'Active', 0),
(3854, 'Yorkshire and the Humber', 'UKE', 222, 'GB', 'Active', 0),
(3855, 'Gujarat', 'GJ', 99, 'GB', 'Active', 0),
(3856, 'Gujarat', 'GJ1', 99, 'GB', 'Active', 0);

-- --------------------------------------------------------

--
-- Table structure for table `subaddon_category`
--

CREATE TABLE `subaddon_category` (
  `iSubAddCategoryId` int NOT NULL,
  `iAddCategoryId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vAddonName` varchar(255) NOT NULL,
  `vCode` varchar(255) NOT NULL,
  `vJaws` varchar(255) DEFAULT NULL,
  `iPrice` int NOT NULL,
  `vSequence` int NOT NULL,
  `dtAddedDate` datetime NOT NULL,
  `dtUpdatedDate` datetime NOT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL,
  `eType` enum('Upper','Lower','Both') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `subaddon_category`
--

INSERT INTO `subaddon_category` (`iSubAddCategoryId`, `iAddCategoryId`, `iCustomerId`, `vAddonName`, `vCode`, `vJaws`, `iPrice`, `vSequence`, `dtAddedDate`, `dtUpdatedDate`, `eStatus`, `eType`) VALUES
(11, 3, NULL, 'Welded mesh', 'AWM', '', 50, 30, '2022-04-12 03:58:55', '2022-05-04 04:43:08', 'Active', 'Both'),
(12, 4, NULL, 'welded mesh', 'AWM', '', 50, 10, '2022-04-12 03:59:30', '2022-04-12 03:59:30', 'Active', 'Lower'),
(13, 19, 11, 'Metal housing', 'MH', '', 35, 5, '2022-04-23 14:30:07', '2022-08-01 22:54:06', 'Active', 'Both'),
(14, 5, NULL, 'Implant labor', 'IML', '', 25, 20, '2022-04-23 14:30:42', '2022-04-23 14:30:42', 'Active', 'Upper'),
(15, 2, NULL, 'Metal housing', 'MH', '', 35, 5, '2022-04-23 14:31:25', '2022-04-23 14:33:49', 'Active', 'Lower'),
(16, 6, NULL, 'Implant labor', 'IML', '', 25, 10, '2022-04-23 14:36:21', '2022-04-23 14:36:21', 'Active', 'Lower'),
(22, 8, NULL, 'Open face A1', 'GTO', '', 150, 20, '2022-04-28 04:01:37', '2022-04-28 04:01:37', 'Active', 'Both'),
(23, 8, NULL, 'Open face A2', 'OP2', '', 150, 30, '2022-04-30 04:51:56', '2022-04-30 04:51:56', 'Active', 'Both'),
(24, 19, 11, 'Clear clasp', 'CC', '', 25, 5, '2022-05-01 20:06:41', '2022-08-01 22:54:11', 'Active', 'Both'),
(25, 19, NULL, 'Wire clasp', 'WC', '', 25, 10, '2022-05-01 20:07:21', '2022-05-01 20:07:21', 'Active', 'Both'),
(26, 3, NULL, 'Premade mesh', 'Prm', '', 15, 10, '2022-05-01 20:09:40', '2022-05-04 04:41:31', 'Active', 'Both'),
(27, 3, NULL, 'Custom mesh', 'CM', '', 45, 5, '2022-05-01 20:10:45', '2022-05-04 04:42:43', 'Active', 'Both'),
(29, 25, NULL, 'addon test', 'ADT', '', 200, 20, '2022-05-27 04:16:56', '2022-05-27 04:16:56', 'Active', 'Both');

-- --------------------------------------------------------

--
-- Table structure for table `toothbrand`
--

CREATE TABLE `toothbrand` (
  `iToothBrandId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `vName` varchar(255) DEFAULT NULL,
  `iSequence` int DEFAULT NULL,
  `eStatus` enum('Active','Inactive') DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `toothbrand`
--

INSERT INTO `toothbrand` (`iToothBrandId`, `iCustomerId`, `vName`, `iSequence`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(5, 11, 'Kulzer', 1, 'Active', '2022-08-02 03:48:34', '2022-03-09 12:02:34'),
(6, NULL, 'Ivoclar', 2, 'Active', '2022-03-11 04:21:06', '2022-03-09 12:02:44'),
(12, NULL, 'Krupesh Brand', 2, 'Active', '2022-05-25 05:29:44', '2022-05-23 07:24:47'),
(18, NULL, 'Test Brand 1', 20, 'Active', '2022-05-27 10:52:07', '2022-05-27 10:52:07');

-- --------------------------------------------------------

--
-- Table structure for table `toothshades`
--

CREATE TABLE `toothshades` (
  `iToothShadesId` int NOT NULL,
  `iToothBrandId` int DEFAULT NULL,
  `iCustomerId` int NOT NULL,
  `vToothShade` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `vCode` varchar(255) DEFAULT NULL,
  `iSequence` int DEFAULT NULL,
  `eStatus` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `toothshades`
--

INSERT INTO `toothshades` (`iToothShadesId`, `iToothBrandId`, `iCustomerId`, `vToothShade`, `vCode`, `iSequence`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(3, 6, 0, NULL, 'A2', 10, 'Active', '2022-03-26 07:54:06', '2022-03-10 14:27:34'),
(4, 5, 0, 'A2', 'A2', 5, 'Active', '2022-05-01 07:22:48', '2022-03-11 04:28:27'),
(5, 5, 0, 'A1', 'A1', 2, 'Active', '2022-04-14 05:33:51', '2022-04-14 05:33:51'),
(6, 5, 0, 'A3', 'A3', 8, 'Active', '2022-05-01 07:23:41', '2022-04-14 05:35:24'),
(7, 5, 0, NULL, 'A3.5', 10, 'Active', '2022-05-01 19:24:16', '2022-05-01 19:24:16'),
(8, 5, 0, NULL, 'A4', 12, 'Active', '2022-05-01 19:24:36', '2022-05-01 19:24:36'),
(9, 12, 11, NULL, 'KB', 1, 'Active', '2022-08-02 03:44:15', '2022-05-23 07:25:02'),
(10, 17, 0, NULL, 'TB', 20, 'Active', '2022-05-26 12:42:19', '2022-05-26 12:42:19'),
(14, 18, 0, NULL, 'TB01', 95, 'Active', '2022-05-27 11:30:21', '2022-05-27 11:30:21');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `iUserId` int NOT NULL,
  `iCustomerId` int DEFAULT NULL,
  `iDoctorId` int DEFAULT NULL,
  `iOfficeId` int DEFAULT NULL,
  `vFirstName` varchar(255) DEFAULT NULL,
  `vLastName` varchar(255) DEFAULT NULL,
  `vUserName` varchar(255) DEFAULT NULL,
  `vTitle` varchar(255) DEFAULT NULL,
  `vEmail` varchar(255) DEFAULT NULL,
  `vPassword` varchar(255) DEFAULT NULL,
  `vMobile` varchar(255) DEFAULT NULL,
  `tDescription` text,
  `eStatus` enum('Active','Inactive','In Vacation','Fired','Resigned','Sick','Suspended') NOT NULL DEFAULT 'Active',
  `dtAddedDate` datetime DEFAULT NULL,
  `dtUpdatedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iUserId`, `iCustomerId`, `iDoctorId`, `iOfficeId`, `vFirstName`, `vLastName`, `vUserName`, `vTitle`, `vEmail`, `vPassword`, `vMobile`, `tDescription`, `eStatus`, `dtAddedDate`, `dtUpdatedDate`) VALUES
(21, 11, NULL, 6, 'peter', 'parker', '9314015932', 'hmc', 'peter@milinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(987) 456-1330', 'demo', 'Active', '2022-08-04 01:44:56', '2022-03-02 10:35:07'),
(22, 11, 18, 6, 'Kasim', 'Abbas', 'kasimabbaspa', 'PA', 'kasim@mailinator.com', '2568884880', '1234588793123', 'Abbas note che', 'Active', '2022-03-02 10:50:15', '2022-03-02 10:50:04'),
(25, 11, NULL, 7, 'demo', 'demo', 'demo', 'demo', 'demo@mail.co', 'fe01ce2a7fbac8fafaed7c982a04e229', '(448) 787-5557', 'demo', 'Active', '2022-03-14 06:16:14', '2022-03-03 12:57:19'),
(28, 11, 34, 11, 'test user', 'user', 'user 1', 'test title', 'usertest@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(644) 545-4545', 'test user', 'Active', '2022-05-26 11:47:44', '2022-05-26 11:47:44'),
(30, 11, 19, 7, 'KTTTT', 'KTTTT', 'KTTTT', 'KTTTT11', 'KTTTT@mailinator.com', 'd41d8cd98f00b204e9800998ecf8427e', '(226) 262-3233', 'KTTTT', 'Active', '2022-07-23 12:32:07', '2022-07-23 01:39:29'),
(31, 11, 19, 7, 'Kaminey', 'Kaminey', 'Kaminey', 'Kaminey', 'Kaminey@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(253) 435-1212', 'Kaminey', 'Active', '2022-07-23 02:13:42', '2022-07-23 02:13:42'),
(32, 12, 19, 7, 'ab007', 'ab007', 'ab007', 'ab007', 'ab007@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', '(551) 212-1212', 'ab007', 'Active', '2022-07-26 04:11:23', '2022-07-26 04:11:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_category`
--
ALTER TABLE `addon_category`
  ADD PRIMARY KEY (`iAddCategoryId`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`iAdminId`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`iBannerId`);

--
-- Indexes for table `calendar_event`
--
ALTER TABLE `calendar_event`
  ADD PRIMARY KEY (`iEventId`);

--
-- Indexes for table `call`
--
ALTER TABLE `call`
  ADD PRIMARY KEY (`iCallId`);

--
-- Indexes for table `case`
--
ALTER TABLE `case`
  ADD PRIMARY KEY (`iCaseId`);

--
-- Indexes for table `casepan`
--
ALTER TABLE `casepan`
  ADD PRIMARY KEY (`iCasepanId`);

--
-- Indexes for table `casepannumber`
--
ALTER TABLE `casepannumber`
  ADD PRIMARY KEY (`iCasepanNumberId`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`iCategoryId`);

--
-- Indexes for table `categoryproduct`
--
ALTER TABLE `categoryproduct`
  ADD PRIMARY KEY (`iCategoryProductId`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`iCountryId`),
  ADD KEY `mod_country_vCountry_index` (`iCountryId`),
  ADD KEY `mod_country_eStatus_index` (`eStatus`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`iCustomerId`);

--
-- Indexes for table `customertype`
--
ALTER TABLE `customertype`
  ADD PRIMARY KEY (`iCustomerTypeId`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`iDepartmentId`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`iDoctorId`);

--
-- Indexes for table `driver_history`
--
ALTER TABLE `driver_history`
  ADD PRIMARY KEY (`iDriverHistoryId`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`iEmailId`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`iFeesId`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`iGradeId`);

--
-- Indexes for table `gumbrand`
--
ALTER TABLE `gumbrand`
  ADD PRIMARY KEY (`iGumBrandId`);

--
-- Indexes for table `gumshades`
--
ALTER TABLE `gumshades`
  ADD PRIMARY KEY (`iGumShadesId`);

--
-- Indexes for table `impression`
--
ALTER TABLE `impression`
  ADD PRIMARY KEY (`iImpressionId`);

--
-- Indexes for table `lab_admin`
--
ALTER TABLE `lab_admin`
  ADD PRIMARY KEY (`iLabAdminId`);

--
-- Indexes for table `module_master`
--
ALTER TABLE `module_master`
  ADD PRIMARY KEY (`iModuleId`);

--
-- Indexes for table `module_permission`
--
ALTER TABLE `module_permission`
  ADD PRIMARY KEY (`iPermissionId`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`iNotificationId`);

--
-- Indexes for table `office`
--
ALTER TABLE `office`
  ADD PRIMARY KEY (`iOfficeId`);

--
-- Indexes for table `office_admin`
--
ALTER TABLE `office_admin`
  ADD PRIMARY KEY (`iOfficeAdminId`);

--
-- Indexes for table `productstage`
--
ALTER TABLE `productstage`
  ADD PRIMARY KEY (`iProductStageId`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`iRoleId`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`iScheduleId`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slip`
--
ALTER TABLE `slip`
  ADD PRIMARY KEY (`iSlipId`),
  ADD KEY `iCaseId` (`iCaseId`);

--
-- Indexes for table `slip_addons`
--
ALTER TABLE `slip_addons`
  ADD PRIMARY KEY (`iSlipAddonsId`);

--
-- Indexes for table `slip_attachment`
--
ALTER TABLE `slip_attachment`
  ADD PRIMARY KEY (`iSlipAttachmentId`);

--
-- Indexes for table `slip_impression`
--
ALTER TABLE `slip_impression`
  ADD PRIMARY KEY (`iSlipImpressionId`);

--
-- Indexes for table `slip_notes`
--
ALTER TABLE `slip_notes`
  ADD PRIMARY KEY (`iSlipNotesId`);

--
-- Indexes for table `slip_product`
--
ALTER TABLE `slip_product`
  ADD PRIMARY KEY (`iSlipProductId`),
  ADD KEY `iSlipId` (`iSlipId`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`iStaffId`);

--
-- Indexes for table `subaddon_category`
--
ALTER TABLE `subaddon_category`
  ADD PRIMARY KEY (`iSubAddCategoryId`);

--
-- Indexes for table `toothbrand`
--
ALTER TABLE `toothbrand`
  ADD PRIMARY KEY (`iToothBrandId`);

--
-- Indexes for table `toothshades`
--
ALTER TABLE `toothshades`
  ADD PRIMARY KEY (`iToothShadesId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iUserId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_category`
--
ALTER TABLE `addon_category`
  MODIFY `iAddCategoryId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `iAdminId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `iBannerId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `calendar_event`
--
ALTER TABLE `calendar_event`
  MODIFY `iEventId` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `call`
--
ALTER TABLE `call`
  MODIFY `iCallId` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `case`
--
ALTER TABLE `case`
  MODIFY `iCaseId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `casepan`
--
ALTER TABLE `casepan`
  MODIFY `iCasepanId` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `casepannumber`
--
ALTER TABLE `casepannumber`
  MODIFY `iCasepanNumberId` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `iCategoryId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `categoryproduct`
--
ALTER TABLE `categoryproduct`
  MODIFY `iCategoryProductId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `iCustomerId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `customertype`
--
ALTER TABLE `customertype`
  MODIFY `iCustomerTypeId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `iDepartmentId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `iDoctorId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `driver_history`
--
ALTER TABLE `driver_history`
  MODIFY `iDriverHistoryId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `iEmailId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `iFeesId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `grade`
--
ALTER TABLE `grade`
  MODIFY `iGradeId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `gumbrand`
--
ALTER TABLE `gumbrand`
  MODIFY `iGumBrandId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `gumshades`
--
ALTER TABLE `gumshades`
  MODIFY `iGumShadesId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `impression`
--
ALTER TABLE `impression`
  MODIFY `iImpressionId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `lab_admin`
--
ALTER TABLE `lab_admin`
  MODIFY `iLabAdminId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `module_master`
--
ALTER TABLE `module_master`
  MODIFY `iModuleId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `module_permission`
--
ALTER TABLE `module_permission`
  MODIFY `iPermissionId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2753;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `iNotificationId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `office`
--
ALTER TABLE `office`
  MODIFY `iOfficeId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `office_admin`
--
ALTER TABLE `office_admin`
  MODIFY `iOfficeAdminId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `productstage`
--
ALTER TABLE `productstage`
  MODIFY `iProductStageId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `iRoleId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `iScheduleId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `slip`
--
ALTER TABLE `slip`
  MODIFY `iSlipId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `slip_addons`
--
ALTER TABLE `slip_addons`
  MODIFY `iSlipAddonsId` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slip_attachment`
--
ALTER TABLE `slip_attachment`
  MODIFY `iSlipAttachmentId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `slip_impression`
--
ALTER TABLE `slip_impression`
  MODIFY `iSlipImpressionId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `slip_notes`
--
ALTER TABLE `slip_notes`
  MODIFY `iSlipNotesId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `slip_product`
--
ALTER TABLE `slip_product`
  MODIFY `iSlipProductId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `iStaffId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `subaddon_category`
--
ALTER TABLE `subaddon_category`
  MODIFY `iSubAddCategoryId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `toothbrand`
--
ALTER TABLE `toothbrand`
  MODIFY `iToothBrandId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `toothshades`
--
ALTER TABLE `toothshades`
  MODIFY `iToothShadesId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iUserId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
